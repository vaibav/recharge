<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('/')->group(__DIR__.'/admin.php');
Route::prefix('/')->group(__DIR__.'/moneytransfer.php');
Route::prefix('/')->group(__DIR__.'/retailer.php');

Route::get('/','LoginController@index');

Route::get('rech_test','B_TestController@index');

/*
|--------------------------------------------------------------------------
| Admin
|--------------------------------------------------------------------------
|
*/



// New Entries
// ADMIN BACKUP NEW RETAILER
Route::get('n_backup_retailer','AD_BackupRetailerController@index')->middleware("validadmin");
Route::post('n_backup_retailer_store','AD_BackupRetailerController@store')->middleware("validadmin");

// ADMIN BACKUP UPDATE BALANCE RETAILER
Route::get('n_backup_update_retailer','AD_Backup_Bal_RetController@index')->middleware("validadmin");
Route::post('n_backup_update_retailer_store','AD_Backup_Bal_RetController@store')->middleware("validadmin");

// ADMIN BACKUP NEW RETAILER
Route::get('n_backup_distributor','AD_Backup_Dis_Controller@index')->middleware("validadmin");
Route::post('n_backup_distributor_store','AD_Backup_Dis_Controller@store')->middleware("validadmin");

// ADMIN BACKUP UPDATE BALANCE DISTRIBUTOR
Route::get('n_backup_update_distributor','AD_Backup_Bal_DisController@index')->middleware("validadmin");
Route::post('n_backup_update_distributor_store','AD_Backup_Bal_DisController@store')->middleware("validadmin");

// ADMIN CHECK ACCOUNT
Route::get('n_check_account_1','AD_CheckAccountController@index')->middleware("validadmin");
Route::post('n_check_account_2','AD_CheckAccountController@view')->middleware("validadmin");

//NETWORK PACKAGE
Route::get('n_netpack','AD_NetworkPackController@index')->middleware("validadmin");
Route::get('n_netpack/{pack_id}','AD_NetworkPackController@view')->middleware("validadmin");
Route::get('n_netpack/{pack_id}/{net_code}','AD_NetworkPackController@view_net')->middleware("validadmin");
Route::post('n_netpack_store','AD_NetworkPackController@store')->middleware("validadmin");
Route::get('n_netpack_status/{trans_id}','AD_NetworkPackController@change_status')->middleware("validadmin");
Route::get('n_netpack_delete/{trans_id}','AD_NetworkPackController@delete')->middleware("validadmin");

//Log
Route::get('n_log','AD_LogController@index')->middleware("validadmin");
Route::get('n_log_view','AD_LogController@view')->middleware("validadmin");


// NETWORK TYPE ENTRY MODULE
Route::get('network_type','AD_NetTypeController@index')->middleware("validadmin");
Route::post('network_type_store','AD_NetTypeController@store')->middleware("validadmin");
Route::get('network_type_delete/{net_code}','AD_NetTypeController@delete')->middleware("validadmin");

// NETWORK ENTRY MODULE
Route::get('network','NetworkController@index')->middleware("validadmin");
Route::post('network_store','NetworkController@store')->middleware("validadmin");
Route::get('network_delete/{net_code}','NetworkController@delete')->middleware("validadmin");

// NETWORK SURPLUS CHARGE DETAILS
Route::get('network_surplus','NetworkSurplusController@index')->middleware("validadmin");
Route::get('network_surplus/{net_code}','NetworkSurplusController@view')->middleware("validadmin");
Route::post('network_surplus_store','NetworkSurplusController@store')->middleware("validadmin");
Route::get('network_surplus_delete/{tr_id}','NetworkSurplusController@delete')->middleware("validadmin");

// API PROVIDER REQUEST ENTRY
Route::get('apirequest','ApiproviderController@index')->middleware("validadmin");
Route::post('apirequest_store','ApiproviderController@store')->middleware("validadmin");
Route::get('apirequest_edit','ApiproviderController@edit')->middleware("validadmin");
Route::get('apirequest_edit1/{code}','ApiproviderController@edit1')->middleware("validadmin");
Route::post('apirequest_update','ApiproviderController@update')->middleware("validadmin");

Route::get('n_apirequest','AD_ApiController@index')->middleware("validadmin");
Route::post('n_apirequest_store','AD_ApiController@store')->middleware("validadmin");
Route::get('n_apirequest_network','AD_ApiController@index_network')->middleware("validadmin");
Route::post('n_apirequest_network_store','AD_ApiController@store_network')->middleware("validadmin");
Route::get('n_apirequest_network_get','AD_ApiController@get_api_network');

Route::get('n_apiresult','AD_ApiResultController@index')->middleware("validadmin");
Route::get('n_apiresult/{api_code}','AD_ApiResultController@view')->middleware("validadmin");
Route::post('n_apiresult_store','AD_ApiResultController@store')->middleware("validadmin");
Route::get('n_apiresult_delete/{tr_id}','AD_ApiResultController@delete')->middleware("validadmin");

// API PROVIDER METHOD ENTRY
Route::get('apiprovider_method','ApiProviderMethodController@index')->middleware("validadmin");
Route::get('apiprovider_method_change/{api_code}','ApiProviderMethodController@change_method')->middleware("validadmin");

// PIN CHECK
Route::post('check_pin','AdminPinController@index')->middleware("validadmin");

// API PROVIDER RESULT ENTRY
Route::get('apiresult','ApiproviderResultController@index')->middleware("validadmin");
Route::get('apiresult/{api_code}','ApiproviderResultController@view')->middleware("validadmin");
Route::post('apiresult_store','ApiproviderResultController@store')->middleware("validadmin");
Route::get('apiresult_delete/{tr_id}','ApiproviderResultController@delete')->middleware("validadmin");

// API PROVIDER RESULT ENTRY
Route::get('apidirectresult','ApiProviderDirectResultController@index')->middleware("validadmin");
Route::get('apidirectresult/{api_code}','ApiProviderDirectResultController@view')->middleware("validadmin");
Route::post('apidirectresult_store','ApiProviderDirectResultController@store')->middleware("validadmin");
Route::get('apidirectresult_delete/{tr_id}','ApiProviderDirectResultController@delete')->middleware("validadmin");

// API URL DETAILS
Route::get('api_details_1','AdminApiUrlController@index')->middleware("validadmin");
Route::get('api_details_2','AdminApiUrlController@view_date')->middleware("validadmin");

// NETWORK LINE ENTRY
Route::get('network_line','NetworkLineController@index')->middleware("validadmin");
Route::get('network_line/{net_code}','NetworkLineController@view')->middleware("validadmin");
Route::post('network_line_store','NetworkLineController@store')->middleware("validadmin");
Route::get('network_line_delete/{tr_id}','NetworkLineController@delete')->middleware("validadmin");

// NETWORK LINE API PARTNER ENTRY
Route::get('network_line_apipartner','NetworkLineApipartnerController@index')->middleware("validadmin");
Route::get('network_line_apipartner/{net_code}/{user_name}','NetworkLineApipartnerController@view')->middleware("validadmin");
Route::post('network_line_apipartner_store','NetworkLineApipartnerController@store')->middleware("validadmin");
Route::get('network_line_apipartner_delete/{tr_id}','NetworkLineApipartnerController@delete')->middleware("validadmin");

// ADMIN USER ENTRY
Route::get('user','UserController@index')->middleware("validadmin");
Route::post('user_store','UserController@store')->middleware("validadmin");
Route::post('user_update','UserController@update')->middleware("validadmin");
Route::post('user_update_photo','UserController@update_photo')->middleware("validadmin");
Route::post('user_update_network','UserController@update_network')->middleware("validadmin");
Route::get('user_delete/{user_code}','UserDeleteController@delete')->middleware("validadmin");
Route::get('user_update_status/{user_code}','UserController@update_user_status')->middleware("validadmin");
Route::get('user_view','UserController@viewall')->middleware("validadmin");
Route::get('user_view/{user_code}','UserController@viewone')->middleware("validadmin");
Route::get('user_view_network/{user_code}','UserController@viewnetwork')->middleware("validadmin");
Route::get('result','UserController@getresult')->middleware("validadmin");
Route::get('check_user/{user_name}','UserController@checkUserName');
Route::get('check_mobile/{user_mobile}','UserController@checkUserMobile');
Route::get('user_approval_view','AdminApprovalController@index')->middleware("validadmin");
Route::get('user_approval_status/{user_code}','AdminApprovalController@update_user_approval')->middleware("validadmin");
// ADMIN RECORD INSERTION ENTRY
Route::get('user_admin','UserController@storeAdmin');

// AGENT ALLOCATION
Route::get('agent_allocation','AgentAreaController@index')->middleware("validadmin");
Route::post('agent_allocation_store','AgentAreaController@store')->middleware("validadmin");

// AGENT COLLECTION VERIFY
Route::get('collection_verify','AgentCollectionVerifyController@index')->middleware("validadmin");
Route::get('collection_verify_view','AgentCollectionVerifyController@view')->middleware("validadmin");
Route::get('collection_verify_success/{trans_id}','AgentCollectionVerifyController@successUpdate')->middleware("validadmin");
Route::get('collection_verify_failure/{trans_id}','AgentCollectionVerifyController@failureUpdate')->middleware("validadmin");

// ADMIN REMOTE FUND
Route::get('remotepayment','AD_Pay_RemoteController@index')->middleware("validadmin");
Route::post('remotepayment_store','AD_Pay_RemoteController@store')->middleware("validadmin");
Route::get('get_ub/{user_name}','AD_Pay_RemoteController@getUserBalance');

// ADMIN OPENING BALANCE FUND
Route::get('opening_balance_entry','OpeningBalanceController@index')->middleware("validadmin");
Route::post('opening_balance_store','OpeningBalanceController@store')->middleware("validadmin");
Route::get('opening_balance_view','OpeningBalanceController@view')->middleware("validadmin");
Route::get('opening_balance_all','OpeningBalanceController@store_all')->middleware("validadmin");


// ADMIN PAYMENT ACCEPT CONTROLLER
Route::get('paymentaccept','AD_Pay_AcceptController@index')->middleware("validadmin");
Route::get('paymentaccept_store/{trans_id}/{trans_amt}','AD_Pay_AcceptController@store')->middleware("validadmin");
Route::get('paymentaccept_delete/{trans_id}','AD_Pay_AcceptController@delete')->middleware("validadmin");

// PAYMENT LEDGER CONTROLLER
Route::get('moveall','PaymentLedgerController@move_all')->middleware("validadmin");

// ADMIN STOCK DETAILS
Route::get('stockdetails','AdminStockController@index')->middleware("validadmin");
Route::post('stockdetails_view','AdminStockController@viewdate')->middleware("validadmin");
Route::post('stockdetails_excel','AdminStockController@viewdate_excel')->middleware("validadmin");
Route::get('stockdetails_all','AdminStockController@viewall')->middleware("validadmin");
Route::post('stockdetails_view_all','AdminStockController@viewalldate')->middleware("validadmin");

// ADMIN USER STOCK DETAILS
Route::get('admin_user_stock_1','ADMIN_StockReportController@index')->middleware("validadmin");
Route::get('admin_user_stock_2','ADMIN_StockReportController@viewdate')->middleware("validadmin");

// ADMIN REFUND PAYMENT
Route::get('refundpayment','AD_Pay_RefundController@index')->middleware("validadmin");
Route::post('refundpayment_store','AD_Pay_RefundController@store')->middleware("validadmin");

// ADMIN PAYMENT DETAILS
Route::get('payment_report','AD_Pay_ReportController@index')->middleware("validadmin");
Route::get('payment_view','AD_Pay_ReportController@view_details')->middleware("validadmin");
Route::get('payment_report_update1/{trans_id}','AD_Pay_ReportController@store_update1')->middleware("validadmin");
Route::get('payment_report_update2/{trans_id}','AD_Pay_ReportController@store_update2')->middleware("validadmin");

// CUSTOMER PENDING COLLECTION
Route::get('collection','AgentPaymentCollectionController@index_admin')->middleware("validadmin");
Route::post('collection_view','AgentPaymentCollectionController@view_admin')->middleware("validadmin");

// ADMIN RECHARGE DETAILS
Route::get('rechargedetails','AD_Rech_ReportController@index')->middleware("validadmin");
Route::get('rechargedetails_view','AD_Rech_ReportController@viewdate')->middleware("validadmin");
Route::get('rechargedetails_pdf','RechargeReportController@viewdate_pdf')->middleware("validadmin");
Route::get('rechargedetails_excel','RechargeReportController@viewdate_excel_new')->middleware("validadmin");
Route::get('rechargedetails_view1','RechargeReportController@viewdate1')->middleware("validadmin");

// ADMIN RECHARGE EBBILL DETAILS
Route::get('rechargebilldetails','RechargeBillReportController@index')->middleware("validadmin");
Route::get('rechargebilldetails_view','RechargeBillReportController@viewdate')->middleware("validadmin");
Route::get('rechargebilldetails_excel','RechargeBillReportController@viewdate_excel_new')->middleware("validadmin");

// ADMIN RECHARGE INFO DETAILS
Route::get('rechargeinfo','UserRechargeOfferController@index')->middleware("validadmin");
Route::post('rechargeinfo_view','UserRechargeOfferController@viewdate')->middleware("validadmin");

// ADMIN OLD RECHARGE DETAILS
Route::get('admin_user_old_recharge_1','ADMIN_Old_RechargeController@index')->middleware("validadmin");
Route::get('admin_user_old_recharge_2','ADMIN_Old_RechargeController@viewdate')->middleware("validadmin");

// ADMIN SMS DETAILS
Route::get('smsdetails','SmsSendController@view')->middleware("validadmin");
Route::get('smsdetails_view','SmsSendController@viewdate')->middleware("validadmin");

// ADMIN DISTRIBUTOR RECHARGE DETAILS
Route::get('rechargedetails_distributor','AD_Rech_DisReportController@index')->middleware("validadmin");
Route::get('rechargedetails_distributor_view','AD_Rech_DisReportController@viewdate')->middleware("validadmin");
Route::get('rechargedetails_distributor_excel','AD_Rech_DisReportController@viewdate_excel')->middleware("validadmin");

// ADMIN SUPER DISTRIBUTOR RECHARGE DETAILS
Route::get('rechargedetails_super','RechargeReportSuperController@index')->middleware("validadmin");
Route::get('rechargedetails_super_view','RechargeReportSuperController@viewdate')->middleware("validadmin");
Route::get('rechargedetails_super_excel','RechargeReportSuperController@viewdate_excel')->middleware("validadmin");

// ADMIN BONRIX RESULT DETAILS
Route::get('bonrixresult','BonrixApiResultController@index');
Route::get('bonrixresult_data','BonrixApiResultController@store');

// ADMIN RECHARGE REQUEST DETAILS
Route::get('rechargerequest','AD_Rech_ReqReportController@index')->middleware("validadmin");
Route::get('rechargerequest_view','AD_Rech_ReqReportController@viewdate')->middleware("validadmin");
Route::post('rechargerequest_excel','AD_Rech_ReqReportController@viewexcel')->middleware("validadmin");

Route::get('serverreply','AD_Reply_ReportController@index')->middleware("validadmin");
Route::get('serverresult_view','AD_Reply_ReportController@viewdate')->middleware("validadmin");

// ADMIN PENDING REPORT DETAILS
Route::get('pendingreport','AD_Pending_RechargeController@index')->middleware("validadmin");
Route::get('pendingreport_success/{trans_id}/{opr_id}','AD_Pending_RechargeController@successUpdate')->middleware("validadmin");
Route::get('pendingreport_failure/{trans_id}/{opr_id}','AD_Pending_RechargeController@failureUpdate')->middleware("validadmin");
Route::get('pendingreport_reprocess/{trans_id}','PendingReportController@webReprocess')->middleware("validadmin");
Route::get('pendingreport_reprocessapi/{trans_id}','PendingReportController@apiReprocess')->middleware("validadmin");

// ADMIN PENDING BILL PAYMENT
Route::get('pendingbill','UserRechargeBillController@viewadmin')->middleware("validadmin");
Route::get('pendingbill_success/{trans_id}/{opr_id}','UserRechargeBillController@success_update');
Route::get('pendingbill_failure/{trans_id}/{opr_id}','UserRechargeBillController@failure_update');

// ADMIN PENDING MONEY PAYMENT
Route::get('pendingmoney','AD_Pending_MoneyController@index')->middleware("validadmin");
Route::get('pendingmoney_success/{trans_id}/{opr_id}/{money_amt}','AD_Pending_MoneyController@success_update');
Route::get('pendingmoney_failure/{trans_id}/{opr_id}/{money_amt}','AD_Pending_MoneyController@failure_update');

// USER COMPLAINT DETAILS
Route::get('complaint','UserComplaintController@admin_view')->middleware("validadmin");
Route::get('complaint_update/{trans_id}/{admin_reply}','UserComplaintController@complaint_update')->middleware("validadmin");
Route::get('complaint_refund/{trans_id}/{admin_reply}/{rech_trans_id}','UserComplaintController@complaint_refund')->middleware("validadmin");
Route::get('complaint_view','UserComplaintController@admin_view1')->middleware("validadmin");

// ADMIN OFFER DETAILS
Route::get('offer_entry','AdminOfferController@index')->middleware("validadmin");
Route::post('offer_store','AdminOfferController@store')->middleware("validadmin");
Route::get('offer_view','AdminOfferController@admin_view')->middleware("validadmin");
Route::get('offer_inactive/{trans_id}','AdminOfferController@offer_inactive')->middleware("validadmin");
Route::get('offer_delete/{trans_id}','AdminOfferController@offer_delete')->middleware("validadmin");

// CHANGE PARENT
Route::get('changeparent','ChangeParentController@index')->middleware("validadmin");
Route::get('changeparent/{user_name}/{user_code}','ChangeParentController@getParent')->middleware("validadmin");
Route::post('changeparent_store','ChangeParentController@store')->middleware("validadmin");

// CHANGE PASSWORD
Route::get('changepassword','ChangePasswordController@index_admin')->middleware("validadmin");
Route::post('changepassword_store','ChangePasswordController@store')->middleware("validadmin");

// MOBILE USER
Route::get('mobile_user','MobileUserController@index')->middleware("validadmin");
Route::get('mobile_user_delete/{auth_token}','MobileUserController@store_delete')->middleware("validadmin");
Route::get('mobile_user_update/{auth_token}','MobileUserController@store_update')->middleware("validadmin");

// SEND SMS
Route::get('smssend','SmsSendController@index')->middleware("validadmin");
Route::post('smssend_store','SmsSendController@store')->middleware("validadmin");

// BACKUP DATABASE
Route::get('backup','RechargeAllController@getDetails')->middleware("validadmin");
Route::get('backup_view','RechargeAllController@getUserRechargeDetails');
Route::get('backup_view1','RechargeAllController@getUserRechargeAllTotal');
Route::get('backup_view2','RechargeAllController@getUserRechargeAllTotal1');
Route::get('backup_view_all','RechargeAllController@getUserCurrentRechargeTotal')->middleware("validadmin");
Route::get('backup_distributor_all','RechargeAllController@getUserCurrentDistributorTotal')->middleware("validadmin");
Route::get('backup_total','RechargeAllController@getUserRechargeTotal')->middleware("validadmin");
Route::get('backup_parent','RechargeAllParentController@getParentDetails')->middleware("validadmin");
Route::get('backup_parent_total','RechargeAllParentController@getUserRechargeParentTotal')->middleware("validadmin");
Route::get('backup_parent_view','RechargeAllParentController@getUserRechargeDetails')->middleware("validadmin");
Route::get('pay_view','PaymentCorrectController@index');

// BACKUP DATABASE NEW
Route::get('backup_recharge','ADMIN_BKUP_RechargeController@store');
Route::get('backup_recharge_add_old','ADMIN_BKUP_RechargeController@add_old');

Route::get('backup_recharge_ds','ADMIN_BKUP_RechargeParentController@store');
Route::get('backup_recharge_ds_add_old','ADMIN_BKUP_RechargeParentController@add_old');


// BACKUP DELETE DATABASE
Route::get('backup_recharge_delete','ADMIN_BKUP_RechargeController@delete');

// OFFER LINE ENTRY
Route::get('offer_line','OfferLineController@index')->middleware("validadmin");
Route::get('offer_line/{net_code}','OfferLineController@view')->middleware("validadmin");
Route::post('offer_line_store','OfferLineController@store')->middleware("validadmin");
Route::get('offer_line_delete/{tr_id}','OfferLineController@delete')->middleware("validadmin");

// API PARTNER BROWSER DETAILS
Route::get('apiagent','ApipartnerBrowserController@index')->middleware("validadmin");
Route::post('apiagent_store','ApipartnerBrowserController@store')->middleware("validadmin");

// USER BANK REMITTER DETAILS
Route::get('bank_agent_view_all','UserBankAgentController@view_all')->middleware("validadmin");

// USER BANK REMITTER DETAILS
Route::get('bank_remitter_view_all','UserBankRemitterController@view_all')->middleware("validadmin");
Route::get('bank_remitter_update_success/{rem_id}/{opr_id}','UserBankRemitterController@update_success')->middleware("validadmin");
Route::get('bank_remitter_update_failure/{rem_id}/{opr_id}','UserBankRemitterController@update_failure')->middleware("validadmin");

// USER BENEFICIARY DETAILS
Route::get('admin_ben_view_1','ADMIN_BenViewController@index')->middleware("validadmin");
Route::get('admin_ben_view_2','ADMIN_BenViewController@view')->middleware("validadmin");
Route::get('ben_update_success/{bn_id}/{opr_id}','UserBeneficiaryController@update_success')->middleware("validadmin");
Route::get('ben_update_failure/{bn_id}/{opr_id}','UserBeneficiaryController@update_failure')->middleware("validadmin");

// USER MONEY TRANSFER DETAILS
Route::get('moneyreport','MoneyTransferReportController@index_admin')->middleware("validadmin");
Route::get('moneyreport_view','MoneyTransferReportController@viewdate_admin')->middleware("validadmin");




// ADVERTISEMENT ENTRY
Route::get('ad_entry','AD_AdController@index')->middleware("validadmin");
Route::post('ad_store','AD_AdController@store')->middleware("validadmin");
Route::get('ad_view','AD_AdController@view')->middleware("validadmin");
Route::get('ad_edit/{trans_id}','AD_AdController@edit')->middleware("validadmin");
Route::post('ad_update','AD_AdController@update')->middleware("validadmin");
Route::get('ad_delete/{trans_id}','AD_AdController@delete')->middleware("validadmin");

// LOGIN
Route::get('login','LoginController@index');
Route::post('login_check','LoginController@check');
Route::get('logout','LoginController@logout');


/*
|--------------------------------------------------------------------------
| Super Distributor and Distributor
|--------------------------------------------------------------------------
|
*/

// USER DASHBOARD
Route::get('dashboard_user','SD_DashboardController@index')->middleware("validuser");

// SUPER DISTRIBUTOR USER-ENTRY
Route::get('user_user','SD_UserController@index')->middleware("validuser");
Route::post('user_store_user','SD_UserController@store')->middleware("validuser");
Route::get('user_view_user','SD_UserController@viewall')->middleware("validuser");
Route::get('user_view_user/{user_code}','SD_UserController@view_one')->middleware("validuser");
Route::get('user_view_network_user/{user_code}','SD_UserController@view_network')->middleware("validuser");
Route::get('check_user1/{user_name}','SD_UserController@checkUserName')->middleware("validuser");
Route::get('check_mobile1/{user_mobile}','SD_UserController@checkUserMobile')->middleware("validuser");

// SUPER DISTRIBUTOR AGENT ALLOCATION
Route::get('sd_agent_allocation','SD_Agent_AreaController@index')->middleware("validuser");
Route::post('sd_agent_allocation_store','SD_Agent_AreaController@store')->middleware("validuser");

// SUPER DISTRIBUTOR AGENT COLLECTION VERIFY
Route::get('sd_collection_verify','SD_Agent_CverifyController@index')->middleware("validuser");
Route::get('sd_collection_verify_view','SD_Agent_CverifyController@view')->middleware("validuser");
Route::get('sd_collection_verify_success/{trans_id}','SD_Agent_CverifyController@successUpdate')->middleware("validuser");
Route::get('sd_collection_verify_failure/{trans_id}','SD_Agent_CverifyController@failureUpdate')->middleware("validuser");

// SUPER DISTRIBUTOR PENDING COLLECTION
Route::get('sd_collection','SD_Agent_CreportController@index')->middleware("validuser");
Route::post('sd_collection_view','SD_Agent_CreportController@view')->middleware("validuser");

// USER (DISTRIBUTOR, SUPER DISTRIBUTOR) REMOTE PAYMENT
Route::get('remotepayment_user','SD_PaymentController@remote_index')->middleware("validuser");
Route::post('remotepayment_store_user','SD_PaymentController@store')->middleware("validuser");

// SUPER DISTRIBUTOR PAYMENT REQUEST
Route::get('paymentrequest_user','SD_PaymentController@request_index')->middleware("validuser");
Route::post('paymentrequest_store_user','SD_PaymentController@store_request')->middleware("validuser");

// SUPER DISTRIBUTOR PAYMENT ACCEPT
Route::get('paymentaccept_user','SD_PaymentController@index_accept')->middleware("validuser");
Route::get('paymentaccept_store_user/{trans_id}/{trans_amt}','SD_PaymentController@store_accept')->middleware("validuser");
Route::get('paymentaccept_delete_user/{trans_id}','SD_PaymentController@store_delete')->middleware("validuser");

// SUPER DISTRIBUTOR STOCK DETAILS
Route::get('stockdetails_user','SD_StockController@index')->middleware("validuser");
Route::get('stockdetails_view_user','SD_StockController@viewdate')->middleware("validuser");

// SUPER DISTRIBUTOR RECHARGE DETAILS
Route::get('rechargedetails_user','SD_RechargeReportController@index')->middleware("validuser");
Route::get('rechargedetails_user_view','SD_RechargeReportController@viewdate')->middleware("validuser");

// USER SUPER DISTRIBUTOR PAYMENT ACCEPT CONTROLLER
Route::get('paymentaccept_user1','UserPaymentAcceptController@index')->middleware("validuser");
Route::get('paymentaccept_store_user1/{trans_id}/{trans_amt}','UserPaymentAcceptController@store')->middleware("validuser");
Route::get('paymentaccept_delete_user1/{trans_id}','UserPaymentAcceptController@delete')->middleware("validuser");

// USER COMPLAINT DETAILS
Route::get('complaint_user','SD_ComplaintController@index')->middleware("validuser");
Route::post('complaint_user_store','SD_ComplaintController@store')->middleware("validuser");
Route::get('complaint_view_user','SD_ComplaintController@user_view')->middleware("validuser");

// CHANGE PASSWORD
Route::get('changepassword_distributor','ChangePasswordController@index_distributor')->middleware("validuser");
Route::post('changepassword1_store','ChangePasswordController@store_distributor')->middleware("validuser");

// DISTRIBUTOR DASHBOARD
Route::get('dashboard_distributor','DS_DashboardController@index')->middleware("validuser");

// DISTRIBUTOR RECHARGE DETAILS
Route::get('user_rechargedetails_distributor','DS_RechargeReportController@index')->middleware("validuser");
Route::get('user_rechargedetails_distributor_view','DS_RechargeReportController@viewdate')->middleware("validuser");

// DISTRIBUTOR STOCK DETAILS
Route::get('stockdetails_distributor','DS_StockController@index')->middleware("validuser");
Route::get('stockdetails_view_distributor','DS_StockController@viewdate')->middleware("validuser");

// DISTRIBUTOR REMOTE PAYMENT
Route::get('remotepayment_distributor','DS_PaymentController@remote_index')->middleware("validuser");
Route::post('remotepayment_store_distributor','DS_PaymentController@store')->middleware("validuser");

// DISTRIBUTOR PAYMENT REQUEST
Route::get('paymentrequest_distributor','DS_PaymentController@request_index')->middleware("validuser");
Route::post('paymentrequest_store_distributor','DS_PaymentController@store_request')->middleware("validuser");

// DISTRIBUTOR PAYMENT ACCEPT
Route::get('paymentaccept_distributor','DS_PaymentController@index_accept')->middleware("validuser");
Route::get('paymentaccept_store_distributor/{trans_id}/{trans_amt}','DS_PaymentController@store_accept')->middleware("validuser");
Route::get('paymentaccept_delete_distributor/{trans_id}','DS_PaymentController@store_delete')->middleware("validuser");

// DISTRIBUTOR USER ENTRY
Route::get('user_distributor','DS_UserController@index')->middleware("validuser");
Route::post('user_store_distributor','DS_UserController@store')->middleware("validuser");
Route::get('user_view_distributor','DS_UserController@viewall')->middleware("validuser");
Route::get('user_view_distributor/{user_code}','DS_UserController@view_one')->middleware("validuser");
Route::get('user_view_network_distributor/{user_code}','DS_UserController@view_network')->middleware("validuser");

// DISTRIBUTOR AGENT ALLOCATION
Route::get('ds_agent_allocation','DS_Agent_AreaController@index')->middleware("validuser");
Route::post('ds_agent_allocation_store','DS_Agent_AreaController@store')->middleware("validuser");

// DISTRIBUTOR AGENT COLLECTION VERIFY
Route::get('ds_collection_verify','DS_Agent_CverifyController@index')->middleware("validuser");
Route::get('ds_collection_verify_view','DS_Agent_CverifyController@view')->middleware("validuser");
Route::get('ds_collection_verify_success/{trans_id}','DS_Agent_CverifyController@successUpdate')->middleware("validuser");
Route::get('ds_collection_verify_failure/{trans_id}','DS_Agent_CverifyController@failureUpdate')->middleware("validuser");

// DISTRIBUTOR PENDING COLLECTION
Route::get('ds_collection','DS_Agent_CreportController@index')->middleware("validuser");
Route::post('ds_collection_view','DS_Agent_CreportController@view')->middleware("validuser");

// DISTRIBUTOR COMPLAINT DETAILS
Route::get('complaint_distributor','DS_ComplaintController@index')->middleware("validuser");
Route::post('complaint_distributor_store','DS_ComplaintController@store')->middleware("validuser");
Route::get('complaint_view_distributor','DS_ComplaintController@user_view')->middleware("validuser");

/*
|--------------------------------------------------------------------------
| Retailer
|--------------------------------------------------------------------------
|
*/

// NEW
Route::get('dashboard_retailer','RT_Recharge_Controller@index')->middleware("validretailer");
Route::post('recharge_nw_1','RT_Recharge_Controller@store_1')->middleware("validretailer");
Route::post('recharge_nw_store','RT_Recharge_Controller@store_2')->middleware("validretailer");

Route::get('recharge_nw_3','RT_Recharge_Controller@store_temp');

Route::get('ebbill_nw','RT_EBBill_Controller@index')->middleware("validretailer");
Route::post('ebbill_nw_1','RT_EBBill_Controller@store_1')->middleware("validretailer");
Route::post('ebbill_nw_store','RT_EBBill_Controller@store_2')->middleware("validretailer");

// USER RECHARGE DETAILS
Route::get('rechargedetails_retailer','RT_RechargeReportController@index')->middleware("validretailer");
Route::get('rechargedetails_retailer_view','RT_RechargeReportController@viewdate')->middleware("validretailer");
Route::get('rechargedetails_retailer_excel','RT_RechargeReportController@viewdate_excel')->middleware("validretailer");
Route::get('rechargedetails_invoice/{trans_id}','RechargeInvoiceController@view_invoice')->middleware("validretailer");
Route::get('rechargedetails_invoice_t/{trans_id}','RechargeInvoiceController@view_invoice_thermal')->middleware("validretailer");

// USER RECHARGE EBBILL DETAILS
Route::get('recharge_bill_retailer','RT_Rech_Bill_ReportController@index')->middleware("validretailer");
Route::get('rechargedetails_bill_retailer_view','RT_Rech_Bill_ReportController@viewdate')->middleware("validretailer");
Route::get('rechargedetails_bill_retailer_excel','RechargeBillRetailerController@viewdate_excel')->middleware("validretailer");
Route::get('rechargebill_invoice/{trans_id}','RechargeInvoiceController@view_bill_invoice')->middleware("validretailer");
Route::get('rechargebill_invoice_t/{trans_id}','RechargeInvoiceController@view_bill_invoice_thermal')->middleware("validretailer");

// USER MONEY TRANSFER DETAILS
Route::get('recharge_money_retailer','RT_Rech_Money_ReportController@index')->middleware("validretailer");
Route::get('rechargedetails_money_retailer_view','RT_Rech_Money_ReportController@viewdate')->middleware("validretailer");
Route::get('rechargedetails_money_retailer_excel','RT_Rech_Money_ReportController@viewdate_excel')->middleware("validretailer");

// PAYMENT REQUEST
Route::get('paymentrequest_retailer','RT_Pay_Req_Controller@index')->middleware("validretailer");
Route::post('paymentrequest_store_retailer','RT_Pay_Req_Controller@store')->middleware("validretailer");


// USER DASHBOARD
Route::get('dashboard_retailer_od','UserRechargeController@index_new_final')->middleware("validretailer");
Route::get('dashboard_retailer_new','UserRechargeController@index_dashboard_new')->middleware("validretailer");
Route::get('dashboard_money_new','MoneyTransferController@index_money_new')->middleware("validretailer");
Route::get('userresult_data','UserRechargeController@loadData')->middleware("validretailer");
Route::get('network_data/{net_type_code}','UserRechargeController@getNetworkData')->middleware("validretailer");

// New 121 OFFERS
Route::get('rech_offer','RT_RechargeOfferController@check_offer');
Route::get('rech_plan','RT_RechargeOfferController@check_plan');
Route::get('rech_dth','RT_RechargeOfferController@check_dth');
Route::get('rech_eb','RT_RechargeOfferController@check_eb');
Route::get('rech_operator','RT_RechargeOfferController@check_operator');

Route::get('m_offer','RT_ANDR_RechOfferController@m_offer');
Route::get('m_plan','RT_ANDR_RechOfferController@m_plan');
Route::get('m_dth','RT_ANDR_RechOfferController@m_dth');

// USER DETAILS
Route::get('user_one','RT_UserController@viewall_user')->middleware("validretailer");
Route::get('user_one_1/{user_code}','RT_UserController@viewone_user')->middleware("validretailer");
Route::get('user_one_2/{user_code}','RT_UserController@viewnetwork_user')->middleware("validretailer");
Route::get('surplus_retailer','NetworkSurplusController@view_retailer')->middleware("validretailer");



// USER STOCK DETAILS
Route::get('stockdetails_retailer','UserStockController@index_retailer')->middleware("validretailer");
Route::get('stockdetails_view_retailer','UserStockController@viewdate_retailer')->middleware("validretailer");
Route::post('stockdetails_view_retailer_excel','UserStockController@viewdate_retailer_excel')->middleware("validretailer");

// USER RECHARGE
Route::get('recharge','RT_RechargeMobileController@index')->middleware("validretailer");
Route::get('recharge_final','UserRechargeController@index_new_final')->middleware("validretailer");

Route::get('recharge_bill_final','UserRechargeController@index_eb_final')->middleware("validretailer");
Route::post('recharge_store','UserRechargeController@store')->middleware("validretailer");
Route::post('recharge_bill','UserRechargeBillController@store')->middleware("validretailer");
Route::post('recharge_money','MoneyTransferController@store')->middleware("validretailer");
Route::post('recharge_money_otp','MoneyTransferController@store_otp')->middleware("validretailer");
Route::get('geteb_data/{con_no}','UserRechargeBillController@getEBdetails');

Route::get('recharge_new','RT_RechargeMobileController@index');
Route::post('recharge_store_1','RT_RechargeMobileController@prepaid_recharge')->middleware("validretailer");
Route::post('recharge_store_2','RT_RechargeMobileController@dth_recharge')->middleware("validretailer");
Route::post('recharge_store_3','RT_RechargeMobileController@postpaid_recharge')->middleware("validretailer");
Route::get('recharge_offer_new/{net_code}/{mobile}','RT_RechargeMobileController@getOfferMobile');
Route::get('recharge_plan_new/{net_code}','RT_RechargeMobileController@getMobilePlans');
Route::get('recharge_dthinfo_new/{net_code}/{mobile}','RT_RechargeMobileController@getMobileDthInfo');
Route::get('recharge_operator_new/{mobile}','RT_RechargeMobileController@getOperatorCheck');

// COLLECTION DETAILS
Route::get('collection_retailer','AgentRetailerCollectionController@index')->middleware("validretailer");


// PENDING REPORT - PARTRIIC
Route::get('pendingreport_success_1/{trans_id}/{opr_id}','PendingReportController@successUpdate');
Route::get('pendingreport_failure_1/{trans_id}/{opr_id}','PendingReportController@failureUpdate');


//USER BANK AGENT DETAILS
Route::get('bank_agent_entry','UserBankAgentController@index')->middleware("validretailer");
Route::post('bank_agent_entry_store','UserBankAgentController@store')->middleware("validretailer");
Route::get('bank_agent_otp_entry','UserBankAgentController@index_otp')->middleware("validretailer");
Route::post('bank_agent_otp_entry_store','UserBankAgentController@store_otp')->middleware("validretailer");
Route::post('bank_agent_otp_resend','UserBankAgentController@send_otp')->middleware("validretailer");
Route::get('bank_agent_view','UserBankAgentController@view_one')->middleware("validretailer");

//USER BENEFICIARY DETAILS
Route::get('ben_entry','UserBeneficiaryController@index')->middleware("validretailer");
Route::post('ben_entry_store','UserBeneficiaryController@store')->middleware("validretailer");
Route::get('ben_otp_entry/{ben_acc_no}','UserBeneficiaryController@index_otp')->middleware("validretailer");
Route::get('ben_otp_entry_store/{ben_id}/{msisdn}/{trans_id}/{otp}','UserBeneficiaryController@store_otp')->middleware("validretailer");
Route::get('ben_otp_resend/{ben_id}/{msisdn}/{trans_id}','UserBeneficiaryController@send_otp')->middleware("validretailer");
Route::get('ben_view','UserBeneficiaryController@view_one')->middleware("validretailer");
Route::get('check_account_number/{bk_acc_no}','UserBeneficiaryController@checkAccountNo')->middleware("validretailer");
Route::post('check_ben_account','UserBeneficiaryController@check_account');
Route::get('check_ben_acc_no/{ben_acc_no}/{msisdn}','UserBeneficiaryController@checkAccountNo')->middleware("validretailer");
Route::get('ben_delete/{ben_acc_no}/{msisdn}/{beneficiary_id}','UserBeneficiaryController@delete')->middleware("validretailer");
Route::get('ben_delete_otp/{trans_id}/{msisdn}/{otp}','UserBeneficiaryController@delete_otp')->middleware("validretailer");
Route::get('rechargemoney_invoice/{trans_id}','RechargeInvoiceController@view_money_invoice')->middleware("validretailer");
Route::get('rechargemoney_invoice_t/{trans_id}','RechargeInvoiceController@view_money_invoice_thermal')->middleware("validretailer");


//USER BANK AGENT DETAILS
Route::get('bank_agent_view','UserBankAgentController@view_one')->middleware("validretailer");
Route::get('bank_agent_entry','UserBankAgentController@index')->middleware("validretailer");
Route::post('bank_agent_entry_store','UserBankAgentController@store')->middleware("validretailer");
Route::get('bank_agent_otp_entry','UserBankAgentController@index_otp')->middleware("validretailer");
Route::post('bank_agent_otp_entry_store','UserBankAgentController@store_otp')->middleware("validretailer");

// RECHARGE OFFERS
Route::post('recharge_offers','UserRechargeOfferController@getOffer');
Route::post('recharge_offers1','UserRechargeOfferController@getOffer1');
Route::get('recharge_offers_mobile/{net_code}/{mob}','UserRechargeOfferController@getOfferMobile');
Route::post('mobile_plans','UserRechargeOfferController@getMobilePlans');
Route::get('mobile_plans_mobile/{net_code}','UserRechargeOfferController@getMobilePlansMobile');
Route::post('dth_offers','UserRechargeOfferController@getDthinfo');
Route::get('dth_offers1/{mob}/{net_code}','UserRechargeOfferController@getDthinfo1');
Route::get('recharge_mobile_check/{mob}','UserRechargeOfferController@getOperatorCheck');
Route::post('eb_info','UserRechargeOfferController@getEbinfo')->middleware("validretailer");
Route::get('money_percentage','MoneyTransferController@getRetailerPer')->middleware("validretailer");

// USER MONEY TRANSFER DETAILS
Route::get('moneyreport_retailer','MoneyTransferReportController@index_retailer')->middleware("validretailer");
Route::get('moneyreport_retailer_view','MoneyTransferReportController@viewdate_retailer')->middleware("validretailer");


// USER MONEY VERIFICATION DETAILS
Route::get('report_money_verify','RetMoneyVerifyController@index')->middleware("validretailer");
Route::get('report_money_verify_view','RetMoneyVerifyController@viewdate')->middleware("validretailer");

// USER COMPLAINT DETAILS
Route::get('complaint_view_retailer','UserComplaintController@user_view')->middleware("validretailer");

// NORMAL COMPLAINT
Route::get('complaint_retailer','UserComplaintController@index_retailer')->middleware("validretailer");
Route::get('complaint_retailer_normal','UserComplaintController@normal_retailer')->middleware("validretailer");
Route::post('complaint_retailer_normal_store','UserComplaintController@store_retailer')->middleware("validretailer");
Route::get('complaint_retailer_recharge','UserComplaintController@recharge_retailer')->middleware("validretailer");
Route::get('complaint_retailer_recharge_store','UserComplaintController@store_retailer_recharge')->middleware("validretailer");

// TEST 
Route::get('test1','TestController@index')->middleware("validretailer");
Route::get('test2','TestController@view')->middleware("validretailer");
Route::get('testpdf','TestController@testpdf');

// CHANGE PASSWORD
Route::get('changepassword_retailer','ChangePasswordController@index_retailer')->middleware("validretailer");
Route::post('changepassword2_store','ChangePasswordController@store_retailer')->middleware("validretailer");



/*
|--------------------------------------------------------------------------
| Response
|--------------------------------------------------------------------------
|
*/

// API SERVER RESPONSE
Route::get('response/{data?}','ResponseController@view');
Route::get('response1/{data1}','ResponseController@view1');

/*
|--------------------------------------------------------------------------
| Api Partiner Url
|--------------------------------------------------------------------------
|
*/

// API PARTNER REQUEST
Route::get('apirecharge/{data?}','ApipartnerController@store');
Route::get('apireprocess/{data?}','ApipartnerController@storeReprocess');
Route::get('webreprocess/{data?}','UserRechargeController@storeWebReprocess');

// API PARTNER DASHBOARD
Route::get('dashboard_apipartner','AP_DashboardController@index')->middleware("validapipartner");
Route::get('apipartnerresult_data','AP_DashboardController@loadData')->middleware("validapipartner");
Route::get('apipartner_rech_data','AP_DashboardController@rech_data');
Route::get('apipartner_chart_data','AP_DashboardController@chart_data');

// USER DETAILS
Route::get('user_one_api','AP_UserController@index')->middleware("validapipartner");
Route::get('user_one_api_1/{user_code}','AP_UserController@viewone_user')->middleware("validapipartner");
Route::get('user_one_api_2/{user_code}','AP_UserController@viewnetwork_user')->middleware("validapipartner");
Route::get('surplus_apipartner','AP_UserController@view_surplus')->middleware("validapipartner");

// PAYMENT REQUEST
Route::get('paymentrequest_apipartner','AP_PaymentRequestController@index')->middleware("validapipartner");
Route::post('paymentrequest_store_apipartner','AP_PaymentRequestController@store')->middleware("validapipartner");

// USER STOCK DETAILS
Route::get('stockdetails_apipartner','AP_StockController@index')->middleware("validapipartner");
Route::get('stockdetails_view_apipartner','AP_StockController@viewdate')->middleware("validapipartner");
Route::get('stockdetails_view_apipartner_excel','UserStockController@viewdate_apipartner_excel')->middleware("validapipartner");

// API PARTNER TEST RESPONSE URL
Route::get('responsetest/{data?}','ResponseController@testurl');
Route::get('responsetest1/{data?}','ResponseController@testurl1');

// USER RECHARGE DETAILS
Route::get('rechargedetails_apipartner','AP_RechargeReportController@index')->middleware("validapipartner");
Route::get('rechargedetails_apipartner_view','AP_RechargeReportController@viewdate')->middleware("validapipartner");
Route::get('rechargedetails_apipartner_excel','AP_RechargeReportController@viewdate_excel')->middleware("validapipartner");

// USER RECHARGE EBBILL DETAILS
Route::get('recharge_bill_apipartner','RechargeBillRetailerController@index_api')->middleware("validapipartner");
Route::get('rechargedetails_bill_apipartner_view','RechargeBillRetailerController@viewdate_api')->middleware("validapipartner");
Route::get('rechargedetails_bill_apipartner_excel','RechargeBillRetailerController@viewdate_excel_api')->middleware("validapipartner");

// USER REMITTER DETAILS
Route::get('remitter_apipartner','AP_RemitterReportController@view_remitter')->middleware("validapipartner");
Route::get('beneficiary_apipartner','AP_RemitterReportController@view_beneficiary')->middleware("validapipartner");

// USER COMPLAINT DETAILS
Route::get('complaint_apipartner','AP_ComplaintController@index')->middleware("validapipartner");
Route::get('complaint_apipartner_recharge','AP_ComplaintController@recharge_apipartner')->middleware("validapipartner");
Route::get('complaint_apipartner_recharge_store','AP_ComplaintController@store_apipartner_recharge')->middleware("validapipartner");
Route::get('complaint_view_apipartner','AP_ComplaintController@user_view')->middleware("validapipartner");

// IP ADDRESS ENTRY
Route::get('apipartner_ipentry','ApipartnerIpaddressController@index')->middleware("validapipartner");
Route::post('apipartner_ipentry_store','ApipartnerIpaddressController@store')->middleware("validapipartner");

// CHANGE PASSWORD
Route::get('changepassword_apipartner','ChangePasswordController@index_api_partner')->middleware("validapipartner");
Route::post('changepassword3_store','ChangePasswordController@store_apipartner')->middleware("validapipartner");

// Token Generation
Route::get('api_token_1','AP_ApiTokenController@index')->middleware("validapipartner");
Route::post('api_token_2','AP_ApiTokenController@store')->middleware("validapipartner");

/*
|--------------------------------------------------------------------------
| Agent
|--------------------------------------------------------------------------
|
*/

// AGENT DASHBOARD
Route::get('dashboard_agent','AgentDashboardController@index')->middleware("validagent");

// USER DETAILS
Route::get('user_one_agent','AgentUserController@index')->middleware("validagent");
Route::get('user_one_agent_1/{user_code}','AgentUserController@viewone_user')->middleware("validagent");

// COLLECTION DETAILS
Route::get('collection_details','AgentPaymentCollectionController@index')->middleware("validagent");

// COLLECTION
Route::get('pay_agent','AgentPaymentCollectionController@pay_agent')->middleware("validagent");
Route::get('pay_agent/{user_name}','AgentPaymentCollectionController@pay_agent_1')->middleware("validagent");
Route::post('pay_agent_store','AgentPaymentCollectionController@pay_agent_store')->middleware("validagent");

// COLLECTION REPORT
Route::get('collection_report','AgentCollectionReportController@index')->middleware("validagent");
Route::get('collection_report_view','AgentCollectionReportController@view')->middleware("validagent");


// TEST AJAX
Route::get('test3','UserRechargeController@bonrixUrl')->middleware("validretailer");
Route::get('test4','UserRechargeController@bonrixUrlResult')->middleware("validretailer");

Route::get('testeb','TestController@testeb');
Route::get('getbank','TestController@getBank');
Route::get('getifsc','TestController@getIFSC');
Route::get('checkaccount','TestController@checkAccount');
Route::get('getallifsc','TestController@getAllIFSC');
Route::get('getstate','TestController@getState');
Route::get('getagent','TestController@getAgent');
Route::get('getcustomer','TestController@getCustomer');
Route::get('testairtel','TestController@checkPOST');
Route::get('testsri','TestController@test_recharge');
Route::get('testoffer','TestController@checkoffers');

// ANDROID WEB UI
Route::get('mobile_user_prepaid1','UserRechargeMobileController@index_all');
Route::get('mobile_user_postpaid','UserRechargeMobileController@index_postpaid');
Route::get('mobile_user_dth','UserRechargeMobileController@index_dth');
Route::post('recharge_mobile_prepaid_store', 'UserRechargeMobileController@prepaid_recharge');
Route::post('recharge_mobile_postpaid_store', 'UserRechargeMobileController@postpaid_recharge');
Route::post('recharge_mobile_dth_store', 'UserRechargeMobileController@dth_recharge');

// RETAILER RECHARGE
Route::get('mobile_user_prepaid','AndroidUserRechargeController@index');
Route::post('recharge_mobile_prepaid_store1', 'AndroidUserRechargeController@prepaid_recharge');
Route::post('recharge_mobile_postpaid_store1', 'AndroidUserRechargeController@postpaid_recharge');
Route::post('recharge_mobile_dth_store1', 'AndroidUserRechargeController@dth_recharge');

// new recharge details
Route::get('mobile_user_det','AndroidUserRechargeController@ret_det');

// RETAILER RECHARGE REPORT
Route::get('mobile_user_recharge_rt_1','AndroidRetRechReportController@index');
Route::get('mobile_user_recharge_rt_2/{auth_token}','AndroidRetRechReportController@view');
Route::get('mobile_user_recharge_rt_3','AndroidRetRechReportController@view_date');

// RETAILER EBBILL
Route::get('mobile_user_ebbill','AndroidUserEbbillController@index');
Route::post('recharge_mobile_ebbill_store', 'AndroidUserEbbillController@ebbill_recharge');

// RETAILER MONEY LOGIN
Route::get('mobile_user_money_1','AndroidUserMoneyController@index');
Route::get('mobile_user_money_out','AndroidUserMoneyController@logout');
Route::post('mobile_user_money_2', 'AndroidUserMoneyController@user_check');
Route::get('mobile_user_money_dash/{msisdn}/moneydashboard/{auth_token}',['as' => 'moneydashboard', 'uses' => 'AndroidUserMoneyController@dashboard'] );

// RETAILER MONEY TRANSFER
Route::get('mobile_user_money_tr_1/{msisdn}/{auth_token}','AndroidUserMoneyTransferController@index');
Route::get('money_percentage_mobile/{user_name}/{mt_amount}/{r_mode}','AndroidUserMoneyTransferController@getRetailerPer');
Route::post('mobile_user_money_tr_2','AndroidUserMoneyTransferController@store');

// RETAILER BENEFICIARY ADD
Route::get('mobile_user_money_ab_1/{msisdn}/{auth_token}','AndroidUserBenRegisterController@index');
Route::get('mobile_user_money_ab_2/{msisdn}/{auth_token}','AndroidUserBenRegisterController@add_view');
Route::post('mobile_user_money_ab_store','AndroidUserBenRegisterController@store');
Route::get('mobile_user_money_ab_store_otp','AndroidUserBenRegisterController@store_otp');
Route::get('mobile_user_money_ab_resend_otp','AndroidUserBenRegisterController@send_otp');
Route::post('mobile_user_money_ab_verify','AndroidUserBenRegisterController@check_account');

// RETAILER MONEY TRANSFER REPORT
Route::get('mobile_user_money_rp_1/{msisdn}/{auth_token}','AndroidUserMoneyTransferController@getMoneyDetailsRetailer');
Route::get('mobile_user_money_rp_2/{msisdn}/{auth_token}','AndroidUserMoneyTransferController@getMoneyFilterDetailsRetailer_1');
Route::get('mobile_user_money_rp_3','AndroidUserMoneyTransferController@getMoneyFilterDetailsRetailer_2');

// RETAILER BENEFICIARY DELETE
Route::get('mobile_user_money_db_1/{msisdn}/{auth_token}','AndroidUserBenDeleteController@index');
Route::get('mobile_user_money_db_delete','AndroidUserBenDeleteController@delete');
Route::get('mobile_user_money_db_delete_otp','AndroidUserBenDeleteController@delete_otp');

// RETAILER REMITTER ADD
Route::get('mobile_user_money_rm_1/{auth_token}','AndroidUserRemRegisterController@index');
Route::post('mobile_user_money_rm_store','AndroidUserRemRegisterController@store');
Route::get('mobile_user_money_rm_otp_1/{msisdn}/{auth_token}','AndroidUserRemRegisterController@checkOtp');
Route::post('mobile_user_money_rm_otp_store','AndroidUserRemRegisterController@otpStore');

// RETAILER COMPLAINT ENTRY
Route::get('mobile_user_complaint_rt_1','AndroidRetComplaintController@index');
Route::get('mobile_user_complaint_rt_2','AndroidRetComplaintController@view');
Route::get('mobile_user_complaint_rt_3','AndroidRetComplaintController@store');
Route::get('mobile_user_complaint_rt_4','AndroidRetComplaintController@view_complaint');

// RETAILER PAYMENT REQUEST ENTRY
Route::get('mobile_user_payment_request_rt_1','RT_ANDR_PayRequest@index');
Route::post('mobile_user_payment_request_rt_2','RT_ANDR_PayRequest@store');
Route::get('mobile_user_payment_request_rt_3','RT_ANDR_PayRequest@view');

// RETAILER PROFILE
Route::get('mobile_user_profile_rt_1','AndroidRetProfileController@index');
Route::get('mobile_user_profile_rt_2','AndroidRetProfileController@percentage');
Route::get('mobile_user_profile_rt_3','AndroidRetProfileController@surplus');

// RETAILER MY ACCOUNT
Route::get('mobile_retailer_myaccount','AndroidMyaccountController@index_retailer');
Route::get('mobile_distributor_myaccount','AndroidMyaccountController@index_distributor');
Route::get('mobile_user_stock_rt_1','RT_ANDR_StockController@index');
Route::get('mobile_user_stock_rt_2','RT_ANDR_StockController@view_date');

// RETAILER DASHBOARD
Route::get('mobile_retailer_dashboard','RT_ANDR_DashboardController@index');
Route::get('mobile_chart_data_rt','RT_ANDR_DashboardController@getRechargeGraphData');

// RETAILER CHANGE PASSWORD
Route::get('mobile_user_password_rt_1','AndroidRetProfileController@changePassword');
Route::post('mobile_user_password_rt_2','AndroidRetProfileController@changePasswordStore');

// RETAILER COLLECTION DETAILS
Route::get('mobile_user_collection_rp_rt_1','RT_ANDR_CollectionController@index');
Route::get('mobile_user_collection_rp_rt_2','RT_ANDR_CollectionController@view_date');

// APIPARTNER RECHARGE REPORT
Route::get('mobile_user_recharge_ap_1','AP_ANDR_RechReportController@index');
Route::get('mobile_user_recharge_ap_2/{auth_token}','AP_ANDR_RechReportController@view');
Route::get('mobile_user_recharge_ap_3','AP_ANDR_RechReportController@view_date');

// APIPARTNER PAYMENT REQUEST ENTRY
Route::get('mobile_user_payment_request_ap_1','AP_PaymentRequestController@index_android');
Route::post('mobile_user_payment_request_ap_2','AP_PaymentRequestController@store_android');
Route::get('mobile_user_payment_request_ap_3','AP_PaymentRequestController@view');

// APIPARTNER COMPLAINT ENTRY
Route::get('mobile_user_complaint_ap_1','AP_ANDR_ComplaintController@index');
Route::get('mobile_user_complaint_ap_2','AP_ANDR_ComplaintController@view');
Route::get('mobile_user_complaint_ap_3','AP_ANDR_ComplaintController@store');
Route::get('mobile_user_complaint_ap_4','AP_ANDR_ComplaintController@view_complaint');

// API PARTNER PROFILE
Route::get('mobile_user_profile_ap_1','AP_ANDR_UserController@index');
Route::get('mobile_user_profile_ap_2','AP_ANDR_UserController@percentage');
Route::get('mobile_user_profile_ap_3','AP_ANDR_UserController@surplus');

// API PARTNER DASHBOARD
Route::get('mobile_apipartner_dashboard','AP_ANDR_DashboardController@index');
Route::get('mobile_chart_data_ap','AP_ANDR_DashboardController@getRechargeGraphData');

// RETAILER CHANGE PASSWORD
Route::get('mobile_user_password_ap_1','AP_ANDR_UserController@changePassword');
Route::post('mobile_user_password_ap_2','AP_ANDR_UserController@changePasswordStore');

// DISTRIBUTOR USER ENTRY
Route::get('mobile_distributor_new','DS_ANDR_UserController@index');
Route::post('mobile_distributor_store','DS_ANDR_UserController@store');

// DISTRIBUTOR REMOTE PAYMENT
Route::get('mobile_remote_payment_ds','DS_ANDR_PaymentController@remote_index');
Route::post('mobile_remote_payment_ds_store','DS_ANDR_PaymentController@store');
Route::get('mobile_remote_payment_ds_rp','DS_ANDR_PaymentController@remote_view');

// DISTRIBUTOR PAYMENT REQUEST ENTRY
Route::get('mobile_user_payment_request_ds_1','DS_ANDR_PaymentController@request_index');
Route::post('mobile_user_payment_request_ds_2','DS_ANDR_PaymentController@store_request');
Route::get('mobile_user_payment_request_ds_3','DS_ANDR_PaymentController@view_request');

// DISTRIBUTOR PAYMENT ACCEPT ENTRY
Route::get('mobile_user_payment_accept_ds_1','DS_ANDR_PaymentController@accept_index');
Route::get('mobile_user_payment_accept_ds_2','DS_ANDR_PaymentController@store_accept');
Route::get('mobile_user_payment_accept_ds_3','DS_ANDR_PaymentController@store_delete');

// DISTRIBUTOR RECHARGE REPORT
Route::get('mobile_user_recharge_ds_1','DS_ANDR_RechReportController@index');
Route::get('mobile_user_recharge_ds_2','DS_ANDR_RechReportController@view');
Route::get('mobile_user_recharge_ds_3','DS_ANDR_RechReportController@view_date');

// DISTRIBUTOR COMPLAINT ENTRY
Route::get('mobile_user_complaint_ds_1','DS_ComplaintController@index_android');
Route::get('mobile_user_complaint_ds_2','DS_ComplaintController@store_android');
Route::get('mobile_user_complaint_ds_3','DS_ComplaintController@view_android');

// DISTRIBUTOR PROFILE
Route::get('mobile_user_profile_ds_1','DS_ANDR_ProfileController@index');
Route::get('mobile_user_profile_ds_2','DS_ANDR_ProfileController@percentage');

// DISTRIBUTOR STOCK
Route::get('mobile_user_stock_ds_1','DS_ANDR_StockController@index');
Route::get('mobile_user_stock_ds_2','DS_ANDR_StockController@view_date');

// DISTRIBUTOR CHANGE PASSWORD
Route::get('mobile_user_password_ds_1','DS_ANDR_ProfileController@changePassword');
Route::post('mobile_user_password_ds_2','DS_ANDR_ProfileController@changePasswordStore');

// DISTRIBUTOR AGENT ALLOCATION
Route::get('mobile_user_agent_allocation_ds_1','DS_ANDR_Agent_AreaController@index');
Route::get('mobile_user_agent_allocation_ds_2','DS_ANDR_Agent_AreaController@store');
Route::get('mobile_user_agent_allocation_ds_3','DS_ANDR_Agent_AreaController@view');

// DISTRIBUTOR AGENT ALLOCATION
Route::get('mobile_user_collection_verify_ds_1','DS_ANDR_Agent_CverifyController@index');
Route::get('mobile_user_collection_verify_ds_2','DS_ANDR_Agent_CverifyController@view');
Route::get('mobile_user_collection_verify_ds_3','DS_ANDR_Agent_CverifyController@successupdate');
Route::get('mobile_user_collection_verify_ds_4','DS_ANDR_Agent_CverifyController@failureupdate');

// DISTRIBUTOR COLLECTION PENDING
Route::get('mobile_user_collection_pending_ds_1','DS_ANDR_Agent_CpendingController@index');
Route::get('mobile_user_collection_pending_ds_2','DS_ANDR_Agent_CpendingController@view');

// DISTRIBUTOR COLLECTION PENDING
Route::get('mobile_user_collection_dashboard_ds','DS_ANDR_Agent_AreaController@dashboard');

// DISTRIBUTOR DASHBOARD
Route::get('mobile_distributor_dashboard','DS_ANDR_DashboardController@index');
Route::get('mobile_chart_data_ds','DS_ANDR_DashboardController@getRechargeGraphData');

// AGENT COllECTION
Route::get('mobile_user_collection_ag_1','AG_ANDR_CollectionController@index');
Route::post('mobile_user_collection_ag_2','AG_ANDR_CollectionController@store');
Route::get('mobile_user_collection_ag_3','AG_ANDR_CollectionController@getUserPaidAmount');
Route::get('mobile_user_collection_ag_4','AG_ANDR_CollectionController@getReport');

// AGENT COllECTION REPORT
Route::get('mobile_user_collection_ag_rp_1','AG_ANDR_CollectionController@report_1');
Route::get('mobile_user_collection_ag_rp_2','AG_ANDR_CollectionController@report_2');

// AGENT PROFILE
Route::get('mobile_user_agent_pr_1','AG_ANDR_UserController@index');
Route::get('mobile_agent_dashboard','AG_ANDR_UserController@dashboard');
Route::get('test-cybor','TestController@testCybor');

Route::get('test-route','TestController@sampleTest');


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});