<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('apirecharge/{data?}','ApipartnerController@store');
Route::get('apirechargeinfo/{data?}','ApipartnerInterfaceController@store');
Route::get('response/{data?}','NewApiResponseController@view');
Route::get('apirecharge/{data?}','NewApiController@insertRecharge');

/*
|--------------------------------------------------------------------------
| Mobile Api
|--------------------------------------------------------------------------
|
*/

Route::get('check','MobileApiController@logcheck');
Route::post('mobile_login','MobileApiController@login_check');
Route::post('mobile_otp','MobileApiController@login_otp_check');
Route::post('mobile_user_balance','MobileApiController@getUserBalance');
Route::post('mobile_get_prepaid','MobileApiController@getPrepaidDetails');
Route::post('mobile_get_postpaid','MobileApiController@getPostpaidDetails');
Route::post('mobile_get_dth','MobileApiController@getDthDetails');
Route::post('mobile_get_bill','MobileApiController@getBillDetails');
Route::post('mobile_get_network','MobileApiController@getNetworkDetails');
Route::post('mobile_prepaid_recharge','MobileApiController@mobile_prepaid_recharge');
Route::post('mobile_ebbil_recharge','MobileApiController@mobile_ebbil_recharge');
Route::post('mobile_prepaid_recharge_amount','MobileApiController@mobile_prepaid_recharge_amount');
Route::post('mobile_recharge_details','MobileApiController@getRetailerRechargeDetails_new');
Route::post('mobile_get_offer','MobileApiController@getOfferDetails');
Route::post('mobile_get_complaint','MobileApiController@getComplaintDetails');
Route::post('mobile_retailer_stock','MobileApiController@getRetailerStock');
Route::post('mobile_apipartner_stock','MobileApiController@getApipartnerStock');
Route::post('mobile_distributor_stock','MobileApiController@getDistributorStock');
Route::post('mobile_recharge_details_date','MobileApiController@getRetailerRechargeDetails1_new');
Route::post('mobile_payment_request','MobileApiController@getPaymentRequest');
Route::post('mobile_complaint_request','MobileApiController@getComplaintRequest');
Route::post('mobile_user_entry_1','MobileApiController@userEntry_1');
Route::post('mobile_user_entry_2','MobileApiController@userEntry_2');
Route::post('mobile_user_entry_3','MobileApiController@userEntry_3');
Route::post('mobile_user_details','MobileApiController@getUserDetails');
Route::post('mobile_payment_details','MobileApiController@getPaymentAcceptDetails');
Route::post('mobile_payment_accept','MobileApiController@storePaymentAccept');
Route::post('mobile_payment_delete','MobileApiController@storePaymentDelete');
Route::post('mobile_user_profile','MobileApiController@getProfileDetails');
Route::post('mobile_apirecharge_details','MobileApiController@getApipartnerRechargeDetails');
Route::post('mobile_apirecharge_details_date','MobileApiController@getApipartnerRechargeDetails1');
Route::post('mobile_remote_payment','MobileApiController@storeRemotePayment');
Route::post('mobile_change_pin','MobileApiController@getChangePin');
Route::post('mobile_121_offer','MobileApiController@mobile_121_offer');
Route::post('mobile_dth_info','MobileApiController@mobile_dth_info');
Route::post('mobile_eb_info','MobileApiController@mobile_eb_info');


Route::post('mobile_sms_recharge','SmsRechargeController@sms_recharge');

/*
|--------------------------------------------------------------------------
| Api Partner Api
|--------------------------------------------------------------------------
|
*/

// API - Remitter Check
Route::get('api_rem_check','AP_API_RemitterController@remitter_check');
Route::get('api_rem_register','AP_API_RemitterController@remitter_entry');

// API - Beneficiary Entry
Route::get('api_ben_entry','AP_API_BeneficiaryController@ben_entry');
Route::get('api_ben_otp_entry','AP_API_BeneficiaryController@ben_otp_entry');
Route::get('api_ben_verify','AP_API_BeneficiaryController@ben_get_account');

// API - Money Transfer
Route::get('api_money_transfer','AP_API_MoneyTransferController@transfer');

// API - Beneficiary Delete
Route::get('api_ben_delete','AP_API_BeneficiaryDeleteController@ben_delete_entry');
Route::get('api_ben_delete_otp','AP_API_BeneficiaryDeleteController@ben_delete_otp_entry');

//Operator check
Route::get('mobile_operator_check','AP_API_OfferController@operator_check');
Route::get('mobile_plan_check','AP_API_OfferController@plan_check');
Route::get('mobile_eb_check','AP_API_OfferController@eb_check');
Route::get('mobile_offer_check','AP_API_OfferController@offer_check');
Route::get('mobile_dth_check','AP_API_OfferController@dth_check');

//New plans
Route::get('plan/web/dth/operator','Offers\PlanWebController@dthOperatorCheck');
Route::get('plan/web/dth/customercheck','Offers\PlanWebController@dthInfo');
Route::get('plan/web/dth/plans','Offers\PlanWebController@dthPlans');
Route::get('plan/web/dth/offer','Offers\PlanWebController@dthOffers');
Route::get('plan/web/dth/refresh','Offers\PlanWebController@dthRefresh');

Route::prefix('cyber/eb')->group(function () {

    Route::get('check','cyberplat\EbController@index');
    Route::get('pay','cyberplat\EbController@payment');

});

Route::prefix('cyber/money/remitter')->group(function () {

    Route::get('check','cyberplat\BankRemitterVerifyController@remitterCheck');
    Route::get('add','cyberplat\BankRemitterAddController@remitterAdd');
    Route::get('otp','cyberplat\BankRemitterAddController@remitterOtpCheck');

});

Route::prefix('cyber/money/beneficiary')->group(function () {
    Route::get('add','cyberplat\BankBeneficiaryAddController@benAdd');
    Route::get('verify','cyberplat\BankBeneficiaryVerifyController@benCheck');
    Route::get('delete','cyberplat\BankBeneficiaryDeleteController@benDelete');
    Route::get('delete-otp','cyberplat\BankBeneficiaryDeleteOtpController@benDelete');
});

Route::prefix('cyber/money/fund')->group(function () {
    Route::get('transfer','cyberplat\BankTransferController@fundTransfer');
});

Route::prefix('cyber/upi')->group(function () {
    
    Route::get('pay','cyberplat\UpiPaymentController@generatePayment');

});

Route::get('test-data','cyberplat\BankRemitterVerifyController@getResult');
Route::get('bank-data','cyberplat\BankNameController@getBank');

Route::get('test-mrobo','TestController@checkMROBO');
