<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//NEW MONEY TRANSFER-CYBERPLAT
Route::get('money_user_check','money\RemitterController@registerRemiiter')->middleware("validretailer");
Route::post('bank_remitter_check','money\RemitterController@registerRemitterCheck')->middleware("validretailer");

//USER BANK REMITTER DETAILS
Route::get('bank_remitter_entry','money\RemitterController@registerRemiiterAdd')->middleware("validretailer");
Route::post('bank_remitter_entry_store','money\RemitterController@registerRemiiterStore')->middleware("validretailer");

//USER BANK REMITTER OTP DETAILS
Route::get('bank_remitter_otp_check','money\RemitterController@registerRemiiterOtp')->middleware("validretailer");
Route::post('bank_remitter_otp_store','money\RemitterController@registerRemiiterOtpStore')->middleware("validretailer");

//DASHBOARD
Route::get('money-dashboard','money\TransferController@indexTransfer')->middleware("validretailer");
Route::post('money-transfer','money\TransferController@store')->middleware("validretailer");

//BENEFICIARY
Route::get('beneficiary-entry','money\BeneficiaryController@registerBeneficiary')->middleware("validretailer");
Route::post('beneficiary-store','money\BeneficiaryController@storeBeneficiary')->middleware("validretailer");
Route::get('beneficiary-delete/{remId}/{mobileNo}/{benId}','money\BeneficiaryController@deleteBeneficiary')->middleware("validretailer");
Route::get('beneficiary-delete-otp/{remId}/{mobileNo}/{benId}/{otc}','money\BeneficiaryController@deleteBeneficiaryOtp')->middleware("validretailer");
Route::get('beneficiary-check','money\BeneficiaryController@checkBeneficiary');
Route::get('get-bank-ifsc/{bankCode}','money\BeneficiaryController@getBankIFSC');
Route::get('get-bank-name/{bankLetter}','money\BeneficiaryController@getBankName');