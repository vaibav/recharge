<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ADMIN DASHBOARD
Route::get('dashboard','Admin\DashboardController@index')->middleware("validadmin");
Route::get('adminresult_data','Admin\DashboardController@getIndex');
Route::get('adminrech_data','Admin\DashboardController@rech_data');
Route::get('adminchart_data','Admin\DashboardController@chart_data');

// SERVER ON-OFF
Route::get('serveronoff','Admin\ServerController@index')->middleware("validadmin");
Route::post('serveronoff_store','Admin\ServerController@store')->middleware("validadmin");
