<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//NEW MONEY TRANSFER-CYBERPLAT
Route::get('recharge-report-all-one','Retailer\ReportController@viewOne')->middleware("validretailer");
Route::get('recharge-report-all-two','Retailer\ReportController@viewTwo')->middleware("validretailer");