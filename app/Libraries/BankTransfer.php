<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;
use App\Libraries\BankTransferInfo;
use App\Libraries\BankRemitter;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\UserBankTransferDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\ServerDetails;

class BankTransfer
{

    /**
     * Add BankTransfer Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_url = "NONE";
        $b_url = "";
        $res_content = "NONE";
        $msg = "NONE";
        $otp_status ="0";
        $netamt = "0";
        $rep_trans_id = "";
        $remarks ="";
        $resx = new stdClass();

        // Modals
        $rech_5 = new UserBankTransferDetails;
       
        // Post Data
        $r_mode = $data['r_mode'];

        
        $trans_id = $data['client_trans_id'];
        $net_code = "0";
        $bn_id = $data['ben_id'];
        $rem_id = $data['msisdn'];
        $bk_acc_no = $data['beneficiary_account_no'];
        $bk_code = $data['beneficiary_bank_code'];
        $mt_amount = $data['amount'];
        $bk_mode = $data['transfer_type'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");
        $rech_type = "BANK_TRANSFER";
        $tran_api = "TRN_API_1";
        $tran_op = "1";

        if(floatval($mt_amount) <= 5000)
        {
            $net_code = self::getNetworkCode("MONEY TRANSFER");
            $tran_api = "TRN_API_1";
            $tran_op = "1";
        }
        else if(floatval($mt_amount) > 5000)
        {
            $net_code = self::getNetworkCode("MONEY TRANSFER 1");
            $tran_api = "TRN_API_1_1";
            $tran_op = "2";
        }


        // 1.Check User Balance
        if(floatval($mt_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 3;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 4;     // Account Balance is low from Setup Fee...
        }


        // Check Mobile Status
        /*$zy = self::checkAccountNoStatus($user_name, $bn_id);
        if ($zy == 6)
        {
            $z = 5;     // Recharge Status is already Pending for this Account No
        }*/

        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine($tran_api);

            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);
 
                if($api_url != "NONE")
                     $res_code = 200;

                // Insertion Process..
                list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $mt_amount, $r_mode);

                // Array
                $rech = [];
                $rech['trans_id'] = $trans_id;
                $rech['api_trans_id'] = $api_trans_id;
                $rech['user_name'] = $user_name;
                $rech['net_code'] = $net_code;
                $rech['bn_id'] = $bn_id;
                $rech['rem_id'] = $rem_id;
                $rech['bk_acc_no'] = $bk_acc_no;
                $rech['bk_code'] = $bk_code;
                $rech['bk_trans_type'] = $bk_mode;
                $rech['mt_amount'] = $mt_amount;
                $rech['mt_per'] = $per;
                $rech['mt_per_amt'] = PercentageCalculation::convertNumberFormat($peramt);
                $rech['mt_surp'] = PercentageCalculation::convertNumberFormat($sur);
                $rech['mt_total'] = PercentageCalculation::convertNumberFormat($netamt);
                $rech['user_balance'] = 0;
                $rech['mt_date'] = $date_time;
                $rech['mt_req_status'] = "done..";
                $rech['mt_status'] = "PENDING";
                $rech['mt_mode'] = $r_mode;
                $rech['mt_option'] = "1";
                $rech['api_code'] = $api_code;
                $rech['mt_reply_trans_id'] = "0";
                $rech['mt_reply_id'] = "";
                $rech['mt_reply_date'] = $date_time;
                $rech['created_at'] = $date_time;
                $rech['updated_at'] = $date_time;

                $zx = self::insertRechargePayment($rech);
                if($zx == 1)
                {

                  // Parent Network Percentage Calculation
                    list($parent_type, $parent_name) = self::getParent($user_name);
                    list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $mt_amount);

                    $rech_p2 = [];
                    $rech_p2['trans_id'] = $trans_id;
                    $rech_p2['api_trans_id'] = $api_trans_id;
                    $rech_p2['parent_name'] = $parent_name;
                    $rech_p2['child_name'] = "NONE";
                    $rech_p2['user_name'] = $user_name;
                    $rech_p2['net_code'] = $net_code;
                    $rech_p2['rech_mobile'] = $bk_acc_no;
                    $rech_p2['rech_amount'] = $mt_amount;
                    $rech_p2['rech_net_per'] = $per_d;
                    $rech_p2['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_d);
                    $rech_p2['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                    $rech_p2['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                    $rech_p2['user_balance'] = 0;
                    $rech_p2['rech_date'] = $date_time;
                    $rech_p2['rech_status'] = "PENDING";
                    $rech_p2['rech_type'] = "BANK_TRANSFER";
                    $rech_p2['rech_mode'] = $r_mode;
                    $rech_p2['rech_option'] = "0";
                    $rech_p2['created_at'] = $date_time;
                    $rech_p2['updated_at'] = $date_time;

                
                    $zp2 = self::insertRechargePaymentDistributor($rech_p2, $rech_p2['parent_name'], $r_mode);
                    $zp22 = self::insertTransactionParent($trans_id, $rech_p2['parent_name'], $rech_type);


                    // Super Parent Network Percentage Calculation
                    list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                    if($parent_name_s != "NONE")
                    {
                        list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $mt_amount);
                    
                        // Super Distributor Payment Details
                        $rech_p3 = [];
                        $rech_p3['trans_id'] = $trans_id;
                        $rech_p3['api_trans_id'] = $api_trans_id;
                        $rech_p3['parent_name'] = $parent_name_s;
                        $rech_p3['child_name'] = $parent_name;
                        $rech_p3['user_name'] = $user_name;
                        $rech_p3['net_code'] = $net_code;
                        $rech_p3['rech_mobile'] = $bk_acc_no;
                        $rech_p3['rech_amount'] = $mt_amount;
                        $rech_p3['rech_net_per'] = $per_s;
                        $rech_p3['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_s);
                        $rech_p3['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                        $rech_p3['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                        $rech_p3['user_balance'] = 0;
                        $rech_p3['rech_date'] = $date_time;
                        $rech_p3['rech_status'] = "PENDING";
                        $rech_p3['rech_type'] = "BANK_TRANSFER";
                        $rech_p3['rech_mode'] = $r_mode;
                        $rech_p3['rech_option'] = "0";
                        $rech_p3['created_at'] = $date_time;
                        $rech_p3['updated_at'] = $date_time;
                        
    
                                        
                        $zp3 = self::insertRechargePaymentDistributor($rech_p3, $rech_p3['parent_name'], $r_mode);
                        $zp33 = self::insertTransactionParent($trans_id, $rech_p3['parent_name'], $rech_type);
    
                    }

                    $zt = self::insertTransaction($trans_id, $user_name, $rech_type);
                    
                    if ($res_code == 200)
                    {
                        $res_content = BankRemitter::runURL($api_url, "TRANSFER");

                        if($tran_op == "1") {
                            list($status, $msg, $otp_status, $rep_trans_id, $remarks) = BankTransferReply::storeTransferupdate_1($res_content, $trans_id);
                        }
                        else if($tran_op == "2") {
                            list($status, $msg, $otp_status, $rep_trans_id, $remarks) = BankTransferReply::storeTransferupdate_1_1($res_content, $trans_id);
                        }
                        
                        

                        if($status == "FAILURE")
                        {
                            $z = 4;
                            //self::refundBalance($user_name, $netamt);
                        }
                        else  if($status == "PENDING")
                        {
                            $z = 5;
                        }

                    }

                    // All Process Over...
                }

            }

            

            

        }

        return array($z, $msg, $otp_status, $rep_trans_id, $remarks, $res_content);
        
    }

    /**
     * Add Money Transfer OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add_otp($data)
    {
        $z = 0;
        $message = "";
        
       
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $trans_id = $data['u_trans_id'];
        $agent_id = $data['agent_id'];
        $msisdn = $data['msisdn'];
        $rep_trans_id = $data['trans_id'];


        // Check Username status
        /*$zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }*/

        file_put_contents(base_path().'/public/sample/bank_api_otp.txt', print_r($data, true), FILE_APPEND);

        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("TRN_API_2");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    $res_content = BankRemitter::runURL($api_url, "TRANSFER_OTP");

                    list($status, $trans_status, $message) = BankReply::storeTransferupdate_2($res_content, $trans_id);
                    

                    if($status == "FAILURE")
                    {
                        $z = 5;
                        $mt_amt = self::getTransferAmount($trans_id);
                        self::refundBalance($user_name, $mt_amt);
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message, $trans_status);
    }


    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    
    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $bn_id)
    {
        $rech_1 = new UserBankTransferDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['bn_id', '=', $bn_id], 
                                ['mt_status', '=', 'PENDING'], ['mt_option', '=', '1']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }


    /**
     * Insertion Process..............................................
     *
     */
    public static function insertRechargePayment($rech_p1)
    {
        $rech_3 = new UserBankTransferDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1['user_name'])->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['mt_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1['user_name'])
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;
            
            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    // ---- New Functions 
    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }

    public static function getTransferAmount($trans_id)
    {
        $rech_3 = new UserBankTransferDetails;
    
        $mt_tot = 0;
       

        $d1 = $rech_3->select('mt_total')->where('trans_id', '=', $trans_id)->get();
        if($d1->count() > 0)
        {
            $mt_tot = $d1[0]->mt_total;
        }

        return $mt_tot;
    }


    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public static function insertRechargePaymentDistributor($rech_p1, $user_name, $r_mode)
    {
        $rech_3 = new UserRechargeNewParentDetails;
        $uacc_1 = new UserAccountBalanceDetails;

        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;

            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function insertTransaction($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public static function insertTransactionParent($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public static function getNetworkCode($net_name)
    {
        $net_1 = new NetworkDetails;

        $net_code = "0";

        $d1 = $net_1->select('net_code')->where('net_name', '=', $net_name)->get();

        if($d1->count() > 0)
        {
            $net_code = $d1[0]->net_code;
        }

        return $net_code;
    }

    

}
