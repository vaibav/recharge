<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\ApiProviderResultDetails;

class GetAPIResult
{
       
    /**
     * Check URL Result Functions..............................................
     *
     */
    public static function getAPIResultParameters($data, $api_code)
    {
        $dx = [];
        $dx = self::getAPIResultData($api_code);
        $s_status = "PENDING";
        if (!empty($dx)) 
        {
            foreach($dx as $d)
            {
                if($d[0] == "STATUS 1")
                {
                    $field = $d[1];
                    $field_result = $d[2];

                    $field = str_replace('a', '',$field);

                    $r_result = $data[intval($field)];          // Get Status Result From  Server Data

                    if(strtolower($field_result) == strtolower($r_result))
                    {
                        $s_status = "SUCCESS";
                    }
                    if(strpos($r_result, $field_result) !== false) 
                    {
                        $s_status = "SUCCESS";
                    }

                    file_put_contents(base_path()."/public/sample/re_success.txt", $s_status."--".$field_result."--".$r_result.PHP_EOL, FILE_APPEND);

                }
                if($d[0] == "STATUS 2")
                {
                    $field = $d[1];
                    $field_result = $d[2];

                    $field = str_replace('a', '',$field);

                    $r_result = $data[intval($field)];          // Get Status Result From  Server Data this

                    if(strtolower($field_result) == strtolower($r_result))
                    {
                        $s_status = "FAILURE";
                    }
                    if(strpos($r_result, $field_result) !== false) 
                    {
                        $s_status = "FAILURE";
                    }

                    file_put_contents(base_path()."/public/sample/re_faild.txt", $s_status."--".$field_result."--".$r_result.PHP_EOL, FILE_APPEND);

                }
                if($d[0] == "MY_TRANSACTION_ID")
                {
                    $field = $d[1];
                    
                    $field = str_replace('a', '',$field);

                    $s_trans_id = $data[intval($field)];          // Get Transaction Id From  Server Data

                }
                if($d[0] == "OPERATOR_ID")
                {
                    $field = $d[1];
                    
                    $field = str_replace('a', '',$field);

                    $s_opr_id = $data[intval($field)];          // Get Operator ID From  Server Data

                }
            }
            
            //echo "<br>Server Status :".$s_status."<br>";
            //echo "<br>Server Transaction Id :".$s_trans_id."<br>";
            //echo "<br>Server Operator Id :".$s_opr_id."<br>";
        }

        return array($s_status, $s_trans_id, $s_opr_id);
    }

    public static function getAPIResultData($api_code)
    {
        $api_4 = new ApiProviderResultDetails;

        $dx = [];
        
        $j = 0;
        if($api_code != "0")
        {
            $d1 = $api_4->select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')->where('api_code', '=', $api_code)->get();
            if($d1->count() > 0)
            {
                foreach($d1 as $d)
                {
                    $dx[$j][0] = $d->api_res_para_name;
                    $dx[$j][1] = $d->api_res_para_field;
                    $dx[$j][2] = $d->api_res_para_value;
                    $j++;
                }
            }
        }

        return $dx;
    }

    

}
