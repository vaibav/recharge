<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DB;

use App\Libraries\BankTransferInfo;
use App\Libraries\InstantReplyUpdate;

use App\Models\UserBankAgentDetails;
use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;
use App\Models\UserBankTransferDetails;
use App\Models\ApiProviderResultDetails;


class BankReply
{

    public static function storeAgentupdate_1($res_content, $agent_id)
    {
        $agen_1 = new UserBankAgentDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "AGENT_API_1");

        $trans_id = "0";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
        }

        // Updation
        if($s_status == "FAILURE")
        {
            $agen_1->where('agent_id', '=', $agent_id)
                                ->update(['agent_status' => "".$s_status, 
                                        'agent_trans_id' => $trans_id, 
                                        'agent_reply_id' => $message]);
        }
        else if($s_status == "PENDING")
        {
            $agen_1->where('agent_id', '=', $agent_id)
                                ->update(['agent_status' => "".$s_status, 
                                        'agent_trans_id' => $trans_id, 
                                        'agent_reply_id' => $message]);
        }
        else if($s_status == "SUCCESS")
        {
            $agen_1->where('agent_id', '=', $agent_id)
                                ->update(['agent_status' => "OTP ".$s_status, 
                                        'agent_trans_id' => $trans_id, 
                                        'agent_reply_id' => $s_opr_id]);
        }
        
        
        return $s_status;
        
    }


    public static function storeAgentupdate_2($res_content, $agent_msisdn)
    {
        $agen_1 = new UserBankAgentDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "AGENT_API_2");

        $trans_id = "0";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
        }

        // Updation
        if($s_status == "FAILURE")
        {
            $agen_1->where('agent_msisdn', '=', $agent_msisdn)
                                ->update(['agent_reply_id' => $message]);
        }
        else if($s_status == "PENDING")
        {
            $agen_1->where('agent_msisdn', '=', $agent_msisdn)
                                ->update(['agent_reply_id' => $message]); 
        }
        else if($s_status == "SUCCESS")
        {
            $agen_1->where('agent_msisdn', '=', $agent_msisdn)
                                ->update(['agent_status' => $s_status, 
                                        'agent_trans_id' => $trans_id, 
                                        'agent_reply_id' => $s_opr_id]);
        }
        
        
        return $s_status;
        
    }

    public static function storeAgentupdate_3($res_content, $agent_msisdn)
    {
        $agen_1 = new UserBankAgentDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "AGENT_API_3");

        $trans_id = "0";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
        }

        if($s_status == "SUCCESS")
        {
            $agen_1->where('agent_msisdn', '=', $agent_msisdn)
                                ->update(['agent_trans_id' => $trans_id]);
        }
        
        
        return array ($s_status, $message);
        
    }


    public static function storeRemitterupdate_1($res_content, $rem_id)
    {
        $rem_1 = new UserBankRemitterDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "REM_API_1");

        
        // Updation
        $rem_1->where('rem_id', '=', $rem_id)
                    ->update(['rem_status' => "".$s_status, 
                                'rem_rep_opr_id' => $s_opr_id]);
        
        
        return array($s_status, $s_opr_id);
        
    }

    public static function storeRemitterupdate_2($res_content)
    {
        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "REM_API_2");

        return array($s_status, $s_opr_id);
        
    }

    public static function storeBeneficiaryupdate_1($res_content, $ben_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_API_1");

        $trans_id = "0";
        $message = "NONE";
        $date_time = date("Y-m-d H:i:s");

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
        }

        // Updation
        if($s_status == "FAILURE")
        {
            /*$ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_status' => "".$s_status, 
                                        'trans_id' => $trans_id, 
                                        'ben_rep_opr_id' => $message, 'updated_at' => $date_time]);*/
            DB::statement("UPDATE user_beneficiary_details SET ben_status = '".$s_status."', 
                                        trans_id = '".$trans_id."',
                                        ben_rep_opr_id ='".$message."' WHERE ben_id = '".$ben_id."'");
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE user_beneficiary_details SET ben_status = '".$s_status."', 
                                        trans_id = '".$trans_id."',
                                        ben_rep_opr_id ='".$message."' WHERE ben_id = '".$ben_id."'");
        }
        else if($s_status == "SUCCESS")
        {
            /*$ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_status' => "SUCCESS", 
                                            'trans_id' => "123456", 
                                            'ben_rep_opr_id' => "Success", 'updated_at' => "'".$date_time."'"]);*/
                                            
            DB::statement("UPDATE user_beneficiary_details SET ben_status = 'OTP ".$s_status."', 
                                                        trans_id = '".$trans_id."',
                                                        ben_rep_opr_id ='".$s_opr_id."' WHERE ben_id = '".$ben_id."'");
        }
        
        
        return array($s_status, $message, $trans_id);
        
    }

    public static function storeBeneficiaryupdate_2($res_content, $ben_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_API_2");

        $ben_id_1 = "0";
        $message = "NONE";
        $remarks = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "beneficiary_id")
            {
                $ben_id_1 = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            $ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_rep_opr_id' => $message]);
            $s_opr_id = $message;
        }
        else if($s_status == "PENDING")
        {
            $ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_rep_opr_id' => $message]); 
            $s_opr_id = $message;
        }
        else if($s_status == "SUCCESS")
        {
            DB::statement("UPDATE user_beneficiary_details SET ben_status = '".$s_status."', 
                                                        ben_id = '".$ben_id_1."',
                                                        ben_rep_opr_id ='".$remarks."' WHERE ben_id = '".$ben_id."'");
        }
        
        return array($s_status, $remarks);
        
    }

    public static function storeBeneficiaryupdate_3($res_content, $trans_id)
    {
        $ben_1 = new UserBeneficiaryAccVerifyDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_VERIFY_1");

        $ben_name = "NONE";
        

        // get Resultant data
        if($s_status == "SUCCESS")
        {
            foreach($data as $d)
            {
                if($d[0] == "beneficiary_name")
                {
                    $ben_name = $d[1];
                }
            }

            DB::statement("UPDATE user_beneficiary_acc_verify_details SET ben_status = '".$s_status."' WHERE trans_id = '".$trans_id."'");
        }
        else  if($s_status == "FAILURE")
        {
            DB::statement("UPDATE user_beneficiary_acc_verify_details SET ben_status = '".$s_status."' WHERE trans_id = '".$trans_id."'");
        }
        
        return array($s_status, $s_opr_id, $ben_name);
        
    }


    public static function storeBeneficiaryupdate_4($res_content, $ben_id)
    {
        $ben_1 = new UserBeneficiaryDetails;
        
        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_API_3");

        $trans_id = "0";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
        }

        if($s_status == "SUCCESS")
        {
            $ben_1->where('ben_id', '=', $ben_id)->update(['trans_id' => $trans_id]);
        }
        
        
        return array ($s_status, $message, $trans_id);
        
    }


    public static function storeBeneficiaryupdate_5($res_content, $ben_id)
    {
        $ben_1 = new UserBeneficiaryDetails;
        
        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_D_API_1");

        $trans_id = "0";
        $message = "NONE";
        $remarks = "NONE";
        $otp_status = "0";

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
        }

        file_put_contents(base_path().'/public/sample/ben_2.txt', $trans_id."--".$message."--".$remarks."--".$otp_status."--".$s_status."--".$ben_id, FILE_APPEND);

        // Updation
        if($s_status == "SUCCESS")
        {
            /*$ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_status' => "SUCCESS", 
                                            'trans_id' => "123456", 
                                            'ben_rep_opr_id' => "Success", 'updated_at' => "'".$date_time."'"]);*/
                                            
            DB::statement("UPDATE user_beneficiary_details SET ben_status = 'OTP FAILED', 
                                                        trans_id = '".$trans_id."',
                                                        ben_rep_opr_id ='".$remarks."' WHERE ben_id = '".$ben_id."'");
        }

        return array ($s_status, $message, $trans_id, $remarks, $otp_status);
        
    }

    public static function storeBeneficiaryupdate_6($res_content, $ben_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_D_API_2");

        $ben_id_1 = "0";
        $message = "NONE";
        $remarks = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "beneficiary_id")
            {
                $ben_id_1 = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
        }

        

        
        
        return array($s_status, $remarks."-".$message);
        
    }

    public static function storeTransferupdate_1($res_content, $trans_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $date_time = date("Y-m-d H:i:s");

        //$trans_s = [];
        //$trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            
        }

        //$bank_trans_id = self::getSubResultData($trans_s);

        // Updation
        if($s_status == "FAILURE")
        {
            /*$ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_status' => "".$s_status, 
                                        'trans_id' => $trans_id, 
                                        'ben_rep_opr_id' => $message, 'updated_at' => $date_time]);*/
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = 'OTP ".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$remarks."' WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$bank_trans_id."' WHERE trans_id = '".$trans_id."'");
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

    public static function storeTransferupdate_2($res_content, $trans_id)
    {
        $ban_1 = new UserBankTransferDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $s_status, 'mt_reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $trans_status, 'mt_reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }


    // Transfer greater than 5000
    public static function storeTransferupdate_1_1($res_content, $trans_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $success_amount = "0";
        $date_time = date("Y-m-d H:i:s");

        $trans_s = [];
        $trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "success_amount")
            {
                $success_amount = $d[1];
            }
            if($d[0] == "transfer_data")
            {
                $trans_s= $d[1];
            }
            if($d[0] == "failed_data")
            {
                $failed_data= $d[1];
            }
            
        }

        

        $bank_trans_id = self::getSubResultData($trans_s);

        $bank_trans_id = $success_amount." :".$bank_trans_id;

        // Updation
        if($s_status == "FAILURE")
        {
            /*$ben_1->where('ben_id', '=', $ben_id)
                                ->update(['ben_status' => "".$s_status, 
                                        'trans_id' => $trans_id, 
                                        'ben_rep_opr_id' => $message, 'updated_at' => $date_time]);*/
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = 'OTP ".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$remarks."' WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$bank_trans_id."' WHERE trans_id = '".$trans_id."'");
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

    public static function storeTransferupdate_2_2($res_content, $trans_id)
    {
        $ban_1 = new UserBankTransferDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $s_status, 'mt_reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $trans_status, 'mt_reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }


    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }


    public static function getBankResult($result, $api_line)
    {
        $dx = [];
        $s_status = "PENDING";
        $s_opr_id = "NONE";
        $data = [];

        list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine($api_line);
        if($api_code > 0)
        {
            $dx = self::getAPIResultData($api_code);

            if(!empty($dx))
            {
                $j = 0;
                foreach($dx as $d)
                {
                    if($d[0] == "STATUS 1")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "SUCCESS";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "STATUS 2")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "FAILURE";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "OPERATOR_ID")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $s_opr_id = $value;
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "DATA")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                    else if($d[0] == "ARRAY")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                }
            }
        }

         
        return array($s_status, $s_opr_id, $data);
    }

    public static function getAPIResultData($api_code)
    {
        $api_4 = new ApiProviderResultDetails;

        $dx = [];
        
        $j = 0;
        if($api_code != "0")
        {
            $d1 = $api_4->select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')->where('api_code', '=', $api_code)->get();
            if($d1->count() > 0)
            {
                foreach($d1 as $d)
                {
                    $dx[$j][0] = $d->api_res_para_name;
                    $dx[$j][1] = $d->api_res_para_field;
                    $dx[$j][2] = $d->api_res_para_value;
                    $j++;
                }
            }
        }

        return $dx;
    }


    public static function getSubResultData($res_content)
    {
        $bank_trans_id = "done..";

        //$result = self::resultFormatter($res_content);

        $bank_trans_id = "[";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new2.txt', print_r($res_content, true), FILE_APPEND);

        foreach($res_content as $d)
        {
            $t1 = "";
            $t2 = "";

            if (is_object($d)) {
                if(isset($d->bank_trans_id))
                {
                    $t1 = $d->bank_trans_id;
                }
                if(isset($d->amount))
                {
                    $t2 = $d->amount;
                }
            }
            else
            {
                if (array_key_exists('bank_trans_id', $d)) {
                    $t1 = $d['bank_trans_id'];
                }
                if (array_key_exists('amount', $d)) {
                    $t2 = $d['amount'];
                }
            }

            $bank_trans_id =  $bank_trans_id . '{"bank_trans_id":"'.$t1.'","amount":"'.$t2.'"}, ';
            
        }

        $bank_trans_id= rtrim($bank_trans_id,", ");

        $bank_trans_id = $bank_trans_id."]";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new1.txt', $bank_trans_id, FILE_APPEND);

        return $bank_trans_id;
    }

   
}
