<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankRemitter;
use App\Libraries\CyberBankRemitter;

use App\Models\UserBankMobileStatusDetails;

class BankMobileCommon
{
    //
    public static function fetchCustomerDetails($res_content)
    {
       
        $data = [];

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        $data['msisdn'] = "";
        $data['user_name'] = "";
        $data['total_limit'] = "";
        $data['consume_limit'] = "";
        $data['remaining_limit'] = "";
        $data['beneficiary'] = "";
        $beni = [];

        foreach($result as $key => $value)
        {
            if($key == "msisdn")
            {
                $data['msisdn'] = $value;
            }
            else if($key == "user_name")
            {
                $data['user_name'] = $value;
            }
            else if($key == "total_limit")
            {
                $data['total_limit'] = $value;
            }
            else if($key == "consume_limit")
            {
                $data['consume_limit'] = $value;
            }
            else if($key == "remaining_limit")
            {
                $data['remaining_limit'] = $value;
            }
            else if($key == "beneficiary")
            {
                $data['beneficiary'] = $value;
            }
        }
        
        $result1 = self::resultFormatter($data['beneficiary']);

        //$result1 = json_decode($result1, true);

        if(!empty($result1)) 
        {
            foreach($result1 as $d)
            {
                if(!empty($d)) 
                {
                    $b_id = "";
                    $b_name = "";
                    $b_accno = "";
                    $b_bank = "";
                    $b_bcode = "";
                    $b_ifsc = "";

                    foreach($d as $key => $value)
                    {
                        if($key == "beneficiary_id")
                        {
                            $b_id = $value;
                        }
                        else if($key == "beneficiary_name")
                        {
                            $b_name = $value;
                        }
                        else if($key == "beneficiary_account_no")
                        {
                            $b_accno = $value;
                        }
                        else if($key == "beneficiary_bank_name")
                        {
                            $b_bank = $value;
                        }
                        else if($key == "beneficiary_bank_code")
                        {
                            $b_bcode = $value;
                        }
                        else if($key == "beneficiary_ifsc_code")
                        {
                            $b_ifsc = $value;
                        }
                    }

                    if($b_id != "" && $b_accno != "")
                    {
                        array_push($beni, ['b_id' => $b_id, 'b_name' => $b_name, 'b_accno' => $b_accno, 'b_bank' => $b_bank, 'b_bcode' => $b_bcode, 'b_ifsc' => $b_ifsc]);
                    }
                }
               
            }
            
        }

         
        return array($data, $beni);
    }

    public static function user_validation($user_name, $msisdn)
    {
        $data = ['user_name_1' => $user_name,
                        'msisdn' => $msisdn,
                        'r_mode' => "WEB"
                    ];

        list($z, $op, $result, $remitter, $beneficiary, $isVerified) = CyberBankRemitter::userCheck($msisdn);

        return array($z, $op, $result, $remitter, $beneficiary, $isVerified);
    }

    public static function add_status($user_name, $msisdn, $status)
	{
        $umob_1 = new UserBankMobileStatusDetails;

        $d1 = $umob_1->where('user_name', '=', $user_name)->get();

        $z = 0;
        $date_time = date("Y-m-d H:i:s");

        if($d1->count() > 0) {
            // Already login
            $umob_1->where('user_name', '=', $user_name)->update(['msisdn' => $msisdn, 
                                        'screen_type' => $status, 'updated_at'=>$date_time]);
            $z = 1;
        }
        else {
            // Fresh Login
            $umob_1->trans_id = rand(10000,99999);
            $umob_1->user_name = $user_name;
            $umob_1->msisdn = $msisdn;
            $umob_1->screen_type = $status;

            $umob_1->save();
            $z = 1;
        }

        return $z;
    }

    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }

}
