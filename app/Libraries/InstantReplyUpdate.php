<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeNewDetails;
use App\Models\UserAccountDetails;

use App\Libraries\RechargeNewReply;
use App\Libraries\BankRemitter;

class InstantReplyUpdate
{
    //
    
     /**
     * Reply Update Functions..............................................
     *
     */
    public static function instantUpdate($res_content, $content, $url_1, $trans_id, $status)
    {
        file_put_contents(base_path().'/public/sample/req_1.txt', $res_content."---".$content.PHP_EOL, FILE_APPEND);

        if (strpos($res_content, $content) !== false) {
               
            file_put_contents(base_path().'/public/sample/req_1.txt', "---".$content.PHP_EOL, FILE_APPEND);

            $oprtr_id = self::getOperatorId($res_content);

            $oprtr_id = strip_tags($oprtr_id);

            if($oprtr_id == "NA" || $oprtr_id == "")
            {
                $oprtr_id = self::getJsonOperatorId($res_content);

                
            }

            if($oprtr_id == "")
                $oprtr_id = "na";
            
            $oprtr_id = self::resultFormatter($oprtr_id);

            $oprtr_id = self::convert_text($oprtr_id);
            
            if(strlen($oprtr_id) > 40)
            {
                $oprtr_id = substr($oprtr_id, 0 ,40);
            }
            
            
            if($status == "1")
            {
                self::successUpdate($trans_id, $oprtr_id);
            }
            else if($status == "2")
            {
                self::failureUpdate($trans_id, $oprtr_id);
            }
            else
            {
                $su_url = $url_1."/".$oprtr_id;

                file_put_contents("paytric_su.txt", $su_url);

                $client = new \GuzzleHttp\Client();
                $res = $client->get($su_url);
                $res_code = $res->getStatusCode();
                $res_content_1 =$res->getBody();

                file_put_contents("paytric_su1.txt", $res_content_1);
            }

        }

    }


    public static function getOperatorId($xml)
    {
        $oprtr_id = "NA";

        try
        {
            if (strpos($xml, '<') !== false) {
                $arrXml = array();
                $dom    = new \DOMDocument;
                $dom->loadXML( $xml );
                foreach( $dom->getElementsByTagName( 'OPID' ) as $item ) {
                    $arrXml[] = $dom->saveXML( $item );
                }
    
                if (sizeof($arrXml) > 0)
                    $oprtr_id = $arrXml[0];
            }
           
        }
        catch(Exception $excp)
        {
            return "NA";
        }

        return $oprtr_id;
    }

    public static function getJsonOperatorId($res_content)
    {
        $oprtr_id = "NA";
        $result1 = "";

        $c = self::json_validate($res_content);

        if($c == 1)
        {
            $result = self::resultFormatter($res_content);
            $result1 = $result;

            $result = (array)json_decode($result);

            file_put_contents(base_path().'/public/sample/req_2.txt', print_r($result,true), FILE_APPEND);

            foreach($result as $key => $value)
            {
                if($key == "tnx_id")
                {
                    $oprtr_id = $value;
                    break;
                }
                else if($key == "Txid")
                {
                    $oprtr_id = $value;
                    break;
                }
                else if($key == "transId")
                {
                    $oprtr_id = $value;
                    break;
                }
            }

            if($oprtr_id == "NA") {
                $oprtr_id = self::getAgainJsonOperatorId($result1, "tnx_id");
            }

            if($oprtr_id == "NA") {
                $oprtr_id = self::getAgainJsonOperatorId($result1, "Txid");
            }

            if($oprtr_id == "NA") {
                $oprtr_id = self::getAgainJsonOperatorId($result1, "transId");
            }
        }


        return $oprtr_id;
    }

    public static function getAgainJsonOperatorId($result, $con)
    {
        $oprtr_id = "NA";
       
        $result = ltrim($result, '{');
        $result = rtrim($result, '}');
        $result = str_replace("{", ",", $result);
        $a = explode(",", $result);
    
        foreach($a as $d)
        {
            $d1 = [];
            $d1 = explode(":", $d);
            if(sizeof($d1) >= 2)
            {
                $d1[0] = str_replace('"', '', $d1[0]);
                $d1[1] = str_replace('"', '', $d1[1]);
    
                if($d1[0] == $con) {
                    $oprtr_id = $d1[1];
                }
            }
        }
        
        return $oprtr_id;
    }

    public static function successUpdate($trans_id, $opr_id)
    {
        $rech_n_1 = new UserRechargeNewDetails;
       
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");

        $d1 = $rech_n_1->select('user_name', 'api_trans_id', 'rech_mode')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            $mode = $d1[0]->rech_mode;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            $zx = RechargeNewReply::updateSuccess($trans_id, $opr_id, "SUCCESS", $date_time);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_s_url, '<trid>') !== false) {
                        $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<oprid>') !== false) {
                        $api_s_url = str_replace("<oprid>", $opr_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<status>') !== false) {
                        $api_s_url = str_replace("<status>", "SUCCESS", $api_s_url);
                    }

                    if($api_s_url != "")
                    {
                        BankRemitter::runURL($api_s_url, "RESPONSE");
                    }
                }

                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }

    }

    public static function failureUpdate($trans_id, $opr_id)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");

        $d1 = $rech_n_1->select('user_name', 'api_trans_id', 'rech_mode')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            $mode = $d1[0]->rech_mode;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            $zx = RechargeNewReply::updateFailure($trans_id, $opr_id, "FAILURE", $date_time);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", "FAILURE", $api_f_url);
                    }

                    if($api_f_url != "")
                    {
                        BankRemitter::runURL($api_f_url, "RESPONSE");
                    }
                }

                $z = 0;
                $op = "Failure Update is executed successfully...";
            }

        }

    }

    public static function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }

    public static function convert_text($text) {

        $t = $text;
        
        $specChars = array(
            '!' => '%21',    '"' => '%22',
            '#' => '%23',    '$' => '%24',    '%' => '%25',
            '&' => '%26',    '\'' => '%27',   '(' => '%28',
            ')' => '%29',    '*' => '%2A',    '+' => '%2B',
            ',' => '%2C',    '-' => '%2D',    '.' => '%2E',
            '/' => '%2F',    ':' => '%3A',    ';' => '%3B',
            '<' => '%3C',    '=' => '%3D',    '>' => '%3E',
            '?' => '%3F',    '@' => '%40',    '[' => '%5B',
            '\\' => '%5C',   ']' => '%5D',    '^' => '%5E',
            '_' => '%5F',    '`' => '%60',    '{' => '%7B',
            '|' => '%7C',    '}' => '%7D',    '~' => '%7E',
            ',' => '%20',  ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }
    
    public static function json_validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);
    
        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }
    
        if ($error !== '') {
            // throw the Exception or exit // or whatever :)
            return 2;
        }
    
        // everything is OK
        return 1;
    }
}
