<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankTransferInfo;
use App\Libraries\BankReply;

use App\Models\UserBankAgentDetails;
use App\Models\ServerDetails;
use App\Models\UserAccountDetails;

class BankAgent
{

    /**
     * Add Agent Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        $agen_1 = new UserBankAgentDetails;

        $z = 0;
        
        $agent_id = rand(1000,9999);
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $data['agent_id'] = $agent_id;


        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 1;     // Server is Temporariy Shutdown
        }

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }

        $zy = self::checkAlreadyEntry($user_name);
        if($zy == 4)
        {
            $z = 4;     // Already Registered...
        }
        

        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("AGENT_API_1");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                file_put_contents("bank_api.txt", $api_url);

                if($api_url != "NONE")
                    $res_code = 200;

                
                // Insert Record... 
                $agen_1->agent_id = $agent_id;
                $agen_1->user_name = $data['user_name'];
                $agen_1->agent_msisdn = $data['agent_msisdn'];
                $agen_1->agent_name = $data['agent_name'];
                $agen_1->agent_cname = $data['agent_cname'];
                $agen_1->agent_address = $data['agent_address'];
                $agen_1->agent_city = $data['agent_city'];
                $agen_1->agent_state_code = $data['agent_state_code'];
                $agen_1->agent_pincode = $data['agent_pincode'];
                $agen_1->agent_status = "PENDING";
                $agen_1->api_code = $api_code;
                $agen_1->agent_trans_id = "0";
                $agen_1->agent_reply_id = "0";

                $agen_1->save();

                if ($res_code == 200)
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($api_url);
                    $res_code = $res->getStatusCode();
                    $res_content = (string) $res->getBody();

                    file_put_contents(base_path().'/public/sample/bank_reply.txt', $res_content.PHP_EOL, FILE_APPEND);

                    $status = BankReply::storeAgentupdate_1($res_content, $agent_id);

                    if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return $z;
    }


    /**
     * Add Agent OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add_otp($data)
    {
        $agen_1 = new UserBankAgentDetails;

        $z = 0;
        
        $agent_id = "";
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $agent_msisdn = $data['agent_msisdn'];
        $trans_id = $data['trans_id'];


        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 1;     // Server is Temporariy Shutdown
        }

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }


        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("AGENT_API_2");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                file_put_contents("bank_api.txt", $api_url);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($api_url);
                    $res_code = $res->getStatusCode();
                    $res_content = (string) $res->getBody();

                    file_put_contents(base_path().'/public/sample/bank_reply.txt', $res_content.PHP_EOL, FILE_APPEND);

                    $status = BankReply::storeAgentupdate_2($res_content, $agent_msisdn);

                    if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return $z;
    }

    /**
     * Resend Agent OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function resend_otp($data)
    {
        $agen_1 = new UserBankAgentDetails;

        $z = 0;
        $message = "";
        
        $agent_id = "";
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $agent_msisdn = $data['agent_msisdn'];
        $trans_id = $data['trans_id'];


        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 1;     // Server is Temporariy Shutdown
        }

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }


        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("AGENT_API_3");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                file_put_contents("bank_api.txt", $api_url);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($api_url);
                    $res_code = $res->getStatusCode();
                    $res_content = (string) $res->getBody();

                    file_put_contents(base_path().'/public/sample/bank_reply.txt', $res_content.PHP_EOL, FILE_APPEND);

                    list($status, $msg) = BankReply::storeAgentupdate_3($res_content, $agent_msisdn);
                    $message = $msg;

                    if($status == "SUCCESS")
                    {
                        $z = 10;
                    }
                    else if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message);
    }


    /**
     * View Particular Agent Details.
     *
     * @param  String  $user_name
     * @return Object  $d1
     */
    public static function view_user($user_name)
    {
        $agen_1 = new UserBankAgentDetails;

        $d1 = $agen_1->where('user_name', '=', $user_name)->orderBy('id', 'desc')->get();
        
        return $d1;
    }

    /**
     * View Check Agent Mobile .
     *
     * @param  String  $rem_mobile
     * @return Object $z
     */
    public static function check_agent_mobile($agent_mobile)
    {
        $agen_1 = new UserBankAgentDetails;

        $z = $agen_1->where('agent_msisdn', '=', $agent_mobile)->count();
        
        return $z;
    }



     /**
     * View All Agent Details.
     *
     * @param  null
     * @return Object  $d1
     */
    public static function view_all()
    {
        $agen_1 = new UserBankAgentDetails;

        $d1 = $agen_1->orderBy('id', 'desc')->get();
        
        return $d1;
    }


    /**
     * Update Agent Status.
     *
     * @param  String  $bn_id, int $x
     * @return int  $z
     */
    public static function update($rem_id, $opr_id, $x)
    {
        $agen_1 = new UserBankAgentDetails;

        $z = 0;
        
        $date_time = date("Y-m-d H:i:s");
       
        $cnt = $agen_1->where('rem_id', '=', $rem_id)->count();
        if($cnt > 0)
        {
            $agen_1->where('rem_id', '=', $rem_id)->update(['rem_status' => $x, 'rem_rep_opr_id' => $opr_id, 'reply_date' => $date_time, 'updated_at' => $date_time]);
           
            $z = 1;
        }
        else
        {
             $z = 2;
             
        }

        return $z;
    }


    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkUserModeandStatus($user_name, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 2;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 3;
            }

        }

        return $z;
    }

    public static function checkAlreadyEntry($user_name)
    {
        $agen_1 = new UserBankAgentDetails;
        
        $z = 0;

        $cnt = $agen_1->where('user_name', '=', $user_name)
                        ->where('agent_status', '=', 'SUCCESS')->count();

        if($cnt > 0)
        {
           $z = 4;
        }

        return $z;
    }

}
