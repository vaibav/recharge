<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\SmsInterface;
use App\Libraries\GetCommon;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;

use App\Models\TransactionAllDetails;
use App\Models\PaymentLedgerDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;
use App\Models\UserRechargeNewParentDetails;

use App\Models\UserOldAccountDetails;
use App\Models\BackupRechargeDetails;
use App\Models\BackupRechargeParentDetails;

use App\Models\AllTransaction;

class Stock
{
    //
    
    public static function stockOpeningBalance($f_date, $t_date, $user_name)
    {
        $upay_1 = new UserPaymentDetails;

        $user_name = strtoupper($user_name);

        $data = $upay_1->where(function($query) use ($user_name){
                            return $query
                            ->whereRaw("(upper(user_name) = '".$user_name."' OR upper(grant_user_name) = '".$user_name."')");
                        })
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
        $balance = 0;

        foreach($data as $f)
        {
            if(strtoupper($f->user_name) == $user_name && $f->trans_status == 1)
            {
                // Debit +
                $balance = floatval($balance) + floatval($f->grant_user_amount);
            }
            else if(strtoupper($f->grant_user_name) == $user_name && $f->trans_status == 1)
            {
                // Credit -
                $balance = floatval($balance) - floatval($f->grant_user_amount);
            }
        }
        
        return $balance;
    }

    public static function stockCustomOpeningBalance($user_name)
    {
        $upay_1 = new UserPaymentDetails;

        $user_name = strtoupper($user_name);

        $d1 = $upay_1->select('grant_user_amount')->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('payment_mode', '=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)->get();
                        
        
        $balance = 0;

        if($d1->count() > 0)
        {
            $balance = $d1[0]->grant_user_amount;
        }
        
        return $balance;
    }

    public static function stockData($user_name, $f_date, $t_date)
    {
        $upay_1 = new UserPaymentDetails;

        $user_name = strtoupper($user_name);

        $data = AllTransaction::where(function($query) use ($user_name){
                            return $query
                            ->whereRaw("(upper(user_name) = '".$user_name."' OR upper(parent_name) = '".$user_name."')");
                        })
                        ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', '=', 'FUND_TRANSFER');
                            })
                        
                        ->where('rech_status', '=', 'SUCCESS')
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
        
        return $data;
    }

    public static function getRetailerOldRecharge($user_name)
    {
        $rech_n_1 = new UserOldAccountDetails;
        $ce_tot = 0;
        
        $user_name = strtoupper($user_name);

       /* $f_date = "2019-08-08 00:00:00";
        $t_date = "2019-08-08 23:59:59";*/

       /* $ce_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$user_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); */
        
         $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                            ->sum('user_amount');
        
        return $ce_tot;
        
    }

    public static function getRetailerRechargeOpeningSales($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $ce_tot = 0;
        
        $user_name = strtoupper($user_name);

        $ce_tot = $rech_n_1->with(['recharge2'])->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->where('rech_status', '=', 'SUCCESS')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

    public static function getRetailerBill($user_name, $f_date, $t_date)
    {
       $rech_2 = new UserRechargeBillDetails;
      
       $user_name = strtoupper($user_name);
       
       $re_tot = 0;
      

       $re_tot = $rech_2->whereBetween('created_at', [$f_date, $t_date])
                           ->whereRaw('upper(user_name) = ?',[$user_name])
                           ->where('con_status', '=', 'SUCCESS')
                           ->sum('con_total'); 
       
      
       
       $re_tot = round($re_tot, 2);
        
       return $re_tot;
        
    }

    public static function getMoneyOpeningSales($user_name, $f_date, $t_date)
    {
        $bank_1 = new UserBankTransferDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_1->whereBetween('mt_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('mt_status', '=', 'SUCCESS')
                                ->sum('mt_total');

        
        
        return $ce_tot;
        
    }

    public static function getMoneyVerifyOpeningSales($user_name, $f_date, $t_date)
    {
        $bank_2 = new UserBeneficiaryAccVerifyDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_2->whereBetween('ben_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('ben_status', '=', 'SUCCESS')
                                ->sum('ben_surplus');          

        
        
        return $ce_tot;
        
    }

    // RECHARGE DETAILS
    public static function getRetailerRecharge($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewDetails;

        $user_name = strtoupper($user_name);

        $d1 = $rech_n_1->with(['recharge2'])->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->orderBy('id', 'asc')->paginate(15); 
        
        
        return $d1;
        
    }

    //Distributor Recharge Total
    public static function getDistributorSales($user_name, $f_date, $t_date, $rech_type)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $ce_tot = 0;
        
        $user_name = strtoupper($user_name);

        $ce_tot = $rech_n_1->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->where('rech_status', '=', 'SUCCESS')
                        ->where('rech_type', '=', $rech_type)
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

    public static function getDistributorOldRecharge($user_name)
    {
        $rech_n_1 = new UserOldAccountDetails;
        $ce_tot = 0;
        
        $user_name = strtoupper($user_name);

         /*$ce_tot = $rech_n_1->whereRaw('upper(parent_name) = ?',[$user_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total');*/
        
        $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->sum('user_amount');
        
        return $ce_tot;
        
    }

    public static function getDistributorRecharge($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        
        $user_name = strtoupper($user_name);

        $d1= $rech_n_1->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->orderBy('id', 'asc')->paginate(15);  
        
        return $d1;
        
    }


    // PAYMENT REQUEST
    public static function stockRequest($data)
    {
        $upay_1 = new UserPaymentDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;

        $z = 0;

        $trans_id = rand(100000,999999);
        $user_code_req = $data['user_code_req'];
        $user_name_req = $data['user_name_req'];
        $user_amount = $data['user_amount'];
        $user_remarks = $data['user_remarks'];
        $date_time = date("Y-m-d H:i:s");

        $cnt = $user_1->where('user_name', '=', $user_name_req)->count();
        if($cnt > 0)
        {
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = $user_2->select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            $payment_mode = "FUND_TRANSFER";

            // Check Already Requested Amount Pending
            $cx = $upay_1->where([['user_name', '=', $user_name_req], ['trans_status', '=', 0]])->count();
            if($cx == 0)
            {
                 // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name_req;
                $upay_1->user_amount = self::convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = "";
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 0;
            
                $upay_1->save();
            
                $z = "Payment Request has been Sent...";

                // Send Sms...
                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_grnt)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".self::convertNumber($user_amount)." is requested from ".$user_name_req." at ".$date_time;
                    $zep_code = SmsInterface::callSms($user_name_grnt, $dx2[0]->user_mobile, $msg);
                }

            }
            else
            {
                $z = "Error! Requested Amount is Already Pending...";
            }   
           
        }
        else
        {
            $z = "Error! User Name does not Exists...";
        }
       
        return $z;
        
    }
    
    
    // REMOTE PAYMENT & PAYMENT ACCEPT
    public static function stockGrant($data)
    {
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
        $user_1 = new UserPersonalDetails;

        $z = 0;
        $c = 0;
        $cx = 0;
        $cy = 0;
        $status = 0;
        $op = "NONE";

        $trans_id = $data['trans_id'];
        $user_code_req = $data['user_code_req'];
        $user_name_req = $data['user_name_req'];
        $user_code_grnt = $data['user_code_grnt'];
        $user_name_grnt = $data['user_name_grnt'];
        $user_amount = $data['user_amount'];
        $user_remarks = $data['user_remarks'];
        $payment_mode = $data['payment_mode'];
        $date_time = date("Y-m-d H:i:s");

        $cnt = $user_1->where('user_name', '=', $user_name_req)->count();
        if($cnt > 0)
        {
           
            $u_bal_1 = self::getBalance($user_name_grnt);

            $setup_fee = GetCommon::getSetupFee($user_name_grnt);
            if(floatval($setup_fee) >= floatval($u_bal_1))
            {
               $c = 1;      //User Balance is less than setup Fee...
            }

            if(floatval($u_bal_1) < floatval($user_amount))
            {
                $c = 2;     // Granted user has less Amount...
            }

            if($c == 0)
            {
                // Deduct Amount from user name... (- amount)
                $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);
                $cx = self::updateBalance($user_code_grnt, $user_name_grnt, $net_bal_1, $date_time);

                if($cx == 1)
                {
                    // Add amount to user balance Account (+ amount)
                    $u_bal_2 = self::getBalance($user_name_req);
                    $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);
                    $cy = self::updateBalance($user_code_req, $user_name_req, $net_bal_2, $date_time);
                }

                if($cx == 1 && $cy == 1)
                {
                    // Common Updations
                    if($payment_mode == "REMOTE_PAYMENT") 
                    {
                        // Amount Transfer Details....
                        $upay_1->trans_id = $trans_id;
                        $upay_1->user_name = $user_name_req;
                        $upay_1->user_amount = self::convertNumber($user_amount);
                        $upay_1->user_req_date = $date_time;
                        $upay_1->grant_user_name = $user_name_grnt;
                        $upay_1->grant_user_amount = self::convertNumber($user_amount);
                        $upay_1->grant_date = $date_time;
                        $upay_1->payment_mode = $payment_mode;
                        $upay_1->user_remarks = $user_remarks;
                        $upay_1->trans_status = 1;

                        $upay_1->save();
                    }
                    else if($payment_mode == "FUND_TRANSFER") 
                    {
                        $upay_1->where('trans_id', '=', $trans_id)
                                    ->update(['grant_user_name'=>$user_name_grnt, 
                                            'grant_user_amount' => self::convertNumber($user_amount),
                                            'grant_date' => $date_time, 'trans_status' => 1, 'updated_at'=>$date_time]);
                    }


                    // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name_req, 'pay_type' => "C",
                    'pay_amount' => self::convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name_grnt, 'pay_date' => $date_time, 'agent_name' => "-", 'pay_remarks' => $payment_mode, 
                    'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];

                    $upay_2->insert($ar1);

                    // Transaction Details.....
                    $tran_1->trans_id = $trans_id;
                    $tran_1->user_name = $user_name_req;
                    $tran_1->trans_type = $payment_mode;
                    $tran_1->trans_option = '1';
                    
                    $tran_1->save();

                    $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_req)->get();
                    if($dx2->count() > 0)
                    {
                        $msg = "Amount ".self::convertNumber($user_amount)." is transferred successfully at ".$date_time;
                        SmsInterface::callSms($user_name_req, $dx2[0]->user_mobile, $msg);
                    }
                }
                
                
            }
               
        
        }
        else
        {
            $c = 3;
        }

        if($c == 0 && $cx == 1 && $cy == 1)
        {
            $status = 0;
            $op = 'Payment Process is completed Successfully...';
        }
        else if($c == 1)
        {
            $status = 1;
            $op = 'Error! User Balance is less than Setup Fee...';
        }
        else if($c == 2)
        {
            $status = 1;
            $op = 'Error! Granted user has low Balance...';
        }
        else if($c == 3)
        {
            $status = 1;
            $op = 'Error! User Name does not Exists...';
        }

        return array($status, $op);

    }

    // REMOTE PAYMENT & PAYMENT ACCEPT
    public static function PaymentGrant($data)
    {

        $z = 0;
        $c = 0;
        $cx = 0;
        $cy = 0;
        $status = 0;
        $op = "NONE";

        $u_bal_1 = 0;
        $u_bal_2 = 0;
        $net_bal_1 = 0;
        $net_bal_2 = 0;

        $trans_id = $data['trans_id'];
        $user_name_req = $data['user_name_req'];
        $user_name_grnt = $data['user_name_grnt'];
        $user_amount = $data['user_amount'];
        $user_remarks = $data['user_remarks'];
        $payment_mode = $data['payment_mode'];
        $date_time = date("Y-m-d H:i:s");

        $bz = self::checkAccount($user_name_req);

        if($bz == 0)
        {
            if($user_name_req != "admin")
            {
                // Deduct Amount from Grant User
                $u_bal_1 = self::getBalance($user_name_grnt);
                $setup_fee = GetCommon::getSetupFee($user_name_grnt);
                if(floatval($setup_fee) >= floatval($u_bal_1))
                {
                   $c = 1;      //User Balance is less than setup Fee...
                }

                if(floatval($u_bal_1) < floatval($user_amount))
                {
                    $c = 2;     // Granted user has less Amount...
                }

                if($c == 0)
                {
                    // Deduct Amount from user name... (- amount)
                    $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);
                    $cx = self::updateBalance("1", $user_name_grnt, $net_bal_1, $date_time);
                }


            }
            else {
                $cx = 1; // Imaginary deduct only form admin self payment
            }
            
            
            // Add amount to user
            if($c == 0 && $cx == 1)
            {
                // Add amount to user balance Account (+ amount)
                $u_bal_2 = self::getBalance($user_name_req);
                $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);
                $cy = self::updateBalance("1", $user_name_req, $net_bal_2, $date_time);

                if($cy == 1)
                {
                    // Common Updations
                    if($payment_mode == "REMOTE_PAYMENT" || $payment_mode == "SELF_PAYMENT") 
                    {
                        // Amount Transfer Details....
                        $arr = ['trans_id' => $trans_id, 
                                'api_trans_id' => '', 
                                'parent_name' => $user_name_grnt, 
                                'child_name' => '-',
                                'user_name' => $user_name_req, 
                                'net_code' => $data['net_code'],
                                'rech_mobile' => '0',
                                'rech_amount' => self::convertNumber($user_amount),
                                'rech_bank' => '-', 'ret_net_per' => '0', 'ret_net_surp' => '0', 
                                'ret_total' => self::convertNumber($user_amount),
                                'ret_bal' => self::convertNumber($net_bal_2),
                                'dis_net_per' => '0', 'dis_net_surp' => '0',
                                'dis_total' => "-".self::convertNumber($user_amount),
                                'dis_bal' => self::convertNumber($net_bal_1), 
                                'sup_net_per' => '0', 'sup_net_surp' => '0', 'sup_total' => '0', 'sup_bal' => '0',
                                'trans_type' => $payment_mode, 
                                'api_code' => '0', 
                                'rech_date' => $date_time, 
                                'rech_status' => 'SUCCESS',
                                'rech_option' => '1', 
                                'reply_date' => $date_time,
                                'reply_id' => $user_remarks,
                                'created_at' => $date_time, 'updated_at' => $date_time];

                        $c1 = AllTransaction::insert($arr);
                    }
                    else if($payment_mode == "FUND_TRANSFER") 
                    {
                        AllTransaction::where('trans_id', '=', $trans_id)->where('user_name', $user_name_req)
                                    ->update(['ret_total' => self::convertNumber($user_amount),
                                                'ret_bal' => self::convertNumber($net_bal_2),
                                                'dis_total' => "-".self::convertNumber($user_amount),
                                                'dis_bal' => self::convertNumber($net_bal_1), 
                                                'rech_status' => 'SUCCESS',
                                                'rech_option' => '1',
                                                'reply_date' => $date_time, 'updated_at'=>$date_time]);
                    }


                    // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name_req, 'pay_type' => "C",
                    'pay_amount' => self::convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name_grnt, 'pay_date' => $date_time, 'agent_name' => "-", 'pay_remarks' => $payment_mode, 
                    'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];

                    PaymentLedgerDetails::insert($ar1);

                    $dx2 = UserPersonalDetails::select('user_mobile')->where('user_name', '=', $user_name_req)->get();
                    if($dx2->count() > 0)
                    {
                        $msg = "Amount ".self::convertNumber($user_amount)." is transferred successfully at ".$date_time;
                        SmsInterface::callSms($user_name_req, $dx2[0]->user_mobile, $msg);
                    }
                }
                
                
            }
               
        
        }
        else
        {
            $c = 3;
        }

        if($c == 0 && $cx == 1 && $cy == 1)
        {
            $status = 0;
            $op = 'Payment Process is completed Successfully...';
        }
        else if($c == 1)
        {
            $status = 1;
            $op = 'Error! User Balance is less than Setup Fee...';
        }
        else if($c == 2)
        {
            $status = 1;
            $op = 'Error! Granted user has low Balance...';
        }
        else if($c == 3)
        {
            $status = 1;
            $op = 'Error! User Name does not Exists...';
        }

        return array($status, $op);

    }

    // REMOTE PAYMENT & PAYMENT ACCEPT
    public static function PaymentRequest($data)
    {

        $z = 0;
        $status = 0;
        $op = "NONE";

        $trans_id = $data['trans_id'];
        $user_name_req = $data['user_name_req'];
        $user_name_grnt = $data['user_name_grnt'];
        $user_amount = $data['user_amount'];
        $user_remarks = $data['user_remarks'];
        $payment_mode = $data['payment_mode'];
        $date_time = date("Y-m-d H:i:s");

        $bz = self::checkAccount($user_name_req);

        if($bz == 0)
        {
            $arr = ['trans_id' => $trans_id, 
                        'api_trans_id' => '', 
                        'parent_name' => $user_name_grnt, 
                        'child_name' => '-',
                        'user_name' => $user_name_req, 
                        'net_code' => $data['net_code'],
                        'rech_mobile' => '0',
                        'rech_amount' => self::convertNumber($user_amount),
                        'rech_bank' => '-', 'ret_net_per' => '0', 'ret_net_surp' => '0', 
                        'ret_total' => '0',
                        'ret_bal' => '0',
                        'dis_net_per' => '0', 'dis_net_surp' => '0',
                        'dis_total' => '0',
                        'dis_bal' => '0', 
                        'sup_net_per' => '0', 'sup_net_surp' => '0', 'sup_total' => '0', 'sup_bal' => '0',
                        'trans_type' => $payment_mode, 
                        'api_code' => '0', 
                        'rech_date' => $date_time, 
                        'rech_status' => 'PENDING',
                        'rech_option' => '0', 
                        'reply_date' => $date_time,
                        'reply_id' => $user_remarks,
                        'created_at' => $date_time, 'updated_at' => $date_time];

                        $c1 = AllTransaction::insert($arr);
            
                        $status = 0;
                        $op = 'Payment Process is completed Successfully...';
        
        }
        else
        {
            $status = 1;
            $op = 'Error! Invalid User..';
        }

        return array($status, $op);

    }

    // REFUND PAYMENT & PAYMENT REFUND
    public static function PaymentRefund($data)
    {

        $z = 0;
        $c = 0;
        $cx = 0;
        $cy = 0;
        $status = 0;
        $op = "NONE";

        $u_bal_1 = 0;
        $u_bal_2 = 0;
        $net_bal_1 = 0;
        $net_bal_2 = 0;

        $trans_id = $data['trans_id'];
        $user_name_req = $data['user_name_req'];
        $user_name_grnt = $data['user_name_grnt'];
        $user_amount = $data['user_amount'];
        $user_remarks = $data['user_remarks'];
        $payment_mode = $data['payment_mode'];
        $date_time = date("Y-m-d H:i:s");

        $bz = self::checkAccount($user_name_req);

        if($bz == 0)
        {
            if($user_name_req != "admin")
            {
                // Deduct Amount from Grant User
                $u_bal_1 = self::getBalance($user_name_req);
            
                if(floatval($u_bal_1) < floatval($user_amount))
                {
                    $c = 2;     // Request user has less Amount...
                }

                if($c == 0)
                {
                    // Deduct Amount from user name... (- amount)
                    $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);
                    $cx = self::updateBalance("1", $user_name_req, $net_bal_1, $date_time);
                }


            }
            else {
                $cx = 1; // Imaginary deduct only form admin self payment
            }
            
            
            // Add amount to user
            if($c == 0 && $cx == 1)
            {
                // Add amount to user balance Account (+ amount)
                $u_bal_2 = self::getBalance($user_name_grnt);
                $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);
                $cy = self::updateBalance("1", $user_name_grnt, $net_bal_2, $date_time);

                if($cy == 1)
                {
                    // Amount Transfer Details....
                    $arr = ['trans_id' => $trans_id, 
                            'api_trans_id' => '', 
                            'parent_name' => $user_name_grnt, 
                            'child_name' => '-',
                            'user_name' => $user_name_req, 
                            'net_code' => $data['net_code'],
                            'rech_mobile' => '0',
                            'rech_amount' => self::convertNumber($user_amount),
                            'rech_bank' => '-', 'ret_net_per' => '0', 'ret_net_surp' => '0', 
                            'ret_total' => self::convertNumber($user_amount),
                            'ret_bal' => self::convertNumber($net_bal_1),
                            'dis_net_per' => '0', 'dis_net_surp' => '0',
                            'dis_total' => "-".self::convertNumber($user_amount),
                            'dis_bal' => self::convertNumber($net_bal_2), 
                            'sup_net_per' => '0', 'sup_net_surp' => '0', 'sup_total' => '0', 'sup_bal' => '0',
                            'trans_type' => $payment_mode, 
                            'api_code' => '0', 
                            'rech_date' => $date_time, 
                            'rech_status' => 'SUCCESS',
                            'rech_option' => '1', 
                            'reply_date' => $date_time,
                            'reply_id' => $user_remarks,
                            'created_at' => $date_time, 'updated_at' => $date_time];

                    $c1 = AllTransaction::insert($arr);


                    // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name_req, 'pay_type' => "R",
                    'pay_amount' => self::convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name_grnt, 'pay_date' => $date_time, 'agent_name' => "-", 'pay_remarks' => $payment_mode, 
                    'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];

                    PaymentLedgerDetails::insert($ar1);

                    $dx2 = UserPersonalDetails::select('user_mobile')->where('user_name', '=', $user_name_req)->get();
                    if($dx2->count() > 0)
                    {
                        $msg = "Amount ".self::convertNumber($user_amount)." is returned successfully at ".$date_time;
                        SmsInterface::callSms($user_name_req, $dx2[0]->user_mobile, $msg);
                    }
                }
                
                
            }
               
        
        }
        else
        {
            $c = 3;
        }

        if($c == 0 && $cx == 1 && $cy == 1)
        {
            $status = 0;
            $op = 'Payment Process is completed Successfully...';
        }
        else if($c == 1)
        {
            $status = 1;
            $op = 'Error! User Balance is less than Setup Fee...';
        }
        else if($c == 2)
        {
            $status = 1;
            $op = 'Error! Granted user has low Balance...';
        }
        else if($c == 3)
        {
            $status = 1;
            $op = 'Error! User Name does not Exists...';
        }

        return array($status, $op);

    }

    public static function checkAccount($user_name)
    {
        $z = 1;

        $d1 = UserAccountDetails::select('user_status')->where('user_name', $user_name)->get();
        
        if($d1->count() > 0)
        {
            if($d1[0]->user_status == '1'){
                $z = 0;
            }
        }

        return $z;
    }

    public static function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $bal = $d1[0]->user_balance;
        }
        
        return $bal;
    }

    public static function updateBalance($user_code, $user_name, $amt, $date_time)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $z = 0;

        $amt = self::convertNumber($amt);

        $cnt = $uacc_1->where('user_name', '=', $user_name)->count();
        if($cnt > 0)
        {
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $amt, 'updated_at'=>$date_time]);
            $z = 1;
        }
        else if($cnt == 0)
        {
            $uacc_1->user_code = self::getCode($user_name);
            $uacc_1->user_name = $user_name;
            $uacc_1->user_balance = $amt;
            $uacc_1->save();
            $z = 1;
        }

        return $z;
    }

    public static function getCode($user_name)
    {
        $u_code = 1;

        $d1 = UserAccountDetails::select('user_code')->where('user_name', $user_name)->get();
        
        if($d1->count() > 0)
        {
            $u_code = $d1[0]->user_code;
        }

        return $u_code;
    }

    public static function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

}
