<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountDetails;
use App\Models\ApipartnerTokenDetails;
use App\Models\UserAccountBalanceDetails;

class GetAPICommon
{

    /**
     * Get User's Session Details.
     *
     * @param  Request  $request
     * @return Object  $ob
     */
    public static function getUserDetails($auth_token)
    {
        $mob_1 = new ApipartnerTokenDetails;

        $user_name = "none";
        $user_code = "0";
        $user_ubal = "0";
        $q = 0;

        $dc1 = $mob_1->select('user_name')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_code = self::getCode($user_name);
            $user_ubal = self::getBalance($user_name);
            $q = 1;
        }

        return array($q, $user_name, $user_code, $user_ubal);
    }

    public static function getCode($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $ux_code = 0;

        $d1 = $uacc_1->select('user_code')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_code = $d->user_code;
        }

        return $ux_code;
    }

    public static function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    public static function checkNumber($no)
    {
        $x = FALSE;

        if($no != "")
        {
            if (is_numeric($no))
            {
                $x = TRUE;
            }
        }

        return $x;
    }

  
}
