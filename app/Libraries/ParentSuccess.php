<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;


class ParentSuccess
{
       
    public static function updateWebParentPaymentSuccess($trans_id, $status, $date_time)
     {
        $rech_4 = new UserRechargePaymentParentDetails;
        $zx = 0;

        $d5 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d5 > 0)
        {
            $d6 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d6 == 0)
            {
                // Update Data
                $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }
        
        return $zx;

    }


    public static function updateApiParentPaymentSuccess($trans_id, $status, $date_time)
    {
        $rech_3 = new UserRechargePaymentParentApipartnerDetails;
        $zx = 0;

        $d5 = $rech_5->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d5 > 0)
        {
            $d6 = $rech_5->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d6 == 0)
            {
                // Update Data
                $rech_5->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }

        return $zx;
    }

   
}
