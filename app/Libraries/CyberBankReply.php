<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DB;

use App\Libraries\BankTransferInfo;
use App\Libraries\InstantReplyUpdate;

use App\Models\UserBankAgentDetails;
use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;
use App\Models\UserBankTransferDetails;
use App\Models\ApiProviderResultDetails;


class CyberBankReply
{

    public static function getRemitterCheckResult($res_content)
    {
        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "REM_API_3");

        $isVerified = "-1";
        $remitter = "";
        $beneficiary = "";

        //print "<pre>";
        //print_r($data);
        foreach($data as $d)
        {
            if($d[0] == "is_verified")
            {
                $isVerified = $d[1];
            }
            if($d[0] == "remitter")
            {
                $remitter = $d[1];
            }
            if($d[0] == "beneficiary")
            {
                $beneficiary = $d[1];
            }
        }

        if(is_array($remitter))
        {
            if (array_key_exists("is_verified",$remitter)) {
                $isVerified = $remitter['is_verified'];
                //echo $isVerified;
            }
        }
        

        return array($s_status, $s_opr_id, $isVerified, $remitter, $beneficiary);
        
    }

    public static function storeBeneficiaryResult($res_content, $ben_id)
    {
        $benDetails = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        $dateTime = date("Y-m-d H:i:s");

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_API_1");

        // Updation
        if($s_status == "SUCCESS") {
            UserBeneficiaryDetails::where('ben_id', $ben_id)->update(['ben_status' => 'SUCCESS', 'ben_rep_opr_id' => $s_opr_id, 'updated_at' => $dateTime]);
        }
        
        return array($s_status, $s_opr_id);
        
    }

    public static function deleteBeneficiaryResult($res_content, $ben_id)
    {
        $benDetails = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        $dateTime = date("Y-m-d H:i:s");

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_D_API_1");

        // Updation
        if($s_status == "SUCCESS") {
            //UserBeneficiaryDetails::where('ben_id', $ben_id)->delete();
        }
        
        return array($s_status, $s_opr_id);
        
    }

    public static function deleteBeneficiaryOtpResult($res_content, $ben_id)
    {
        $benDetails = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        $dateTime = date("Y-m-d H:i:s");

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_D_API_2");

        // Updation
        if($s_status == "SUCCESS") {
            UserBeneficiaryDetails::where('ben_id', $ben_id)->delete();
        }
        
        return array($s_status, $s_opr_id);
        
    }


    public static function beneficiaryVerifyResult($res_content, $transId)
    {
        $benDetails = new UserBeneficiaryAccVerifyDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        $dateTime = date("Y-m-d H:i:s");

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "BEN_VERIFY_1");

        $benStatus = "VERIFIED";
        $benName = "";
        foreach($data as $d)
        {
            if($d[0] == "benName")
            {
                $benName = $d[1];
            }
            if($d[0] == "benStatus")
            {
                $benStatus = $d[1];
            }
        }

        // Updation
        if($s_status == "SUCCESS") {
            if($benStatus == "VERIFIED"){
                return array($s_status, $s_opr_id, $benName, $benStatus);
            }
            else {
                return array("FAILURE", "-", $benName, $benStatus);
            }
            
        }
        else{
            return array("FAILURE", "-", $benName, $benStatus);
        }

        DB::statement("UPDATE user_beneficiary_acc_verify_details SET ben_status = '".$s_status."' WHERE trans_id = '".$transId."'");
        
    }


    public static function storeRemitterResult($res_content, $rem_id)
    {
        $rem_1 = new UserBankRemitterDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "REM_API_1");

        $isVerified = "0";
        foreach($data as $d)
        {
            if($d[0] == "is_verified")
            {
                $isVerified = $d[1];
            }
        }

        
        // Updation
        $rem_1->where('rem_id', '=', $rem_id)
                    ->update(['rem_status' => "".$s_status, 
                                'rem_rep_opr_id' => $s_opr_id]);
        
        
        return array($s_status, $s_opr_id, $isVerified);
        
    }

   
    public static function getBankResult($result, $api_line)
    {
        $dx = [];
        $s_status = "PENDING";
        $s_opr_id = "NONE";
        $data = [];

       

        list($api_code, $v) = CyberBankApi::getAgent1NetworkLine($api_line);
        if($api_code > 0)
        {
            $dx = self::getAPIResultData($api_code);

           

          
            if(!empty($dx))
            {
                $j = 0;
                foreach($dx as $d)
                {
                    if($d[0] == "STATUS 1")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "SUCCESS";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "STATUS 2")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "FAILURE";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "OPERATOR_ID")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $s_opr_id = $value;
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "DATA")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                    else if($d[0] == "ARRAY")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                }
            }
        }

         
        return array($s_status, $s_opr_id, $data);
    }

    public static function getAPIResultData($api_code)
    {
        $api_4 = new ApiProviderResultDetails;

        $dx = [];
        
        $j = 0;
        if($api_code != "0")
        {
            $d1 = $api_4->select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')->where('api_code', '=', $api_code)->get();
            if($d1->count() > 0)
            {
                foreach($d1 as $d)
                {
                    $dx[$j][0] = $d->api_res_para_name;
                    $dx[$j][1] = $d->api_res_para_field;
                    $dx[$j][2] = $d->api_res_para_value;
                    $j++;
                }
            }
        }

        return $dx;
    }

    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }
}