<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeBillDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderResultDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;

use App\Models\AllTransaction;

class CheckServerResult
{
       
    /**
     * Check URL Result Functions..............................................
     *
     */
    public static function checkResult($data)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $rech_n_2 = new UserRechargeNewStatusDetails;
        $rech_b_1 = new UserRechargeBillDetails;
        $api_1 = new ApiProviderDetails;
        $api_4 = new ApiProviderResultDetails;
        $trans_id = "0";
        $api_trans_id = "0";
        $api_code = "0";
        $user_name = "NONE";
        $zx = 0;
        $c = 0;

        // Checking - Normal Web Recharge data
        $d1 = $rech_n_1->select('trans_id', 'user_name', 'api_trans_id', 'rech_mode')
                ->where([['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();

        foreach($data as $d)
        {
            if($d != "-" && $d != "")
            {
                foreach($d1 as $e)
                {
                    if($e->trans_id == $d)
                    {
                        $trans_id = $e->trans_id;
                        $user_name = $e->user_name;
                        $r_mode = $e->rech_mode;
                        $api_trans_id = $e->api_trans_id;

                        if($e->recharge2 != null) {
                            $api_code = $e->recharge2->api_code;
                        }

                        $zx = 1;
                        if($r_mode == "API") {
                            $c = 2;
                        }
                        else {
                            $c = 1;
                        }
                        
                        break;
                    }
                }
                
            }
        }

      

        // Checking - EBBill Data
        if($zx == 0)
        {
            foreach($data as $d)
            {
                if($d != "-" && $d != "")
                {
                    $d1 = $rech_b_1->select('trans_id', 'user_name', 'api_code')
                                    ->where([['trans_id', '=', $d], ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->get();
                    if($d1->count() > 0)
                    {
                        $trans_id = $d1[0]->trans_id;
                        $user_name = $d1[0]->user_name;
                        $api_code = $d1[0]->api_code;
                        $zx = 1;
                        $c = 3;
                        break;
                    }
                }
            }
        }
       
        // Checking - Ez, Bonrix - web, Api Recharge data - Unknown Transaction ID
        if($zx == 0)
        {
            $da1 = $api_1->select('api_code')->get();

            foreach($da1 as $e)
            {
                $da2 = $api_4->select('api_code', 'api_res_para_name', 'api_res_para_field')
                         ->where('api_code', '=', $e->api_code)
                         ->where(function($query){
                            return $query
                            ->where('api_res_para_name', '=', 'MOBILE NO')
                            ->orWhere('api_res_para_name', '=', 'AMOUNT');
                        })
                        ->get();
                
                $mobile = "";
                $amt = "";
                foreach($da2 as $f)
                {
                    $field = $f->api_res_para_field;
                    $field = str_replace('a', '',$field);
                    if($f->api_res_para_name == 'MOBILE NO')
                    {
                        // Mobile no
                        $mobile = $data[intval($field)];
                    }
                    else if($f->api_res_para_name == 'AMOUNT')
                    {
                        // Mobile no
                        $amt = $data[intval($field)];
                    }
                }

                if (!is_numeric($mobile) || !is_numeric($amt)) 
                {
                   $mobile = "";
                } 
                else
                {
                    if (strpos($amt, '.') !== false) 
                    {
                        $amt = "";
                    }
                }

                if($mobile != "" && $amt != "")
                {
                    
                    $d2 = $rech_n_1->select('trans_id', 'api_trans_id', 'user_name', 'rech_mode')
                                    ->where([['rech_mobile', '=', $mobile], ['rech_amount', '=', $amt],  ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
                    if($d2->count() > 0)
                    {
                        $trans_id = $d2[0]->trans_id;
                        $api_trans_id = $d2[0]->api_trans_id;
                        $user_name = $d2[0]->user_name;
                        $r_mode = $d2[0]->rech_mode;
                        $zx = 1;
                        if($r_mode == "API") {
                            $c = 2;
                        }
                        else {
                            $c = 1;
                        }

                        if($d2[0]->recharge2 != null) {
                            $api_code = $e->recharge2->api_code;
                        }
                        
                        
                    }
      
                    
                } 

                if($zx == 1)
                    break;

            }

            
        }

       

    
        return array($trans_id, $api_code, $user_name, $api_trans_id, $c, $zx);
    }

     public static function newCheckResult($data)
    {
        $rech_n_1 = new AllTransaction;
        $rech_b_1 = new UserRechargeBillDetails;
        $api_1 = new ApiProviderDetails;
        $api_4 = new ApiProviderResultDetails;
        $trans_id = "0";
        $api_trans_id = "0";
        $api_code = "0";
        $user_name = "NONE";
        $zx = 0;
        $c = 0;

        // Checking - Normal Web Recharge data
        $d1 = $rech_n_1->select('trans_id', 'user_name', 'api_trans_id', 'api_code')
                                ->where([['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();

        foreach($data as $d)
        {
            if($d != "-" && $d != "")
            {
                foreach($d1 as $e)
                {
                    if($e->trans_id == $d)
                    {
                        $trans_id = $e->trans_id;
                        $user_name = $e->user_name;
                        $api_trans_id = $e->api_trans_id;
                        $api_code = $e->api_code;

                        $zx = 1;
                        if (strpos($trans_id, 'VBA') !== false) {
                            $r_mode = "API";
                            $c = 2;
                        }
                        else {
                            $c = 1;
                        }
                        
                        break;
                    }
                }
                
            }
        }

      

        // Checking - EBBill Data
        if($zx == 0)
        {
            foreach($data as $d)
            {
                if($d != "-" && $d != "")
                {
                    $d1 = $rech_b_1->select('trans_id', 'user_name', 'api_code')
                                    ->where([['trans_id', '=', $d], ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->get();
                    if($d1->count() > 0)
                    {
                        $trans_id = $d1[0]->trans_id;
                        $user_name = $d1[0]->user_name;
                        $api_code = $d1[0]->api_code;
                        $zx = 1;
                        $c = 3;
                        break;
                    }
                }
            }
        }
       
        // Checking - Ez, Bonrix - web, Api Recharge data - Unknown Transaction ID
        if($zx == 0)
        {
            $da1 = $api_1->select('api_code')->get();

            foreach($da1 as $e)
            {
                $da2 = $api_4->select('api_code', 'api_res_para_name', 'api_res_para_field')
                         ->where('api_code', '=', $e->api_code)
                         ->where(function($query){
                            return $query
                            ->where('api_res_para_name', '=', 'MOBILE NO')
                            ->orWhere('api_res_para_name', '=', 'AMOUNT');
                        })
                        ->get();
                
                $mobile = "";
                $amt = "";
                foreach($da2 as $f)
                {
                    $field = $f->api_res_para_field;
                    $field = str_replace('a', '',$field);
                    if($f->api_res_para_name == 'MOBILE NO')
                    {
                        // Mobile no
                        $mobile = $data[intval($field)];
                    }
                    else if($f->api_res_para_name == 'AMOUNT')
                    {
                        // Mobile no
                        $amt = $data[intval($field)];
                    }
                }

                if (!is_numeric($mobile) || !is_numeric($amt)) 
                {
                   $mobile = "";
                } 
                else
                {
                    if (strpos($amt, '.') !== false) 
                    {
                        $amt = "";
                    }
                }

                if($mobile != "" && $amt != "")
                {
                    
                    $d2 = $rech_n_1->select('trans_id', 'api_trans_id', 'user_name', 'api_code')
                                    ->where([['rech_mobile', '=', $mobile], ['rech_amount', '=', $amt],  
                                            ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
                    if($d2->count() > 0)
                    {
                        $trans_id = $d2[0]->trans_id;
                        $api_trans_id = $d2[0]->api_trans_id;
                        $user_name = $d2[0]->user_name;
                        $api_code = $e->api_code;
                       
                        $zx = 1;
                        if (strpos($trans_id, 'VBA') !== false) {
                            $r_mode = "API";
                            $c = 2;
                        }
                        else {
                            $c = 1;
                        }
 
                    }
      
                    
                } 

                if($zx == 1)
                    break;

            }

            
        }

       

    
        return array($trans_id, $api_code, $user_name, $api_trans_id, $c, $zx);
    }


    

}
