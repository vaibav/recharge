<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountDetails;
use App\Models\NetworkLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;
use App\Models\NetworkLineApipartnerDetails;

class ApiUrl
{
    //
    
     /**
     * Network Line and API URL Functions..............................................
     *
     */
    public static function getNetworkLine($net_code, $amt)
    {
        $net_3 = new NetworkLineDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Specific Amount
        $d1 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'SPECIFIC_AMOUNT'],  
                             ['net_line_value_1', '=', $amt], ['net_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
        else
        {
            // Range Values
            $d2 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'RANGE_VALUES'], ['net_line_status', '=', 1]])->get();

            $zx = 0;
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    $v1 = $d->net_line_value_1;
                    $v2 = $d->net_line_value_2;

                    if($v1 != "" & $v2 != "")
                    {
                        if($v1 != "*" & $v2 != "*")
                        {
                            if((floatval($amt) >= floatval($v1)) && (floatval($amt) <= floatval($v2)))
                            {
                                $api_code = $d->api_code;
                                $zx = 1;
                                $v = 2;
                                break;
                            }
                        }
                    }

                }
            }

            // All Values
            if($zx == 0)
            {
                $d3 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'ALL'], ['net_line_status', '=', 1]])->get();
                if($d3->count() > 0)
                {
                    $api_code = $d3[0]->api_code;
                    $v = 3;
                }
            }

        }

        return array($api_code, $v);
    }


    public static function getApiNetworkLine($net_code, $amt, $user_name)
    {
        $net_3 = new NetworkLineApipartnerDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Specific Amount
        $d1 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'SPECIFIC_AMOUNT'],  
                             ['net_line_value_1', '=', $amt], ['net_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
        else
        {
            // Range Values
            $d2 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'RANGE_VALUES'], ['net_line_status', '=', 1]])->get();

            $zx = 0;
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    $v1 = $d->net_line_value_1;
                    $v2 = $d->net_line_value_2;

                    if($v1 != "" & $v2 != "")
                    {
                        if($v1 != "*" & $v2 != "*")
                        {
                            if((floatval($amt) >= floatval($v1)) && (floatval($amt) <= floatval($v2)))
                            {
                                $api_code = $d->api_code;
                                $zx = 1;
                                $v = 2;
                                break;
                            }
                        }
                    }

                }
            }

            // All Values
            if($zx == 0)
            {
                $d3 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'ALL'], ['net_line_status', '=', 1]])->get();
                if($d3->count() > 0)
                {
                    $api_code = $d3[0]->api_code;
                    $v = 3;
                }
            }

        }

        return array($api_code, $v);
    }

    public static function generateAPICommon($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $api_1 = new ApiProviderDetails;

        $api_method = "GET";
        $api_url = "NONE";
        $api_data = [];

        $d1 = $api_1->select('api_method')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_method = $d1[0]->api_method;

            if($api_method == "GET")
            {
                $api_url = self::generateAPI($api_code, $net_code, $mobile, $amt, $trans_id);
            }
            else if($api_method == "POST")
            {
                list($api_url, $api_data) = self::generateAPIwithPost($api_code, $net_code, $mobile, $amt, $trans_id);
            }
        }

        return array($api_url, $api_data, $api_method);
    }


    public static function generateAPI($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            

            if (strpos($url_link, '<mobileno>') !== false) {
                $url = self::generateNewGETUrl($api_code, $net_code, $mobile, $amt, $trans_id);
                $zx = 2;
            }
            else
            {
                // Get Network Code Details
                $d3 = $api_3->select('api_net_short_code')->where([['api_code', '=', $api_code], ['net_code', '=', $net_code]])->get();
                if($d3->count() > 0)
                {
                    $api_net_code = $d3[0]->api_net_short_code;

                    // Get Paramenters
                    $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
                    if($d2->count() > 0)
                    {
                        foreach($d2 as $d)
                        {
                            if ($d->api_para_type == "PRE_DEFINED")
                            {
                                $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                            }
                            else if ($d->api_para_type == "MOBILE_NO")
                            {
                                $url_link = $url_link.$d->api_para_name."=".$mobile."&";
                            }
                            else if ($d->api_para_type == "AMOUNT")
                            {
                                $url_link = $url_link.$d->api_para_name."=".$amt."&";
                            }
                            else if ($d->api_para_type == "NETWORK")
                            {
                                $url_link = $url_link.$d->api_para_name."=".$api_net_code."&";
                            }
                            else if ($d->api_para_type == "NETWORK_REPLACE")
                            {
                                $url_link = $url_link.$api_net_code;
                            }
                            else if ($d->api_para_type == "TRANSACTION_ID")
                            {
                                $url_link = $url_link.$d->api_para_name."=".$trans_id."&";
                            }
                        }
                    }
                    else
                    {
                        $zx = 1;
                    }
                }
                else
                {
                    $zx = 1;
                }
            }

            

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }

    public static function generateNewGETUrl($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $url = "";

        $d1 = ApiProviderDetails::where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();

        if($d1->count() > 0)
        {
            $url = $d1[0]->api_url;

            $net_short = "";
            if($d1[0]->apinetwork != null)
            {
                foreach($d1[0]->apinetwork as $d)
                {
                    if($d->net_code == $net_code)
                    {
                        $net_short = $d->api_net_short_code;
                    }
                }
            }

            //file_put_contents(base_path().'/public/sample/net_short.txt', $net_short.PHP_EOL, FILE_APPEND);

            //$net_short = "3";

            if (strpos($url, '<trans_id>') !== false) {
                $url = str_replace("<trans_id>", $trans_id, $url);
            }

            if (strpos($url, '<mobileno>') !== false) {
                $url = str_replace("<mobileno>", $mobile, $url);
            }

            if (strpos($url, '<amt>') !== false) {
                $url = str_replace("<amt>", $amt, $url);
            }

            if (strpos($url, '<opr>') !== false) {
                $url = str_replace("<opr>", $net_short, $url);
            }

            file_put_contents(base_path().'/public/sample/rech_new_url.txt', $url.PHP_EOL, FILE_APPEND);

        }

        return $url;
    }

    public static function generateAPIwithPost($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $url_data = [];
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            
            

            /*if (strpos($url_link, '?') !== false) {
                $cccc = 0;
            }
            else
            {
                $url_link = $url_link."?";
            }*/

            // Get Network Code Details
            $d3 = $api_3->select('api_net_short_code')->where([['api_code', '=', $api_code], ['net_code', '=', $net_code]])->get();
            if($d3->count() > 0)
            {
                $api_net_code = $d3[0]->api_net_short_code;

                 // Get Paramenters
                $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
                if($d2->count() > 0)
                {
                    foreach($d2 as $d)
                    {
                        if ($d->api_para_type == "PRE_DEFINED")
                        {
                            //$url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                            $url_data[$d->api_para_name] = $d->api_para_value;
                        }
                        else if ($d->api_para_type == "MOBILE_NO")
                        {
                            //$url_link = $url_link.$d->api_para_name."=".$mobile."&";
                            $url_data[$d->api_para_name] = $mobile;
                        }
                        else if ($d->api_para_type == "AMOUNT")
                        {
                            //$url_link = $url_link.$d->api_para_name."=".$amt."&";
                            $url_data[$d->api_para_name] = $amt;
                        }
                        else if ($d->api_para_type == "NETWORK")
                        {
                            //$url_link = $url_link.$d->api_para_name."=".$api_net_code."&";
                            $url_data[$d->api_para_name] = $api_net_code;
                        }
                        else if ($d->api_para_type == "NETWORK_REPLACE")
                        {
                            //$url_link = $url_link.$api_net_code;
                        }
                        else if ($d->api_para_type == "TRANSACTION_ID")
                        {
                            //$url_link = $url_link.$d->api_para_name."=".$trans_id."&";
                            $url_data[$d->api_para_name] = $trans_id;
                        }
                    }
                }
                else
                {
                    $zx = 1;
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return array($url, $url_data);
    }



    public static function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public static function sendResultApipartner($user_name, $api_trans_id, $opr_id, $status, $option)
    {
        $res_code = 0;
        $api_url = "";

        list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

        if($option == 1)
        {
            $api_url = $api_s_url;
        }
        else if($option == 2)
        {
            $api_url = $api_f_url;
        }

        if (strpos($api_url, '<trid>') !== false) {
            $api_url = str_replace("<trid>", $api_trans_id, $api_url);
        }
        if (strpos($api_url, '<oprid>') !== false) {
            $api_url = str_replace("<oprid>", $opr_id, $api_url);
        }
        if (strpos($api_url, '<status>') !== false) {
            $api_url = str_replace("<status>", $status, $api_url);
        }

        if($api_url != "") {
            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content_1 =$res->getBody();
        }
       

        return $res_code;

    }

}
