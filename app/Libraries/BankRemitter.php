<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankTransferInfo;
use App\Libraries\BankReply;
use App\Libraries\Beneficiary;

use App\Models\UserBankRemitterDetails;
use App\Models\UserAccountDetails;
use App\Models\ApiUrlDetails;

class BankRemitter
{

    /**
     * Add Remitter Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        $rem_1 = new UserBankRemitterDetails;

        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        
        $rem_id = rand(1000,9999);
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name_1'];
        $date_time = date("Y-m-d H:i:s");

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 1;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
        }

        $zy = self::checkAlreadyEntry($user_name);
        if($zy == 3)
        {
            $z = 3;     // Already Registered...
        }

        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("REM_API_1");
            if($api_code > 0)
            {
               $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

        

                if($api_url != "NONE")
                    $res_code = 200;
                
                
                // Insert Record... 
                $rem_1->rem_id = $rem_id;
                $rem_1->api_trans_id = $data['api_trans_id'];
                $rem_1->user_name = $data['user_name_1'];
                $rem_1->user_rem_name = $data['user_name'];
                $rem_1->user_address = $data['user_address'];
                $rem_1->user_city = $data['user_city'];
                $rem_1->user_state_code = $data['user_state_code'];
                $rem_1->user_pincode = $data['user_pincode'];
                $rem_1->msisdn = $data['msisdn'];
                $rem_1->rem_date = $date_time;
                $rem_1->api_code = "0";
                $rem_1->rem_req_status = "done";
                $rem_1->rem_status = "PENDING";
                $rem_1->rem_rep_opr_id = "0";
                $rem_1->reply_date = $date_time;

                $rem_1->save();
                

                if ($res_code == 200)
                {
                    $res_content = self::runURL($api_url, "REM_ADD");

                    list($status, $msg) = BankReply::storeRemitterupdate_1($res_content, $rem_id);
                    $message = $msg;

                    if($status == "FAILURE")
                    {
                        $z = 4;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 5;
                    }

                }
            }
            
        }
        

        return array($z, $message, $res_content);
    }


    /**
     * Check Valid Remitter.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function userCheck($data)
    {
        $z = 1;
        $message = "No Action...";

        $rem_1 = new UserBankRemitterDetails;
        

        $r_mode = $data['r_mode'];
        $user_name = $data['user_name_1'];
        $msisdn = $data['msisdn'];

        // Get Api Code
        list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("REM_API_2");
        if($api_code > 0)
        {
            $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

            file_put_contents(base_path().'/public/sample/bank_api_url.txt', $api_url.PHP_EOL, FILE_APPEND);

            $res_code = 201;

            if ($api_url != "NONE")
            {
                $res_content = self::runURL($api_url, "REM_CHECK");

                list($status, $msg) = BankReply::storeRemitterupdate_2($res_content);
                $message = $msg;

                if($status == "SUCCESS")
                {
                    $z = 0;
                    $y1 = self::addCustomer($res_content, $user_name);
                    //$y2 = Beneficiary::checkServerBeneficiary($res_content, $user_name, $msisdn);
                }
                else if($status == "FAILURE")
                {
                    $z = 1;
                }
            }
        }

        
       

        return array($z, $message, $res_content);
    }


    /**
     * View Particular Remitter Details.
     *
     * @param  String  $user_name
     * @return Object  $d1
     */
    public static function view_user($user_name)
    {
        $rem_1 = new UserBankRemitterDetails;

        $d1 = $rem_1->where('user_name', '=', $user_name)->orderBy('id', 'desc')->get();
        
        return $d1;
    }

    /**
     * View Check Remitter Mobile .
     *
     * @param  String  $rem_mobile
     * @return Object $z
     */
    public static function check_remitter_mobile($rem_mobile)
    {
        $rem_1 = new UserBankRemitterDetails;

        $z = $rem_1->where('msisdn', '=', $rem_mobile)->count();
        
        return $z;
    }



     /**
     * View All Remitter Details.
     *
     * @param  null
     * @return Object  $d1
     */
    public static function view_all()
    {
        $rem_1 = new UserBankRemitterDetails;

        $d1 = $rem_1->orderBy('id', 'desc')->get();
        
        return $d1;
    }


    /**
     * Update Remitter Status.
     *
     * @param  String  $bn_id, int $x
     * @return int  $z
     */
    public static function update($rem_id, $opr_id, $x)
    {
        $rem_1 = new UserBankRemitterDetails;

        $z = 0;
        
        $date_time = date("Y-m-d H:i:s");
       
        $cnt = $rem_1->where('rem_id', '=', $rem_id)->count();
        if($cnt > 0)
        {
            $rem_1->where('rem_id', '=', $rem_id)->update(['rem_status' => $x, 'rem_rep_opr_id' => $opr_id, 'reply_date' => $date_time, 'updated_at' => $date_time]);
           
            $z = 1;
        }
        else
        {
             $z = 2;
             
        }

        return $z;
    }

    public static function checkUserModeandStatus($user_name, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 2;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 3;
            }

        }

        return $z;
    }

    public static function checkAlreadyEntry($msisdn)
    {
        $rem_1 = new UserBankRemitterDetails;
        
        $z = 0;

        $cnt = $rem_1->where('msisdn', '=', $msisdn)
                        ->where('rem_status', '=', 'SUCCESS')->count();

        if($cnt > 0)
        {
           $z = 3;
        }

        return $z;
    }

    public static function addCustomer($res_content, $user_name)
    {
        $rem_1 = new UserBankRemitterDetails;

        $res = (array)json_decode($res_content);

        $msisdn = "";
        $name = "";
        $y = 1;
        $date_time = date("Y-m-d H:i:s");

        if (array_key_exists('msisdn', $res)) {
            $msisdn = $res['msisdn'];
        }
        if (array_key_exists('user_name', $res)) {
            $name = $res['user_name'];
        }

        $cnt = $rem_1->where('msisdn', '=', $msisdn)->count();

        if($cnt == 0)
        {
            $rem_1->rem_id = rand(1000,9999);
            $rem_1->api_trans_id = "-";
            $rem_1->user_name = $user_name;
            $rem_1->user_rem_name = $name;
            $rem_1->user_address = "-";
            $rem_1->user_city = "-";
            $rem_1->user_state_code = "0";
            $rem_1->user_pincode = "0";
            $rem_1->msisdn = $msisdn;
            $rem_1->rem_date = $date_time;
            $rem_1->api_code = "0";
            $rem_1->rem_req_status = "done";
            $rem_1->rem_status = "SUCCESS";
            $rem_1->rem_rep_opr_id = "0";
            $rem_1->reply_date = $date_time;

            $rem_1->save();

            $y = 0;
        }
        else if($cnt > 0)
        {
            $rem_1->where('msisdn', '=', $msisdn)->update(['rem_status' => 'SUCCESS', 'updated_at' => $date_time]);
        }

        return $y;

    }

    public static function runURL($url, $type)
    {
        $api_1 = new ApiUrlDetails;
        $res_content = "";

         file_put_contents(base_path().'/public/sample/api_url.txt', $url.PHP_EOL, FILE_APPEND);
        
        try
        {
            $client = new \GuzzleHttp\Client();
            $res = $client->get($url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();
            
             file_put_contents(base_path().'/public/sample/curl.txt', $res_content.PHP_EOL, FILE_APPEND);
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            $res_content = "{'status':'failure','Message':'".$e->getMessage()."'}";
        }
        
        // Save Result
        $api_1->trans_id = "VB".rand(10000,99999);
        $api_1->api_type = $type;
        $api_1->api_request = $url;
        $api_1->api_result = $res_content;
    
        $api_1->save();

        
        return $res_content;
    }

}
