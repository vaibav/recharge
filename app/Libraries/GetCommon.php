<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\AdminOfferDetails;

class GetCommon
{

    /**
     * Get User's Session Details.
     *
     * @param  Request  $request
     * @return Object  $ob
     */
    public static function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->offer = null;
        $ob->inactive = null;
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        $ob->offer = self::getInfo($ob->mode);
        $ob->inactive = self::getInactive();

        return $ob;
    }

    public static function getInfo($user_type)
    {
        $off = AdminOfferDetails::where(function($query) use ($user_type){
            return $query
            ->where('user_type', '=', 'ALL')
            ->orWhere('user_type', '=', $user_type);
        })->where('offer_status', '=', '1')->get();

        return $off;
    }

    public static function getInactive()
    {
        $d3 = NetworkDetails::select('net_name')->where('net_status', '=', 2)->get();

        return $d3;
    }


    public static function getUserAccountDetails($user_name, $user_pwd)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;
        $ob->status = 0;

        $uacc_1 = new UserAccountBalanceDetails;
        $user_2 = new UserAccountDetails;

        $user_rec_mode = "";
        $user_status = 25;

        $d2 = $user_2->select('user_code', 'user_type', 'user_rec_mode', 'user_status')->where([['user_name', '=', $user_name], ['user_pwd', '=', $user_pwd]])->get();
        foreach($d2 as $d)
        {
            $ob->user = $user_name;
            $ob->code = $d->user_code;
            $ob->mode = $d->user_type;
            $user_rec_mode = $d->user_rec_mode;
            $user_status = $d->user_status;
            $ob->status = 1;
        }
        if (strpos($user_rec_mode, 'API') !== false) {
            // do nothing
            $x3 = 0;    // test variable
        }
        else
        {
            $ob->status = 3;
        }
        // Check User Account Status
        if($user_status != 1)
        {
            $ob->status = 4;
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }


    /**
     * Send SMS to User's Mobile Number.
     *
     * @param  string  $user_name, $mobile, $msg
     * @return void
     */
    public static function sendSms($mobile, $user_name, $msg)
    {
        //Sms sending
        $client = new \GuzzleHttp\Client();

       $response = $client->request('GET', 'http://SMS.VAIBAVONLINE.IN/rest/services/sendSMS/sendGroupSms?', [
            'query' => ['AUTH_KEY'    =>  '519a6bdfbaca597c541d7dd8285ca24',
            'message'    =>  $msg,
            'senderId'  =>  'VAIBAV',
            'routeId'   =>  '1',
            'mobileNos'  =>  $mobile,
            'smsContentType'    =>  'english',
            ]
        ]);

        
        $res_code = $response->getStatusCode();
        $res_content =$response->getBody();

    }



    /**
     * Get Setup Fee from UserAccountDetails Table.
     *
     * @param  string  $user_name
     * @return string  $user_ubal
     */
    public static function getSetupFee($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_setup_fee')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_setup_fee;

            if($user_ubal == "")
            {
                $user_ubal = 0;
            }
        }

        return $user_ubal;
    }


}
