<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\OfferLineDetails;
use App\Models\MobileSmsDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Libraries\BankRemitter;

class SmsInterface
{
    //
    
     /**
     * Sms Interface Functions..............................................
     *
     */
    public static function callSms($user_name, $mobile, $msg)
    {
        $off_1 = new OfferLineDetails;
        $mob_1 = new MobileSmsDetails;

        $api_code = '0';

        $res_x = 0;

        $m1 = $off_1->select('api_code')->where('offer_type', '=', 'SMS')->get();

        if($m1->count() > 0)
        {
            $api_code = $m1[0]->api_code;

            $msg_1 = $msg;

            $msg = self::convert_text($msg);

            $sms_url = self::generateAPI($api_code, $mobile, $msg);

            $res_content = BankRemitter::runURL($sms_url, "SMS");

            // Insert Data......
            $mob_1->trans_id = rand(10000,99999);
            $mob_1->user_name = $user_name;
            $mob_1->api_code = $api_code;
            $mob_1->user_mobile = $mobile;
            $mob_1->user_message = $msg_1;
            $mob_1->reply_message = $res_content;

            $mob_1->save();

            $res_x = 200;
        }

        return $res_x;

    }


    public static function generateAPI($api_code, $mobile, $msg)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;


        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            //$url_link = $url_link."?";

            // Get Paramenters
            $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    if ($d->api_para_type == "PRE_DEFINED")
                    {
                        $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                    }
                    else if ($d->api_para_type == "MOBILE_NO")
                    {
                        $url_link = $url_link.$d->api_para_name."=".$mobile."&";
                    }
                    else if ($d->api_para_type == "MESSAGE")
                    {
                        $url_link = $url_link.$d->api_para_name."=".$msg."&";
                    }
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }

    public static function convert_text($text) {

        $t = $text;
        
        $specChars = array(
            '!' => '%21',    '"' => '%22',
            '#' => '%23',    '$' => '%24',    '%' => '%25',
            '&' => '%26',    '\'' => '%27',   '(' => '%28',
            ')' => '%29',    '*' => '%2A',    '+' => '%2B',
            ',' => '%2C',    '-' => '%2D',    '.' => '%2E',
            '/' => '%2F',    ':' => '%3A',    ';' => '%3B',
            '<' => '%3C',    '=' => '%3D',    '>' => '%3E',
            '?' => '%3F',    '@' => '%40',    '[' => '%5B',
            '\\' => '%5C',   ']' => '%5D',    '^' => '%5E',
            '_' => '%5F',    '`' => '%60',    '{' => '%7B',
            '|' => '%7C',    '}' => '%7D',    '~' => '%7E',
            ',' => '%E2%80%9A',  ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }
    

}
