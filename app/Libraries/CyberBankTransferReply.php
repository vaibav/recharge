<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DB;

use App\Libraries\BankTransferInfo;
use App\Libraries\InstantReplyUpdate;
use App\Libraries\CyberBankReply;
use App\Libraries\PercentageCalculation;
use App\Libraries\BankRemitter;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;

use App\Models\AllTransaction;
use App\Models\AllRequest;

class CyberBankTransferReply
{
	 public static function storeTransferResult($res_content, $trans_id)
    {

        $result = CyberBankReply::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = CyberBankReply::getBankResult($result, "TRN_API_1_1");

        $operatorID = "0";
        $resultMessage = "-";
        foreach($data as $d)
        {
            if($d[0] == "opr_id")
            {
                $operatorID = $d[1];
            }
            if($d[0] == "message")
            {
                $resultMessage = $d[1];
            }
        }

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, "Transaction Failure");
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE all_transactions SET rech_status = '".$s_status."', reply_id ='Pending...' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            DB::statement("UPDATE all_transactions SET rech_status = '".$s_status."', reply_id = '".$s_opr_id."'
                                                        WHERE trans_id = '".$trans_id."'");
        }

        return array($s_status, $operatorID, $resultMessage);
        
    }


    /**
     * Success Update
     */

    public static function success_update($trans_id, $rep_trans_id, $bank_trans_id, $res_amt, $cc)
	{
		
        $rech = new AllTransaction;
        $net_2 = new NetworkDetails;
        
        
        $date_time = date("Y-m-d H:i:s");
        $status = "SUCCESS";
        $user_name = "NONE";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
        if($d1->count() > 0)
        {
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $r_mode = "API";
            }
            else {
                $r_mode = "WEB";
            }

            // Update Data
            DB::statement("UPDATE all_transactions SET rech_status = '".$status."', rech_option = '1',
                                                        reply_id ='".$bank_trans_id."' WHERE trans_id = '".$trans_id."'");
            $zx++;

            if($r_mode == "API" )
            {
                list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                // API Partner Reply URL Parameter Setting...
                if (strpos($api_s_url, '<trid>') !== false) {
                    $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                }
                if (strpos($api_s_url, '<oprid>') !== false) {
                    $api_s_url = str_replace("<oprid>", $bank_trans_id, $api_s_url);
                }
                if (strpos($api_s_url, '<status>') !== false) {
                    $api_s_url = str_replace("<status>", "SUCCESS", $api_s_url);
                }

                if($api_s_url != "")
                {
                    BankRemitter::runURL($api_s_url, "RESPONSE");
                }
            }

            if($cc == "2")
            {
                if(floatval($rech_amount) > floatval($res_amt))
                {
                    $red_amount = floatval($rech_amount) - floatval($res_amt);
                    $red_amount = round($red_amount, 2);
                    self::refundBalance($user_name, $red_amount);
                }
            }
   
        }
 
        return $zx;
       
    }

     /**
     * Failure Update
     */

    public static function failure_update($trans_id, $opr_id)
	{
		
        $rech = new AllTransaction;
        $net_2 = new NetworkDetails;
        
        $date_time = date("Y-m-d H:i:s");
        $status = "FAILURE";
        
        $zx = 0;
        $z = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->ret_total;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }
            else {
                $mode = "WEB";
            }

            $zx = self::insertFailure($trans_id, $opr_id, $date_time);

            $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                        ->update([ 'rech_option' => '2', 'updated_at' => $date_time]);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", "FAILURE", $api_f_url);
                    }

                    if($api_f_url != "")
                    {
                        BankRemitter::runURL($api_f_url, "RESPONSE");
                    }
                }

                $z = 1;
            }

        }
 
        return $z;
       
    }

    public static function insertFailure($trans_id, $opr_id, $date_time)
    {
        $rech = [];
        $c1 = 0;

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            // Array
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $d1[0]->api_trans_id;

            $rech['parent_name'] = $d1[0]->parent_name;
            $rech['child_name'] = $d1[0]->child_name;
            $rech['user_name'] =  $d1[0]->user_name;

            $rech['net_code'] =  $d1[0]->net_code;
            $rech['rech_mobile'] = $d1[0]->rech_mobile;
            $rech['rech_amount'] = $d1[0]->rech_amount;
            $rech['rech_bank'] = "-";
            $rech['ret_net_per'] = $d1[0]->ret_net_per;
            $rech['ret_net_surp'] = $d1[0]->ret_net_surp;
            $rech['ret_total'] = $d1[0]->ret_total;
            $rech['ret_bal'] = '0';

            $rech['dis_net_per'] = $d1[0]->dis_net_per;
            $rech['dis_net_surp'] = $d1[0]->dis_net_surp;
            $rech['dis_total'] = $d1[0]->dis_total;
            $rech['dis_bal'] = '0';

            $rech['sup_net_per'] = $d1[0]->sup_net_per;
            $rech['sup_net_surp'] = $d1[0]->sup_net_surp;
            $rech['sup_total'] = $d1[0]->sup_total;
            $rech['sup_bal'] = '0';

            $rech['trans_type'] = $d1[0]->trans_type;
            $rech['api_code'] = $d1[0]->api_code;
            $rech['rech_date'] = $d1[0]->rech_date;
            $rech['rech_status'] = "FAILURE";
            $rech['rech_option'] = "2";
            $rech['reply_date'] = $date_time;
            $rech['reply_id'] = $opr_id;
            
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;

            list($rx_bal, $zx) = self::update_balance($d1[0]->user_name, $d1[0]->ret_total, $date_time);

            if($zx > 0)
            {
                $rech['ret_bal'] = $rx_bal;

                if($d1[0]->child_name != "NONE")
                {
                    if($d1[0]->dis_total != "0.00" && $d1[0]->dis_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->child_name, $d1[0]->dis_total, $date_time);
                        $rech['dis_bal'] = $dx_bal;
                    }
                }
                if($d1[0]->sup_total != "0.00" && $d1[0]->sup_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->parent_name, $d1[0]->sup_total, $date_time);
                        $rech['sup_bal'] = $dx_bal;
                }

                $c1 = AllTransaction::insert($rech);
               
            }
        }

        return $c1;
    }

    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }

    public static function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }

}