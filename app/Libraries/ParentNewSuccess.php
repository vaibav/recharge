<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;


class ParentNewSuccess
{
       
    public static function updateParentPaymentSuccess($trans_id, $status, $date_time)
     {
        $rech_4 = new UserRechargeNewParentDetails;
        $zx = 0;

        $d5 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->count();
        if($d5 > 0)
        {
           
            // Update Data
            $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                ->update(['rech_status' => $status, 'rech_option' => '1', 'updated_at' => $date_time]);
            $zx++;
            
        }
        
        return $zx;

    }


    

   
}
