<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankRemitter;
use App\Libraries\RechargeInfo;

use App\Models\RechargeInfoDetails;

class RechargeOffers
{
    //
    public static function getOffer121($api_code, $net_code, $mobile, $user_name, $trans_id)
    {
        $ri = new RechargeInfoDetails;

        $api_url = RechargeInfo::getApiUrl($api_code, $net_code, $mobile);

        $res_content = BankRemitter::runURL($api_url, "OFFER_121");

        $ri->trans_id = $trans_id;
        $ri->user_name = $user_name;
        $ri->rech_type = "OFFER_121";
        $ri->rech_mobile = $mobile;

        $ri->save();

        return $res_content;

    }

    public static function getDth($api_code, $net_code, $mobile, $user_name, $trans_id)
    {
        $ri = new RechargeInfoDetails;

        $api_url = RechargeInfo::getApiUrl($api_code, $net_code, $mobile);

        $res_content = BankRemitter::runURL($api_url, "DTH_INFO");

        $ri->trans_id = $trans_id;
        $ri->user_name = $user_name;
        $ri->rech_type = "DTH_INFO";
        $ri->rech_mobile = $mobile;

        $ri->save();

        return $res_content;

    }

    public static function getPlan($api_code, $net_code, $mobile, $user_name, $trans_id, $net_short)
    {
        $ri = new RechargeInfoDetails;

        $api_url = RechargeInfo::getApiUrl($api_code, $net_code, $mobile);

        $res_content = BankRemitter::runURL($api_url, "MOBILE_PLAN");

        $ri->trans_id = $trans_id;
        $ri->user_name = $user_name;
        $ri->rech_type = "MOBILE_PLAN";
        $ri->rech_mobile = $net_short;

        $ri->save();

        return $res_content;

    }

    public static function checkOperator($api_code, $net_code, $mobile, $user_name, $trans_id)
    {
        $ri = new RechargeInfoDetails;

        $api_url = RechargeInfo::getApiUrl($api_code, $net_code, $mobile);

       

        $res_content = BankRemitter::runURL($api_url, "OPERATOR_CHECK");

        $ri->trans_id = $trans_id;
        $ri->user_name = $user_name;
        $ri->rech_type = "OPERATOR_CHECK";
        $ri->rech_mobile = $mobile;

        $ri->save();



        return $res_content;

    }

    public static function checkEB($api_code, $net_code, $mobile, $user_name, $trans_id, $net_short)
    {
        $ri = new RechargeInfoDetails;

        $api_url = RechargeInfo::getApiUrl($api_code, $net_code, $mobile);

        $res_content = BankRemitter::runURL($api_url, "BILL_CHECK");

        $ri->trans_id = $trans_id;
        $ri->user_name = $user_name;
        $ri->rech_type = "BILL_CHECK";
        $ri->rech_mobile = $net_short;

        $ri->save();

        return $res_content;

    }

    public static function jsonFormatter($res_content)
    {
        $jx = str_replace("\\\\\"", "###dq###", $res_content);
        $jx = str_replace("\\", "", $jx);
        $jx = str_replace("###dq###", "\\\"", $jx);
        $jx = str_replace("\n", " ", $jx);

        return $jx;
    }


}
