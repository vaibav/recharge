<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\CyberBankApi;
use App\Libraries\BankRemitter;
use App\Libraries\CyberBankRemitter;
use App\Libraries\CyberBankReply;
use App\Libraries\PercentageCalculation;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserAccountDetails;
use App\Models\ApiUrlDetails;

use App\Models\UserBeneficiaryAccVerifyDetails;
use App\Models\UserAccountBalanceDetails;

class CyberBankBeneficiary
{

   /**
     * Add Remitter Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $lastName = "-";
        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        $msg = "";
        $isVerified = "-";

        $details = UserBankRemitterDetails::select('user_rem_lname')->where('rem_rep_opr_id', $data['remId'])->first();

        if($details) {
            $lastName = $details->user_rem_lname;
        }

        $data['lastName'] = $lastName;
        
        $benId    = rand(1000,9999);
        $remId    = $data['remId'];
        $dateTime = date("Y-m-d H:i:s");
        $rechMode = $data['rechMode'];
        $userName = $data['user_name'];
       

        // Check Username status
        $zy = CyberBankRemitter::checkUserModeandStatus($userName, $rechMode);
        if($zy == 2)
        {
            $z = 1;
            return array($z, $message, $res_content);
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
            return array($z, $message, $res_content);
        }

        // Do it.... 
        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("BEN_API_1");

        if($apiCode <= 0) {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $data);

        //echo $api_url;

        if($apiUrl == "NONE") {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $details = UserBeneficiaryDetails::select('ben_status')->where('user_name', $data['user_name'])
                                          ->where('msisdn', $data['mobileNo'])
                                          ->where('ben_acc_no', $data['benAccount'])->first();

        if($details)
        {
            $benStatus = $details->ben_status;

            if($benStatus == "PENDING") 
            {
                $resContent = BankRemitter::runURL($apiUrl, "BEN_ADD");

                list($status, $msg) = CyberBankReply::storeBeneficiaryResult($resContent, $benId);

                if($status == "FAILURE") {
                    $z = 4;
                }
                else  if($status == "PENDING") {
                    $z = 5;
                }

                return array($z, $msg, $resContent);
            }
            else if($benStatus == "SUCCESS") {
                return array($z, $msg, $resContent);
            } 
        }

        // Insert Record... 
        $ben_1->ben_id = $benId;
        $ben_1->api_trans_id = $data['api_trans_id'];
        $ben_1->user_name = $data['user_name'];
        $ben_1->agent_id = "0";
        $ben_1->msisdn = $data['mobileNo'];
        $ben_1->ben_acc_no = $data['benAccount'];
        $ben_1->ben_acc_name = $data['benName'];
        $ben_1->bank_code = $data['benBank'];
        $ben_1->bank_ifsc = $data['benIFSC'];
        $ben_1->api_code = $apiCode;
        $ben_1->ben_req_status = "done...";
        $ben_1->ben_status = "PENDING";
        $ben_1->ben_s_status = "0";
        $ben_1->trans_id = "0";
        $ben_1->ben_rep_opr_id = "";
        $ben_1->ben_rep_date = $dateTime;

        $v = $ben_1->save();

        if($v)
        {
            $resContent = BankRemitter::runURL($apiUrl, "BEN_ADD");

            list($status, $msg) = CyberBankReply::storeBeneficiaryResult($resContent, $benId);

            if($status == "FAILURE")
            {
                $z = 4;
            }
            else  if($status == "PENDING")
            {
                $z = 5;
            }

            return array($z, $msg, $resContent);
        }
        else {
            $z = 6;
        }

        return array($z, $msg, $resContent);


    }


    public static function delete($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $lastName = "-";
        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        $msg = "";
        $isVerified = "-";

        $benId    = $data['benId'];
        $remId    = $data['remId'];
        $mobileNo = $data['mobileNo'];
        $dateTime = date("Y-m-d H:i:s");
        $rechMode = $data['rechMode'];
        $userName = $data['user_name'];
       
        // Check Username status
        $zy = CyberBankRemitter::checkUserModeandStatus($userName, $rechMode);
        if($zy == 2)
        {
            $z = 1;
            return array($z, $message, $res_content);
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
            return array($z, $message, $res_content);
        }

        // Do it.... 
        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("BEN_D_API_1");

        if($apiCode <= 0) {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $data);

        //echo $api_url;

        if($apiUrl == "NONE") {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }


        $resContent = BankRemitter::runURL($apiUrl, "BEN_DELETE");

        list($status, $msg) = CyberBankReply::deleteBeneficiaryResult($resContent, $benId);

        if($status == "SUCCESS") {
            $z = 21;
        }

        return array($z, $msg, $resContent);

    }

    public static function deleteOtp($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $lastName = "-";
        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        $msg = "";
        $isVerified = "-";

        $benId    = $data['benId'];
        $remId    = $data['remId'];
        $mobileNo = $data['mobileNo'];
        $dateTime = date("Y-m-d H:i:s");
        $rechMode = $data['rechMode'];
        $userName = $data['user_name'];
       
        // Check Username status
        $zy = CyberBankRemitter::checkUserModeandStatus($userName, $rechMode);
        if($zy == 2)
        {
            $z = 1;
            return array($z, $message, $res_content);
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
            return array($z, $message, $res_content);
        }

        // Do it.... 
        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("BEN_D_API_2");

        if($apiCode <= 0) {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $data);

        //echo $api_url;

        if($apiUrl == "NONE") {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }


        $resContent = BankRemitter::runURL($apiUrl, "BEN_DELETE_OTP");

        list($status, $msg) = CyberBankReply::deleteBeneficiaryOtpResult($resContent, $benId);

        if($status == "SUCCESS") {
            $z = 22;
        }

        return array($z, $msg, $resContent);

    }

    public static function checkBeneficiary($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $lastName = "-";
        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        $msg = "";
        $isVerified = "-";

        
        $mobileNo = $data['mobileNo'];
        $dateTime = date("Y-m-d H:i:s");
        $rechMode = $data['rechMode'];
        $userName = $data['user_name'];
       
        // Check Username status
        $zy = CyberBankRemitter::checkUserModeandStatus($userName, $rechMode);
        if($zy == 2)
        {
            $z = 1;
            return array($z, $message, $res_content);
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
            return array($z, $message, $res_content);
        }

        // Do it.... 
        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("BEN_VERIFY_1");

        if($apiCode <= 0) {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $data);

        //echo $apiUrl;

        if($apiUrl == "NONE") {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content);
        }

        $surCharge = 5;
        $transId = rand(10000, 99999);

        $zx = self::reduceBalance($userName, $transId, $data, $surCharge, $apiCode, $rechMode);


        $resContent = BankRemitter::runURL($apiUrl, "BEN_VERIFY");

        list($status, $msg, $benName, $benStatus) = CyberBankReply::beneficiaryVerifyResult($resContent, $transId);

        if($status == "SUCCESS") {
            $z = 1;
        }
        if($status == "FAILURE") {
            $z = 2;
            $msg = "Not Verified";
            $cx = self::refundBalance($userName, $surCharge);
        }


        return array($z, $msg, $benName);

    }

      public static function reduceBalance($user_name, $trans_id, $data, $surp, $api_code, $r_mode)
    {
        $ben_2 = new UserBeneficiaryAccVerifyDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $date_time = date("Y-m-d H:i:s");

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($surp);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
            
           
            // Insert Record... 
            $ben_2->trans_id = $trans_id;
            $ben_2->api_trans_id = $data['api_trans_id'];
            $ben_2->user_name = $user_name;
            $ben_2->agent_id =  "0";
            $ben_2->msisdn = $data['mobileNo'];
            $ben_2->ben_acc_no = $data['benAccount'];
            $ben_2->ben_code = $data['benBank'];
            $ben_2->ben_surplus = $surp;
            $ben_2->user_bal = $net_bal;
            $ben_2->ben_date = $date_time;
            $ben_2->api_code = $api_code;
            $ben_2->ben_mode = $r_mode;
            $ben_2->ben_status = "PENDING";
            $ben_2->ben_rep_opr_id = "";

            $ben_2->save();

            $zx = 1;
            
        }

        return $zx;
    }

    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }
}
