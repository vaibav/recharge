<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\BonrixApiResultDetails;

use App\Models\UserRechargeNewDetails; 
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\ServerDetails;

class RechargeNewReMobile
{

    /**
     * Add RechargeBill Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_code = 0;
        $api_url = "NONE";
        $api_data = [];
        $api_method = "GET";
        $r_mode = "WEB";
        $rech_type = "WEB_RECHARGE";
        $b_url = "";
        $res_content = "NONE";
        $resx = new stdClass();
        $rech1 = [];

        // Modals
        $bon_1 = new BonrixApiResultDetails;
        $rech_n_2 = new UserRechargeNewStatusDetails;
       
        // Post Data
        $trans_id = $data['trans_id'];
        $net_type_code = $data['net_type_code'];
        $net_code = $data['net_code'];
        $user_mobile = $data['user_mobile'];
        $user_amount = $data['user_amount'];
        $api_trans_id = $data['api_trans_id'];
        $r_mode = $data['mode'];
        $rech_type = $data['rech_type'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");

        


        
        // Get Network Line
        
        if ($r_mode == "API") {
            list($api_code, $v) = ApiUrl::getApiNetworkLine($net_code, $user_amount, $user_name);
        }
        else
        {
            list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        }
        
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 3;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 4;     // Account is Inactivated...
        }
       


        // Check Mobile Status
        $zy = self::checkAccountNoStatus($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy != 6)
        {
            $z = 6;     // Recharge Status is already Not Pending for this Account No
        }

        // Check 10 Minutes Status
        $zy =self::checkTenMinsCheck($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy > 0)
        {
            $z = 7;
        }

        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 8;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            

            list($api_url, $api_data, $api_method) = ApiUrl::generateAPICommon($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
            
            if ($api_url != "NONE")
            {
                $res_code = 200;

                $rech_web = "";
                $res_content = "";
                if (strpos($api_url, 'ssh1.bonrix.in') !== false) 
                {
                    // Bonrix URL
                    $bon_1->trans_id = $trans_id;
                    $bon_1->result_url = $api_url;
                    $bon_1->result_status = "1";
                    $bon_1->save();

                    $rech_web = "BROWSER";
                    $rech1['rech_web'] = "BROWSER";
                    $rech1['rech_req_status'] = "done";

                }
                else
                {
                    $client = new \GuzzleHttp\Client();

                    if($api_method == "POST")
                    {
                        $params['form_params'] = $api_data;
                        $res = $client->post($api_url, $params);
                        $res_content = $res->getBody();
                    }
                    else if($api_method == "GET")
                    {
                        
                        $res = $client->get($api_url);
                        $res_code = $res->getStatusCode();
                        $res_content = $res->getBody();
                    }

                    $rech_web = "CURL";
                    $rech1['rech_web'] = "CURL";
                    $rech1['rech_req_status'] = $res_content;
                }

                $rech_n_2->where('trans_id', '=', $trans_id)->update(['rech_req_status' => $res_content, 'rech_web' => $rech_web]);


                // Reply Update
                InstantReplyUpdate::instantUpdate($res_content, "SUCCESS", url('/')."/pendingreport_success_1/".$trans_id);
                InstantReplyUpdate::instantUpdate($res_content, "success", url('/')."/pendingreport_success_1/".$trans_id);
                InstantReplyUpdate::instantUpdate($res_content, "FAILED", url('/')."/pendingreport_failure_1/".$trans_id);
                InstantReplyUpdate::instantUpdate($res_content, "Failed", url('/')."/pendingreport_failure_1/".$trans_id);
                InstantReplyUpdate::instantUpdate($res_content, "FAILURE", url('/')."/pendingreport_failure_1/".$trans_id);
                InstantReplyUpdate::instantUpdate($res_content, "failure", url('/')."/pendingreport_failure_1/".$trans_id);

                
            }

            // All Process Over...
          
        }

        return $z;
        
    }



    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    

    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeNewDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['rech_mobile', '=', $mobile], 
                                ['rech_amount', '=', $amount], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public static function checkTenMinsCheck($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeNewDetails;
        
        $z = 0;

        $d1 = $rech_1->select('created_at')->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'SUCCESS']])->orderBy('id', 'desc')->limit(1)->get();
        if($d1->count() > 0)
        {
            $old_d = $d1[0]->created_at;
            $cur_d = date("Y-m-d H:i:s");

            $o_d = new DateTime($old_d);
            $c_d = new DateTime($cur_d);

            $dif = $o_d->diff($c_d);

            $minutes = $dif->days * 24 * 60;
            $minutes += $dif->h * 60;
            $minutes += $dif->i;

            if($minutes < 10)
                $z = 1;
        }

        return $z;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }


    
}
