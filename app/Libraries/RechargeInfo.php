<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\OfferLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;
use App\Models\RechargeInfoDetails;

class RechargeInfo
{
    //
    public static function getInfoNetworkLine($net_code, $info_type)
    {
        $off_1 = new OfferLineDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Get Details
        $d1 = $off_1->where([['net_code', '=', $net_code], ['offer_type', '=', $info_type], ['offer_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
       
        return array($api_code, $v);

    }


    public static function generateInfoAPI($api_code, $net_code, $mobile)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;

            //echo "URL:".$url_link;

            if($api_code == "989")
            {
                if (strpos($url_link, '<mobileno>') !== false) {
                    $url_link = str_replace("<mobileno>", $mobile, $url_link);
                }
            }
            else
            {
                if (strpos($url_link, '<mobileno>') !== false) {
                    $url_link = str_replace("<mobileno>", $mobile, $url_link);
                }
                else
                {
                     $d3 = $api_3->select('api_net_short_code')->where([['api_code', '=', $api_code], ['net_code', '=', $net_code]])->get();
                    //echo $api_code."---".$net_code."<br>";
                    //print "<pre>";
                    //print_r($d3);
                    if($d3->count() > 0)
                    {
                        $api_net_code = $d3[0]->api_net_short_code;

                         // Get Paramenters
                        $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
                        if($d2->count() > 0)
                        {
                            foreach($d2 as $d)
                            {
                                if ($d->api_para_type == "PRE_DEFINED")
                                {
                                    $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                                }
                                else if ($d->api_para_type == "MOBILE_NO")
                                {
                                    $url_link = $url_link.$d->api_para_name."=".$mobile."&";
                                }
                                else if ($d->api_para_type == "NETWORK")
                                {
                                    $url_link = $url_link.$d->api_para_name."=".$api_net_code."&";
                                }
                            }

                            $url_link = rtrim($url_link, "& ");
                        }
                        else
                        {
                            $zx = 1;
                        }
                    }
                    else
                    {
                        $zx = 1;
                    }
                }
                //$url_link = $url_link."?";

                //Get Network Code Details
               
            }
            

          

            

            //echo "URL link-------->".$url_link."---Mobile:".$mobile."<br>";

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            
        }

        return $url;
    }

    public static function getNewNetworkLine($netCode, $infoType)
    {
        
        // Get Details
        $offerDetails = OfferLineDetails::where([['net_code', '=', $netCode], ['offer_type', '=', $infoType], ['offer_line_status', '=', 1]])->first();

        if($offerDetails) {
            return $offerDetails->api_code;
        }
        
        return 0;

    }

    public static function getApiUrl($apiCode, $netCode, $mobileNo)
    {
        $apiUrl = 0;

        $operatorDetails = ApiProviderNetworkDetails::select('api_net_short_code')
                                                    ->where([['api_code', '=', $apiCode], ['net_code', '=', $netCode]])->first();

        if($operatorDetails) {
            $operator = $operatorDetails->api_net_short_code;
        }

        $apiDetails = ApiProviderDetails::select('api_url')->where([['api_code', '=', $apiCode], ['api_status', '=', 1]])->first();
        if($apiDetails)
        {
            $apiUrl = $apiDetails->api_url;

            if (strpos($apiUrl, '<mobileno>') !== false) {
                $apiUrl = str_replace("<mobileno>", $mobileNo, $apiUrl);
            }

            if (strpos($apiUrl, '<opr>') !== false) {
                $apiUrl = str_replace("<opr>", $operator, $apiUrl);
            }
        }
        

        return $apiUrl;
    }

    public static function storeOffers($apiUrl, $storeType, $userName, $netShort)
    {
        $ri = new RechargeInfoDetails;

        $resContent = BankRemitter::runURL($apiUrl, $storeType);

        $ri->trans_id    = rand(10000, 99999);
        $ri->user_name   = $userName;
        $ri->rech_type   = $storeType;
        $ri->rech_mobile = $netShort;

        $ri->save();

        return $resContent;
    }


    public static function getResult($userName, $connectNo, $netCode, $operatorType)
    {
        $apiCode = RechargeInfo::getNewNetworkLine($netCode, $operatorType);

        if($apiCode == 0) {
            return "0";
        }

        $apiUrl     = RechargeInfo::getApiUrl($apiCode, $netCode, $connectNo);
        $resContent = RechargeInfo::storeOffers($apiUrl, $operatorType, $userName, "NA");

        return $resContent;
    }

    public static function dthPlans($resultData)
    {
         $resultPlans = array();

         if (array_key_exists('records', $resultData)) {
            $data = (array)$resultData['records'];

             if(array_key_exists('Plan', $data)){
                $plans = (array)$data['Plan'];

                foreach($plans as $plan)
                {
                    $planArray = array();
                    foreach($plan as $key => $value)
                    {
                        if(!is_object($value))
                        {
                            $planArray[$key] = $value;
                        }
                        else
                        {
                            $amt = "";
                            $val = (array)$value;
                            foreach($val as $k => $v) {
                                $amt .= $k. "-".$v."---";
                            }

                            $planArray[$key] = $amt;
                        }
                    }

                    array_push($resultPlans, $planArray);
                }
            }

        }

        return $resultPlans;
    }

    public static function dthCustomerInfo($resultData)
    {
         $resultPlans = array();

         if (array_key_exists('records', $resultData)) {
            $data = (array)$resultData['records'];

            foreach($data as $plan)
            {
                $planArray = array();
                foreach($plan as $key => $value)
                {
                    if(!is_object($value))
                    {
                        $planArray[$key] = $value;
                    }
                    
                }

                array_push($resultPlans, $planArray);
            }

        }

        return $resultPlans;
    }

}
