<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankTransferInfo;
use App\Libraries\ParentNewSuccess;
use App\Libraries\ParentNewFailure;

use App\Models\UserRechargeNewDetails; 
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\UserAccountBalanceDetails;

class RechargeNewReply
{

    public static function updateSuccess($trans_id, $opr_id, $status, $date_time)
    {
        $rech_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeNewStatusDetails;
       
        $zx = 0;

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->count();
        if($d1 > 0)
        {
            
            // Update Data
            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                ->update(['rech_status' => $status, 'updated_at' => $date_time]);
            $zx++;
        }

        $d2 = $rech_2->where('trans_id', '=', $trans_id)->count();
        if($d2 > 0)
        {
            // Update Data
            $rech_2->where('trans_id', '=', $trans_id)
                                ->update(['reply_opr_id' => $opr_id, 'reply_date' => $date_time, 'updated_at' => $date_time]);
            $zx++;
        }

        if($zx > 0)
        {
            $zx = ParentNewSuccess::updateParentPaymentSuccess($trans_id, $status, $date_time);
        }

        return $zx;

    }

    public static function updateFailure($trans_id, $opr_id, $status, $date_time)
	{
		$rech_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeNewStatusDetails;

        
        $net_2 = new NetworkDetails;
        
        
        $zx = 0;
        $rech_type = "NONE";

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->rech_total;
            $user_name = $d1[0]->user_name;
            $r_mode = $d1[0]->rech_mode;
            $api_trans_id = $d1[0]->api_trans_id;

            if($r_mode == "API") {
                $rech_type = "API_RECHARGE";
            }
            else {
                $rech_type = "WEB_RECHARGE";
            }

            $net_bal = self::updateUserAccountBalance($user_name, $rech_total);

            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                        ->update(['rech_option' => '2', 'updated_at' => $date_time]);

            // Array
            $rc = [];
            $rc['trans_id'] = $d1[0]->trans_id;
            $rc['api_trans_id'] = $d1[0]->api_trans_id;
            $rc['user_name'] = $user_name;
            $rc['net_code'] = $d1[0]->net_code;
            $rc['rech_mobile'] = $d1[0]->rech_mobile;
            $rc['rech_amount'] = $d1[0]->rech_amount;
            $rc['rech_net_per'] = $d1[0]->rech_net_per;
            $rc['rech_net_per_amt'] = $d1[0]->rech_net_per_amt;
            $rc['rech_net_surp'] = $d1[0]->rech_net_surp;
            $rc['rech_total'] = $rech_total;
            $rc['user_balance'] = $net_bal;
            $rc['rech_date'] = $date_time;
            $rc['rech_status'] = $status;
            $rc['rech_mode'] = $d1[0]->rech_mode;
            $rc['rech_option'] = "2";
            $rc['created_at'] = $date_time;
            $rc['updated_at'] = $date_time;


            // Update Data
            $rech_1->insert($rc);
            $zx++;

            // Reply Update
            $dc2 = $rech_2->where('trans_id', '=', $trans_id)->count();
            if($dc2 > 0)
            {
                // Update Data
                $rech_2->where('trans_id', '=', $trans_id)
                                    ->update(['reply_opr_id' => $opr_id, 'reply_date' => $date_time, 'updated_at' => $date_time]);
                $zx++;
            }

            // Parent Update
            $zx = intval($zx) + ParentNewFailure::updateParentPaymentFailure($trans_id, $status, $date_time, $rech_type);
           
            if ($r_mode == "API")
            {
                //$res_code = ApiUrl::sendResultApipartner($user_name, $api_trans_id, $opr_id, $status, 2);
            }

            $zt = self::insertTransaction1($trans_id, $user_name, $rech_type);

            
        }
 
        return $zx;
       
    }


    public static function updateUserAccountBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;

        $net_bal = 0;

        $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d2->count() > 0)
        {
            $u_bal = $d2[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = self::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance'=>$net_bal]);

        }

        return $net_bal;

    }

    public static function insertTransaction1($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public static function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }
}
