<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\TransactionAllParentDetails;

class ParentFailure
{
       
    public static function updateWebParentPaymentFailure($trans_id, $status, $date_time, $trans_mode)
     {
         $rech_3 = new UserRechargePaymentParentDetails;
         $uacc_1 = new UserAccountBalanceDetails;
         $zx = 0;
 
         $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
         if($dx3->count() > 0)
         {
             $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
             
             if($dx4 == 0)
             {
                 foreach($dx3 as $r)
                 {
                     // Start Failure
                      // Update Data
                     $rech_total = $r->rech_total;
                     $user_name1 = $r->user_name;
                     $parent_name = $r->parent_name;
                     $super_parent_name = $r->super_parent_name;
 
                     if($super_parent_name == "NONE")
                     {
                         $user_name = $parent_name;
                     }
                     else
                     {
                         $user_name = $super_parent_name;
                     }
 
                     $net_bal = 0;
                     // Update Failure
                     $d5 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                     if($d5->count() > 0)
                     {
                         $u_bal = $d5[0]->user_balance;
 
                         $net_bal = floatval($u_bal) + floatval($rech_total);
                         $net_bal = round($net_bal, 2);
                         $net_bal = self::convertNumberFormat($net_bal);
 
                         // Update Balance
                         $uacc_1->where('user_name', '=', $user_name)
                                             ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                         
                         // Insert Payment Data
                         $pay_1 = array('trans_id' => $r->trans_id, 
                                             'super_parent_name' => $r->super_parent_name,
                                             'parent_name' => $r->parent_name,
                                             'user_name' => $r->user_name, 
                                             'net_code' => $r->net_code,
                                             'rech_mobile' => $r->rech_mobile, 
                                             'rech_amount' => $r->rech_amount, 
                                             'rech_net_per' => $r->rech_net_per, 
                                             'rech_net_per_amt' => $r->rech_net_per_amt,
                                             'rech_net_surp' => $r->rech_net_surp, 
                                             'rech_total' => $r->rech_total, 
                                             'user_balance' => $net_bal,
                                             'rech_date' => $date_time,
                                             'rech_status' => $status,
                                             'rech_type' => $r->rech_type,
                                             'created_at' => $date_time, 'updated_at' => $date_time);
                         
                         if($rech_3->insert($pay_1))
                         {
                             $zxt = self::insertTransactionParent($trans_id, $user_name, $trans_mode);
                             $zx =1;
                         }
                         
                     }
                     // End Failure For
                 }
                
 
                 // End Failure
             }
             
         }

         return $zx;

    }


    public static function updateApiParentPaymentFailure($trans_id, $status, $date_time, $trans_mode)
    {
        $rech_3 = new UserRechargePaymentParentApipartnerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;

        $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
        if($dx3->count() > 0)
        {
            $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($dx4 == 0)
            {
                foreach($dx3 as $r)
                {
                    // Start Failure
                     // Update Data
                    $rech_total = $r->rech_total;
                    $user_name1 = $r->user_name;
                    $parent_name = $r->parent_name;
                    $super_parent_name = $r->super_parent_name;

                    if($super_parent_name == "NONE")
                    {
                        $user_name = $parent_name;
                    }
                    else
                    {
                        $user_name = $super_parent_name;
                    }

                    // Update Failure
                    $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    if($d1->count() > 0)
                    {
                        $u_bal = $d1[0]->user_balance;

                        $net_bal = floatval($u_bal) + floatval($rech_total);
                        $net_bal = round($net_bal, 2);
                        $net_bal = self::convertNumberFormat($net_bal);

                        // Update Balance
                        $uacc_1->where('user_name', '=', $user_name)
                                            ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                        
                        // Insert Payment Data
                        $pay_1 = array('trans_id' => $r->trans_id,
                                            'api_trans_id' => $r->api_trans_id, 
                                            'super_parent_name' => $r->super_parent_name,
                                            'parent_name' => $r->parent_name,
                                            'user_name' => $r->user_name, 
                                            'net_code' => $r->net_code,
                                            'rech_mobile' => $r->rech_mobile, 
                                            'rech_amount' => $r->rech_amount, 
                                            'rech_net_per' => $r->rech_net_per, 
                                            'rech_net_per_amt' => $r->rech_net_per_amt,
                                            'rech_net_surp' => $r->rech_net_surp, 
                                            'rech_total' => $r->rech_total, 
                                            'user_balance' => $net_bal,
                                            'rech_date' => $date_time,
                                            'rech_status' => $status,
                                            'rech_type' => $r->rech_type,
                                            'created_at' => $date_time, 'updated_at' => $date_time);
                        
                        if($rech_3->insert($pay_1))
                        {
                            $zxt = self::insertTransactionParentAPI($trans_id, $user_name, $trans_mode);
                            $zx =1;
                        }
                        
                    }
                    // End Failure For
                }
               

                // End Failure
            }
            
        }

        return $zx;
    }

    public static function insertTransactionParent($trans_id, $user_name, $trans_mode)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $trans_mode;
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public static function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }

}
