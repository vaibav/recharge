<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\OfferLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

class CyberBankApi
{
    //
    public static function getAgent1NetworkLine($info_type)
    {
        $off_1 = new OfferLineDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Get Details
        $d1 = $off_1->where([['offer_type', '=', $info_type], ['offer_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
       
        return array($api_code, $v);

    }

    public static function generateBankAPI($api_code, $data)
    {
        
        $url = "NONE";

        $d1 = ApiProviderDetails::select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url = $d1[0]->api_url;
            
            if (strpos($url, '<mobileno>') !== false) {
                $url = str_replace("<mobileno>", $data['mobileNo'], $url);
            }

            if (strpos($url, '<firstname>') !== false) {
                $url = str_replace("<firstname>", $data['user_name'], $url);
            }

            if (strpos($url, '<lastname>') !== false) {
                $url = str_replace("<lastname>", $data['user_lname'], $url);
            }

            if (strpos($url, '<pincode>') !== false) {
                $url = str_replace("<pincode>", $data['user_pincode'], $url);
            }

            if (strpos($url, '<remid>') !== false) {
                $url = str_replace("<remid>", $data['remId'], $url);
            }

            if (strpos($url, '<otpnumber>') !== false) {
                $url = str_replace("<otpnumber>", $data['otpNumber'], $url);
            }

            if (strpos($url, '<mode>') !== false) {
                $url = str_replace("<mode>", $data['routingType'], $url);
            }

            if (strpos($url, '<benid>') !== false) {
                $url = str_replace("<benid>", $data['benId'], $url);
            }

            if (strpos($url, '<amount>') !== false) {
                $url = str_replace("<amount>", $data['amount'], $url);
            }

            if (strpos($url, '<benaccount>') !== false) {
                $url = str_replace("<benaccount>", $data['benAccount'], $url);
            }

            if (strpos($url, '<benifsc>') !== false) {
                $url = str_replace("<benifsc>", $data['benIFSC'], $url);
            }

            if (strpos($url, '<fname>') !== false) {
                $url = str_replace("<fname>", $data['firstName'], $url);
            }

            if (strpos($url, '<lname>') !== false) {
                $url = str_replace("<lname>", $data['lastName'], $url);
            }

            if (strpos($url, '<otc>') !== false) {
                $url = str_replace("<otc>", $data['otc'], $url);
            }

            if (strpos($url, '<address>') !== false) {
                $url = str_replace("<address>", $data['user_address'], $url);
            }

            if (strpos($url, '<transid1>') !== false) {
                $url = str_replace("<transid1>", $data['trans_id'], $url);
            }

            if (strpos($url, '<city>') !== false) {
                $url = str_replace("<city>", $data['user_pincode'], $url);
            }

            if (strpos($url, '<otp>') !== false) {
                $url = str_replace("<otp>", $data['otpNumber'], $url);
            }

            if (strpos($url, '<transid>') !== false) {
                $url = str_replace("<transid>", $data['clientTransId'], $url);
            }

            if (strpos($url, '<accountNo>') !== false) {
                $url = str_replace("<accountNo>", $data['accountNo'], $url);
            }

            if (strpos($url, '<benId>') !== false) {
                $url = str_replace("<benId>", $data['benId'], $url);
            }

            if (strpos($url, '<bankIFSC>') !== false) {
                $url = str_replace("<bankIFSC>", $data['bankIFSC'], $url);
            }

            if (strpos($url, '<customerName>') !== false) {
                $url = str_replace("<customerName>", $data['customerName'], $url);
            }

            if (strpos($url, '<transferMode>') !== false) {
                $url = str_replace("<transferMode>", $data['routingType'], $url);
            }

            if (strpos($url, '<transferMode>') !== false) {
                $url = str_replace("<transferMode>", $data['routingType'], $url);
            }

            if (strpos($url, '<transferAmount>') !== false) {
                $url = str_replace("<transferAmount>", $data['amount'], $url);
            }

            if (strpos($url, '<remitterFirstName>') !== false) {
                $url = str_replace("<remitterFirstName>", $data['remitterFirstName'], $url);
            }

            if (strpos($url, '<remitterAddress>') !== false) {
                $url = str_replace("<remitterAddress>", $data['remitterAddress'], $url);
            }

            if (strpos($url, '<remitterCity>') !== false) {
                $url = str_replace("<remitterCity>", $data['remitterCity'], $url);
            }

            if (strpos($url, '<remitterPincode>') !== false) {
                $url = str_replace("<remitterPincode>", $data['remitterPincode'], $url);
            }

            if (strpos($url, '<mobileNo>') !== false) {
                $url = str_replace("<mobileNo>", $data['mobileNo'], $url);
            }

            if (strpos($url, '<remId>') !== false) {
                $url = str_replace("<remId>", $data['remId'], $url);
            }

            if (strpos($url, '<benFirstName>') !== false) {
                $url = str_replace("<benFirstName>", $data['firstName'], $url);
            }

            if (strpos($url, '<benAccountNo>') !== false) {
                $url = str_replace("<benAccountNo>", $data['benAccount'], $url);
            }

            if (strpos($url, '<benAccountName>') !== false) {
                $url = str_replace("<benAccountName>", $data['benName'], $url);
            }

            if (strpos($url, '<benBankIFSC>') !== false) {
                $url = str_replace("<benBankIFSC>", $data['benIFSC'], $url);
            }

            if (strpos($url, '<bankCode>') !== false) {
                $url = str_replace("<bankCode>", $data['benBank'], $url);
            }


        }

        return $url;
    }
}