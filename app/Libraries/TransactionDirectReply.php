<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\AllTransaction;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\ApiProviderResultDetails;

use App\Libraries\BankRemitter;
use App\Libraries\PercentageCalculation;

class TransactionDirectReply
{
    //
    
     /**
     * Reply Update Functions..............................................
     *
     */
    public static function instantUpdate($res_content, $trans_id, $api_code)
    {
        list($status, $opr_name) = self::getApiResult($api_code, $res_content);

        file_put_contents(base_path().'/public/sample/direct_reply.txt', $status."--".$opr_name.PHP_EOL, FILE_APPEND);

        if($status != "PENDING")
        {
            $res_content = strip_tags($res_content);

            $oprtr_id = self::getJsonOperatorId($res_content, $opr_name);

            if($status == "SUCCESS")
            {
                self::successUpdate($trans_id, $oprtr_id);
            }
            else if($status == "FAILURE")
            {
                self::failureUpdate($trans_id, $oprtr_id);
            }
        }

    }


    public static function getApiResult($api_code, $res_content)
    {
        $status = "PENDING";
        $opr_name = "txid";

        $d1 = ApiProviderResultDetails::select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')
                       ->where('api_code', '=', $api_code)->get();
        
        foreach($d1 as $d)
        {
            if($d->api_res_para_name == "STATUS 1")
            {
                if (strpos($res_content, $d->api_res_para_value) !== false) {
                    $status = "SUCCESS";
                    
                }
            }

            if($d->api_res_para_name == "STATUS 2")
            {
                if (strpos($res_content, $d->api_res_para_value) !== false) {
                    $status = "FAILURE";
                    
                }
            }

            if($d->api_res_para_name == "OPERATOR_ID") {
                $opr_name = $d->api_res_para_field;
            }
        }

        return array($status, $opr_name);
    }

    public static function getJsonOperatorId($res_content, $opr_name)
    {
        $oprtr_id = "NA";
        $result1 = "";

        $c = self::json_validate($res_content);

        if($c == 1)
        {
            $result = self::resultFormatter($res_content);
            $result1 = $result;

            $result = (array)json_decode($result);

            file_put_contents(base_path().'/public/sample/req_2.txt', print_r($result,true), FILE_APPEND);

            foreach($result as $key => $value)
            {
                if($key == $opr_name)
                {
                    $oprtr_id = $value;
                    break;
                }
            }

            if($oprtr_id == "NA") {
                $oprtr_id = self::getAgainJsonOperatorId($result1, $opr_name);
            }
        }


        return $oprtr_id;
    }

    public static function getAgainJsonOperatorId($result, $con)
    {
        $oprtr_id = "NA";
       
        $result = ltrim($result, '{');
        $result = rtrim($result, '}');
        $result = str_replace("{", ",", $result);
        $a = explode(",", $result);
    
        foreach($a as $d)
        {
            $d1 = [];
            $d1 = explode(":", $d);
            if(sizeof($d1) >= 2)
            {
                $d1[0] = str_replace('"', '', $d1[0]);
                $d1[1] = str_replace('"', '', $d1[1]);
    
                if($d1[0] == $con) {
                    $oprtr_id = $d1[1];
                }
            }
        }
        
        return $oprtr_id;
    }

    public static function successUpdate($trans_id, $opr_id)
    {
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        $mode = "WEB";

        $d1 = AllTransaction::select('user_name', 'api_trans_id')->where([['trans_id', '=', $trans_id], 
                                            ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }
            
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            $zx = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                    ->update(['rech_status' => 'SUCCESS', 
                                                'rech_option' => '1', 
                                                'reply_date' => $date_time,
                                                'reply_id' => $opr_id,
                                                'updated_at' => $date_time
                                            ]);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_s_url, '<trid>') !== false) {
                        $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<oprid>') !== false) {
                        $api_s_url = str_replace("<oprid>", $opr_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<status>') !== false) {
                        $api_s_url = str_replace("<status>", "SUCCESS", $api_s_url);
                    }

                    if($api_s_url != "")
                    {
                        BankRemitter::runURL($api_s_url, "RESPONSE");
                    }
                }

                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }

    }

    public static function failureUpdate($trans_id, $opr_id)
    {
     
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        $mode = "WEB";

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            $zx = self::insertFailure($trans_id, $opr_id, $date_time);

            $zy = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                    ->update(['rech_option' => '2',  'updated_at' => $date_time ]);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", "FAILURE", $api_f_url);
                    }

                    if($api_f_url != "")
                    {
                        BankRemitter::runURL($api_f_url, "RESPONSE");
                    }
                }

                $z = 0;
                $op = "Failure Update is executed successfully...";
            }

        }

        return $z;

    }

    public static function insertFailure($trans_id, $opr_id, $date_time)
    {
        $rech = [];
        $c1 = 0;

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], 
                                            ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            // Array
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $d1[0]->api_trans_id;

            $rech['parent_name'] = $d1[0]->parent_name;
            $rech['child_name'] = $d1[0]->child_name;
            $rech['user_name'] =  $d1[0]->user_name;

            $rech['net_code'] =  $d1[0]->net_code;
            $rech['rech_mobile'] = $d1[0]->rech_mobile;
            $rech['rech_amount'] = $d1[0]->rech_amount;
            $rech['rech_bank'] = "-";
            $rech['ret_net_per'] = $d1[0]->ret_net_per;
            $rech['ret_net_surp'] = $d1[0]->ret_net_surp;
            $rech['ret_total'] = $d1[0]->ret_total;
            $rech['ret_bal'] = '0';

            $rech['dis_net_per'] = $d1[0]->dis_net_per;
            $rech['dis_net_surp'] = $d1[0]->dis_net_surp;
            $rech['dis_total'] = $d1[0]->dis_total;
            $rech['dis_bal'] = '0';

            $rech['sup_net_per'] = $d1[0]->sup_net_per;
            $rech['sup_net_surp'] = $d1[0]->sup_net_surp;
            $rech['sup_total'] = $d1[0]->sup_total;
            $rech['sup_bal'] = '0';

            $rech['trans_type'] = "RECHARGE";
            $rech['api_code'] = $d1[0]->api_code;
            $rech['rech_date'] = $d1[0]->rech_date;
            $rech['rech_status'] = "FAILURE";
            $rech['rech_option'] = "2";
            $rech['reply_date'] = $date_time;
            $rech['reply_id'] = $opr_id;
            
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;

            list($rx_bal, $zx) = self::update_balance($d1[0]->user_name, $d1[0]->ret_total, $date_time);

            if($zx > 0)
            {
                $rech['ret_bal'] = $rx_bal;

                if($d1[0]->child_name != "NONE")
                {
                    if($d1[0]->dis_total != "0.00" && $d1[0]->dis_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->child_name, $d1[0]->dis_total, $date_time);
                        $rech['dis_bal'] = $dx_bal;
                    }
                }
                if($d1[0]->sup_total != "0.00" && $d1[0]->sup_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->parent_name, $d1[0]->sup_total, $date_time);
                        $rech['sup_bal'] = $dx_bal;
                }

                $c1 = AllTransaction::insert($rech);
               
            }
        }

        return $c1;
    }

    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }

    public static function getApiResultType($api_code)
    {
        $type = "INDIRECT";

        $d1 = ApiProviderResultDetails::select('api_result_type')->where('api_code', '=', $api_code)->get();
        foreach($d1 as $d)
        {
            $type = $d->api_result_type;
        }

        return $type;
    }

    public static function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }

    public static function convert_text($text) {

        $t = $text;
        
        $specChars = array(
            '!' => '%21',    '"' => '%22',
            '#' => '%23',    '$' => '%24',    '%' => '%25',
            '&' => '%26',    '\'' => '%27',   '(' => '%28',
            ')' => '%29',    '*' => '%2A',    '+' => '%2B',
            ',' => '%2C',    '-' => '%2D',    '.' => '%2E',
            '/' => '%2F',    ':' => '%3A',    ';' => '%3B',
            '<' => '%3C',    '=' => '%3D',    '>' => '%3E',
            '?' => '%3F',    '@' => '%40',    '[' => '%5B',
            '\\' => '%5C',   ']' => '%5D',    '^' => '%5E',
            '_' => '%5F',    '`' => '%60',    '{' => '%7B',
            '|' => '%7C',    '}' => '%7D',    '~' => '%7E',
            ',' => '%20',  ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }
    
    public static function json_validate($string)
    {
        // decode the JSON data
        $result = json_decode($string);
    
        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }
    
        if ($error !== '') {
            // throw the Exception or exit // or whatever :)
            return 2;
        }
    
        // everything is OK
        return 1;
    }
}
