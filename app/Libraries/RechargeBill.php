<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\ServerDetails;

class RechargeBill
{

    /**
     * Add RechargeBill Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_url = "NONE";
        $b_url = "";
        $res_content = "NONE";
        $resx = new stdClass();

        // Modals
        $rech_5 = new UserRechargeBillDetails;
       
        // Post Data
        $r_mode = $data['mode'];

        if($r_mode == "WEB") {
            $cat = "VBER";
        }
        else if($r_mode == "API") {
            $cat = "VBEA";
        }
        else if($r_mode == "GPRS") {
            $cat = "VBEG";
            $r_mode = "WEB";
        }
        else if($r_mode == "SMS") {
            $cat = "VBES";
            $r_mode = "WEB";
        }

        $trans_id = $cat.rand(10000000,99999999);
        $net_code = $data['net_code'];
        $cons_no = $data['cons_no'];
        $cons_name = $data['cons_name'];
        $cons_mobile = $data['cons_mobile'];
        $cons_amount = $data['cons_amount'];
        $cons_duedate = $data['cons_duedate'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");


        // 1.Check User Balance
        if(floatval($cons_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 3;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 4;     // Account Balance is low from Setup Fee...
        }


        // Check Mobile Status
        $zy = self::checkAccountNoStatus($user_name, $cons_no);
        if ($zy == 6)
        {
            $z = 5;     // Recharge Status is already Pending for this Account No
        }

        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            // Do Recharge.... 
            list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $cons_amount);
            if($api_code > 0)
            {
                $api_url = ApiUrl::generateAPI($api_code, $net_code, $cons_no, $cons_amount, $trans_id);

                if($api_url != "NONE")
                    $res_code = 200;
            }


            // Insertion Process..
            list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $cons_amount, $r_mode);

            // Array
            $rech = [];
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $api_trans_id;
            $rech['user_name'] = $user_name;
            $rech['net_code'] = $net_code;
            $rech['con_name'] = $cons_name;
            $rech['con_acc_no'] = $cons_no;
            $rech['con_mobile'] = $cons_mobile;
            $rech['con_amount'] = $cons_amount;
            $rech['con_due_date'] = $cons_duedate;
            $rech['con_net_per'] = $per;
            $rech['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt);
            $rech['con_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
            $rech['con_total'] = PercentageCalculation::convertNumberFormat($netamt);
            $rech['user_balance'] = 0;
            $rech['con_req_status'] = "";
            $rech['con_status'] = "PENDING";
            $rech['con_mode'] = $r_mode;
            $rech['con_option'] = "1";
            $rech['api_code'] = $api_code;
            $rech['reply_opr_id'] = "";
            $rech['reply_date'] = $date_time;
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;

            $zx = self::insertRechargePayment($rech);
            if($zx == 1)
            {

                // Parent Network Percentage Calculation
                list($parent_type, $parent_name) = self::getParent($user_name);
                list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $cons_amount);

                $rech_p2 = [];
                $rech_p2['trans_id'] = $trans_id;
                $rech_p2['api_trans_id'] = $api_trans_id;
                $rech_p2['parent_name'] = $parent_name;
                $rech_p2['child_name'] = "NONE";
                $rech_p2['user_name'] = $user_name;
                $rech_p2['net_code'] = $net_code;
                $rech_p2['rech_mobile'] = $cons_no;
                $rech_p2['rech_amount'] = $cons_amount;
                $rech_p2['rech_net_per'] = $per_d;
                $rech_p2['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_d);
                $rech_p2['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech_p2['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech_p2['user_balance'] = 0;
                $rech_p2['rech_date'] = $date_time;
                $rech_p2['rech_status'] = "PENDING";
                $rech_p2['rech_type'] = "BILL_PAYMENT";
                $rech_p2['rech_mode'] = $r_mode;
                $rech_p2['rech_option'] = "0";
                $rech_p2['created_at'] = $date_time;
                $rech_p2['updated_at'] = $date_time;

                

                $zp2 = self::insertRechargePaymentDistributor($rech_p2, $rech_p2['parent_name'], $r_mode);
                $zp22 = self::insertTransactionParent($trans_id, $rech_p2['parent_name']);


                 // Super Parent Network Percentage Calculation
                 list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                 if($parent_name_s != "NONE")
                 {
                     list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $cons_amount);
                 
                     // Super Distributor Payment Details
                     $rech_p3 = [];
                     $rech_p3['trans_id'] = $trans_id;
                     $rech_p3['api_trans_id'] = $api_trans_id;
                     $rech_p3['parent_name'] = $parent_name_s;
                     $rech_p3['child_name'] = $parent_name;
                     $rech_p3['user_name'] = $user_name;
                     $rech_p3['net_code'] = $net_code;
                     $rech_p3['rech_mobile'] = $cons_no;
                     $rech_p3['rech_amount'] = $cons_amount;
                     $rech_p3['rech_net_per'] = $per_s;
                     $rech_p3['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_s);
                     $rech_p3['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                     $rech_p3['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                     $rech_p3['user_balance'] = 0;
                     $rech_p3['rech_date'] = $date_time;
                     $rech_p3['rech_status'] = "PENDING";
                     $rech_p3['rech_type'] = "BILL_PAYMENT";
                     $rech_p3['rech_mode'] = $r_mode;
                     $rech_p3['rech_option'] = "0";
                     $rech_p3['created_at'] = $date_time;
                     $rech_p3['updated_at'] = $date_time;

                
                     $zp3 = self::insertRechargePaymentDistributor($rech_p3, $rech_p3['parent_name'], $r_mode);
                     $zp33 = self::insertTransactionParent($trans_id, $rech_p3['parent_name']);
 
                 }

                $zt = self::insertTransaction($trans_id, $user_name);
                 
                if ($res_code == 200)
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($api_url);
                    $res_code = $res->getStatusCode();
                    $res_content =$res->getBody();

                    $rech_5->where('trans_id', '=', $trans_id)->update(['con_req_status' => $res_content]);


                    // Reply Update
                    InstantReplyUpdate::instantUpdate($res_content, "SUCCESS", url('/')."/pendingbill_success/".$trans_id, $trans_id, "1");
                    InstantReplyUpdate::instantUpdate($res_content, "Success", url('/')."/pendingbill_success/".$trans_id, $trans_id, "1");
                    InstantReplyUpdate::instantUpdate($res_content, "success", url('/')."/pendingbill_success/".$trans_id, $trans_id, "1");
                    InstantReplyUpdate::instantUpdate($res_content, "FAILED", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                    InstantReplyUpdate::instantUpdate($res_content, "Failed", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                    InstantReplyUpdate::instantUpdate($res_content, "failed", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                    InstantReplyUpdate::instantUpdate($res_content, "FAILURE", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                    InstantReplyUpdate::instantUpdate($res_content, "Failure", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                    InstantReplyUpdate::instantUpdate($res_content, "failure", url('/')."/pendingbill_failure/".$trans_id, $trans_id, "2");
                }

                // All Process Over...
            }


        }

        return $z;
        
    }



    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    
    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $accno)
    {
        $rech_1 = new UserRechargeBillDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['con_acc_no', '=', $accno], 
                                ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }


    /**
     * Insertion Process..............................................
     *
     */
    public static function insertRechargePayment($rech_p1)
    {
        $rech_3 = new UserRechargeBillDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1['user_name'])->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['con_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1['user_name'])
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;
            
            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public static function insertRechargePaymentDistributor($rech_p1, $user_name, $r_mode)
    {
        $rech_3 = new UserRechargeNewParentDetails;
        $uacc_1 = new UserAccountBalanceDetails;

        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;

            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function insertTransaction($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "BILL_PAYMENT";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public static function insertTransactionParent($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "BILL_PAYMENT";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    

}
