<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\SmsInterface;
use App\Libraries\GetCommon;

use App\Models\PaymentLedgerDetails;

class Collection
{
    //
    
    public static function collectionOpeningBalance($f_date, $t_date, $user_name)
    {
        $pay_1 = new PaymentLedgerDetails;

        $user_name = strtoupper($user_name);
        $balance = 0;

        // debit (taken)
        $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount');

        // credit (paid)
        $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount');

       
        $balance = floatval($de_tot) - floatval($ce_tot);
        
        return $balance;
    }

    public static function collectionDebitTotal($f_date, $t_date, $user_name)
    {
        $pay_1 = new PaymentLedgerDetails;

        $user_name = strtoupper($user_name);
        $balance = 0;

        // debit (taken)
        $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount');

        return $de_tot;
    }

    public static function collectionCreditTotal($f_date, $t_date, $user_name)
    {
        $pay_1 = new PaymentLedgerDetails;

        $user_name = strtoupper($user_name);

        // credit (paid)
        $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount');

        return $ce_tot;
    }

    

    public static function collectionData($user_name, $f_date, $t_date)
    {
        $pay_1 = new PaymentLedgerDetails;

        $user_name = strtoupper($user_name);

        $data = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where(function($query) {
                            return $query
                            ->where('pay_type', '=', 'C')
                            ->orWhere('pay_type', '=', 'D');
                        })
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        
        return $data;
    }

   
    public static function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

}
