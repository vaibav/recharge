<?php
namespace App\Libraries;

class CyberPlat {

    const CERT   = '473F75E517169907C3918BCA2AB3D88B99CDB171';
    const CPPASS = 'Vaibav@143';
    const SD     = 357355;
    const AP     = 400135;
    const OP     = 400136;
    

    public static function callAPI($URI, $querString)
    {
        $secKey     = file_get_contents(base_path().'/public/cybor/private.key');
        $serverCert = file_get_contents(base_path().'/public/cybor/mycert.pem');
        $passwd     = self::CPPASS;

        $querString = "CERT=".self::CERT."\r\nSD=".self::SD."\r\nAP=".self::AP."\r\nOP=".self::OP."\r\n".$querString;

        //echo $querString."<br>";

        // make SHA1RSA signature
        $pkeyid = openssl_pkey_get_private($secKey, $passwd);
        openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
        openssl_free_key($pkeyid);

        $encoded = base64_encode($signature);
        $encoded = chunk_split($encoded, 76, "\r\n");

        $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";

        $response = self::get_query_result($signInMsg, $URI);

        return $response;
    }

    public static function get_query_result($qs, $url)
    {
        $opts = array( 
            'http'=>array(
                'method'=>"POST",
                'header'=>array("Content-type: application/x-www-form-urlencoded\r\n".
                                "X-CyberPlat-Proto: SHA1RSA\r\n"),
                'content' => "inputmessage=".urlencode($qs)
            )
        ); 
        $context = stream_context_create($opts);    
        return file_get_contents($url, false, $context);
    }

    public static function getSplitData($resultString)
    {
        if (strpos($resultString, '=') !== false) {
            $result = explode("=", $resultString);
            return $result[1];
        }
        else {
            return "";
        }
    }

    public static function checkString($resultString, $parameter)
    {
        if (strpos($resultString, $parameter) !== false) {
            return true;
        }
        return false;
    }

    public static function getError($code)
    {
        $msg = "None";
        switch ($code) {
          case 1:
            $msg = "Error";
            break;
          case 2:
            $msg = "Error";
            break;
          case 3:
            $msg = "Error";
            break;
          case 4:
            $msg = "Error";
            break;
          case 5:
            $msg = "Error";
            break;
          case 6:
            $msg = "Error";
            break;
          case 7:
            $msg = "Error";
            break;
          case 8:
            $msg = "Invalid phone number format.";
            break;
          case 9:
            $msg = "Error";
            break;
          case 10:
            $msg = "Error";
            break;
          case 11:
            $msg = "Error";
            break;
          case 12:
            $msg = "Error";
            break;
          case 13:
            $msg = "Error";
            break;
          case 14:
            $msg = "Error";
            break;
          case 15:
            $msg = "Error";
            break;
          case 16:
            $msg = "Error";
            break;
          case 17:
            $msg = "Error";
            break;
          case 18:
            $msg = "Error";
            break;
          case 19:
            $msg = "Error";
            break;
          default:
            $msg = "Error";
        }
        return $msg;
    }

   
}