<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\SmsInterface;
use App\Libraries\GetCommon;

use App\Models\PaymentLedgerDetails;
use App\Models\UserAccountDetails;

class CollectionCommon
{
    //
    
    public static function getArea($parent_name)
    {
        $user_2 = new UserAccountDetails;

        $d1 = $user_2->with(['personal'])->where('parent_name', '=', $parent_name)->where('user_type', '!=', "AGENT")->get();
                                   
        $area = [];
        $j = 0;
        foreach($d1 as $d)
        {
            if($d->personal->user_state != "")
            {
                $z = self::checkArea($area, $d->personal->user_state);
                if($z == 0)
                {
                    $area[$j] = strtolower($d->personal->user_state);
                    $j++;
                }
            }
            
        }
        
        sort($area);

        return $area;
    }

    public static function checkArea($area, $name)
    {
        $z = 0;
        foreach($area as $d)
        {
            if(strtolower($d) == strtolower($name))
                $z = 1;
        }

        return $z;
    }

    public static function getUser($parent, $area_name)
    {
        $user_2 = new UserAccountDetails;

        $d1 = $user_2->with(['personal'])->where('parent_name', '=', $parent)->where('user_type', '!=', "AGENT")->get();
                                   
        $user = [];
        $j = 0;
        foreach($d1 as $d)
        {
            if(strtolower($d->personal->user_state) == strtolower($area_name))
            {
                $user[$j][0] = $d->user_name;
                $user[$j][1] = $d->personal->user_per_name;
                $user[$j][2] = $d->personal->user_mobile;
                $j++;
            }
            
        }
        
        sort($user);

        return $user;
    }

    public static function getAllUser($parent)
    {
        $user_2 = new UserAccountDetails;

        $d1 = $user_2->with(['personal'])->where('parent_name', '=', $parent)->where('user_type', '!=', "AGENT")->get();
                                   
        $user = [];
        $j = 0;
        foreach($d1 as $d)
        {
            $user[$j][0] = $d->user_name;
            $user[$j][1] = $d->personal->user_per_name;
            $user[$j][2] = $d->personal->user_mobile;
            $j++;
            
        }
        
        sort($user);

        return $user;
    }
   
    public static function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

}
