<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DB;

use App\Libraries\BankTransferInfo;
use App\Libraries\InstantReplyUpdate;
use App\Libraries\PercentageCalculation;
use App\Libraries\ParentNewSuccess;
use App\Libraries\ParentNewFailure;
use App\Libraries\ApiUrl;

use App\Models\UserBankAgentDetails;
use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankTransferDetails;
use App\Models\ApiProviderResultDetails;

use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkDetails;

use App\Models\TransactionAllDetails;


class BankTransferReply
{

    public static function storeTransferupdate_1($res_content, $trans_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $date_time = date("Y-m-d H:i:s");

        //$trans_s = [];
        //$trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            
        }

        //$bank_trans_id = self::getSubResultData($trans_s);

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = 'OTP ".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$remarks."' WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                self::success_update($trans_id, $rep_trans_id, $bank_trans_id, "0", "1");
               
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

    public static function storeTransferupdate_2($res_content, $trans_id)
    {
        $ban_1 = new UserBankTransferDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $s_status, 'mt_reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $trans_status, 'mt_reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }


    // Transfer greater than 5000
    public static function storeTransferupdate_1_1($res_content, $trans_id)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $success_amount = "0";
        $date_time = date("Y-m-d H:i:s");

        $trans_s = [];
        $trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "success_amount")
            {
                $success_amount = $d[1];
            }
            if($d[0] == "transfer_data")
            {
                $trans_s= $d[1];
            }
            if($d[0] == "failed_data")
            {
                $failed_data= $d[1];
            }
            
        }

        

        $bank_trans_id = self::getSubResultData($trans_s);

        $bank_trans_id = $success_amount." :".$bank_trans_id;

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$s_status."', 
                                        mt_reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE user_bank_transfer_details SET mt_status = 'OTP ".$s_status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$remarks."' WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                self::success_update($trans_id, $rep_trans_id, $bank_trans_id, $success_amount, "2");
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

    public static function storeTransferupdate_2_2($res_content, $trans_id)
    {
        $ban_1 = new UserBankTransferDetails;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)
                                ->update(['mt_reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $s_status, 'mt_reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['mt_status' => $trans_status, 'mt_reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }


    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }


    public static function getBankResult($result, $api_line)
    {
        $dx = [];
        $s_status = "PENDING";
        $s_opr_id = "NONE";
        $data = [];

        list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine($api_line);
        if($api_code > 0)
        {
            $dx = self::getAPIResultData($api_code);

            if(!empty($dx))
            {
                $j = 0;
                foreach($dx as $d)
                {
                    if($d[0] == "STATUS 1")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "SUCCESS";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "STATUS 2")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "FAILURE";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "OPERATOR_ID")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $s_opr_id = $value;
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "DATA")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                    else if($d[0] == "ARRAY")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                }
            }
        }

         
        return array($s_status, $s_opr_id, $data);
    }

    public static function getAPIResultData($api_code)
    {
        $api_4 = new ApiProviderResultDetails;

        $dx = [];
        
        $j = 0;
        if($api_code != "0")
        {
            $d1 = $api_4->select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')->where('api_code', '=', $api_code)->get();
            if($d1->count() > 0)
            {
                foreach($d1 as $d)
                {
                    $dx[$j][0] = $d->api_res_para_name;
                    $dx[$j][1] = $d->api_res_para_field;
                    $dx[$j][2] = $d->api_res_para_value;
                    $j++;
                }
            }
        }

        return $dx;
    }


    public static function getSubResultData($res_content)
    {
        $bank_trans_id = "done..";

        //$result = self::resultFormatter($res_content);

        $bank_trans_id = "[";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new2.txt', print_r($res_content, true), FILE_APPEND);

        foreach($res_content as $d)
        {
            $t1 = "";
            $t2 = "";

            if (is_object($d)) {
                if(isset($d->bank_trans_id))
                {
                    $t1 = $d->bank_trans_id;
                }
                if(isset($d->amount))
                {
                    $t2 = $d->amount;
                }
            }
            else
            {
                if (array_key_exists('bank_trans_id', $d)) {
                    $t1 = $d['bank_trans_id'];
                }
                if (array_key_exists('amount', $d)) {
                    $t2 = $d['amount'];
                }
            }

            $bank_trans_id =  $bank_trans_id . '{"bank_trans_id":"'.$t1.'","amount":"'.$t2.'"}, ';
            
        }

        $bank_trans_id= rtrim($bank_trans_id,", ");

        $bank_trans_id = $bank_trans_id."]";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new1.txt', $bank_trans_id, FILE_APPEND);

        return $bank_trans_id;
    }


    /**
     * Success Update
     */

    public static function success_update($trans_id, $rep_trans_id, $bank_trans_id, $res_amt, $cc)
	{
		
        $rech = new UserBankTransferDetails;
        $net_2 = new NetworkDetails;
        
        
        $date_time = date("Y-m-d H:i:s");
        $status = "SUCCESS";
        $user_name = "NONE";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['mt_status', '=', 'PENDING'], ['mt_option', '=', '1']])->get();
        if($d1->count() > 0)
        {
            $rech_amount = $d1[0]->mt_amount;
            $rech_total = $d1[0]->mt_total;
            $user_name = $d1[0]->user_name;
            $r_mode = $d1[0]->mt_mode;
            $api_trans_id = $d1[0]->api_trans_id;

            
            // Update Data
            DB::statement("UPDATE user_bank_transfer_details SET mt_status = '".$status."', 
                                                        mt_reply_trans_id = '".$rep_trans_id."',
                                                        mt_reply_id ='".$bank_trans_id."' WHERE trans_id = '".$trans_id."'");
            $zx++;

            // Parent Updation
            $zx = ParentNewSuccess::updateParentPaymentSuccess($trans_id, $status, $date_time);
            if($r_mode == "API") 
            {
                // Result API URL forwarding
                $res_code = ApiUrl::sendResultApipartner($user_name, $api_trans_id, $bank_trans_id, $status, 1);
            }

            if($cc == "2")
            {
                if(floatval($rech_amount) > floatval($res_amt))
                {
                    $red_amount = floatval($rech_amount) - floatval($res_amt);
                    $red_amount = round($red_amount, 2);
                    self::refundBalance($user_name, $red_amount);
                }
            }
   
        }
 
        return $zx;
       
    }

     /**
     * Failure Update
     */

    public static function failure_update($trans_id, $opr_id)
	{
		
        $rech = new UserBankTransferDetails;
        $net_2 = new NetworkDetails;
        
        
        $date_time = date("Y-m-d H:i:s");
        $status = "FAILURE";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['mt_status', '=', 'PENDING'], ['mt_option', '=', '1']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->mt_total;
            $user_name = $d1[0]->user_name;
            $r_mode = $d1[0]->mt_mode;
            $api_trans_id = $d1[0]->api_trans_id;

            $net_bal = self::updateUserAccountBalance($user_name, $rech_total);

            $rech->where([['trans_id', '=', $trans_id], ['mt_status', '=', 'PENDING']])
                                        ->update(['mt_option' => '2', 'updated_at' => $date_time]);

            // Array
            $rc = [];
            $rc['trans_id'] = $d1[0]->trans_id;
            $rc['api_trans_id'] = $d1[0]->api_trans_id;
            $rc['user_name'] = $user_name;
            $rc['net_code'] = $d1[0]->net_code;
            $rc['bn_id'] = $d1[0]->bn_id;
            $rc['rem_id'] = $d1[0]->rem_id;
            $rc['bk_acc_no'] = $d1[0]->bk_acc_no;
            $rc['bk_code'] = $d1[0]->bk_code;
            $rc['bk_trans_type'] = $d1[0]->bk_trans_type;
            $rc['mt_amount'] = $d1[0]->mt_amount;
            $rc['mt_per'] = $d1[0]->mt_per;
            $rc['mt_per_amt'] = $d1[0]->mt_per_amt;
            $rc['mt_surp'] = $d1[0]->mt_surp;
            $rc['mt_total'] = $rech_total;
            $rc['user_balance'] = $net_bal;
            $rc['mt_date'] = $d1[0]->mt_date;
            $rc['mt_req_status'] = "";
            $rc['mt_status'] = $status;
            $rc['mt_mode'] = $d1[0]->mt_mode;
            $rc['mt_option'] = "2";
            $rc['api_code'] = $d1[0]->api_code;
            $rc['mt_reply_trans_id'] = "0";
            $rc['mt_reply_id'] = $opr_id;
            $rc['mt_reply_date'] = $date_time;
            $rc['created_at'] = $date_time;
            $rc['updated_at'] = $date_time;


            // Update Data
            $rech->insert($rc);
            $zx++;

            // Parent Update
            $zx = intval($zx) + ParentNewFailure::updateParentPaymentFailure($trans_id, $status, $date_time, "BANK_TRANSFER");
            if ($r_mode == "API")
            {
                // Result API URL forwarding
                $res_code = ApiUrl::sendResultApipartner($user_name, $api_trans_id, $opr_id, $status, 2);
            }
            

            $zt = self::insertTransaction($trans_id, $user_name);


        }
 
        return $zt;
       
    }

    public static function updateUserAccountBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;

        $net_bal = 0;

        $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d2->count() > 0)
        {
            $u_bal = $d2[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance'=>$net_bal]);

        }

        return $net_bal;

    }

    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }

    public static function insertTransaction($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "BANK_TRANSFER";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

   
}
