<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\CyberBankApi;
use App\Libraries\BankRemitter;
use App\Libraries\CyberBankReply;

use App\Models\UserBankRemitterDetails;
use App\Models\UserAccountDetails;
use App\Models\ApiUrlDetails;

class CyberBankRemitter
{

    /**
     * Add Remitter Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function userCheck($mobileNo)
    {
        $resStatus  = 1;
        $resMsg     = "No Action...";
        $resContent = [];
        $userData   = [];
        $remitter   = "";
        $beneficiary = "";

        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("REM_API_3");

        if($apiCode <= 0) {
            return array($resStatus, $resMsg, $resContent);
        }

        $userData['mobileNo'] = $mobileNo;

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $userData);

        file_put_contents(base_path().'/public/sample/bank_api_url.txt', $apiUrl.PHP_EOL, FILE_APPEND);

        if($apiUrl == "NONE") {
            return array($resStatus, $resMsg, $resContent);
        }

        $resContent = BankRemitter::runURL($apiUrl, "REM_CHECK");

        //echo $resContent."<br>";

        list($status, $resMsg, $isVerified, $remitter, $beneficiary) = CyberBankReply::getRemitterCheckResult($resContent);

        if($status == "SUCCESS")
        {
            $resStatus = 0;
        }

         //echo $isVerified."<br>";

        return array($resStatus, $resMsg, $resContent, $remitter, $beneficiary, $isVerified);
       
    }

    /**
     * Add Remitter Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        $rem_1 = new UserBankRemitterDetails;

        $z = 0;
        $res_code = 0;
        $res_content = "none";
        $message = "";
        $isVerified = "-";
        
        $rem_id = rand(1000,9999);
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name_1'];
        $date_time = date("Y-m-d H:i:s");

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 1;
            return array($z, $message, $res_content, $isVerified);
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
            return array($z, $message, $res_content, $isVerified);
        }

        $zy = self::checkAlreadyEntry($user_name);
        if($zy == 3)
        {
            $z = 3;     // Already Registered...
            return array($z, $message, $res_content, $isVerified);
        }


        // Do it.... 
        list($api_code, $v) = CyberBankApi::getAgent1NetworkLine("REM_API_1");

        if($api_code <= 0) {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content, $isVerified);
        }

        //other vaibav
        $data['trans_id'] = $rem_id;

        $api_url = CyberBankApi::generateBankAPI($api_code, $data);

        //echo $api_url;

        if($api_url == "NONE") {
            $z = 6;     // Already Registered...
            return array($z, $message, $res_content, $isVerified);
        }

       

         // Insert Record... 
        $rem_1->rem_id = $rem_id;
        $rem_1->api_trans_id = $data['api_trans_id'];
        $rem_1->user_name = $data['user_name_1'];
        $rem_1->user_rem_name = $data['user_name'];
        $rem_1->user_rem_lname = $data['user_lname'];
        $rem_1->user_address = $data['user_address'];
        $rem_1->user_city = $data['user_city'];
        $rem_1->user_state_code = $data['user_state_code'];
        $rem_1->user_pincode = $data['user_pincode'];
        $rem_1->msisdn = $data['mobileNo'];
        $rem_1->rem_date = $date_time;
        $rem_1->api_code = "0";
        $rem_1->rem_req_status = "done";
        $rem_1->rem_status = "PENDING";
        $rem_1->rem_rep_opr_id = "0";
        $rem_1->reply_date = $date_time;

        $v = $rem_1->save();

        if($v)
        {
            $res_content = BankRemitter::runURL($api_url, "REM_ADD");

            list($status, $msg, $isVerified) = CyberBankReply::storeRemitterResult($res_content, $rem_id);
            $message = $msg;

            if($status == "FAILURE")
            {
                $z = 4;
            }
            else  if($status == "PENDING")
            {
                $z = 5;
            }

            return array($z, $message, $res_content, $isVerified);
        }
        else {
            $z = 6;
        }

        return array($z, $message, $res_content, $isVerified);


    }

    /**
     * Check Remitter OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function userOtpCheck($userData)
    {
        $resStatus  = 1;
        $resMsg     = "No Action...";
        $resContent = [];
        $isVerified = "-";

        list($apiCode, $v) = CyberBankApi::getAgent1NetworkLine("REM_API_2");       //For OTP Check

        if($apiCode <= 0) {
            return array($resStatus, $resMsg, $resContent);
        }

        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $userData);

        file_put_contents(base_path().'/public/sample/bank_api_url.txt', $apiUrl.PHP_EOL, FILE_APPEND);

        if($apiUrl == "NONE") {
            return array($resStatus, $resMsg, $resContent, $isVerified);
        }

        $resContent = BankRemitter::runURL($apiUrl, "REM_API_2");

        list($status, $resMsg, $isVerified, $remitter, $beneficiary) = CyberBankReply::getRemitterCheckResult($resContent);

        if($status == "SUCCESS")
        {
            $resStatus = 0;
        }

        return array($resStatus, $resMsg, $resContent, $isVerified);
       
    }

     public static function checkUserModeandStatus($user_name, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 0;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 3;
            }

        }

        return $z;
    }

    public static function checkAlreadyEntry($msisdn)
    {
        $rem_1 = new UserBankRemitterDetails;
        
        $z = 0;

        $cnt = $rem_1->where('msisdn', '=', $msisdn)
                        ->where('rem_status', '=', 'SUCCESS')->count();

        if($cnt > 0)
        {
           $z = 3;
        }

        return $z;
    }

}