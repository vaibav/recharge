<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;
use App\Libraries\BankRemitter;
use App\Libraries\DirectReply;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\BonrixApiResultDetails;

use App\Models\UserRechargeNewDetails; 
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\ServerDetails;

class RechargeNewMobile
{

    /**
     * Add RechargeBill Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_code = 0;
        $api_url = "NONE";
        $api_data = [];
        $api_method = "GET";
        $b_url = "";
        $res_content = "NONE";
        $resx = new stdClass();
        $rech1 = [];

        // Modals
        $bon_1 = new BonrixApiResultDetails;
        $rech_n_2 = new UserRechargeNewStatusDetails;
       
        // Post Data
        $r_mode = $data['mode'];
        $rech_type = "WEB_RECHARGE";

        if($r_mode == "WEB") {
            $cat = "VBR";
            $rech_type = "WEB_RECHARGE";
        }
        else if($r_mode == "API") {
            $cat = "VBA";
            $rech_type = "API_RECHARGE";
        }
        else if($r_mode == "GPRS") {
            $cat = "VBG";
            $rech_type = "WEB_RECHARGE";
            $r_mode = "GPRS";
        }
        else if($r_mode == "SMS") {
            $cat = "VBS";
            $rech_type = "WEB_RECHARGE";
            $r_mode = "SMS";
        }
        
        
        $net_type_code = $data['net_type_code'];
        $net_code = $data['net_code'];
        $user_mobile = $data['user_mobile'];
        $user_amount = $data['user_amount'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");

        $trans_id = $cat.rand(10000000,99999999);


        // 1.Check User Balance
        if(floatval($user_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }

        // Get Network Line
        
        if ($r_mode == "API") {
            list($api_code, $v) = ApiUrl::getApiNetworkLine($net_code, $user_amount, $user_name);
        }
        else
        {
            list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        }
        
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 3;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 4;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 5;     // Account Balance is low from Setup Fee...
        }


        // Check Mobile Status
        $zy = self::checkAccountNoStatus($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy == 6)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        // Check 10 Minutes Status
        $zy =self::checkTenMinsCheck($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy > 0)
        {
            $z = 7;
        }

        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 8;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            // Do Recharge.... 
            /*if($net_code == "857")
            {
                list($api_url, $api_data) = ApiUrl::generateAPICommon($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
            }
            else
            {
                $api_url = ApiUrl::generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
                
            }*/

            list($api_url, $api_data, $api_method) = ApiUrl::generateAPICommon($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
            

            if($api_url != "NONE")
                $res_code = 200;


            // Insertion Process..
            list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $user_amount, $r_mode);

            // Array
            $rech = [];
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $api_trans_id;
            $rech['user_name'] = $user_name;
            $rech['net_code'] = $net_code;
            $rech['rech_mobile'] = $user_mobile;
            $rech['rech_amount'] = $user_amount;
            $rech['rech_net_per'] = PercentageCalculation::convertNumberFormat($per);
            $rech['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt);
            $rech['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
            $rech['rech_total'] = PercentageCalculation::convertNumberFormat($netamt);
            $rech['user_balance'] = '0';
            $rech['rech_date'] = $date_time;
            $rech['rech_status'] = "PENDING";
            $rech['rech_mode'] = $r_mode;
            $rech['rech_option'] = "0";
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;
            

            $zx = self::insertRecharge($rech, $r_mode);
            if($zx == 1)
            {

                // Insert Recharge Request
                $rech1['trans_id'] = $trans_id;
                $rech1['rech_type'] = "RECHARGE";
                $rech1['rech_web'] = "";
                $rech1['api_code'] = $api_code;
                $rech1['rech_req_status'] = "";
                $rech1['reply_opr_id'] = "";
                $rech1['reply_date'] = $date_time;
                $rech1['created_at'] = $date_time;
                $rech1['updated_at'] = $date_time;

                $zy = self::insertRechargeRequest($rech1, $r_mode);

                
                // Parent Network Percentage Calculation
                list($parent_type, $parent_name) = self::getParent($user_name);
                list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $user_amount);

                $rech_p2 = [];
                $rech_p2['trans_id'] = $trans_id;
                $rech_p2['api_trans_id'] = $api_trans_id;
                $rech_p2['parent_name'] = $parent_name;
                $rech_p2['child_name'] = "NONE";
                $rech_p2['user_name'] = $user_name;
                $rech_p2['net_code'] = $net_code;
                $rech_p2['rech_mobile'] = $user_mobile;
                $rech_p2['rech_amount'] = $user_amount;
                $rech_p2['rech_net_per'] = $per_d;
                $rech_p2['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_d);
                $rech_p2['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech_p2['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech_p2['user_balance'] = 0;
                $rech_p2['rech_date'] = $date_time;
                $rech_p2['rech_status'] = "PENDING";
                $rech_p2['rech_type'] = "RECHARGE";
                $rech_p2['rech_mode'] = $r_mode;
                $rech_p2['rech_option'] = "0";
                $rech_p2['created_at'] = $date_time;
                $rech_p2['updated_at'] = $date_time;

                $zp2 = self::insertRechargePaymentDistributor($rech_p2, $rech_p2['parent_name'], $r_mode);
                $zp22 = self::insertTransactionParent($trans_id, $rech_p2['parent_name'], $rech_type);


                 // Super Parent Network Percentage Calculation
                 list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                 if($parent_name_s != "NONE" && $parent_name_s != "admin")
                 {
                     list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $user_amount);
                 
                     // Super Distributor Payment Details
                     $rech_p3 = [];
                     $rech_p3['trans_id'] = $trans_id;
                     $rech_p3['api_trans_id'] = $api_trans_id;
                     $rech_p3['parent_name'] = $parent_name_s;
                     $rech_p3['child_name'] = $parent_name;
                     $rech_p3['user_name'] = $user_name;
                     $rech_p3['net_code'] = $net_code;
                     $rech_p3['rech_mobile'] = $user_mobile;
                     $rech_p3['rech_amount'] = $user_amount;
                     $rech_p3['rech_net_per'] = $per_s;
                     $rech_p3['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_s);
                     $rech_p3['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                     $rech_p3['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                     $rech_p3['user_balance'] = 0;
                     $rech_p3['rech_date'] = $date_time;
                     $rech_p3['rech_status'] = "PENDING";
                     $rech_p3['rech_type'] = "RECHARGE";
                     $rech_p3['rech_mode'] = $r_mode;
                     $rech_p3['rech_option'] = "0";
                     $rech_p3['created_at'] = $date_time;
                     $rech_p3['updated_at'] = $date_time;
 
                                    
                     $zp3 = self::insertRechargePaymentDistributor($rech_p3, $rech_p3['parent_name'], $r_mode);
                     $zp33 = self::insertTransactionParent($trans_id, $rech_p3['parent_name'], $rech_type);
 
                 }

                $zt = self::insertTransaction($trans_id, $user_name, $rech_type);
                 
                if ($res_code == 200)
                {

                    $rech_web = "";
                    $res_content = "";
                    if (strpos($api_url, 'ssh1.bonrix.in') !== false) 
                    {
                        // Bonrix URL
                        $bon_1->trans_id = $trans_id;
                        $bon_1->result_url = $api_url;
                        $bon_1->result_status = "1";
                        $bon_1->save();

                        $rech_web = "BROWSER";
                        $rech1['rech_web'] = "BROWSER";
                        $rech1['rech_req_status'] = "done";

                    }
                    else
                    {
                        try
                        {
                            $client = new \GuzzleHttp\Client();
                            if($api_method == "POST")
                            {
                                $params['form_params'] = $api_data;
                                $res = $client->post($api_url, $params);
                                $res_content = $res->getBody();
                            }
                            else if($api_method == "GET")
                            {
                                $res_content = BankRemitter::runURL($api_url, "RECHARGE");
                                /*$res = $client->get($api_url);
                                $res_code = $res->getStatusCode();
                                $res_content = $res->getBody();*/
                            }
                        }
                        catch (\GuzzleHttp\Exception\ClientException $e) {
                            $res_content = "{'status':'failure','tnx_id':'API URL error...'}";
                        }
                        

                        $rech_web = "CURL";
                        $rech1['rech_web'] = "CURL";
                        $rech1['rech_req_status'] = $res_content;
                    }

                    $rech_n_2->where('trans_id', '=', $trans_id)->update(['rech_req_status' => $res_content, 'rech_web' => $rech_web]);

  
                    // Direct Reply New
                    $api_type = DirectReply::getApiResultType($api_code);

                    if($api_type == "DIRECT")
                    {
                        DirectReply::instantUpdate($res_content, $trans_id, $api_code);
                    }
                    else
                    {
                        // Reply Update
                        InstantReplyUpdate::instantUpdate($res_content, "SUCCESS", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                        InstantReplyUpdate::instantUpdate($res_content, "success", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                        InstantReplyUpdate::instantUpdate($res_content, "Success", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                        InstantReplyUpdate::instantUpdate($res_content, "FAILED", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                        InstantReplyUpdate::instantUpdate($res_content, "Failed", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                        InstantReplyUpdate::instantUpdate($res_content, "failed", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                        InstantReplyUpdate::instantUpdate($res_content, "FAILURE", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                        InstantReplyUpdate::instantUpdate($res_content, "failure", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                    }

                    

                   
                }

                // All Process Over...
            }


        }

        return $z;
        
    }



    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    

    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeNewDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['rech_mobile', '=', $mobile], 
                                ['rech_amount', '=', $amount], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public static function checkTenMinsCheck($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeNewDetails;
        
        $z = 0;

        $d1 = $rech_1->select('created_at')->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'SUCCESS']])->orderBy('id', 'desc')->limit(1)->get();
        if($d1->count() > 0)
        {
            $old_d = $d1[0]->created_at;
            $cur_d = date("Y-m-d H:i:s");

            $o_d = new DateTime($old_d);
            $c_d = new DateTime($cur_d);

            $dif = $o_d->diff($c_d);

            $minutes = $dif->days * 24 * 60;
            $minutes += $dif->h * 60;
            $minutes += $dif->i;

            if($minutes < 10)
                $z = 1;
        }

        return $z;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }


    /**
     * Insertion Process..............................................
     *
     */
    public static function insertRecharge($rech_p1, $r_mode)
    {
        $rech_1 = new UserRechargeNewDetails;
        $uacc_1 = new UserAccountBalanceDetails;

        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1['user_name'])->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1['user_name'])
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;
            
            if($rech_1->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function insertRechargeRequest($rech_p1, $r_mode)
    {
        $rech_1 = new UserRechargeNewStatusDetails;

        $zx = 0;
       
        $d1 = $rech_1->where('trans_id', '=', $rech_p1['trans_id'])->get();
        if($d1->count() == 0)
        {
           
            if($rech_1->insert($rech_p1))
            {
                $zx = 1;
            }
            
        }

        return $zx;
    }

    public static function insertRechargePaymentDistributor($rech_p1, $user_name, $r_mode)
    {
        $rech_3 = new UserRechargeNewParentDetails;
        $uacc_1 = new UserAccountBalanceDetails;

        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;

            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function insertTransaction($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public static function insertTransactionParent($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    

}
