<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeResultDetails;
use App\Models\UserRechargeDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeRequestDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\ApiProviderResultDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\UserAccountDetails;
use App\Models\UserRechargeApipartnerDetails;
use App\Models\UserRechargeRequestApipartnerDetails;
use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\BonrixApiResultDetails;
use App\Models\ApiProviderDetails;

use App\Models\UserRechargeNewDetails;

use App\Libraries\CheckServerResult;
use App\Libraries\GetAPIResult;
use App\Libraries\SmsInterface;
use App\Libraries\RechargeNewReply;
use App\Libraries\BankRemitter;

class ResponseController extends Controller
{
    //
    public function index()
	{
        //
		
    }

    public function testurl(Request $request)
    {
        $d1 = $request->all();
        
        $str = "";
		foreach($d1 as $key => $value) 
		{
           $str = $str.$key."---".$value."--|--";
        }

        file_put_contents("testapiurl.txt", "welcome-succes-".$str."-".date("Y-m-d H:i:s").PHP_EOL, FILE_APPEND);
    }

    public function testurl1(Request $request)
    {
        $d1 = $request->all();
        
        $str = "";
		foreach($d1 as $key => $value) 
		{
           $str = $str.$key."---".$value."--|--";
        }

        file_put_contents("testapiurl.txt", "welcome-failure-".$str."-".date("Y-m-d H:i:s").PHP_EOL, FILE_APPEND);
        
    }

    public function view(Request $request)
	{
        $res_1 = new UserRechargeResultDetails;
        $bon_1 = new BonrixApiResultDetails;
        $op = "done..";

        $data = [];
        
        for($j = 0; $j<=12; $j++)
		{
			$data[$j] = "-";
		}

        $d1 = $request->all();
        
        $i = 1;
		foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if ( strlen($value) > 85)
            {
                $vx = substr($value, 0, 85);
            }
            else
            {
                $vx = $value;
            }
            $data[$i] = trim($vx);
			$i++;
        }

        file_put_contents(base_path()."/public/sample/testapiresult.txt", print_r($data, true), FILE_APPEND);

        $date_time = date("Y-m-d H:i:s");

        
        //echo "Record is inserted...";

        /*
        |--------------------------------------------------------------------------
        | Result Update Process
        |--------------------------------------------------------------------------
        |   1. Get all Pending rows in recharge_details table 
        |   2. In each row, Get API Result 
        |   3. Check User Account status
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        // Get API Code
        $trans_id = "0";
        $api_trans_id = "0";
        $api_code = "0";
        $user_name = "NONE";
        $reply_status = "";
        $reply_oprid = "NA";
        $e = 11;

        list($trans_id, $api_code, $user_name, $api_trans_id, $c, $zxx) = CheckServerResult::checkResult($data);

        

        if($zxx == 0)
        {
            $op = "No match for this result..";
        }

        $res_1->a1 = $data[1];
        $res_1->a2 = $data[2];
        $res_1->a3 = $data[3];
        $res_1->a4 = $data[4];
        $res_1->a5 = $data[5];
        $res_1->a6 = $data[6];
        $res_1->a7 = $data[7];
        $res_1->a8 = $data[8];
        $res_1->a9 = $op;
        $res_1->a10 = $trans_id;
        $res_1->a11 = $api_code;
        
        $res_1->save();

        

        $api_res_url = "NONE";
        $api_res_c = 0;


        //Check if already result is updated... 
        $e = $this->checkResultAlreadyStatus($trans_id, $c);
        if($e == 1)
        {
            // Now Pending... 
            // Get API result Data
            $s_status = "NONE";
            $s_trans_id = "0";
            $s_opr_id = "0";
            if($api_code != "0")
            {
                
                list($s_status, $s_trans_id, $s_opr_id) = GetAPIResult::getAPIResultParameters($data, $api_code);

            }

            // Web Recharge Result
            if($c == 1)
            {

                if($s_status == "SUCCESS")
                {
                    
                    $reply_status = "SUCCESS";
                    $reply_oprid = $s_opr_id;
                    $zx = RechargeNewReply::updateSuccess($trans_id, $s_opr_id, $s_status, $date_time);
                    
                   
                    if($zx > 0)
                        $op = "Response has done...";
                }
                else if($s_status == "FAILURE")
                {
                    // Do lot of Failure Updates
                    $reply_status = "FAILURE";
                    $reply_oprid = "NA";
                    $zx = RechargeNewReply::updateFailure($trans_id, $s_opr_id, $s_status, $date_time);
                    
            
                    if($zx > 0)
                        $op = "Response has done...";
                }
            }
            
            // API Recharge Result
            if($c == 2)
            {

                
                list($api_s_url, $api_f_url) = $this->getApipartnerUrl($user_name);
                $api_p_url = "";
                
                if($s_status == "SUCCESS")
                {
                    $reply_status = "SUCCESS";
                    $reply_oprid = $s_opr_id;

                    $zx = RechargeNewReply::updateSuccess($trans_id, $s_opr_id, $s_status, $date_time);

                    $api_p_url = $api_s_url;
                    
                    if($zx > 0)
                        $op = "Response has done...";
                }
                else if($s_status == "FAILURE")
                {
                    $reply_status = "FAILURE";
                    $reply_oprid = "NA";

                    $zx = RechargeNewReply::updateFailure($trans_id, $s_opr_id, $s_status, $date_time);

                    $api_p_url = $api_f_url;
                                
                   
                    if($zx > 0)
                        $op = "Response has done...";
                }

                // API Partner Reply URL Parameter Setting...
                if (strpos($api_p_url, '<trid>') !== false) {
                    $api_p_url = str_replace("<trid>", $api_trans_id, $api_p_url);
                }
                if (strpos($api_p_url, '<oprid>') !== false) {
                    $api_p_url = str_replace("<oprid>", $s_opr_id, $api_p_url);
                }
                if (strpos($api_p_url, '<status>') !== false) {
                    $api_p_url = str_replace("<status>", $s_status, $api_p_url);
                }
                

                // Check Bonrix
                if (strpos($api_p_url, 'bonrix.in') !== false) {
                    $bon_1->trans_id = rand(100000,999999);
                    $bon_1->result_url = $api_p_url;
                    $bon_1->result_status = "1";
                    $bon_1->save();
                }
                else
                {
                   /* $api_res_url = $api_p_url;
                    $api_res_c = 1;*/
                   $bon_1->trans_id = rand(100000,999999);
                    $bon_1->result_url = $api_p_url;
                    $bon_1->result_status = "1";
                    $bon_1->save();
                    
                    /*$n_url = "http://ec2-3-19-53-122.us-east-2.compute.amazonaws.com/response.php?tx=".$api_p_url;
                    
                     file_put_contents(base_path()."/public/sample/response_update1_url.txt", $n_url.PHP_EOL, FILE_APPEND);
                    
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($n_url);
                    $res_code = $res->getStatusCode();
                    $res_content_1 =$res->getBody();*/
                }

            }

            // TNEB Result
            if($c == 3)
            {
                $su_url = "";

                if($s_status == "SUCCESS")
                {
                    $su_url = url('/')."/pendingbill_success/".$trans_id."/".$s_opr_id;
                }
                else if($s_status == "FAILURE")
                {
                    $su_url = url('/')."/pendingbill_failure/".$trans_id."/".$s_opr_id;
                }

                if($su_url != "") {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($su_url);
                    $res_code = $res->getStatusCode();
                    $res_content_1 =$res->getBody();
                }
            }
        }
        else if($e == 2)
        {
            $op = "Error! Recharge has already done....";
        }
        else
        {
            $op = "Error! Transaction Id is Invalid...";
        }
        
       

        // SMS SENDING PROCESS...... 
        if (strpos($trans_id, 'VBS') !== false) 
        {
            list($ux_bal, $ux_mob) = $this->getBalance($user_name);
            list($r_mobile, $r_amount) = $this->getRechDetails($trans_id);
            
            if($ux_mob != "")
            {
                $reply_data = $reply_status." NO:".$r_mobile." AMT:".$r_amount." OPRID:".$reply_oprid." BAL:".$ux_bal;
                
                $zep_code = SmsInterface::callSms($user_name, $ux_mob, $reply_data);

            }
        }
        

        // Response URL for API Partner
        try
        {
            if($c == 2)
            {
                if($api_res_c == 1)
                {
                    if($api_res_url != "")
                    {
                        file_put_contents(base_path()."/public/sample/response_update_url.txt", $api_res_url.PHP_EOL, FILE_APPEND);
                        BankRemitter::runURL($api_res_url, "RESPONSE");
                       /* $client = new \GuzzleHttp\Client();
                        $res = $client->get($api_res_url);
                        $res_code = $res->getStatusCode();
                        $res_content_1 =$res->getBody();*/
                    }
                   
                }
            }
        }
        catch(\GuzzleHttp\Exception\ClientException $e)
        {
            $op = "Response has done! but Invalid API Result URL...";
        }

        $f_result = ['output' => $op];
        
        return response()->json($f_result, 200);
       
    }

    public function view1(Request $request)
	{
        
        echo "new data";
        echo $request->data1;
        
        
    }

    public function checkResultAlreadyStatus($trans_id, $c)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $rech_b_1 = new UserRechargeBillDetails;

        $e = 0;

        if($c == 1 || $c == 2) {
            $d2 = $rech_n_1->where('trans_id', '=', $trans_id)
                                ->where('rech_status', '=', 'PENDING')
                                ->where('rech_option', '=', '0')->count();
            if($d2 > 0)
                $e = 1;
        }
        else if($c == 3) {
            $d1 = $rech_b_1->select('con_status')->where([['trans_id', '=', $trans_id],
                                         ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->count();
            if($d1 > 0)
                $e = 1;
        }
        
        return $e;
        
    }

    
  

    public function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }


    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }



    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $uper_1 = new UserPersonalDetails;
        $ux_bal = 0;
        $ux_mob = "";

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        $d2 = $uper_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
        foreach($d2 as $d)
        {
            $ux_mob = $d->user_mobile;
        }

        return array($ux_bal, $ux_mob);
    }

    public function getRechDetails($trans_id)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_2 = new UserRechargeApipartnerDetails;

        $r_amount = 0;
        $r_mobile = 0;

        $d1 = $rech_1->select('trans_id', 'rech_mobile', 'rech_amount')->where('trans_id', '=', $trans_id)->get();
        foreach($d1 as $d)
        {
            $r_mobile = $d->rech_mobile;
            $r_amount = $d->rech_amount;
        }

        if($r_mobile == 0)
        {
            $d2 = $rech_2->select('trans_id', 'rech_mobile', 'rech_amount')->where('trans_id', '=', $trans_id)->get();
            foreach($d2 as $d)
            {
                $r_mobile = $d->rech_mobile;
                $r_amount = $d->rech_amount;
            }
        }

        return array($r_mobile, $r_amount);
    }
}
