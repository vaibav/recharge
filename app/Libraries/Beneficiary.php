<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\BankTransferInfo;
use App\Libraries\BankRemitter;
use App\Libraries\BankReply;
use App\Libraries\PercentageCalculation;

use App\Models\UserBeneficiaryDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;

class Beneficiary
{

    /**
     * Add Beneficiary Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $z = 0;
        $message = "";
        $trans_id = "";
        $res_content = "";
        
        $ben_id = rand(1000,9999);
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $data['beneficiary_id'] = "";
        $data['type'] = 'new';

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 1;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
        }

       /* $zy = self::checkAlreadyEntry($data['beneficiary_account_no'], $data['msisdn']);
        if($zy == 3)
        {
            $z = 3;     // Already Registered...
        }*/

        if($z == 0)
        {
             // Do it.... 
             list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_API_1");
             if($api_code > 0)
             {
                $api_url = BankTransferInfo::generateBankBeneficiaryNewAPI($api_code, $data);
 
                file_put_contents(base_path().'/public/sample/bank_api.txt', "BEN :".$api_url.PHP_EOL, FILE_APPEND);
 
                if($api_url != "NONE")
                     $res_code = 200;
                 
                 
                 // Insert Record... 
                $ben_1->ben_id = $ben_id;
                $ben_1->api_trans_id = $data['api_trans_id'];
                $ben_1->user_name = $data['user_name'];
                $ben_1->agent_id = "0";
                $ben_1->msisdn = $data['msisdn'];
                $ben_1->ben_acc_no = $data['beneficiary_account_no'];
                $ben_1->ben_acc_name = $data['beneficiary_name'];
                $ben_1->bank_code = $data['beneficiary_bank_code'];
                $ben_1->bank_ifsc = $data['beneficiary_ifsc_code'];
                $ben_1->api_code = $api_code;
                $ben_1->ben_req_status = "done...";
                $ben_1->ben_status = "PENDING";
                $ben_1->ben_s_status = "0";
                $ben_1->trans_id = "0";
                $ben_1->ben_rep_opr_id = "";
                $ben_1->ben_rep_date = $date_time;
    
                $ben_1->save();
 
                if ($res_code == 200)
                {
                    $res_content = BankRemitter::runURL($api_url, "BEN_ADD");

                    list($status, $msg, $trans_id) = BankReply::storeBeneficiaryupdate_1($res_content, $ben_id);
                    $message = $msg;

                    if($status == "FAILURE")
                    {
                        $z = 4;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 5;
                    }

                }
            }
        }
       
        

        return array($z, $message, $trans_id, $ben_id, $res_content);
    }


    /**
     * Add Agent OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add_otp($data)
    {
        $z = 0;
        $message = "";
        $res_content = "";
        
        $agent_id = "";
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $ben_id = $data['ben_id'];
        $agent_id = "0";
        $msisdn = $data['msisdn'];
        $trans_id = $data['trans_id'];


        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }


        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_API_2");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    
                    $res_content = BankRemitter::runURL($api_url, "BEN_ADD_OTP");

                    list($status, $msg) = BankReply::storeBeneficiaryupdate_2($res_content, $ben_id);
                    $message = $msg;

                    if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message, $res_content);
    }


    /**
     * Resend Beneficiary OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function resend_otp($data)
    {

        $z = 0;
        $message = "";
        
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $msisdn = $data['msisdn'];
        $ben_id = $data['ben_id'];
        $trans_id = $data['trans_id'];
        $trans_id_r = "";


        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }



        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_API_3");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                file_put_contents(base_path().'/public/sample/bank_api.txt', "BEN_ROTP-".$date_time." :".$api_url.PHP_EOL, FILE_APPEND);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    $res_content = BankRemitter::runURL($api_url, "BEN_SEND_OTP");

                    list($status, $msg, $trans_id_r) = BankReply::storeBeneficiaryupdate_4($res_content, $ben_id);
                    $message = $msg;

                    if($status == "SUCCESS")
                    {
                        $z = 10;
                    }
                    else if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message, $trans_id_r);
    }


    /**
     * Check Beneficiary Account No.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function check_ben_account($data)
    {
       

        $z = 0;
        $message = "";
        $ben_name = "NONE";
        $res_content = "NONE";
        
        $agent_id = "";
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $agent_id = "0";
        $msisdn = $data['msisdn'];
        $trans_id = $data['client_trans_id'];

        $surp = 5;


        // Do it.... 
        list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_VERIFY_1");
        if($api_code > 0)
        {
            $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

            

            $res_code = 201;

            if ($api_url != "NONE")
            {
               
                $zx = self::reduceBalance($user_name, $trans_id, $data, $surp, $api_code, $r_mode);

                $res_content = BankRemitter::runURL($api_url, "BEN_VERIFY");

                list($status, $msg, $ben_name) = BankReply::storeBeneficiaryupdate_3($res_content, $trans_id);
                $message = $msg;

                if($status == "SUCCESS")
                {
                    $z = 1;
                }
                if($status == "FAILURE")
                {
                    $z = 2;
                    self::refundBalance($user_name, $surp);
                }

            }
        }
       
        return array($z, $message, $ben_name, $res_content);
    }


    /**
     * delete Beneficiary Account No.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function delete($data)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $z = 0;
        $message = "";
        $remarks = "";
        $trans_id = "";
        $res_content = "";
        $otp_status = "0";
        
        $ben_id = $data['ben_id'];
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        

        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 1;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 2;     // Account is Inactivated...
        }

       

        if($z == 0)
        {
             // Do it.... 
             list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_D_API_1");
             if($api_code > 0)
             {
                $api_url = BankTransferInfo::generateBankBeneficiaryNewDeleteAPI($api_code, $data);
 
                if($api_url != "NONE")
                {
                    $res_content = BankRemitter::runURL($api_url, "BEN_DEL");

                    list($status, $message, $trans_id, $remarks, $otp_status) = BankReply::storeBeneficiaryupdate_5($res_content, $ben_id);
                   

                    if($status == "FAILURE")
                    {
                        $z = 4;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 5;
                    }

                }
            }
        }
       
        
        return array($z, $message."--".$remarks, $trans_id, $otp_status, $res_content);
    }


     /**
     * Add Agent OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function delete_otp($data)
    {
        $z = 0;
        $message = "";
        $res_content = "";
        
        $agent_id = "";
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $ben_id = "0";
        $agent_id = "0";
        $msisdn = $data['msisdn'];
        $trans_id = $data['trans_id'];


        // Check Username status
        $zy = self::checkUserModeandStatus($user_name, $r_mode);
        if($zy == 2)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($zy == 3)
        {
            $z = 3;     // Account is Inactivated...
        }


        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("BEN_D_API_2");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    
                    $res_content = BankRemitter::runURL($api_url, "BEN_DEL_OTP");

                    list($status, $msg) = BankReply::storeBeneficiaryupdate_6($res_content, $ben_id);
                    $message = $msg;

                    if($status == "FAILURE")
                    {
                        $z = 5;
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message, $res_content);
    }

    /**
     * View Particular Beneficiary Details.
     *
     * @param  String  $user_name
     * @return Object  $d1
     */
    public static function view_user($user_name, $msisdn)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $d1 = $ben_1->where('user_name', '=', $user_name)->where('msisdn', '=', $msisdn)->orderBy('id', 'desc')->get();
        
        return $d1;
    }
    
    public static function view_user1($user_name)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $d1 = $ben_1->where('user_name', '=', $user_name)->orderBy('id', 'desc')->get();
        
        return $d1;
    }



     /**
     * View All Beneficiary Details.
     *
     * @param  null
     * @return Object  $d1
     */
    public static function view_all()
    {
        $ben_1 = new UserBeneficiaryDetails;

        $d1 = $ben_1->orderBy('id', 'desc')->get();
        
        return $d1;
    }


    /**
     * Update Beneficiary Status.
     *
     * @param  String  $bn_id, int $x
     * @return int  $z
     */
    public static function update($bn_id, $opr_id, $x)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $z = 0;
        
        $date_time = date("Y-m-d H:i:s");
       
        $cnt = $ben_1->where('ben_id', '=', $bn_id)->count();
        if($cnt > 0)
        {
            $ben_1->where('ben_id', '=', $bn_id)->update(['ben_status' => $x, 'ben_rep_opr_id' => $opr_id, 'ben_rep_date' => $date_time, 'updated_at' => $date_time]);
           
            $z = 1;
        }
        else
        {
             $z = 2;
             
        }

        return $z;
    }

    public static function checkUserModeandStatus($user_name, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 2;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 3;
            }

        }

        return $z;
    }

    public static function checkAlreadyEntry($bank_acc_no, $msisdn)
    {
        $ben_1 = new UserBeneficiaryDetails;
        
        $z = 0;

        $cnt = $ben_1->where('ben_acc_no', '=', $bank_acc_no)->where('msisdn', '=', $msisdn)
                        ->where(function($query){
                            return $query
                            ->where('ben_status', '=', 'SUCCESS')
                            ->orWhere('ben_status', '=', 'OTP SUCCESS');
                        })->count();

        if($cnt > 0)
        {
           $z = 3;
        }

        return $z;
    }


    public static function reduceBalance($user_name, $trans_id, $data, $surp, $api_code, $r_mode)
    {
        $ben_2 = new UserBeneficiaryAccVerifyDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $date_time = date("Y-m-d H:i:s");

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($surp);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
            
           
            // Insert Record... 
            $ben_2->trans_id = $trans_id;
            $ben_2->api_trans_id = $data['api_trans_id'];
            $ben_2->user_name = $user_name;
            $ben_2->agent_id =  "0";
            $ben_2->msisdn = $data['msisdn'];
            $ben_2->ben_acc_no = $data['beneficiary_account_no'];
            $ben_2->ben_code = $data['beneficiary_bank_code'];
            $ben_2->ben_surplus = $surp;
            $ben_2->user_bal = $net_bal;
            $ben_2->ben_date = $date_time;
            $ben_2->api_code = $api_code;
            $ben_2->ben_mode = $r_mode;
            $ben_2->ben_status = "PENDING";
            $ben_2->ben_rep_opr_id = "";

            $ben_2->save();

            $zx = 1;
            
        }

        return $zx;
    }

    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }

    public static function checkServerBeneficiary($res_content, $user_name, $msisdn)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $beneficiary = [];
        $date_time = date("Y-m-d H:i:s");
        $e = 0;

        $res = (array)json_decode($res_content);

        if (array_key_exists('beneficiary', $res)) {
            $beneficiary =(array) $res['beneficiary'];
        }

        $rx = rand(100,999);

        foreach($beneficiary as $d)
        {
            $b_acc_no =  $d->beneficiary_account_no;
            $b_id = $d->beneficiary_id;
            $b_name = $d->beneficiary_name;
            $b_bank_code = $d->beneficiary_bank_code;
            $b_ifsc_code = $d->beneficiary_ifsc_code;

            $cnt = $ben_1->where('ben_acc_no', '=', $b_acc_no)->where('msisdn', '=', $msisdn)->count();

            if($cnt > 0)
            {
                $ben_1->where('ben_acc_no', '=', $b_acc_no)->where('msisdn', '=', $msisdn)
                            ->update(['ben_status' => 'SUCCESS', 'ben_id' => $b_id,
                            'ben_s_status' => $rx, 'updated_at' => $date_time]);
               $e = 1;
            }
            else if($cnt == 0)
            {
                $ben_1->ben_id = $b_id;
                $ben_1->api_trans_id = "-";
                $ben_1->user_name = $user_name;
                $ben_1->agent_id = "0";
                $ben_1->msisdn = $msisdn;
                $ben_1->ben_acc_no = $b_acc_no;
                $ben_1->ben_acc_name = $b_name;
                $ben_1->bank_code = $b_bank_code;
                $ben_1->bank_ifsc = $b_ifsc_code;
                $ben_1->api_code = '0';
                $ben_1->ben_req_status = "done...";
                $ben_1->ben_status = "SUCCESS";
                $ben_1->ben_s_status = $rx;
                $ben_1->trans_id = "0";
                $ben_1->ben_rep_opr_id = "";
                $ben_1->ben_rep_date = $date_time;
    
                $ben_1->save();

                $e = 1;
               
            }
          
        }

        if($e == 1)
        {
            $ben_1->where('msisdn', '=', $msisdn)->where('ben_s_status', '!=', $rx)->delete();
        }

        return $e;
    }

}
