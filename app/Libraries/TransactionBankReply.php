<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;
use DB;

use App\Libraries\PercentageCalculation;
use App\Libraries\BankTransferInfo;
use App\Libraries\BankRemitter;
use App\Libraries\BankTransferReply;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\ApiProviderResultDetails;

use App\Models\ServerDetails;
use App\Models\AllTransaction;
use App\Models\AllRequest;

class TransactionBankReply
{
	public static function storeTransferupdate_1($res_content, $trans_id)
    {
        
        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $date_time = date("Y-m-d H:i:s");

        //$trans_s = [];
        //$trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            
        }

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE all_transactions SET rech_status = '".$s_status."', reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE all_transactions SET rech_status = 'OTP ".$s_status."', reply_id = '".$rep_trans_id."',
                                                        WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                self::success_update($trans_id, $rep_trans_id, $bank_trans_id, "0", "1");
               
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

     public static function storeTransferupdate_2($res_content, $trans_id)
    {
        $ban_1 = new AllTransaction;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

       
        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)->update(['reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['rech_status' => $s_status, 'rech_option' => '1', 'reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['rech_status' => $trans_status, 'rech_option' => '1', 'reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }

    // Transfer greater than 5000
    public static function storeTransferupdate_1_1($res_content, $trans_id)
    {
        $ben_1 = new AllTransaction;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_1_1");

        $rep_trans_id = "0";
        $message = "NONE";
        $remarks = "";
        $otp_status = "";
        $bank_trans_id = "0";
        $success_amount = "0";
        $date_time = date("Y-m-d H:i:s");

        $trans_s = [];
        $trans_f = [];

        foreach($data as $d)
        {
            if($d[0] == "trans_id")
            {
                $rep_trans_id = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            if($d[0] == "remarks")
            {
                $remarks = $d[1];
            }
            if($d[0] == "otp_status")
            {
                $otp_status = $d[1];
            }
            if($d[0] == "success_amount")
            {
                $success_amount = $d[1];
            }
            if($d[0] == "transfer_data")
            {
                $trans_s= $d[1];
            }
            if($d[0] == "failed_data")
            {
                $failed_data= $d[1];
            }
            
        }

        $bank_trans_id = self::getSubResultData($trans_s);

        $bank_trans_id = $success_amount." :".$bank_trans_id;

        // Updation
        if($s_status == "FAILURE")
        {
            self::failure_update($trans_id, $message);
        }
        else if($s_status == "PENDING")
        {
            DB::statement("UPDATE all_transactions SET rech_status = '".$s_status."', rech_option = '1',
                                        reply_id ='".$message."' WHERE trans_id = '".$trans_id."'");
        }
        else if($s_status == "SUCCESS")
        {
           
            if($otp_status == "1")
            {
                DB::statement("UPDATE all_transactions SET rech_status = 'OTP ".$s_status."', rech_option = '1',
                                                        reply_id ='".$remarks."' WHERE trans_id = '".$trans_id."'");
            }
            else if($otp_status == "0")
            {
                self::success_update($trans_id, $rep_trans_id, $bank_trans_id, $success_amount, "2");
            }
            
        }
        
        
        return array($s_status, $message, $otp_status, $rep_trans_id, $remarks);
        
    }

    public static function storeTransferupdate_2_2($res_content, $trans_id)
    {
        $ban_1 = new AllTransaction;

        $result = self::resultFormatter($res_content);

        $result = json_decode($result, true);

        list($s_status, $s_opr_id, $data) = self::getBankResult($result, "TRN_API_2_2");

        $bank_trans_id = "0";
        $trans_status = "None";
        $message = "NONE";

        foreach($data as $d)
        {
            if($d[0] == "bank_trans_id")
            {
                $bank_trans_id = $d[1];
            }
            if($d[0] == "trans_status")
            {
                $trans_status = $d[1];
            }
            if($d[0] == "message")
            {
                $message = $d[1];
            }
            
        }

        

        // Updation
        if($s_status == "FAILURE")
        {
            $ban_1->where('trans_id', '=', $trans_id)->update(['reply_id' => $message]);
            
        }
        else if($s_status == "PENDING")
        {
            $ban_1->where('trans_id', '=', $trans_id)->update(['reply_id' => $message]);
            
        }
        else if($s_status == "SUCCESS")
        {
            if($trans_status == "Success")
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['rech_status' => $s_status, 'rech_option' => '1', 'reply_id' => $bank_trans_id]);
            }
            else
            {
                $ban_1->where('trans_id', '=', $trans_id)->update(['rech_status' => $trans_status, 'rech_option' => '1', 'reply_id' => $message]);
            }
            
        }
        
        return array($s_status, $trans_status, $message);
        
    }

     /**
     * Success Update
     */

    public static function success_update($trans_id, $rep_trans_id, $bank_trans_id, $res_amt, $cc)
	{
		
        $rech = new AllTransaction;
        $net_2 = new NetworkDetails;
        
        
        $date_time = date("Y-m-d H:i:s");
        $status = "SUCCESS";
        $user_name = "NONE";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
        if($d1->count() > 0)
        {
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $r_mode = "API";
            }
            else {
                $r_mode = "WEB";
            }

            // Update Data
            DB::statement("UPDATE all_transactions SET rech_status = '".$status."', rech_option = '1',
                                                        reply_id ='".$bank_trans_id."' WHERE trans_id = '".$trans_id."'");
            $zx++;

            if($r_mode == "API" )
            {
                list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                // API Partner Reply URL Parameter Setting...
                if (strpos($api_s_url, '<trid>') !== false) {
                    $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                }
                if (strpos($api_s_url, '<oprid>') !== false) {
                    $api_s_url = str_replace("<oprid>", $bank_trans_id, $api_s_url);
                }
                if (strpos($api_s_url, '<status>') !== false) {
                    $api_s_url = str_replace("<status>", "SUCCESS", $api_s_url);
                }

                if($api_s_url != "")
                {
                    BankRemitter::runURL($api_s_url, "RESPONSE");
                }
            }

            if($cc == "2")
            {
                if(floatval($rech_amount) > floatval($res_amt))
                {
                    $red_amount = floatval($rech_amount) - floatval($res_amt);
                    $red_amount = round($red_amount, 2);
                    self::refundBalance($user_name, $red_amount);
                }
            }
   
        }
 
        return $zx;
       
    }

     /**
     * Failure Update
     */

    public static function failure_update($trans_id, $opr_id)
	{
		
        $rech = new AllTransaction;
        $net_2 = new NetworkDetails;
        
        $date_time = date("Y-m-d H:i:s");
        $status = "FAILURE";
        
        $zx = 0;
        $z = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->ret_total;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }
            else {
                $mode = "WEB";
            }

            $zx = self::insertFailure($trans_id, $opr_id, $date_time);

            $rech->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                        ->update([ 'rech_option' => '2', 'updated_at' => $date_time]);

            if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = self::getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", "FAILURE", $api_f_url);
                    }

                    if($api_f_url != "")
                    {
                        BankRemitter::runURL($api_f_url, "RESPONSE");
                    }
                }

                $z = 1;
            }

        }
 
        return $z;
       
    }

    public static function insertFailure($trans_id, $opr_id, $date_time)
    {
        $rech = [];
        $c1 = 0;

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            // Array
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $d1[0]->api_trans_id;

            $rech['parent_name'] = $d1[0]->parent_name;
            $rech['child_name'] = $d1[0]->child_name;
            $rech['user_name'] =  $d1[0]->user_name;

            $rech['net_code'] =  $d1[0]->net_code;
            $rech['rech_mobile'] = $d1[0]->rech_mobile;
            $rech['rech_amount'] = $d1[0]->rech_amount;
            $rech['rech_bank'] = "-";
            $rech['ret_net_per'] = $d1[0]->ret_net_per;
            $rech['ret_net_surp'] = $d1[0]->ret_net_surp;
            $rech['ret_total'] = $d1[0]->ret_total;
            $rech['ret_bal'] = '0';

            $rech['dis_net_per'] = $d1[0]->dis_net_per;
            $rech['dis_net_surp'] = $d1[0]->dis_net_surp;
            $rech['dis_total'] = $d1[0]->dis_total;
            $rech['dis_bal'] = '0';

            $rech['sup_net_per'] = $d1[0]->sup_net_per;
            $rech['sup_net_surp'] = $d1[0]->sup_net_surp;
            $rech['sup_total'] = $d1[0]->sup_total;
            $rech['sup_bal'] = '0';

            $rech['trans_type'] = $d1[0]->trans_type;
            $rech['api_code'] = $d1[0]->api_code;
            $rech['rech_date'] = $d1[0]->rech_date;
            $rech['rech_status'] = "FAILURE";
            $rech['rech_option'] = "2";
            $rech['reply_date'] = $date_time;
            $rech['reply_id'] = $opr_id;
            
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;

            list($rx_bal, $zx) = self::update_balance($d1[0]->user_name, $d1[0]->ret_total, $date_time);

            if($zx > 0)
            {
                $rech['ret_bal'] = $rx_bal;

                if($d1[0]->child_name != "NONE")
                {
                    if($d1[0]->dis_total != "0.00" && $d1[0]->dis_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->child_name, $d1[0]->dis_total, $date_time);
                        $rech['dis_bal'] = $dx_bal;
                    }
                }
                if($d1[0]->sup_total != "0.00" && $d1[0]->sup_total != "0") {
                        list($dx_bal, $zy) = self::update_balance($d1[0]->parent_name, $d1[0]->sup_total, $date_time);
                        $rech['sup_bal'] = $dx_bal;
                }

                $c1 = AllTransaction::insert($rech);
               
            }
        }

        return $c1;
    }

    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }



    //result API functions
     public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }


    public static function getBankResult($result, $api_line)
    {
        $dx = [];
        $s_status = "PENDING";
        $s_opr_id = "NONE";
        $data = [];

        list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine($api_line);
        if($api_code > 0)
        {
            $dx = self::getAPIResultData($api_code);

            if(!empty($dx))
            {
                $j = 0;
                foreach($dx as $d)
                {
                    if($d[0] == "STATUS 1")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "SUCCESS";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "STATUS 2")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key && $d[2] == $value)
                            {
                                $s_status = "FAILURE";
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "OPERATOR_ID")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $s_opr_id = $value;
                                break;
                            }
                        }
    
                    }
                    else if($d[0] == "DATA")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                    else if($d[0] == "ARRAY")
                    {
                        foreach($result as $key => $value)
                        {
                            if($d[1] == $key)
                            {
                                $data[$j][0] = $key;
                                $data[$j][1] = $value;
                                $j++;
                                break;
                            }
                        }
                    }
                }
            }
        }

         
        return array($s_status, $s_opr_id, $data);
    }

    public static function getAPIResultData($api_code)
    {
        $api_4 = new ApiProviderResultDetails;

        $dx = [];
        
        $j = 0;
        if($api_code != "0")
        {
            $d1 = $api_4->select('api_res_para_name', 'api_res_para_field', 'api_res_para_value')->where('api_code', '=', $api_code)->get();
            if($d1->count() > 0)
            {
                foreach($d1 as $d)
                {
                    $dx[$j][0] = $d->api_res_para_name;
                    $dx[$j][1] = $d->api_res_para_field;
                    $dx[$j][2] = $d->api_res_para_value;
                    $j++;
                }
            }
        }

        return $dx;
    }


    public static function getSubResultData($res_content)
    {
        $bank_trans_id = "done..";

        //$result = self::resultFormatter($res_content);

        $bank_trans_id = "[";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new2.txt', print_r($res_content, true), FILE_APPEND);

        foreach($res_content as $d)
        {
            $t1 = "";
            $t2 = "";

            if (is_object($d)) {
                if(isset($d->bank_trans_id))
                {
                    $t1 = $d->bank_trans_id;
                }
                if(isset($d->amount))
                {
                    $t2 = $d->amount;
                }
            }
            else
            {
                if (array_key_exists('bank_trans_id', $d)) {
                    $t1 = $d['bank_trans_id'];
                }
                if (array_key_exists('amount', $d)) {
                    $t2 = $d['amount'];
                }
            }

            $bank_trans_id =  $bank_trans_id . '{"bank_trans_id":"'.$t1.'","amount":"'.$t2.'"}, ';
            
        }

        $bank_trans_id= rtrim($bank_trans_id,", ");

        $bank_trans_id = $bank_trans_id."]";
        
        file_put_contents(base_path().'/public/sample/bank_trans_new1.txt', $bank_trans_id, FILE_APPEND);

        return $bank_trans_id;
    }

    public static function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }
}