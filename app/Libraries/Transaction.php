<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;
use App\Libraries\BankRemitter;
use App\Libraries\TransactionDirectReply;
use App\Libraries\TransactionIndirectReply;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\BonrixApiResultDetails;

use App\Models\ServerDetails;
use App\Models\AllTransaction;
use App\Models\AllRequest;


class Transaction
{

    /**
     * Add RechargeBill Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function recharge_mobile($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_code = 0;
        $api_url = "NONE";
        $api_data = [];
        $api_method = "GET";
        $b_url = "";
        $res_content = "NONE";
        $resx = new stdClass();
        $rech1 = [];

        // Modals
        $bon_1 = new BonrixApiResultDetails;
        
       
        // Post Data
        $r_mode = $data['mode'];
        $rech_type = "RECHARGE";

        if($r_mode == "WEB") {
            $cat = "VBR";
        }
        else if($r_mode == "API") {
            $cat = "VBA";
        }
        else if($r_mode == "GPRS") {
            $cat = "VBG";
           
        }
        else if($r_mode == "SMS") {
            $cat = "VBS";
        }
        
        
        $net_type_code = $data['net_type_code'];
        $net_code = $data['net_code'];
        $user_mobile = $data['user_mobile'];
        $user_amount = $data['user_amount'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");

        $trans_id = $cat.rand(10000000,99999999);


        // 1.Check User Balance
        if(floatval($user_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }

        // Get Network Line
        
        if ($r_mode == "API") {
            list($api_code, $v) = ApiUrl::getApiNetworkLine($net_code, $user_amount, $user_name);
        }
        else
        {
            list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        }
        
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 3;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 4;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 5;     // Account Balance is low from Setup Fee...
        }


        // Check Mobile Status
        $zy = self::checkAccountNoStatus($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy == 6)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        // Check 10 Minutes Status
        $zy =self::checkTenMinsCheck($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy > 0)
        {
            $z = 7;
        }

        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 8;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {

            list($api_url, $api_data, $api_method) = ApiUrl::generateAPICommon($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
            

            if($api_url != "NONE")
                $res_code = 200;


            // Retailer Percentage..
            list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $user_amount, $r_mode);

            // Distributor Network Percentage Calculation
            list($parent_type, $parent_name) = self::getParent($user_name);
            list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $user_amount);

            $per_s ="0";
            $peramt_s = "0";
            $sur_s = "0";
            $netamt_s = "0.00";
            $parent_name_s = "NONE";

            if($parent_type == "DISTRIBUTOR") {
                // Super Parent Network Percentage Calculation
                 list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                 list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $user_amount);
            }

            // Array
            $rech = [];
            $rech['trans_id'] = $trans_id;
            $rech['api_trans_id'] = $api_trans_id;

            if($parent_type == "DISTRIBUTOR") {
                $rech['parent_name'] = $parent_name_s;
                $rech['child_name'] = $parent_name;
            }
            else if($parent_type == "SUPER DISTRIBUTOR") {
                $rech['parent_name'] = $parent_name;
                $rech['child_name'] = $parent_name_s;
            }
            else {
                $rech['parent_name'] = "-";
                $rech['child_name'] = "-";
            }

            $rech['user_name'] = $user_name;
            $rech['net_code'] = $net_code;
            $rech['rech_mobile'] = $user_mobile;
            $rech['rech_amount'] = $user_amount;
            $rech['rech_bank'] = "-";
            $rech['ret_net_per'] = PercentageCalculation::convertNumberFormat($per);
            $rech['ret_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
            $rech['ret_total'] = PercentageCalculation::convertNumberFormat($netamt);
            $rech['ret_bal'] = '0';
            if($parent_type == "DISTRIBUTOR") {
                $rech['dis_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech['dis_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech['dis_bal'] = '0';
                $rech['sup_net_per'] = PercentageCalculation::convertNumberFormat($per_s);
                $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                $rech['sup_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                $rech['sup_bal'] = '0';
            }
            else if($parent_type == "SUPER DISTRIBUTOR") 
            {
                $rech['dis_net_per'] = "0";
                $rech['dis_net_surp'] = "0.00";
                $rech['dis_total'] = "0.00";
                $rech['dis_bal'] = '0';
                $rech['sup_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech['sup_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech['sup_bal'] = '0';
            }
            else {
                $rech['dis_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech['dis_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech['dis_bal'] = '0';
                $rech['sup_net_per'] = "0.00";
                $rech['sup_net_surp'] = "0.00";
                $rech['sup_total'] = "0.00";
                $rech['sup_bal'] = '0';
            }
            $rech['trans_type'] = "RECHARGE";
            $rech['api_code'] = $api_code;
            $rech['rech_date'] = $date_time;
            $rech['rech_status'] = "PENDING";
            $rech['rech_option'] = "0";
            $rech['reply_date'] = $date_time;
            $rech['reply_id'] = "-";
            
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;

            list($rx_bal, $zx) = self::update_balance($user_name, $netamt, $date_time);

            if($zx > 0)
            {
                $rech['ret_bal'] = $rx_bal;

                 if($parent_type == "DISTRIBUTOR") {

                    list($dx_bal, $zy) = self::update_balance($parent_name, $netamt_d, $date_time);
                    list($sx_bal, $zz) = self::update_balance($parent_name_s, $netamt_s, $date_time);
                    $rech['dis_bal'] = $dx_bal;
                    $rech['sup_bal'] = $sx_bal;
                }
                else if($parent_type == "SUPER DISTRIBUTOR") 
                {
                    list($sx_bal, $zz) = self::update_balance($parent_name, $netamt, $date_time);
                    $rech['sup_bal'] = $sx_bal;
                }

                $c1 = AllTransaction::insert($rech);
                if($c1 > 0)
                {
                    if ($res_code == 200)
                    {
                        $req = [];
                        $req['trans_id'] = $trans_id;
                        $req['req_id'] = "-";
                        $req['reply_id'] = "-";

                        $res_content = "";
                        if (strpos($api_url, 'ssh1.bonrix.in') !== false) 
                        {
                            // Bonrix URL
                            $bon_1->trans_id = $trans_id;
                            $bon_1->result_url = $api_url;
                            $bon_1->result_status = "1";
                            $bon_1->save();

                            $req['req_id'] = "done";
                            $req['reply_id'] = "done";

                        }
                        else
                        {
                            try
                            {
                                $client = new \GuzzleHttp\Client();
                                if($api_method == "POST")
                                {

                                    file_put_contents(base_path().'/public/sample/bank_post.txt', $api_url."--".print_r($api_data, true).PHP_EOL, FILE_APPEND);
                                    $params['form_params'] = $api_data;
                                    $res = $client->post($api_url, $params);
                                    $res_content = $res->getBody();


                                }
                                else if($api_method == "GET")
                                {
                                    $res_content = BankRemitter::runURL($api_url, "RECHARGE");
                                }
                            }
                            catch (\GuzzleHttp\Exception\ClientException $e) {
                                $res_content = "{'status':'failure','tnx_id':'API URL error...'}";
                            }
                            

                            $req['req_id'] = $res_content;
                            $req['reply_id'] = "done";
                        }

                        $req['created_at'] = $date_time;
                        $req['updated_at'] = $date_time;

                        $c2 = AllRequest::insert($req);

                       
                        // Direct Reply New
                        $api_type = TransactionDirectReply::getApiResultType($api_code);

                        if($api_type == "DIRECT")
                        {
                            TransactionDirectReply::instantUpdate($res_content, $trans_id, $api_code);
                        }
                        else
                        {
                            // Reply Update
                            TransactionIndirectReply::instantUpdate($res_content, "SUCCESS", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                            TransactionIndirectReply::instantUpdate($res_content, "success", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                            TransactionIndirectReply::instantUpdate($res_content, "Success", url('/')."/pendingreport_success_1/".$trans_id, $trans_id, "1");
                            TransactionIndirectReply::instantUpdate($res_content, "FAILED", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                            TransactionIndirectReply::instantUpdate($res_content, "Failed", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                            TransactionIndirectReply::instantUpdate($res_content, "failed", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                            TransactionIndirectReply::instantUpdate($res_content, "FAILURE", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                            TransactionIndirectReply::instantUpdate($res_content, "failure", url('/')."/pendingreport_failure_1/".$trans_id, $trans_id, "2");
                        }

                        

                       
                    }

                }

            }
            

        }

        return $z;
        
    }



    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    

    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new AllTransaction;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['rech_mobile', '=', $mobile], 
                                ['rech_amount', '=', $amount], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public static function checkTenMinsCheck($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new AllTransaction;
        
        $z = 0;

        $d1 = $rech_1->select('rech_date')->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'SUCCESS']])->orderBy('id', 'desc')->limit(1)->get();
        if($d1->count() > 0)
        {
            $old_d = $d1[0]->rech_date;
            $cur_d = date("Y-m-d H:i:s");

            $o_d = new DateTime($old_d);
            $c_d = new DateTime($cur_d);

            $dif = $o_d->diff($c_d);

            $minutes = $dif->days * 24 * 60;
            $minutes += $dif->h * 60;
            $minutes += $dif->i;

            if($minutes < 10)
                $z = 1;
        }

        return $z;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }


    /**
     * Insertion Process..............................................
     *
     */
    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }

  
}
