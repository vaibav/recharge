<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;
use App\Models\UserBankTransferDetails;

use App\Models\AllTransaction;
use App\Models\AllRequest;

class RechargeReportAdmin
{
    //
    public static function getRechargeDetailsAdmin()
    {
        $tran_1 = new TransactionAllDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment'])
                    ->where('trans_type', '=', 'WEB_RECHARGE')
                    ->orWhere('trans_type', '=', 'API_RECHARGE')
                    ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                    ->orWhere('trans_type', '=', 'BANK_TRANSFER')
                    ->orderBy('id', 'desc')
                    ->limit(15)->get(); 
        
      //  return response()->json($d1, 200);
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    $api_name = "";
                    $reply_id = "NA";
                    $reply_date = "";
                    if($d->newrecharge2 != null)
                    {
                        foreach($d3 as $r)
                        {
                            if($d->newrecharge2->api_code == $r->api_code)
                                $api_name = $r->api_name;
                        }

                        $reply_id = $d->newrecharge2->reply_opr_id;
                        $reply_date = $d->newrecharge2->reply_date;
                    }
                   

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $u_bal = $d->newrecharge1[0]->user_balance;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                        $str = $str."<tr>";
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $u_bal = $d->newrecharge1[1]->user_balance;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                    }
                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }

                    $mode = "WEB";
                    if($d->trans_id != "")
                    {
                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                        $r_l = $matches[0][0];

                        $r_l = substr($r_l, -1);

                        if($r_l == "R")
                            $mode = "WEB";
                        else if($r_l == "A")
                            $mode = "API";
                        else if($r_l == "G")
                            $mode = "GPRS";
                        else if($r_l == "S")
                            $mode = "SMS";

                    }

                  
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->user_name."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_mobile."<br>".$net_name."</td>";
                   
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_amount."</td>";
                    if($d->trans_option == 1)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_net_per;
                        $str = $str."--".$d->newrecharge1[0]->rech_net_per_amt."";
                        $str = $str."--".$d->newrecharge1[0]->rech_net_surp."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."<br>".$mode."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>O:".$d->newrecharge1[0]->rech_date."<br>C:".$reply_date."</td>";
                        
                    }
                    else if($d->trans_option == 2)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[1]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                       
                    }
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>O:".number_format($o_bal,2, ".", "")."<br>C:".number_format($u_bal,2, ".", "")."</td>";
                   
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                    
                }
                
               
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->billpayment[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                

               
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->user_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."<br>".$net_name."</td>";
                
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                    $str = $str."--".$d->billpayment[0]->con_net_per_amt."";
                    $str = $str."--".$d->billpayment[0]->con_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."<br>".$mode."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>O:".$d->billpayment[0]->created_at."<br>C:".$d->billpayment[0]->reply_date."</td>";
                   
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                   
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>O:".number_format($o_bal,2, ".", "")."<br>C:".number_format($u_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td></tr>";
                
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->moneytransfer[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                    
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                

               
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->user_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->bk_acc_no."<br>".$net_name."</td>";
               
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_per;
                    $str = $str."--".$d->moneytransfer[0]->mt_per_amt."";
                    $str = $str."--".$d->moneytransfer[0]->mt_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."<br>".$mode."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>O:".$d->moneytransfer[0]->mt_date."<br>C:".$d->moneytransfer[0]->reply_date."</td>";
                    
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>O:".number_format($o_bal,2, ".", "")."<br>C:".number_format($u_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td></tr>";
                
            }                                        
                      
            $j++;
        }
        
        return $str; 
    }

    public static function adminDashboard()
    {
        $rech_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $d1 = $rech_1->orderBy('id', 'desc')->limit(15)->get(); 
        
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
            $net_name = "";
            $api_name = "";
            $rech_status = "";
            $status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;
            $mode = "WEB";


            foreach($d2 as $r)
            {
                if($d->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            foreach($d3 as $r)
            {
                if($d->api_code == $r->api_code)
                    $api_name = $r->api_name;
            }

            $u_bal = $d->ret_bal;
            $r_tot = $d->ret_total;
            $rech_status = $d->rech_status;
            $rech_option = $d->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) - floatval($r_tot);
                $str = $str."<tr style='background-color:#E8DAEF;'>";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }


            if($d->trans_id != "")
            {
                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                $r_l = $matches[0][0];

                $r_l = substr($r_l, -1);

                if($r_l == "R")
                    $mode = "WEB";
                else if($r_l == "A")
                    $mode = "API";
                else if($r_l == "G")
                    $mode = "GPRS";
                else if($r_l == "S")
                    $mode = "SMS";

            }

            //percentage amount
            $per = floatval($d->ret_net_per);
            $peramt1 = 0;
            $peramt2 = 0;
            if(floatval($per) != 0)
            {
                $peramt1 = floatval($per) / 100;
                $peramt1 = round($peramt1, 4);
        
                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                $peramt2 = round($peramt2, 2);
            }

            if($d->trans_type == "REMOTE_PAYMENT" || $d->trans_type == "FUND_TRANSFER" || $d->trans_type == "SELF_PAYMENT")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->parent_name."-->".$d->user_name."</td>";
            }
            else if($d->trans_type == "REFUND_PAYMENT")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."-->".$d->parent_name."</td>";
            }
            else {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
            }
            
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."<br>".$net_name."</td>";
           
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
            if($d->rech_status == "PENDING" || $d->rech_status == "SUCCESS")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                $str = $str."--".$peramt2."";
                $str = $str."--".$d->ret_net_surp."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."<br>".$mode."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>O:".$d->rech_date."<br>C:".$d->reply_date."</td>";
                
            }
            else if($d->rech_status == "FAILURE")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
               
            }
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";

            if($d->trans_type == "REMOTE_PAYMENT" || $d->trans_type == "FUND_TRANSFER" || $d->trans_type == "SELF_PAYMENT")
            {
                $o_bal = floatval($u_bal) - floatval($d->ret_total);
            }

            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>O:".number_format($o_bal,2, ".", "");
            $str = $str."<br>C:".number_format($u_bal,2, ".", "")."</td>";
           
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td></tr>";                                     
                      
            $j++;
        }
        
        return $str; 
    }

    public static function getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new TransactionAllDetails;

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'WEB_RECHARGE')
                                ->orWhere('trans_type', '=', 'API_RECHARGE')
                                ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                                ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                            })
                            ->orderBy('id', 'desc')
                            ->limit(30)
                            ->get();

            
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'WEB_RECHARGE')
                                ->orWhere('trans_type', '=', 'API_RECHARGE');
                            })
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->newrecharge1[0]->rech_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->newrecharge1[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->newrecharge1[0]->rech_mobile == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->newrecharge2->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BILL_PAYMENT')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->billpayment[0]->con_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->billpayment[0]->con_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->billpayment[0]->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->moneytransfer[0]->mt_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->moneytransfer[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->moneytransfer[0]->bk_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->moneytransfer[0]->api_code == $u_api_code;
                });
            }
        }

        

        
        
        
        

      /*  if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->newrecharge1->rech_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->newrecharge1->rech_mobile == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->newrecharge1->rech_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }*/

        
        
        
        
        return $dc1; 
         
    }

    public static function getRechargeReportDetails_new_1($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new TransactionAllDetails;

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'WEB_RECHARGE')
                                ->orWhere('trans_type', '=', 'API_RECHARGE')
                                ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                                ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                            })
                            ->orderBy('id', 'desc')
                            ->paginate(30);

            
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'WEB_RECHARGE')
                                ->orWhere('trans_type', '=', 'API_RECHARGE');
                            });

            if($u_name != "" ) 
            {
                $dc1 = $dc1->where('user_name' , $u_name);
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->with(array('newrecharge1' => function($query) use ($u_status)
                {
                    $query->where('rech_status', $u_status);
                }));
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->with(array('newrecharge1' => function($query) use ($u_net_code)
                {
                    $query->where('net_code', $u_net_code);
                }));
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->with(array('newrecharge1' => function($query) use ($u_mobile)
                {
                    $query->where('rech_mobile', $u_mobile);
                }));
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->with(array('newrecharge2' => function($query) use ($u_api_code)
                {
                    $query->where('api_code', $u_api_code);
                }));
            }

            $dc1 = $dc1->orderBy('id', 'desc')->paginate(30);
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BILL_PAYMENT')
                            ->orderBy('id', 'desc')
                            ->paginate(30);

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->billpayment[0]->con_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->billpayment[0]->con_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->billpayment[0]->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->paginate(30);

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->moneytransfer[0]->mt_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->moneytransfer[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->moneytransfer[0]->bk_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->moneytransfer[0]->api_code == $u_api_code;
                });
            }
        }

        
        return $dc1; 
         
    }

    public static function getRechargeReportDetails_new_2($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new AllTransaction;

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->orderBy('id', 'desc')
                            ->paginate(30);

            
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', '=', 'RECHARGE');

            if($u_name != "" ) 
            {
                $dc1 = $dc1->where('user_name' , $u_name);
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status' , $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code' , $u_net_code);
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->where('rech_mobile' , $u_mobile);
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->where('api_code' , $u_api_code);
            }

            $dc1 = $dc1->orderBy('id', 'desc')->paginate(30);
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $dc1 = $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', '=', $rech_type)->orderBy('id', 'desc')
                            ->paginate(30);

            if($u_name != "" ) 
            {
                $dc1 = $dc1->filter(function ($d) use ($u_name){
                    return $d->user_name == $u_name;
                });
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->billpayment[0]->con_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->billpayment[0]->con_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->billpayment[0]->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->paginate(30);

            if($u_name != "" ) 
            {
                $dc1 = $dc1->where('user_name' , $u_name);
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status' , $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code' , $u_net_code);
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->where('rech_mobile' , $u_mobile);
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->where('api_code' , $u_api_code);
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "PAYMENT")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', '=', 'SELF_PAYMENT')
                                ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                            })->orderBy('id', 'desc')
                            ->paginate(30);

            if($u_name != "" ) 
            {
                $dc1 = $dc1->where('user_name' , $u_name);
            }
            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status' , $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code' , $u_net_code);
            }
        }

        
        return $dc1; 
         
    }


    public static function getNewRechargeList()
    {
        $rech_n_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $bank_1 = new UserBankTransferDetails;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        
        $res_tot = 0;
        $ref_tot = 0;
        $rec_tot = 0;
        $trn_tot = 0;
        $trs_tot = 0;
        $trf_tot = 0;
        
        $query = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'RECHARGE')->get();
        $query1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'BANK_TRANSFER')->get();
        

        $res_tot = $query->where('rech_status', '=', 'SUCCESS')->sum('ret_total'); 

        $res_tot = round($res_tot, 2);

       /* $ref_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('rech_status', '=', 'FAILURE')
                                ->where('rech_option', '=', '2')
                                ->sum('rech_total'); */
        $ref_tot = $query->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->sum('ret_total');

        $ref_tot = round($ref_tot, 2);

        $rec_tot = floatval($res_tot) + floatval($ref_tot);


        // Money Transfer
        

        $mes_tot = $query1->where('rech_status', '=', 'SUCCESS')->sum('ret_total'); 
        
        $mes_tot = round($mes_tot, 2);
        
        $mef_tot = $query1->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->sum('ret_total'); 

        $mef_tot = round($mef_tot, 2);

        $mec_tot = floatval($mes_tot) + floatval($mef_tot);

        $trs_tot = $query->where('rech_status', '=', 'SUCCESS')->count();      
                                
        $trf_tot = $query->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->count();

        $trn_tot = floatval($trs_tot) + floatval($trf_tot);
        
        $res_tot = number_format($res_tot, 2, '.', '');
        
        $rec_tot = number_format($rec_tot, 2, '.', '');

                                 
        return array($rec_tot, $res_tot, $ref_tot, $trn_tot, $trs_tot, $trf_tot, $mec_tot, $mes_tot, $mef_tot);
    }

    public static function getRechargeGraphData()
    {
        $rech_n_1 = new AllTransaction;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        $r_date = date('Y-m-d')." 00:00:00";
        
        $ars = [];
        $arf = [];

        $h = date("H");

        $query = $rech_n_1->where('rech_date','>=', $r_date)->orderBy('rech_date', 'asc')->get();

        //echo $query->count()."<br>";
        
        for($i=0; $i<=$h; $i++)
        {
            $f_date = date("Y-m-d")." ".$i.":00:00";

            if($i == $h) {
                $t_date = date("Y-m-d H:i:s");
            }
            else {
                $t_date = date("Y-m-d")." ".$i.":59:59";
            }

            $dt_1=strtotime($f_date);
            $dt_2=strtotime($t_date);

            $res_tot = 0;
            $ref_tot = 0;
            foreach ($query as $d)
            {
                if (strtotime($d->rech_date) >= $dt_1  && strtotime($d->rech_date) <= $dt_2 )
                {
                    if($d->rech_status == "SUCCESS")
                        $res_tot++;
                    else if($d->rech_status == "FAILURE" && $d->rech_option == '2')
                        $ref_tot++;
                }
                    
            }

            $ars[$i] = $res_tot;
            $arf[$i] = $ref_tot;
            
        }
  
        return array($ars, $arf);
    }

}
