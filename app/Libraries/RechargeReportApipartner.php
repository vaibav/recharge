<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

use App\Models\AllTransaction;

class RechargeReportApipartner
{
    //
    public static function getRechargeDetailsApipartner($user_name)
    {
        $tran_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $tran_1->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                        ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(15)->get(); 
        
      //  return response()->json($d1, 200);
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
            
            if($d->trans_type == "RECHARGE")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->net_code == $r->net_code) {
                        $net_name = $r->net_name;
                        break;
                    }
                }

                $reply_id = $d->reply_id;
                $reply_date = $d->reply_date;

                $rech_status = $d->rech_status;
                $rech_option = $d->rech_option;
                $u_bal = $d->ret_bal;
                $r_tot = $d->ret_total;

                if($rech_status == "PENDING" && $rech_option == "0")
                {
                    $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                    $str = $str."<tr>";
                }
                else if($rech_status == "PENDING" && $rech_option == "2")
                {
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                    $str = $str."<tr>";
                }
                else if($rech_status == "FAILURE" && $rech_option == "2")
                {      
                    $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) - floatval($r_tot);
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                    $str = $str."<tr>";
                }

                //percentage amount
                $per = floatval($d->ret_net_per);
                $peramt1 = 0;
                $peramt2 = 0;
                if(floatval($per) != 0)
                {
                    $peramt1 = floatval($per) / 100;
                    $peramt1 = round($peramt1, 4);
            
                    $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                    $peramt2 = round($peramt2, 2);
                }

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                    $str = $str."--".$peramt2."--".$d->ret_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                }
                else if($d->rech_status == "FAILURE")
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->rech_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td></tr>";
                                
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->billpayment[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
               
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->user_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td></tr>";
               
            } 
             if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->moneytransfer[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                    
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->bk_acc_no."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_per;
                    $str = $str."-- ".$d->moneytransfer[0]->mt_per_amt."";
                    $str = $str."--".$d->moneytransfer[0]->mt_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_date."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->mt_per;
                    $str = $str."-- ".$d->moneytransfer[1]->mt_per_amt."";
                    $str = $str."--".$d->moneytransfer[1]->mt_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->api_trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                
            }  
                                                            
            $j++;
        }
        
        return $str; 
    }

    public static function getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new AllTransaction;

        $u_name = strtoupper($u_name);

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'RECHARGE')
                                ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                                ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                            })
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->orderBy('id', 'desc')
                            ->get();

            
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'API_RECHARGE')
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status', '=', $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code', '=', $u_net_code);
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->where('rech_mobile', '=', $u_mobile);
            }
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'BILL_PAYMENT')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->billpayment[0]->con_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->billpayment[0]->con_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->billpayment[0]->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->moneytransfer[0]->mt_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->moneytransfer[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->moneytransfer[0]->bk_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->moneytransfer[0]->api_code == $u_api_code;
                });
            }
        } 

       
        return $dc1; 
         
    }


    
    public static function getRechargeComplaint($f_date, $t_date, $u_name, $u_mobile)
    {
        $tran_1 = new TransactionAllDetails;

        $u_name = strtoupper($u_name);

        if($f_date != "" && $t_date != "")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'API_RECHARGE')
                            ->orderBy('id', 'desc')
                            ->get(); 
        }
        else
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'API_RECHARGE')
                            ->orderBy('id', 'desc')
                            ->get(); 
        }
       
        if($u_mobile != "" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_mobile){
                return $d->newrecharge1[0]->rech_mobile == $u_mobile;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        return $dc2; 
         
    }

    public static function getRechargeList($user_name)
    {
        $rech_n_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        
        $res_tot = 0;
        $ref_tot = 0;
        $rec_tot = 0;
        $trn_tot = 0;
        $trs_tot = 0;
        $trf_tot = 0;
        

        $res_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('user_name', '=', $user_name)
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('ret_total'); 

        $res_tot = round($res_tot, 2);

        $ref_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('user_name', '=', $user_name)
                                ->where('rech_status', '=', 'FAILURE')
                                ->where('rech_option', '=', '2')
                                ->sum('ret_total'); 

        $ref_tot = round($ref_tot, 2);

        $rec_tot = floatval($res_tot) + floatval($ref_tot);

        
        $trs_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('user_name', '=', $user_name)
                                ->where('rech_status', '=', 'SUCCESS')->count();      
                                
        $trf_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('user_name', '=', $user_name)
                                ->where('rech_status', '=', 'FAILURE')
                                ->where('rech_option', '=', '2')->count();

        $trn_tot = floatval($trs_tot) + floatval($trf_tot);
        
        $res_tot = number_format($res_tot, 2, '.', '');
        
        $rec_tot = number_format($rec_tot, 2, '.', '');

                                 
        return array($rec_tot, $res_tot, $ref_tot, $trn_tot, $trs_tot, $trf_tot);
    }

    public static function getRechargeGraphData($user_name)
    {
        $rech_n_1 = new AllTransaction;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        
        $ars = [];
        $arf = [];

        $h = date("H");
        
        for($i=0; $i<=$h; $i++)
        {
            $f_date = date("Y-m-d")." ".$i.":00:00";
           
            if($i == $h)
            {
                $t_date = date("Y-m-d H:i:s");
            }
            else
            {
                $t_date = date("Y-m-d")." ".$i.":59:59";
            }
            
            $res_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('user_name', '=', $user_name)
                                ->where('rech_status', '=', 'SUCCESS')
                                ->count(); 

            $res_tot = round($res_tot);

            $ref_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                    ->where('user_name', '=', $user_name)
                                    ->where('rech_status', '=', 'FAILURE')
                                    ->where('rech_option', '=', '2')
                                    ->count(); 

            $ref_tot = round($ref_tot);
            
            $ars[$i] = $res_tot;
            $arf[$i] = $ref_tot;
            
        }
  
        return array($ars, $arf);
    }

}
