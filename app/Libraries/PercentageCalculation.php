<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserNetworkDetails;
use App\Models\NetworkSurplusDetails;
use App\Models\NetworkPackageDetails;
use App\Models\UserAccountDetails;

class PercentageCalculation
{
    //
    
    /**
     * Percentage Calculation Functions..............................................
     *
     */
    public static function getRetailerPercentage($user_name, $net_code, $amount, $r_mode)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        if($r_mode == "WEB" || $r_mode == "GPRS")
        {
            list($per, $sur) = self::get_packagePercentage($user_name, $net_code, $amount);

            file_put_contents(base_path().'/public/sample/new_per.txt', $user_name."--".$net_code."--".$amount."--".$per."--".$sur.PHP_EOL, FILE_APPEND);

            if($per == "-0.1111")
            {
                // Check User Mode (Enable -web)
                $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
                if($d1->count() > 0)
                {
                   $per = $d1[0]->user_net_per;
                }

                $sur = self::getSurplusCharge($net_code, $amount,"RETAILER");
            }
        }
        else if($r_mode == "API")
        {
             // Check User Mode (Enable -web)
            $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
            if($d1->count() > 0)
            {
               $per = $d1[0]->user_net_per;
               //$sur = $d1[0]->user_net_surp;
            }

            $sur = self::getSurplusCharge($net_code, $amount, "API PARTNER");
        }


        // Calculation
        list($peramt1, $peramt2, $netamt) =self::recharge_calculation($amount, $amount, $per, $sur);

        $peramt2 = round($peramt2, 2);

        return array($per, $peramt2, $sur, $netamt);
    }

    public static function getSurplusCharge($net_code, $amount, $user_type)
    {
        $net_2 = new NetworkSurplusDetails;
        $surp = 0;
        
        $d1 = $net_2->select('from_amount', 'to_amount', 'surplus_charge')
                        ->where('net_code', '=', $net_code)
                        ->where('user_type', '=', $user_type)
                        ->get();

        foreach($d1 as $d)
        {
            $f_amt = $d->from_amount;
            $t_amt = $d->to_amount;

            if(floatval($f_amt) <= floatval($amount) && floatval($t_amt) >= floatval($amount))
            {
                $surp = $d->surplus_charge;
                break;
            }
            
            //file_put_contents(base_path().'/public/sample/surplus.txt', $net_code."--".$user_type."--".$amount."---".$f_amt."---".$t_amt."---".$surp."--".PHP_EOL, FILE_APPEND);
        }
        
        return $surp;
    }

   
    public static function recharge_calculation($amt, $amt1, $per, $sur)
    {
        $peramt1 = 0;
        $peramt2 = 0;

        if(floatval($per) != 0)
        {
            $peramt1 = floatval($per) / 100;
            $peramt1 = round($peramt1, 4);
    
            $peramt2 = floatval($amt) * floatval($peramt1);
            $peramt2 = round($peramt2, 2);
            
            file_put_contents(base_path().'/public/sample/percentage.txt', $amt."--".$amt1."--".$per."---".$sur."---".$peramt1."---".$peramt2."--".PHP_EOL, FILE_APPEND);
        }
       

        $netamt = (floatval($amt1) - floatval($peramt2)) +floatval($sur);
        $netamt = round($netamt, 2);

        return array($peramt1, $peramt2, $netamt);
    }

   

    public static function getDistributorPercentage($user_name, $net_code, $amount)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
        if($d1->count() > 0)
        {
           $per = $d1[0]->user_net_per;
           //$sur = $d1[0]->user_net_surp;
        }

        $sur = self::getSurplusCharge($net_code, $amount,'');

        // Calculation
        list($peramt1, $peramt2, $netamt) = self::recharge_calculation($amount, 0, $per, $sur);

        return array($per, $peramt2, $sur, $netamt);
    }

    public static function net_getPercentage($user_name, $parent, $gr_parent, $net_code)
    {
        $ret_per = 0;
        $dis_per = 0;
        $sdis_per = 0;

        $dx =[$user_name, $parent, $gr_parent];

        $d1 = UserNetworkDetails::select('user_name', 'user_net_per')
                                    ->where('net_code', $net_code)
                                    ->where(function($query) use ($dx){
                                        return $query
                                        ->where('user_name', '=', $dx[0])
                                        ->orWhere('user_name', '=', $dx[1])
                                        ->orWhere('user_name', '=', $dx[2]);
                                    })->get();
    
        foreach($d1 as $d)
        {
            if($d->user_name == $user_name) {
                $ret_per = $d->user_net_per;
            }
            if($d->user_name == $parent) {
                $dis_per = $d->user_net_per;
            }
            if($d->user_name == $gr_parent) {
                $sdis_per = $d->user_net_per;
            }
        }

        return array($ret_per, $dis_per, $sdis_per);
    }

    public static function get_packagePercentage($user_name, $net_code, $amount)
    {
        $pack_id = "0";
        $per = "-0.1111";
        $surp = "0";

        $d1 = UserAccountDetails::select('pack_id')->where('user_name', '=', $user_name)->get();

        if($d1->count() > 0)
        {
            $pack_id = $d1[0]->pack_id;
            list($per, $surp) = self::getPercentage($net_code, $amount, $pack_id);
        }

        return array($per, $surp);
    }

    public static function getPercentage($net_code, $amount, $pack_id)
    {
        $surp = 0;
        $per = "";
        
        $d1 = NetworkPackageDetails::select('from_amt', 'to_amt', 'net_per', 'net_surp')
                        ->where('pack_id', '=', $pack_id)
                        ->where('net_code', '=', $net_code)
                        ->get();

        foreach($d1 as $d)
        {
            $f_amt = $d->from_amt;
            $t_amt = $d->to_amt;

            if(floatval($f_amt) <= floatval($amount) && floatval($t_amt) >= floatval($amount))
            {
                $per = $d->net_per;
                $surp = $d->net_surp;
                break;
            }
            
            //file_put_contents(base_path().'/public/sample/surplus.txt', $net_code."--".$user_type."--".$amount."---".$f_amt."---".$t_amt."---".$surp."--".PHP_EOL, FILE_APPEND);
        }
        
        return array($per, $surp);
    }




    public static function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }


    

}
