<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\NetworkDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;

use App\Models\AllTransaction;

class RechargeReportSuperDistributor
{
    //
    
    public static function getRechargeDetailsSuperDistributor($user_name)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $rech_2 = new UserRechargeBillDetails;
        $rech_3 = new UserBankTransferDetails;

        $net_2 = new NetworkDetails;
    
        $user_name = strtoupper($user_name);

        $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->orderBy('id', 'desc')->limit(15)->get();

        $d3 = $net_2->select('net_code','net_name')->get();

        $j = 0;
        $str = "";
        foreach($d1 as $f)
        {
                                                   
            $net_name = "";
            foreach($d3 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;
           

            if($f->rech_type == "RECHARGE")
            {
                $reply_id = $f->newparentrecharge2->reply_opr_id;
                $reply_date =$f->newparentrecharge2->reply_date;
            }
            else if($f->rech_type == "BILL_PAYMENT")
            {
                $d2 = $rech_2->select('reply_opr_id', 'reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->reply_opr_id;
                    $reply_date =$d2[0]->reply_date;
                }
                
            }
            else if($f->rech_type == "BANK_TRANSFER")
            {
                $d2 = $rech_3->select('mt_reply_id', 'mt_reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->mt_reply_id;
                    $reply_date =$d2[0]->mt_reply_date;
                }
                
            }

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light'><i class='small material-icons '>check</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            
            if($z == 0)
            {
                $str = $str. "<tr>";
                if($f->child_name != "NONE")
                {
                    $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->child_name."</td>";
                }
                else
                {
                    $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>-</td>";
                }
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_net_per."--".$f->rech_net_per_amt."";
                $str = $str. "--".$f->rech_net_surp."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 130px;word-break: break-word;'>".$reply_id."</div></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_type."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                
            }
            else
            {
                $str = $str. "<tr style='background-color:#E8DAEF;'>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
            }
            $str = $str. "</tr>";
                                                    
            $j++;
        }
        
        

        return $str;
    }

    public static function getRechargeReportDetails_new($f_date, $t_date, $rech_type, $user_name, $u_name1, $u_status, $u_mobile, $u_amount, $u_net_code, $u_type)
    {
        $rech_n_1 = new AllTransaction;
        

        $user_name = strtoupper($user_name);

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $d1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date]);
           
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $d1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->where('trans_type', '=', 'RECHARGE');

        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $d1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BANK_TRANSFER');
 
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $d1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BILL_PAYMENT');
 
        }

       
        if($user_name != "-" && $user_name != "") 
        {
            if($u_type == "DISTRIBUTOR") {
                $d1 = $d1->where('child_name' , $user_name);
            }
            else if($u_type == "SUPER DISTRIBUTOR") {
                $d1 = $d1->where('parent_name' , $user_name);
            }
            
        }
        
        if($u_net_code != "-" && $u_net_code != "") 
        {
            $d1 = $d1->where('net_code', $u_net_code);
        }
        
        if($u_status != "-" && $u_status != "") 
        {
            $d1 = $d1->where('rech_status', $u_status);
        }
       
        if($u_mobile != "" ) 
        {
            $d1 = $d1->where('rech_mobile', $u_mobile);
        }
        
        if($u_amount != "" ) 
        {
            $d1 = $d1->where('rech_amount', $u_amount);
        }
       
        $d1 = $d1->orderBy('id', 'desc')->paginate(30);

        return $d1; 
    }


   
}
