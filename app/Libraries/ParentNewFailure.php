<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeNewParentDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\TransactionAllParentDetails;

class ParentNewFailure
{
       
    public static function updateParentPaymentFailure($trans_id, $status, $date_time, $trans_mode)
     {
         $rech_3 = new UserRechargeNewParentDetails;
         $uacc_1 = new UserAccountBalanceDetails;
         $zx = 0;
 
         $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get();
         if($dx3->count() > 0)
         {
             
            foreach($dx3 as $r)
            {
                // Start Failure
                // Update Data
                $rech_total = $r->rech_total;
                $user_name1 = $r->user_name;
                $parent_name = $r->parent_name;
                $user_name = $parent_name;

                $net_bal = 0;
                // Update Failure
                $d5 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                if($d5->count() > 0)
                {
                    $u_bal = $d5[0]->user_balance;

                    $net_bal = floatval($u_bal) + floatval($rech_total);
                    $net_bal = round($net_bal, 2);
                    $net_bal = self::convertNumberFormat($net_bal);

                    // Update Balance
                    $uacc_1->where('user_name', '=', $user_name)
                                        ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);

                   // Update Recharge Option in parent
                    $rech_3->where([['trans_id', '=', $trans_id], ['parent_name', '=', $user_name], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])
                                   ->update(['rech_option' => '2', 'updated_at' => $date_time]);
                    
                    // Insert Payment Data
                    $pay_1 = array('trans_id' => $r->trans_id,
                                        'api_trans_id' => $r->api_trans_id, 
                                        'parent_name' => $r->parent_name,
                                        'child_name' => $r->child_name,
                                        'user_name' => $r->user_name, 
                                        'net_code' => $r->net_code,
                                        'rech_mobile' => $r->rech_mobile, 
                                        'rech_amount' => $r->rech_amount, 
                                        'rech_net_per' => $r->rech_net_per, 
                                        'rech_net_per_amt' => $r->rech_net_per_amt,
                                        'rech_net_surp' => $r->rech_net_surp, 
                                        'rech_total' => $r->rech_total, 
                                        'user_balance' => $net_bal,
                                        'rech_date' => $date_time,
                                        'rech_status' => $status,
                                        'rech_type' => $r->rech_type,
                                        'rech_mode' => $r->rech_mode,
                                        'rech_option' => "2",
                                        'created_at' => $date_time, 'updated_at' => $date_time);
                    
                    if($rech_3->insert($pay_1))
                    {
                        $zxt = self::insertTransactionParent($trans_id, $user_name, $trans_mode);
                        $zx =1;
                    }
                    
                }
                // End Failure For
            }
             
         }

         return $zx;

    }


    

    public static function insertTransactionParent($trans_id, $user_name, $trans_mode)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $trans_mode;
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public static function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }

}
