<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\CyberBankApi;
use App\Libraries\BankRemitter;
use App\Libraries\CyberBankTransferReply;

use App\Libraries\PercentageCalculation;

use App\Models\UserBankRemitterDetails;
use App\Models\ApiUrlDetails;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\ServerDetails;
use App\Models\AllTransaction;
use App\Models\AllRequest;

class CyberBankTransferNew
{

    /**
     * Fund Transfer Function.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Post Data
        $rechMode       = $data['rechMode'];
        $clientTransId  = $data['clientTransId'];
        $apiTransId     = $data['apiTransId'];
        $remId          = $data['mobileNo'];

        $beneId         = $data['benId'];
        $accountNo      = $data['accountNo'];
        $bankIFSC       = $data['bankIFSC'];
        $bankName       = $data['bankName'];
        $transferMode   = $data['routingType'];
        $transferAmount = $data['amount'];

        $userCode = $data['userCode'];
        $userName = $data['userName'];
        $userBal  = $data['userBal'];

        $dateTime = date("Y-m-d H:i:s");
        $rechType = "BANK_TRANSFER";
        $transApi = "TRN_API_1";
        $tranOp   = "1";

        $status  = 0;
        $message = "NONE";
        $reply   = "-";

        $netCode = self::getNetworkCode("MONEY TRANSFER");          //db--->1

        // 1.Check User Balance
        if(floatval($transferAmount) > floatval($userBal)) {
            return array($status  = 1, "Low User Balance...", $reply);
        }

        // Check Username status
        $z1 = self::checkUserModeandStatus($userName, $userBal, $rechMode);        //db--->2
        if($z1 == 3) {
            return array($status  = 2, "Mode web is not Selected...", $reply);     // Mode web is not Selected...
        }
        else if($z1 == 4) {
            return array($status  = 3, "Account is Inactivated...", $reply);     // Account is Inactivated...
        }
        else if($z1 == 5) {
            return array($status  = 4, "Account Balance is low from Setup Fee...", $reply);     // Account Balance is low from Setup Fee...
        }

        // Check Server Status
        $zy = self::checkServerStatus();        //db---->3
        if (!$zy) {
            return array($status  = 6, "Server is Temporarily shutdown...", $reply);      // Server Shutdown
        }

        //Process Starts............................................
        list($apiCode, $v) =  CyberBankApi::getAgent1NetworkLine("TRN_API_1_1");

        //echo $apiCode;

        if($apiCode <= 0) {
            return array($status  = 5, "API is not Selected...", $reply);      // Server Shutdown
        }

        $remitter = $data['remitter'];

        $data['remitterFirstName'] = $remitter['name'];
        $data['remitterAddress']   = $remitter['address'];
        $data['remitterCity']      = $remitter['city'];
        $data['remitterPincode']   = $remitter['pincode'];


        $apiUrl = CyberBankApi::generateBankAPI($apiCode, $data);

        //echo $apiUrl;

       

        //print_r($data['remitter']);

        // Insertion Process..
        list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($userName, $netCode, $transferAmount, $rechMode);

        // Distributor Network Percentage Calculation
        list($parent_type, $parent_name) = self::getParent($userName);
        list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $netCode, $transferAmount);

        $per_s ="0";
        $peramt_s = "0";
        $sur_s = "0";
        $netamt_s = "0.00";
        $parent_name_s = "NONE";

        if($parent_type == "DISTRIBUTOR") {
            // Super Parent Network Percentage Calculation
             list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
             list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $netCode, $transferAmount);
        }

        // Array
        $rech = [];
        $rech['trans_id'] = $clientTransId;
        $rech['api_trans_id'] = $apiTransId;

        if($parent_type == "DISTRIBUTOR") {
            $rech['parent_name'] = $parent_name_s;
            $rech['child_name'] = $parent_name;
        }
        else if($parent_type == "SUPER DISTRIBUTOR") {
            $rech['parent_name'] = $parent_name;
            $rech['child_name'] = $parent_name_s;
        }
        else {
            $rech['parent_name'] = "-";
            $rech['child_name'] = "-";
        }

        $rech['user_name']    = $userName;
        $rech['net_code']     = $netCode;
        $rech['rech_mobile']  = $accountNo;          // rech_mobile is bank account no
        $rech['rech_amount']  = $transferAmount;
        $rech['rech_bank']    = $bankIFSC."--".$transferMode;
        $rech['ret_net_per']  = PercentageCalculation::convertNumberFormat($per);
        $rech['ret_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
        $rech['ret_total']    = PercentageCalculation::convertNumberFormat($netamt);
        $rech['ret_bal']      = '0';

        if($parent_type == "DISTRIBUTOR") {
            $rech['dis_net_per']  = PercentageCalculation::convertNumberFormat($per_d);
            $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
            $rech['dis_total']    = PercentageCalculation::convertNumberFormat($netamt_d);
            $rech['dis_bal']      = '0';
            $rech['sup_net_per']  = PercentageCalculation::convertNumberFormat($per_s);
            $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
            $rech['sup_total']    = PercentageCalculation::convertNumberFormat($netamt_s);
            $rech['sup_bal']      = '0';
        }
        else if($parent_type == "SUPER DISTRIBUTOR") 
        {
            $rech['dis_net_per']  = "0";
            $rech['dis_net_surp'] = "0.00";
            $rech['dis_total']    = "0.00";
            $rech['dis_bal']      = '0';
            $rech['sup_net_per']  = PercentageCalculation::convertNumberFormat($per_d);
            $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
            $rech['sup_total']    = PercentageCalculation::convertNumberFormat($netamt_d);
            $rech['sup_bal']      = '0';
        }
        else {
            $rech['dis_net_per']  = PercentageCalculation::convertNumberFormat($per_d);
            $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
            $rech['dis_total']    = PercentageCalculation::convertNumberFormat($netamt_d);
            $rech['dis_bal']      = '0';
            $rech['sup_net_per']  = "0.00";
            $rech['sup_net_surp'] = "0.00";
            $rech['sup_total']    = "0.00";
            $rech['sup_bal']      = '0';
        }

        $rech['trans_type']  = "BANK_TRANSFER";
        $rech['api_code']    = $apiCode;
        $rech['rech_date']   = $dateTime;
        $rech['rech_status'] = "PENDING";
        $rech['rech_option'] = "0";
        $rech['reply_date']  = $dateTime;
        $rech['reply_id']    = "-";
        
        $rech['created_at'] = $dateTime;
        $rech['updated_at'] = $dateTime;

        list($rx_bal, $zx) = self::update_balance($userName, $netamt, $dateTime);

        if($zx <= 0)
        {
             return array($status  = 7, "Balance is not updated...", $reply);      // Balance is not updated
        }

        $rech['ret_bal'] = $rx_bal;

        if($parent_type == "DISTRIBUTOR") {

            list($dx_bal, $zy) = self::update_balance($parent_name, $netamt_d, $dateTime);
            list($sx_bal, $zz) = self::update_balance($parent_name_s, $netamt_s, $dateTime);
            $rech['dis_bal'] = $dx_bal;
            $rech['sup_bal'] = $sx_bal;
        }
        else if($parent_type == "SUPER DISTRIBUTOR") 
        {
            list($sx_bal, $zz) = self::update_balance($parent_name, $netamt, $dateTime);
            $rech['sup_bal'] = $sx_bal;
        }

        $c1 = AllTransaction::insert($rech);

        if($c1 <= 0)
        {
             return array($status  = 8, "Record is not inserted...", $reply);      // Record is not inserted...
        }

        $resContent = BankRemitter::runURL($apiUrl, "TRANSFER");

        list($resultStatus, $s_opr_id, $sResult) = CyberBankTransferReply::storeTransferResult($resContent, $clientTransId);

        $req = [];
        $req['trans_id'] = $clientTransId;
        $req['req_id'] = $resContent;
        $req['reply_id'] = $s_opr_id;
        $req['created_at'] = $dateTime;
        $req['updated_at'] = $dateTime;

        $c2 = AllRequest::insert($req);

        if($resultStatus == "SUCCESS")
        {
            return array($status, "Success...", $reply);      // Success
        }
        else if($resultStatus == "FAILURE")
        {
            return array($status  = 8, $sResult, $reply);      // Balance is not updated
        }
        else  if($resultStatus == "PENDING")
        {
            return array($status  = 9, $sResult, $reply);      // Balance is not updated
        }

        // All Process Over...
    }


    public static function getNetworkCode($netName)
    {
        $details = NetworkDetails::select('net_code')->where('net_name', '=', $netName)->first();

        if($details) {
            return $details->net_code;
        }
        return "0";
    }

    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $z = 0;

        // Check User Mode (Enable -web)
        $details = UserAccountDetails::select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->first();
        if($details)
        {
            $user_rec_mode = $details->user_rec_mode;
            $setup = $details->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($details->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }

    public static function checkServerStatus()
    {
        $details = ServerDetails::select('server_status')->first();
        if($details) {
            if($details->server_status == 2)
                return false;
        }
        return true;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }
}