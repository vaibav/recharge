<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

use App\Models\AllTransaction;

class RechargeReportRetailer
{
    //
    public static function getRechargeDetailsRetailer($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment'])
                    ->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'WEB_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(7)->get(); 
        
      // return response()->json($d1, 200);
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }
    
                    $api_name = "";
                    foreach($d3 as $r)
                    {
                        if($d->newrecharge2->api_code == $r->api_code)
                            $api_name = $r->api_name;
                    }
    
                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;
    
                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $u_bal = $d->newrecharge1[0]->user_balance;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                        $str = $str."<tr>";
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $u_bal = $d->newrecharge1[1]->user_balance;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                    }
                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "<button class='btn-floating btn-sm waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "<button class='btn-floating btn-sm waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    
                    $mode = "WEB";
                    if($d->trans_id != "")
                    {
                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
    
                        $r_l = $matches[0][0];
    
                        $r_l = substr($r_l, -1);
    
                        if($r_l == "R")
                            $mode = "WEB";
                        else if($r_l == "A")
                            $mode = "API";
                        else if($r_l == "G")
                            $mode = "GPRS";
                        else if($r_l == "S")
                            $mode = "SMS";
    
                    }

                    $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_mobile."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_amount."</td>";
                    if($d->trans_option == 1)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_net_per;
                        $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->newrecharge1[0]->rech_net_per_amt."";
                        $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->newrecharge1[0]->rech_net_surp."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge2->reply_opr_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_date."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge2->reply_date."</td>";
                    }
                    else if($d->trans_option == 2)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[1]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    }
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                    $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn-floating btn-sm ' id='bill1_".$d->trans_id."'>
                        <i class='material-icons right'>show_chart</i></button></td></tr>";
    
                }
               
                
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->billpayment[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<button class='btn-floating btn-sm waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<button class='btn-floating btn-sm waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn-floating btn-sm ' id='bill2_".$d->trans_id."'>
                <i class='material-icons right'>show_chart</i></button></td></tr>";
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                foreach($d3 as $r)
                {
                    if($d->moneytransfer[0]->api_code == $r->api_code)
                        $api_name = $r->api_name;
                }

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                    
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->user_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->bk_acc_no."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_per;
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->moneytransfer[0]->mt_per_amt."";
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->moneytransfer[0]->mt_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_date."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->mt_reply_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td>";
            }                                        
           
           
           
           // $str = $str."</tr>"; 

            
            
           
                                                    
            $j++;
        }
        
        return $str; 
    }

    public static function getRechargeDetailsRetailer_new($user_name)
    {
        $rech_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $rech_1->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
      // return response()->json($d1, 200);
        
        
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
            if($f->trans_type == "RECHARGE")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($f->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $reply_id = "";
                $reply_date = "";
                $rech_status = "";
                $o_bal = 0;
                $z = 0;

                $reply_id = $f->reply_id;
                $reply_date =$f->reply_date;

                $rech_status = $f->rech_status;
                $rech_option = $f->rech_option;

                if($rech_status == "PENDING" && $rech_option == "0")
                {
                    $status = "PENDING";
                    
                    $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
                }
                else if($rech_status == "PENDING" && $rech_option == "2")
                {
                    $status = "FAILURE";
                   
                    $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
                }
                else if($rech_status == "FAILURE" && $rech_option == "2")
                {      
                    $status = "FAILURE";
                   
                    $o_bal = floatval($f->ret_bal) - floatval($f->ret_total);
                    $z = 1;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                   
                    $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
                }

                $o_bal = number_format($o_bal, 2, '.', '');
                $c_bal = floatval($f->ret_bal);
                $c_bal = number_format($c_bal, 2, '.', '');

                //percentage amount
                $per = floatval($f->ret_net_per);
                $peramt1 = 0;
                $peramt2 = 0;
                if(floatval($per) != 0)
                {
                    $peramt1 = floatval($per) / 100;
                    $peramt1 = round($peramt1, 4);
            
                    $peramt2 = floatval($f->rech_amount) * floatval($peramt1);
                    $peramt2 = round($peramt2, 2);
                }

                $str = $str."<div class='card-panel shadow br-2 pb-1' style='margin-bottom:35px;'>"; 
                $str = $str."<div class = 'sm-box br-2 c-pink white-text'>
                                <div class='row'>"; 
                $str = $str."<div class='col s8 m7 l7'><h6 class='m-1'>".$f->rech_mobile."</h6><h6 class='m-1'>".$net_name."</h6></div>
                            <div class='col s4 m5 l5 right-align'><h5 class='m-1'>".$f->rech_amount."</h5></div>"; 
                $str = $str."</div></div>";

                $str = $str."<div class='row'>
                                <div class='col s8 m6 l6 mt-1'>"; 
                $str = $str."<p class='m-0 p-1 f-1 purple-text text-darken-3'>".$reply_id."</p>"; 
                $str = $str."<p class='m-0 p-1 f-1'>".$f->ret_net_per. "--".number_format($peramt2,2, ".", "")."--".number_format($f->ret_total,2, ".", "")."</p>";
                $str = $str."<p class='m-0 p-1 f-1'>".$f->rech_date."</p>"; 
                $str = $str."</div>
                            <div class='col s4 m6 l6 mt-1 right-align'>";
                if($status == "SUCCESS") {
                    $str = $str."<a class='btn-floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
                }
                else if($status == "FAILURE") {
                    $str = $str."<a class='btn-floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
                }
                else if($status == "PENDING") {
                    $str = $str."<a class='btn-floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
                }

                $str = $str."<P class='m-0 p-1 f-1'>".number_format($o_bal,2, ".", "")." <i class='tiny material-icons '>favorite_border</i>
                                        ".number_format($c_bal,2, ".", "")."</p>";
            
                $str = $str."</div></div></div>";  
            }                                       
            

           

            

            

               
            
        }
        
        return $str; 
    }

    public static function getN_RechargeDetailsRetailer_new($user_name)
    {
        $rech_1 = new UserRechargeNewDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $rech_1->with(['recharge2'])->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
      // return response()->json($d1, 200);
        
        
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            $reply_id = $f->recharge2->reply_opr_id;
            $reply_date =$f->recharge2->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;
            $cl = "#A569BD";

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
                $cl = "#CD6155";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
                $cl = "#CD6155";
               
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            $str = $str."<div class='card' style='margin:10px 5px;background-color: ".$cl.";color: white;'>"; 
            $str = $str."<div class='card-body' style='padding:5px 25px;'>
                           <div class='row'>"; 
            $str = $str."<div class='col-12 col-sm-12 col-md-7' style='padding:2px 3px;'>";
            $str = $str."<p style='margin-bottom: 2px;'>".$f->rech_mobile." &nbsp;&nbsp; ".$net_name."</p>";
            $str = $str."<p style='margin-bottom: 2px;'>".$reply_id." </p>";
            $str = $str."<p style='margin-bottom: 2px;'>".$f->rech_date."</p>";
            $str = $str."<p style='margin-bottom: 2px;'>".$f->rech_net_per. "--".number_format($f->rech_net_per_amt,2, ".", "")."--".number_format($f->rech_total,2, ".", "")."</p>";

            $str = $str."</div><div class='col-12 col-sm-12 col-md-5 text-right' style='padding:2px 3px;'>";
            $str = $str."<p style='font-size:25px;margin-bottom: 3px;'>".$f->rech_amount."</p>";
            $str = $str."<p style='margin-bottom: 2px;'>".$status." </p>";
            $str = $str."<p style='margin-bottom: 2px;'>".number_format($o_bal,2, ".", "")."---".number_format($c_bal,2, ".", "")."</p>";

            $str = $str."</div></div></div></div>";

            
           
        }
        
        return $str; 
    }

    public static function getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new AllTransaction;

        $u_name = strtoupper($u_name);

        if($f_date != "" && $t_date != "" && $rech_type == "ALL")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'RECHARGE')
                                ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                                ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                            })
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->orderBy('id', 'desc')
                            ->get();

            
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $dc1 = $tran_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'RECHARGE')
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status', '=', $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code', '=', $u_net_code);
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->where('rech_mobile', '=', $u_mobile);
            }
            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $dc1 = $tran_1->whereBetween('created_at', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'BILL_PAYMENT')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_status){
                    return $d->billpayment[0]->con_status == $u_status;
                });
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_mobile){
                    return $d->billpayment[0]->con_acc_no == $u_mobile;
                });
            }
            if($u_api_code != "" && $u_api_code != "-")
            {
                $dc1 = $dc1->filter(function ($d) use ($u_api_code){
                    return $d->billpayment[0]->api_code == $u_api_code;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $dc1 = $tran_1->whereBetween('created_at', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->get();

            if($u_status != "" && $u_status != "-")
            {
                $dc1 = $dc1->where('rech_status', '=', $u_status);
            }
            if($u_net_code != "" && $u_net_code != "-")
            {
                $dc1 = $dc1->where('net_code', '=', $u_net_code);
            }
            if($u_mobile != "")
            {
                $dc1 = $dc1->where('rech_mobile', '=', $u_mobile);
            }
            
        } 

        
        
        
        
        return $dc1; 
         
    }


    public static function getRechargeReportDetails_bill($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new TransactionAllDetails;

        $u_name = strtoupper($u_name);

        $dc1 = $tran_1->with(['billpayment'])
                    ->whereRaw('upper(user_name) = ?',[$u_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BILL_PAYMENT')
                    ->orderBy('id', 'desc')
                    ->get(); 

       
        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->billpayment[0]->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->billpayment[0]->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->billpayment[0]->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        
        
        return $dc4; 
         
    }

    public static function getRechargeReportDetails_money($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code, $u_api_code)
    {
        $tran_1 = new TransactionAllDetails;

        $u_name = strtoupper($u_name);

        $dc1 = $tran_1->with(['moneytransfer'])
                    ->whereRaw('upper(user_name) = ?',[$u_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BANK_TRANSFER')
                    ->orderBy('id', 'desc')
                    ->get(); 

       
        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->moneytransfer[0]->mt_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->moneytransfer[0]->bk_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->moneytransfer[0]->mt_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        
        
        return $dc4; 
         
    }

    public static function getRechargeComplaint($f_date, $t_date, $u_name, $u_mobile)
    {
        $tran_1 = new TransactionAllDetails;

        $u_name = strtoupper($u_name);

        if($f_date != "" && $t_date != "")
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->whereBetween('created_at', [$f_date, $t_date])
                            ->where('trans_type', '=', 'WEB_RECHARGE')
                            ->orderBy('id', 'desc')
                            ->get(); 
        }
        else
        {
            $dc1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                            ->whereRaw('upper(user_name) = ?',[$u_name])
                            ->where('trans_type', '=', 'WEB_RECHARGE')
                            ->orderBy('id', 'desc')
                            ->get(); 
        }
       
        if($u_mobile != "" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_mobile){
                return $d->newrecharge1[0]->rech_mobile == $u_mobile;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        return $dc2; 
         
    }


}
