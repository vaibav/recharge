<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\ApiUrl;
use App\Libraries\PercentageCalculation;
use App\Libraries\InstantReplyUpdate;

use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\BonrixApiResultDetails;
use App\Models\UserRechargeDetails; 
use App\Models\UserRechargeApipartnerDetails; 
use App\Models\UserRechargeRequestDetails;
use App\Models\UserRechargeRequestApipartnerDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\ServerDetails;


class RechargeMobile
{

    /**
     * Add RechargeBill Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_code = 0;
        $api_url = "NONE";
        $api_data = [];
        $b_url = "";
        $res_content = "NONE";
        $resx = new stdClass();

        // Modals
        $rech_5 = new UserRechargeRequestDetails;
        $rech_5_1 = new UserRechargeRequestApipartnerDetails;
        $bon_1 = new BonrixApiResultDetails;
       
        // Post Data
        $r_mode = $data['mode'];
        $rech_type = "WEB_RECHARGE";

        if($r_mode == "WEB") {
            $cat = "VBR";
            $rech_type = "WEB_RECHARGE";
        }
        else if($r_mode == "API") {
            $cat = "VBA";
            $rech_type = "API_RECHARGE";
        }
        else if($r_mode == "GPRS") {
            $cat = "VBG";
            $rech_type = "WEB_RECHARGE";
            $r_mode = "WEB";
        }
        else if($r_mode == "SMS") {
            $cat = "VBS";
            $rech_type = "WEB_RECHARGE";
            $r_mode = "WEB";
        }
        
        $trans_id = $cat.rand(10000000,99999999);
        $net_type_code = $data['net_type_code'];
        $net_code = $data['net_code'];
        $user_mobile = $data['user_mobile'];
        $user_amount = $data['user_amount'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");


        // 1.Check User Balance
        if(floatval($user_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }

        // Get Network Line
        if ($r_mode == "WEB") {
            list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        }
        else if ($r_mode == "API") {
            list($api_code, $v) = ApiUrl::getApiNetworkLine($net_code, $user_amount, $user_name);
        }
        
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 3;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 4;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 5;     // Account Balance is low from Setup Fee...
        }


        // Check Mobile Status
        $zy = self::checkAccountNoStatus($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy == 6)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        // Check 10 Minutes Status
        $zy =self::checkTenMinsCheck($user_name, $user_mobile, $user_amount, $r_mode);
        if ($zy > 0)
        {
            $z = 7;
        }

        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 8;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            // Do Recharge.... 
            if($net_code == "857")
            {
                list($api_url, $api_data) = ApiUrl::generateAPIwithPost($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
            }
            else
            {
                $api_url = ApiUrl::generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);
                
            }
            

            if($api_url != "NONE")
                $res_code = 200;


            // Insertion Process..
            list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $user_amount, $r_mode);

            // Array
            $rech = [];
            $rech['trans_id'] = $trans_id;
            if($r_mode == "API") {
                $rech['api_trans_id'] = $api_trans_id;
            }
            $rech['user_name'] = $user_name;
            $rech['net_code'] = $net_code;
            $rech['rech_mobile'] = $user_mobile;
            $rech['rech_amount'] = $user_amount;
            $rech['rech_total'] = PercentageCalculation::convertNumberFormat($netamt);
            $rech['rech_date'] = $date_time;
            $rech['rech_status'] = "PENDING";
            $rech['rech_mode'] = $r_mode;
            $rech['api_code'] = $api_code;
            $rech['trans_bid'] = $trans_id;
            $rech['reply_opr_id'] = "";
            $rech['reply_date'] = $date_time;
            $rech['created_at'] = $date_time;
            $rech['updated_at'] = $date_time;
            

            $zx = self::insertRecharge($rech, $r_mode);
            if($zx == 1)
            {

                // Insert Recharge Request
                $rech1 = [];
                $rech1['trans_id'] = $trans_id;
                if($r_mode == "API") {
                    $rech1['api_trans_id'] = $api_trans_id;
                }
                $rech1['user_name'] = $user_name;
                $rech1['net_code'] = $net_code;
                $rech1['rech_mobile'] = $user_mobile;
                $rech1['rech_amount'] = $user_amount;
                $rech1['rech_date'] = $date_time;
                $rech1['rech_status'] = "PENDING";
                $rech1['rech_mode'] = $r_mode;
                $rech1['api_code'] = $api_code;
                $rech1['rech_req_status'] = "";
                $rech1['created_at'] = $date_time;
                $rech1['updated_at'] = $date_time;

                $zy = self::insertRechargeRequest($rech1, $r_mode);


                // Insert Recharge Payment
                $rech2 = [];
                $rech2['trans_id'] = $trans_id;
                if($r_mode == "API") {
                    $rech2['api_trans_id'] = $api_trans_id;
                }
                $rech2['user_name'] = $user_name;
                $rech2['net_code'] = $net_code;
                $rech2['rech_mobile'] = $user_mobile;
                $rech2['rech_amount'] = $user_amount;
                $rech2['rech_net_per'] = PercentageCalculation::convertNumberFormat($per);
                $rech2['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt);
                $rech2['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
                $rech2['rech_total'] = PercentageCalculation::convertNumberFormat($netamt);
                $rech2['user_balance'] = '0';
                $rech2['rech_date'] = $date_time;
                $rech2['rech_status'] = "PENDING";
                $rech2['rech_type'] = $rech_type;
                $rech2['created_at'] = $date_time;
                $rech2['updated_at'] = $date_time;

                $zy = self::insertRechargePayment($rech2, $r_mode);

                // Parent Network Percentage Calculation
                list($parent_type, $parent_name) = self::getParent($user_name);
                list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $user_amount);

                $rech_p2 = [];
                $rech_p2['trans_id'] = $trans_id;
                if($r_mode == "API") {
                    $rech_p2['api_trans_id'] = $api_trans_id;
                }
                $rech_p2['super_parent_name'] = "NONE";
                $rech_p2['parent_name'] = $parent_name;
                $rech_p2['user_name'] = $user_name;
                $rech_p2['net_code'] = $net_code;
                $rech_p2['rech_mobile'] = $user_mobile;
                $rech_p2['rech_amount'] = $user_amount;
                $rech_p2['rech_net_per'] = $per_d;
                $rech_p2['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_d);
                $rech_p2['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                $rech_p2['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                $rech_p2['user_balance'] = 0;
                $rech_p2['rech_date'] = $date_time;
                $rech_p2['rech_status'] = "PENDING";
                $rech_p2['rech_type'] = $rech_type;
                $rech_p2['created_at'] = $date_time;
                $rech_p2['updated_at'] = $date_time;

                $zp2 = self::insertRechargePaymentDistributor($rech_p2, $rech_p2['parent_name'], $r_mode);
                $zp22 = self::insertTransactionParent($trans_id, $rech_p2['parent_name'], $rech_type);


                 // Super Parent Network Percentage Calculation
                 list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                 if($parent_name_s != "NONE")
                 {
                     list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $user_amount);
                 
                     // Super Distributor Payment Details
                     $rech_p3 = [];
                     $rech_p3['trans_id'] = $trans_id;
                     if($r_mode == "API") {
                        $rech_p3['api_trans_id'] = $api_trans_id;
                     }
                     $rech_p3['super_parent_name'] = $parent_name_s;
                     $rech_p3['parent_name'] = $parent_name;
                     $rech_p3['user_name'] = $user_name;
                     $rech_p3['net_code'] = $net_code;
                     $rech_p3['rech_mobile'] = $user_mobile;
                     $rech_p3['rech_amount'] = $user_amount;
                     $rech_p3['rech_net_per'] = $per_s;
                     $rech_p3['rech_net_per_amt'] = PercentageCalculation::convertNumberFormat($peramt_s);
                     $rech_p3['rech_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                     $rech_p3['rech_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                     $rech_p3['user_balance'] = 0;
                     $rech_p3['rech_date'] = $date_time;
                     $rech_p3['rech_status'] = "PENDING";
                     $rech_p3['rech_type'] = $rech_type;
                     $rech_p3['created_at'] = $date_time;
                     $rech_p3['updated_at'] = $date_time;
 
                                    
                     $zp3 = self::insertRechargePaymentDistributor($rech_p3, $rech_p3['super_parent_name'], $r_mode);
                     $zp33 = self::insertTransactionParent($trans_id, $rech_p3['super_parent_name'], $rech_type);
 
                 }

                $zt = self::insertTransaction($trans_id, $user_name, $rech_type);
                 
                if ($res_code == 200)
                {

                    if (strpos($api_url, 'ssh1.bonrix.in') !== false) 
                    {
                        // Bonrix URL
                        $bon_1->trans_id = $trans_id;
                        $bon_1->result_url = $api_url;
                        $bon_1->result_status = "1";
                        $bon_1->save();

                        $res_content = "done";
                    }
                    else
                    {
                        $client = new \GuzzleHttp\Client();

                        if($net_code == "857")
                        {
                            $params['form_params'] = $api_data;
                            file_put_contents("airtel.txt", print_r($params, true));
                            file_put_contents("airtel1.txt", $api_url);
                            $res = $client->post($api_url, $params);
                            $res_content = $res->getBody();
                        }
                        else
                        {
                            
                            $res = $client->get($api_url);
                            $res_code = $res->getStatusCode();
                            $res_content = $res->getBody();
                        }
                       
                    }

                    if($r_mode == "WEB") {
                        $rech_5->where('trans_id', '=', $trans_id)->update(['rech_req_status' => $res_content]);
                    }
                    else if($r_mode == "API") {
                        $rech_5_1->where('trans_id', '=', $trans_id)->update(['rech_req_status' => $res_content]);
                    }
                        


                    // Reply Update
                    InstantReplyUpdate::instantUpdate($res_content, "SUCCESS", url('/')."/pendingreport_success_1/".$trans_id);
                    InstantReplyUpdate::instantUpdate($res_content, "success", url('/')."/pendingreport_success_1/".$trans_id);
                    InstantReplyUpdate::instantUpdate($res_content, "FAILED", url('/')."/pendingreport_failure_1/".$trans_id);
                    InstantReplyUpdate::instantUpdate($res_content, "Failed", url('/')."/pendingreport_failure_1/".$trans_id);
                    InstantReplyUpdate::instantUpdate($res_content, "failure", url('/')."/pendingreport_failure_1/".$trans_id);
                    InstantReplyUpdate::instantUpdate($res_content, "FAILURE", url('/')."/pendingreport_failure_1/".$trans_id);

                   
                }

                // All Process Over...
            }


        }

        return array($z, $trans_id);
        
    }



    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    

    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

    public static function checkAccountNoStatus($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeDetails;
        if($r_mode == "WEB") {
            $rech_1 = new UserRechargeDetails;
        }
        else  if($r_mode == "API") {
            $rech_1 = new UserRechargeApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_1 = new UserRechargeDetails;
        }
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], ['rech_mobile', '=', $mobile], 
                                ['rech_amount', '=', $amount], ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public static function checkTenMinsCheck($user_name, $mobile, $amount, $r_mode)
    {
        $rech_1 = new UserRechargeDetails;

        if($r_mode == "WEB") {
            $rech_1 = new UserRechargeDetails;
        }
        else  if($r_mode == "API") {
            $rech_1 = new UserRechargeApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_1 = new UserRechargeDetails;
        }
        
        $z = 0;

        $d1 = $rech_1->select('created_at')->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'SUCCESS']])->orderBy('id', 'desc')->limit(1)->get();
        if($d1->count() > 0)
        {
            $old_d = $d1[0]->created_at;
            $cur_d = date("Y-m-d H:i:s");

            $o_d = new DateTime($old_d);
            $c_d = new DateTime($cur_d);

            $dif = $o_d->diff($c_d);

            $minutes = $dif->days * 24 * 60;
            $minutes += $dif->h * 60;
            $minutes += $dif->i;

            if($minutes < 10)
                $z = 1;
        }

        return $z;
    }


    /**
     * Insertion Process..............................................
     *
     */
    public static function insertRecharge($rech_p1, $r_mode)
    {
        $rech_1 = new UserRechargeDetails;

        if($r_mode == "WEB") {
            $rech_1 = new UserRechargeDetails;
        }
        else  if($r_mode == "API") {
            $rech_1 = new UserRechargeApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_1 = new UserRechargeDetails;
        }
        
        $zx = 0;
       
        $d1 = $rech_1->where('trans_id', '=', $rech_p1['trans_id'])->get();
        if($d1->count() == 0)
        {
           
            if($rech_1->insert($rech_p1))
            {
                $zx = 1;
            }
            
        }

        return $zx;
    }

    public static function insertRechargeRequest($rech_p1, $r_mode)
    {
        $rech_1 = new UserRechargeRequestDetails;

        if($r_mode == "WEB") {
            $rech_1 = new UserRechargeRequestDetails;
        }
        else  if($r_mode == "API") {
            $rech_1 = new UserRechargeRequestApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_1 = new UserRechargeRequestDetails;
        }

        $zx = 0;
       
        $d1 = $rech_1->where('trans_id', '=', $rech_p1['trans_id'])->get();
        if($d1->count() == 0)
        {
           
            if($rech_1->insert($rech_p1))
            {
                $zx = 1;
            }
            
        }

        return $zx;
    }


    public static function insertRechargePayment($rech_p1, $r_mode)
    {
        $rech_3 = new UserRechargePaymentDetails;

        if($r_mode == "WEB") {
            $rech_3 = new UserRechargePaymentDetails;
        }
        else  if($r_mode == "API") {
            $rech_3 = new UserRechargePaymentApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_3 = new UserRechargePaymentDetails;
        }

        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1['user_name'])->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1['user_name'])
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;
            
            if($rech_3->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public static function insertRechargePaymentDistributor($rech_p1, $user_name, $r_mode)
    {
        $rech_4 = new UserRechargePaymentParentDetails;
        if($r_mode == "WEB") {
            $rech_4 = new UserRechargePaymentParentDetails;
        }
        else  if($r_mode == "API") {
            $rech_4 = new UserRechargePaymentParentApipartnerDetails;
        }
        if($r_mode == "GPRS") {
            $rech_4 = new UserRechargePaymentParentDetails;
        }
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1['rech_total']);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance' => $net_bal, 'updated_at' => $rech_p1['updated_at'] ]);
            
            $rech_p1['user_balance'] = $net_bal;

            if($rech_4->insert($rech_p1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public static function insertTransaction($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public static function insertTransactionParent($trans_id, $user_name, $rech_type)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = $rech_type;
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    

}
