<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

class RechargeReportDistributor
{
    //
    public static function getRechargeDetailsDistributor_old($user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $tran_1->with(['newparentrecharge1', 'newparentrecharge2'])
                    ->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'WEB_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT');

                    })
                    ->orderBy('id', 'desc')
                    ->limit(10)->get(); 
        
      //  return response()->json($d1, 200);
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE")
            {
                if(sizeof($d->newparentrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newparentrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    $api_name = "";
                   
                    $reply_id = $d->newparentrecharge2->reply_opr_id;
                    $reply_date = $d->newparentrecharge2->reply_date;

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newparentrecharge1[0]->rech_status;
                        $rech_option = $d->newparentrecharge1[0]->rech_option;
                        $u_bal = $d->newparentrecharge1[0]->user_balance;
                        $r_tot = $d->newparentrecharge1[0]->rech_total;
                        $str = $str."<tr>";
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newparentrecharge1[1]->rech_status;
                        $rech_option = $d->newparentrecharge1[1]->rech_option;
                        $u_bal = $d->newparentrecharge1[1]->user_balance;
                        $r_tot = $d->newparentrecharge1[1]->rech_total;
                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                    }

                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }

                    $mode = "WEB";
                    if($d->trans_id != "")
                    {
                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                        $r_l = $matches[0][0];

                        $r_l = substr($r_l, -1);

                        if($r_l == "R")
                            $mode = "WEB";
                        else if($r_l == "A")
                            $mode = "API";
                        else if($r_l == "G")
                            $mode = "GPRS";
                        else if($r_l == "S")
                            $mode = "SMS";

                    }

                    $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->user_name."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->rech_mobile."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->rech_amount."</td>";
                    if($d->trans_option == 1)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->rech_net_per;
                        $str = $str."--".$d->newparentrecharge1[0]->rech_net_per_amt."";
                        $str = $str."--".$d->newparentrecharge1[0]->rech_net_surp."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newparentrecharge1[0]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->rech_date."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                    }
                    else if($d->trans_option == 2)
                    {
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newparentrecharge1[1]->rech_total."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    }
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newparentrecharge1[0]->rech_type."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";

                }
                
               
            }
                                            
           
           
           
            $str = $str."</tr>"; 

            
            
           
                                                    
            $j++;
        }
        
        return $str; 
    }

    public static function getRechargeDetailsDistributor($user_name)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $rech_2 = new UserRechargeBillDetails;
        $rech_3 = new UserBankTransferDetails;

        $net_2 = new NetworkDetails;
    
        $user_name = strtoupper($user_name);

        $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->orderBy('id', 'desc')->limit(15)->get();

        $d3 = $net_2->select('net_code','net_name')->get();

        $j = 0;
        $str = "";
        foreach($d1 as $f)
        {
                                                   
            $net_name = "";
            foreach($d3 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;
           

            if($f->rech_type == "RECHARGE")
            {
                if($f->newparentrecharge2 != null)
                {
                    $reply_id = $f->newparentrecharge2->reply_opr_id;
                    $reply_date =$f->newparentrecharge2->reply_date;
                }
            }
            else if($f->rech_type == "BILL_PAYMENT")
            {
                $d2 = $rech_2->select('reply_opr_id', 'reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->reply_opr_id;
                    $reply_date =$d2[0]->reply_date;
                }
                
            }
            else if($f->rech_type == "BANK_TRANSFER")
            {
                $d2 = $rech_3->select('mt_reply_id', 'mt_reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->mt_reply_id;
                    $reply_date =$d2[0]->mt_reply_date;
                }
                
            }

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "<button class='btn-floating btn-small waves-effect waves-light'><i class='small material-icons '>check</i></button>";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            
            if($z == 0)
            {
                $str = $str. "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_net_per."--".$f->rech_net_per_amt."";
                $str = $str. "--".$f->rech_net_surp."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 130px;word-break: break-word;'>".$reply_id."</div></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_type."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                
            }
            else
            {
                $str = $str. "<tr style='background-color:#E8DAEF;'><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;'></td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
            }
            $str = $str. "</tr>";
                                                    
            $j++;
        }

        return $str;
    }

    public static function getRechargeReportDetails_new($f_date, $t_date, $rech_type, $user_name, $u_name1, $u_status, $u_mobile, $u_amount, $u_net_code)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $net_2 = new NetworkDetails;
        

        $user_name = strtoupper($user_name);

        if($f_date != "" && $t_date != "" && $rech_type == "-")
        {
            $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->orderBy('id', 'desc')->get();
            
           
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->where('rech_type', '=', 'RECHARGE')
                        ->orderBy('id', 'desc')->get();

        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->where('rech_type', '=', 'BANK_TRANSFER')
                        ->orderBy('id', 'desc')->get();
 
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_TRANSFER")
        {
            $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->where('rech_type', '=', 'BILL_TRANSFER')
                        ->orderBy('id', 'desc')->get();
 
        }

       
        if($u_name1 != "-" ) 
        {

            $d1 = $d1->filter(function ($d) use ($u_name1){
                return $d->user_name == $u_name1;
            });
        }

        if($u_net_code != "-" ) 
        {
            $d1 = $d1->filter(function ($d) use ($u_net_code){
                return $d->net_code == $u_net_code;
            });
        }
        
        if($u_status != "-" ) 
        {
            $d1 = $d1->filter(function ($d) use ($u_status){
                return $d->rech_status == $u_status;
            });
        }
       
        if($u_mobile != "" ) 
        {
            $d1 = $d1->filter(function ($d) use ($u_mobile){
                return $d->rech_mobile == $u_mobile;
            });
        }
        
        if($u_amount != "" ) 
        {
            $d1 = $d1->filter(function ($d) use ($u_amount){
                return $d->rech_amount == $u_amount;
            });
        }
       

       return $d1; 
    }


    public static function getRechargeReportDetails($f_date, $t_date, $u_name, $u_name1, $u_status, $u_mobile, $u_amount, $u_net_code, $u_rech_type)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $dc1 = $rech_n_1->with(['newparentrecharge2'])->where('parent_name', '=', $u_name)
                    ->whereBetween('rech_date', [$f_date, $t_date])->orderBy('id', 'asc')->get();


        if($u_name1 != "" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name1){
                return $d->user_name == $u_name1;
            });
        }
                  

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->rech_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->rech_mobile == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->rech_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }

        if($u_net_code != "-" ) 
        {
            $dc5 = $dc4->filter(function ($d) use ($u_net_code){
                return $d->net_code == $u_net_code;
            });
        }
        else
        {
            $dc5 = $dc4;
        }

        if($u_rech_type != "-" ) 
        {
            $dc6 = $dc5->filter(function ($d) use ($u_rech_type){
                return $d->rech_type == $u_rech_type;
            });
        }
        else
        {
            $dc6 = $dc5;
        }
        
     
        return $dc6; 
    }

}
