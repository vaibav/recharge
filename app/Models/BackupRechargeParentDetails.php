<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackupRechargeParentDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'backup_recharge_parent_details';
    protected $fillable = array(
        'trans_id',
        'parent_name',
        'child_name',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_total',
        'user_balance',
        'rech_date',
        'rech_status',
        'rech_type'
    );

    public $timestamps = true;
}
