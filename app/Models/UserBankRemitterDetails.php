<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankRemitterDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_bank_remitter_details';
    protected $fillable = array(
        'rem_id',
        'api_trans_id',
        'user_name',
        'user_rem_name',
        'user_rem_lname',
        'user_address',
        'user_city',
        'user_state_code',
        'user_pincode',
        'msisdn',
        'rem_date',
        'api_code',
        'rem_req_status',
        'rem_status',
        'rem_rep_opr_id',
        'reply_date'
    );

    public $timestamps = true;
}
