<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferLineDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'offer_line_details';
    protected $fillable = array(
        'offer_line_tr_id',
        'net_code',
        'api_code',
        'offer_line_status'
    );

    public $timestamps = true;
}
