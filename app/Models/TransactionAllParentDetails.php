<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionAllParentDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'transaction_all_parent_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'trans_type',
        'trans_option'
    );

    public $timestamps = true;

    public function newparentrecharge1()
    {
        return $this->hasMany('App\Models\UserRechargeNewParentDetails', 'trans_id', 'trans_id');
    }

    public function newparentrecharge2()
    {
        return $this->hasOne('App\Models\UserRechargeNewStatusDetails', 'trans_id', 'trans_id');
    }

    public function webparentrecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentParentDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'super_parent_name', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where('rech_type', '=' ,'WEB_RECHARGE');
    }

    public function webparentrecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode', 'rech_date', 
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'WEB');
    }

    public function webparentapirecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentParentApipartnerDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'super_parent_name', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where('rech_type', '=' ,'WEB_RECHARGE');
    }

    public function webparentapirecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeApipartnerDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'api_trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode', 'rech_date', 
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'WEB');
    }

    public function websuperparentrecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentParentDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'super_parent_name', 'parent_name', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where([['rech_type', '=', 'WEB_RECHARGE'], ['super_parent_name', '!=', 'NONE']]);
    }

    public function websuperparentrecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode', 'rech_date', 
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'WEB');
    }

    public function websuperparentapirecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentParentApipartnerDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'api_trans_id', 'super_parent_name', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where([['rech_type', '=', 'API_RECHARGE'], ['super_parent_name', '=', 'NONE']]);
    }

    public function websuperparentapirecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeApipartnerDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode', 'rech_date', 
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'API');
    }

    public function getusertype()
    {
        return $this->hasOne('App\Models\UserAccountDetails', 'user_name', 'user_name')
            ->select(['user_code', 'user_name', 'user_type']);
    }

}
