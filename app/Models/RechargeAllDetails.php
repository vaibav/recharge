<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RechargeAllDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'recharge_all_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_date',
        'con_name',
        'con_acc_no',
        'con_mobile',
        'con_amount',
        'bank_acc_no',
        'bank_acc_name',
        'bank_name',
        'bank_ifsc',
        'bank_acc_mobile',
        'bank_acc_amount',
        'rech_net_per',
        'rech_net_surp',
        'rech_total',
        'user_balance',
        'rech_req_status',
        'rech_status',
        'rech_mode',
        'rech_all_mode',
        'rech_code',
        'api_code',
        'reply_opr_id',
        'reply_date'
    );

    public $timestamps = true;
}
