<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserChainDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_chain_details';
    protected $fillable = array(
        'user_code',
        'user_name',
        'parent_code',
        'parent_name',
        'user_level',
        'user_chain'
    );

    public $timestamps = true;
}
