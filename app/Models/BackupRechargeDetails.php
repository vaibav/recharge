<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackupRechargeDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'backup_recharge_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_total',
        'user_balance',
        'rech_date',
        'rech_status',
        'reply_id',
        'api_code',
    );

    public $timestamps = true;
}
