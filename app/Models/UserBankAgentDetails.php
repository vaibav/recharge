<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankAgentDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_bank_agent_details';
    protected $fillable = array(
        'agent_id',
        'user_name',
        'agent_msisdn',
        'agent_name',
        'agent_cname',
        'agent_address',
        'agent_city',
        'agent_state_code',
        'agent_pincode',
        'agent_status',
        'api_code',
        'agent_trans_id',
        'agent_reply_id'
    );

    public $timestamps = true;
}
