<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApipartnerBrowserDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'apipartner_browser_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'agent_type'
    );

    public $timestamps = true;
}
