<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkSurplusDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'network_surplus_details';
    protected $fillable = array(
        'net_tr_id',
        'net_code',
        'user_type',
        'from_amount',
        'to_amount',
        'surplus_charge'
    );

    public $timestamps = true;
}
