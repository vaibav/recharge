<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServerDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'server_details';
    protected $fillable = array(
        'server_code',
        'server_status'
    );

    public $timestamps = true;
}
