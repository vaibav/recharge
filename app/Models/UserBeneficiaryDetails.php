<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBeneficiaryDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_beneficiary_details';
    protected $fillable = array(
        'ben_id',
        'api_trans_id',
        'user_name',
        'agent_id',
        'msisdn',
        'ben_acc_no',
        'ben_acc_name',
        'bank_code',
        'bank_ifsc',
        'api_code',
        'ben_req_status',
        'ben_status',
        'ben_s_status',
        'trans_id',
        'ben_rep_opr_id',
        'ben_rep_date'
    );

    public $timestamps = true;

}
