<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPersonalDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_personal_details';
    protected $fillable = array(
        'user_code',
        'user_name',
        'user_per_name',
        'user_city',
        'user_state',
        'user_phone',
        'user_mobile',
        'user_mail',
        'user_kyc',
        'user_photo',
        'user_kyc_proof'
    );

    public $timestamps = true;

    public function account()
    {
        return $this->hasOne('App\Models\UserAccountDetails', 'user_code', 'user_code');
    }

    public function balance()
    {
        return $this->hasOne('App\Models\UserAccountBalanceDetails', 'user_code', 'user_code');
    }
}
