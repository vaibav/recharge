<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeNewStatusDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_new_status_details';
    protected $fillable = array(
        'trans_id',
        'rech_type',
        'rech_web',
        'api_code',
        'rech_req_status',
        'reply_opr_id',
        'reply_date'
    );

    public $timestamps = true;
}
