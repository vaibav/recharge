<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AllTransaction extends Model
{
    //
    public $timestamps = true;

    public function request()
    {
        return $this->hasOne('App\Models\AllRequest', 'trans_id', 'trans_id');
    }
}
