<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkLineDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'network_line_details';
    protected $fillable = array(
        'net_line_tr_id',
        'net_code',
        'api_code',
        'net_line_type',
        'net_line_value_1',
        'net_line_value_2',
        'net_line_status'
    );

    public $timestamps = true;
}
