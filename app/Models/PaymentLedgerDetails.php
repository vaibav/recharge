<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentLedgerDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'payment_ledger_details';
    protected $fillable = array(
        'trans_id',
        'bill_no',
        'user_name',
        'pay_type',
        'pay_amount',
        'pay_mode',
        'pay_option',
        'pay_image',
        'from_user',
        'pay_date',
        'agent_name',
        'pay_remarks',
        'pay_status'
    );

    public $timestamps = true;
}
