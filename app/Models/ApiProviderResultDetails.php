<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiProviderResultDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_provider_result_details';
    protected $fillable = array(
        'api_res_tr_id',
        'api_code',
        'api_result_type',
        'api_res_para_name',
        'api_res_para_field',
        'api_res_para_value'
    );

    public $timestamps = true;
}
