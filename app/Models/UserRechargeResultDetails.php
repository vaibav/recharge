<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeResultDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_result_details';
    protected $fillable = array(
        'a1',
        'a2',
        'a3',
        'a4',
        'a5',
        'a6',
        'a7',
        'a8',
        'a9',
        'a10',
        'a11'
    );

    public $timestamps = true;

}
