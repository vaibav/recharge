<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeNewDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_new_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_net_per',
        'rech_net_per_amt',
        'rech_net_surp',
        'rech_total',
        'user_balance',
        'rech_date',
        'rech_status',
        'rech_mode',
        'rech_option'
    );

    public $timestamps = true;

    public function recharge2()
    {
        return $this->hasOne('App\Models\UserRechargeNewStatusDetails', 'trans_id', 'trans_id');
    }

    public function backup()
    {
        return $this->hasMany('App\Models\BackupRechargeDetails', 'trans_id', 'trans_id');
    }
}
