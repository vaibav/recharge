<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNetworkDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_network_details';
    protected $fillable = array(
        'user_code',
        'user_name',
        'net_code',
        'user_net_per',
        'user_net_surp'
    );

    public $timestamps = true;

    public function usernetwork()
    {
        return $this->belongsTo('App\Models\UserPersonalDetails', 'user_code', 'user_code');
    }
}
