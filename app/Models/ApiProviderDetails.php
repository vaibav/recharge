<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiProviderDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_provider_details';
    protected $fillable = array(
        'api_tr_id',
        'api_code',
        'api_name',
        'api_url',
        'api_method',
        'api_status'
    );

    public $timestamps = true;

    public function apinetwork()
    {
        return $this->hasMany('App\Models\ApiProviderNetworkDetails', 'api_code', 'api_code')->select('net_code', 'api_net_short_code');
    }
}
