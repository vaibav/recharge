<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargePaymentParentApipartnerDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_payment_parent_api_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'super_parent_name',
        'parent_name',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_net_per',
        'rech_net_per_amt',
        'rech_net_surp',
        'rech_total',
        'user_balance',
        'rech_date',
        'rech_status',
        'rech_type'
    );

    public $timestamps = true;

    public function userrechargeparent()
    {
        return $this->hasOne('App\Models\UserRechargeApipartnerDetails', 'trans_id', 'trans_id');
    }

}
