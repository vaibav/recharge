<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApipartnerIpaddressDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'apipartner_ipaddress_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'ip_address',
        'ip_status'
    );

    public $timestamps = true;
}
