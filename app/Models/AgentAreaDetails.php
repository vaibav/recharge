<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgentAreaDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'agent_area_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'area_name',
        'area_status'
    );

    public $timestamps = true;
}
