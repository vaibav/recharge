<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOldAccountDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_old_account_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'from_date',
        'too_date',
        'rech_type',
        'user_amount'
    );
}
