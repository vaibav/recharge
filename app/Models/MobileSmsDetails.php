<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileSmsDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'mobile_sms_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'api_code',
        'user_mobile',
        'user_message',
        'reply_message'
    );

    public $timestamps = true;
}
