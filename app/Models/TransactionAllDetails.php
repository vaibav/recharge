<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionAllDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'transaction_all_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'trans_type',
        'trans_option'
    );

    public $timestamps = true;

    public function newrecharge1()
    {
        return $this->hasMany('App\Models\UserRechargeNewDetails', 'trans_id', 'trans_id');
    }

    public function newrecharge2()
    {
        return $this->hasOne('App\Models\UserRechargeNewStatusDetails', 'trans_id', 'trans_id');
    }

    public function webrecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where('rech_type', '=' ,'WEB_RECHARGE');
    }

    public function webrecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode', 'rech_date', 
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'WEB');
    }

    public function webrecharge3()
    {
        // Recharge request
        return $this->belongsTo('App\Models\UserRechargeRequestDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'rech_req_status']);
                
    }

    public function apirecharge1()
    {
        return $this->hasMany('App\Models\UserRechargePaymentApipartnerDetails', 'trans_id', 'trans_id')
            ->select(['trans_id', 'rech_net_per', 'rech_net_per_amt', 
            'rech_net_surp', 'rech_total', 'user_balance', 'rech_status'])
            ->where('rech_type', '=' ,'API_RECHARGE');
    }

    public function apirecharge2()
    {
        return $this->belongsTo('App\Models\UserRechargeApipartnerDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'api_trans_id', 'user_name','net_code', 
                'rech_mobile', 'rech_amount', 'rech_status', 'rech_mode',  'rech_date',
                'api_code', 'reply_opr_id', 'reply_date'])
                ->where('rech_mode', '=' ,'API');
    }

    public function apirecharge3()
    {
        // Recharge request
        return $this->belongsTo('App\Models\UserRechargeRequestApipartnerDetails', 'trans_id', 'trans_id')
                ->select(['trans_id', 'rech_req_status'])
                ->where('rech_mode', '=' ,'API');
    }

    public function billpayment()
    {
        return $this->hasMany('App\Models\UserRechargeBillDetails', 'trans_id', 'trans_id');
                
    }

    public function moneytransfer()
    {
        return $this->hasMany('App\Models\UserBankTransferDetails', 'trans_id', 'trans_id');
                
    }


}
