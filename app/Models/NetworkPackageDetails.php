<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkPackageDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'network_package_details';
    protected $fillable = array(
        'trans_id',
        'pack_id',
		'pack_name',
        'net_code',
        'from_amt',
        'to_amt',
        'net_per',
        'net_surp',
        'pack_status'
    );

    public $timestamps = true;
}
