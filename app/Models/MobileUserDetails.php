<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileUserDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'mobile_user_details';
    protected $fillable = array(
        'user_name',
        'user_type',
        'mobile_imei',
        'mobile_name',
        'auth_token',
        'user_otp',
        'user_status'
    );

    public $timestamps = true;
}
