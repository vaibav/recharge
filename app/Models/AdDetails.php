<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'ad_details';
    protected $fillable = array(
        'trans_id',
        'ad_data',
        'ad_order',
        'from_date',
        'too_date',
        'ad_photo',
        'ad_status'
    );
}
