<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccountDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_account_details';
    protected $fillable = array(
        'user_code',
        'user_name',
        'user_pwd',
        'user_type',
        'user_setup_fee',
        'parent_type',
        'parent_code',
        'parent_name',
        'pack_id',
        'user_rec_mode',
        'user_api_url_1',
        'user_api_url_2',
        'user_status'
    );

    public $timestamps = true;

    public function personal()
    {
        return $this->hasOne('App\Models\UserPersonalDetails', 'user_code', 'user_code');
    }

    public function UserPersonalDetails()
    {
        return $this->hasMany('App\Models\UserPersonalDetails', 'user_code', 'user_code');
    }
}
