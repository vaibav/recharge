<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeApipartnerDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_apipartner_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_total',
        'rech_date',
        'rech_status',
        'rech_mode',
        'api_code',
        'trans_bid',
        'reply_opr_id',
        'reply_date'
    );

    public $timestamps = true;
}
