<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeRequestApipartnerDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_request_apipartner_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_date',
        'rech_status',
        'rech_mode',
        'api_code',
        'rech_req_status'
    );
}
