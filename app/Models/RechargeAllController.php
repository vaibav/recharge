<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserPaymentDetails;
use App\Models\TransactionAllDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllParentDetails;

class RechargeAllController extends Controller
{
    //
    public function getDetails(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        
        $f_date = "2018-11-02 00:00:00";
        $t_date = "2018-11-02 23:59:59";

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['webrecharge1', 'webrecharge2', 'apirecharge1', 'apirecharge2'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        echo "<table border=1 cellspacing=0 cellpadding=2>";
        foreach($d1 as $d)
        {
            if($d->trans_type == 'WEB_RECHARGE')
            {
                $trans_id = $d->trans_id;
                $api_trans_id = '';
                $user_name = $d->user_name;
                $net_code = $d->webrecharge2->net_code;
                $rech_mobile = $d->webrecharge2->rech_mobile;
                $rech_amount = $d->webrecharge2->rech_amount;
                $rech_date = $d->webrecharge2->rech_date;

                if($d->trans_option == 1)
                {
                    $rech_net_per = $d->webrecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->webrecharge1[0]->rech_net_surp;
                    $rech_total = $d->webrecharge1[0]->rech_total;
                    $user_balance = $d->webrecharge1[0]->user_balance;
                    // PENDING OR SUCCESS
                    if ($d->webrecharge2->rech_status == 'SUCCESS')
                    {
                        // SUCCESS
                        $rech_status = "SUCCESS";
                        $rech_code = 1;
                    }
                    else if ($d->webrecharge2->rech_status == 'FAILURE')
                    {
                        // PENDING FAILURE
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }
                    else
                    {
                        // PENDING 
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }

                   

                }
                else if($d->trans_option == 2)
                {
                    $rech_net_per = $d->webrecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->webrecharge1[0]->rech_net_surp;
                    $rech_total = $d->webrecharge1[0]->rech_total;
                    $user_balance = $d->webrecharge1[1]->user_balance;

                     // DEFINITELY FAILURE
                     $rech_status = "FAILURE";
                     $rech_code = 2;
                }

                $rech_mode = 'WEB';
                if (strpos($d->trans_id, 'VBG') !== false) {
                    $rech_mode = "GPRS";
                }

                $rech_req_status = '';
                $rech_all_mode = 'WEB_RECHARGE';
                $reply_opr_id = $d->webrecharge2->reply_opr_id;
                $reply_opr_date = $d->webrecharge2->reply_date;
                
            }
            else if($d->trans_type == 'API_RECHARGE')
            {
                $trans_id = $d->trans_id;
                $api_trans_id = $d->apirecharge2->api_trans_id;
                $user_name = $d->user_name;
                $net_code = $d->apirecharge2->net_code;
                $rech_mobile = $d->apirecharge2->rech_mobile;
                $rech_amount = $d->apirecharge2->rech_amount;
                $rech_date = $d->apirecharge2->rech_date;

                if($d->trans_option == 1)
                {
                    $rech_net_per = $d->apirecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->apirecharge1[0]->rech_net_surp;
                    $rech_total = $d->apirecharge1[0]->rech_total;
                    $user_balance = $d->apirecharge1[0]->user_balance;
                    // PENDING OR SUCCESS
                    if ($d->apirecharge2->rech_status == 'SUCCESS')
                    {
                        // SUCCESS
                        $rech_status = "SUCCESS";
                        $rech_code = 1;
                    }
                    else if ($d->apirecharge2->rech_status == 'FAILURE')
                    {
                        // PENDING FAILURE
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }
                    else
                    {
                        // PENDING 
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }

                   

                }
                else if($d->trans_option == 2)
                {
                    $rech_net_per = $d->apirecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->apirecharge1[0]->rech_net_surp;
                    $rech_total = $d->apirecharge1[0]->rech_total;
                    $user_balance = $d->apirecharge1[1]->user_balance;

                     // DEFINITELY FAILURE
                     $rech_status = "FAILURE";
                     $rech_code = 2;
                }

                $rech_mode = 'API';

                $rech_req_status = '';
                $rech_all_mode = 'API_RECHARGE';
                $reply_opr_id = $d->apirecharge2->reply_opr_id;
                $reply_opr_date = $d->apirecharge2->reply_date;
                
            }

            echo "<tr><td>".$j."</td><td>".$trans_id."</td>";
            echo "<tr><td>".$api_trans_id."</td><td>".$user_name."</td>";

            echo "<tr><td>".$net_code."</td><td>".$rech_mobile."</td>";
            echo "<tr><td>".$rech_amount."</td><td>".$rech_date."</td>";
            echo "<tr><td>".$rech_amount."</td><td>".$rech_date."</td>";

            echo "<tr><td>".$rech_net_per."</td><td>".$rech_net_surp."</td>";
            echo "<tr><td>".$rech_total."</td><td>".$user_balance."</td>";
            
            echo "<tr><td>".$rech_req_status."</td><td>".$rech_status."</td>";
            echo "<tr><td>".$rech_mode."</td><td>".$rech_all_mode."</td><td>".$rech_code."</td>";

            echo "<tr><td>".$reply_opr_id."</td><td>".$reply_opr_date."</td></tr>";

            $j++;
        }
        
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }
}
