<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiUrlDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_url_details';
    protected $fillable = array(
        'trans_id',
        'api_type',
        'api_request',
        'api_result'
    );

    public $timestamps = true;
}
