<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiProviderNetworkDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_provider_network_details';
    protected $fillable = array(
        'api_net_tr_id',
        'api_code',
        'net_code',
        'api_net_short_code',
        'api_net_per',
        'api_net_surp'
    );

    public $timestamps = true;
}
