<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankTransferDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_bank_transfer_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'bn_id',
        'rem_id',
        'bk_acc_no',
        'bk_code',
        'bk_trans_type',
        'mt_amount',
        'mt_per',
        'mt_per_amt',
        'mt_surp',
        'mt_total',
        'user_balance',
        'mt_date',
        'mt_req_status',
        'mt_status',
        'mt_mode',
        'mt_option',
        'api_code',
        'mt_reply_trans_id',
        'mt_reply_id',
        'mt_reply_date'
    );

    public $timestamps = true;
}
