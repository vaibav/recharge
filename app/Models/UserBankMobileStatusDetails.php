<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBankMobileStatusDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_bank_mobile_status_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'msisdn',
        'screen_type'
    );

    public $timestamps = true;
}
