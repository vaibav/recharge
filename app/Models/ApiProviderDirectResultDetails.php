<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiProviderDirectResultDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_provider_direct_result_details';
    protected $fillable = array(
        'api_res_tr_id',
        'api_code',
        'api_res_para_name',
        'api_res_para_field',
        'api_res_para_value'
    );

    public $timestamps = true;
}
