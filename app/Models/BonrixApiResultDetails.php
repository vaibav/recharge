<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonrixApiResultDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'bonrix_api_result_details';
    protected $fillable = array(
        'trans_id',
        'result_url',
        'result_status'
    );

    public $timestamps = true;
}
