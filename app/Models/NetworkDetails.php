<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'network_details';
    protected $fillable = array(
        'net_tr_id',
        'net_code',
        'net_name',
        'net_short_code',
        'net_type_code',
        'net_status'
    );

    public $timestamps = true;
}
