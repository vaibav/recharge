<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RechargeInfoDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'recharge_info_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'rech_type',
        'rech_mobile'

    );

    public $timestamps = true;
}
