<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccountBalanceDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_account_balance_details';
    protected $fillable = array(
        'user_code',
        'user_name',
        'user_balance'
    );

    public $timestamps = true;
}
