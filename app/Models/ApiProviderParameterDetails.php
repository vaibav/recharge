<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiProviderParameterDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'api_provider_parameter_details';
    protected $fillable = array(
        'api_para_tr_id',
        'api_code',
        'api_para_type',
        'api_para_name',
        'api_para_value'
    );

    public $timestamps = true;
}
