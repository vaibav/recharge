<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkTypeDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'network_type_details';
    protected $fillable = array(
        'net_type_tr_id',
        'net_type_code',
        'net_type_name',
        'net_type_status'
    );

    public $timestamps = true;
}
