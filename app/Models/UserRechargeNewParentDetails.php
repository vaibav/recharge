<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeNewParentDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_new_parent_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'parent_name',
        'child_name',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_net_per',
        'rech_net_per_amt',
        'rech_net_surp',
        'rech_total',
        'user_balance',
        'rech_date',
        'rech_status',
        'rech_type',
        'rech_mode',
        'rech_option'
    );

    public $timestamps = true;
    
    public function newparentrecharge2()
    {
        return $this->hasOne('App\Models\UserRechargeNewStatusDetails', 'trans_id', 'trans_id');
    }

    public function backup()
    {
        return $this->hasOne('App\Models\BackupRechargeParentDetails', 'trans_id', 'trans_id');
    }
}
