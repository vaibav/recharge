<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserComplaintDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_complaint_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'user_type',
        'comp_type',
        'rech_trans_id',
        'rech_mobile',
        'rech_amount',
        'user_complaint',
        'admin_reply',
        'reply_status',
        'reply_date'
    );

    public $timestamps = true;
}
