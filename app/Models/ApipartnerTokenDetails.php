<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApipartnerTokenDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'apipartner_token_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'api_type',
        'api_ip_address',
        'auth_token'
    );

    public $timestamps = true;
}
