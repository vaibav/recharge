<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBeneficiaryAccVerifyDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_beneficiary_acc_verify_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'agent_id',
        'msisdn',
        'ben_acc_no',
        'ben_code',
        'ben_surplus',
        'user_bal',
        'ben_date',
        'api_code',
        'ben_mode',
        'ben_status',
        'ben_rep_opr_id',
    );

    public $timestamps = true;
}
