<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminOfferDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'admin_offer_details';
    protected $fillable = array(
        'trans_id',
        'offer_details',
        'user_type',
        'offer_status'
    );
}
