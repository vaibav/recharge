<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'net_code',
        'rech_mobile',
        'rech_amount',
        'rech_total',
        'rech_date',
        'rech_status',
        'rech_mode',
        'api_code',
        'trans_bid',
        'reply_opr_id',
        'reply_date'
    );

    public $timestamps = true;

    public function userpayment()
    {
        return $this->hasMany('App\Models\UserRechargePaymentDetails', 'trans_id', 'trans_id');
    }
}