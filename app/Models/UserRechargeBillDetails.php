<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRechargeBillDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_recharge_bill_details';
    protected $fillable = array(
        'trans_id',
        'api_trans_id',
        'user_name',
        'net_code',
        'con_name',
        'con_acc_no',
        'con_mobile',
        'con_amount',
        'con_due_date',
        'con_net_per',
        'rech_net_per_amt',
        'con_net_surp',
        'con_total',
        'user_balance',
        'con_req_status',
        'con_status',
        'con_mode',
        'con_option',
        'api_code',
        'reply_opr_id',
        'reply_date'
    );

    public $timestamps = true;
}
