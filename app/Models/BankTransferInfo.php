<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;

use App\Models\OfferLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

class BankTransferInfo
{
    //
    public static function getAgent1NetworkLine($info_type)
    {
        $off_1 = new OfferLineDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Get Details
        $d1 = $off_1->where([['offer_type', '=', $info_type], ['offer_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
       
        return array($api_code, $v);

    }


    public static function generateBankAPI($api_code, $data)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            //$url_link = $url_link."?";

            // Get Paramenters
            $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    if ($d->api_para_type == "PRE_DEFINED")
                    {
                        $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                    }
                    else if ($d->api_para_type == "USER_DEFINED")
                    {
                        foreach($data as $key => $value)
                        {
                            if($d->api_para_name == $key)
                            {
                                $url_link = $url_link.$d->api_para_name."=".$value."&";
                                break;
                            }
                        }

                        
                    }
                    
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }

    public static function generateBankBeneficiaryNewAPI($api_code, $data)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            //$url_link = $url_link."?";

            // Get Paramenters
            $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    if ($d->api_para_type == "PRE_DEFINED")
                    {
                        $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                    }
                    else if ($d->api_para_type == "USER_DEFINED")
                    {
                        foreach($data as $key => $value)
                        {
                            if($d->api_para_name == $key)
                            {
                                if($key != "beneficiary_id" && $value != "")
                                {
                                    $url_link = $url_link.$d->api_para_name."=".$value."&";
                                    break;
                                   
                                }
                                
                            }
                        }

                        
                    }
                    
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }

}
