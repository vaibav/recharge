<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPaymentDetails extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user_payment_details';
    protected $fillable = array(
        'trans_id',
        'user_name',
        'user_amount',
        'user_req_date',
        'grant_user_name',
        'grant_user_amount',
        'grant_date',
        'payment_mode',
        'user_remarks',
        'trans_status'
    );

    public $timestamps = true;
}
