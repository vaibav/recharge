<?php

namespace App\Http\Middleware;

use Closure;

class CheckValidApipartner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->session()->has('valid'))
        {
            if($request->session()->has('mode'))
            {
                $op = $request->session()->get('valid');
                $md = $request->session()->get('mode');
                if($op == 1 && $md == "API PARTNER")
                {
                    return $next($request);
                }
                else
                {
                    return redirect('login')->with('data', [ 'output' => "Invalid User... Login First.."]); 
                }
            }
            
        }
        
        
        return redirect('login')->with('data', [ 'output' => "Invalid User...Login First.."]);
    }
}
