<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;

use App\Models\ApiUrlDetails;

class AdminApiUrlController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.api_details_1', ['user' => $ob]);
        
    }

    public function view_date(Request $request)
	{
        $api_1 = new ApiUrlDetails;

        $ob = GetCommon::getUserDetails($request);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $api_type = trim($request->api_type);

        $dat = date("Y-m-d");
        $f_date = "";
        $t_date = "";

        if($date_1 != "" && $date_2 != "")
        {
            $f_date = $date_1." 00:00:00";
            $t_date = $date_2." 23:59:59";
        }
        else
        {
            $f_date = $dat." 00:00:00";
            $t_date = $dat." 23:59:59";
        }
        
        if ($api_type != "-" && $api_type != "")
        {
            $d1 = $api_1->where('api_type', '=', $api_type)
                        ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->paginate(15);
        }
        else
        {
            $d1 = $api_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->paginate(15);
        }
        
        
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);
        
        return view('admin.api_details_2', ['user' => $ob, 'api' => $d1]);
    }
    
}
