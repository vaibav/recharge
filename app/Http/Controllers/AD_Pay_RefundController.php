<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;

use App\Models\NetworkDetails;
use App\Models\AllTransaction;

class AD_Pay_RefundController extends Controller
{
    //
    public function index(Request $request)
	{
         
        $ob = GetCommon::getUserDetails($request);

        $data = UserAccountDetails::select('user_code','user_name')->where('user_name', '!=', $ob->user)->orderBy('user_name')->get();
        $data1 = AllTransaction::where('trans_type', 'REFUND_PAYMENT')->orderBy('id', 'desc')->limit(6)->get();

        return view('admin.ad_pay_refund', ['user1' => $data, 'pay1' => $data1, 'user' => $ob]);
        		
    }

    

    public function store(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        
        $op = "NONE";
        
        // Validation
        $this->validate($request, [
            'user_name' => 'required',
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_name.required' => ' The user name is required.',
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.'
        ]);

        $ob = GetCommon::getUserDetails($request);
        
        // Post Data
        $data['trans_id'] = rand(100000,999999);
        $data['user_name_req'] = trim($request->user_name);
        $data['user_name_grnt'] = $ob->user;
        $data['user_amount'] = trim($request->user_amount);
        $data['user_remarks'] = trim($request->user_remarks);
        $data['payment_mode'] = "REFUND_PAYMENT";

        $d1 = NetworkDetails::select('net_code')->where('net_name', 'REFUND_PAYMENT')->get();

        $net_code = '0';
        if($d1->count() > 0) {
            $net_code = $d1[0]->net_code;
        }

        $data['net_code'] = $net_code;

        list($status, $op) = Stock::PaymentRefund($data);

        // Insert Data
       /* $cnt = $user_1->where('user_name', '=', $user_name)->count();
        if($cnt > 0)
        {
            // Check Admin balance Amount
            $u_bal_1 = "";
            $u_bal_2 = "";
            $flg_1 = 0;
            $net_bal_1 = 0;
            $net_bal_2 = 0;

            $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name_grnt)->get();
            if($d1->count() > 0)
            {
                $u_bal_1 = $d1[0]->user_balance;        // From User Balance

                if(floatval($u_bal_1) < floatval($user_amount))
                {
                    $flg_1 = 1;     // Granted user has less Amount...
                }
                else
                {
                    // Deduct Amount from user name...
                    $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);

                    $net_bal_1 = $this->convertNumber($net_bal_1);
                                            
                    $uacc_1->where('user_name', '=', $user_name_grnt)
                            ->update(['user_balance'=>$net_bal_1, 'updated_at'=>$date_time]);
                }

                if($flg_1 == 0)
                {
                    // Add amount to user balance Account
                    $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    if($d2->count() > 0)
                    {
                        $u_bal_2 = $d2[0]->user_balance;
                    }

                    if($u_bal_2 != "")
                    {
                        // Add Amount to user name...
                        $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);

                        $net_bal_2 = $this->convertNumber($net_bal_2);
                        
                        $uacc_1->where('user_name', '=', $user_name)
                                    ->update(['user_balance'=>$net_bal_2, 'updated_at'=>$date_time]);
                    }
                    else
                    {
                        $net_bal_2 = floatval($user_amount);         // Granted User has No Amount (No Entry...Insert It..)

                        $net_bal_2 = round($net_bal_2, 2);
                        $net_bal_2 = number_format($net_bal_2, 2, '.', '');
                        
                        $uacc_1->user_code = $user_code;
                        $uacc_1->user_name = $user_name;
                        $uacc_1->user_balance = $net_bal_2;
                        $uacc_1->save();
                    }

                    $payment_mode = "REFUND_PAYMENT";

                    // Amount Transfer Details....
                    $upay_1->trans_id = $trans_id;
                    $upay_1->user_name = $user_name;
                    $upay_1->user_amount = $this->convertNumber($user_amount);
                    $upay_1->user_req_date = $date_time;
                    $upay_1->grant_user_name = $user_name_grnt;
                    $upay_1->grant_user_amount = $this->convertNumber($user_amount);
                    $upay_1->grant_date = $date_time;
                    $upay_1->payment_mode = $payment_mode;
                    $upay_1->user_remarks = $user_remarks;
                    $upay_1->trans_status = 1;
                
                    $upay_1->save();
                    
                     // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name_grnt, 'pay_type' => "R",
                    'pay_amount' => $this->convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name, 'pay_date' => $date_time, 
                    'agent_name' => "-", 'pay_remarks' => $payment_mode, 'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
    
                    $upay_2->insert($ar1);


                    // Transaction Details.....
                    $tran_1->trans_id = $trans_id;
                    $tran_1->user_name = $user_name;
                    $tran_1->trans_type = $payment_mode;
                    $tran_1->trans_option = '1';
                    
                    $tran_1->save();

                    $op = "Amount is transferred successfully...";
                }
                else if($flg_1 == 1)
                {
                    $op = "Error! Your Account Balance is Low...";
                }
                
            }
            else if($flg_1 == 2)
            {
                $op = "Error! Your have no Account Balance...";
            }
            
        }
        else
        {
            $op = "User Name does not Exists...";
        }*/

        
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }


}
