<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;
use App\Models\NetworkDetails;

class DS_ANDR_RechReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $this->getRechargeDetailsDistributor($user_name);

            return view('android.recharge_report_ds_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'recharge' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
           
            return view('android.recharge_report_ds_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view_date(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $f_date = trim($request->f_date);
        $t_date = trim($request->t_date);
        $rech_type = trim($request->rech_type);
        // $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $this->getRechargeDetailsDistributor_1($user_name, $f_date, $t_date, $rech_type, $user_mobile);

            return view('android.recharge_report_ds_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'recharge' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function getRechargeDetailsDistributor($user_name)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $rech_2 = new UserRechargeBillDetails;
        $rech_3 = new UserBankTransferDetails;
        $net_2 = new NetworkDetails;
    
        $user_name = strtoupper($user_name);

        $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->orderBy('id', 'desc')->limit(20)->get();

        $d3 = $net_2->select('net_code','net_name')->get();

        $j = 0;
        $str = "";
        foreach($d1 as $f)
        {
                                                    
            $net_name = "";
            foreach($d3 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;
            

            if($f->rech_type == "RECHARGE")
            {
                $reply_id = $f->newparentrecharge2->reply_opr_id;
                $reply_date =$f->newparentrecharge2->reply_date;
            }
            else if($f->rech_type == "BILL_PAYMENT")
            {
                $d2 = $rech_2->select('reply_opr_id', 'reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->reply_opr_id;
                    $reply_date =$d2[0]->reply_date;
                }
                
            }
            else if($f->rech_type == "BANK_TRANSFER")
            {
                $d2 = $rech_3->select('mt_reply_id', 'mt_reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->mt_reply_id;
                    $reply_date =$d2[0]->mt_reply_date;
                }
                
            }

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');


            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 

            if($status == "SUCCESS") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p style='font-size: 14px;margin: 0;'>".$f->rech_amount."</p></div>";
        
            
            $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->user_name."</p>"; 
            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->rech_mobile."</p>"; 
            
            if($z == 0) {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$reply_id."</p>"; 
            }
            else if($z == 1) {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
            }

            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p>"; 
            
            $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;margin-top:10px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($c_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'><i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->rech_total,2, ".", "")."</p>";
            $str = $str."</div></div></div>";

                                     
            $j++;
        }

        return $str; 

    }

    public function getRechargeDetailsDistributor_1($user_name, $f_date, $t_date, $rech_type, $user_mobile)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $rech_2 = new UserRechargeBillDetails;
        $rech_3 = new UserBankTransferDetails;
        $net_2 = new NetworkDetails;

        $user_name = strtoupper($user_name);

        $d1 = [];

        if($f_date != "" && $t_date != "")
        {
            $f_date = $f_date." 00:00:00";
            $t_date = $t_date." 23:59:59";
        }

        if($f_date != "" && $t_date != "" && $rech_type == "RECHARGE")
        {
            $d1 = $rech_n_1->with(['newparentrecharge2'])
                        ->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date])
                        ->where('rech_type', '=', 'RECHARGE')
                        ->orderBy('id', 'desc')->limit(20)->get();

            
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $d1 = $rech_n_1->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date])
                        ->where('rech_type', '=', 'BILL_PAYMENT')
                        ->orderBy('id', 'desc')->limit(20)->get(); 

           
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $d1 = $rech_n_1->whereRaw('upper(parent_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date])
                        ->where('rech_type', '=', 'BANK_TRANSFER')
                        ->orderBy('id', 'desc')->limit(20)->get(); 

        }
        
       
        if($user_mobile != "")
        {
            $d1 = $d1->filter(function ($d) use ($user_mobile){
                return $d->rech_mobile == $user_mobile;
            });
        }
        
        // return response()->json($d1, 200);
        
        $d2 = $net_2->select('net_code','net_name')->get();
     
      
        $j = 0;
        $str = "";
        foreach($d1 as $f)
        {
                                                    
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;
            

            if($f->rech_type == "RECHARGE")
            {
                $reply_id = $f->newparentrecharge2->reply_opr_id;
                $reply_date =$f->newparentrecharge2->reply_date;
            }
            else if($f->rech_type == "BILL_PAYMENT")
            {
                $d2 = $rech_2->select('reply_opr_id', 'reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->reply_opr_id;
                    $reply_date =$d2[0]->reply_date;
                }
                
            }
            else if($f->rech_type == "BANK_TRANSFER")
            {
                $d2 = $rech_3->select('mt_reply_id', 'mt_reply_date')->where('trans_id', '=', $f->trans_id)->get();
                if($d2->count() > 0)
                {
                    $reply_id = $d2[0]->mt_reply_id;
                    $reply_date =$d2[0]->mt_reply_date;
                }
                
            }

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');


            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 

            if($status == "SUCCESS") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p style='font-size: 14px;margin: 0;'>".$f->rech_amount."</p></div>";
        
            
            $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$f->user_name."</p>"; 
            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->rech_mobile."</p>"; 
            
            if($z == 0) {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$reply_id."</p>"; 
            }
            else if($z == 1) {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
            }

            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p>"; 
            
            $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;margin-top:10px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($c_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'><i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->rech_total,2, ".", "")."</p>";
            $str = $str."</div></div></div>";

                                     
            $j++;
        }

        return $str; 
    }


    
}
