<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Libraries\GetCommon;
use App\Libraries\GetAPICommon;
use App\Libraries\RechargeInfo;
use App\Libraries\RechargeOffers;
use App\Libraries\BankRemitter;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\AdminOfferDetails;
use App\Models\UserRechargeNewDetails;

use App\Models\RechargeInfoDetails;

class RT_RechargeOfferController extends Controller
{
    //
    public function check_offer(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = trim($request->user_mobile);

        $net_type_code = "0";
        $rdata = [];
        $str = 'No Plan Available for this Mobile..';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getOffer121($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999));

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (!empty($dx)) 
                {
                    if (array_key_exists('records', $dx)) {
                        $data = (array)$dx['records'];
                        
                        foreach($data as $d)
                        {
                            $c = (array)$d;
                            $key = "";
                            $val = "";
                            if (array_key_exists('desc', $c)) {
                                $key = $c['desc'];
                            }
                            if (array_key_exists('rs', $c)) {
                                $val = $c['rs'];
                            }

                            array_push($rdata, ['desc' => $key, 'price' => $val]);
                        }
                        
                    }
                    else
                    {
                        array_push($rdata, ['desc' => "No Plan Available for this Mobile..", 'price' => ""]);
                    }

                }

                //display data
                if(!empty($rdata)) 
                {
                    $str = "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($rdata as $d)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$d['desc'].'</td>';
                        $str = $str . '<td style = "font-size:14px;padding:4px 4px;"><a class="btn-floating center-align #7b1fa2 blue darken-2" id="id_off">'.$d['price'].'</a>';
                        $str = $str . '</td></tr>';
                    }
                    $str = $str . "</tbody>";
                }

                
            }
            else
            {
                $str =  "No Plan Available for this Mobile..";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "No Plan Available for this Mobile..";
        }
    
        return $str;
	
    }

    public function check_plan(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = "0";

        $net_type_code = "0";
        $rdata = [];
        $str = "";

        if($net_code != "")
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "MOBILE_PLAN");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getPlan($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999), $net_code);

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (!empty($dx)) 
                {

                    if (array_key_exists('records', $dx)) {
                        $data = (array)$dx['records'];

                        foreach($data as $key => $value)
                        {
                            $content = [];
                            foreach($value as $v)
                            {
                                $sx = "";
                                $sy = "";
                                foreach($v as $k1 => $v1)
                                {
                                    if($k1 == "desc") {
                                        $sx = $v1;
                                    }
                                    else if($k1 == "rs") {
                                        $sy = $v1;
                                    }
                                }
                                if($sx != "" && $sy != "") {
                                    array_push($content, ['text' => $sx, 'price' => $sy]);
                                }
                                
                            }

                            array_push($rdata, ['title' => $key, 'content' => $content]);
                        }
                        
                        
                    }
                    else
                    {
                        array_push($rdata, ['title' => 'No plans available in this No.', 'conent' => '']);
                    } 
                }
                if(!empty($rdata)) 
                {
                    $str = $str . "<ul id='tabs-swipe-demo' class='tabs'>";
                    $j = 1;
                    foreach($rdata as $d)
                    {
                        $str = $str ."<li class='tab col s2' style='padding:0px 0px;'>";
                        if($j == 1)
                        {
                            $str = $str . "<a class='active' href='#page_".$j."' style='padding:0px 2px; font-size:12px;'>
                                            ".$d['title']."</a></li>";
                        }
                        else
                        {
                            $str = $str . "<a href='#page_".$j."' style='padding:0px 2px; font-size:12px;'>
                                            ".$d['title']."</a></li>";
                        }
                        
                        $j++;
                    }  
                    $str = $str . "</ul>";  
                    $j = 1;
                    foreach($rdata as $d)
                    {
                        $str = $str ."<div id='page_".$j."' class='col s12'>";
                        $str = $str ."<table class='striped responsive-table'>";
                        foreach($d['content'] as $c)
                        {
                            
                            $str = $str . "<tr><td class='left-align' style = 'font-size:14px;padding:4px 4px;'>".$c['text']."</td>
                                                <td class='right-align' style = 'font-size:14px;padding:4px 4px;'>";
                            $str = $str . "<a class='btn-floating center-align #7b1fa2 blue darken-2' id='id_off'>".$c['price']."</a></td></tr>";
                            
                        }
                        $str = $str ."</table></div>";
                        $j++;
                    }  
                
                }
                else
                {
                    $str = "No Plans Available for this Network...";
                }
            
            }

        }

    
        return $str;
	
    }

    public function check_dth(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = trim($request->user_mobile);

        $net_type_code = "0";
        $str = '';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getDth($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999));

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
                    
                    foreach($data as $key => $value)
                    {
                        foreach($value as $key1 => $value1)
                        {
                            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key1.'</td>';
                            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.(string)$value1.'</td></tr>';
                        }
                       
                    }
                    
                }
            }
            else
            {
                $str =  "No Details Available....";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "No Details Available....";
        }
    
        return $str;
	
    }

     public function check_eb(Request $request)
    {

        // Validation
        $validator = Validator::make($request->all(), [
            'net_code'      => 'required',
            'con_no'    => 'required',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['status' => false, 'message' => $message];
            return response()->json($result, 200);
        }

        $netTypeCode     = "28";
        $netCode         = trim($request->net_code);
        $consumerNo      = trim($request->con_no);

        $customerName    = "-";
        $customerAmount  = "-";
        $customerDueDate = "-";
        $str             = '';

        // Post Data
        list($apiCode, $v) = RechargeInfo::getInfoNetworkLine($netCode, "BILL_CHECK");

        if($apiCode == 0) {
            return response()->json(['status' => false, 'message' => 'There is problem in network!']); ;
        }

        $apiUrl = RechargeInfo::generateInfoAPI($apiCode, $netCode, $consumerNo);

        file_put_contents(base_path().'/public/sample/eb_api_url.txt', $apiUrl, FILE_APPEND);

        $resContent = BankRemitter::runUrl($apiUrl, "BILL_CHECK");

        $jx = str_replace("\\\\\"", "###dq###", $resContent);
        $jx = str_replace("\\", "", $jx);
        $jx = str_replace("###dq###", "\\\"", $jx);
        $jx = str_replace("\n", " ", $jx);

        $dx = (array)json_decode($jx);

        
        if($dx['status'] == false)
        {
            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Message</td>';
             $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$dx['message'].'</td></tr>';
        }
        else if($dx['status'] == true)
        {
            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Customer Name </td>';
            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$dx['customerName'].'</td></tr>';

            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">billAmount </td>';
            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$dx['billAmount'].'</td></tr>';

            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">billStatus </td>';
            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$dx['billStatus'].'</td></tr>';

            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Due Date </td>';
            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$dx['dueDate'].'</td></tr>';

            $customerName    = $dx['customerName'];
            $customerAmount  = $dx['billAmount'];
            $customerDueDate = $dx['dueDate'];
        }

        $result = ['status' => $dx['status'], 'message' => $str, 'cus_amt' => $customerAmount, 
                                                'cus_name' => $customerName, 'cus_dued' => $customerDueDate];

        return response()->json($result); 
       
    }

    public function check_operator(Request $request)
	{

        $ob = GetCommon::getUserDetails($request);

        $mobile = trim($request->user_mobile);

        $net_type_code = "0";
        $net_code = "857";
        $str = '';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            $api_code = RechargeInfo::getNewNetworkLine($net_code, "OPERATOR_CHECK");

            
            if($api_code != 0)
            {


                $res_content = RechargeOffers::checkOperator($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999));

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (!empty($dx)) 
                {
                    $records = (array)$dx['records'];
                    if (array_key_exists('Operator', $records)) {

                        $str = $records['Operator'];
                        $str = strtoupper($str);
                    }
                    
                }
            }
            else
            {
                $str =  "0.";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "0";
        }
    
        return $str;
	
    }


}
