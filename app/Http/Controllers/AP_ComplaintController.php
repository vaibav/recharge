<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportApipartner;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserComplaintDetails;

class AP_ComplaintController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('apipartner.complaint_1', ['user' => $ob]);
        
    }

    public function recharge_apipartner(Request $request)
	{
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $mobile = trim($request->user_mobile);

        if($date_1 != "" && $date_2 != "")
        {
            $f_date = $date_1." 00:00:00";
            $t_date = $date_2." 23:59:59";
        }
        else
        {
            $f_date = "";
            $t_date = "";
        }
        

        $dc4 = RechargeReportApipartner::getRechargeComplaint($f_date, $t_date, $ob->user, $mobile);

        $d2 = $net_2->select('net_code','net_name')->get();
        
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('apipartner.complaint_recharge', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2]); 

    }

    public function store_apipartner_recharge(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;

        $ob = GetCommon::getUserDetails($request);

        $rech_tr_id = trim($request->trid);
        $rech_mobile = trim($request->mob);
        $rech_amount = trim($request->amt);
        $user_complaint = trim($request->cmp);


        //file_put_contents('complaint.txt', $rech_tr_id."--".$rech_mobile."--".$rech_amount."--".$user_complaint);
        
        // Insert Record... 
        $comp_1->trans_id = rand(10000,99999);
        $comp_1->user_name = $ob->user;
        $comp_1->user_type = $ob->mode;
        $comp_1->comp_type = "RECHARGE";
        $comp_1->rech_trans_id = $rech_tr_id;
        $comp_1->rech_mobile = $rech_mobile;
        $comp_1->rech_amount = $rech_amount;
        $comp_1->user_complaint = $user_complaint;
        $comp_1->admin_reply = "";
        $comp_1->reply_status = 1;
        $comp_1->reply_date = date("Y-m-d H:i:s");

        $comp_1->save();
        
        $op = 1;

       
        return redirect()->back()->with('msg', $op);

		
		
    }

    public function user_view(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->where('user_name', '=', $ob->user)->get();

        return view('apipartner.complaint_view', ['comp1' => $data, 'user' => $ob]);
        
    }
    
}
