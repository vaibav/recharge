<?php

namespace App\Http\Controllers\Retailer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;

class ReportController extends Controller
{
    //
    public function viewOne(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.report.view_one', ['user' => $ob]);
    }

    public function viewTwo(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [ 'f_date' => 'required', 't_date' => 'required' ],
                                    [ 'f_date.required' => ' The From Date is required.', 't_date.required' => ' The To Date is required.' ]);

        $fromDate = trim($request->f_date)." 00:00:00";
        $toDate   = trim($request->t_date)." 23:59:59";

        $rechargeDetails = AllTransaction::whereBetween('rech_date', [$fromDate, $toDate])->whereRaw('upper(user_name) = ?',[$ob->user])
                                                        ->orderBy('id', 'desc')->get();

        $networkDetails = NetworkDetails::select('net_code','net_name')->get();


        return view('retailer.report.view_two', ['user' => $ob, 'rechargeDetails' => $rechargeDetails, 'networkDetails' => $networkDetails]);
    }
}