<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Stock;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;

class RT_ANDR_PayRequest extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...

            return view('android.payment_request_rt_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $user_amount = trim($request->user_amt);
        $user_remarks = trim($request->user_remarks);

        $z1 = 0;
        $z = 0;
       

        list($q, $user_name_req, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {

            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = UserAccountDetails::select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            //Only Retailer...
            $data = [];
            $data['trans_id'] = rand(100000,999999);
            $data['user_name_req'] = $user_name_req;
            $data['user_name_grnt'] = $user_name_grnt;
            $data['user_amount'] = $user_amount;
            $data['user_remarks'] = $user_remarks;
            $data['payment_mode'] = "FUND_TRANSFER";

            $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

            $net_code = '0';
            if($d1->count() > 0) {
                $net_code = $d1[0]->net_code;
            }

            $data['net_code'] = $net_code;

            list($status, $op) = Stock::PaymentRequest($data);

            return redirect()->back()->with('msg', $op);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $user_name = strtoupper($user_name);
            $d1 = AllTransaction::whereRaw('upper(user_name) = ?',[$user_name])
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                    ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                    ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                                })->orderBy('id', 'desc')->get(); 

            return view('android.payment_request_rt_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'payment' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

   
}
