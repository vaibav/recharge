<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use stdClass;
use DateTime;
use App\Libraries\GetCommon;
use App\Libraries\SmsInterface;
use App\Libraries\ApiUrl;
use App\Libraries\RechargeMobile;
use App\Libraries\RechargeBill;
use App\Libraries\RechargeInfo;
use App\Libraries\PercentageCalculation;
use App\Libraries\BankRemitter;
use App\Libraries\Beneficiary;
use App\Libraries\BankTransfer;
use App\Libraries\RechargeNewMobile;

// User
use App\Models\UserAccountDetails;
use App\Models\UserPersonalDetails;
use App\Models\MobileUserDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserNetworkDetails;
use App\Models\UserChainDetails;

// Network
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;
use App\Models\NetworkLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

// Recharge
use App\Models\UserRechargeDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeApipartnerDetails;
use App\Models\UserRechargeRequestDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\BonrixApiResultDetails;

// Offer & Complaint
use App\Models\AdminOfferDetails;
use App\Models\UserComplaintDetails;
use App\Models\RechargeInfoDetails;

// Payment
use App\Models\UserPaymentDetails;
use App\Models\PaymentLedgerDetails;


class MobileApiController extends Controller
{

    /**
     * Login check
     */
    public function login_check(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $mob_1 = new MobileUserDetails;

        $result = [];
        $op = "NONE";
        $auth_token = "";
        
        $user_name = trim($request->user_name);
        $user_pwd = trim($request->user_pwd);
        $mobile_imei = trim($request->mobile_imei);
        $mobile_name = trim($request->mobile_name);

        $d1 = $user_2->select('user_code','user_type','user_status')->where([['user_name', '=', $user_name], ['user_pwd', '=', $user_pwd]])->get();
        $d2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
        
        if($d1->count() > 0 && $d2->count() > 0)
        {
            $status = $d1[0]->user_status;

            if($status == "1")
            {
                $user_type = $d1[0]->user_type;
                $user_mobile = $d2[0]->user_mobile;
                $auth_token = uniqid(base64_encode(Str::random(30)));
                $user_otp = rand(1000,9999);

                $mob_1->user_name = $user_name;
                $mob_1->user_type = $user_type;
                $mob_1->mobile_imei = $mobile_imei;
                $mob_1->mobile_name = $mobile_name;
                $mob_1->auth_token = $auth_token;
                $mob_1->user_otp = $user_otp;
                $mob_1->user_status = 0;
                $mob_1->save();

                SmsInterface::callSms($user_name, $user_mobile, $user_otp);

                $result = ["output" => "Success! Valid user..", "auth_token" => $auth_token, "status" => 200];

            }
            else if($status == "2")
            {
                $result = ["output" => "Account is locked", "auth_token" => "", "status" => 600];
                
            }

        }
        else
        {
            $result = ["output" => "Invalid User", "auth_token" => "", "status" => 601];
            
        }

        return response()->json($result, 200);
    }

    public function logcheck(Request $request)
	{
        
        $user_name = trim($request->user_name);
       
        $result = ["output" => "Success! Server check..", "user name" => $user_name, "status" => 200];
        
        return response()->json($result, 200);
    }

    /**
     * Login OTP check
     */
    public function login_otp_check(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $mob_1 = new MobileUserDetails;

        $result = [];
        $op = "NONE";
        $auth_token = "";
        
        $auth_token = trim($request->auth_token);
        $user_otp = trim($request->user_otp);
        $date_time = date('Y-m-d H:i:s');
        
        $d1 = $mob_1->select('user_name', 'user_type', 'user_otp')->where('auth_token', '=', $auth_token)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $user_type = $d1[0]->user_type;
            $user_bal = 0;
            $otp = $d1[0]->user_otp;


            if($user_otp > 0)
            {
                $mob_1->where('auth_token', '=', $auth_token)->update(['user_status' => '1', 'updated_at' => $date_time]);

                $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                if($d2->count() > 0)
                {
                    $user_bal = $d2[0]->user_balance;
                }

                $result = ["output" => "OTP is verified successfully..", "user_name" => $user_name, 
                                "user_type" => $user_type, "user_bal" => $user_bal,  "status" => 200];

            }
            else 
            {
                $result = ["output" => "Invalid OTP", "user_name" => " ", 
                "user_type" => "", "user_bal" => 0, "status" => 602];
                
            }

        }
        else
        {
            $result = ["output" => "Invalid User", "user_name" => " ", 
                "user_type" => "", "user_bal" => 0, "status" => 601];
            
        }

        return response()->json($result, 200);
    }

    /**
     * Prepaid Network Details
     */
    public function getPrepaidDetails(Request $request)
    {
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $data = [];
        $result = [];

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'PREPAID')->get();
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
            foreach($d2 as $d)
            {
                array_push($data, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }

        return response()->json($result, 200);
    }

    /**
     * Postpaid Network Details
     */
    public function getPostpaidDetails(Request $request)
    {
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $data = [];
        $result = [];

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'POSTPAID')->get();
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
            foreach($d2 as $d)
            {
                array_push($data, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }

        return response()->json($result, 200);
    }

    /**
     * DTH Network Details
     */
    public function getDthDetails(Request $request)
    {
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $data = [];
        $result = [];

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'DTH')->get();
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
            foreach($d2 as $d)
            {
                array_push($data, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }

        return response()->json($result, 200);
    }

    /**
     * Bill Payment Network Details
     */
    public function getBillDetails(Request $request)
    {
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $data = [];
        $result = [];

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'BILL PAYMENT')->get();
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
            foreach($d2 as $d)
            {
                array_push($data, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }

        return response()->json($result, 200);
    }

    /**
     * All Network Details
     */
    public function getNetworkDetails(Request $request)
    {
        $net_2 = new NetworkDetails;
        $data = [];
        $result = [];

        $d1 = $net_2->select('net_code','net_name')->get();
        if($d1->count() > 0)
        {
            foreach($d1 as $d)
            {
                array_push($data, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }

        return response()->json($result, 200);
    }

    /**
     * Retailer Recharge Details - Last 10 records
     */
    public function getRetailerRechargeDetails_new(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_n_1 = new UserRechargeNewDetails;
        //$rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];
        $rs = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            // Opening
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment'])
                    ->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'WEB_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(10)->get(); 

            $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
            $d3 = $api_1->select('api_code','api_name')->get();

            $j = 1;
            foreach($d1 as $d)
            {
                
                if($d->trans_type == "WEB_RECHARGE")
                {
                    if(sizeof($d->newrecharge1) > 0)
                    {
                        $net_name = "";
                        foreach($d2 as $r)
                        {
                            if($d->newrecharge1[0]->net_code == $r->net_code)
                                $net_name = $r->net_name;
                        }
        
                       
        
                        $rech_status = "";
                        $status = "";
                        $o_bal = 0;
                        $u_bal = 0;
                        $r_tot = 0;
        
                        if($d->trans_option == 1)
                        {
                            $rech_status = $d->newrecharge1[0]->rech_status;
                            $rech_option = $d->newrecharge1[0]->rech_option;
                            $u_bal = $d->newrecharge1[0]->user_balance;
                            $r_tot = $d->newrecharge1[0]->rech_total;
                            
                        }
                        else if($d->trans_option == 2)
                        {
                            $rech_status = $d->newrecharge1[1]->rech_status;
                            $rech_option = $d->newrecharge1[1]->rech_option;
                            $u_bal = $d->newrecharge1[1]->user_balance;
                            $r_tot = $d->newrecharge1[1]->rech_total;
                            
                        }
                        if($rech_status == "PENDING" && $rech_option == "0")
                        {
                            $status = "PENDING";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        else if($rech_status == "PENDING" && $rech_option == "2")
                        {
                            $status = "FAILURE";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        else if($rech_status == "FAILURE" && $rech_option == "2")
                        {      
                            $status = "FAILURE";
                            $o_bal = floatval($u_bal) - floatval($r_tot);
                        }
                        else if ($rech_status == "SUCCESS")
                        {
                            $status = "SUCCESS";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        
                        
                        array_push($data, ['trans_id' => $d->trans_id, 
                                        'net_name' => $net_name,
                                        'rech_mobile' => $d->newrecharge1[0]->rech_mobile,
                                        'rech_amount' => $d->newrecharge1[0]->rech_amount,
                                        'rech_total' => $d->newrecharge1[0]->rech_total,
                                        'rech_date' => $d->newrecharge1[0]->rech_date,
                                        'rech_status' => $status,
                                        'reply_opr_id' => $d->newrecharge2->reply_opr_id,
                                        'o_bal' => $o_bal,
                                        'c_bal' => $u_bal]);
    
                    }
                    
                    
                }
                if($d->trans_type == "BILL_PAYMENT")
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->billpayment[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }
    
                    $api_name = "";
                    foreach($d3 as $r)
                    {
                        if($d->billpayment[0]->api_code == $r->api_code)
                            $api_name = $r->api_name;
                    }
    
                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;
    
                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->billpayment[0]->con_status;
                        $rech_option = $d->billpayment[0]->con_option;          
                        $r_tot = $d->billpayment[0]->con_total;
                        $u_bal = $d->billpayment[0]->user_balance;
                        $reply_id = $d->billpayment[0]->reply_opr_id;
                    }
                    else if($d->trans_option == 2)
                    {  
                        $rech_status = $d->billpayment[1]->con_status;
                        $rech_option = $d->billpayment[1]->con_option;          
                        $r_tot = $d->billpayment[1]->con_total;
                        $u_bal = $d->billpayment[1]->user_balance;
                        $reply_id = $d->billpayment[1]->reply_opr_id;
                    }
                    if($rech_status == "PENDING" && $rech_option == 1)
                    {
                        $status = "PENDING";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == 2)
                    {
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" )
                    {      
                        $status = "FAILURE";
                        //$o_bal = floatval($u_bal) - floatval($r_tot);
                        $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "SUCCESS";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    
                    array_push($data, ['trans_id' => $d->trans_id, 
                                        'net_name' => $net_name,
                                        'rech_mobile' => $d->billpayment[0]->con_acc_no,
                                        'rech_amount' => $d->billpayment[0]->con_amount,
                                        'rech_total' => $d->billpayment[0]->con_total,
                                        'rech_date' => $d->billpayment[0]->created_at->toDateTimeString(),
                                        'rech_status' => $status,
                                        'reply_opr_id' => $reply_id,
                                        'o_bal' => $this->convertNumberFormat($o_bal),
                                        'c_bal' => $this->convertNumberFormat($u_bal)]);
                    
    
                    
                } 
                                  
                $j++;
            }



            $result = ["status" => 200, 'data' => $data];
            // Ending
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);
       
        
       
    }

    /**
     * Retailer Recharge Details - datewise report
     */

    public function getRetailerRechargeDetails1_new(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        //$rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];
        $rs = [];

        $auth_token = trim($request->auth_token);
        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        $user_mobile = trim($request->mobile);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            // Opening
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment'])
                    ->where('user_name', '=', $user_name)
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'WEB_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(20)->get(); 
            

            $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
            $d3 = $api_1->select('api_code','api_name')->get();

            $j = 1;
            foreach($d1 as $d)
            {
            
                if($d->trans_type == "WEB_RECHARGE")
                {
                    if(sizeof($d->newrecharge1) > 0)
                    {
                        $net_name = "";
                        foreach($d2 as $r)
                        {
                            if($d->newrecharge1[0]->net_code == $r->net_code)
                                $net_name = $r->net_name;
                        }
        
                       
        
                        $rech_status = "";
                        $status = "";
                        $o_bal = 0;
                        $u_bal = 0;
                        $r_tot = 0;
        
                        if($d->trans_option == 1)
                        {
                            $rech_status = $d->newrecharge1[0]->rech_status;
                            $rech_option = $d->newrecharge1[0]->rech_option;
                            $u_bal = $d->newrecharge1[0]->user_balance;
                            $r_tot = $d->newrecharge1[0]->rech_total;
                            
                        }
                        else if($d->trans_option == 2)
                        {
                            $rech_status = $d->newrecharge1[1]->rech_status;
                            $rech_option = $d->newrecharge1[1]->rech_option;
                            $u_bal = $d->newrecharge1[1]->user_balance;
                            $r_tot = $d->newrecharge1[1]->rech_total;
                            
                        }
                        if($rech_status == "PENDING" && $rech_option == "0")
                        {
                            $status = "PENDING";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        else if($rech_status == "PENDING" && $rech_option == "2")
                        {
                            $status = "FAILURE";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        else if($rech_status == "FAILURE" && $rech_option == "2")
                        {      
                            $status = "FAILURE";
                            $o_bal = floatval($u_bal) - floatval($r_tot);
                        }
                        else if ($rech_status == "SUCCESS")
                        {
                            $status = "SUCCESS";
                            $o_bal = floatval($u_bal) + floatval($r_tot);
                        }
                        
                        
                        array_push($data, ['trans_id' => $d->trans_id, 
                                        'net_name' => $net_name,
                                        'rech_mobile' => $d->newrecharge1[0]->rech_mobile,
                                        'rech_amount' => $d->newrecharge1[0]->rech_amount,
                                        'rech_total' => $d->newrecharge1[0]->rech_total,
                                        'rech_date' => $d->newrecharge1[0]->rech_date,
                                        'rech_status' => $status,
                                        'reply_opr_id' => $d->newrecharge2->reply_opr_id,
                                        'o_bal' => $o_bal,
                                        'c_bal' => $u_bal]);
    
                    }
                    
                    
                }                                          
                if($d->trans_type == "BILL_PAYMENT")
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->billpayment[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;
                    $reply_id = "";

                    
                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->billpayment[0]->con_status;
                        $rech_option = $d->billpayment[0]->con_option;          
                        $r_tot = $d->billpayment[0]->con_total;
                        $u_bal = $d->billpayment[0]->user_balance;
                        $reply_id = $d->billpayment[0]->reply_opr_id;
                    }
                    else if($d->trans_option == 2)
                    {  
                        $rech_status = $d->billpayment[1]->con_status;
                        $rech_option = $d->billpayment[1]->con_option;          
                        $r_tot = $d->billpayment[1]->con_total;
                        $u_bal = $d->billpayment[1]->user_balance;
                        $reply_id = $d->billpayment[1]->reply_opr_id;
                    }
                    if($rech_status == "PENDING" && $rech_option == 1)
                    {
                        $status = "PENDING";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == 2)
                    {
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" )
                    {      
                        $status = "FAILURE";
                        //$o_bal = floatval($u_bal) - floatval($r_tot);
                        $o_bal = floatval($u_bal) - floatval($r_tot) ;
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "SUCCESS";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    
                    if($user_mobile == "")
                    {
                        array_push($data, ['trans_id' => $d->trans_id, 
                                        'net_name' => $net_name,
                                        'rech_mobile' => $d->billpayment[0]->con_acc_no,
                                        'rech_amount' => $d->billpayment[0]->con_amount,
                                        'rech_total' => $d->billpayment[0]->con_total,
                                        'rech_date' => $d->billpayment[0]->created_at->toDateTimeString(),
                                        'rech_status' => $status,
                                        'reply_opr_id' => $reply_id,
                                        'o_bal' => $this->convertNumberFormat($o_bal),
                                        'c_bal' => $this->convertNumberFormat($u_bal)]);
                    }
                    else if($user_mobile != "")
                    {
                        if($d->billpayment[0]->con_acc_no == $user_mobile)
                        {
                            array_push($data, ['trans_id' => $d->trans_id, 
                                        'net_name' => $net_name,
                                        'rech_mobile' => $d->billpayment[0]->con_acc_no,
                                        'rech_amount' => $d->billpayment[0]->con_amount,
                                        'rech_total' => $d->billpayment[0]->con_total,
                                        'rech_date' => $d->billpayment[0]->created_at->toDateTimeString(),
                                        'rech_status' => $status,
                                        'reply_opr_id' => $d->billpayment[0]->reply_opr_id,
                                        'o_bal' => $this->convertNumberFormat($o_bal),
                                        'c_bal' => $this->convertNumberFormat($u_bal)]);
                        }
                    }

                   
                    
                }   
            
                            
                $j++;
            }

            $result = ["status" => 200, 'data' => $data];
            // Ending
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);
       
        
       
    }

    public function getRetailerRechargeDetails1(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $rech_1 = new UserRechargePaymentDetails;
        $net_2 = new NetworkDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];


        $auth_token = trim($request->auth_token);
        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        $user_mobile = trim($request->mobile);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            if($user_mobile == "")
            {
                $d1 = $rech_1->select('trans_id', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_date', 'rech_status', 'user_balance')
                         ->with(['userrecharge'])
                         ->where('user_name', '=', $user_name)
                         ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->get();
            }
            else
            {
                $d1 = $rech_1->select('trans_id', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_date', 'rech_status', 'user_balance')
                         ->with(['userrecharge'])
                         ->where('user_name', '=', $user_name)
                         ->where('rech_mobile', '=', $user_mobile)
                         ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->get();
            }
            
            $d2 = $net_2->select('net_code','net_name')->get();
            foreach($d1 as $d)
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $c_bal = $d->user_balance;
                $o_bal = 0;
                $rech_status = "PENDING";
                if($d->rech_status == "FAILURE")
                {
                    $o_bal = floatval($c_bal) - floatval($d->rech_total);
                    $rech_status = "FAILURE";
                }
                else
                {
                    $o_bal = floatval($c_bal) + floatval($d->rech_total);
                    if($d->userrecharge->rech_status == "FAILURE")
                    {
                        $rech_status = "FAILURE";
                    }
                    else if($d->userrecharge->rech_status == "SUCCESS")
                    {
                        $rech_status = "SUCCESS";
                    }
                    else if($d->userrecharge->rech_status == "PENDING")
                    {
                        $rech_status = "PENDING";
                    }
                }
                $o_bal1 = round($o_bal, 2);
                $o_bal1 = number_format($o_bal1 ,2, ".", "");
                $c_bal = number_format($c_bal ,2, ".", "");

                array_push($data, ['trans_id' => $d->trans_id, 
                                    'net_name' => $net_name,
                                    'rech_mobile' => $d->rech_mobile,
                                    'rech_amount' => $d->rech_amount,
                                    'rech_total' => $d->rech_total,
                                    'rech_date' => $d->rech_date,
                                    'rech_status' => $rech_status,
                                    'reply_opr_id' => $d->userrecharge->reply_opr_id,
                                    'o_bal' => $o_bal1,
                                    'c_bal' => $c_bal]);
                                   
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);

    }

     /**
     * ApiPartner Recharge Details - Last 10 records
     */
    public function getApipartnerRechargeDetails(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $rech_1 = new UserRechargeApipartnerDetails;
        $net_2 = new NetworkDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];


        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $rech_1->select('trans_id', 'api_trans_id', 'net_code', 'rech_mobile', 'rech_amount', 'rech_date', 'rech_status', 'reply_opr_id')
                         ->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get();
            
            $d2 = $net_2->select('net_code','net_name')->get();
            foreach($d1 as $d)
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                array_push($data, ['trans_id' => $d->trans_id, 
                                    'api_trans_id' => $d->api_trans_id,
                                    'net_name' => $net_name,
                                    'rech_mobile' => $d->rech_mobile,
                                    'rech_amount' => $d->rech_amount,
                                    'rech_date' => $d->rech_date,
                                    'rech_status' => $d->rech_status,
                                    'reply_opr_id' => $d->reply_opr_id]);
                                   
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);

    }

     /**
     * Apipartner Recharge Details - datewise report
     */
    public function getApipartnerRechargeDetails1(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $rech_1 = new UserRechargeApipartnerDetails;
        $net_2 = new NetworkDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];


        $auth_token = trim($request->auth_token);
        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        $user_mobile = trim($request->mobile);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            //$user_name = $dc1[0]->user_name;
            $user_name = 'balaapi';
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            if($user_mobile == "")
            {
                $d1 = $rech_1->select('trans_id', 'api_trans_id', 'net_code', 'rech_mobile', 'rech_amount', 'rech_date', 'rech_status', 'reply_opr_id')
                         ->where('user_name', '=', $user_name)
                         ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->get();
            }
            else
            {
                $d1 = $rech_1->select('trans_id', 'api_trans_id', 'net_code', 'rech_mobile', 'rech_amount', 'rech_date', 'rech_status', 'reply_opr_id')
                         ->where('user_name', '=', $user_name)
                         ->where('rech_mobile', '=', $user_mobile)
                         ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->get();
            }
            
            $d2 = $net_2->select('net_code','net_name')->get();
            foreach($d1 as $d)
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                array_push($data, ['trans_id' => $d->trans_id, 
                                    'api_trans_id' => $d->api_trans_id,
                                    'net_name' => $net_name,
                                    'rech_mobile' => $d->rech_mobile,
                                    'rech_amount' => $d->rech_amount,
                                    'rech_date' => $d->rech_date,
                                    'rech_status' => $d->rech_status,
                                    'reply_opr_id' => $d->reply_opr_id]);
                                   
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);

    }

    /**
     * Get Offer details
     */
    public function getOfferDetails(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $off_1 = new AdminOfferDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $off_1->select('trans_id', 'offer_details')->where('offer_status', '=', 1)
                            ->where(function($query) use ($user_type){
                                return $query
                                ->where('user_type', '=', 'ALL')
                                ->orWhere('user_type', '=', $user_type);
                            })->orderBy('id', 'desc')->limit(10)->get();
                            
            foreach($d1 as $d)
            {
                array_push($data, ['trans_id' => $d->trans_id, 'offer_details' => $d->offer_details]);               
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);

    }

    /**
     *  Get Complaint Details
     */
    public function getComplaintDetails(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $comp_1 = new UserComplaintDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $comp_1->select('trans_id', 'rech_mobile', 'rech_amount', 'user_complaint', 'admin_reply', 'reply_status', 'reply_date', 'created_at')
                            ->where('user_name', '=', $user_name)
                            ->orderBy('id', 'desc')->limit(10)->get();
                            
            foreach($d1 as $d)
            {
                array_push($data, ['trans_id' => $d->trans_id, 
                                    'rech_mobile' => $d->rech_mobile,
                                    'rech_amount' => $d->rech_amount,
                                    'user_complaint' => $d->user_complaint,
                                    'admin_reply' => $d->admin_reply,
                                    'reply_status' => $d->reply_status,
                                    'reply_date' => $d->reply_date,
                                    'created_at' => $d->created_at]);
                
            }
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];       
        }
        
        return response()->json($result, 200);

    }


    /**
     * Get Stock Details
     */
    public function getRetailerStock(Request $request)
    {
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $debit = 0;
        $credit = 0;
        $balance = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $upay_1->where(function($query) use ($user_name){
                        return $query
                        ->where('user_name', '=', $user_name)
                        ->orWhere('grant_user_name', '=', $user_name);
                    })
                    ->where('trans_status', '=', 1)
                    ->orderBy('id', 'asc')->get();

            $rech_amount = $this->getRetailerRechargeAmount($user_name);
            
            $j = 0;
            foreach($d1 as $f)
            {
                if($f->user_name == $user_name && $f->trans_status == 1)
                {
                    // Debit +
                    $debit = floatval($debit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) + floatval($f->grant_user_amount);
                    
                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->grant_user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'DEBIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);
                    
                    
                }
                else if($f->grant_user_name == $user_name && $f->trans_status == 1)
                {
                    // Credit -
                    $credit = floatval($credit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) - floatval($f->grant_user_amount);

                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'CREDIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);

                }
                $j++;
            
            }

            // RECHARGE AMOUNT
            $credit = floatval($credit) + floatval($rech_amount);
            $balance = floatval($balance) - floatval($rech_amount);
            array_push($data, ['date' => '', 'from_user' => '', 'payment_mode' => 'RECHARGE',
                                    'payment_type' => 'CREDIT',
                                    'amount' => number_format($rech_amount,2,'.',''),
                                    'balance' => number_format($balance,2,'.','')]);
            
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];
        }

        return response()->json($result, 200);

    }


    /**
     * Get Apipartner Stock Details
     */
    public function getApipartnerStock(Request $request)
    {
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $debit = 0;
        $credit = 0;
        $balance = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $upay_1->where(function($query) use ($user_name){
                        return $query
                        ->where('user_name', '=', $user_name)
                        ->orWhere('grant_user_name', '=', $user_name);
                    })
                    ->where('trans_status', '=', 1)
                    ->orderBy('id', 'asc')->get();

            $rech_amount = $this->getApipartnerRechargeAmount($user_name);
            
            $j = 0;
            foreach($d1 as $f)
            {
                if($f->user_name == $user_name && $f->trans_status == 1)
                {
                    // Debit +
                    $debit = floatval($debit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) + floatval($f->grant_user_amount);
                    
                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->grant_user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'DEBIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);
                    
                    
                }
                else if($f->grant_user_name == $user_name && $f->trans_status == 1)
                {
                    // Credit -
                    $credit = floatval($credit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) - floatval($f->grant_user_amount);

                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'CREDIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);

                }
                $j++;
            
            }

            // RECHARGE AMOUNT
            $credit = floatval($credit) + floatval($rech_amount);
            $balance = floatval($balance) - floatval($rech_amount);
            array_push($data, ['date' => '', 'from_user' => '', 'payment_mode' => 'RECHARGE',
                                    'payment_type' => 'CREDIT',
                                    'amount' => number_format($rech_amount,2,'.',''),
                                    'balance' => number_format($balance,2,'.','')]);
            
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];
        }

        return response()->json($result, 200);

    }


    /**
     * Get Distributor Stock Details
     */
    public function getDistributorStock(Request $request)
    {
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;
        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $debit = 0;
        $credit = 0;
        $balance = 0;
        $rech_amount = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $upay_1->where(function($query) use ($user_name){
                        return $query
                        ->where('user_name', '=', $user_name)
                        ->orWhere('grant_user_name', '=', $user_name);
                    })
                    ->where('trans_status', '=', 1)
                    ->orderBy('id', 'asc')->get();

            if($user_type == "DISTRIBUTOR") {
                $rech_amount = $this->getDistributorRechargeAmount($user_name);
            }
            else if($user_type == "SUPER DISTRIBUTOR") {
                $rech_amount = $this->getSuperDistributorRechargeAmount($user_name);
            }
            
            $j = 0;
            foreach($d1 as $f)
            {
                if($f->user_name == $user_name && $f->trans_status == 1)
                {
                    // Debit +
                    $debit = floatval($debit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) + floatval($f->grant_user_amount);
                    
                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->grant_user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'DEBIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);
                    
                    
                }
                else if($f->grant_user_name == $user_name && $f->trans_status == 1)
                {
                    // Credit -
                    $credit = floatval($credit) + floatval($f->grant_user_amount);
                    $balance = floatval($balance) - floatval($f->grant_user_amount);

                    array_push($data, ['date' => $f->grant_date, 
                                    'from_user' => $f->user_name,
                                    'payment_mode' => $f->payment_mode,
                                    'payment_type' => 'CREDIT',
                                    'amount' => $f->grant_user_amount,
                                    'balance' => number_format($balance,2,'.','')]);

                }
                $j++;
            
            }

            // RECHARGE AMOUNT
            $credit = floatval($credit) + floatval($rech_amount);
            $balance = floatval($balance) - floatval($rech_amount);
            array_push($data, ['date' => '', 'from_user' => '', 'payment_mode' => 'RECHARGE',
                                    'payment_type' => 'CREDIT',
                                    'amount' => number_format($rech_amount,2,'.',''),
                                    'balance' => number_format($balance,2,'.','')]);
            
            $result = ["status" => 200, 'data' => $data];
        }
        else
        {
            $result = ["status" => 603, 'data' => $data];
        }

        return response()->json($result, 200);

    }

    /**
     * Get User Details         ----DISTRIBUTOR / SUPER DISTRIBUTOR
     */
    public function getUserDetails(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $user_1 = new UserPersonalDetails;
        $user_4 = new UserChainDetails;
        $result = [];
        $data = [];
        $rs = [];
        $user_ubal = 0;

        $auth_token = trim($request->auth_token);

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_code = $this->getCode($user_name);
            $user_ubal = $this->getBalance($user_name);

            
            $d1 = $user_4->select('user_code', 'user_level')->where('parent_code', '=', $user_code)-> orderBy('user_name', 'asc')->get();

            $j = 0;
            $c_bal = 0;
            $statx = "NONE";
            foreach($d1 as $d)
            {
                $d2 = $user_1->with(['account', 'balance'])->where('user_code', '=', $d->user_code)->get();

                $arr = (array)$d2[0]->balance;
                if(empty($arr)) {
                    $c_bal = 0;
                }
                else {
                    $c_bal = $d2[0]->balance->user_balance;
                }

                if($d2[0]->account->user_status == 1) {
                    $statx = "LIVE";
                }
                else {
                    $statx = "INACTIVE";
                }

                array_push($rs, ['user_code' => $d2[0]->user_code, 
                                    'user_name' => $d2[0]->user_name, 
                                    'user_per_name' => $d2[0]->user_per_name,
                                    'user_type' => $d2[0]->account->user_type,
                                    'user_status' => $statx,
                                    'balance' => number_format($c_bal,2,'.','')]);
                $j++;
            }              
            
        }
        
        $result = ["status" => 200, 'balance' => $user_ubal, 'user_details' => $rs];
        return response()->json($result, 200);
    }

    /**
     * Get User Balance
     */
    public function getUserBalance(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $off_1 = new AdminOfferDetails;
        $result = [];
        $data = [];
        $user_ubal = 0;

        $auth_token = trim($request->auth_token);
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);

            $d1 = $off_1->select('trans_id', 'offer_details')->where('offer_status', '=', 1)
                            ->where(function($query) use ($user_type){
                                return $query
                                ->where('user_type', '=', 'ALL')
                                ->orWhere('user_type', '=', $user_type);
                            })->orderBy('id', 'desc')->limit(10)->get();
                            
            foreach($d1 as $d)
            {
                array_push($data, ['trans_id' => $d->trans_id, 'offer_details' => $d->offer_details]);               
            }
        }
        
        $result = ["status" => 200, 'balance' => $user_ubal, 'offer' => $data];
        return response()->json($result, 200);
    }

    /**
     * User Payment Request
     */
    public function getPaymentRequest(Request $request)
    {
        $upay_1 = new UserPaymentDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name_req = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code_req = $this->getCode($user_name);
            $trans_id = "MB".rand(100000,999999);
            
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = $user_2->select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            $payment_mode = "FUND_TRANSFER";

            // Check Already Requested Amount Pending
            $cx = $upay_1->where([['user_name', '=', $user_name_req], ['trans_status', '=', 0]])->count();
            if($cx == 0)
            {
                 // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name_req;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = "";
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 0;
            
                $upay_1->save();

                // Send Sms...
                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_grnt)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".$this->convertNumber($user_amount)." is requested from ".$user_name_req." at ".date('d/m/Y H:i:s');
                    SmsInterface::callSms($user_name_req, $dx2[0]->user_mobile, $msg);
                }
            
                $result = ["status" => 200];
            }
            else
            {
                $result = ["status" => 604];        // Already Payment Pending
            }   
           

            
        }
        else
        {
            $result = ["status" => 603];
        }

        return response()->json($result, 200);

    }

    /**
     * User Complaint Request
     */
    public function getComplaintRequest(Request $request)
    {
        $comp_1 = new UserComplaintDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $rech_mobile = trim($request->rech_mobile);
        $rech_amount = trim($request->rech_amount);
        $user_complaint = trim($request->user_complaint);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
                        
             // Insert Record... 
            $comp_1->trans_id = "MB".rand(10000,99999);
            $comp_1->user_name = $user_name;
            $comp_1->user_type = $user_type;
            $comp_1->rech_mobile = $rech_mobile;
            $comp_1->rech_amount = $rech_amount;
            $comp_1->user_complaint = $user_complaint;
            $comp_1->admin_reply = "";
            $comp_1->reply_status = 1;
            $comp_1->reply_date = $date_time;

            $comp_1->save();

            $result = ["status" => 200];
            
        }
        else
        {
            $result = ["status" => 603];
        }

        return response()->json($result, 200);

    }

    /**
     * User Entry 1
     */

    public function userEntry_1(Request $request)
    {
        $user_1 = new UserPersonalDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        // Post Data
        $auth_token = trim($request->auth_token);
        $user_code = trim($request->user_code);
        $user_per_name = trim($request->user_per_name);
        $user_city = trim($request->user_city);
        $user_state = "";
        $user_phone = "";
        $user_mobile = trim($request->user_mobile);
        $user_mail = trim($request->user_mail);
        $user_kyc = "";
        $user_name_1 = trim($request->user_name);

        $file_name1 = $user_code."_PHOTO.jpg";
        $file_name2 = $user_code."_KYC.jpg";

       
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;

            if($user_type == "SUPER DISTRIBUTOR" || $user_type == "DISTRIBUTOR")
            {
                $cnt1 = $user_1->where('user_code', '=', $user_code)->count();
                if($cnt1 == 0)
                {
                    $cnt2 = $user_1->where('user_mobile', '=', $user_mobile)->count();
                    if($cnt2 == 0)
                    {
                        // Personal Details....
                        $cnt = $user_1->where('user_name', '=', $user_name_1)->count();
                        if($cnt == 0)
                        {
                            $user_1->user_code = $user_code;
                            $user_1->user_name = $user_name_1;
                            $user_1->user_per_name = $user_per_name;
                            $user_1->user_city = $user_city;
                            $user_1->user_state = "";
                            $user_1->user_phone = "";
                            $user_1->user_mobile = $user_mobile;
                            $user_1->user_mail = $user_mail;
                            $user_1->user_kyc = "";
                            $user_1->user_photo = $file_name1;
                            $user_1->user_kyc_proof = $file_name2;
        
                            $user_1->save();

                            $result = ["status" => 200, 'user_code' => $user_code];
                        }
                        else
                        {
                            $result = ["status" => 621, 'user_code' => ''];
                        }
    
                    }
                    else
                    {
                        $result = ["status" => 620, 'user_code' => ''];
                    }
                }
                else
                {
                    
                    // Data is Already Available. so update...
                    $cnt2 = $user_1->where('user_mobile', '=', $user_mobile)->where('user_code', '!=', $user_code)->count();
                    if($cnt2 == 0)
                    {
                        $a = $user_1->where('user_code', '=', $user_code)
                                        ->update(['user_per_name' => $user_per_name, 
                                                'user_city' => $user_city,
                                                'user_mobile' => $user_mobile,
                                                'user_mail' => $user_mail ]);
                        
                        $result = ["status" => 200, 'user_code' => $user_code];
                    }
                    else
                    {
                        $result = ["status" => 620, 'user_code' => ''];
                    }
                    
                }
            }
            else
            {
                $result = ["status" => 622, 'user_code' => ''];
            }
           
        }

        return response()->json($result, 200);

    }

    /**
     * User Entry 2
     */

    public function userEntry_2(Request $request)
    {
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_3 = new UserNetworkDetails;
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_type = "";
        $parent_code = "";
        $user_ubal = 0;
        $data = [];
        $result = [];

        // Post Data
        $auth_token = trim($request->auth_token);
        $user_code = trim($request->user_code);
        $user_name_1 = trim($request->user_name);
        $user_pwd = trim($request->user_pwd);
        $user_acc_type = trim($request->user_acc_type);
        $user_setup_fee = trim($request->user_setup_fee);
        $user_rec_mode = trim($request->user_rec_mode);
        $user_api_url_1 = trim($request->user_api_url_1);
        $user_api_url_2 = trim($request->user_api_url_2);
       
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $parent_code = $this->getCode($user_name);

            if($user_type == "SUPER DISTRIBUTOR" || $user_type == "DISTRIBUTOR")
            {
                $cnt1 = $user_1->where('user_code', '=', $user_code)->count();
                if($cnt1 > 0)
                {
                    $cnt2 = $user_2->where('user_code', '=', $user_code)->count();
                    if($cnt2 == 0)
                    {
                        // Account Details.....
                        $user_2->user_code = $user_code;
                        $user_2->user_name = $user_name_1;
                        $user_2->user_pwd = $user_pwd;
                        $user_2->user_type = $user_acc_type;
                        $user_2->user_setup_fee = $user_setup_fee;
                        $user_2->parent_type = $user_type;
                        $user_2->parent_code = $parent_code;
                        $user_2->parent_name = $user_name;
                        $user_2->user_rec_mode = $user_rec_mode;
                        $user_2->user_api_url_1 = $user_api_url_1;
                        $user_2->user_api_url_2 = $user_api_url_2;
                        $user_2->user_status = 4;

                        $user_2->save();

                        // User Chain Details
                        $d1 = $user_4->select('user_level', 'user_chain')->where('user_code', '=', $parent_code)->get();
                        if($d1->count() > 0)
                        {
                            $user_level = intval($d1[0]->user_level) + 1;
                            $user_chain = $d1[0]->user_chain."-".$user_code;
                        }
                        else
                        {
                            $user_level = 1;
                            $user_chain = "1-".$user_code;
                        }

                        $user_4->user_code = $user_code;
                        $user_4->user_name = $user_name_1;
                        $user_4->parent_code = $parent_code;
                        $user_4->parent_name = $user_name;
                        $user_4->user_level = $user_level;
                        $user_4->user_chain = $user_chain;
                        
                        $user_4->save();
                        
                        // Account Balance Details.....
                        $user_5->user_code = $user_code;
                        $user_5->user_name = $user_name_1;
                        $user_5->user_balance = "0.00";
                        
                        $user_5->save();
                        $result = ["status" => 200];
                        
                    }
                    else
                    {
                        $b = $user_2->where('user_code', '=', $user_code)
                                        ->update(['user_rec_mode' => $user_rec_mode,
                                                    'user_type' => $user_acc_type,
                                                    'user_setup_fee' => $user_setup_fee,
                                                    'user_api_url_1' => $user_api_url_1,
                                                    'user_api_url_2' => $user_api_url_2
                                                    ]);
                        $result = ["status" => 200];
                    }
                }
                else
                {
                    $result = ["status" => 624];
                }
                
            }
            else
            {
                $result = ["status" => 622];
            }
           
        }

        return response()->json($result, 200);
    }


    /**
     * User Entry 3 - User Network Entry
     */

    public function userEntry_3(Request $request)
    {
        $user_1 = new UserPersonalDetails;
        $user_3 = new UserNetworkDetails;
        $user_5 = new UserAccountBalanceDetails;
        $mob_1 = new MobileUserDetails;

        $z = 0;
        $user_name = "";
        $user_name_1 = "";
        $user_ubal = 0;
        $result = [];

        // Post Data
        $auth_token = trim($request->auth_token);
        $user_code = trim($request->user_code);
        $user_network = trim($request->user_network);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            
            $d2 = $user_1->select('user_name')->where('user_code', '=', $user_code)->get();
            if($d2->count() > 0)
            {
                $user_name_1 = $d2[0]->user_name;
            }

            $aa = explode("*", $user_network);

            $i = 0;
            foreach($aa as $a)
            {
                $str1 = $a;
                $bb = explode("@", $str1);

                if(sizeof($bb) > 2)
                {
                    $nt = explode("-", $bb[0]);

                    $net_code = $nt[0];
                    $net_pers = $bb[1];
                    $net_surp = $bb[2];


                    // Validation
                    if($net_pers == "") {
                        $net_pers = 0;
                    }

                    if($net_surp == "") {
                        $net_surp = 0;
                    }

                    $cnt = $user_3->where([['user_code', '=', $user_code], ['net_code', '=', $net_code]])->count();
                    if($cnt > 0)
                    {
                        $az = $user_3->where([['user_code', '=', $user_code], ['net_code', '=', $net_code]])
                                ->update(['user_net_per' => $net_pers, 'user_net_surp' => $net_surp ]);
                        $i++;
                    }
                    else
                    {
                        $arr1 = array('user_code' => $user_code, 'user_name' => $user_name_1, 'net_code' => $net_code,
                                'user_net_per' => $net_pers, 'user_net_surp' => $net_surp, 
                                'created_at' => $date_time, 'updated_at' => $date_time);
                        
                        $user_3->insert($arr1);
                        
                        $i++;
                    }
                    
                }
            
            }

            if($i > 0)
                $result = ["status" => 200];
            else
                $result = ["status" => 623];
        }
        else
        {
            $result = ["status" => 624];
        }

        return response()->json($result, 200);


    }

    /**
     * User Profile Details
     */
    public function getProfileDetails(Request $request)
    {
        $user_1 = new UserPersonalDetails;
        $mob_1 = new MobileUserDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
                        
            $d1 = $user_1->with(['account'])->where('user_name', '=', $user_name)->get();
            
            foreach($d1 as $d)
            {
                array_push($data, ['user_code' => $d->user_code, 
                                    'user_per_name' => $d->user_per_name,
                                    'user_city' => $d->user_city,
                                    'user_mobile' => $d->user_mobile,
                                    'user_setup_fee' => $d->account->user_setup_fee,
                                    'user_type' => $d->account->user_type,
                                    'parent_name' => $d->account->parent_name,
                                    'user_rec_mode' => $d->account->user_rec_mode,
                                    'user_api_url_1' => $d->account->user_api_url_1,
                                    'user_api_url_2' => $d->account->user_api_url_2 ]);
                                               
            }
        }
        
        $result = ["status" => 200, 'data' => $data];
        
        return response()->json($result, 200);

    }

    /**
     * User Payment Accept Details
     */
    public function getPaymentAcceptDetails(Request $request)
    {
        $upay_1 = new UserPaymentDetails;
        $mob_1 = new MobileUserDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
                        
            $d1 = $upay_1->select('trans_id','user_name', 'user_amount', 'user_req_date', 'payment_mode', 'user_remarks')
            ->where([['grant_user_name', '=', $user_name], ['trans_status', '=', 0]])->orderBy('id', 'desc')->get();
            
            foreach($d1 as $d)
            {
                array_push($data, ['trans_id' => $d->trans_id, 
                                    'user_name' => $d->user_name,
                                    'user_amount' => $d->user_amount,
                                    'user_req_date' => $d->user_req_date,
                                    'payment_mode' => $d->payment_mode,
                                    'user_remarks' => $d->user_remarks ]);
                                               
            }
        }
        
        $result = ["status" => 200, 'payment_details' => $data];
        
        return response()->json($result, 200);

    }

    /**
     * Store User Payment Accept
     */
    public function storePaymentAccept(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        $mob_1 = new MobileUserDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);
        $trans_amt = trim($request->trans_amt);
        $date_time = date("Y-m-d H:i:s");
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name_grnt = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
                        
            $d1 = $upay_1->select('user_name')->where([['trans_id', '=', $trans_id], ['trans_status', '=', 0]])->get();
            if($d1->count() > 0)
            {
                $user_name = $d1[0]->user_name;

                // Insert Data
                $cnt = $user_1->where('user_name', '=', $user_name)->count();
                if($cnt > 0)
                {
                    // Check Admin balance Amount
                    $u_bal_1 = "";
                    $u_bal_2 = "";
                    $flg_1 = 0;
                    $net_bal_1 = 0;
                    $net_bal_2 = 0;

                    $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name_grnt)->get();
                    if($d2->count() > 0)
                    {
                        $u_bal_1 = $d2[0]->user_balance;
                    }

                    $setup_fee = GetCommon::getSetupFee($user_name_grnt);
                    if(floatval($setup_fee) >= floatval($u_bal_1))
                    {
                        $flg_1 = 3;      //User Balance is less than setup Fee...
                    }

                    if($u_bal_1 != "" && $flg_1 == 0)
                    {
                        if(floatval($u_bal_1) < floatval($trans_amt))
                        {
                            $flg_1 = 1;     // Granted user has less Amount...
                        }
                        else
                        {
                            // Deduct Amount from user name...
                            $net_bal_1 = floatval($u_bal_1) - floatval($trans_amt);

                            $net_bal_1 = $this->convertNumber($net_bal_1);
                                                    
                            $uacc_1->where('user_name', '=', $user_name_grnt)
                                    ->update(['user_balance'=>$net_bal_1, 'updated_at'=>$date_time]);
                        }
                    }
                    else
                    {
                        $flg_1 = 2;         // Granted User has No Amount...(No Entry..)
                    }
                    

                    if($flg_1 == 0)
                    {
                        // Add amount to user balance Account
                        $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                        foreach($d2 as $d)
                        {
                            $u_bal_2 = $d->user_balance;
                        }

                        if($u_bal_2 != "")
                        {
                            // Add Amount to user name...
                            $net_bal_2 = floatval($u_bal_2) + floatval($trans_amt);

                            $net_bal_2 = $this->convertNumber($net_bal_2);
                            
                            $uacc_1->where('user_name', '=', $user_name)
                                        ->update(['user_balance'=>$net_bal_2, 'updated_at'=>$date_time]);
                        }
                        else
                        {
                            $net_bal_2 = floatval($trans_amt);         // Granted User has No Amount (No Entry...Insert It..)

                            $net_bal_2 = $this->convertNumber($net_bal_2);
                            
                            $uacc_1->user_code = $user_code;
                            $uacc_1->user_name = $user_name;
                            $uacc_1->user_balance = $net_bal_2;
                            $uacc_1->save();
                        }

                        $payment_mode = "FUND_TRANSFER";

                        // Update Amount Transfer Details....
                        $upay_1->where('trans_id', '=', $trans_id)
                                        ->update(['grant_user_name'=>$user_name_grnt, 
                                                'grant_user_amount' => $this->convertNumber($trans_amt),
                                                'grant_date' => $date_time,
                                                'trans_status' => 1,
                                                'updated_at'=>$date_time]);
                                                
                        
                         // Payment Ledger
                        $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name, 'pay_type' => "C",
                        'pay_amount' => $this->convertNumber($trans_amt), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                        'from_user' => $user_name_grnt, 'pay_date' => $date_time, 
                        'agent_name' => "-", 'pay_remarks' => $payment_mode, 'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
        
                        $upay_2->insert($ar1);
                        


                        // Transaction Details.....
                        $tran_1->trans_id = $trans_id;
                        $tran_1->user_name = $user_name;
                        $tran_1->trans_type = $payment_mode;
                        $tran_1->trans_option = '1';
                        
                        $tran_1->save();

                        $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
                        if($dx2->count() > 0)
                        {
                            $msg = "Amount ".$this->convertNumber($trans_amt)." is transferred successfully at ".date('d/m/Y H:i:s');
                            SmsInterface::callSms($user_name, $dx2[0]->user_mobile, $msg);
                        }

                        $result = ["status" => 200, 'output' => 'Amount is transferred successfully...'];

                        
                    }
                    else if($flg_1 == 1)
                    {
                        $result = ["status" => 630, 'output' => 'Error! Your Account Balance is Low...'];
                    }
                    else if($flg_1 == 2)
                    {
                        $result = ["status" => 631, 'output' => 'Error! Your have no Account Balance...'];
                    } 
                    else if($flg_1 == 3)
                    {
                        $result = ["status" => 631, 'output' => 'Error! Your have Low Account Balance in SetupFee...'];
                    }   
                }
                else
                {
                    $result = ["status" => 632, 'output' => 'Error! User Name does not Exists...'];
                }
            }
            else
            {
                $result = ["status" => 633, 'output' => 'Error! This transaction is already finished...'];
            }

        }
        
        
        
        return response()->json($result, 200);

    }

    /**
     * User Payment Delete
     */
    public function storePaymentDelete(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        $mob_1 = new MobileUserDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);
        $date_time = date("Y-m-d H:i:s");
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name_grnt = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
                        
            $d1 = $upay_1->select('user_name')->where([['trans_id', '=', $trans_id], ['trans_status', '=', 0]])->get();
            if($d1->count() > 0)
            {
                $user_name = $d1[0]->user_name;

                // Insert Data
                $cnt = $user_1->where('user_name', '=', $user_name)->count();
                if($cnt > 0)
                {
                    // Check Admin balance Amount
                    $u_bal_1 = "";
                    $u_bal_2 = "";
                    $flg_1 = 0;
                    $net_bal_1 = 0;
                    $net_bal_2 = 0;
                    $trans_amt = 0;
                
                    $payment_mode = "FUND_TRANSFER";

                    // Update Amount Transfer Details....
                    $upay_1->where('trans_id', '=', $trans_id)
                                    ->update(['grant_user_name'=>$user_name_grnt, 
                                            'grant_user_amount' => $this->convertNumber($trans_amt),
                                            'grant_date' => $date_time,
                                            'trans_status' => 2,
                                            'updated_at'=>$date_time]);
                    


                    // Transaction Details.....
                    $tran_1->trans_id = $trans_id;
                    $tran_1->user_name = $user_name;
                    $tran_1->trans_type = $payment_mode;
                    $tran_1->trans_option = "1";
                    
                    $tran_1->save();

                    $result = ["status" => 200, 'output' => 'Transaction is cancelled successfully...'];
                }
                else
                {
                    $result = ["status" => 634, 'output' => 'Error! User Name does not Exists...'];
                    
                }
            }
            else
            {
                $result = ["status" => 635, 'output' => 'Error! This transaction is already finished...'];
                
            }
        }
        
        return response()->json($result, 200);

    }


    /**
     * User Remote Payment store
     */
    public function storeRemotePayment(Request $request)
    {
        $mob_1 = new MobileUserDetails;
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        $user_4 = new UserChainDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $trans_id = rand(100000,999999);
        $auth_token = trim($request->auth_token);
        $user_code = trim($request->user_code);
        $user_name = trim($request->user_name);
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name_grnt = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_code_grnt = $this->getCode($user_name_grnt);
            $u_bal_1 = $this->getBalance($user_name_grnt);

            // --Code starts
            // Insert Data
            $cnt = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt > 0)
            {
                // Check Admin balance Amount
                $u_bal_2 = "";
                $flg_1 = 0;
                $net_bal_1 = 0;
                $net_bal_2 = 0;

                $setup_fee = GetCommon::getSetupFee($user_name_grnt);
                if(floatval($setup_fee) >= floatval($u_bal_1))
                {
                    $flg_1 = 3;      //User Balance is less than setup Fee...
                }

                if($user_name != "admin")           // To User is not Admin
                {
                    if($u_bal_1 != "" && $flg_1 == 0)
                    {
                        if(floatval($u_bal_1) < floatval($user_amount))
                        {
                            $flg_1 = 1;     // Granted user has less Amount...
                        }
                        else
                        {
                            // Deduct Amount from user name...
                            $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);

                            $net_bal_1 = $this->convertNumber($net_bal_1);
                                                    
                            $uacc_1->where('user_name', '=', $user_name_grnt)
                                ->update(['user_balance'=>$net_bal_1, 'updated_at'=>$date_time]);
                        }
                    }
                    else
                    {
                        $flg_1 = 2;         // Granted User has No Amount...(No Entry..)
                    }
                }
                

                if($flg_1 == 0)
                {
                    // Add amount to user balance Account
                    $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    foreach($d2 as $d)
                    {
                        $u_bal_2 = $d->user_balance;
                    }

                    if($u_bal_2 != "")
                    {
                        // Add Amount to user name...
                        $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);

                        $net_bal_2 = $this->convertNumber($net_bal_2);
                        
                        $uacc_1->where('user_name', '=', $user_name)
                                    ->update(['user_balance'=>$net_bal_2, 'updated_at'=>$date_time]);
                    }
                    else
                    {
                        $net_bal_2 = floatval($user_amount);         // Granted User has No Amount (No Entry...Insert It..)

                        $net_bal_2 = round($net_bal_2, 2);
                        $net_bal_2 = number_format($net_bal_2, 2, '.', '');
                        
                        $uacc_1->user_code = $user_code;
                        $uacc_1->user_name = $user_name;
                        $uacc_1->user_balance = $net_bal_2;
                        $uacc_1->save();
                    }

                    $payment_mode = "REMOTE_PAYMENT";
                    

                    // Amount Transfer Details....
                    $upay_1->trans_id = $trans_id;
                    $upay_1->user_name = $user_name;
                    $upay_1->user_amount = $this->convertNumber($user_amount);
                    $upay_1->user_req_date = $date_time;
                    $upay_1->grant_user_name = $user_name_grnt;
                    $upay_1->grant_user_amount = $this->convertNumber($user_amount);
                    $upay_1->grant_date = $date_time;
                    $upay_1->payment_mode = $payment_mode;
                    $upay_1->user_remarks = $user_remarks;
                    $upay_1->trans_status = 1;
                
                    $upay_1->save();
                    
                    // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name, 'pay_type' => "C",
                    'pay_amount' => $this->convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name_grnt, 'pay_date' => $date_time, 
                    'agent_name' => "-", 'pay_remarks' => $payment_mode, 'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
    
                    $upay_2->insert($ar1);


                    // Transaction Details.....
                    $tran_1->trans_id = $trans_id;
                    $tran_1->user_name = $user_name;
                    $tran_1->trans_type = $payment_mode;
                    $tran_1->trans_option = '1';
                    
                    $tran_1->save();

                    $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
                    if($dx2->count() > 0)
                    {
                        $msg = "Amount ".$this->convertNumber($user_amount)." is transferred successfully at ".date('d/m/Y H:i:s');
                        SmsInterface::callSms($user_name, $dx2[0]->user_mobile, $msg);
                    }

                    $result = ["status" => 200, 'output' => 'Amount is transferred successfully...'];
                }
                else if($flg_1 == 1)
                {
                    $result = ["status" => 640, 'output' => 'Error! Your Account Balance is Low...'];
                }
                else if($flg_1 == 2)
                {
                    $result = ["status" => 641, 'output' => 'Error! Your have no Account Balance...'];
                } 
                else if($flg_1 == 3)
                {
                    $result = ["status" => 641, 'output' => 'Error! Your have Low Account Balance in SetupFee......'];
                }   
            }
            else
            {
                $result = ["status" => 642, 'output' => 'Error! Your have no Account Balance...'];
            }

            // --Code ends
                        
            
        }
        
        
        return response()->json($result, 200);

    }

    /**
     * User Change Pin
     */
    public function getChangePin(Request $request)
    {
        $user_2 = new UserAccountDetails;
        $mob_1 = new MobileUserDetails;

        $user_name = "";
        $user_type = "";
        $data = [];
        $result = [];

        $auth_token = trim($request->auth_token);
        $user_pwd = trim($request->user_pwd);
        
        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_code = $this->getCode($user_name);

            $a = $user_2->where('user_code', '=', $user_code)->update(['user_pwd' => $user_pwd]);

            if($a > 0)
            {
                $result = ["status" => 200, 'output' => 'Pin has been changed successfully..'];
            }
            else
            {
                $result = ["status" => 650, 'output' => 'Error! Pin is not changed...'];
            }
                
        }
        else
        {
            $result = ["status" => 651, 'output' => 'Error! Invalid User...'];
        }
        
        
        return response()->json($result, 200);

    }

    
    

    // Prepaid recharge
    public function mobile_prepaid_recharge(Request $request)
	{
        $bon_1 = new BonrixApiResultDetails;
        $mob_1 = new MobileUserDetails;
        $rech_r_2 = new UserRechargeRequestDetails;

        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);
        $user_amount = trim($request->user_amount);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            $z = RechargeNewMobile::add($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
        }
        

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);
    }


    public function mobile_prepaid_recharge_amount(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $u_bal = 0;
        $resulx = [];
        $details = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);
        $user_amount = trim($request->user_amount);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $u_bal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {
            list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $user_amount, "WEB");

            $peramt_1 = round($peramt, 2);

            $netamt_1 = round($netamt, 2);
            
            $details = ['per' => $per, 'per_amount' => $peramt_1, 'surplus' => $sur, 'net_amount' => $netamt_1];

            $m_z = 200;
            
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
        }
        

        $resulx = ["status" => $m_z, 'balance' => $u_bal, 'percentage_details' => $details];

        return response()->json($resulx, 200);
    }


    // EBBILL recharge
    public function mobile_ebbil_recharge(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $cons_no = trim($request->cons_no);
        $cons_name = trim($request->cons_name);
        $cons_mobile = trim($request->cons_mobile);
        $cons_duedate = trim($request->cons_duedate);
        $cons_amount = trim($request->cons_amount);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {

            // Post Data
            $data = [];
            $data['net_code'] = $net_code;
            $data['cons_no'] = $cons_no;
            $data['cons_name'] = $cons_name;
            $data['cons_mobile'] = $cons_mobile;
            $data['cons_amount'] = $cons_amount;
            $data['cons_duedate'] = $cons_duedate;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";


            $z = RechargeBill::add($data);

            if($z == 0)
            {
                $op = "Bill Payment Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Web Mode is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! This Account is locked...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! This Account is below from Setup Fee...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Recharge is already Pending for this Consumer No...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Sorry! Server is Temporarily shut down...";
                $m_z = 610;
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 611;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 612;
        }
        

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);
    }

    // Mobile Remitter Entry
    public function  mobile_remitter_entry(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);
        $rem_mobile = trim($request->rem_mobile);
        $rem_name = trim($request->rem_name);
        $rem_city = trim($request->rem_city);
        $rem_pincode = trim($request->rem_pincode);
        $date_time = date("Y-m-d H:i:s");

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            $data = ['user_name' => $user_name,
                'rem_name' => trim($rem_name),
                'rem_city' => trim($rem_city),
                'rem_pincode' => trim($rem_pincode),
                'rem_mobile' => trim($rem_mobile)
                ];

            $z1 = BankRemitter::add($data);

            if($z1 == 1)
            {
                $op = "Entry is added Successfully...";
                $m_z = 200;
            }   
            else if($z1 == 2) {
                $op = "Mobile No for this Remitter is Already Added...";
                $m_z = 601;
            } 
            else {
                $op = "No Entry is Added...";
                $m_z = 602;
            }
                
        }
        else if($z == 1)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 603;
        }
        else if($z == 2)
        {
            $op = "Error! You are not retailer";
            $m_z = 604;
        }

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);

    }

    // Mobile Remitter Details
    public function  mobile_remitter_details(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            $d1 = BankRemitter::view_user($user_name);

            foreach($d1 as $d)
            {
                array_push($data, [ 'rem_id' => $d->rem_id,
                                    'rem_name' => $d->rem_name, 
                                    'rem_city' => $d->rem_city,
                                    'rem_pincode' => $d->rem_pincode,
                                    'rem_mobile' => $d->rem_mobile]);
            }

            $m_z = 200;
                
        }
        else if($z == 1)
        {
           $m_z = 603;
        }
        else if($z == 2)
        {
            $m_z = 604;
        }

        $resulx = ["status" => $m_z, 'output' => $data];

        return response()->json($resulx, 200);

    }

    // Mobile Beneficiary Entry
    public function  mobile_ben_entry(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);
        $rem_id = trim($request->rem_id);
        $bk_acc_no = trim($request->bk_acc_no);
        $bk_acc_name = trim($request->bk_acc_name);
        $bk_code = trim($request->bk_code);
        $bk_ifsc = trim($request->bk_ifsc);
        $bk_acc_mobile = trim($request->bk_acc_mobile);
        $bk_acc_mobile = trim($request->bk_acc_mobile);
        $date_time = date("Y-m-d H:i:s");

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            $data = ['user_name' => $user_name,
                'rem_id' => trim($rem_id),
                'bk_acc_no' => trim($bk_acc_no),
                'bk_acc_name' => trim($bk_acc_name),
                'bk_code' => trim($bk_code),
                'bk_ifsc' => trim($bk_ifsc),
                'bk_acc_mobile' => trim($bk_acc_mobile),
                'bk_acc_branch' => trim($bk_acc_branch)
                ];

            $z1 = Beneficiary::add($data);

            if($z1 == 1)
            {
                $op = "Entry is added Successfully...";
                $m_z = 200;
            }   
            else if($z1 == 2) {
                $op = "Mobile No for this Remitter is Already Added...";
                $m_z = 601;
            } 
            else {
                $op = "No Entry is Added...";
                $m_z = 602;
            }
                
        }
        else if($z == 1)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 603;
        }
        else if($z == 2)
        {
            $op = "Error! You are not retailer";
            $m_z = 604;
        }

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);

    }

    // Mobile beneficiary Details
    public function  mobile_ben_details(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            $d1 = Beneficiary::view_user($user_name);

            foreach($d1 as $d)
            {
                array_push($data, [ 'rem_id' => $d->rem_id,
                                    'bk_acc_no' => $d->bk_acc_no,
                                    'bk_acc_name' => $d->bk_acc_name,
                                    'bk_code' => $d->bk_code,
                                    'bk_ifsc' => $d->bk_ifsc,
                                    'bk_acc_mobile' => $d->bk_acc_mobile,
                                    'bk_acc_branch' => $d->bk_acc_branch]);
            }

            $m_z = 200;
                
        }
        else if($z == 1)
        {
           $m_z = 603;
        }
        else if($z == 2)
        {
            $m_z = 604;
        }

        $resulx = ["status" => $m_z, 'output' => $data];

        return response()->json($resulx, 200);

    }

    // Mobile Bank Transfer Entry
    public function  mobile_bank_transfer(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $rem_id = trim($request->rem_id);
        $bn_id = trim($request->bn_id);
        $bk_acc_no = trim($request->bk_acc_no);
        $bk_code = trim($request->bk_code);
        $mt_amount = trim($request->mt_amount);
        $date_time = date("Y-m-d H:i:s");

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            $data = [];
            $data['net_code'] = $net_code;
            $data['bn_id'] = $bn_id;
            $data['rem_id'] = $rem_id;
            $data['bk_acc_no'] = $bk_acc_no;
            $data['bk_code'] = $bk_code;
            $data['mt_amount'] = $mt_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            $z1 = BankTransfer::add($data);

            if($z1 == 1)
            {
                $op = "Money Transfer Process is Completed Successfully..";
                $m_z = 200;
            }   
            else if($z1 == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 600;
            }
            else if($z1 == 2)
            {  
                $op = "Error! No Web Mode is Selected...";
                $m_z = 601;
            }
            else if($z1 == 3)
            {   
                $op = "Error! This Account is locked...";
                $m_z = 602;
            }
            else if($z1 == 4)
            {   
                $op = "Error! This Account is below from Setup Fee...";
                $m_z = 603;
            }
            else if($z1 == 5)
            {
                $op = "Error! Transfer is already Pending for this Benificiary Id...";
                $m_z = 604;
            }
            else if($z1 == 6)
            {
                $op = "Sorry! Server is Temporarily shut down...";
                $m_z = 605;
            }
                
        }
        else if($z == 1)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 606;
        }
        else if($z == 2)
        {
            $op = "Error! You are not retailer";
            $m_z = 607;
        }

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);

    }

    // Mobile bank Details
    public function  mobile_bank_details(Request $request)
    {
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $data = [];
         $resulx = [];
         $m_z = 0;

         // Post Data
        $auth_token = trim($request->auth_token);

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            array_push($data, [ 'bk_code' => '1', 'bk_name' => 'AXIS BANK']);
            array_push($data, [ 'bk_code' => '2', 'bk_name' => 'BANK OF BARODA']);
            array_push($data, [ 'bk_code' => '3', 'bk_name' => 'CANARA BANK']);
            array_push($data, [ 'bk_code' => '4', 'bk_name' => 'CORPORATION BANK']);
            array_push($data, [ 'bk_code' => '5', 'bk_name' => 'CITY UNION BANK']);
            array_push($data, [ 'bk_code' => '6', 'bk_name' => 'HDFC BANK']);
            array_push($data, [ 'bk_code' => '7', 'bk_name' => 'IDBI BANK LTD']);
            array_push($data, [ 'bk_code' => '8', 'bk_name' => 'INDIAN BANK']);
            array_push($data, [ 'bk_code' => '9', 'bk_name' => 'INDIAN OVERSEAS BANK']);
            array_push($data, [ 'bk_code' => '10', 'bk_name' => 'ICICI BANK']);
            array_push($data, [ 'bk_code' => '11', 'bk_name' => 'KARUR VYSYA BANK']);
            array_push($data, [ 'bk_code' => '12', 'bk_name' => 'STATE BANK OF INDIA']);
            array_push($data, [ 'bk_code' => '13', 'bk_name' => 'SYNDICATE BANK']);
            array_push($data, [ 'bk_code' => '14', 'bk_name' => 'TAMILNADU MERCANTILE BANK']);
            array_push($data, [ 'bk_code' => '15', 'bk_name' => 'VIJAYA BANK']);
            
            $m_z = 200;
                
        }
        else if($z == 1)
        {
           $m_z = 603;
        }
        else if($z == 2)
        {
            $m_z = 604;
        }

        $resulx = ["status" => $m_z, 'output' => $data];

        return response()->json($resulx, 200);

    }

    // Mobile 121 Offer Details
    public function mobile_121_offer(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        $ri = new RechargeInfoDetails;
        
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;
        $user_name = "";

        // Post Data
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);
        $date_time = date("Y-m-d H:i:s");

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
            if($api_code == 0)
            {
                $m_z = 201;
                $op = "No plans Available in this Network...";
            }
            else
            {
                $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $user_mobile);

                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                $res_code = $res->getStatusCode();
                $op = json_decode($res->getBody());
                
                $ri->trans_id = "M".rand(10000,99999);
                $ri->user_name = $user_name;
                $ri->rech_type = "121_OFFER";
                $ri->rech_mobile = $user_mobile;
                $ri->save();

                $m_z = 200;

            }
            
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 611;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 612;
        }
        

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);
    }


     // Mobile 121 Offer Details
     public function mobile_dth_info(Request $request)
     {
         $mob_1 = new MobileUserDetails;
         $ri = new RechargeInfoDetails;
         
         // Declarations
         $op = "";
         $z = 0;
         $z1 = 0;
         $per = 0;
         $peramt = 0;
         $sur = 0;
         $resx = new stdClass();
         $resulx = [];
         $m_z = 0;
         $user_name = "";
 
         // Post Data
         $auth_token = trim($request->auth_token);
         $net_code = trim($request->net_code);
         $user_mobile = trim($request->user_mobile);
         $date_time = date("Y-m-d H:i:s");
 
         $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
 
         if($dc1->count() > 0)
         {
             $user_name = $dc1[0]->user_name;
             $user_type1 = $dc1[0]->user_type;
             $user_ubal = $this->getBalance($user_name);
             $user_code = $this->getCode($user_name);
 
             if($user_type1 != "RETAILER")
                 $z1 = 1;
             
         }
         else
         {
             $z1 = 2;
         }
 
         if($z1 == 0)
         {
 
             list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "DTH_INFO");
             if($api_code == 0)
             {
                 $m_z = 201;
                 $op = "No plans Available in this Network...";
             }
             else
             {
                 $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $user_mobile);
 
                 $client = new \GuzzleHttp\Client();
                 $res = $client->get($api_url);
                 $res_code = $res->getStatusCode();
                 $op = json_decode($res->getBody());
                 
                $ri->trans_id = "M".rand(10000,99999);
                $ri->user_name = $user_name;
                $ri->rech_type = "DTH_INFO";
                $ri->rech_mobile = $user_mobile;
                $ri->save();
 
                 $m_z = 200;
                 
             }
             
         }
         else if($z1 == 1)
         {
             $op = "Error! You are not retailer";
             $m_z = 611;
         }
         else if($z1 == 2)
         {
             $op = "Error! Invalid API Token...";
             $m_z = 612;
         }
         
 
         $resulx = ["status" => $m_z, 'output' => $op];
 
         return response()->json($resulx, 200);
     }


    // Mobile EB Offer Details
    public function mobile_eb_info(Request $request)
    {
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $resulx = [];
        $m_z = 0;

        
        // Post Data
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $mob = trim($request->eb_no);

        list($user_name, $user_type, $user_ubal, $user_code) = $this->getMobileUser($auth_token);

        if($user_name == "NONE") {
            $z = 1;
        }
        else if($user_type != "RETAILER") {
            $z = 2;
        }

        if($z == 0)
        {
            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "BILL_CHECK");
            if($api_code != 0)
            {
                $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                $res_code = $res->getStatusCode();
                $op = json_decode($res->getBody());

                $m_z = 200;
            }
            else
            {
                $m_z = 605;     //No Network Line is Selected...
                $op = "No Details Available in this No...";
            }
            
            
                
        }
        else if($z == 1)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 603;
        }
        else if($z == 2)
        {
            $m_z = 604;
            $op = "Error! You Are not Retailer...";
        }

        $resulx = ["status" => $m_z, 'output' => $op];

        return response()->json($resulx, 200);
    }
   

  
    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    public function getCode($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $ux_code = 0;

        $d1 = $uacc_1->select('user_code')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_code = $d->user_code;
        }

        return $ux_code;
    }

    public function getRetailerRechargeAmount($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $ce_tot = 0;
        

       /* $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['webrecharge1', 'webrecharge2'])
                ->where('user_name', '=', $user_name)
                ->orderBy('id', 'asc')->get(); 
        
        
        foreach($d1 as $d)
        {
            
            if($d->trans_type == "WEB_RECHARGE")
            {
                if($d->trans_option == 1 && $d->webrecharge2->rech_status == "SUCCESS")
                {
                    $ce_tot = floatval($ce_tot) + floatval($d->webrecharge1[0]->rech_total);
                }
               
            }
                                            
        }*/

        return $ce_tot;
        
    }

    public function getApipartnerRechargeAmount($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $ce_tot = 0;
        

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['apirecharge1', 'apirecharge2'])
                ->where('user_name', '=', $user_name)
                ->orderBy('id', 'asc')->get(); 
        
        
        foreach($d1 as $d)
        {
            
            if($d->trans_type == "API_RECHARGE")
            {
                if($d->trans_option == 1 && $d->apirecharge2->rech_status == "SUCCESS")
                {
                    $ce_tot = floatval($ce_tot) + floatval($d->apirecharge1[0]->rech_total);
                }
               
            }
                                            
        }

        return $ce_tot;
        
    }

    public function getDistributorRechargeAmount($user_name)
    {
        $rech_2 = new UserRechargeNewParentDetails;
       
        
        $re_tot = 0;
       
 
        $re_tot = $rech_2->whereRaw('upper(parent_name) = ?',[$user_name])
                            ->where('child_name', '=', 'NONE')
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 
        
       
        
        $re_tot = round($re_tot, 2);
         
        return $re_tot;

        
        
    }

    public function getSuperDistributorRechargeAmount($user_name)
    {
       $rech_2 = new UserRechargeNewParentDetails;
       
        
        $re_tot = 0;
       
 
        $re_tot = $rech_2->whereRaw('upper(parent_name) = ?',[$user_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 
        
       
        
        $re_tot = round($re_tot, 2);
         
        return $re_tot;
        
    }

    public function getSetupFee($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_setup_fee')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_setup_fee;

            if($user_ubal == "")
            {
                $user_ubal = 0;
            }
        }

        return $user_ubal;
    }

    public function getMobileUser($auth_token)
    {
        $mob_1 = new MobileUserDetails;

        $user_name = "NONE";
        $user_type = "NONE";
        $user_ubal = 0;
        $user_code = 0;

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);
        }

        return array($user_name, $user_type, $user_ubal, $user_code);

    }

    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

}
