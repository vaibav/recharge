<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;

use App\Models\UserAccountDetails;
use App\Models\AdminOfferDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankRemitterDetails;

use App\Libraries\GetCommon;
use App\Libraries\RechargeMobile;
use App\Libraries\RechargeNewMobile;
use App\Libraries\ApiUrl;
use App\Libraries\RechargeReportRetailer;

class UserRechargeController extends Controller
{
    private $bon_url;
    //
   
    public function index_new_final(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;
        

        $ob = GetCommon::getUserDetails($request);

        //$rs = [];
        $rs = RechargeReportRetailer::getRechargeDetailsRetailer_new($ob->user);

        //print_r($rs);

        $d1 = $net_1->select('net_type_code','net_type_name')->where('net_type_status', '=', '1')->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $d4 = [];
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', 'RETAILER');
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('retailer.recharge_new', ['networktype' => $d1, 'network' => $d2, 'recharge' => $rs, 'user' => $ob, 'offer' => $off, 'in_active' => $d3, 'ben' => $d4]);
        		
    }

    public function index_eb_final(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;

        $ob = GetCommon::getUserDetails($request);

        //$rs = [];
        $rs = RechargeReportRetailer::getRechargeDetailsRetailer($ob->user);

        //print_r($rs);

        $d1 = $net_1->select('net_type_code','net_type_name')->where('net_type_status', '=', '1')->where('net_type_name', '=', 'BILL PAYMENT')->get();

        $net_type_code = 0;
        if($d1->count() > 0)
        {
            $net_type_code = $d1[0]->net_type_code;
        }
        $d2 = $net_2->select('net_code','net_name')->where('net_type_code', '=', $net_type_code)->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', 'RETAILER');
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('retailer.ebbill', ['networktype' => $d1, 'network' => $d2, 'recharge' => $rs, 'user' => $ob, 'offer' => $off, 'in_active' => $d3]);
        		
    }

   
    
   

    public function loadData(Request $request)
    {
        $ob = GetCommon::getUserDetails($request);
        
        $str = RechargeReportRetailer::getN_RechargeDetailsRetailer_new($ob->user);

        echo $str;
    }

    
    public function getNetworkData($net_type_code, Request $request)
    {
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $net_type_code], ['net_status', '=', 1]])->get();
        
        $rs = [];
        
        $j = 0;
        foreach($d1 as $d)
        {
            $rs[$j][0] = $d->net_code;
            $rs[$j][1] = $d->net_name;
            $j++;
        }
        
        $result['data'] = $rs;
        echo json_encode($result);
    }

   

    

    public function store(Request $request)
	{
               // Declarations
        $op = "";
        $z = 0;
      

        // Validation
        $this->validate($request, [
            'user_mobile' => 'required',
            'user_amount' => 'required'
        ],
        [
            'user_mobile.required' => ' Mobile No is required.',
            'user_amount.required' => ' Recharge Amount is required.'
        ]);

        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $data = [];
        $data['net_type_code'] = trim($request->net_type_code);
        $data['net_code'] = trim($request->net_code);
        $data['user_mobile'] = trim($request->user_mobile);
        $data['user_amount'] = trim($request->user_amount);
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        //Store log
        $date_time = date("Y-m-d H:i:s");
        $ip = $request->ip();
        $ua = $request->server('HTTP_USER_AGENT');
        $log_data = $ob->user."^^^^^".$date_time."^^^^^WEB^^^^^".$data['user_mobile']."^^^^^".$ip."^^^^^".$ua;
        file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);
        

        //list($z, $trans_id) = RechargeMobile::add($data);
        //$data['trans_id'] = $trans_id;
        $z = RechargeNewMobile::add($data);

        if($z == 0)
        {
            $op = "Recharge Process is Completed Successfully..";
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! Mode web is not Selected...";
        }
        else if($z == 4)
        {   
            $op = "Error! Account is Inactive...";
        }
        else if($z == 5)
        {
            $op = "Error! Account Balance is low from Setup Fee...";
        }
        else if($z == 6)
        {
            $op = "Error! Recharge Status is already Pending for this Mobile No...";
        }
        else if($z == 7)
        {
            $op = "Error! Wait for 10 Minutes...";
        }
        else if($z == 8)
        {
            $op = "Sorry! Server is Temporarily shut down...";
        }
       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

       
    }

  

    
    public function bonrixUrl()
    {
        echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>";
        $xx = "<script>$.ajax({
            method: 'GET', // Type of response and matches what we said in the route
            url: 'test4', // This is the url we gave in the route
            data: {'id' : 1}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                document.writeln(response); 
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                document.writeln('AJAX error: ' + textStatus + ' : ' + errorThrown);
            }
        });</script>";
        echo $xx;
        file_put_contents("bonrix_url1.txt", $xx);
    }

    public function bonrixUrlResult(Request $request)
    {
        if($request->session()->has('bonrix_url'))
        {
            $url = $request->session()->get('bonrix_url');
            file_put_contents("bonrix_url.txt", $url);
            echo "<iframe src=".$url." id='ab' style='display:none;'></iframe>";
            $request->session()->forget('bonrix_url');
        }
       
       
        echo "Please Wait.... ";
    }


   
    
    // Reprocess
    public function storeWebReprocess(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;

        $d1 = $request->all();
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "trid")
            {
                $trans_id = $value;
            }
            else if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "net_code")
            {
                $net_code = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
            else if($key == "amt")
            {
                $user_amount = $value;
            }
            
            
        }

        $ob = $this->getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;

        $date_time = date("Y-m-d H:i:s");

       /* echo "User Code :".$user_code."<BR>";
        echo "User Name :".$user_name."<BR>";
        echo "User PWD :".$user_pwd."<BR>";
        echo "User Balance :".$user_ubal."<BR>";
        echo "Operator :".$operator."<BR>";
        echo "User Mobile :".$user_mobile."<BR>";
        echo "User Amount :".$user_amount."<BR>";
        echo "User Trid :".$user_trid."<BR>";
        echo "Network Code :".$net_code."<BR>";
        echo "Network Type Code :".$net_type_code."<BR>";
        echo "Transaction Id :".$trans_id."<BR>";
        echo "date time :".$date_time."<BR>";*/

        /*
        |--------------------------------------------------------------------------
        | Recharge Process
        |--------------------------------------------------------------------------
        |   1. Check Valid User and Valid API Partner and User Account Status
        |   2. Check User Balance  
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        
        // Get Network Line
        list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }

        
        
        if($z == 0)
        {
            $api_url = ApiUrl::generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);

            //echo "URL :".$api_url."<br>";

            if($api_url != "NONE")
            {
                
                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                if($res->getStatusCode() == 200)
                {
                    $op = "Recharge Process has Completed...";
                    // End DO payment Process
                }
                else
                {
                    $z = 5;
                }

            }
            // Url Success Ends.....
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else
        {
            $op = "Error! ";
        }

        $net_ubal = $this->getUserBalance($user_name);

        $resultdata = array('status' => $op, 'balance' => $net_ubal);
        
        return response()->json($resultdata, 200);

        

    }

    public function getUserAccountDetails($user_name, $user_pwd)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;
        $ob->status = 0;

        $uacc_1 = new UserAccountBalanceDetails;
        $user_2 = new UserAccountDetails;

        $user_rec_mode = "";
        $user_status = 25;

        $d2 = $user_2->select('user_code', 'user_type', 'user_rec_mode', 'user_status')->where([['user_name', '=', $user_name], ['user_pwd', '=', $user_pwd]])->get();
        foreach($d2 as $d)
        {
            $ob->user = $user_name;
            $ob->code = $d->user_code;
            $ob->mode = $d->user_type;
            $user_rec_mode = $d->user_rec_mode;
            $user_status = $d->user_status;
            $ob->status = 0;
        }
         // Check User Account Status
        if($user_status != 1)
        {
            $ob->status = 4;
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getUserBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_balance;
        }

        return $user_ubal;
    }


}