<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\BackupRechargeDetails;
use App\Models\UserOldAccountDetails;


class AD_Backup_Bal_RetController extends Controller
{
    //
    public function index(Request $request)
	{
        $rech_date = "";

        $ob = GetCommon::getUserDetails($request);

        $d1 = UserOldAccountDetails::select('too_date')->where('rech_type', 'RECHARGE')->orderby('id', 'desc')->limit(1)->get();

        if($d1->count() > 0) {
            $rech_date = $d1[0]->too_date;
        }

        return view('admin.ad_backup_bal_retailer', ['user' => $ob, 'rech_date' => $rech_date]);
        
    }

    public function store(Request $request)
	{
        $back_1 = new BackupRechargeDetails;
    
        $op = 0;
        $j = 0;
        $k = 0;
        $date_time = date('Y-m-d H:i:s');
        $time = "0 Seconds..";
        
        $ob = GetCommon::getUserDetails($request);

        $month = trim($request->month);
        $year = trim($request->year);

        $m = $this->getMonth($month);

        $query_date = $year."-".$m."-01";

        $f_date = date('Y-m-01', strtotime($query_date)); // hard-coded '01' for first day
        $t_date = date('Y-m-t', strtotime($query_date));

        $f_date = $f_date." 00:00:00";
        $t_date = $t_date." 23:59:59";

        if($f_date != "" && $t_date != "")
        {
            $start = microtime(true);

            $d1 = BackupRechargeDetails::groupBy('user_name')
                                        ->selectRaw('user_name, sum(rech_total) as total')
                                        ->whereBetween('rech_date', [$f_date, $t_date])
                                        ->where('rech_status', 'SUCCESS')->get(); 

            $data_x = [];
            $j = 0;
            $k = 0;
            $m = 0;
            
            foreach($d1 as $d)
            {
                //echo $d->user_name."-----".$d->total."<br>";

                $user_name = $d->user_name;
                $ce_tot = $d->total;

                if($user_name != "" && $ce_tot != "")
                {
                    $ce_tot = round($ce_tot, 2);
                    $user_name =strtoupper($user_name);

                    $cnt = UserOldAccountDetails::whereRaw('upper(user_name) = ?',[$user_name])
                                                    ->where('from_date', $f_date)
                                                    ->where('too_date', $t_date)
                                                    ->where('rech_type', 'RECHARGE')
                                                    ->count();

                    if($cnt == 0)
                    {
                        $data = [ 
                            'trans_id' => rand(100000, 999999),
                            'user_name' => $user_name,
                            'from_date' => $f_date,
                            'too_date' => $t_date,
                            'rech_type' => "RECHARGE",
                            'user_amount' => $ce_tot,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        ];

                        array_push($data_x, $data);
                        $j++;
                    }
                    else {
                        $cx = UserOldAccountDetails::whereRaw('upper(user_name) = ?',[$user_name])
                                                    ->where('from_date', $f_date)
                                                    ->where('too_date', $t_date)
                                                    ->where('rech_type', 'RECHARGE')
                                                    ->update(['user_amount' => $ce_tot, 'updated_at' => $date_time]);
                        $k++;
                    }
                }

                
            }

            if($j > 0)
            {
                $v = UserOldAccountDetails::insert($data_x);
            }

            $time = microtime(true) - $start;

        }
        
        $op = $j." Entries Inserted successfully....<br>".$k." Entries Updated Successfully...<br> Execution Time :".$time;

        return redirect()->back()->with('msg', $op);
    }

    public function getMonth($month)
    {
        $m = '0';
        if($month == "January") {
            $m = '01';
        }
        else if($month == "February") {
            $m = '02';
        }
        else if($month == "March") {
            $m = '03';
        }
        else if($month == "April") {
            $m = '04';
        }
        else if($month == "May") {
            $m = '05';
        }
        else if($month == "June") {
            $m = '06';
        }
        else if($month == "July") {
            $m = '07';
        }
        else if($month == "August") {
            $m = '08';
        }
        else if($month == "September") {
            $m = '09';
        }
        else if($month == "October") {
            $m = '10';
        }
        else if($month == "November") {
            $m = '11';
        }
        else if($month == "December") {
            $m = '12';
        }

        return $m;
    }

    
}
