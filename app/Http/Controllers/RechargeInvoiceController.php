<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;


use App\Models\UserRechargeDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\NetworkDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;

use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\UserAccountDetails;
use App\Models\UserPersonalDetails;
use App\Models\ApiProviderDetails;
use App\Models\UserBankTransferDetails;
use App\Models\AllTransaction;

use PDF;
use EXCEL;

class RechargeInvoiceController extends Controller
{
    //
    public function view_invoice(Request $request, $trans_id)
	{
        $rech_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeNewStatusDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('trans_id', '=', $trans_id)->get();

        //$d2 = $rech_2->where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->rech_date;

            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $net_name = "";
            $d3 = $unet_1->where('net_code', '=', $net_code)->get();
            if($d3->count() > 0)
            {
                $net_name = $d3[0]->net_name;
            }

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $html = "
            <!doctype html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title>Vaibav Online - Retailer Recharge Invoice</title>
                
                <style>
                .invoice-box {
                    max-width: 800px;
                    margin: auto;
                    padding: 30px;
                    border: 1px solid #eee;
                    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
                    font-size: 16px;
                    line-height: 24px;
                    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                    color: #555;
                }
                
                .invoice-box table {
                    width: 100%;
                    line-height: inherit;
                    text-align: left;
                }
                
                .invoice-box table td {
                    padding: 5px;
                    vertical-align: top;
                }
                
                .invoice-box table tr td:nth-child(2) {
                    text-align: right;
                }
                
                .invoice-box table tr.top table td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.top table td.title {
                    font-size: 45px;
                    line-height: 45px;
                    color: #333;
                }
                
                .invoice-box table tr.information table td {
                    padding-bottom: 40px;
                }
                
                .invoice-box table tr.heading td {
                    background: #eee;
                    border-bottom: 1px solid #ddd;
                    font-weight: bold;
                }
                
                .invoice-box table tr.details td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.item td{
                    border-bottom: 1px solid #eee;
                }
                
                .invoice-box table tr.item.last td {
                    border-bottom: none;
                }
                
                .invoice-box table tr.total td:nth-child(2) {
                    border-top: 2px solid #eee;
                    font-weight: bold;
                }
                
                @media only screen and (max-width: 600px) {
                    .invoice-box table tr.top table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                    
                    .invoice-box table tr.information table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                }
                
                /** RTL **/
                .rtl {
                    direction: rtl;
                    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                }
                
                .rtl table {
                    text-align: right;
                }
                
                .rtl table tr td:nth-child(2) {
                    text-align: left;
                }
                </style>

                <script type='text/javascript'>
                    function add(f) {

                        var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                        var second_number = parseFloat(document.getElementById('s_charge').value);
                        var result = first_number;
                        if (second_number != '')
                        {
                            result = first_number + second_number;
                        }           
                        document.getElementById('s_total').innerHTML = result;
                        f.style.width = ((f.value.length + 1) * 8) + 'px';
                    }
                </script>
            </head>
            
            <body>
                <div class='invoice-box'>
                    <table cellpadding='0' cellspacing='0'>
                        <tr class='top'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td class='title'>
                                            <img src='".asset('img/brand/logo.png') ."' style='height:70px; width:180px;'>
                                        </td>
                                        
                                        <td>
                                            <B>Invoice # </B>: ".$trans_id."<br>
                                            <B>Dated</B>: ".$rech_date."<br>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='information'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td>
                                            <B>".$per_name."</B><br>
                                            <B>".$per_city."</B><br>
                                        </td>
                                        
                                        <td>
                                           
                                            ".$per_mobile."<br>
                                            ".$per_mail."<br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Network
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td>
                               ".$net_name."
                            </td>
                            
                            <td>
                               
                            </td>
                        </tr>

                        <tr class='heading'>
                            <td>
                                Mobile No
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td style = 'padding-bottom:5px;'>
                               ".$rech_mobile."
                            </td>
                            
                            <td style = 'padding-bottom:5px;'>
                                &#8377; <label id = 's_amt'>".$rech_amount."</label>
                            </td>
                        </tr>

                        <tr class='details'>
                            <td>
                                Service Charges
                            </td>
                            
                            <td>
                            &#8377; <input type = 'text' id = 's_charge' value = '0' onkeyup='add(this)' style = 'border:0px;width:8px;font-size: 16px;
                            line-height: 24px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;text-align:right;
                            color: #555;' />
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Status
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='item'>
                            <td>
                                REF.ID:".$reply_id."
                            </td>
                            
                            <td>
                                ".$rech_status."
                            </td>
                        </tr>

                        
                        
                       
                        
                        
                        
                        <tr class='total'>
                            <td></td>
                            
                            <td>
                               Total: &#8377; <label id = 's_total'>".$rech_amount."</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            </html>
            
            ";

            echo $html;
        }
    }


    public function view_bill_invoice(Request $request, $trans_id)
	{
        $rech_1 = new UserRechargeBillDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->created_at;
            $rech_option = $d1[0]->rech_option;

            $status = "PENDING";
            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
            }

            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $net_name = "";
            $d3 = $unet_1->where('net_code', '=', $net_code)->get();
            if($d3->count() > 0)
            {
                $net_name = $d3[0]->net_name;
            }

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $html = "
            <!doctype html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title>Vaibav Online - Retailer Recharge Invoice</title>
                
                <style>
                .invoice-box {
                    max-width: 800px;
                    margin: auto;
                    padding: 30px;
                    border: 1px solid #eee;
                    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
                    font-size: 16px;
                    line-height: 24px;
                    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                    color: #555;
                }
                
                .invoice-box table {
                    width: 100%;
                    line-height: inherit;
                    text-align: left;
                }
                
                .invoice-box table td {
                    padding: 5px;
                    vertical-align: top;
                }
                
                .invoice-box table tr td:nth-child(2) {
                    text-align: right;
                }
                
                .invoice-box table tr.top table td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.top table td.title {
                    font-size: 45px;
                    line-height: 45px;
                    color: #333;
                }
                
                .invoice-box table tr.information table td {
                    padding-bottom: 40px;
                }
                
                .invoice-box table tr.heading td {
                    background: #eee;
                    border-bottom: 1px solid #ddd;
                    font-weight: bold;
                }
                
                .invoice-box table tr.details td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.item td{
                    border-bottom: 1px solid #eee;
                }
                
                .invoice-box table tr.item.last td {
                    border-bottom: none;
                }
                
                .invoice-box table tr.total td:nth-child(2) {
                    border-top: 2px solid #eee;
                    font-weight: bold;
                }
                
                @media only screen and (max-width: 600px) {
                    .invoice-box table tr.top table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                    
                    .invoice-box table tr.information table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                }
                
                /** RTL **/
                .rtl {
                    direction: rtl;
                    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                }
                
                .rtl table {
                    text-align: right;
                }
                
                .rtl table tr td:nth-child(2) {
                    text-align: left;
                }
                </style>

                <script type='text/javascript'>
                    function add(f) {

                        var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                        var second_number = parseFloat(document.getElementById('s_charge').value);
                        var result = first_number;
                        if (second_number != '')
                        {
                            result = first_number + second_number;
                        }           
                        document.getElementById('s_total').innerHTML = result;
                        f.style.width = ((f.value.length + 1) * 8) + 'px';
                    }
                </script>
            </head>
            
            <body>
                <div class='invoice-box'>
                    <table cellpadding='0' cellspacing='0'>
                        <tr class='top'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td class='title'>
                                            <img src='".asset('img/brand/logo.png') ."' style='height:70px; width:180px;'>
                                        </td>
                                        
                                        <td>
                                            <B>Invoice # </B>: ".$trans_id."<br>
                                            <B>Dated</B>: ".$rech_date."<br>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='information'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td>
                                            <B>".$per_name."</B><br>
                                            <B>".$per_city."</B><br>
                                        </td>
                                        
                                        <td>
                                           
                                            ".$per_mobile."<br>
                                            ".$per_mail."<br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Network
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td>
                               ".$net_name."
                            </td>
                            
                            <td>
                               
                            </td>
                        </tr>

                        <tr class='heading'>
                            <td>
                                TNEB Connection No
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td style = 'padding-bottom:5px;'>
                               ".$rech_mobile."
                            </td>
                            
                            <td style = 'padding-bottom:5px;'>
                                &#8377; <label id = 's_amt'>".$rech_amount."</label>
                            </td>
                        </tr>

                        <tr class='details'>
                            <td>
                                Service Charges
                            </td>
                            
                            <td>
                            &#8377; <input type = 'text' id = 's_charge' value = '0' onkeyup='add(this)' style = 'border:0px;width:8px;font-size: 16px;
                            line-height: 24px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;text-align:right;
                            color: #555;' />
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Status
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='item'>
                            <td>
                                REF.ID:".$reply_id."
                            </td>
                            
                            <td>
                                ".$status."
                            </td>
                        </tr>

                        
                        
                       
                        
                        
                        
                        <tr class='total'>
                            <td></td>
                            
                            <td>
                               Total: &#8377; <label id = 's_total'>".$rech_amount."</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            </html>
            
            ";

            echo $html;
        }
    }

    public function view_money_invoice(Request $request, $trans_id)
	{
        $rech_1 = new UserBankTransferDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $bank_code1 = $d1[0]->rech_bank;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->rech_date;
            $rech_option = $d1[0]->rech_option;

            $status = "PENDING";
            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
            }


            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $bk = explode("--", $bank_code1);
            $bankCode  = $bk[0];
            $bankShort = substr($bankCode, 0, 3);
            $bankName  = $bankCode;


            $bankDetails = $this->getBank();

            foreach($bankDetails as $code)
            {
                if($code['bankCode'] == $bankShort)
                {
                    $bankName = $code['bankName'];
                    break;
                }
            }

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $html = "
            <!doctype html>
            <html>
            <head>
                <meta charset='utf-8'>
                <title>Vaibav Online - Retailer Recharge Invoice</title>
                
                <style>
                .invoice-box {
                    max-width: 800px;
                    margin: auto;
                    padding: 30px;
                    border: 1px solid #eee;
                    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
                    font-size: 16px;
                    line-height: 24px;
                    font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                    color: #555;
                }
                
                .invoice-box table {
                    width: 100%;
                    line-height: inherit;
                    text-align: left;
                }
                
                .invoice-box table td {
                    padding: 5px;
                    vertical-align: top;
                }
                
                .invoice-box table tr td:nth-child(2) {
                    text-align: right;
                }
                
                .invoice-box table tr.top table td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.top table td.title {
                    font-size: 45px;
                    line-height: 45px;
                    color: #333;
                }
                
                .invoice-box table tr.information table td {
                    padding-bottom: 40px;
                }
                
                .invoice-box table tr.heading td {
                    background: #eee;
                    border-bottom: 1px solid #ddd;
                    font-weight: bold;
                }
                
                .invoice-box table tr.details td {
                    padding-bottom: 20px;
                }
                
                .invoice-box table tr.item td{
                    border-bottom: 1px solid #eee;
                }
                
                .invoice-box table tr.item.last td {
                    border-bottom: none;
                }
                
                .invoice-box table tr.total td:nth-child(2) {
                    border-top: 2px solid #eee;
                    font-weight: bold;
                }
                
                @media only screen and (max-width: 600px) {
                    .invoice-box table tr.top table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                    
                    .invoice-box table tr.information table td {
                        width: 100%;
                        display: block;
                        text-align: center;
                    }
                }
                
                /** RTL **/
                .rtl {
                    direction: rtl;
                    font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
                }
                
                .rtl table {
                    text-align: right;
                }
                
                .rtl table tr td:nth-child(2) {
                    text-align: left;
                }
                </style>

                <script type='text/javascript'>
                    function add(f) {

                        var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                        var second_number = parseFloat(document.getElementById('s_charge').value);
                        var result = first_number;
                        if (second_number != '')
                        {
                            result = first_number + second_number;
                        }           
                        document.getElementById('s_total').innerHTML = result;
                        f.style.width = ((f.value.length + 1) * 8) + 'px';
                    }
                </script>
            </head>
            
            <body>
                <div class='invoice-box'>
                    <table cellpadding='0' cellspacing='0'>
                        <tr class='top'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td class='title'>
                                            <img src='".asset('img/brand/logo.png') ."' style='height:70px; width:180px;'>
                                        </td>
                                        
                                        <td>
                                            <B>Invoice # </B>: ".$trans_id."<br>
                                            <B>Dated</B>: ".$rech_date."<br>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='information'>
                            <td colspan='2'>
                                <table>
                                    <tr>
                                        <td>
                                            <B>".$per_name."</B><br>
                                            <B>".$per_city."</B><br>
                                        </td>
                                        
                                        <td>
                                           
                                            ".$per_mobile."<br>
                                            ".$per_mail."<br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Bank
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td>
                               ".$bankName."
                            </td>
                            
                            <td>
                               
                            </td>
                        </tr>

                        <tr class='heading'>
                            <td>
                                Account No
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='details'>
                            <td style = 'padding-bottom:5px;'>
                               ".$rech_mobile."
                            </td>
                            
                            <td style = 'padding-bottom:5px;'>
                                &#8377; <label id = 's_amt'>".$rech_amount."</label>
                            </td>
                        </tr>

                        <tr class='details'>
                            <td>
                                Service Charges
                            </td>
                            
                            <td>
                            &#8377; <input type = 'text' id = 's_charge' value = '0' onkeyup='add(this)' style = 'border:0px;width:8px;font-size: 16px;
                            line-height: 24px;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;text-align:right;
                            color: #555;' />
                            </td>
                        </tr>
                        
                        <tr class='heading'>
                            <td>
                                Status
                            </td>
                            
                            <td>
                                
                            </td>
                        </tr>
                        
                        <tr class='item'>
                            <td style = 'font-size:12px;'>
                                REF.ID:".$reply_id."
                            </td>
                            
                            <td>
                                ".$status."
                            </td>
                        </tr>

                        
                        
                       
                        
                        
                        
                        <tr class='total'>
                            <td></td>
                            
                            <td style = 'width:200px;'>
                               Total: &#8377; <label id = 's_total'>".$rech_amount."</label>
                            </td>
                        </tr>
                    </table>
                </div>
            </body>
            </html>
            
            ";

            echo $html;
        }
    }

    public function view_invoice_thermal(Request $request, $trans_id)
	{
        $rech_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeNewStatusDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('trans_id', '=', $trans_id)->get();

        //$d2 = $rech_2->where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->rech_date;

            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $net_name = "";
            $d3 = $unet_1->where('net_code', '=', $net_code)->get();
            if($d3->count() > 0)
            {
                $net_name = $d3[0]->net_name;
            }

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $m = "MOBILE";
            $n = "NETWORK";

            $html = "
            <!DOCTYPE html>
            <html lang='en'>
            <head>
            <meta charset='utf-8'>
            <title>Receipt</title>
            <style>
            page[size='A4'] { background-color:white; width:  3in; height: auto; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 10px rgba(0, 0, 0, .15); }
            @media print { body, page[size='A4'] { margin: 0; box-shadow: 0; } footer {page-break-after: always;} }
            </style> 
            <script type='text/javascript'>
                function add(f) {

                    var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                    var second_number = parseFloat(document.getElementById('s_charge').value);
                    var result = first_number;
                    if (second_number != '')
                    {
                        result = first_number + second_number;
                    }           
                    document.getElementById('s_total').innerHTML = result;
                    f.style.width = ((f.value.length + 1) * 8) + 'px';
                }
            </script>
            </head>
            
            <body>
            <page size='A4'>
                <div style='padding-left:5px;'>
                    <label ><b><font style='font-size:18px;'>VAIBAVONLINE</font></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font style='font-size:13px;'>".$per_name."</font></label><br>
                    <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <font style='font-size:13px;'>".$per_city."</font></label><br>
                        <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <font style='font-size:13px;'>".$per_mobile."</font></label><br>
                </div>
                <br>
				<div style='padding-left:5px;'>
					<label style='font-size:12px;'><b>INVOICE NO : </b> ".$trans_id."</label><br>
					<label style='font-size:12px;'><b>DATE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_date."</label><br>
					<label style='font-size:12px;'>--------------------------------------------------------------------</label>
				</div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>".$n."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$net_name."</label><br>
                    <label style='font-size:12px;'><b>".$m." NO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$rech_mobile."</label><br>
                    <label style='font-size:12px;'><b>AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                        </label><label id = 's_amt'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'><b>SERVICE CHARGES&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                            <input type ='text' id = 's_charge' value = '0' onkeyup='add(this)' style='width:35px;border:0px;box-shadow: none;' /></label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_status."</label><br>
                    <label style='font-size:12px;'><b>REF.ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$reply_id."</label><br>
                    
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>

                <div style='padding-left:5px;'>
                    <label style='font-size:14px;'><b>TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                        </label><label id = 's_total'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
            </page>
            </body>
            </html>";

            echo $html;
        }
    }

    public function view_bill_invoice_thermal(Request $request, $trans_id)
	{
        $rech_1 = new UserRechargeBillDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->created_at;
            $rech_option = $d1[0]->rech_option;

            $status = "PENDING";
            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
            }

            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $net_name = "";
            $d3 = $unet_1->where('net_code', '=', $net_code)->get();
            if($d3->count() > 0)
            {
                $net_name = $d3[0]->net_name;
            }

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $m = "TNEB CON No";
            $n = "NETWORK";

            $html = "
            <!DOCTYPE html>
            <html lang='en'>
            <head>
            <meta charset='utf-8'>
            <title>Receipt</title>
            <style>
            page[size='A4'] { background-color:white; width:  3in; height: auto; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 10px rgba(0, 0, 0, .15); }
            @media print { body, page[size='A4'] { margin: 0; box-shadow: 0; } footer {page-break-after: always;} }
            </style> 
            <script type='text/javascript'>
                function add(f) {

                    var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                    var second_number = parseFloat(document.getElementById('s_charge').value);
                    var result = first_number;
                    if (second_number != '')
                    {
                        result = first_number + second_number;
                    }           
                    document.getElementById('s_total').innerHTML = result;
                    f.style.width = ((f.value.length + 1) * 8) + 'px';
                }
            </script>
            </head>
            
            <body>
            <page size='A4'>
                <div style='padding-left:5px;'>
                    <label ><b><font style='font-size:18px;'>VAIBAVONLINE</font></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font style='font-size:13px;'>".$per_name."</font></label><br>
                    <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <font style='font-size:13px;'>".$per_city."</font></label><br>
                        <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <font style='font-size:13px;'>".$per_mobile."</font></label><br>
                </div>
                <br>
				<div style='padding-left:5px;'>
					<label style='font-size:12px;'><b>INVOICE NO : </b> ".$trans_id."</label><br>
					<label style='font-size:12px;'><b>DATE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_date."</label><br>
					<label style='font-size:12px;'>--------------------------------------------------------------------</label>
				</div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>".$n."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$net_name."</label><br>
                    <label style='font-size:12px;'><b>".$m."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$rech_mobile."</label><br>
                    <label style='font-size:12px;'><b>AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                        </label><label id = 's_amt'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'><b>SERVICE CHARGES&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                            <input type ='text' id = 's_charge' value = '0' onkeyup='add(this)' style='width:35px;border:0px;box-shadow: none;' /></label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_status."</label><br>
                    <label style='font-size:12px;'><b>REF.ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$reply_id."</label><br>
                    
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>

                <div style='padding-left:5px;'>
                    <label style='font-size:14px;'><b>TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                        </label><label id = 's_total'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
            </page>
            </body>
            </html>";

            echo $html;
        }
    }

    public function view_money_invoice_thermal(Request $request, $trans_id)
	{
        $rech_1 = new UserBankTransferDetails;
        $uper_1 = new UserPersonalDetails;
        $unet_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = ALlTransaction::where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $bank_code1 = $d1[0]->rech_bank;
            $rech_amount = $d1[0]->rech_amount;
            $rech_total = $d1[0]->ret_total;
            $rech_status = $d1[0]->rech_status;
            $reply_id = $d1[0]->reply_id;
            $rech_date = $d1[0]->rech_date;
            $rech_option = $d1[0]->rech_option;

            $status = "PENDING";
            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
            }

            $d2 = $uper_1->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $per_name = $d2[0]->user_per_name;
                $per_city = $d2[0]->user_city;
                $per_mobile = $d2[0]->user_mobile;
                $per_mail = $d2[0]->user_mail;
            }

            $bk = explode("--", $bank_code1);
            $bankCode  = $bk[0];
            $bankShort = substr($bankCode, 0, 3);
            $bankName  = "-";

            $bankDetails = $this->getBank();

            foreach($bankDetails as $code)
            {
                if($code['bankCode'] == $bankShort)
                {
                    $bankName = $code['bankName'];
                    break;
                }
            }

            if($bankName == "-")
            {
                $bankShort = substr($bankCode, 0, 4);

                foreach($bankDetails as $code)
                {
                    if($code['bankCode'] == $bankShort)
                    {
                        $bankName = $code['bankName'];
                        break;
                    }
                }
            }

            if($bankName == "-")
            {
                $bankName = $bankCode;
            }

            

            //echo $user_name."--".$net_code."--".$rech_mobile."--".$rech_amount.'--'.$rech_total."--".$rech_status."--".$reply_id;

            $m = "ACCOUNT NO";
            $n = "BANK";

            $html = "
            <!DOCTYPE html>
            <html lang='en'>
            <head>
            <meta charset='utf-8'>
            <title>Receipt</title>
            <style>
            page[size='A4'] { background-color:white; width:  3in; height: auto; display: block; margin: 0 auto; margin-bottom: 0.5cm; box-shadow: 0 0 10px rgba(0, 0, 0, .15); }
            @media print { body, page[size='A4'] { margin: 0; box-shadow: 0; } footer {page-break-after: always;} }
            </style> 
            <script type='text/javascript'>
                function add(f) {

                    var first_number = parseFloat(document.getElementById('s_amt').innerHTML);
                    var second_number = parseFloat(document.getElementById('s_charge').value);
                    var result = first_number;
                    if (second_number != '')
                    {
                        result = first_number + second_number;
                    }           
                    document.getElementById('s_total').innerHTML = result;
                    f.style.width = ((f.value.length + 1) * 8) + 'px';
                }
            </script>
            </head>
            
            <body>
            <page size='A4'>
                <div style='padding-left:5px;'>
                    <label ><b><font style='font-size:18px;'>VAIBAVONLINE</font></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font style='font-size:13px;'>".$per_name."</font></label><br>
                    <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <font style='font-size:13px;'>".$per_city."</font></label><br>
                        <label >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <font style='font-size:13px;'>".$per_mobile."</font></label><br>
                </div>
                <br>
				<div style='padding-left:5px;'>
					<label style='font-size:12px;'><b>INVOICE NO : </b> ".$trans_id."</label><br>
					<label style='font-size:12px;'><b>DATE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_date."</label><br>
					<label style='font-size:12px;'>--------------------------------------------------------------------</label>
				</div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>".$n."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$bankName."</label><br>
                    <label style='font-size:12px;'><b>".$m." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                    ".$rech_mobile."</label><br>
                    <label style='font-size:12px;'><b>AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                        </label><label id = 's_amt'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'><b>SERVICE CHARGES&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                                            <input type ='text' id = 's_charge' value = '0' onkeyup='add(this)' style='width:35px;border:0px;box-shadow: none;' /></label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
                <div style='padding-left:5px;'>
                    <label style='font-size:12px;'><b>STATUS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$rech_status."</label><br>
                    <label style='font-size:12px;'><b>REF.ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                            ".$reply_id."</label><br>
                    
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>

                <div style='padding-left:5px;'>
                    <label style='font-size:14px;'><b>TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b> 
                        </label><label id = 's_total'>".$rech_amount."</label><br>
                    <label style='font-size:12px;'>--------------------------------------------------------------------</label>
                </div>
            </page>
            </body>
            </html>";

         

            echo $html;
        }
    }


    public function getBank()
    {
        // code...
        $apiUrl = "http://www.vaibavonline.com/vaibav/api/bank-data";
        $bank   = [];

        $client = new \GuzzleHttp\Client();
        $res = $client->get($apiUrl);
        $res_code = $res->getStatusCode();
        $res_content = (string) $res->getBody();

        if($res_code == 200) {
            $jsonData     = json_decode($res_content);

            usort($jsonData->bank, array( $this, 'compareBank' ));

            foreach($jsonData->bank as $d)
            {
                array_push($bank, ['bankCode' => $d->bank_sort_name, 'bankName' => $d->bank_name, 'ifscShort' => $d->ifsc_alias, 
                        'branchIfsc' => $d->branch_ifsc]);
            }
        }

        return $bank;
    }

    public function compareBank($a, $b)
    {
        return strcmp($a->bank_name, $b->bank_name);
    }

    public function getRecharge($transId)
    {
        $rechData = [];
        $details  = AllTransaction::where('trans_id', '=', $transId)->first();

        $rechData['status'] = false;

        if($details)
        {
            $rechData['status']     = true;
            $rechData['userName']   = $details->user_name;
            $rechData['netCode']    = $details->net_code;
            $rechData['rechMobile'] = $details->rech_mobile;
            $rechData['rechAmount'] = $details->rech_amount;
            $rechData['rechTotal']  = $details->ret_total;
            $rechData['rechStatus'] = $details->rech_status;
            $rechData['replyId']    = $details->reply_id;
            $rechData['rechDate']   = $details->created_at;
            $rechData['rechOption'] = $details->rech_option;
            $rechData['replyStatus'] = "PENDING";


            if($rechData['rechStatus'] == "PENDING" && $rechData['rechOption'] == 1)
            {
                $rechData['replyStatus'] = "PENDING";
            }
            else if($rechData['rechStatus'] == "PENDING" && $rechData['rechOption'] == 2)
            {
                $rechData['replyStatus'] = "FAILURE";
            }
            else if($rechData['rechStatus'] == "FAILURE"  && $rechData['rechOption'] == 2)
            {      
                $rechData['replyStatus'] = "FAILURE";
            }
            else if($rechData['rechStatus'] == "SUCCESS")
            {
                $rechData['replyStatus'] = "SUCCESS";
            }
        }

        return $rechData;
    }

}
