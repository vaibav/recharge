<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;
use App\Models\AllTransaction;

class AD_Pay_RemoteController extends Controller
{
    //
    public function index(Request $request)
	{

        $data = UserAccountDetails::select('user_code','user_name')->where('user_type', '=', 'SUPER DISTRIBUTOR')
                                                                    ->orWhere('user_type', '=', 'ADMIN')->orderBy('user_name')->get();
        $data1 = AllTransaction::where('trans_type', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', 'SELF_PAYMENT')
                                ->orWhere('trans_type', 'FUND_TRANSFER')
                                ->orWhere('trans_type', 'REFUND_PAYMENT')
                                ->orderBy('id', 'desc')->limit(6)->get();

        $ob = GetCommon::getUserDetails($request);

        return view('admin.ad_pay_remote', ['user1' => $data, 'pay1' => $data1, 'user' => $ob]);
        
		
    }

    public function getUserBalance($user_name, Request $request)
	{
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    
    public function store(Request $request)
	{

        // Validation
        $this->validate($request, [
            'user_name' => 'required',
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_name.required' => ' The user name is required.',
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.'
        ]);

        // Post Data
        $user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_code = trim($request->user_code);
        $user_name = trim($request->user_name);
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        $data['trans_id'] = rand(100000,999999);
        $data['user_name_req'] = $user_name;
        $data['user_name_grnt'] = $user_name_grnt;
        $data['user_amount'] = $user_amount;
        $data['user_remarks'] = $user_remarks;

        if($user_name == $user_name_grnt) {
            $data['payment_mode'] = "SELF_PAYMENT";

            $d1 = NetworkDetails::select('net_code')->where('net_name', 'SELF_PAYMENT')->get();

            $net_code = '0';
            if($d1->count() > 0) {
                $net_code = $d1[0]->net_code;
            }

            $data['net_code'] = $net_code;
        }
        else {
            $data['payment_mode'] = "REMOTE_PAYMENT";

            $d1 = NetworkDetails::select('net_code')->where('net_name', 'REMOTE_PAYMENT')->get();

            $net_code = '0';
            if($d1->count() > 0) {
                $net_code = $d1[0]->net_code;
            }

            $data['net_code'] = $net_code;
        }

        list($status, $op) = Stock::PaymentGrant($data);

      
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

}