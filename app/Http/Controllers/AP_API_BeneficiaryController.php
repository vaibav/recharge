<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetAPICommon;
use App\Libraries\BankRemitter;
use App\Libraries\Beneficiary;

use App\Models\UserBeneficiaryDetails;

class AP_API_BeneficiaryController extends Controller
{
    //
    public function ben_entry(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $beneficiary_account_no = trim($request->beneficiary_account_no);
        $beneficiary_name = trim($request->beneficiary_name);
        $beneficiary_bank_code = trim($request->beneficiary_bank_code);
        $beneficiary_ifsc_code = trim($request->beneficiary_ifsc_code);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name' => $user_name,
                    'msisdn' => $msisdn,
                    'beneficiary_account_no' => $beneficiary_account_no,
                    'beneficiary_name' => $beneficiary_name,
                    'beneficiary_bank_code' => $beneficiary_bank_code,
                    'beneficiary_ifsc_code' => $beneficiary_ifsc_code,
                    'api_trans_id' => $ben_id,
                    'r_mode' => 'API'
            ];
            
            $v = [];
            $v = $this->checkValues($data);

            if($v[0] == 0)
            {
                list($z, $msg, $trans_id, $ben_id, $res_content) = Beneficiary::add($data);

                $result = (array)json_decode($res_content);
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function ben_otp_entry(Request $request)
	{
        $ben_1 = new UserBeneficiaryDetails;

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);
        $otp = trim($request->otp);
        $msisdn = trim($request->msisdn);
        $api_trans_id = trim($request->ben_id);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name' => $user_name,
                'ben_id' => $api_trans_id,
                'msisdn' => $msisdn,
                'trans_id' => $trans_id,
                'otp' => $otp,
                'r_mode' => "API"
                ];
        
            
            $v = [];
            $v = $this->checkOtpValues($data);

            if($v[0] == 0)
            {
                $d1 = $ben_1->select('ben_id')->where('user_name', '=', $user_name)->where('msisdn', '=', $msisdn)
                                        ->where('api_trans_id', '=', $api_trans_id)->get();

                if($d1->count() > 0)
                {
                    $data['ben_id'] = $d1[0]->ben_id;

                    list($z, $msg, $res_content) = Beneficiary::add_otp($data);

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'Beneficiary Id is not registered...'];
                }

               
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function ben_get_account(Request $request)
	{
        $ben_1 = new UserBeneficiaryDetails;

        $auth_token = trim($request->auth_token);
        $beneficiary_account_no = trim($request->beneficiary_account_no);
        $beneficiary_bank_code = trim($request->beneficiary_bank_code);
        $beneficiary_ifsc_code = trim($request->beneficiary_ifsc_code);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name' => $user_name,
                        'msisdn' => $msisdn,
                        'beneficiary_account_no' => $beneficiary_account_no,
                        'beneficiary_bank_code' => $beneficiary_bank_code,
                        'beneficiary_ifsc_code' => $beneficiary_ifsc_code,
                        'client_trans_id' => rand(100000,999999),
                        'api_trans_id' => $ben_id,
                        'r_mode' => 'API'
                    ];
        
            
            $v = [];
            $v = $this->checkVerifyValues($data);

            if($v[0] == 0)
            {
                list($z, $msg, $ben_name, $res_content) = Beneficiary::check_ben_account($data);

                $result = (array)json_decode($res_content);

            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function checkValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['beneficiary_account_no']))
        {
            $v[0] = 2;
            $v[1] = "Invalid Account No";
        }
        else if($data['beneficiary_name'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary Name";
        }
        else if($data['beneficiary_bank_code'] == "" && $data['beneficiary_ifsc_code'] == "")
        {
            $v[0] = 3;
            $v[1] = "Enter Atleast Bank code or IFSC Code..";
        }
        else if($data['beneficiary_bank_code'] != "" && $data['beneficiary_ifsc_code'] != "")
        {
            $v[0] = 3;
            $v[1] = "Enter Anyone value Either Bank code or IFSC Code..";
        }
        else if($data['api_trans_id'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary ID";
        }

        return $v;
    }

    public function checkOtpValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['otp']))
        {
            $v[0] = 2;
            $v[1] = "Invalid OTP";
        }
        else if($data['trans_id'] == "")
        {
            $v[0] = 3;
            $v[1] = "Invalid Transaction Id..";
        }
        else if($data['ben_id'] == "" )
        {
            $v[0] = 3;
            $v[1] = "Invalid Beneficiary Id..";
        }
       

        return $v;
    }

    public function checkVerifyValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['beneficiary_account_no']))
        {
            $v[0] = 2;
            $v[1] = "Invalid Account No";
        }
        else if($data['beneficiary_bank_code'] == "" && $data['beneficiary_ifsc_code'] == "")
        {
            $v[0] = 3;
            $v[1] = "Enter Atleast Bank code or IFSC Code..";
        }
        else if($data['beneficiary_bank_code'] != "" && $data['beneficiary_ifsc_code'] != "")
        {
            $v[0] = 3;
            $v[1] = "Enter Anyone value Either Bank code or IFSC Code..";
        }
        else if($data['api_trans_id'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary ID";
        }
        

        return $v;
    }
}
