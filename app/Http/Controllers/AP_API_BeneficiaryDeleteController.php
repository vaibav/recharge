<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetAPICommon;
use App\Libraries\BankRemitter;
use App\Libraries\Beneficiary;

use App\Models\UserBeneficiaryDetails;

class AP_API_BeneficiaryDeleteController extends Controller
{
    //
    public function ben_delete_entry(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $beneficiary_id = trim($request->beneficiary_id);
        $msisdn = trim($request->msisdn);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name' => $user_name,
                    'msisdn' => $msisdn,
                    'ben_id' => $beneficiary_id,
                    'beneficiary_id' => $beneficiary_id,
                    'r_mode' => 'API'
            ];
            
            $v = [];
            $v = $this->checkValues($data);

            if($v[0] == 0)
            {
                list($z, $msg, $trans_id, $otp_status, $res_content) = Beneficiary::delete($data);

                $result = (array)json_decode($res_content);
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function ben_delete_otp_entry(Request $request)
	{
        $ben_1 = new UserBeneficiaryDetails;

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);
        $otp = trim($request->otp);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->beneficiary_id);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name' => $user_name,
                'ben_id' => $ben_id,
                'msisdn' => $msisdn,
                'trans_id' => $trans_id,
                'otp' => $otp,
                'r_mode' => "API"
                ];
        
            
            $v = [];
            $v = $this->checkOtpValues($data);

            if($v[0] == 0)
            {
                $d1 = $ben_1->select('ben_id')->where('msisdn', '=', $msisdn)
                                        ->where('trans_id', '=', $trans_id)->get();
            
                if($d1->count() > 0)
                {
                    $data['ben_id'] = $d1[0]->ben_id;

                    list($z, $msg, $res_content) = Beneficiary::delete_otp($data);

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'Beneficiary Id is not registered...'];
                }
                
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function checkValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
       
        else if($data['beneficiary_id'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary Id";
        }
        

        return $v;
    }

    public function checkOtpValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['otp']))
        {
            $v[0] = 2;
            $v[1] = "Invalid OTP";
        }
        else if($data['trans_id'] == "")
        {
            $v[0] = 3;
            $v[1] = "Invalid Transaction Id..";
        }
        

        return $v;
    }
}
