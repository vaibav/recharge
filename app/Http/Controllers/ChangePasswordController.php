<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use stdClass;

use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;


class ChangePasswordController extends Controller
{
    //
    public function index_admin(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('admin.change_password', ['user' => $ob]);
        
    }

    public function index_retailer(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('user.change_password_retailer', ['user' => $ob]);
        
    }

    public function index_distributor(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('user.change_password_distributor', ['user' => $ob]);
        
    }

    public function index_sdistributor(Request $request)
    {
        
        $ob = $this->getUserDetails($request);
        
        return view('user.change_password_sdistributor', ['user' => $ob]);
        
    }

    public function index_super_distributor(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('user.change_password_super_distributor', ['user' => $ob]);
        
    }

    public function index_api_partner(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('admin.change_password_api_partner', ['user' => $ob]);
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
        $user_2 = new UserAccountDetails;
        $op = 0;
        
        // Validation
        $this->validate($request, ['user_pwd' => 'required'], ['user_pwd.required' => ' The password is required.']);

        $ob = $this->getUserDetails($request);
        
        $user_pwd = trim($request->user_pwd);

        $password = bcrypt($user_pwd);

        $a = $user_2->where('user_code', '=', $ob->code)->update(['user_pwd' => $password]);
        
        if($a > 0)
            $op = 1;
        
        return redirect()->back()->with('msg', $op);
    }

    public function store_distributor(Request $request)
	{
        $user_2 = new UserAccountDetails;
        $op = 0;
        
        // Validation
        $this->validate($request, ['user_pwd' => 'required'], ['user_pwd.required' => ' The password is required.']);

        $ob = $this->getUserDetails($request);
        
        $user_pwd = trim($request->user_pwd);
        $user_name = trim($request->user_name);

        $password = bcrypt($user_pwd);

        $a = $user_2->where('user_name', '=', $user_name)->update(['user_pwd' => $password]);
        
        if($a > 0)
            $op = 1;
        
        return redirect()->back()->with('msg', $op);
    }

    public function store_sdistributor(Request $request)
    {
        $user_2 = new UserAccountDetails;
        $op = 0;
        
        // Validation
        $this->validate($request, ['user_pwd' => 'required'], ['user_pwd.required' => ' The password is required.']);

        $ob = $this->getUserDetails($request);
        
        $user_pwd = trim($request->user_pwd);
        $user_name = trim($request->user_name);

        $password = bcrypt($user_pwd);

        $a = $user_2->where('user_name', '=', $user_name)->update(['user_pwd' => $password]);
        
        if($a > 0)
            $op = 1;
        
        return redirect()->back()->with('msg', $op);
    }

    public function store_retailer(Request $request)
	{
        $user_2 = new UserAccountDetails;
        $op = 0;
        
        // Validation
        $this->validate($request, ['user_pwd' => 'required'], ['user_pwd.required' => ' The password is required.']);

        $ob = $this->getUserDetails($request);
        
        $user_pwd = trim($request->user_pwd);
        $user_name = trim($request->user_name);

        $password = bcrypt($user_pwd);

        $a = $user_2->where('user_name', '=', $user_name)->update(['user_pwd' => $password]);
        
        if($a > 0)
            $op = 1;
        
        return redirect()->back()->with('msg', $op);
    }

    public function store_apipartner(Request $request)
	{
        $user_2 = new UserAccountDetails;
        $op = 0;
        
        // Validation
        $this->validate($request, ['user_pwd' => 'required'], ['user_pwd.required' => ' The password is required.']);

        $ob = $this->getUserDetails($request);
        
        $user_pwd = trim($request->user_pwd);
        $user_name = trim($request->user_name);

        $password = bcrypt($user_pwd);

        $a = $user_2->where('user_name', '=', $user_name)->update(['user_pwd' => $password]);
        
        if($a > 0)
            $op = 1;
        
        return redirect()->back()->with('msg', $op);
    }

}
