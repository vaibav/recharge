<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Collection;

class RT_ANDR_CollectionController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                if($user_type != "API PARTNER") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            return view('android.rt_collection_details_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view_date(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                if($user_type != "API PARTNER") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            // Opening Balanace
            $o_balance = Collection::collectionOpeningBalance("2019-05-01 00:00:00", $date_1." 00:00:00", $user_name);
           
            // current collection...
            $c_tot_ce = Collection::collectionCreditTotal($f_date, $t_date, $user_name);
            $c_tot_de = Collection::collectionDebitTotal($f_date, $t_date, $user_name);
           
            $data = Collection::collectionData($user_name, $f_date, $t_date);

            $balance = floatval($o_balance) + floatval($c_tot_de);
            $balance = floatval($balance) - floatval($c_tot_ce);

            return view('android.rt_collection_details_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token, 
                                 'pay' => $data, 'obalance' => $o_balance, 'ce_tot' => $c_tot_ce, 
                                 'de_tot' => $c_tot_de, 'balance' => $balance]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
