<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;
use App\Models\NetworkLineDetails;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;

use App\Models\UserRechargeDetails;
use App\Models\UserRechargeRequestDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\BonrixApiResultDetails;

use App\Models\AdminOfferDetails;
use App\Models\UserBeneficiaryDetails;

use App\Libraries\ApiUrl;

class UserRechargeController extends Controller
{
    private $bon_url;
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;
        $ben_1 = new UserBeneficiaryDetails;

        $ob = $this->getUserDetails($request);

        //$rs = [];
        $rs = $this->getRechargeDetailsTest($ob->user);

        //print_r($rs);

        $d1 = $net_1->select('net_type_code','net_type_name')->where('net_type_status', '=', '1')->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $d4 = $ben_1->select('bn_id', 'bk_acc_no', 'bk_acc_name')->where('user_name', '=', $ob->user)
                            ->where('bk_status', '=', '1')->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('user.userrecharge', ['networktype' => $d1, 'network' => $d2, 'recharge' => $rs, 'user' => $ob, 'offer' => $off, 'in_active' => $d3, 'ben' => $d4]);
        		
    }

    public function loadData(Request $request)
    {
        $ob = $this->getUserDetails($request);
        
        $str = $this->getRechargeDetailsTest($ob->user);

        echo $str;
    }

    public function getNetworkData($net_type_code, Request $request)
    {
        $net_2 = new NetworkDetails;

        $ob = $this->getUserDetails($request);

        $d1 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $net_type_code], ['net_status', '=', 1]])->get();
        
        $rs = [];
        
        $j = 0;
        foreach($d1 as $d)
        {
            $rs[$j][0] = $d->net_code;
            $rs[$j][1] = $d->net_name;
            $j++;
        }
        
        $result['data'] = $rs;
        echo json_encode($result);
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
        $bon_1 = new BonrixApiResultDetails;
        $rech_r_2 = new UserRechargeRequestDetails;

        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();

        // Validation
        $this->validate($request, [
            'user_mobile' => 'required',
            'user_amount' => 'required'
        ],
        [
            'user_mobile.required' => ' Mobile No is required.',
            'user_amount.required' => ' Recharge Amount is required.'
        ]);

        $ob = $this->getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;
        $user_ubal = $ob->ubal;

        // Post Data
        $trans_id = "VBR".rand(10000000,99999999);
        $net_type_code = trim($request->net_type_code);
        $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);
        $user_amount = trim($request->user_amount);
        $date_time = date("Y-m-d H:i:s");

        /*
        |--------------------------------------------------------------------------
        | Recharge Process
        |--------------------------------------------------------------------------
        |   1. Check User Balance 
        |   2. Check User Mode (Enable -web)
        |   3. Check User Account status
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        // 1.Check User Balance
        if(floatval($user_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }

        // Get Network Line
        list($api_code, $v) = ApiUrl::getNetworkLine($net_code, $user_amount);
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }

        // Check Username status
        $z1 = $this->checkUserModeandStatus($user_name);
        if($z1 == 3)
        {
            $z = 3;
        }
        else if($z1 == 4)
        {
            $z = 4;
        }

        // Check Mobile Status
        $zy =$this->checkMobileStatus($user_name, $user_mobile, $user_amount);
        if ($zy == 6)
        {
            $z = 6;
        }

        // Check 10 Minutes Status
        $zy =$this->getTenMinsCheck($user_name, $user_mobile, $user_amount);
        if ($zy > 0)
        {
            $z = 7;
        }

        // Get Setup Fee
        $setup_fee = $this->getSetupFee($user_name);
        if(floatval($setup_fee) >= floatval($user_ubal))
        {
            $z = 8;     //Low user Balance...
        }

        if($z == 0)
        {
             // Echo Data
            /*echo "Transaction ID :".$trans_id."<br>"; 
            echo "Network Type Code :".$net_type_code."<br>";
            echo "Network Code :".$net_code."<br>";
            echo "Mobile No :".$user_mobile."<br>";
            echo "Recharge Amount :".$user_amount."<br>";
            echo "API Code :".$api_code."<br>";
            echo "Network Line Type :".$v."<br>";*/

            $api_url = ApiUrl::generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);

            //echo "URL :".$api_url."<br>";
            $b_url = "";
            $res_code = 0;
            $res_content = "NONE";
            $dz = 0;

            if($api_url != "NONE")
            {
                $res_code = 200;
                
                if($res_code == 200)
                {
                    // Do Payment Process
                    // Network Percentage Calculation
                    list($per, $peramt, $sur, $netamt) = $this->getRetailerPercentage($user_name, $net_code, $user_amount);
                    /*echo "<br>Retailer Network Percentage Amount<br>";
                    echo "Recharge Amount   :".$user_amount."<br>";
                    echo "Percentage        :".$per."<br>";
                    echo "Percentage Amount :".number_format($peramt,2)."<br>";
                    echo "Surplus Charge    :".number_format($sur,2)."<br>";
                    echo "Net Amount        :".number_format($netamt,2)."<br>";*/

                    // User Recharge Details
                    $rech = new stdClass();
                    $rech->trans_id = $trans_id;
                    $rech->user_name = $user_name;
                    $rech->net_code = $net_code;
                    $rech->rech_mobile = $user_mobile;
                    $rech->rech_amount = $user_amount;
                    $rech->recharge_total = $this->convertNumberFormat($netamt);
                    $rech->rech_date = $date_time;
                    $rech->rech_status = "PENDING";
                    $rech->rech_mode = "WEB";
                    $rech->api_code = $api_code;
                    $rech->trans_bid = $trans_id;
                    $rech->reply_opr_id = "";
                    $rech->reply_date = $date_time;

                    $zx = $this->insertRecharge($rech);
                    if($zx == 1)
                    {
                        // Do All other Insertions...
                        // User Recharge Request Details
                        $rech_r = new stdClass();
                        $rech_r->trans_id = $trans_id;
                        $rech_r->user_name = $user_name;
                        $rech_r->net_code = $net_code;
                        $rech_r->rech_mobile = $user_mobile;
                        $rech_r->rech_amount = $user_amount;
                        $rech_r->rech_date = $date_time;
                        $rech_r->rech_status = "PENDING";
                        $rech_r->rech_mode = "WEB";
                        $rech_r->api_code = $api_code;
                        $rech_r->rech_req_status = $res_content;

                        $zx1 = $this->insertRechargeRequest($rech_r);

                         // User Payment Details
                        $rech_p1 = new stdClass();
                        $rech_p1->trans_id = $trans_id;
                        $rech_p1->user_name = $user_name;
                        $rech_p1->net_code = $net_code;
                        $rech_p1->rech_mobile = $user_mobile;
                        $rech_p1->rech_amount = $user_amount;
                        $rech_p1->rech_net_per = $per;
                        $rech_p1->rech_net_per_amt = $this->convertNumberFormat($peramt);
                        $rech_p1->rech_net_surp = $this->convertNumberFormat($sur);
                        $rech_p1->rech_total = $this->convertNumberFormat($netamt);
                        $rech_p1->user_balance = 0;
                        $rech_p1->rech_date = $date_time;
                        $rech_p1->rech_status = "PENDING";
                        $rech_p1->rech_type = "WEB_RECHARGE";
                        $rech_p1->created_at = $date_time;
                        $rech_p1->updated_at = $date_time;

                        $zp1 = $this->insertRechargePayment($rech_p1);

                        // Parent Network Percentage Calculation
                        list($parent_type, $parent_name) = $this->getParent($user_name);
                        list($per_d, $peramt_d, $sur_d, $netamt_d) = $this->getDistributorPercentage($parent_name, $net_code, $user_amount);
                        /*echo "<br>Distributor Network Percentage Amount<br>";
                        echo "Recharge Amount   :".$user_amount."<br>";
                        echo "Percentage        :".$per_d."<br>";
                        echo "Percentage Amount :".number_format($peramt_d,2)."<br>";
                        echo "Surplus Charge    :".number_format($sur_d,2)."<br>";
                        echo "Net Amount        :".number_format($netamt_d,2)."<br>";*/

                        // Distributor Payment Details
                        $rech_p2 = new stdClass();
                        $rech_p2->trans_id = $trans_id;
                        $rech_p2->super_parent_name = "NONE";
                        $rech_p2->parent_name = $parent_name;
                        $rech_p2->user_name = $user_name;
                        $rech_p2->net_code = $net_code;
                        $rech_p2->rech_mobile = $user_mobile;
                        $rech_p2->rech_amount = $user_amount;
                        $rech_p2->rech_net_per = $per_d;
                        $rech_p2->rech_net_per_amt = $this->convertNumberFormat($peramt_d);
                        $rech_p2->rech_net_surp = $this->convertNumberFormat($sur_d);
                        $rech_p2->rech_total = $this->convertNumberFormat($netamt_d);
                        $rech_p2->user_balance = 0;
                        $rech_p2->rech_date = $date_time;
                        $rech_p2->rech_status = "PENDING";
                        $rech_p2->rech_type = "WEB_RECHARGE";
                        $rech_p2->created_at = $date_time;
                        $rech_p2->updated_at = $date_time;

                        $zp2 = $this->insertRechargePaymentDistributor($rech_p2, $parent_name);
                        $zp22 = $this->insertTransactionParent($trans_id, $parent_name);

                        list($parent_type_s, $parent_name_s) = $this->getParent($parent_name);
                        if($parent_name_s != "NONE")
                        {
                            list($per_s, $peramt_s, $sur_s, $netamt_s) = $this->getDistributorPercentage($parent_name_s, $net_code, $user_amount);
                            /*echo "<br>Super Distributor Network Percentage Amount<br>";
                            echo "Recharge Amount   :".$user_amount."<br>";
                            echo "Percentage        :".$per_s."<br>";
                            echo "Percentage Amount :".number_format($peramt_s,2)."<br>";
                            echo "Surplus Charge    :".number_format($sur_s,2)."<br>";
                            echo "Net Amount        :".number_format($netamt_s,2)."<br>";*/

                            // Super Distributor Payment Details
                            $rech_p3 = new stdClass();
                            $rech_p3->trans_id = $trans_id;
                            $rech_p3->super_parent_name = $parent_name_s;
                            $rech_p3->parent_name = $parent_name;
                            $rech_p3->user_name = $user_name;
                            $rech_p3->net_code = $net_code;
                            $rech_p3->rech_mobile = $user_mobile;
                            $rech_p3->rech_amount = $user_amount;
                            $rech_p3->rech_net_per = $per_s;
                            $rech_p3->rech_net_per_amt = $this->convertNumberFormat($peramt_s);
                            $rech_p3->rech_net_surp = $this->convertNumberFormat($sur_s);
                            $rech_p3->rech_total = $this->convertNumberFormat($netamt_s);
                            $rech_p3->user_balance = 0;
                            $rech_p3->rech_date = $date_time;
                            $rech_p3->rech_status = "PENDING";
                            $rech_p3->rech_type = "WEB_RECHARGE";
                            $rech_p3->created_at = $date_time;
                            $rech_p3->updated_at = $date_time;

                            $zp3 = $this->insertRechargePaymentDistributor($rech_p3, $parent_name_s);
                            $zp33 = $this->insertTransactionParent($trans_id, $parent_name_s);

                        }

                        $zt = $this->insertTransaction($trans_id, $user_name);
                        $op = "Recharge Process has Completed...";

                        if (strpos($api_url, 'ssh1.bonrix.in') !== false) 
                        {
                            //Bondrix URL
                            //echo "<iframe src=".$api_url." id='ab' style='display:none;'></iframe>";
                            $b_url = $api_url;
                            $res_code = 200;
                            $res_content = "done";
                            $dz = 1;
                        }
                        else
                        {
                            $client = new \GuzzleHttp\Client();
                            $res = $client->get($api_url);
                            $res_code = $res->getStatusCode();
                            $res_content =$res->getBody();
                        }

                        $rech_r_2->where('trans_id', '=', $trans_id)->update(['rech_req_status' => $res_content]);
                        
                        if($dz == 1)
                        {
                            // Bonrix URL
                            $bon_1->trans_id = $trans_id;
                            $bon_1->result_url = $b_url;
                            $bon_1->result_status = "1";
                            $bon_1->save();
                            //file_put_contents("b_url.txt", $b_url);
                            //$request->session()->put('bonrix_url', $b_url);
                            //$request->session()->put('result', [ 'output' => $op]);
                            //return view('user.test1', ['user' => $ob, 'url' => $b_url]);

                            /*$this->bon_url = $b_url;
                            //$this->bonrixUrl();
                            $client = new \GuzzleHttp\Client();
                            $res = $client->get("http://www.vaibavonline.com/vaibav/test3");
                            $res_code = $res->getStatusCode();
                            $res_content =$res->getBody();*/
                        }

                        // Check Paytriic Result
                        // SUCCESS - RESULT
                        if (strpos($res_content, 'SUCCESS') !== false) {
                            
                            $oprtr_id = $this->getOperatorId($res_content);

                            $oprtr_id = strip_tags($oprtr_id);

                            $su_url = url('/')."/pendingreport_success_1/" .$trans_id."/".$oprtr_id;

                            file_put_contents("paytric_su.txt", $su_url);

                            $client = new \GuzzleHttp\Client();
                            $res = $client->get($su_url);
                            $res_code = $res->getStatusCode();
                            $res_content_1 =$res->getBody();

                            file_put_contents("paytric_su1.txt", $res_content_1);
                        }

                        // FAILURE - RESULT
                        if (strpos($res_content, 'FAILED') !== false) {
                            
                            $oprtr_id = $this->getOperatorId($res_content);

                            $oprtr_id = strip_tags($oprtr_id);
                            
                            if($oprtr_id == "")
                                $oprtr_id = "NA";
                                
                            $fa_url = url('/')."/pendingreport_failure_1/" .$trans_id."/".$oprtr_id;

                            file_put_contents("paytric_fa.txt", $fa_url);

                            $client = new \GuzzleHttp\Client();
                            $res = $client->get($fa_url);
                            $res_code = $res->getStatusCode();
                            $res_content_1 =$res->getBody();

                            file_put_contents("paytric_fa1.txt", $res_content_1);

                        }

                        if (strpos($res_content, 'FAILURE') !== false) {
                            
                            $oprtr_id = $this->getOperatorId($res_content);

                            $oprtr_id = strip_tags($oprtr_id);
                            
                            if($oprtr_id == "")
                                $oprtr_id = "NA";

                            $fa_url = url('/')."/pendingreport_failure_1/" .$trans_id."/".$oprtr_id;

                            file_put_contents("paytric_fa.txt", $fa_url);

                            $client = new \GuzzleHttp\Client();
                            $res = $client->get($fa_url);
                            $res_code = $res->getStatusCode();
                            $res_content_1 =$res->getBody();

                            file_put_contents("paytric_fa1.txt", $res_content_1);

                        }
                        
                    }
                    // End DO payment Process
                }
                else
                {
                    $z = 5;
                }

            }
            // Url Success Ends.....

        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! Low user Balance...";
        }
        else if($z == 4)
        {   
            $op = "Error! This Account is locked...";
        }
        else if($z == 5)
        {
            $op = "Error! API Provider is Unavailable...";
        }
        else if($z == 6)
        {
            $op = "Error! Pending Status for this Mobile No & amt...";
        }
        else if($z == 7)
        {
            $op = "Error! Less than 10 Mins for this Mobile No & amt...";
        }
        else if($z == 8)
        {
            $op = "Error! Balance is reserved for Setup Fee...";
        }

        
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }

   /* public function getNetworkLine($net_code, $amt)
    {
        $net_3 = new NetworkLineDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Specific Amount
        $d1 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'SPECIFIC_AMOUNT'],  
                             ['net_line_value_1', '=', $amt], ['net_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
        else
        {
            // Range Values
            $d2 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'RANGE_VALUES'], ['net_line_status', '=', 1]])->get();

            $zx = 0;
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    $v1 = $d->net_line_value_1;
                    $v2 = $d->net_line_value_2;

                    if($v1 != "" & $v2 != "")
                    {
                        if($v1 != "*" & $v2 != "*")
                        {
                            if((floatval($amt) >= floatval($v1)) && (floatval($amt) <= floatval($v2)))
                            {
                                $api_code = $d->api_code;
                                $zx = 1;
                                $v = 2;
                                break;
                            }
                        }
                    }

                }
            }

            // All Values
            if($zx == 0)
            {
                $d3 = $net_3->where([['net_code', '=', $net_code], ['net_line_type', '=', 'ALL'], ['net_line_status', '=', 1]])->get();
                if($d3->count() > 0)
                {
                    $api_code = $d3[0]->api_code;
                    $v = 3;
                }
            }

        }

        return array($api_code, $v);
    }*/

   /* public function generateAPI($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            $url_link = $url_link."?";

            // Get Network Code Details
            $d3 = $api_3->select('api_net_short_code')->where([['api_code', '=', $api_code], ['net_code', '=', $net_code]])->get();
            if($d3->count() > 0)
            {
                $api_net_code = $d3[0]->api_net_short_code;

                 // Get Paramenters
                $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
                if($d2->count() > 0)
                {
                    foreach($d2 as $d)
                    {
                        if ($d->api_para_type == "PRE_DEFINED")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                        }
                        else if ($d->api_para_type == "MOBILE_NO")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$mobile."&";
                        }
                        else if ($d->api_para_type == "AMOUNT")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$amt."&";
                        }
                        else if ($d->api_para_type == "NETWORK")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$api_net_code."&";
                        }
                        else if ($d->api_para_type == "TRANSACTION_ID")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$trans_id."&";
                        }
                    }
                }
                else
                {
                    $zx = 1;
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }*/

    public function checkMobileStatus($user_name, $mobile, $amount)
    {
        $rech_1 = new UserRechargeDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public function getTenMinsCheck($user_name, $mobile, $amount)
    {
        $rech_1 = new UserRechargeDetails;
        $z = 0;

        $d1 = $rech_1->select('created_at')->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'SUCCESS']])->orderBy('id', 'desc')->limit(1)->get();
        if($d1->count() > 0)
        {
            $old_d = $d1[0]->created_at;
            $cur_d = date("Y-m-d H:i:s");

            $o_d = new DateTime($old_d);
            $c_d = new DateTime($cur_d);

            $dif = $o_d->diff($c_d);

            $minutes = $dif->days * 24 * 60;
            $minutes += $dif->h * 60;
            $minutes += $dif->i;

            if($minutes < 10)
                $z = 1;
        }

        return $z;
    }

    public function checkUserModeandStatus($user_name)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            if (strpos($user_rec_mode, 'WEB') !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }
            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }
        }

        return $z;
    }

    public function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public function getRetailerPercentage($user_name, $net_code, $amount)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
        if($d1->count() > 0)
        {
           $per = $d1[0]->user_net_per;
           //$sur = $d1[0]->user_net_surp;
        }

        $sur = $this->getSurplusCharge($net_code, $amount);

        // Calculation
        list($peramt1, $peramt2, $netamt) = $this->recharge_calculation($amount, $amount, $per, $sur);

        return array($per, $peramt2, $sur, $netamt);
    }

    public function getDistributorPercentage($user_name, $net_code, $amount)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
        if($d1->count() > 0)
        {
           $per = $d1[0]->user_net_per;
           //$sur = $d1[0]->user_net_surp;
        }

        $sur = $this->getSurplusCharge($net_code, $amount);

        // Calculation
        list($peramt1, $peramt2, $netamt) = $this->recharge_calculation($amount, 0, $per, $sur);

        return array($per, $peramt2, $sur, $netamt);
    }

    public function getSurplusCharge($net_code, $amount)
    {
        $net_2 = new NetworkSurplusDetails;
        $surp = 0;
        
        $d1 = $net_2->select('from_amount', 'to_amount', 'surplus_charge')
                ->where('net_code', '=', $net_code)
                ->where('user_type', '=', 'RETAILER')->get();
                
        foreach($d1 as $d)
        {
            $f_amt = $d->from_amount;
            $t_amt = $d->to_amount;

            if(floatval($f_amt) <= floatval($amount) && floatval($t_amt) >= floatval($amount))
            {
                $surp = $d->surplus_charge;
                break;
            }
        }
        
        return $surp;
    }

    public function recharge_calculation($amt, $amt1, $per, $sur)
    {
        $peramt1 = 0;
        $peramt2 = 0;

        if(floatval($per) != 0)
        {
            $peramt1 = floatval($per) / 100;
            $peramt1 = round($peramt1, 3);
    
            $peramt2 = floatval($amt) * floatval($peramt1);
            $peramt2 = round($peramt2, 2);
        }
       

        $netamt = (floatval($amt1) - floatval($peramt2)) +floatval($sur);
        $netamt = round($netamt, 2);

        return array($peramt1, $peramt2, $netamt);
    }

    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }

    //INSERT FUNCTIONS
    public function insertRecharge($rech)
    {
        $rech_1 = new UserRechargeDetails;
        $zx = 0;

        $rech_1->trans_id = $rech->trans_id;
        $rech_1->user_name = $rech->user_name;
        $rech_1->net_code = $rech->net_code;
        $rech_1->rech_mobile = $rech->rech_mobile;
        $rech_1->rech_amount = $rech->rech_amount;
        $rech_1->rech_total = $rech->recharge_total;
        $rech_1->rech_date = $rech->rech_date;
        $rech_1->rech_status = $rech->rech_status;
        $rech_1->rech_mode = $rech->rech_mode;
        $rech_1->api_code = $rech->api_code;
        $rech_1->trans_bid = $rech->trans_bid;
        $rech_1->reply_opr_id = $rech->reply_opr_id;
        $rech_1->reply_date = $rech->reply_date;

        if($rech_1->save())
        {
            $zx = 1;
        }
        
        return $zx;
    }

    public function insertRechargeRequest($rech_r)
    {
        $rech_2 = new UserRechargeRequestDetails;
        $zx = 0;

        $rech_2->trans_id =$rech_r->trans_id;
        $rech_2->user_name = $rech_r->user_name;
        $rech_2->net_code = $rech_r->net_code;
        $rech_2->rech_mobile = $rech_r->rech_mobile;
        $rech_2->rech_amount = $rech_r->rech_amount;
        $rech_2->rech_date = $rech_r->rech_date;
        $rech_2->rech_status = $rech_r->rech_status;
        $rech_2->rech_mode = $rech_r->rech_mode;
        $rech_2->api_code = $rech_r->api_code;
        $rech_2->rech_req_status = $rech_r->rech_req_status;

        if($rech_2->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertRechargePayment($rech_p1)
    {
        $rech_3 = new UserRechargePaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1->user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1->rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = $this->convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1->user_name)
                                ->update(['user_balance'=>$net_bal, 'updated_at'=>$rech_p1->updated_at]);
            
            // Insert Payment Data
            $pay_1 = array('trans_id' => $rech_p1->trans_id, 
                                'user_name' => $rech_p1->user_name, 
                                'net_code' => $rech_p1->net_code,
                                'rech_mobile' =>  $rech_p1->rech_mobile, 
                                'rech_amount' => $rech_p1->rech_amount, 
                                'rech_net_per' => $rech_p1->rech_net_per, 
                                'rech_net_per_amt' => $rech_p1->rech_net_per_amt,
                                'rech_net_surp' => $rech_p1->rech_net_surp, 
                                'rech_total' => $rech_p1->rech_total, 
                                'user_balance' => $net_bal,
                                'rech_date' => $rech_p1->rech_date,
                                'rech_status' => $rech_p1->rech_status,
                                'rech_type' => $rech_p1->rech_type,
                                'created_at' => $rech_p1->created_at, 'updated_at' => $rech_p1->updated_at);
            
            if($rech_3->insert($pay_1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public function insertRechargePaymentDistributor($rech_p1, $user_name)
    {
        $rech_4 = new UserRechargePaymentParentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1->rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = $this->convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance'=>$net_bal, 'updated_at'=>$rech_p1->updated_at]);
            
            // Insert Payment Data
            $pay_1 = array('trans_id' => $rech_p1->trans_id, 
                                'super_parent_name' => $rech_p1->super_parent_name,
                                'parent_name' => $rech_p1->parent_name,
                                'user_name' => $rech_p1->user_name, 
                                'net_code' => $rech_p1->net_code,
                                'rech_mobile' =>  $rech_p1->rech_mobile, 
                                'rech_amount' => $rech_p1->rech_amount, 
                                'rech_net_per' => $rech_p1->rech_net_per, 
                                'rech_net_per_amt' => $rech_p1->rech_net_per_amt,
                                'rech_net_surp' => $rech_p1->rech_net_surp, 
                                'rech_total' => $rech_p1->rech_total, 
                                'user_balance' => $net_bal,
                                'rech_date' => $rech_p1->rech_date,
                                'rech_status' => $rech_p1->rech_status,
                                'rech_type' => $rech_p1->rech_type,
                                'created_at' => $rech_p1->created_at, 'updated_at' => $rech_p1->updated_at);
            
            if($rech_4->insert($pay_1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public function insertTransaction($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "WEB_RECHARGE";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertTransactionParent($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "WEB_RECHARGE";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    //VIEW FUNCTIONS
    public function getRechargeDetails($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;

        $rs = [];
        $d1 = $tran_1->select('trans_id','trans_option')->where([['user_name', '=', $user_name], ['trans_type', '=', 'WEB_RECHARGE']])
                                        ->orderBy('id', 'desc')->limit(6)->get(); 
        
        $j = 0;
        foreach($d1 as $d)
        {
            $data = null;
            if($d->trans_option == '1')
            {
                $data = $rech_2->with(['userrecharge'])->where(function($query){
                                                            return $query
                                                            ->where('rech_status', '=', 'PENDING')
                                                            ->orWhere('rech_status', '=', 'SUCCESS');
                                                        })
                                                        ->where('trans_id', '=', $d->trans_id)
                                                        ->where('rech_type', '=', 'WEB_RECHARGE')->get();
                
            }
            else if($d->trans_option == '2')
            {
                $data = $rech_2->with(['userrecharge'])->where([['trans_id', '=', $d->trans_id], 
                                                        ['rech_status', '=', 'FAILURE'], ['rech_type', '=', 'WEB_RECHARGE']])
                                                        ->get();
               
            }
            
            $rs[$j][0] = $data;
            $j++;
        }
        
        return $rs;
    }

    public function bonrixUrl()
    {
        echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>";
        $xx = "<script>$.ajax({
            method: 'GET', // Type of response and matches what we said in the route
            url: 'test4', // This is the url we gave in the route
            data: {'id' : 1}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                document.writeln(response); 
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                document.writeln('AJAX error: ' + textStatus + ' : ' + errorThrown);
            }
        });</script>";
        echo $xx;
        file_put_contents("bonrix_url1.txt", $xx);
    }

    public function bonrixUrlResult(Request $request)
    {
        if($request->session()->has('bonrix_url'))
        {
            $url = $request->session()->get('bonrix_url');
            file_put_contents("bonrix_url.txt", $url);
            echo "<iframe src=".$url." id='ab' style='display:none;'></iframe>";
            $request->session()->forget('bonrix_url');
        }
       
       
        echo "Please Wait.... ";
    }


    public function getRechargeDetailsTest($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        //$rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $tran_1->with(['webrecharge1', 'webrecharge2', 'billpayment'])
                    ->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'WEB_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(7)->get(); 
        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->webrecharge2->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }
               
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->webrecharge1[0]->rech_status;
                    $u_bal = $d->webrecharge1[0]->user_balance;
                    $r_tot = $d->webrecharge1[0]->rech_total;
                    $str = $str."<tr>";
                }
                else if($d->trans_option == 2)
                {
                    $rech_status = $d->webrecharge1[1]->rech_status;
                    $u_bal = $d->webrecharge1[1]->user_balance;
                    $r_tot = $d->webrecharge1[1]->rech_total;
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                }
                if($rech_status == "PENDING" && $d->webrecharge2->rech_status == "PENDING")
                {
                    $status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $d->webrecharge2->rech_status == "FAILURE")
                {
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE" && $d->webrecharge2->rech_status == "FAILURE")
                {      
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    $o_bal = floatval($u_bal) - floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge2->rech_mobile."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge2->rech_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge1[0]->rech_net_per;
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->webrecharge1[0]->rech_net_per_amt."";
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->webrecharge1[0]->rech_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->webrecharge1[0]->rech_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge2->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge2->rech_date."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->webrecharge2->reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->webrecharge1[0]->rech_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                
                
            }                                            
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                
                if($d->trans_option == 1)
                {
                    $str = $str."<tr>";
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                }
                else if($d->trans_option == 2)
                {  
                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE" )
                {      
                    $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                

                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
               
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                if($d->trans_option == 1)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                }
                else if($d->trans_option == 2)
                {
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[1]->con_total."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                }
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                
            }   
            $str = $str."</tr>"; 

            
            
           
                                                    
            $j++;
        }
        
        return $str;
    }

    // Reprocess
    public function storeWebReprocess(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;

        $d1 = $request->all();
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "trid")
            {
                $trans_id = $value;
            }
            else if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "net_code")
            {
                $net_code = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
            else if($key == "amt")
            {
                $user_amount = $value;
            }
            
            
        }

        $ob = $this->getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;

        $date_time = date("Y-m-d H:i:s");

       /* echo "User Code :".$user_code."<BR>";
        echo "User Name :".$user_name."<BR>";
        echo "User PWD :".$user_pwd."<BR>";
        echo "User Balance :".$user_ubal."<BR>";
        echo "Operator :".$operator."<BR>";
        echo "User Mobile :".$user_mobile."<BR>";
        echo "User Amount :".$user_amount."<BR>";
        echo "User Trid :".$user_trid."<BR>";
        echo "Network Code :".$net_code."<BR>";
        echo "Network Type Code :".$net_type_code."<BR>";
        echo "Transaction Id :".$trans_id."<BR>";
        echo "date time :".$date_time."<BR>";*/

        /*
        |--------------------------------------------------------------------------
        | Recharge Process
        |--------------------------------------------------------------------------
        |   1. Check Valid User and Valid API Partner and User Account Status
        |   2. Check User Balance  
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        
        // Get Network Line
        list($api_code, $v) = $this->getNetworkLine($net_code, $user_amount);
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }

        
        
        if($z == 0)
        {
            $api_url = $this->generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);

            //echo "URL :".$api_url."<br>";

            if($api_url != "NONE")
            {
                
                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                if($res->getStatusCode() == 200)
                {
                    $op = "Recharge Process has Completed...";
                    // End DO payment Process
                }
                else
                {
                    $z = 5;
                }

            }
            // Url Success Ends.....
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else
        {
            $op = "Error! ";
        }

        $net_ubal = $this->getUserBalance($user_name);

        $resultdata = array('status' => $op, 'balance' => $net_ubal);
        
        return response()->json($resultdata, 200);

        

    }

    public function getUserAccountDetails($user_name, $user_pwd)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;
        $ob->status = 0;

        $uacc_1 = new UserAccountBalanceDetails;
        $user_2 = new UserAccountDetails;

        $user_rec_mode = "";
        $user_status = 25;

        $d2 = $user_2->select('user_code', 'user_type', 'user_rec_mode', 'user_status')->where([['user_name', '=', $user_name], ['user_pwd', '=', $user_pwd]])->get();
        foreach($d2 as $d)
        {
            $ob->user = $user_name;
            $ob->code = $d->user_code;
            $ob->mode = $d->user_type;
            $user_rec_mode = $d->user_rec_mode;
            $user_status = $d->user_status;
            $ob->status = 0;
        }
         // Check User Account Status
        if($user_status != 1)
        {
            $ob->status = 4;
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getUserBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_balance;
        }

        return $user_ubal;
    }


    public function getSetupFee($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_setup_fee')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_setup_fee;

            if($user_ubal == "")
            {
                $user_ubal = 0;
            }
        }

        return $user_ubal;
    }

    function getOperatorId($xml)
    {
        $oprtr_id = "NA";

        $arrXml = array();
        $dom    = new \DOMDocument;
        $dom->loadXML( $xml );
        foreach( $dom->getElementsByTagName( 'OPID' ) as $item ) {
            $arrXml[] = $dom->saveXML( $item );
        }

        if (sizeof($arrXml) > 0)
            $oprtr_id = $arrXml[0];

        return $oprtr_id;
    }

}
