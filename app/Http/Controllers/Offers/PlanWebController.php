<?php

namespace App\Http\Controllers\Offers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Libraries\RechargeInfo;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;

class PlanWebController extends Controller
{
    //
    public function dthOperatorCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|integer',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $connectNo = trim($request->mobile);

        if($request->has('userName')) {
            $userName  = trim($request->userName);
        }
        else {
            $userName  = "USER";
        }
        

       /* if(!$this->checkUser($userName)){
            return response()->json($result = ['status' => '2', 'message' => 'Invalid API Key....'], 200);
        }*/

        $resContent = RechargeInfo::getResult($userName, $connectNo, "857", "DTH_OPERATOR_CHECK");

        if($resContent == "0") {
            return response()->json($result = ['status' => '2', 'message' => 'Api Provider is not Set....'], 200);
        }

        $resultData     = (array)json_decode($resContent);
        $resultOperator = "";

        if (!empty($resultData)) 
        {
            if (array_key_exists('Operator', $resultData)) {

                $resultOperator = $resultData['Operator'];
                $resultOperator = strtoupper($resultOperator);
            }
            
        }

        return $resultOperator;
    }

    public function dthInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required|integer',
            'operator'   => 'required',
            
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $connectNo     = trim($request->mobile);
        $operatorShort = trim($request->operator);
        if($request->has('userName')) {
            $userName  = trim($request->userName);
        }
        else {
            $userName  = "USER";
        }

        /*if(!$this->checkUser($userName)){
            return response()->json($result = ['status' => '2', 'message' => 'Invalid API Key....'], 200);
        }*/

        $netCode = $operatorShort;

        $resContent = RechargeInfo::getResult($userName, $connectNo, $netCode, "DTH_INFO");

        echo $resContent;

        if($resContent == "0") {
            return response()->json($result = ['status' => '2', 'message' => 'Api Provider is not Set....'], 200);
        }

        $resultData   = (array)json_decode($resContent);
        
        $resultString = "";

        foreach($resultData as $planx)
        {
            foreach($planx as $key1 => $value1)
            {
                $resultString .=  '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key1.'</td>';
                $resultString .= '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.(string)$value1.'</td></tr>';
            }
           
        }

        return $resultString;
    }

    public function dthPlans(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'operator'   => 'required',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $operatorShort = trim($request->operator);
        if($request->has('userName')) {
            $userName  = trim($request->userName);
        }
        else {
            $userName  = "USER";
        }

        /*if(!$this->checkUser($userName)){
            return response()->json($result = ['status' => '2', 'message' => 'Invalid API Key....'], 200);
        }*/

        $netCode = $operatorShort;

        $resContent = RechargeInfo::getResult($userName, "-", $netCode, "DTH_PLAN");

        if($resContent == "0") {
            return response()->json($result = ['status' => '2', 'message' => 'Api Provider is not Set....'], 200);
        }

        $resultData  = (array)json_decode($resContent);
        
        $stringValue = "";

        foreach($resultData  as $planx)
        {
            $stringValue .=  '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center"></td><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center"></td></tr>';
            
            foreach($planx as $key => $value)
            {
                 $stringValue .=  '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="text-value-sm text-danger">'.$key.'</td>';

                 $stringValue .= '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$value.'</td></tr>';
            }
        }

        return $stringValue;
    }

    public function dthOffers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'operator'   => 'required',
            'mobile'     => 'required',
            'userName'   => 'required',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $operatorShort = trim($request->operator);
        $connectNo     = trim($request->mobile);
        $userName      = trim($request->userName);

        /*if(!$this->checkUser($userName)){
            return response()->json($result = ['status' => '2', 'message' => 'Invalid API Key....'], 200);
        }*/

        $netCode = $this->getNetworkCode($operatorShort);

        $resContent = RechargeInfo::getResult($userName, $connectNo, $netCode, "DTH_OFFER");

        if($resContent == "0") {
            return response()->json($result = ['status' => '2', 'message' => 'Api Provider is not Set....'], 200);
        }

        $resultData = (array)json_decode($resContent);

        return response()->json($resultData, 200);
    }

    public function dthRefresh(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'operator'   => 'required',
            'mobile'     => 'required',
            'userName'   => 'required',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $operatorShort = trim($request->operator);
        $connectNo     = trim($request->mobile);
        $userName      = trim($request->userName);

        /*if(!$this->checkUser($userName)){
            return response()->json($result = ['status' => '2', 'message' => 'Invalid API Key....'], 200);
        }*/

        $netCode = $this->getNetworkCode($operatorShort);

        $resContent = RechargeInfo::getResult($userName, $connectNo, $netCode, "DTH_REFRESH");

        if($resContent == "0") {
            return response()->json($result = ['status' => '2', 'message' => 'Api Provider is not Set....'], 200);
        }

        $resultData = (array)json_decode($resContent);

        return response()->json($resultData, 200);
    }

    public function getNetworkCode($netShort)
    {

        $networkDetails = NetworkDetails::select('net_code', 'net_type_code')->where('net_short_code', $netShort)->where('net_status', '1')->first();

        if($networkDetails) {
           return $networkDetails->net_code;
        }

        return "0";
       
    }


    public function checkUser($userName) {

        $userCheck = UserAccountDetails::where('user_name', $userName)->first();

        if($userCheck) {
            return true;
        }

        return false;
    }

    
}