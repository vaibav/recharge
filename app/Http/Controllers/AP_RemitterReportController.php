<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankRemitterDetails;

class AP_RemitterReportController extends Controller
{
    //
    public function view_remitter(Request $request)
	{
        $rem_1 = new UserBankRemitterDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $user_name = strtoupper($ob->user);

        $d1 = $rem_1->whereRaw('upper(user_name) = ?',[$user_name])->paginate(15);

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('apipartner.remitter_report_1', ['user' => $ob, 'rem' => $d1]);
        
    }

    public function view_beneficiary(Request $request)
	{
        $ben_1 = new UserBeneficiaryDetails;
        $rem_1 = new UserBankRemitterDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $user_name = strtoupper($ob->user);

        $d1 = $ben_1->whereRaw('upper(user_name) = ?',[$user_name])->paginate(15);

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        $d2 = $rem_1->get();

        return view('apipartner.beneficiary_report_1', ['user' => $ob, 'bene' => $d1, 'remitter' => $d2]);
        
    }
}
