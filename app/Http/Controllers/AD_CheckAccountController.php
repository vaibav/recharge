<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\UserAccountDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserPaymentDetails;

use App\Models\UserBankTransferDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\BackupRechargeDetails;



class AD_CheckAccountController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = UserAccountDetails::select('user_name')->where('user_type', 'RETAILER')->orderby('user_name', 'asc')->get();

        return view('admin.ad_check_acc_1', ['user' => $ob, 'users' => $d1]);
        
    }

    public function view(Request $request)
	{
        
        $op = 0;
        $j = 0;
        $k = 0;
        $date_time = date('Y-m-d H:i:s');
        $str = "";
        $time = "0 Seconds..";
        
        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, 
        	[ 'f_date' => 'required', 't_date' => 'required' ],
        	[ 'f_date.required' => ' From Date is required.', 't_date.required' => ' To Date is required.' ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $user_name = trim($request->user_name);
        $user_name = strtoupper($user_name);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";


        if($f_date != "" && $t_date != "")
        {
            $start = microtime(true);

            $d1 = UserRechargeNewDetails::whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('rech_status', 'SUCCESS')
                                ->get(); 

            $d2 = UserPaymentDetails::whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('trans_status', '=', 1)
                                ->whereBetween('grant_date', [$f_date, $t_date])
                                ->orderBy('id', 'asc')->get();

            $d3 = UserRechargeBillDetails::whereBetween('created_at', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('con_status', 'SUCCESS')
                                ->get(); 

            $d4 = UserBankTransferDetails::whereBetween('mt_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('mt_status', 'SUCCESS')
                                ->get();

            $d5 = UserBeneficiaryAccVerifyDetails::whereBetween('ben_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('ben_status', 'SUCCESS')
                                ->get();

            $i = 0;
            $st_date = $date_1;
            $n_tot = 0;
           
            for($i = 0; $i<365; $i++)
            {
            	$dt_1 = $st_date ." 00:00:00";
            	$dt_2 = $st_date ." 23:59:59";
            	$dt_d = date('Y-m-d') . " 00:00:00";

            	$ct_1 =strtotime($dt_1);
            	$ct_2 =strtotime($dt_2);
            	$ct_d =strtotime($dt_d);

            	if($ct_1 <= $ct_d)
            	{
                    // Recharge total
            		$j = 1;
	            	$s_tot = 0;
	            	$o_bal_1 = 0;
	            	$c_bal_1 = 0;
                    $o_dt_1 = $dt_2;
                    $c_dt_1 = $dt_1;
	            	foreach($d1 as $d)
	            	{
	            		if (strtotime($d->rech_date) >= $ct_1  && strtotime($d->rech_date) <= $ct_2 )
		                {
		                	if($j == 1) {
		                		$o_bal_1 = floatval($d->user_balance) + floatval($d->rech_total);
                                $o_dt_1  = $d->rech_date;
		                	}

		                	$c_bal_1 = floatval($d->user_balance);
                            $c_dt_1  = $d->rech_date;
		                   	$s_tot = floatval($s_tot) + floatval($d->rech_total);
		                   	$j++;
		                }
	            	}

                    // Eb Total
                    $k = 1;
                    $eb_tot = 0;
                    $o_bal_2 = 0;
                    $c_bal_2 = 0;
                    $o_dt_2 = $dt_2;
                    $c_dt_2 = $dt_1;
                    foreach($d3 as $d)
                    {
                        if (strtotime($d->created_at) >= $ct_1  && strtotime($d->created_at) <= $ct_2 )
                        {
                            if($k == 1) {
                                $o_bal_2 = floatval($d->user_balance) + floatval($d->con_total);
                                $o_dt_2  = $d->created_at;
                            }

                            $c_bal_2 = floatval($d->user_balance);
                            $c_dt_2  = $d->created_at;
                            $eb_tot = floatval($eb_tot) + floatval($d->con_total);
                            $k++;
                        }
                    }

                    // Money Transfer Total
                    $m = 1;
                    $mt_tot = 0;
                    $o_bal_3 = 0;
                    $c_bal_3 = 0;
                    $o_dt_3 = $dt_2;
                    $c_dt_3 = $dt_1;
                    foreach($d4 as $d)
                    {
                        if (strtotime($d->mt_date) >= $ct_1  && strtotime($d->mt_date) <= $ct_2 )
                        {
                            if($m == 1) {
                                $o_bal_3 = floatval($d->user_balance) + floatval($d->mt_total);
                                $o_dt_3  = $d->mt_date;
                            }

                            $c_bal_3 = floatval($d->user_balance);
                            $c_dt_3  = $d->mt_date;
                            $mt_tot = floatval($mt_tot) + floatval($d->mt_total);
                            $m++;
                        }
                    }

                    // Money Transfer Benificiary Verification Total
                    $mv_tot = 0;
                    $h = 1;
                    $o_bal_4 = 0;
                    $c_bal_4 = 0;
                    $o_dt_4 = $dt_2;
                    $c_dt_4 = $dt_1;
                    foreach($d5 as $d)
                    {
                        if (strtotime($d->ben_date) >= $ct_1  && strtotime($d->ben_date) <= $ct_2 )
                        {
                            if($h == 1) {
                                $o_bal_4 = floatval($d->user_bal) + floatval($d->ben_surplus);
                                $o_dt_4  = $d->ben_date;
                            }

                            $c_bal_4 = floatval($d->user_bal);
                            $c_dt_4  = $d->ben_date;
                            $mv_tot = floatval($mv_tot) + floatval($d->ben_surplus);
                            $h++;
                        }
                    }



                    // Fund Transfer Total
                    $p_tot = 0;
                    $gr_date = $dt_2;
                    $n = 1;
                    foreach($d2 as $d)
                    {
                        if (strtotime($d->grant_date) >= $ct_1  && strtotime($d->grant_date) <= $ct_2)
                        {
                            if($n ==1) {
                                $gr_date = $d->grant_date;
                            }
                            $p_tot = floatval($p_tot) + floatval($d->grant_user_amount);
                            $n++;
                        }
                    }

                    $o_bal_o = $o_bal_1;
                    $c_bal_o = $c_bal_1;
                    $o_dat_o = $dt_2;

                    //Get Opening
                    $z_1 = $this->getOpening($o_dt_1, $o_dt_2, $o_dt_3, $o_dt_4);

                    if($z_1 == 1) {
                        $o_bal_o = $o_bal_1;
                        $o_dat_o = $o_dt_1;
                    }
                    else if($z_1 == 2) {
                        $o_bal_o = $o_bal_2;
                        $o_dat_o = $o_dt_2;
                    }
                    else if($z_1 == 3) {
                        $o_bal_o = $o_bal_3;
                        $o_dat_o = $o_dt_3;
                    }
                    else if($z_1 == 4) {
                        $o_bal_o = $o_bal_4;
                        $o_dat_o = $o_dt_4;
                    }

                    $fg_1 = $this->getFundOpening($o_dat_o, $gr_date);



                    //$str = $str."---".$z_1."-".$o_bal_o."--".$o_dt_1."--".$o_dt_2."--".$o_dt_3."<br>";

                    //Get Closing
                    $z_1 = $this->getClosing($c_dt_1, $c_dt_2, $c_dt_3, $c_dt_4);

                    if($z_1 == 1) {
                        $c_bal_o = $c_bal_1;
                    }
                    else if($z_1 == 2) {
                        $c_bal_o = $c_bal_2;
                    }
                    else if($z_1 == 3) {
                        $c_bal_o = $c_bal_3;
                    }
                    else if($z_1 == 4) {
                        $c_bal_o = $c_bal_4;
                    }


                    // Calculation
                    $o_bal_c = $o_bal_o;

                    if($fg_1 == 1) {
                        $c_bal_c = (floatval($o_bal_o) + floatval($p_tot)) - (floatval($s_tot) + floatval($eb_tot) + floatval($mt_tot) + floatval($mv_tot));
                    }
                    else if($fg_1 == 2) {
                        $c_bal_c = (floatval($o_bal_o)) - (floatval($s_tot) + floatval($eb_tot) + floatval($mt_tot) + floatval($mv_tot));
                    }
                    

                    $dev = floatval($c_bal_o) - floatval($c_bal_c);
                    $dev = abs($dev);
                    $dev = round($dev, 2);

	            	$str = $str."<tr><td>".$i."</td><td>".$st_date."</td><td>".$o_bal_o."</td><td><b>".$c_bal_o."</b></td>";
                    $str = $str."<td>".$o_bal_c."</td><td>".$p_tot."</td><td>".$s_tot."</td><td>".$eb_tot."</td>";
                    $str = $str."<td>".$mt_tot."</td><td>".$mv_tot."</td><td><b>".$c_bal_c."</b></td><td>".$dev."</td></tr>";
            	}
            	else
            	{
            		break;
            	}

                //go to next date
                $st_date = date('Y-m-d', strtotime("+1 day", strtotime($st_date)));
            
            }


           
            $time = microtime(true) - $start;
            $time = round($time, 4);

        }
        

       return view('admin.ad_check_acc_2', ['user' => $ob, 'data' => $str, 'time' => $time]);
    }

    public function getOpening($d_1, $d_2, $d_3, $d_4)
    {
        $z  = 1;
        $d1 = strtotime($d_1);
        $d2 = strtotime($d_2);
        $d3 = strtotime($d_3);
        $d4 = strtotime($d_4);

        if($d1 < $d2 && $d1 < $d3 && $d1 < $d4) {
            $z = 1;
        }
        else if($d2 < $d3 && $d2 < $d4) {
            $z = 2;
        }
        else if($d3 < $d4) {
            $z = 3;
        }
        else {
           $z = 4; 
        }

        return $z;
    }

    public function getFundOpening($d_1, $d_2)
    {
        $z  = 1;
        $d1 = strtotime($d_1);
        $d2 = strtotime($d_2);

        if($d1 < $d2) {
            $z = 1;
        }
        else {
            $z = 2;
        }
       
        return $z;
    }

    public function getClosing($d_1, $d_2, $d_3, $d_4)
    {
        $z  = 1;
        $d1 = strtotime($d_1);
        $d2 = strtotime($d_2);
        $d3 = strtotime($d_3);
        $d4 = strtotime($d_4);

        if($d1 > $d2 && $d1 > $d3 && $d1 > $d4) {
            $z = 1;
        }
        else if($d2 > $d3 && $d2 > $d4) {
            $z = 2;
        }
        else if($d3 > $d4) {
            $z = 3;
        }
        else {
            $z = 4;
        }

        return $z;
    }

    
}
