<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Stock;

use App\Models\AllTransaction;

class DS_ANDR_StockController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only Distributor...
            
            return view('android.ds_stock_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Distributor";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }


    public function view_date(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only Distributor...
           /* $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $user_name);
            $o_balance = Stock::stockCustomOpeningBalance($user_name);
            $o_balance = floatval($o_balance) + floatval($o_balance1);
            
            $o_sales_re = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00",$f_date, "RECHARGE");
            $o_sales_eb = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00", $f_date, "BILL_PAYMENT");
            $o_sales_mo = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00",$f_date, "BANK_TRANSFER");
            $o_sales_mv = 0;
            $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));
            
            $old_re = Stock::getDistributorOldRecharge($user_name);

            // current sales...
            $c_sales_re = Stock::getDistributorSales($user_name, $f_date, $t_date, "RECHARGE");
            $c_sales_eb = Stock::getDistributorSales($user_name, $f_date, $t_date, "BILL_PAYMENT");
            $c_sales_mo = Stock::getDistributorSales($user_name, $f_date, $t_date, "BANK_TRANSFER");
            $c_sales_mv = 0;

            $data = Stock::stockData($user_name, $f_date, $t_date);*/

            $data = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                                   ->where(function($query) use ($user_name) {
                                        return $query
                                        ->where('parent_name', '=', $user_name)
                                        ->orWhere('user_name', '=', $user_name);
                                    })
                                    ->where(function($query) {
                                        return $query
                                        ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                        ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                        ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                                    })->orderBy('id', 'desc')->get();

            return view('android.ds_stock_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token, 
                                 'pay1' => $data]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Distributor";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
