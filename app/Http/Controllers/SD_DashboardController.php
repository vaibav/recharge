<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportSuperDistributor;

use App\Models\NetworkDetails;
use App\Models\AdminOfferDetails;

class SD_DashboardController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;

        $ob = GetCommon::getUserDetails($request);

        $rs = [];

        $str = RechargeReportSuperDistributor::getRechargeDetailsSuperDistributor($ob->user);

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('sdistributor.dashboard',  ['user' => $ob, 'recharge' => $str, 'network' => $d1, 'offer' => $off, 'in_active' => $d3]);
		
    }
}
