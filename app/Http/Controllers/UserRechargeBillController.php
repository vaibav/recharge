<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\UserRechargeBillDetails;

use App\Models\TransactionAllDetails;
use App\Models\ApiProviderDetails;

use App\Libraries\GetCommon;
use App\Libraries\ApiUrl;
use App\Libraries\RechargeBill;
use App\Libraries\ParentFailure;
use App\Libraries\ParentSuccess;
use App\Libraries\ParentNewSuccess;
use App\Libraries\ParentNewFailure;
use App\Libraries\SmsInterface;

class UserRechargeBillController extends Controller
{
    //
   
    public function viewadmin(Request $request)
	{
		
        $rech = new UserRechargeBillDetails;
        
        $net_2 = new NetworkDetails;
        //$api_1 = new ApiProviderDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        //$d3 = $api_1->select('api_code','api_name')->get();
        
        $d1 = $rech->where('con_status', '=', 'PENDING')->get(); 

        $d2 = $rech->where('con_status', '=', 'FAILURE')->get(); 
               
        return view('admin.pendingbillreport', ['user' => $ob, 'recharge_1' => $d1, 'recharge_2' => $d2, 'network' => $d2]);

    }

    public function store(Request $request)
	{
        
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 11;
        

        // Validation
        $this->validate($request, [
            'eb_cons_no' => 'required',
            'eb_cons_mobile' => 'required',
            'eb_cons_amount' => 'required',
        ],
        [
            'eb_cons_no.required' => ' Consumer No is required.',
            'eb_cons_mobile.required' => ' Consumer Mobile is required.',
            'eb_cons_amount.required' => ' Consumer Amount is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
       

        // Post Data
        $data = [];
        $data['net_code'] = trim($request->net_code);
        $data['cons_no'] = trim($request->eb_cons_no);
        $data['cons_name'] = trim($request->eb_cons_name);
        $data['cons_mobile'] = trim($request->eb_cons_mobile);
        $data['cons_amount'] = trim($request->eb_cons_amount);
        $data['cons_duedate'] = trim($request->eb_cons_duedate);
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        file_put_contents("eb_data.txt", $data['cons_no']."--".$data['cons_name']."--".$data['cons_mobile']."--".$data['cons_amount']."--".$data['cons_duedate']);

        if($data['cons_name'] == "") {
            $data['cons_name'] = "WEB_NAME";
        }

        if($data['cons_duedate'] == "") {
            $data['cons_duedate'] = "04/05/2019";
        }
        
        $z = RechargeBill::add($data);

    
        if($z == 0)
        {
            $op = "Bill Payment Process is Completed Successfully..";
            $z1 = 10;
           
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Web Mode is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! This Account is locked...";
        }
        else if($z == 4)
        {   
            $op = "Error! This Account is below from Setup Fee...";
        }
        else if($z == 5)
        {
            $op = "Error! Recharge is already Pending for this Consumer No...";
        }
        else if($z == 6)
        {
            $op = "Sorry! Server is Temporarily shut down...";
        }
       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1]);

    }

    public function success_update($trans_id, $opr_id, Request $request)
	{
		
        $rech = new UserRechargeBillDetails;
        $net_2 = new NetworkDetails;
        
        
        $ob = GetCommon::getUserDetails($request);
        

        $date_time = date("Y-m-d H:i:s");
        $status = "SUCCESS";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->con_total;
            $user_name = $d1[0]->user_name;
            $r_mode = $d1[0]->con_mode;
            $api_trans_id = $d1[0]->api_trans_id;
            $con_mobile = $d1[0]->con_mobile;
            $con_accno = $d1[0]->con_acc_no;
            $con_amount = $d1[0]->con_amount;

            
            // Update Data
            $rech->where([['trans_id', '=', $trans_id], ['con_status', '=', 'PENDING']])
                                ->update(['con_status' => $status, 
                                        'reply_opr_id' => $opr_id, 
                                        'reply_date' => $date_time, 
                                        'updated_at' => $date_time]);
            $zx++;


            // Parent Updation
            $zx = ParentNewSuccess::updateParentPaymentSuccess($trans_id, $status, $date_time);

            
            if($r_mode == "API") 
            {
                // Result API URL forwarding
                $res_code = ApiUrl::sendResultApipartner($user_name, $api_trans_id, $opr_id, $status, 1);

            }

            if($con_mobile != "")
            {
                $msg = "EB Bill Amount :".$con_amount." has been paid Successfully for Cons.No :". $con_accno;
                $zep_code = SmsInterface::callSms($user_name, $con_mobile, $msg);
            }


        }
 
        return redirect()->back()->with('msg', $zx);
       
    }

    public function failure_update($trans_id, $opr_id, Request $request)
	{
		
        $rech = new UserRechargeBillDetails;
        $net_2 = new NetworkDetails;
        
        $ob = GetCommon::getUserDetails($request);
        
        $date_time = date("Y-m-d H:i:s");
        $status = "FAILURE";
        
        $zx = 0;

        $d1 = $rech->where([['trans_id', '=', $trans_id], ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->get();
        if($d1->count() > 0)
        {
            $rech_total = $d1[0]->con_total;
            $user_name = $d1[0]->user_name;
            $r_mode = $d1[0]->con_mode;
            $api_trans_id = $d1[0]->api_trans_id;

            $net_bal = $this->updateUserAccountBalance($user_name, $rech_total);

            $rech->where([['trans_id', '=', $trans_id], ['con_status', '=', 'PENDING']])
                                        ->update(['con_option' => '2', 'updated_at' => $date_time]);

            // Array
            $rc = [];
            $rc['trans_id'] = $d1[0]->trans_id;
            $rc['api_trans_id'] = $d1[0]->api_trans_id;
            $rc['user_name'] = $user_name;
            $rc['net_code'] = $d1[0]->net_code;
            $rc['con_name'] = $d1[0]->con_name;
            $rc['con_acc_no'] = $d1[0]->con_acc_no;
            $rc['con_mobile'] = $d1[0]->con_mobile;
            $rc['con_amount'] = $d1[0]->con_amount;
            $rc['con_due_date'] = $d1[0]->con_due_date;
            $rc['con_net_per'] = $d1[0]->con_net_per;
            $rc['rech_net_per_amt'] = $d1[0]->rech_net_per_amt;
            $rc['con_net_surp'] = $d1[0]->con_net_surp;
            $rc['con_total'] = $rech_total;
            $rc['user_balance'] = $net_bal;
            $rc['con_req_status'] = "";
            $rc['con_status'] = $status;
            $rc['con_mode'] = $d1[0]->con_mode;
            $rc['con_option'] = "2";
            $rc['api_code'] = $d1[0]->api_code;
            $rc['reply_opr_id'] = $opr_id;
            $rc['reply_date'] = $date_time;
            $rc['created_at'] = $date_time;
            $rc['updated_at'] = $date_time;


            // Update Data
            $rech->insert($rc);
            $zx++;

            // Parent Update
            $zx = intval($zx) + ParentNewFailure::updateParentPaymentFailure($trans_id, $status, $date_time, "BILL_PAYMENT");

            if ($r_mode == "API")
            {
                $res_code = ApiUrl::sendResultApipartner($user_name, $api_trans_id, $opr_id, $status, 2);
            }

            $zt = $this->insertTransaction1($trans_id, $user_name);

            


        }
 
        return redirect()->back()->with('msg', $zx);
       
    }


    public function updateUserAccountBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;

        $net_bal = 0;

        $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d2->count() > 0)
        {
            $u_bal = $d2[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = $this->convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance'=>$net_bal]);

        }

        return $net_bal;

    }
    
   
    

    public function insertTransaction1($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "BILL_PAYMENT";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }


    public function getEBdetails($con_no)
    {
        $res_code = 0;
        $res_content = "";
       
        $url = "http://api.adisrecharge.org.in/ws/ws_api/view_bill/apepdcl.asp?";
        $url .= "RechMob=" . $con_no . "&ApiToken=dm9oaXRoYXBAZ21haWwuY29t";

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();
    
        echo $res_content;
    }


    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }
    

}
