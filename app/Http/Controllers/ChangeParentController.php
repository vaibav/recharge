<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserChainDetails;
use App\Models\UserAccountBalanceDetails;

class ChangeParentController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $user_1 = new UserAccountDetails;
        $user_2 = new UserPersonalDetails;
        $data = $user_1->select('user_code','user_name')->where('user_name', '!=', 'admin')
                                                        ->where('user_type', '!=', 'SUPER DISTRIBUTOR')->orderBy('user_name', 'asc')->get();
        
        $data2 = $user_2->select('user_code','user_name', 'user_per_name')->where('user_name', '!=', 'admin')->get();

        $user_code = 0;

        $ob = $this->getUserDetails($request);

        $data1 = [];
        
        return view('admin.changeparent', ['user1' => $data, 'user2' => $data1, 'user3' => $data2, 'user' => $ob, 'user_name' => '', 'user_code' => '0']);
        
		
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }


    public function getParent($user_name, $user_code, Request $request)
	{
        
        $user_1 = new UserAccountDetails;
        $user_2 = new UserPersonalDetails;

        $data = $user_1->select('user_code','user_name')->where('user_name', '!=', 'admin')
                                                        ->where('user_type', '!=', 'SUPER DISTRIBUTOR')->get();
        
        $parent_name = "NONE";
        $parent_type = "NONE";

        $d1 = $user_1->select('parent_name', 'parent_type')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_name = $d1[0]->parent_name;
            $parent_type = $d1[0]->parent_type;
        }

        $data1 = $user_1->select('user_code','user_name')->where('user_name', '!=', 'admin')
                                                        ->where('user_name', '!=', $parent_name)
                                                        ->where('user_type', '=', $parent_type)->get();
                                            
        $data2 = $user_2->select('user_code','user_name', 'user_per_name')->where('user_name', '!=', 'admin')->get();

        //echo $parent_name."--".$parent_type;
        //print_r($data1);

        $ob = $this->getUserDetails($request);
        
        return view('admin.changeparent', ['user1' => $data, 'user2' => $data1, 'user3' => $data2, 'user' => $ob, 'user_name' => $user_name, 'user_code' => $user_code]);
        
		
    }

    public function store(Request $request)
	{
        $user_1 = new UserAccountDetails;
        $user_2 = new UserPersonalDetails;
        $user_4 = new UserChainDetails;

        $ob = $this->getUserDetails($request);

         // Validation
         $this->validate($request, [
            'user_name' => 'required',
            'user_name' => 'required|without_spaces'
        ],
        [
            'user_name.required' => ' The user name is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.',
            ]);

        $user_name = trim($request->user_name);
        $user_code = trim($request->user_code);
        $parent_name = trim($request->parent_name);
        $op = 0;

         //echo $user_name."---".$parent_name."----".$user_code."<br>";

        if($parent_name != "-")
        {
           

            $parent_type = "NONE";
            $parent_code = 0;

            // get Parent Details
            $d1 = $user_1->select('user_code', 'user_type')->where('user_name', '=', $parent_name)->get();
            if($d1->count() > 0)
            {
                $parent_code = $d1[0]->user_code;
                $parent_type = $d1[0]->user_type;
            }

            //echo $parent_code."<br>";
            //echo $parent_type."<br>";

           
            // User Chain Details
            $d1 = $user_4->select('user_level', 'user_chain')->where('user_code', '=', $parent_code)->get();
            if($d1->count() > 0)
            {
                $user_level = intval($d1[0]->user_level) + 1;
                $user_chain = $d1[0]->user_chain."-".$user_code;
            }
            else
            {
                $user_level = 1;
                $user_chain = "1-".$user_code;
            }

            //echo $user_level."<br>".$user_chain."<br>";

            // Updations... 
            // User chain Update
            $user_4->where('user_code', '=', $user_code)->update(['parent_code' => $parent_code, 
                                                                  'parent_name' => $parent_name, 
                                                                  'user_level' => $user_level, 
                                                                  'user_chain' => $user_chain]);
            // Parent Details Update
            $user_1->where('user_code', '=', $user_code)->update(['parent_code' => $parent_code, 
                                                                  'parent_name' => $parent_name, 
                                                                  'parent_type' => $parent_type]);
            $op = 1;
        }
        else
        {
            //echo "Error.....";
            $op = 2;
        }

        return redirect()->back()->with('msg', $op);
        
    }

}
