<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountBalanceDetails;

class OpeningBalanceController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $user_1 = new UserPersonalDetails;

        $data = $user_1->select('user_code','user_name', 'user_per_name')->orderBy('user_name')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.admin_opening_balance_entry', ['user1' => $data, 'user' => $ob]);
        
    }


    public function store(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        
        // Validation
        $this->validate($request, [
            'user_amount' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.'
        ]);

        // Post Data
        $user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_name = trim($request->user_name);
        $user_amount = trim($request->user_amount);
        $date_time = date("Y-m-d H:i:s");
        $payment_mode = "OPENING_BALANCE";

        // Insert Data
        $cnt = $upay_1->where('user_name', '=', $user_name)->where('payment_mode', '=', $payment_mode)->count();
        if($cnt == 0)
        {
           // Amount Transfer Details....
           $upay_1->trans_id = $trans_id;
           $upay_1->user_name = $user_name;
           $upay_1->user_amount = $this->convertNumber($user_amount);
           $upay_1->user_req_date = $date_time;
           $upay_1->grant_user_name = $user_name_grnt;
           $upay_1->grant_user_amount = $this->convertNumber($user_amount);
           $upay_1->grant_date = $date_time;
           $upay_1->payment_mode = $payment_mode;
           $upay_1->user_remarks = "opening balance";
           $upay_1->trans_status = 1;
       
           $upay_1->save();


           // Transaction Details.....
           $tran_1->trans_id = $trans_id;
           $tran_1->user_name = $user_name;
           $tran_1->trans_type = $payment_mode;
           $tran_1->trans_option = '1';
        
           $tran_1->save();

           $op = "Opening Balance is added successfully...";

            
        }
        else
        {
            $upay_1->where('user_name', '=', $user_name)->where('payment_mode', '=', $payment_mode)
                    ->update(['user_amount' => $this->convertNumber($user_amount), 
                                'grant_user_amount' => $this->convertNumber($user_amount),'grant_date'=> $date_time]);

            $op = "Opening Balance is added successfully...";
        }

       
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function store_all(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        
        
        $ct1 = $uacc_1->where('user_name', '!=', "admin")->where('user_name', '!=', "SUBBURAJ")->get();

        $j = 1;

        foreach($ct1 as $cx)
        {
            // Post Data
            $user_name_grnt = "admin";
            $trans_id = rand(100000,999999);
            $user_name = $cx->user_name;
            $user_amount = $cx->user_balance;
            $date_time = date("Y-m-d H:i:s");
            $payment_mode = "OPENING_BALANCE";

            // Insert Data
            $cnt = $upay_1->where('user_name', '=', $user_name)->where('payment_mode', '=', $payment_mode)->count();
            if($cnt == 0)
            {
                // Amount Transfer Details....
               /* $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = $this->convertNumber($user_amount);
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = "opening balance";
                $upay_1->trans_status = 1;*/

                $arr1 = array('trans_id' => $trans_id, 'user_name' => $user_name, 'user_amount' => $this->convertNumber($user_amount),
                        'user_req_date' => $date_time, 'grant_user_name' => $user_name_grnt, 'grant_user_amount' => $this->convertNumber($user_amount),
                        'grant_date' => $date_time, 'payment_mode' => $payment_mode, 'user_remarks' => "opening balance", 'trans_status' => '1',
                        'created_at' => $date_time, 'updated_at' => $date_time);
            
                $upay_1->insert($arr1);


                // Transaction Details.....
               /* $tran_1->trans_id = $trans_id;
                $tran_1->user_name = $user_name;
                $tran_1->trans_type = $payment_mode;
                $tran_1->trans_option = '1';*/

                $arr2 = array('trans_id' => $trans_id, 'user_name' => $user_name, 'trans_type' => $payment_mode,
                'trans_option' => '1', 'created_at' => $date_time, 'updated_at' => $date_time);
                
                $tran_1->insert($arr2);

                $op = "Opening Balance is added successfully...";

                
            }
            else
            {
                $upay_1->where('user_name', '=', $user_name)->where('payment_mode', '=', $payment_mode)
                        ->update(['user_amount' => $this->convertNumber($user_amount), 
                                    'grant_user_amount' => $this->convertNumber($user_amount),'grant_date'=> $date_time]);

                $op = "Opening Balance is added successfully...";
            }

            echo $op."<br>";
            $j++;
        }

        

       
        echo $op;
    }

    public function view(Request $request)
	{
        
        $upay_1 = new UserPaymentDetails;

        $data = $upay_1->where('payment_mode', '=', 'OPENING_BALANCE')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.admin_opening_balance_view', ['data' => $data, 'user' => $ob]);
        
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
    
}
