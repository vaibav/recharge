<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\NetworkPackageDetails;

class AD_NetworkPackController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkDetails::select('net_code','net_name')->get();
        $d2 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        return view('admin.ad_net_pack', ['user' => $ob, 'network' => $d1, 'package' => $d2, 'pack_id' => '', 'net_code' => '', 'pack' => '']);
        
    }

    public function view($pack_id, Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkDetails::select('net_code','net_name')->get();
        $d2 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $pack = $this->get_package($pack_id, "");

        return view('admin.ad_net_pack', ['user' => $ob, 'network' => $d1, 'package' => $d2, 'pack_id' => $pack_id, 'net_code' => '', 'pack' => $pack]);
    }

    public function view_net($pack_id, $net_code, Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkDetails::select('net_code','net_name')->get();
        $d2 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $pack = $this->get_package($pack_id, $net_code);

        return view('admin.ad_net_pack', ['user' => $ob, 'network' => $d1, 'package' => $d2, 'pack_id' => $pack_id, 'net_code' => $net_code, 'pack' => $pack]);

        
    }

    public function store(Request $request)
	{
    
        $op = "No record Entered...";
        $cnt = 0;
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $trans_id = rand(10000,99999);
        $pack_id_new = "NP".rand(1000,9999);
        $pack_name_new = trim($request->pack_id_new);
        $pack_id_old = trim($request->pack_id_old);
        $net_code = trim($request->net_code);
        $from_amt = trim($request->from_amt);
        $to_amt = trim($request->to_amt); 
        $net_per = trim($request->net_per); 
        $net_surp = trim($request->net_surp);
        
       
        if($pack_id_old != '-')
        {
            $cnt = NetworkPackageDetails::where([['pack_id', '=', $pack_id_old], ['net_code', '=', $net_code],
                                                    ['from_amt', '=', $from_amt], ['to_amt', '=', $to_amt]])->count();

            if($cnt == 0)
            {
                 // Insert API Result Record... 
                $data = [ 'trans_id' => $trans_id, 'pack_id' => $pack_id_old, 'pack_name' => $this->getPackName($pack_id_old), 
                            'net_code' => $net_code, 'from_amt' => $from_amt,
                            'to_amt' => $to_amt, 'net_per' => $net_per, 'net_surp' => $net_surp, 'pack_status' => '1',
                            'created_at' => $date_time, 'updated_at' => $date_time];

                $z =  NetworkPackageDetails::insert($data);

                if($z > 0)
                {
                    $op = "Package Entry is Added Successfully....";
                }
            }
            else
            {
                $op = "Error! Record is already inserted...";
            }
        }
        else
        {
             // Insert Package Record... 
            $data = [ 'trans_id' => $trans_id, 'pack_id' => $pack_id_new, 'pack_name' => $pack_name_new, 'net_code' => $net_code, 
                            'from_amt' => $from_amt, 'to_amt' => $to_amt, 'net_per' => $net_per, 'net_surp' => $net_surp, 'pack_status' => '1',
                            'created_at' => $date_time, 'updated_at' => $date_time];

            $z =  NetworkPackageDetails::insert($data);

            if($z > 0)
            {
                $op = "Package Entry is Added Successfully....";
            }
        }
        
        return redirect()->back()->with('msg', $op);
    }

    public function change_status($trans_id, Request $request)
	{
        
        $op = "Error! No Action..";
        $stat = "1";

        $d1 = NetworkPackageDetails::select('pack_status')->where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0) {
           
            $status = $d1[0]->pack_status;
            if($status == "1") {
                $stat = "2";
            }
            else if($status == "2") {
                $stat = "1";
            }

            $z =  NetworkPackageDetails::where('trans_id', '=', $trans_id)->update(['pack_status' => $stat]);

            if($z > 0)
            {
                $op = "Package Status is Updated Successfully....";
            }
            
        }
      
        return redirect()->back()->with('msg', $op);

        
    }

    public function delete($trans_id, Request $request)
	{
        
        $op = "Error! No Action..";
        $api_code = 0;

        $d1 = NetworkPackageDetails::where('trans_id', '=', $trans_id)->get();
        
        if($d1->count() > 0) {
            // Delete Record... 
            NetworkPackageDetails::where('trans_id', '=', $trans_id)->delete();
            $op = "Record is deleted Successfully...";
            
        }
      
        return redirect()->back()->with('msg', $op);

        
    }

    /**
     * Add Methods
     */

    public function getPackName($pack_id)
	{
        $pack_name = "";

        $d1 = NetworkPackageDetails::select('pack_name')->where('pack_id',$pack_id)->get();
        
        if($d1->count() > 0) {
            $pack_name = $d1[0]->pack_name;
        }

        return $pack_name;
        
    }

    public function get_package($pack_id, $net_code)
	{

        $d1 = NetworkDetails::select('net_code','net_name')->get();

        if($net_code == "") {
            $d2 = NetworkPackageDetails::where('pack_id', $pack_id)->get();
        }
        else {
            $d2 = NetworkPackageDetails::where('pack_id', $pack_id)->where('net_code', $net_code)->get();
        }
        

        $str = "";

        $j = 1;
        foreach($d2 as $d)
        {
            $net_name = "";
            $n_code = $d->net_code;
            foreach($d1 as $e)
            {
                if($e->net_code == $n_code) {
                    $net_name = $e->net_name;
                    break;
                }
            }

            $str = $str. "<tr><td style='font-size:12px;padding:2px 8px;'>".$j."</td>";
            $str = $str. "<td style='font-size:12px;padding:2px 8px;'>".$d->trans_id."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->pack_name."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$net_name."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->from_amt."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->to_amt."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->net_per."</td>";
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->net_surp."</td>";
            if($d->pack_status == "1") {
                $str = $str. "<td style='font-size:12px;padding:5px 8px;'>ACTIVE</td>";
            }
            else {
                $str = $str. "<td style='font-size:12px;padding:5px 8px;'>INACTIVE</td>";
            }

            $str = $str. "<td style='font-size:12px;padding:2px 8px;'>";
            $str = $str. "<button class = 'btn btn-sm btn-warning' id='id_status_".$d->trans_id."' >Change Status</button>&nbsp;&nbsp;";
            $str = $str. "<button class = 'btn btn-sm btn-danger' id='id_delete_".$d->trans_id."'>Delete</button></td></tr>";
            $j++;
        }

       return $str;
        
    }

  
}
