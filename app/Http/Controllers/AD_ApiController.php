<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderNetworkDetails;
use App\Models\NetworkDetails;

class AD_ApiController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.ad_api_provider', ['user' => $ob, 'title' => 'Server Status']);
        
    }

    public function store(Request $request)
	{
    
        $op = 0;
        
        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'pr_name' => 'required', 'pr_url' => 'required',
        ],
        [
            'pr_name.required' => ' The Provier Name field is required.', 
            'pr_url.required' => ' The Provider URL field is required.' ]);

        $pr_code = rand(100,999);
        $pr_name = trim($request->pr_name);
        $pr_method = trim($request->pr_method);
        $pr_url = trim($request->pr_url); 
        
        $date_time = date("Y-m-d H:i:s");

        $data = [ 'api_tr_id' => rand(1000,9999), 'api_code' => $pr_code, 'api_name' => $pr_name, 'api_url' => $pr_url, 'api_method' => $pr_method, 
                        'api_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
        
        $z = ApiProviderDetails::insert($data);

        if($z > 0)
        {
            $op = "Api Provider URL Entry is Added Successfully....";
        }
        return redirect()->back()->with('msg', $op);
    }

    /**
     * Add Network Methods
     */

    public function index_network(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = ApiProviderDetails::orderby('api_name', 'asc')->get();
        $d2 = NetworkDetails::get();

        return view('admin.ad_api_provider_network', ['user' => $ob, 'api' => $d1, 'network' => $d2]);
        
    }

    public function get_api_network(Request $request)
	{
        
        $api_code = trim($request->api_code);
        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkDetails::get();
        $d2 = ApiProviderNetworkDetails::where('api_code', $api_code)->get();

        $str = "";

        $j = 1;
        foreach($d1 as $d)
        {
            $nt_short = "";
            $n_code = $d->net_code;
            foreach($d2 as $e)
            {
                if($e->net_code == $n_code) {
                    $nt_short = $e->api_net_short_code;
                }
            }

            $str = $str. "<tr><td style='font-size:12px;padding:2px 8px;'>";
            if($nt_short != "")
            {
                $str = $str. "<input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select[]' value = '".$d->net_code."' checked/></td>";
            }
            else
            {
                $str = $str. "<input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select[]' value = '".$d->net_code."' /></td>";

            }
            
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->net_name."</td>";

            $str = $str. "<td style='font-size:12px;padding:2px 8px;'>";
            $str = $str. "<input type='text' id='id_net_short_".$j."'  name='net_".$d->net_code."' value ='".$nt_short."' /></td></tr>";
            $j++;
        }

       echo $str;
        
    }

    public function store_network(Request $request)
	{
    
        $op = "No Entry is Added...";
        
        $ob = GetCommon::getUserDetails($request);

        $api_code = trim($request->api_code);
        $net_select = $request->net_select;
        $date_time = date("Y-m-d H:i:s");

        // Insert API Network Record... 
        $ar_2 = [];
        

        $j = 1;
        foreach($net_select as $net_code)
        {
            $n_short = trim($request['net_'.$net_code]);

            if($n_short != "")
            {
                $cnt = ApiProviderNetworkDetails::where('api_code', $api_code)->where('net_code', $net_code)->count();

                if($cnt == 0)
                {
                    $ar_1 = ['api_net_tr_id' => rand(100,999), 'api_code' => $api_code, 'net_code' => $net_code,
                    'api_net_short_code' => $n_short, 'api_net_per' => '0', 'api_net_surp' => "0", 'created_at' => $date_time, 'updated_at' => $date_time];

                    array_push($ar_2, $ar_1);
        
                    $j++;
                }
                else 
                {
                    $z = ApiProviderNetworkDetails::where('api_code', $api_code)->where('net_code', $net_code)
                                ->update(['api_net_short_code' => $n_short, 'updated_at' => $date_time]);
                    
                    $j++;
                }
            }
        }

        if($j > 1)
        {
            $z = ApiProviderNetworkDetails::insert($ar_2);

            if($z > 0)
            {
                $op = "Api Provider Network Entry is Added Successfully....";
            }
        }
 
        return redirect()->back()->with('msg', $op);
    }

}
