<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\GetCommon;
use App\Libraries\RechargeInfo;

use App\Models\UserAccountBalanceDetails;
use App\Models\OfferLineDetails;
use App\Models\RechargeInfoDetails;
use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

class UserRechargeOfferController extends Controller
{
    //
    public function getOffer(Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $net_code = "0";
        $mob = "0";

        $ob = GetCommon::getUserDetails($request);

        // Post Data from website
        if ($request->has('net_type_code')) {
            $net_type_code = trim($request->net_type_code);
        }
        if ($request->has('net_code')) {
            $net_code = trim($request->net_code);
        }
        if ($request->has('user_mobile')) {
            $mob = trim($request->user_mobile);
        }

        // Post Data from Android
        if ($request->has('net_code_1')) {
            $net_code = trim($request->net_code_1);
        }
        if ($request->has('user_mobile_1')) {
            $mob = trim($request->user_mobile_1);
        }
        

        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "121_OFFER";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = json_decode($jx);

            file_put_contents(base_path().'/public/sample/121_result.txt', $res_content, FILE_APPEND);
            
            $str = '';
            if (!empty($dx)) 
            {
                if( isset( $dx->records ) )
                {
                    // do something
                    foreach($dx->records as $c)
                    {
                        $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">';
                        $str = $str . '<label><input type="radio" name="group1"  id="id_off" value="'.$c->rs.'" /> <span> '.$c->rs.' </span> </label></td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$c->desc.'</td></tr>';
                      
                    
                    }
                }
                
            }


            


            $z = 20;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob]);
    }

    public function getOffer1(Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $net_code = "0";
        $mob = "0";

        $ob = GetCommon::getUserDetails($request);

        // Post Data from website
        if ($request->has('net_type_code')) {
            $net_type_code = trim($request->net_type_code);
        }
        if ($request->has('net_code')) {
            $net_code = trim($request->net_code);
        }
        if ($request->has('user_mobile')) {
            $mob = trim($request->user_mobile);
        }

        // Post Data from Android
        if ($request->has('net_code_1')) {
            $net_code = trim($request->net_code_1);
        }
        if ($request->has('user_mobile_1')) {
            $mob = trim($request->user_mobile_1);
        }
        
        file_put_contents(base_path().'/public/sample/121_data.txt', $net_code."--".$mob);
        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }

        file_put_contents(base_path().'/public/sample/121_api_code.txt', $api_code);
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            file_put_contents(base_path().'/public/sample/121_api_url.txt', $api_url);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "121_OFFER";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/121_result.txt', $res_content);
            
            $str = '';
            if (!empty($dx)) 
            {

                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
                    
                    $str = $str . "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($data as $d)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$d->desc.'</td>';
                        $str = $str . '<td style = "font-size:14px;padding:4px 4px;"><a class="btn-floating center-align #7b1fa2 blue darken-2" id="id_off">'.$d->rs.'</a>';
                        $str = $str . '</td></tr>';

                       
                    }
                    $str = $str . "</tbody>";
                }


                
                
            }

           

            $z = 20;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob]);
    }

    public function getOfferMobile($net_code, $mob, Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        
        $ob = GetCommon::getUserDetails($request);

        file_put_contents(base_path().'/public/sample/121_data.txt', $net_code."--".$mob);
        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }

        file_put_contents(base_path().'/public/sample/121_api_code.txt', $api_code);
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            file_put_contents(base_path().'/public/sample/121_api_url.txt', $api_url);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "M".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "121_OFFER";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/121_result.txt', $res_content);
            
            $str = '';
            if (!empty($dx)) 
            {

                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
                    
                    $str = $str . "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($data as $d)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$d->desc.'</td>';
                        $str = $str . '<td style = "font-size:14px;padding:4px 4px;">
                        <a class="floating btn-small waves-effect waves-light #ff6d00 blue accent-4 white-text" 
                        id="id_off" style="padding:5px 5px;">'.$d->rs.'</a>';
                        $str = $str . '</td></tr>';

                       
                    }
                    $str = $str . "</tbody>";
                }


                
                
            }

           

            $z = 20;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        echo $op;
    }


    public function getMobilePlans(Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $net_code = "0";
        $mob = "0";

        $ob = GetCommon::getUserDetails($request);

        // Post Data from website
        if ($request->has('net_code')) {
            $net_code = trim($request->net_code);
        }
       
        // Post Data from Android
        if ($request->has('net_code_1')) {
            $net_code = trim($request->net_code_1);
        }
       
        
        file_put_contents(base_path().'/public/sample/mobile_plan.txt', $net_code, FILE_APPEND);
        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "MOBILE_PLAN");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "MOBILE_PLAN";
            $ri->rech_mobile = "0";

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/mobile_plan.txt', $res_content, FILE_APPEND);
            
            $str = '';
            if (!empty($dx)) 
            {
                if (array_key_exists('RDATA', $dx)) {
                    $data = (array)$dx['RDATA'];
                    
                    $str = $str . "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($data as $key => $value)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;font-weight:bold;background-color: #E8DAEF">'.$key.'</td><td style = "background-color: #E8DAEF"></td>';

                        foreach($value as $v)
                        {
                            $str = $str . '<tr>';
                            $sx = "";
                            $sy = "";
                            foreach($v as $k1 => $v1)
                            {
                               
                                if($k1 == "desc")
                                {
                                    $sx = '<td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$v1.'</td>';
                                }
                                else if($k1 == "rs")
                                {
                                    $sy = '<td style = "font-size:14px;padding:4px 4px;"><a class="btn-floating center-align #7b1fa2 blue darken-2" id="id_off">'.$v1.'</a></td>';
                                }
                                
                                
                                
                            }
                            $str = $str . $sx.$sy. '</tr>';
                        }
                    }
                    
                }
                
                
            }
            

            $z = 22;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob]);
    }

    public function getMobilePlansMobile($net_code, Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $mob = "0";

        $ob = GetCommon::getUserDetails($request);

        file_put_contents(base_path().'/public/sample/mobile_plan.txt', $net_code, FILE_APPEND);
        
        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "MOBILE_PLAN");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "M".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "MOBILE_PLAN";
            $ri->rech_mobile = "0";

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/mobile_plan.txt', $res_content, FILE_APPEND);
            
            $str = '';
            if (!empty($dx)) 
            {
                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
                    
                    $str = $str . "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($data as $key => $value)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;font-weight:bold;background-color: #E8DAEF">'.$key.'</td><td style = "background-color: #E8DAEF"></td>';

                        foreach($value as $v)
                        {
                            $str = $str . '<tr>';
                            $sx = "";
                            $sy = "";
                            foreach($v as $k1 => $v1)
                            {
                               
                                if($k1 == "desc")
                                {
                                    $sx = '<td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$v1.'</td>';
                                }
                                else if($k1 == "rs")
                                {
                                    $sy = '<td style = "font-size:14px;padding:4px 4px;">
                                    <a class="floating btn-small waves-effect waves-light #ff6d00 blue accent-4 white-text" 
                                    id="id_off">'.$v1.'</a></td>';
                                }
                                
                                
                                
                            }
                            $str = $str . $sx.$sy. '</tr>';
                        }
                    }
                    
                }
                
                
            }
            

            $z = 22;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        echo $op;
    }

    public function getOperatorCheck($mob, Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $net_code = "388";

        $ob = GetCommon::getUserDetails($request);

      
        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OPERATOR_CHECK");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            file_put_contents(base_path().'/public/sample/operator_check_url.txt', $api_url);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "OPERATOR_CHECK";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/operator_check.txt', $res_content);
            
            $str = '';
            if (!empty($dx)) 
            {
                if (array_key_exists('Operator', $dx)) {

                    $str = $dx['Operator'];
                    
                }
                
            }
            

            $z = 23;

            $op = $str;
        }

        echo $op;
    }

    public function getDthinfo(Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
        $net_code = "0";
        $mob = "0";


        $ob = GetCommon::getUserDetails($request);

         // Post Data from website
         if ($request->has('net_type_code')) {
            $net_type_code = trim($request->net_type_code);
        }
        if ($request->has('net_code')) {
            $net_code = trim($request->net_code);
        }
        if ($request->has('user_mobile')) {
            $mob = trim($request->user_mobile);
        }

        // Post Data from Android
        if ($request->has('net_code_2')) {
            $net_code = trim($request->net_code_2);
        }
        if ($request->has('user_mobile_2')) {
            $mob = trim($request->user_mobile_2);
        }

        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "DTH_INFO");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "DTH_INFO";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/dth_result.txt', $res_content, FILE_APPEND);
            
            $str = '';
            if (array_key_exists('records', $dx)) {
                $data = (array)$dx['records'];
                
                foreach($data as $key => $value)
                {
                    foreach($value as $key1 => $value1)
                    {
                        $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key1.'</td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.(string)$value1.'</td></tr>';
                    }
                   
                }
                $str = $str . "";
            }


           
           

            $z = 20;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        echo $op;

        //return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob]);
    }

    public function getDthinfo1($mob, $net_code, Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $net_type_code = "0";
      
        $ob = GetCommon::getUserDetails($request);

     

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "DTH_INFO");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No plans Available in this Network...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $ri->trans_id = "W".rand(10000,99999);
            $ri->user_name = $ob->user;
            $ri->rech_type = "DTH_INFO";
            $ri->rech_mobile = $mob;

            $ri->save();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);

            $dx = (array)json_decode($jx);

            file_put_contents(base_path().'/public/sample/dth_result.txt', $res_content, FILE_APPEND);
            
            $str = '';
            if (array_key_exists('records', $dx)) {
                $data = (array)$dx['records'];
                
                foreach($data as $key => $value)
                {
                    foreach($value as $key1 => $value1)
                    {
                        $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key1.'</td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.(string)$value1.'</td></tr>';
                    }
                    
                    

                   
                }
                $str = $str . "";
            }


           
           

            $z = 20;

            if($str == "")
                $str = "No Plan is available in this Network..";

            $op = $str;
        }

        echo $op;

        //return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob]);
    }

    public function getEbinfo(Request $request)
	{
        $ri = new RechargeInfoDetails;
        $op = "NONE";
        $z = 0;
        $opr = "NONE";
        $cus_name = "NONE";
            $cus_amnt = 0;
            $cus_dued = "";

        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $net_type_code = trim($request->net_type_code);
        $net_code = trim($request->net_code);
        $mob = trim($request->eb_cons_no);

        

        list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "BILL_CHECK");
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
            $op = "No Details Available in this No...";
        }
        
        if($z == 0)
        {
            $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $mob);

            file_put_contents(base_path().'/public/sample/eb_api_url.txt', $api_url, FILE_APPEND);

            $client = new \GuzzleHttp\Client();
            $res = $client->get($api_url);
            $res_code = $res->getStatusCode();
            $res_content = (string) $res->getBody();

            $jx = str_replace("\\\\\"", "###dq###", $res_content);
            $jx = str_replace("\\", "", $jx);
            $jx = str_replace("###dq###", "\\\"", $jx);
            $jx = str_replace("\n", " ", $jx);
            
            file_put_contents(base_path().'/public/sample/eb_info.txt', $res_content, FILE_APPEND);

           

            $dx = (array)json_decode($jx);

            
            $str = '';
            if (array_key_exists('records', $dx)) {
                $data = (array)$dx['records'];

                file_put_contents(base_path().'/public/sample/eb_data.txt',print_r($data,true), FILE_APPEND);
                if (!empty($data)) 
                {
                    $data = (array)$data[0];
                    $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Bill Amount</td>';
                    $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$data['Billamount'].'</td></tr>';

                    
                    $cus_amnt = trim($data['Billamount']);

                    foreach($data as $key => $value)
                    {
                        if (strpos($key, 'Name') !== false) {
                            $cus_name = trim($value);
                        }
                        if (strpos($key, 'Due') !== false) {
                            $cus_dued = trim($value);
                        }

                        $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key.'</td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$value.'</td></tr>';

                    
                    }
                    
                    
                }
                else
                {
                    if (array_key_exists('Message', $dx)) {
                        $msg = $dx['Message'];
                        $msg = $this->resultFormatter($msg);
                        $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Message</td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$msg.'</td></tr>';
                    }
                }

                
               
                $str = $str . "";
            }

            

            $z = 21;

            if($str == "")
                $str = "Error! No Details are available in this No..";

            $op = $str;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'net_code' => $net_code, 'net_type' => $net_type_code, 'mob' => $mob, 'name' => $cus_name, 'amount' => $cus_amnt, 'due' => $cus_dued]);
    }

   
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('admin.admin_recharge_info', ['user' => $ob]);
        
		
    }

    public function viewdate(Request $request)
    {
        
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $data = $ri->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

        return view('admin.admin_recharge_info_view', ['info' => $data, 'user' => $ob]); 
        
    }

    public function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);
        $res = str_replace("'", "", $res);
        $res = str_replace("(", " ", $res);
        $res = str_replace(")", " ", $res);

        return $res;
    }


}
