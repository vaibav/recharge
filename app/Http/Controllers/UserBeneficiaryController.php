<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\Beneficiary;

use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankRemitterDetails;
use App\Models\UserBankAgentDetails;

class UserBeneficiaryController extends Controller
{
    //
    public function index(Request $request)
	{
        $rem_1 = new UserBankRemitterDetails;

        $agen_1 = new UserBankAgentDetails;

        $agent_id = 0;
        $msisdn = "0";
        $rem_name = "NONE";

        $ob = GetCommon::getUserDetails($request);

        if($request->session()->has('customer_msisdn'))
        {
            $msisdn = $request->session()->get('customer_msisdn');
            $res_data = $request->session()->get('result_data');
            $rem_name = $this->getRemitterName($msisdn, $ob->user);
            list($data, $beni) = $this->fetchCustomerDetails($res_data);
        }

        $d1 = $rem_1->where('user_name', '=', $ob->user)->where('msisdn', '=', $msisdn)
                                ->where('rem_status', '=', 'SUCCESS')->orderBy('user_rem_name', 'asc')->get();

        //$d2 = $agen_1->select('agent_id')->where('user_name', '=', $ob->user)->get();

        /*if($d2->count() > 0)
        {
            $agent_id = $d2[0]->agent_id;
        }*/

        return view('user.ben_entry', ['user' => $ob, 'remitter' => $d1, 'agent_id' => $agent_id, 'msisdn' => $msisdn, 'rem_name' => $rem_name, 'data' => $data]);
    
    }

    public function index_otp(Request $request, $ben_acc_no)
	{
        $ben_1 = new UserBeneficiaryDetails;

        $ob = GetCommon::getUserDetails($request);

        $msisdn = "0";
        $rem_name = "NONE";

        /*if($request->session()->has('customer_msisdn'))
        {
            $msisdn = $request->session()->get('customer_msisdn');
            $rem_name = $this->getRemitterName($msisdn, $ob->user);
        }*/
        

        //echo $ob->user;

        $d1 = $ben_1->where('user_name', '=', $ob->user)->where('ben_acc_no', '=', $ben_acc_no)
                            ->get();

        //print_r($d1);

        return view('user.ben_otp_entry', ['user' => $ob, 'beneficiary' => $d1, 'msisdn' => $msisdn, 'rem_name' => $rem_name]);
    
    }

    public function getRemitterName($msisdn, $user_name)
    {
        $rem_1 = new UserBankRemitterDetails;

        $rem_name = "NONE";

        $d1 = $rem_1->select('user_rem_name')
                    ->where([['user_name', '=', $user_name], ['msisdn', '=', $msisdn]])->get();
        
        foreach($d1 as $d)
        {
            $rem_name = $d->user_rem_name;
        }
        
        return $rem_name;
    }

    public function store(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";
        $trans_id = "0";
        $ben_id = "0";

        $this->validate($request, ['ben_acc_no' => 'required'
                                ], 
                                ['ben_acc_no.required' => ' Account No is required.'
                                
                                ]);

        $agent_id = $this->getAgentId($ob->user);
        $ben_acc_no = trim($request->ben_acc_no);

        $data = ['user_name' => $ob->user,
                    'msisdn' => trim($request->c_msisdn),
                    'beneficiary_account_no' => $ben_acc_no,
                    'beneficiary_name' => trim($request->ben_acc_name),
                    'beneficiary_bank_code' => trim($request->bank_code),
                    'beneficiary_ifsc_code' => trim($request->bank_ifsc),
                    'api_trans_id' => '-',
                    'r_mode' => 'WEB'
            ];

        list($z, $msg, $trans_id, $ben_id, $res) = Beneficiary::add($data);

        if($z == 0)
            $op = $msg;
        else if($z == 1)
            $op = "Mode web is not Selected...";
        else if($z == 2)
            $op = "Account is Inactivated...";
        else if($z == 3)
            $op = "Account No is Already Registered...";
        else if($z == 4)
            $op = $msg;
        else if($z == 5)
            $op = "PENDING...";
       
         return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'ben_acc_no' => $ben_acc_no, 'trans_id' => $trans_id, 'ben_id' => $ben_id]);
	
    }

    public function store_otp($ben_id, $msisdn, $trans_id, $otp, Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z1 = 0;
        $z = 0;
        $op = "";

        $data = ['user_name' => $ob->user,
                'ben_id' => $ben_id,
                'msisdn' => $msisdn,
                'trans_id' => $trans_id,
                'otp' => $otp,
                'r_mode' => "WEB"
                ];
        
        //file_put_contents("bank_details.txt", print_r($data, true), FILE_APPEND);

        list($z, $msg, $res) = Beneficiary::add_otp($data);

        if($z == 0)
        {
            $op = $msg;
            $z1 = 10;
            return redirect('bank_remitter_direct_check');
        }
        else if($z == 1)
        {
            $op = "Server is Temporariy Shutdown...";
            $z1 = 11;
        }
        else if($z == 2)
        {
            $op = "Mode web is not Selected...";
            $z1 = 12;
        }
        else if($z == 3)
        {
            $op = "Account is Inactivated...";
            $z1 = 13;
        }
        else if($z == 4)
        {
            $op = "Already Registered...";
            $z1 = 14;
        }
        else if($z == 5)
        {
            $op = $msg;
            $z1 = 15;
        }
        else if($z == 6)
        {
            $op = "Wait! Pending....";
            $z1 = 16;
        }
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1, 'ben_acc_no' => '', 'trans_id' => '', 'ben_id' => '']);
	
    }

    public function send_otp($ben_id, $msisdn, $trans_id, Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $z1 = 0;
        $op = "";
        $trans_id_r = "";

        $data = ['user_name' => $ob->user,
                'msisdn' => trim($request->msisdn),
                'trans_id' => trim($request->trans_id),
                'ben_id' => trim($request->ben_id),
                'r_mode' => "WEB"
                ];
        

        list($z, $msg, $trans_id_r) = Beneficiary::resend_otp($data);

        $z1 = 0;
        if($z == 10)
        {
            $op = $msg;
            $z1 = 20;
        } 
        else if($z == 1)
        {
            $op = "Server is Temporariy Shutdown...";
            $z1 = 21;
        }
        else if($z == 2) 
        {
            $op = "Mode web is not Selected...";
            $z1 = 22;
        }
        else if($z == 3)
        {
            $op = "Account is Inactivated...";
            $z1 = 23;
        }
        else if($z == 4)
        {
            $op = "Already Registered...";
            $z1 = 24;
        }
        else if($z == 5)
        {
            $op = $msg;
            $z1 = 25;
        }
        else if($z == 6)
        {
            $op = "Wait! Pending....";
            $z1 = 26;
        }
        else
        {
            $op = "Unable to send otp...";
            $z1 = 27;
        }
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1, 'trans_id_r' => $trans_id_r, 'msisdn' => $msisdn, 'ben_id' => $ben_id]);
	
    }

    public function check_account(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

 
        $data = ['user_name' => $ob->user,
                    'msisdn' => trim($request->agent_msisdn),
                    'beneficiary_account_no' => trim($request->ben_acc_no),
                    'beneficiary_bank_code' => trim($request->bank_code),
                    'beneficiary_ifsc_code' => trim($request->bank_ifsc),
                    'client_trans_id' => rand(100000,999999),
                    'api_trans_id' => '-',
                    'r_mode' => 'WEB'
            ];
        
        file_put_contents("bank_details.txt", print_r($data, true));

        list($z, $msg, $ben_name, $res) = Beneficiary::check_ben_account($data);

        $result = ['status' => $z, 'ben_name' => $ben_name, 'message' => $msg];
       
       

        return response()->json($result); 
	
    }

    public function delete($ben_acc_no, $msisdn, $beneficiary_id, Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $z1 = 0;
        $op = "";
        $trans_id = "0";
        $agent_id = "0";
        $otp_status = "0";

        $ben_id = $this->getBendId($ben_acc_no, $msisdn, $ob->user);

        $data = ['user_name' => $ob->user,
                    'msisdn' => $msisdn,
                    'ben_id' => $ben_id,
                    'beneficiary_id' => $beneficiary_id,
                    'r_mode' => 'WEB'
            ];

        //echo $ben_id;
        file_put_contents(base_path().'/public/sample/ben_1.txt', $ob->user."--".$msisdn."--".$ben_id."--".$ben_acc_no);

        list($z, $msg, $trans_id, $otp_status, $res) = Beneficiary::delete($data);

        if($z == 0) {
            $op = $msg;
            $z1 = 40;
        }
        else if($z == 1) {
            $op = "Mode web is not Selected...";
            $z1 = 41;
        }
        else if($z == 2) {
            $op = "Account is Inactivated...";
            $z1 = 42;
        }  
        else if($z == 3) {
            $op = "Account No is Already Registered...";
            $z1 = 43;
        }
        else if($z == 4) {
            $op = $msg;
            $z1 = 44;
        } 
        else if($z == 5) {
            $op = "PENDING...";
            $z1 = 45;
        }
           

       
         return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1, 'msisdn' => $msisdn, 'ben_acc_no' => $ben_acc_no, 'trans_id' => $trans_id, 'ben_id' => $ben_id, 'rep_trans_id' => "", 'otp_status' => $otp_status, 'remarks' => '']);

         
	
    }

    public function delete_otp($trans_id, $msisdn, $otp, Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z1 = 0;
        $z = 0;
        $op = "";

       


        $data = ['user_name' => $ob->user,
                'msisdn' => $msisdn,
                'trans_id' => $trans_id,
                'otp' => $otp,
                'r_mode' => "WEB"
                ];
        
        file_put_contents("bank_details.txt", print_r($data, true));

        list($z, $msg, $res) = Beneficiary::delete_otp($data);

        if($z == 0)
        {
            $op = $msg;
            $z1 = 50;
        }
        else if($z == 1)
        {
            $op = "Server is Temporariy Shutdown...";
            $z1 = 51;
        }
        else if($z == 2)
        {
            $op = "Mode web is not Selected...";
            $z1 = 52;
        }
        else if($z == 3)
        {
            $op = "Account is Inactivated...";
            $z1 = 53;
        }
        else if($z == 4)
        {
            $op = "Already Registered...";
            $z1 = 54;
        }
        else if($z == 5)
        {
            $op = $msg;
            $z1 = 55;
        }
        else if($z == 6)
        {
            $op = "Wait! Pending....";
            $z1 = 56;
        }
       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1, 'ben_acc_no' => "", 'trans_id' => "", 'ben_id' => "", 'rep_trans_id' => "", 'otp_status' => "", 'remarks' => '']);
	
    }


    public function view_one(Request $request)
    {
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $msisdn = "0";
        $rem_name = "NONE";

        

        $d1 = Beneficiary::view_user1($ob->user, $msisdn);

        $d2 = $rem_1->where('user_name', '=', $ob->user)->orderBy('user_rem_name', 'asc')->get();

        $agent_id = $this->getAgentId($ob->user);

        $status = "PENDING";

        foreach($d1 as $d)
        {
            if($d->ben_status == "SUCCESS")
            {
                $status = "SUCCESS";
                break;
            }
        }

        return view('user.ben_view', ['user' => $ob, 'data' =>$d1, 'remitter' => $d2, 'agent_id' => $agent_id, 'status' => $status, 'msisdn' => $msisdn, 'rem_name' => $rem_name]);

    }

    public function view_all(Request $request)
    {

        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = Beneficiary::view_all();

        $d2 = $rem_1->orderBy('user_rem_name', 'asc')->get();

        return view('admin.ben_view_all', ['user' => $ob, 'data' =>$d1, 'remitter' => $d2]);

    }

    public function checkAccountNo($ben_acc_no, $msisdn, Request $request)
    {
        $user_1 = new UserBeneficiaryDetails;
        $cnt = 0;

        $cnt = $user_1->where('ben_acc_no', '=', $ben_acc_no)->where('msisdn', '=', $msisdn)
                        ->where(function($query){
                            return $query
                            ->where('ben_status', '=', 'SUCCESS')
                            ->orWhere('ben_status', '=', 'OTP SUCCESS');
                        })->count();

        echo $cnt;
    }

    public function update_success(Request $request, $bn_id, $opr_id)
    {

        $ob = GetCommon::getUserDetails($request);

        $z = Beneficiary::update($bn_id, $opr_id, "SUCCESS");

        if($z == 1)
            $op = "Entry is Updated Successfully...";
        else if($z == 2)
            $op = "Invalid Entry...";
        else
            $op = "No Entry is Added...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function update_failure(Request $request, $bn_id, $opr_id)
    {

        $ob = GetCommon::getUserDetails($request);

        $z = Beneficiary::update($bn_id, $opr_id, "FAILURE");

        if($z == 1)
            $op = "Entry is Updated Successfully...";
        else if($z == 2)
            $op = "Invalid Entry...";
        else
            $op = "No Entry is Added...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public static function getAgentId($user_name)
    {
        $agen_1 = new UserBankAgentDetails;
        
        $agent_id = 0;

        $d1 = $agen_1->where('user_name', '=', $user_name)
                        ->where('agent_status', '=', 'SUCCESS')->get();

        if($d1->count() > 0)
        {
            $agent_id = $d1[0]->agent_id;
        }

        return $agent_id;
    }

    public function getBendId($ben_acc_no, $msisdn, $user_name)
    {
        $user_1 = new UserBeneficiaryDetails;
        $ben_id = 0;

        $d1 = $user_1->where('ben_acc_no', '=', $ben_acc_no)->where('msisdn', '=', $msisdn)->where('user_name', '=', $user_name)->get();
          
        if($d1->count() > 0)
        {
            $ben_id = $d1[0]->ben_id;
        }

        return $ben_id;
    }

    public function fetchCustomerDetails($res_content)
    {
       
        $data = [];

        $result = $this->resultFormatter($res_content);

        $result = json_decode($result, true);

        $data['msisdn'] = "";
        $data['user_name'] = "";
        $data['total_limit'] = "";
        $data['consume_limit'] = "";
        $data['remaining_limit'] = "";
        $data['beneficiary'] = "";
        $beni = [];

        foreach($result as $key => $value)
        {
            if($key == "msisdn")
            {
                $data['msisdn'] = $value;
            }
            else if($key == "user_name")
            {
                $data['user_name'] = $value;
            }
            else if($key == "total_limit")
            {
                $data['total_limit'] = $value;
            }
            else if($key == "consume_limit")
            {
                $data['consume_limit'] = $value;
            }
            else if($key == "remaining_limit")
            {
                $data['remaining_limit'] = $value;
            }
            else if($key == "beneficiary")
            {
                $data['beneficiary'] = $value;
            }
        }
        
        $result1 = $this->resultFormatter($data['beneficiary']);

        //$result1 = json_decode($result1, true);

        if(!empty($result1)) 
        {
            foreach($result1 as $d)
            {
                if(!empty($d)) 
                {
                    $b_id = "";
                    $b_name = "";
                    $b_accno = "";
                    $b_bank = "";
                    $b_bcode = "";
                    $b_ifsc = "";

                    foreach($d as $key => $value)
                    {
                        if($key == "beneficiary_id")
                        {
                            $b_id = $value;
                        }
                        else if($key == "beneficiary_name")
                        {
                            $b_name = $value;
                        }
                        else if($key == "beneficiary_account_no")
                        {
                            $b_accno = $value;
                        }
                        else if($key == "beneficiary_bank_name")
                        {
                            $b_bank = $value;
                        }
                        else if($key == "beneficiary_bank_code")
                        {
                            $b_bcode = $value;
                        }
                        else if($key == "beneficiary_ifsc_code")
                        {
                            $b_ifsc = $value;
                        }
                    }

                    if($b_id != "" && $b_accno != "")
                    {
                        array_push($beni, ['b_id' => $b_id, 'b_name' => $b_name, 'b_accno' => $b_accno, 'b_bank' => $b_bank, 'b_bcode' => $b_bcode, 'b_ifsc' => $b_ifsc]);
                    }
                }
               
            }
            
        }

         
        return array($data, $beni);
    }

    public static function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }

}
