<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;

use App\Libraries\RechargeNewReMobile;
use App\Libraries\GetCommon;
use App\Libraries\RechargeNewReply;
use App\Libraries\BankRemitter;

use App\Models\AllTransaction;

use App\Libraries\TransactionDirectReply;

class PendingReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $rech_n_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $d15 = $rech_n_1->with(['request'])->where([['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 
       
        return view('admin.pendingreport', ['user' => $ob, 'new_recharge' => $d15, 'network' => $d2, 'api' => $d3]);

    }

    public function failureUpdate($trans_id, $opr_id, Request $request)
	{

        $rech_n_1 = new AllTransaction;
        
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = $rech_n_1->select('user_name', 'api_trans_id', 'api_code')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            //$mode = $d1[0]->rech_mode;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }

            //$zx = RechargeNewReply::updateFailure($trans_id, $opr_id, "FAILURE", $date_time);

            $zx = TransactionDirectReply::failureUpdate($trans_id, $opr_id);

           /* if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = $this->getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", "FAILURE", $api_f_url);
                    }

                    if($api_f_url != "")
                    {
                        BankRemitter::runURL($api_f_url, "RESPONSE");
                    }
                }

                $z = 0;
                $op = "Failure Update is executed successfully...";
            }*/

            if($zx == 0)
            {
                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }

       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
       
    }

    

    public function successUpdate($trans_id, $opr_id, Request $request)
	{
		
        $rech_n_1 = new AllTransaction;
       
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = $rech_n_1->select('user_name', 'api_trans_id', 'api_code')
                            ->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            //$mode = $d1[0]->rech_mode;
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            //$zx = RechargeNewReply::updateSuccess($trans_id, $opr_id, "SUCCESS", $date_time);
            $zx = TransactionDirectReply::successUpdate($trans_id, $opr_id);

           /* if($zx > 0)
            {
                if($mode == "API" )
                {
                    list($api_s_url, $api_f_url) = $this->getApipartnerUrl($user_name);

                    // API Partner Reply URL Parameter Setting...
                    if (strpos($api_s_url, '<trid>') !== false) {
                        $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<oprid>') !== false) {
                        $api_s_url = str_replace("<oprid>", $opr_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<status>') !== false) {
                        $api_s_url = str_replace("<status>", "SUCCESS", $api_s_url);
                    }

                    if($api_s_url != "")
                    {
                        file_put_contents(base_path()."/public/sample/response_update_url.txt",$api_s_url.PHP_EOL, FILE_APPEND);
                        BankRemitter::runURL($api_s_url, "RESPONSE");
                    }
                }

                
            }*/

            if($zx == 0)
            {
                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }
       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);     

    }

    public function apiReprocess($trans_id, Request $request)
	{
		
        $rech_2 = new UserRechargeNewDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $api_trans_id = "";
        $user_name = "";
        $net_code = "";
        $rech_mobile = "";
        $rech_amount = "";
        $op = "NA";

       
        $d1 = $rech_2->select('trans_id','api_trans_id', 'user_name', 'net_code', 'rech_mobile', 'rech_amount')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 
        
        if($d1->count() > 0)
        {
            // Remains Pending
            $api_trans_id = $d1[0]->api_trans_id;
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;
            
            $ob = $this->getUserAccountDetails($user_name);
            $user_code = $ob->code;
            $user_ubal = $ob->ubal;

            // Post Data
            $data = [];
            $data['trans_id'] = $trans_id;
            $data['net_type_code'] = "0";
            $data['net_code'] = $net_code;
            $data['user_mobile'] =  $rech_mobile;
            $data['user_amount'] = $rech_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "API";
            $data['api_trans_id'] = $api_trans_id;
            $data['rech_type'] = "API_RECHARGE";
            
            $z = RechargeNewReMobile::add($data);

            if($z == 0)
            {
                $op = "Recharge ReProcess is Completed Successfully..";
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }

        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function webReprocess($trans_id, Request $request)
	{
		
        $rech_2 = new UserRechargeNewDetails;
       
        
        $ob = GetCommon::getUserDetails($request);

        $user_name = "";
        $net_code = "";
        $rech_mobile = "";
        $rech_amount = "";
        $op = "NA";

       
        $d1 = $rech_2->select('trans_id', 'user_name', 'net_code', 'rech_mobile', 'rech_amount')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 
        
        if($d1->count() > 0)
        {
            // Remains Pending
            $user_name = $d1[0]->user_name;
            $net_code = $d1[0]->net_code;
            $rech_mobile = $d1[0]->rech_mobile;
            $rech_amount = $d1[0]->rech_amount;

            $ob = $this->getUserAccountDetails($user_name);
            $user_code = $ob->code;
            $user_ubal = $ob->ubal;

             // Post Data
            $data = [];
            $data['trans_id'] = $trans_id;
            $data['net_type_code'] = "0";
            $data['net_code'] = $net_code;
            $data['user_mobile'] =  $rech_mobile;
            $data['user_amount'] = $rech_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "WEB";
            $data['api_trans_id'] = "";
            $data['rech_type'] = "WEB_RECHARGE";
            
            $z = RechargeNewReMobile::add($data);


            if($z == 0)
            {
                $op = "Recharge ReProcess is Completed Successfully..";
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }

           
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function getUserAccountDetails($user_name)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;
        $ob->status = 0;

        $uacc_1 = new UserAccountBalanceDetails;
        $user_2 = new UserAccountDetails;

        $user_rec_mode = "";
        $user_status = 25;

        $d2 = $user_2->select('user_code', 'user_type', 'user_rec_mode', 'user_status')->where('user_name', '=', $user_name)->get();
        foreach($d2 as $d)
        {
            $ob->user = $user_name;
            $ob->code = $d->user_code;
            $ob->mode = $d->user_type;
            $user_rec_mode = $d->user_rec_mode;
            $user_status = $d->user_status;
            $ob->status = 0;
        }
         // Check User Account Status
        if($user_status != 1)
        {
            $ob->status = 4;
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }
}