<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\GetMobileCommon;

use App\Models\UserComplaintDetails;

class DS_ComplaintController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('distributor.complaint_1', ['user' => $ob]);
        
    }

    public function store(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;


        $this->validate($request, [
            'user_complaint' => 'required'
        ],
        [
            'user_complaint.required' => 'Complaint field is required.'
            ]);

        $ob = GetCommon::getUserDetails($request);
        
        // Insert Record... 
        $comp_1->trans_id = rand(10000,99999);
        $comp_1->user_name = $ob->user;
        $comp_1->user_type = $ob->mode;
        $comp_1->comp_type = "NORMAL";
        $comp_1->rech_trans_id = "-";
        $comp_1->rech_mobile = trim($request->rech_mobile);
        $comp_1->rech_amount = trim($request->rech_amount);
        $comp_1->user_complaint = trim($request->user_complaint);
        $comp_1->admin_reply = "";
        $comp_1->reply_status = 1;
        $comp_1->reply_date = date("Y-m-d H:i:s");

        $comp_1->save();
        
        $op = 1;

       
        return redirect()->back()->with('msg', $op);

		
		
    }

    public function user_view(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->where('user_name', '=', $ob->user)->get();

        return view('distributor.complaint_2', ['comp1' => $data, 'user' => $ob]);
        
    }

    public function index_android(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
           
            return view('android.ds_complaint_entry_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store_android(Request $request)
	{
        $comp_1 = new UserComplaintDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $op = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
           
            $comp_1->trans_id = rand(10000,99999);
            $comp_1->user_name = $user_name;
            $comp_1->user_type = $user_type;
            $comp_1->comp_type = "NORMAL";
            $comp_1->rech_trans_id = "-";
            $comp_1->rech_mobile = trim($request->rech_mobile);
            $comp_1->rech_amount = trim($request->rech_amount);
            $comp_1->user_complaint = trim($request->user_complaint);
            $comp_1->admin_reply = "";
            $comp_1->reply_status = 1;
            $comp_1->reply_date = date("Y-m-d H:i:s");

            $comp_1->save();
            
            $op = 1;
            
        }
        else if($z1 == 1)
        {
            $op = 2;
        }
        else if($z1 == 2)
        {
            $op = 3;
        }

        return redirect()->back()->with('msg', $op);

    }

    public function view_android(Request $request)
	{
        $comp_1 = new UserComplaintDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $user_name = strtoupper($user_name);
            $d1 = $comp_1->whereRaw('upper(user_name) = ?',[$user_name])->orderBy('id', 'desc')->get(); 
           
            return view('android.ds_complaint_entry_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'complaint' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
