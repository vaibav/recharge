<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\AdminOfferDetails;

use App\Libraries\RechargeReportDistributor;
use App\Libraries\RechargeReportSuperDistributor;

class DashboardDistributorController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;

        $ob = $this->getUserDetails($request);

        $rs = [];
        //$rs = $this->getRechargeDetailsParent($ob->user, $ob->mode);

        $str = RechargeReportDistributor::getRechargeDetailsDistributor($ob->user);

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('user.dashboard_distributor',  ['user' => $ob, 'recharge' => $str, 'network' => $d1, 'offer' => $off, 'in_active' => $d3]);
		
    }


    public function index_super(Request $request)
	{
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;

        $ob = $this->getUserDetails($request);

        $rs = [];
        //$rs = $this->getRechargeDetailsParent($ob->user, $ob->mode);

        $str = RechargeReportSuperDistributor::getRechargeDetailsDistributor($ob->user);

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('user.dashboard_super_distributor',  ['user' => $ob, 'recharge' => $str, 'network' => $d1, 'offer' => $off, 'in_active' => $d3]);
		
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getRechargeDetailsParent($user_name, $mode)
    {
        
        
        $tran_2 = new TransactionAllParentDetails;
        $rech_3 = new UserRechargePaymentParentDetails;
        $rech_a_3 = new UserRechargePaymentParentApipartnerDetails;

        $rs = [];
        $field = "NONE";
        $d1 = $tran_2->select('trans_id', 'trans_type', 'trans_option')->where('user_name', '=', $user_name)
                                        ->orderBy('id', 'desc')->limit(15)->get(); 
        
        
        
        $j = 0;
        foreach($d1 as $d)
        {
            $data = null;
            
            if($d->trans_type == 'WEB_RECHARGE')
            {
                if($mode == "DISTRIBUTOR")
                {
                    $field = "parent_name";
                }
                else if($mode == "SUPER DISTRIBUTOR")
                {
                    $field = "super_parent_name";
                }
                if($d->trans_option == '1')
                {
                    $data = $rech_3->with(['userrechargeparent'])->where(function($query){
                                                                return $query
                                                                ->where('rech_status', '=', 'PENDING')
                                                                ->orWhere('rech_status', '=', 'SUCCESS');
                                                            })
                                                            ->where('trans_id', '=', $d->trans_id)
                                                            ->where($field, '=', $user_name)
                                                            ->where('rech_type', '=', 'WEB_RECHARGE')->get();
                    
                }
                else if($d->trans_option == '2')
                {
                    $data = $rech_3->with(['userrechargeparent'])->where([['trans_id', '=', $d->trans_id], 
                                                            ['rech_status', '=', 'FAILURE'],
                                                            [$field, '=', $user_name], 
                                                            ['rech_type', '=', 'WEB_RECHARGE']])
                                                            ->get();
                   
                }

                $rs[$j][0] = $data;
                $j++;
            }
            else if($d->trans_type == 'API_RECHARGE')
            {
                $field = "parent_name";

                if($d->trans_option == '1')
                {
                    $data = $rech_a_3->with(['userrechargeparent'])->where(function($query){
                                                                return $query
                                                                ->where('rech_status', '=', 'PENDING')
                                                                ->orWhere('rech_status', '=', 'SUCCESS');
                                                            })
                                                            ->where('trans_id', '=', $d->trans_id)
                                                            ->where($field, '=', $user_name)
                                                            ->where('rech_type', '=', 'API_RECHARGE')->get();
                    
                }
                else if($d->trans_option == '2')
                {
                    $data = $rech_a_3->with(['userrechargeparent'])->where([['trans_id', '=', $d->trans_id], 
                                                            ['rech_status', '=', 'FAILURE'],
                                                            [$field, '=', $user_name], 
                                                            ['rech_type', '=', 'API_RECHARGE']])
                                                            ->get();
                   
                }

                $rs[$j][0] = $data;
                $j++;

            }
            
        }
        
        return $rs;
    }
}
