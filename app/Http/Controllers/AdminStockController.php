<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserPaymentDetails;

use PDF;
use EXCEL;

class AdminStockController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('admin.adminstock', ['user' => $ob]);
        
		
    }

    

    public function viewdate(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2019-01-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'USER_RECHARGE')
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        

        return view('admin.adminstock_view', ['pay1' => $data, 'obalance' => $o_balance, 'user' => $ob]); 

        
    }

    public function viewdate_excel(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2019-01-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'USER_RECHARGE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        

        //return view('admin.adminstock_view', ['pay1' => $data, 'obalance' => $o_balance, 'user' => $ob]); 

        $headings = ['NO', 'DATE', 'USER', 'MODE', 'REMARKS', 'PURCHASE', 'SALES', 'BALANCE']; 
                   

        $j = 1;
        
        $content = [];

        $k = 0;

        $debit = 0;
        $credit = 0;
        $balance = 0;
        $user = $ob;
        if($o_balance != 0)
        {
            $balance = floatval($balance) + floatval($o_balance);
            $content[$k++] = ['', '', 'OPENING BALANCE', '', '', '', '', $balance]; 
           
        }
        foreach($data as $f)
        {
            if($f->user_name == $user->user && $f->trans_status == 1)
            {
                // Debit +
                $debit = floatval($debit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) + floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->grant_user_name, $f->payment_mode, 
                                    $f->user_remarks, $f->grant_user_amount, '', $balance]; 
                
                
            }
            else if($f->grant_user_name == $user->user && $f->trans_status == 1)
            {
                // Credit -
                $credit = floatval($credit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) - floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->user_name, $f->payment_mode, 
                                    $f->user_remarks, '', $f->grant_user_amount, $balance]; 
               
            }
            $j++;
                       
        }

        $cc = [$headings, $content];

        $tit = "Admin_stock_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }


    public function viewall(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.adminstock_all', ['user' => $ob]); 

    }

    public function viewalldate(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
 

        $data = $upay_1->whereBetween('grant_date', [$f_date, $t_date])
                    ->where('payment_mode', '!=', 'USER_RECHARGE')
                    ->where('payment_mode', '!=', 'OPENING_BALANCE')
                    ->orderBy('id', 'asc')->get();
                        

        return view('admin.adminstock_view_all', ['pay1' => $data, 'user' => $ob]); 

        
    }

    

    public function stockOpeningBalance($f_date, $t_date, $ob, $upay_1)
    {
        
        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'USER_RECHARGE')
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
        $balance = 0;

        foreach($data as $f)
        {
            if($f->user_name == $ob->user && $f->trans_status == 1)
            {
                // Debit +
                $balance = floatval($balance) + floatval($f->grant_user_amount);
            }
            else if($f->grant_user_name == $ob->user && $f->trans_status == 1)
            {
                // Credit -
                $balance = floatval($balance) - floatval($f->grant_user_amount);
            }
        }
        
        return $balance;
    }

    public function stockCustomOpeningBalance($ob, $upay_1)
    {
        
        $d1 = $upay_1->select('grant_user_amount')->where('user_name', '=', $ob->user)
                        ->where('payment_mode', '=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)->get();
                        
        
        $balance = 0;

        if($d1->count() > 0)
        {
            $balance = $d1[0]->grant_user_amount;
        }
        
        return $balance;
    }
}
