<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeNewMobile;
use App\Libraries\RechargeOffers;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\AdminOfferDetails;
use App\Models\UserRechargeNewDetails;

class RT_RechargeMobileController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        
        if($d1->count() > 0)
        {
            foreach($d1 as $d)
            {
                if($d->net_type_name == "PREPAID") {
                    $pre_code = $d->net_type_code;
                }
                else if($d->net_type_name == "POSTPAID") {
                    $pos_code = $d->net_type_code;
                }
                else if($d->net_type_name == "DTH") {
                    $dth_code = $d->net_type_code;
                }
                
            }
            
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pre_code], ['net_status', '=', '1']])->get();
            
            $d3 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pos_code], ['net_status', '=', '1']])->get();

            $d4 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $dth_code], ['net_status', '=', '1']])->get();
            
        }

        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', 'RETAILER');
                        })
                        ->where('offer_status', '=', '1')->get();
        
        $rs = $this->getRechargeDetailsRetailer($ob->user);
        
        return view('retailer.recharge_mobile', ['network1' => $d2, 'network2' => $d3, 'network3' => $d4,  'recharge' => $rs, 'user' => $ob, 'offer' => $off]);
        		
    }

    // Prepaid recharge
    public function prepaid_recharge(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;

        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $net_code = trim($request->net_code_1);
        $user_mobile = trim($request->user_mobile_1);
        $user_amount = trim($request->user_amount_1);
        $date_time = date("Y-m-d H:i:s");

        // Post Assign
        $data = [];
        $data['net_type_code'] = "30";
        $data['net_code'] = $net_code;
        $data['user_mobile'] = $user_mobile;
        $data['user_amount'] = $user_amount;
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        $z = RechargeNewMobile::add($data);

        $op = $this->setResult($z);
        
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    // Postpaid Recharge
    public function postpaid_recharge(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;

        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $net_code = trim($request->net_code_3);
        $user_mobile = trim($request->user_mobile_3);
        $user_amount = trim($request->user_amount_3);
        $date_time = date("Y-m-d H:i:s");
        
        // Post Data
        $data = [];
        $data['net_type_code'] = "30";
        $data['net_code'] = $net_code;
        $data['user_mobile'] = $user_mobile;
        $data['user_amount'] = $user_amount;
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        $z = RechargeNewMobile::add($data);

        $op = $this->setResult($z);
    
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    // Dth Recharge
    public function dth_recharge(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        
        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $net_code = trim($request->net_code_2);
        $user_mobile = trim($request->user_mobile_2);
        $user_amount = trim($request->user_amount_2);
        $date_time = date("Y-m-d H:i:s");

        // Post Data
        $data = [];
        $data['net_type_code'] = "30";
        $data['net_code'] = $net_code;
        $data['user_mobile'] = $user_mobile;
        $data['user_amount'] = $user_amount;
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        $z = RechargeNewMobile::add($data);

        $op = $this->setResult($z);

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }


    public function setResult($z)
    {
        $op = "NONE";
        if($z == 0) {
            $op = "Recharge Request is Accepted..";
        }
        else if($z == 1) {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2) {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3) {   
            $op = "Error! Mode web is not Selected...";
        }
        else if($z == 4) {   
            $op = "Error! Account is Inactive...";
        }
        else if($z == 5) {
            $op = "Error! Account Balance is low from Setup Fee...";
        }
        else if($z == 6) {
            $op = "Error! Recharge Status is already Pending for this Mobile No...";
        }
        else if($z == 7) {
            $op = "Error! Wait for 10 Minutes...";
        }
        else if($z == 8) {
            $op = "Sorry! Server is Temporarily shut down...";
        }

        return $op;
    }

    public function getRechargeDetailsRetailer($user_name)
    {
        $rech_1 = new UserRechargeNewDetails;
        $net_2 = new NetworkDetails;
       
        $rs = [];
        $d1 = $rech_1->with(['recharge2'])->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
       
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
                                              
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            if($f->recharge2 != null)
            {
                $reply_id = $f->recharge2->reply_opr_id;
                $reply_date =$f->recharge2->reply_date;
            }
            
            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
               
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            $str = $str."  <div style='margin:4px 10px;border-radius:15px;' class='card '><div class='row ' style='padding:1px 3px;'>"; 
            $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center m-0 mt-2 pl-1 pr-1' >"; 
            if($status == "SUCCESS") {
                $str = $str."<a class='btn btn-circle green text-white' ><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='btn btn-circle red text-white' ><i class='small material-icons '>close</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='btn btn-circle green text-white' ><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p class='m-0'>".$f->rech_amount."</p></div>";
        
            $str = $str."<div class='col-6 col-sm-6 col-md-6 text-left m-0 pl-1 pr-1' >"; 
            $str = $str."<p class='text-muted m-1' >".$net_name."</p><p class='text-muted m-1' >".$f->rech_mobile."</p>"; 
            
            if($z== 0) {
                $str = $str."<p class='text-muted' style='margin:3px 5px;font-size: 11px;'>".$reply_id."</p>"; 
            }
            else 
            {
                $str = $str."<p class='text-muted' style='margin:3px 5px;font-size: 11px;'></p>";  
            }

            $str = $str."<p class='text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p></div>"; 
            
            $str = $str."<div class='col-3 col-sm-3 col-md-3 text-left m-0 pl-1 pr-1' >";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;margin-top:5px;'>
                                       % &nbsp;".$f->rech_net_per. "--".number_format($f->rech_net_per_amt,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'>
                                        <i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->rech_total,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($c_bal,2, ".", "")."</p>";
            
            $str = $str."</div></div></div>";       
                               
     
        }
        
        return $str; 
    }

    public function getOfferMobile($net_code, $mobile, Request $request)
	{
        $str = "";
        
        $ob = GetCommon::getUserDetails($request);

        $dx = RechargeOffers::getOffer121($net_code, $mobile, $ob->user);

        if(!empty($dx)) {

            foreach($dx as $d) {
                $str = $str ."<div class = 'card mt-2 mb-1'>";
                $str = $str . "<div class='row p-1' >
                                    <div class='col-md-9 text-left m-0 pl-1 pr-1'>
                                    <p class='text-muted p-2'>".$d['text']."</p> </div>";
                $str = $str . "<div class='col-md-2 text-center mt-2'><a id='id_off' 
                                    class='btn btn-circle green text-white'>".$d['price']."</a></div>";
                $str = $str ."</div></div>";

            }    
        }
        else {
            $str = "No Plan is available in this Network..";
        }

        echo $str;
    }

    public function getMobilePlans($net_code, Request $request)
	{
       
        $str = "";
        
        $ob = GetCommon::getUserDetails($request);

        $dx = RechargeOffers::getPlans($net_code, $ob->user);

        if(!empty($dx)) 
        {
            $str = $str . "<ul id='tabs-swipe-demo' class='tabs'>";
            $j = 1;
            foreach($dx as $d)
            {
                $str = $str ."<li class='tab col s2' style='padding:0px 0px;'>";
                if($j == 1)
                {
                    $str = $str . "<a class='nav-link active' data-toggle='tab' href='#page_".$j."' role='tab' style = 'font-size:12px'>
                                    ".$d['title']."</a></li>";
                }
                else
                {
                    $str = $str . "<a class='nav-link' data-toggle='tab' href='#page_".$j."' role='tab' style = 'font-size:12px'>
                                    ".$d['title']."</a></li>";
                }
                
                $j++;
            }  
            $str = $str . "</ul>";  
           
            $j = 1;
            foreach($dx as $d)
            {
                $str = $str ."<div id='page_".$j."' class='col s12'>";
                $str = $str ."<table class='striped responsive-table'>";
               
               
                foreach($d['content'] as $c)
                {
                    
                    $str = $str . "<tr><td class='left-align' style = 'font-size:14px;padding:4px 4px;'>".$c['text']."</td>
                    <td class='right-align' style = 'font-size:14px;padding:4px 4px;'>";
                    $str = $str . "<a class='btn-floating center-align #7b1fa2 blue darken-2' id='id_off'>".$c['price']."</a></td></tr>";
                    
                   
                }
                $str = $str ."</table></div>";
                $j++;
            }  
            
        }
        else
        {
            $str = "<p class = 'text-muted'>No Plan is available in this Network..</p>";
        }

        echo $str;
    }

    public function getMobileDthInfo($net_code, $mobile, Request $request)
	{
        $str = "";
        
        $ob = GetCommon::getUserDetails($request);

        $dx = RechargeOffers::getDthInfo($net_code, $mobile, $ob->user);

        if(!empty($dx)) {

            foreach($dx as $d) {
                $str = $str ."<div class = 'card mt-2 mb-1'>";
                $str = $str . "<div class='row p-1' >
                                    <div class='col-md-4 text-left m-0 pl-1 pr-1'>
                                    <p class='text-muted p-2'>".$d['key']."</p> </div>";
                $str = $str . "<div class='col-md-8 text-left mt-2'><p class='text-muted p-2'>".$d['value']."</p></div>";
                $str = $str ."</div></div>";

            }    
        }
        else {
            $str = "No Plan is available in this Network..";
        }

        echo $str;
    }

    public function getOperatorCheck($mobile, Request $request)
	{
        $net_2 = new NetworkDetails;
        $str = "";
        $net_code = "1";
        $x = 1;
        
        $ob = GetCommon::getUserDetails($request);

        $operator = RechargeOffers::getOperator($mobile, $ob->user);

        $d2 = $net_2->select('net_code', 'net_name')->get();

        foreach($d2 as $d) {
            if(strtolower($d->net_name) == strtolower($operator)) {
                $net_code = $d->net_code;
                $x = 2;
            }
        }

        if($x == 1) {
            foreach($d2 as $d) {
                $c = explode(" ", $d->net_name);

                if(sizeof($c > 0))
                {
                    if (strpos(strtolower($operator), strtolower($c[0])) !== false) {
                        $net_code = $d->net_code;
                        $x = 2;
                    }
                }
                
            }
        }

        echo $net_code;
    }


}
