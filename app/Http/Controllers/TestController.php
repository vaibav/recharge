<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use PDF;
use App\Models\UserAccountBalanceDetails;

class TestController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = $this->getUserDetails($request);
        return view('user.test1', ['user' => $ob]);
        		
    }
    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function view(Request $request)
	{
        echo "welcome to all...";
    }

    public function testAjax(Request $request)
    {
        echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>";
        echo "<script>$.ajax({
            method: 'GET', // Type of response and matches what we said in the route
            url: 'test4', // This is the url we gave in the route
            data: {'id' : 1}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                document.writeln(response); 
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                document.writeln('AJAX error: ' + textStatus + ' : ' + errorThrown);
            }
        });</script>";
    }

    public function testAjax1(Request $request)
    {
        $a = "1";
        $url = "http://ssh1.bonrix.in:5167/gprs?page=recharge_confirm&mobileno=1234567890&pin=123&recharge_type=Rr&txtclientmobile=9894492775&txtamount=10&operator=Airtel";
        echo "<iframe src=".$url." id='ab' style='display:none;'></iframe>";
        echo "welcome ".$a;
    }
    
    public function testpdf(Request $request)
    {
        $html = '<html>

        <head>
        <title>My first PHP Page</title>
        </head>
        <body>
            <h1>Welcome to All</h1>
        
       
        
        </body>
        </html>';
        
        $pdf = PDF::loadHTML($html);
        return $pdf->stream();
    }
    
    
     public function testrecharge(Request $request)
    {
        $url = "https://www.vastwebindia.com/api/api/recharge?Mobile=8300640076&OptCode=A&Amount=10&Token=IwL0u2AA2Hf%2bUh5ERekvaFAbnkGL6%2fy5fhHf1qOAuKM%3d&Userid=vaibavonline@gmail.com&rch_id=VBR75306707";

        $url = $this->convert_text($url);

        /*$client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();*/
        
        $res = file_get_contents($url);

        echo $res_content;
    }
    
    
    public function testeb(Request $request)
    {
        $url = "https://www.vastwebindia.com/Offer/ElectricityViewBill?userid=vaibavonline@gmail.com&token=IwL0u2AA2Hf%2bUh5ERekvaFAbnkGL6%2fy5fhHf1qOAuKM%3d&StateName=21&BoardName=Tamil Nadu&account=null&ConsumerNumber=141006720";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
    }
    
    public function testmoneyservice(Request $request)
    {
        $url = "https://api.goprocessing.in/getServiceData.go?goid=5061079115&apikey=3dHlrw57F6lGGgt&rtype=json";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

       echo $res_content;
    }
    
    public function getState(Request $request)
    {
        $url = "https://api.goprocessing.in/getServiceData.go?goid=5061079115&apikey=eQqABqL7W4HTLF7&rtype=json&type=states";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getBank(Request $request)
    {
        $url = "https://api.goprocessing.in/getServiceData.go?goid=5061079115&apikey=eQqABqL7W4HTLF7&type=bank&rtype=json";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getIFSC(Request $request)
    {
        $url = "https://api.goprocessing.in/getIfscCodeInfo.go?goid=5061079115&apikey=eQqABqL7W4HTLF7&rtype=json&type=getDistricts&bank_code=TMBL&branch_state_code=6";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getAllIFSC(Request $request)
    {
        $url = "https://api.goprocessing.in/getIfscCodeInfo.go?goid=5061079115&apikey=9z6BLpREHl8wHxn&rtype=json&type=getAllBranches&bank_code=TMBL&branch_state_code=6&branch_district=Madurai";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getBranch(Request $request)
    {
        $url = "https://api.goprocessing.in/getIfscCodeInfo.go?goid=5061079115&apikey=3dHlrw57F6lGGgt&rtype=json&type=getAllBranches&bank_code=CNRB&branch_state_code=6&branch_district=Madurai";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function checkIFSC(Request $request)
    {
        $url = "https://api.goprocessing.in/getIfscCodeInfo.go?goid=5061079115&apikey=3dHlrw57F6lGGgt&rtype=json&type=validIFSCcode&ifsc_code=CNRB0003054";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function setUser(Request $request)
    {
        $url = "https://api.goprocessing.in/dmt/moneyRegister.go?goid=5061079115&apikey=3dHlrw57F6lGGgt&rtype=json&service_family=25&apimode=test&agent_id=1&msisdn=9999999999&user_name=test&user_address=delhi&user_city=newdelhi&user_state_code=1&user_pincode=110020";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getAgent(Request $request)
    {
        $url = "https://api.goprocessing.in/agent/agentFetchData.go?goid=5061079115&apikey=9z6BLpREHl8wHxn&rtype=json&apimode=test&agent_msisdn=9994030843";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }


    public function checkMROBO(Request $request)
    {
        $url = "https://mrobotics.in/api/recharge_get?api_token=cd6f99d1-1f64-47e7-b004-3b97b428fc97&mobile_no=1234567891&amount=10&company_id=17&order_id=1112233&is_stv=false";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function getCustomer(Request $request)
    {
        //$url = "https://api.goprocessing.in/dmt/moneyGetDetails.go?goid=5061079115&apikey=9z6BLpREHl8wHxn&rtype=json&apimode=test&service_family=25&agent_id=8042&msisdn=9944876688";
        
        $url = "https://api.goprocessing.in/dmt/moneyGetDetails.go?goid=5061079115&apikey=9z6BLpREHl8wHxn&rtype=json&apimode=test&service_family=25&agent_id=8042&msisdn=9894492775";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
     public function checkAccount(Request $request)
    {
        $url = "https://api.goprocessing.in/dmt/moneyVerification.go?goid=5061079115&apikey=9z6BLpREHl8wHxn&rtype=json&apimode=test&service_family=25&agent_id=8934&msisdn=9976929248&verification_type=BANK&client_trans_id=123461&beneficiary_account_no=1608000012&beneficiary_bank_code=KVBL";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }

    public function checkPOST(Request $request)
    {
        $url = "https://mrobotics.in/api/recharge";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        //$params['headers'] = ['Content-Type' => 'application/json', 'Authorization' => 'Zoho-authtoken ' . $AuthCode];
        $params['form_params'] = array('api_token' => '63fb13ea-e6e1-4aba-9c19-d7ddaf1ef217', 
                                        'mobile_no' =>'9894492775', 'amount' => '10', 'company_id' => '2', 'order_id' => 'VBR678976', 'is_stv' => 'false');
        $response = $client->post($url, $params);
        
        $res_content =$response->getBody();
        echo "DONE!<br>";

        
        
        echo $res_content;
        
        
        //return response()->json($res_content, 200);
    }


    
    
    public function jsonformatter(Request $request)
    {
        $url = "https://api.goprocessing.in/getIfscCodeInfo.go?goid=5061079115&apikey=3dHlrw57F6lGGgt&rtype=json&type=validIFSCcode&ifsc_code=CNRB0003054";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content = (string) $res->getBody();

        $result = $this->resultFormatter($res_content);

        $result = json_decode($result, true);
    
        
    
        $this->jsonDataMatcher($result);

        
        $data = [];
        
        //return response()->json($res_content, 200);
    }
    
    public function test_recharge(Request $request)
    {
        $url = "http://ec2-3-19-53-122.us-east-2.compute.amazonaws.com/vaibav/api/apirecharge/?uname=newbala&pin=12345&trid=VBA234343&opr=RA&mob=22334455&amt=2";

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content = (string) $res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }

    public function checkoffers(Request $request)
    {
        //$url = "https://www.mplan.in/api/plans.php?apikey=da4d1e5855529cfaf80b662ec57fa463&cricle=Tamil Nadu&operator=Airtel";

        $url = "https://www.mplan.in/api/electricinfo.php?apikey=da4d1e5855529cfaf80b662ec57fa463&offer=roffer&tel=051410092156&operator=TNEB";
       

        $url = $this->convert_text($url);

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_code = $res->getStatusCode();
        $res_content =$res->getBody();

        echo $res_content;
        
        //return response()->json($res_content, 200);
    }
    
    public function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }

    public function jsonDataMatcher($result)
    {

        $status = "";
        if(!empty($result))
        {
            //print_r($result);

            // CHeck Result
            foreach($result as $key => $value)
            {
                $status = $this->statusCheck($key, $value);
                if ($status == "SUCCESS" || $status == "FAILURE")
                    break;
            }
            
             echo "Status Result =>".$status."<br>";
             
             foreach($result as $key => $value)
            {
                echo $key." => ". $value."<Br>";
            }

        }
        
       

    }
    
    public function statusCheck($key, $value)
    {
        $status = "PENDING";
        
        $data = [];
        
        $data[0][0] = "STATUS 1";
        $data[0][1] = "status";
        $data[0][2] = "1";
        
        $data[1][0] = "STATUS 2";
        $data[1][1] = "status";
        $data[1][2] = "2";
        
        foreach($data as $d)
        {
            if($d[1] == $key)
            {
                
                if($d[2] == $value)
                {
                   
                    if($d[0] == "STATUS 1")
                    {
                       $status = "SUCCESS";
                        
                    }
                    else if($d[0] == "STATUS 2")
                    {
                        $status = "FAILURE";
                    }
                }
            }
        }
        
        echo $status."<br>";
        return $status;
    }

    public static function convert_text($text) {

        $t = $text;
        
        $specChars = array(
           
            ',' => '%E2%80%9A',  ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }

    public function testCybor(Request $request)
    {
        $ses = date("Ymdhis");
		$url = "https://in.cyberplat.com/cgi-bin/vd/vd_pay_check.cgi";
		$data_string = "SD=357355&AP=325448&OP=325449&SESSION=".$ses."&COMMENT=Test&NUMBER=9894492775&AMOUNT_ALL=10.0&AMOUNT=10.0";
		$signature = "BEGIN
			mQCTAwABb6RgH5kWAAABBADGcLgvY5WKZe0A9rqXS0o/pBNq/j7yhfOzUTMQCSzM
			p3NAejFFUAAPaQf2aa7JW9lB5f/Y9Yg78eEsgCpkoE4w1QI+5WY5lb43p7dqMbnk
			VfCg/3y5y8plFNkOQLbHWz9zK2210DoW/wiuigaJhApIS5ty5P3YVaC2cORjIwwS
			VQARAQABsAGHtAlhcGk0MDAxMzWwAQM=
			=0QSo
			END";
		$data_new_string = "inputmessage=BEGIN%0ACERT%3D473F75E517169907C3918BCA2AB3D88B99CDB171%0ASD%3D357355%0AAP%3D357356%0AOP%3D357357%0ASESSION%3DTueJul032021171453%0ANUMBER%3D9894492775%0AAMOUNT%3D10.00%0ATERM_ID%3D325448%0ACOMMENT%3Dtest%0AEND%0ABEGIN%20SIGNATURE%0AMIICGjCCAYMCFFb9OjT4JElWx4XGGIbPS46KPe%2BDMA0GCSqGSIb3DQEBDAUAMEwx%0ACzAJBgNVBAYTAklOMQ8wDQYDVQQIDAZ2YWliYXYxFTATBgNVBAoMDHZhaWJhdm9u%0AbGluZTEVMBMGA1UECwwMdmFpYmF2b25saW5lMB4XDTIxMDcwNjA3MjgzMloXDTIy%0AMDcwNjA3MjgzMlowTDELMAkGA1UEBhMCSU4xDzANBgNVBAgMBnZhaWJhdjEVMBMG%0AA1UECgwMdmFpYmF2b25saW5lMRUwEwYDVQQLDAx2YWliYXZvbmxpbmUwgZ8wDQYJ%0AKoZIhvcNAQEBBQADgY0AMIGJAoGBAL98IUNok84B2Pzimejpz0pNJDQNfgB%2B0ilB%0ADy7Vft8u4LuBHaL5SkyTLkKa8z2VGkqtXeF10FkRBMl9b%2BfG3XAFHuXoUgc98W%2B%2F%0AWc6d5cpnzbx32%2BZL4ewIrtjf9euoskOVUTYfxXYksHYTTYXnkb6%2BcDEZKx9QtSog%0AvgJbnM1RAgMBAAEwDQYJKoZIhvcNAQEMBQADgYEALfVEjtnT1VwfGMpgJtTe4rJP%0A3bHr0ZIdlbd2O3T1AmP2NGana81j9KZsO8%2Birs8%2BcMxoAFP17JD7PeTYXIwxvX8F%0AW9ZnZIYSR7YoPyw9SSN9cOviROo4x6Y3MnVt3aYjZrGN%2FX66LRr4NIXB7shj8Y9Y%0A5s2PvdCHlmmSez04Vhg%3D%0AEND%20SIGNATURE";

		$headers = [
		    'Content-Type: application/x-www-form-urlencoded',
		    'Content-Length: '.strlen($data_new_string),
		];

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_new_string);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		$result = curl_exec($ch);

		var_dump($result);



		/*$ses = date("Ymdhis");
		$url = "https://in.cyberplat.com/cgi-bin/vd/vd_pay_check.cgi";
		$data_string = "SD=357355&AP=357356&OP=357357&SESSION=1232334344545565&NUMBER=9894492775&AMOUNT=10.00&AMOUNT_ALL=10.00&TERM_ID=325448";

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Content-Length: '.strlen($data_string)));
		curl_setopt($ch, CURLOPT_POSTFIELDS, "inputmessage=0000059001NS000002370000016700000217&".$data_string);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		$result = curl_exec($ch);

		var_dump($result);*/
        
        
        //return response()->json($res_content, 200);

        
    }

    public function get_query_result($qs, $url)
	{
	    $opts = array( 
	        'http'=>array(
	            'method'=>"POST",
	            'header'=>array("Content-type: application/x-www-form-urlencoded\r\n".
	                            "X-CyberPlat-Proto: SHA1RSA\r\n"),
	            'content' => "inputmessage=".urlencode($qs)
	        )
	    ); 
	    $context = stream_context_create($opts);    
	    return file_get_contents($url, false, $context);
	}


	function check_signature($response, $serverCert)
	{
	    $fields = preg_split("/END\r\nBEGIN SIGNATURE\r\n|END SIGNATURE\r\n|BEGIN\r\n/", $response, NULL, PREG_SPLIT_NO_EMPTY);
	    if (count($fields) != 2) {
	        print "Bad response\n";
			return;
	    }

	    $pubkeyid = openssl_pkey_get_public($serverCert);
	    $ok = openssl_verify(trim($fields[0]), base64_decode($fields[1]), $pubkeyid);
	    print "Signature is ";
	    if ($ok==1) {
	            print "good";
	    } elseif ($ok==0) {
	            print "bad";
	    } else {
	            print "ugly, error checking signature";
	    }
	    print "\n";
	    openssl_free_key($pubkeyid);
	}


    public function sampleTest(Request $request)
    {
        echo "welcome To Test Controller";
    }
    
    
}
