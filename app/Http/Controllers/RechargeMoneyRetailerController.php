<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use stdClass;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Libraries\RechargeReportRetailer;

use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllDetails;
use App\Models\ApiProviderDetails;
use App\Models\UserRechargeBillDetails;

use PDF;
use EXCEL;


class RechargeMoneyRetailerController extends Controller
{
    //
    public function index(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.rechargereport_money', ['user' => $ob]);
        
    }

    public function index_api(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        return view('user.rechargereport_bill_apipartner', ['user' => $ob]);
        
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
       
        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($ob->user);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
         

        $dc4 = RechargeReportRetailer::getRechargeReportDetails_money($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, "", "");
        

        $d2 = $net_2->select('net_code','net_name')->get();

        //-----------------------------------------------------
        //Total Calculation-------------------------------------------------------
        //eb
        $msua_tot = 0;
        $msut_tot = 0;
        $mfua_tot = 0;
        $mfut_tot = 0;
        $mrea_tot = 0;
        $mret_tot = 0;
      
        foreach($dc4 as $d)
        {
            if($d->trans_type == "BANK_TRANSFER")
            {
               
                $rech_status = "";
                $status = "";
                $r_tot = 0;
                $r_amt = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $r_amt = $d->moneytransfer[0]->mt_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $r_amt = $d->moneytransfer[1]->mt_amount;
                    
                }
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $mfua_tot = floatval($mfua_tot) + floatval($r_amt);
                    $mfut_tot = floatval($mfut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $msua_tot = floatval($msua_tot) + floatval($r_amt);
                    $msut_tot = floatval($msut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
               
            }                                                    
        }

        $total = ['msua_tot' => $msua_tot, 'msut_tot' => $msut_tot, 'mfua_tot' => $mfua_tot, 'mfut_tot' => $mfut_tot, 
                    'mrea_tot' => $mrea_tot, 'mret_tot' => $mret_tot];
       
       
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        $rs = [];
                               
        return view('retailer.rechargereport_money_view', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2, 'total' => $total]); 

    }

    public function viewdate_api(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($ob->user);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
                               
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        
        
        $d2 = $net_2->select('net_code','net_name')->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        return view('user.rechargereport_bill_view', ['user' => $ob, 'recharge' => $dc4, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'api' => $dx3, 'total' => '0']); 

    }


    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        
        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($ob->user);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        
        
        $dc4 = RechargeReportRetailer::getRechargeReportDetails_money($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, "", "");
        

        $d2 = $net_2->select('net_code','net_name')->get();
       
        
        

        $headings = ['NO', 'ACC.NO', 'BANK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
        $str = "";
        $content = [];

        $k = 0;
        foreach($dc4 as $d)
        {
           
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
               
                $api_name = "";
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }

                $bank = "";
                if($d->moneytransfer[0]->bk_code == "UTIB") {
                    $bank = "AXIS BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "BARB") {
                    $bank = "BANK OF BARODA";
                }
                else if($d->moneytransfer[0]->bk_code == "CNRB") {
                    $bank = "CANARA BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "CORP") {
                    $bank = "CORPORATION BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "CIUB") {
                    $bank = "CITY UNION BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "HDFC") {
                    $bank = "HDFC BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "IBKL") {
                    $bank = "IDBI BANK LTD";
                }
                else if($d->moneytransfer[0]->bk_code == "IDIB") {
                    $bank = "INDIAN BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "IOBA") {
                    $bank = "INDIAN OVERSEAS BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "ICIC") {
                    $bank = "ICICI BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "KVBL") {
                    $bank = "KARUR VYSYA BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "SBIN") {
                    $bank = "STATE BANK OF INDIA";
                }
                else if($d->moneytransfer[0]->bk_code == "SYNB") {
                    $bank = "SYNDICATE BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "TMBL") {
                    $bank = "TAMILNADU MERCANTILE BANK";
                }
                else if($d->moneytransfer[0]->bk_code == "VIJB") {
                    $bank = "VIJAYA BANK";
                }
                else {
                    $bank = $d->moneytransfer[0]->bk_code;
                }
                
                if($d->trans_option == 1)
                {
                   
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $bank, 
                        $d->moneytransfer[0]->mt_amount, 
                        $d->moneytransfer[0]->mt_per."-".$d->moneytransfer[0]->mt_per_amt."-".$d->moneytransfer[0]->mt_surp,
                        $d->moneytransfer[0]->mt_total, $d->trans_id, $d->moneytransfer[0]->mt_reply_id,
                        $d->moneytransfer[0]->mt_date, $d->moneytransfer[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];

                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $bank, 
                    $d->moneytransfer[0]->mt_amount, '',
                    $d->moneytransfer[0]->mt_total, $d->trans_id, '','','',
                    $status, number_format($o_bal,2, ".", ""),
                    number_format($u_bal,2, ".", "")];

                }
               
            }   
                                               
                  
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $u_name."_Recharge_Money_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }

    public function viewdate_excel_api(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($ob->user);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
                              
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        

        $d2 = $net_2->select('net_code','net_name')->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        

        $headings = ['NO', 'EB CONN NO', 'NETWORK', 'EB DUE AMT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
        $str = "";
        $content = [];

        $k = 0;
        foreach($dc4 as $d)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($d->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $api_name = "";
            foreach($dx3 as $r)
            {
                if($d->api_code == $r->api_code)
                    $api_name = $r->api_name;
            }

            $rech_status = "";
            $status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;

            $rech_option = $d->con_option;
            $rech_status = $d->con_status;
            $r_tot = $d->con_total;
            $u_bal = $d->user_balance;

            

            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
                //$o_bal = floatval($u_bal) - floatval($r_tot);
                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                //$u_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            

            
            if($rech_option == 2 && $rech_status == "FAILURE")
            {
                $content[$k++] = [$j, $d->con_acc_no, $net_name, $d->con_amount, 
                            '', $d->con_total, $d->trans_id, 
                             $d->reply_opr_id, '', '', $status, 
                            $o_bal, $u_bal];
                
            }
            else  
            {

                $content[$k++] = [$j, $d->con_acc_no, $net_name, $d->con_amount, 
                                    $d->con_net_per."-".$d->con_net_per_amt."-".$d->con_net_surp, $d->con_total, $d->trans_id, 
                                    $d->reply_opr_id, $d->created_at, $d->reply_date, $status, 
                                    $o_bal, $u_bal];
                
            }
            
                                                    
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $u_name."_Recharge_bill_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }


}
