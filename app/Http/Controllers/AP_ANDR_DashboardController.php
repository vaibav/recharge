<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\UserRechargeNewDetails;

class AP_ANDR_DashboardController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API PARTNER...
            return view('android.dashboard_ap_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function getRechargeGraphData(Request $request)
    {
        $rech_n_1 = new UserRechargeNewDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API PARTNER...
            $f_date = $this->getFromDate();
            $user_name = strtoupper($user_name);
            
            $ars = [];
            $arf = [];
            $ard = $this->getDates();

            $h = date("H");
            
            for($i = 0; $i <= 14; $i++)
            {
                $f_date_1 = $f_date." 00:00:00";
                $t_date_1 = $f_date." 23:59:59";

                $res_tot = $rech_n_1->whereBetween('rech_date', [$f_date_1, $t_date_1])
                                    ->whereRaw('upper(user_name) = ?',[$user_name])
                                    ->where('rech_status', '=', 'SUCCESS')
                                    ->count(); 

                $res_tot = round($res_tot);

                $ref_tot = $rech_n_1->whereBetween('rech_date', [$f_date_1, $t_date_1])
                                        ->whereRaw('upper(user_name) = ?',[$user_name])
                                        ->where('rech_status', '=', 'FAILURE')
                                        ->where('rech_option', '=', '2')
                                        ->count(); 

                $ref_tot = round($ref_tot);
                
                $ars[$i] = $res_tot;
                $arf[$i] = $ref_tot;

                $f_date = date('Y-m-d', strtotime($f_date .' +1 day'));
                
            }
            
            $arr = ['res' => $ars, 'ref' => $arf, 'dat' => $ard];

            //file_put_contents(base_path().'/public/sample/andr_rt.txt', print_r($arr, true), FILE_APPEND);
        
            return response()->json($arr, 200);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        
    }

    public function getDates()
    {
        $dates = [];

        $db = $this->getFromDate();

        for($i = 0; $i <= 14; $i++)
        {
            $timestamp = strtotime($db);

            $dates[$i] = date("d", $timestamp);

            $db = date('Y-m-d', strtotime($db .' +1 day'));
        }

        return $dates;
    }

    public function getFromDate()
    {
        $da = date('Y-m-d');

        $date = date_create($da);
        date_sub($date,date_interval_create_from_date_string("14 days"));
        $db = date_format($date,"Y-m-d");

        return $db;
    }
}
