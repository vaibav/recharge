<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\MobileUserDetails;

use App\Models\UserPaymentDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;

class AndroidMyaccountController extends Controller
{
    //
    public function index_retailer(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $user_name = "none";
        $user_type = "none";
        $user_ubal = "0";
        $res_amt = "0";
        $rep_amt = "0";
        $ebs_amt = "0";
        $ebp_amt = "0";
        $mns_amt = "0";
        $mnp_amt = "0";
        $mvs_amt = "0";
        $d1 = [];

        list($user_name, $user_type, $user_ubal) = $this->getUserDetails($auth_token);

        if($user_name != "none")
        {
            $user_name = strtoupper($user_name);

            if($user_type == "RETAILER" || $user_type == "API PARTNER")
            {
                $d1 = $upay_1->where(function($query) use ($user_name){
                                    return $query
                                    ->whereRaw("(upper(user_name) = '".$user_name."' OR upper(grant_user_name) = '".$user_name."')");
                                })
                                ->where('trans_status', '=', 1)
                                ->orderBy('id', 'asc')->get();
                
                $res_amt = $this->getRetailerSuccessRechargeAmount($user_name);
                $rep_amt = $this->getRetailerPendingRechargeAmount($user_name);
                $ebs_amt = $this->getRetailerSuccessEBbillAmount($user_name);
                $ebp_amt = $this->getRetailerPendingEBbillAmount($user_name);
                $mns_amt = $this->getRetailerSuccessMoneyAmount($user_name);
                $mnp_amt = $this->getRetailerPendingMoneyAmount($user_name);
                $mvs_amt = $this->getRetailerSuccessMoneyVerifyAmount($user_name);
            }
        }

        return view('android.my_account_rt', ['user_name' => $user_name, 'user_type' => $user_type, 
                            'user_bal' => $user_ubal, 'pay' => $d1, "res" => $res_amt, 'rep' => $rep_amt, 
                            'ebs' => $ebs_amt, 'ebp' => $ebp_amt, 'mns' => $mns_amt, 'mnp' => $mnp_amt, 'mvs' => $mvs_amt]);
    }



    public function index_distributor(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $user_name = "none";
        $user_type = "none";
        $user_ubal = "0";
        $res_amt = "0";
        $rep_amt = "0";
        $ebs_amt = "0";
        $ebp_amt = "0";
        $d1 = [];

        list($user_name, $user_type, $user_ubal) = $this->getUserDetails($auth_token);
        
       

        if($user_name != "none")
        {
            if($user_type != "none")
            {
                $d1 = $upay_1->where(function($query) use ($user_name){
                                    return $query
                                    ->where('user_name', '=', $user_name)
                                    ->orWhere('grant_user_name', '=', $user_name);
                                })
                                ->where('trans_status', '=', 1)
                                ->orderBy('id', 'asc')->get();
            
                
                $res_amt = $this->getDistributorSuccessRechargeAmount($user_name);
                $rep_amt = $this->getDistributorPendingRechargeAmount($user_name);
               
            }
        }

        
        return view('android.my_account_ds', ['user_name' => $user_name, 'user_type' => $user_type, 'user_bal' => $user_ubal, 'pay' => $d1, "res" => $res_amt, 'rep' => $rep_amt, 'ebs' => $ebs_amt, 'ebp' => $ebp_amt]);
    }

    public function getUserDetails($auth_token)
    {
        $mob_1 = new MobileUserDetails;

        $user_name = "none";
        $user_type = "none";
        $user_ubal = "0";

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
        }

        return array($user_name, $user_type, $user_ubal);
    }

    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $user_name = strtoupper($user_name);

        $d1 = $uacc_1->select('user_balance')->whereRaw('upper(user_name) = ?',[$user_name])->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    // Retailer
    public function getRetailerSuccessRechargeAmount($user_name)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('rech_status', '=', 'SUCCESS')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }


    public function getRetailerPendingRechargeAmount($user_name)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('rech_status', '=', 'PENDING')
                        ->where('rech_option', '=', '0')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

    public function getRetailerSuccessEBbillAmount($user_name)
    {
        $rech_n_1 = new UserRechargeBillDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('con_status', '=', 'SUCCESS')
                        ->sum('con_total'); 
        
        return $ce_tot;
        
    }

    public function getRetailerPendingEBbillAmount($user_name)
    {
        $rech_n_1 = new UserRechargeBillDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('con_status', '=', 'PENDING')
                        ->where('con_option', '=', '1')
                        ->sum('con_total'); 
        
        return $ce_tot;
        
    }

    public function getRetailerSuccessMoneyAmount($user_name)
    {
        $bank_1 = new UserBankTransferDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_1->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('mt_status', '=', 'SUCCESS')
                                ->sum('mt_total');

        
        
        return $ce_tot;
        
    }

    public function getRetailerPendingMoneyAmount($user_name)
    {
        $bank_1 = new UserBankTransferDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_1->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('mt_status', '=', 'PENDING')
                                ->where('mt_option', '=', '1')
                                ->sum('mt_total');

        return $ce_tot;
        
    }

    public function getRetailerSuccessMoneyVerifyAmount($user_name)
    {
        $bank_2 = new UserBeneficiaryAccVerifyDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_2->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('ben_status', '=', 'SUCCESS')
                                ->sum('ben_surplus');          
        return $ce_tot;
        
    }

    // Distributor
    public function getDistributorSuccessRechargeAmount($user_name)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $ce_tot = 0;
        
        $ce_tot = $rech_n_1->where('parent_name', '=', $user_name)
                        ->where('rech_status', '=', 'SUCCESS')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

    public function getDistributorPendingRechargeAmount($user_name)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $ce_tot = 0;
        
        $ce_tot = $rech_n_1->where('parent_name', '=', $user_name)
                        ->where('rech_status', '=', 'PENDING')
                        ->where('rech_option', '=', '0')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

}
