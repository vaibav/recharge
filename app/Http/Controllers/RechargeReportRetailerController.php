<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportAdmin;
use App\Libraries\RechargeReportRetailer;
use App\Libraries\RechargeReportApipartner;

use App\Models\UserAccountBalanceDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\NetworkDetails;

use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\UserAccountDetails;

use PDF;
use EXCEL;

class RechargeReportRetailerController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_2 = new NetworkDetails;

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $ob = GetCommon::getUserDetails($request);
        return view('retailer.rechargereport', ['user' => $ob, 'network' => $d1]);
        
    }

    public function index_apipartner(Request $request)
	{
        $net_2 = new NetworkDetails;

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $ob = GetCommon::getUserDetails($request);
        return view('user.rechargereport_apipartner', ['user' => $ob, 'network' => $d1]);
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
       

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);

        $rs = [];
       
        $dc4 = RechargeReportRetailer::getRechargeReportDetails_new($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, "");

      
        
       if($u_net_code_1 != "-" ) 
        {
            $net_x = explode("*", $u_net_code_1);
            $u_net_code =$net_x[0];
            

            if (strpos($net_x[1], 'MONEY') !== false) {

                $t_type = "BANK_TRANSFER";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return $d->trans_type == $t_type;
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code) {
                    return $d->moneytransfer[0]->net_code == $u_net_code;
                });
            }
            else if (strpos($net_x[1], 'TNEB') !== false) {

                $t_type = "BILL_PAYMENT";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return $d->trans_type == $t_type;
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            else
            {
                $t_type = [];
                $t_type[0] = "WEB_RECHARGE";
                $t_type[1] = "API_RECHARGE";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return ($d->trans_type == $t_type[0] || $d->trans_type == $t_type[1]);
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code){
                    return $d->newrecharge1[0]->net_code == $u_net_code;
                });
            }

            
        }
        else
        {
            $dc5 = $dc4;
        }

        $dc6 = $dc5;

        $d2 = $net_2->select('net_code','net_name')->get();

        //-----------------------------------------------------
        //Total Calculation-------------------------------------------------------
        //recharge
        $wsua_tot = 0;
        $wsut_tot = 0;
        $wfua_tot = 0;
        $wfut_tot = 0;
        $wrea_tot = 0;
        $wret_tot = 0;
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
        //money
        $msua_tot = 0;
        $msut_tot = 0;
        $mfua_tot = 0;
        $mfut_tot = 0;
        $mrea_tot = 0;
        $mret_tot = 0;

        foreach($dc6 as $d)
        {
            
            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    
                    $rech_status = "";
                    $status = "";
                    $r_tot = 0;
                    $r_amt = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                        $r_amt = $d->newrecharge1[0]->rech_amount;
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                        $r_amt = $d->newrecharge1[1]->rech_amount;
                    }
                    if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $wfua_tot = floatval($wfua_tot) + floatval($r_amt);
                        $wfut_tot = floatval($wfut_tot) + floatval($r_tot);
                        $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                        $wret_tot = floatval($wret_tot) + floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $wsua_tot = floatval($wsua_tot) + floatval($r_amt);
                        $wsut_tot = floatval($wsut_tot) + floatval($r_tot);
                        $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                        $wret_tot = floatval($wret_tot) + floatval($r_tot);
                    }

                }
                
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;
                $r_amt = 0;
                

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $r_amt = $d->billpayment[0]->con_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $r_amt = $d->billpayment[1]->con_amount;
                }
                
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $efua_tot = floatval($efua_tot) + floatval($r_amt);
                    $efut_tot = floatval($efut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $esua_tot = floatval($esua_tot) + floatval($r_amt);
                    $esut_tot = floatval($esut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
               
                $rech_status = "";
                $status = "";
                $r_tot = 0;
                $r_amt = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $r_amt = $d->moneytransfer[0]->mt_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $r_amt = $d->moneytransfer[1]->mt_amount;
                    
                }
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $mfua_tot = floatval($mfua_tot) + floatval($r_amt);
                    $mfut_tot = floatval($mfut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $msua_tot = floatval($msua_tot) + floatval($r_amt);
                    $msut_tot = floatval($msut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
               
            }                                        
                            
        }

        $total = ['wsua_tot' => $wsua_tot, 'wsut_tot' => $wsut_tot, 'wfua_tot' => $wfua_tot, 'wfut_tot' => $wfut_tot, 
                    'wrea_tot' => $wrea_tot, 'wret_tot' => $wret_tot,
                    'esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot,
                    'msua_tot' => $msua_tot, 'msut_tot' => $msut_tot, 'mfua_tot' => $mfua_tot, 'mfut_tot' => $mfut_tot, 
                    'mrea_tot' => $mrea_tot, 'mret_tot' => $mret_tot];
       
       
        $rs = [];

        foreach($dc5 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('retailer.rechargereport_view', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2, 'total' => $total]); 

        
        
    }

   

    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
       
        $dc4 = RechargeReportAdmin::getRechargeReportDetails_new($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, "");

      
       if($u_net_code_1 != "-" ) 
        {
            $net_x = explode("*", $u_net_code_1);
            $u_net_code =$net_x[0];
            

            if (strpos($net_x[1], 'MONEY') !== false) {

                $t_type = "BANK_TRANSFER";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return $d->trans_type == $t_type;
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code) {
                    return $d->moneytransfer[0]->net_code == $u_net_code;
                });
            }
            else if (strpos($net_x[1], 'TNEB') !== false) {

                $t_type = "BILL_PAYMENT";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return $d->trans_type == $t_type;
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code){
                    return $d->billpayment[0]->net_code == $u_net_code;
                });
            }
            else
            {
                $t_type = [];
                $t_type[0] = "WEB_RECHARGE";
                $t_type[1] = "API_RECHARGE";

                $dc5 = $dc4->filter(function ($d) use ($t_type){
                    return ($d->trans_type == $t_type[0] || $d->trans_type == $t_type[1]);
                });

                $dc5 = $dc5->filter(function ($d) use ($u_net_code){
                    return $d->newrecharge1[0]->net_code == $u_net_code;
                });
            }

            
        }
        else
        {
            $dc5 = $dc4;
        }

        $d2 = $net_2->select('net_code','net_name')->get();
       

        $headings = ['NO', 'MOBILE', 'NETWORK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
         
        $content = [];

        $k = 0;

        foreach($dc5 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    $api_name = "";
                    $reply_id = "NA";
                    $reply_date = "";
                   

                    $reply_id = $d->newrecharge2->reply_opr_id;
                    $reply_date = $d->newrecharge2->reply_date;
                   

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $u_bal = $d->newrecharge1[0]->user_balance;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $u_bal = $d->newrecharge1[1]->user_balance;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                    }
                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "PENDING";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "SUCCESS";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }

                    $mode = "WEB";
                    if($d->trans_id != "")
                    {
                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                        $r_l = $matches[0][0];

                        $r_l = substr($r_l, -1);

                        if($r_l == "R")
                            $mode = "WEB";
                        else if($r_l == "A")
                            $mode = "API";
                        else if($r_l == "G")
                            $mode = "GPRS";
                        else if($r_l == "S")
                            $mode = "SMS";

                    }

                    
                    if($d->trans_option == 1)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, 
                        $d->newrecharge1[0]->rech_net_per."-".$d->newrecharge1[0]->rech_net_per_amt."-".$d->newrecharge1[0]->rech_net_surp,
                        $d->newrecharge1[0]->rech_total, $d->trans_id, $reply_id,
                        $d->newrecharge1[0]->rech_date, $reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                    else if($d->trans_option == 2)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, '',$d->newrecharge1[1]->rech_total,
                        $d->trans_id, '','','', $status,
                        number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                   
                }
                
               
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
               

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "PENDING";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
            
                if($d->trans_option == 1)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount, 
                        $d->billpayment[0]->con_net_per."-".$d->billpayment[0]->con_net_per_amt."-".$d->billpayment[0]->con_net_surp,
                        $d->billpayment[0]->con_total, $d->trans_id, $d->billpayment[0]->reply_opr_id,
                        $d->billpayment[0]->created_at, $d->billpayment[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount,'',
                        $d->billpayment[0]->con_total, $d->trans_id, '','','',
                        $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
               
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                
                if($d->trans_option == 1)
                {
                   
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                        $d->moneytransfer[0]->mt_amount, 
                        $d->moneytransfer[0]->mt_per."-".$d->moneytransfer[0]->mt_per_amt."-".$d->moneytransfer[0]->mt_surp,
                        $d->moneytransfer[0]->mt_total, $d->trans_id, $d->moneytransfer[0]->mt_reply_id,
                        $d->moneytransfer[0]->mt_date, $d->moneytransfer[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];

                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                    $d->moneytransfer[0]->mt_amount, '',
                    $d->moneytransfer[0]->mt_total, $d->trans_id, '','','',
                    $status, number_format($o_bal,2, ".", ""),
                    number_format($u_bal,2, ".", "")];

                }
               
            }                                        
                  
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $ob->user."_Recharge_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');


    }


    public function viewdate_apipartner(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $n_code = trim($request->net_code);

              
        $d1 = RechargeReportRetailer::getRechargeReportDetails($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, $n_code);
                 
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

   
        $re_tot = 0;
        $ce_tot = 0;
        $su_tot = 0;
        $fa_tot = 0;
        $pe_tot = 0;
        $su = 0;
        $fa = 0;
        $pe = 0;
        $j = 0;
        foreach($d1 as $r)
        {
            if($r->rech_status == "SUCCESS" )
            {
                $su_tot = floatval($su_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $su++;
            }
            else if($r->rech_status == "FAILURE" && $r->rech_option == "2")
            {
                $fa_tot = floatval($fa_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $fa++;
            }
            else if($r->rech_status == "PENDING" && $r->rech_option == "0")
            {
                $pe_tot = floatval($pe_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $pe++;
            }

            
            
        }
        
        $total = ['re_tot' => $re_tot, 'ce_tot' => $ce_tot, 'su_tot' => $su_tot, 'fa_tot' => $fa_tot, 'pe_tot' => $pe_tot, 'no_su' => $su, 'no_fa' => $fa, 'no_pe' => $pe];
       
       
        $rs = [];

        foreach($d1 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        $offset = ($page-1) * $perPage;

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('user.rechargereport_apipartner_view', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'total' => $total]); 

        
    }

    public function viewdate_apipartner_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $n_code = trim($request->net_code);

        $rs = [];
        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                                ->whereBetween('created_at', [$f_date, $t_date])
                                ->where('user_name', '=', $u_name)
                                ->orderBy('id', 'asc')->get(); 
        

        $j = 0;
        foreach($d1 as $d)
        {
            $data = null;
            if($d->trans_type == 'API_RECHARGE')
            {
                
                $data = $this->getAPINormalData($d->trans_option, $d->trans_id);

                if($data != null)
                {
                    $rs[$j][0] = $data;
                    $j++;
                }
            }
            
        }
                        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

       
        $rs2 = [];
        $j = 0;
        if($u_status == "SUCCESS")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "SUCCESS" )
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else if($u_status == "FAILURE")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "PENDING" ||  $r->rech_status == "FAILURE")
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                    else if($u_mobile == "")
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                }
            }
        }

        $rs3 = [];
        $j = 0;
        // Recharge Amount
        foreach($rs2 as $d)
        {
            foreach($d[0] as $r)
            {
                if($u_amount != "" &&  $r->rech_amount == $u_amount)
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($u_amount == "")
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }

        $rs4 = [];
        $j = 0;
        // Recharge Amount
        foreach($rs3 as $d)
        {
            foreach($d[0] as $r)
            {
                if($n_code != "-" &&  $r->net_code == $n_code)
                {
                    $rs4[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($n_code == "-")
                {
                    $rs4[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }
        
        $re_tot = 0;
        $ce_tot = 0;
        $su_tot = 0;
        $fa_tot = 0;
        $pe_tot = 0;
        $su = 0;
        $fa = 0;
        $pe = 0;
        $j = 0;
        foreach($rs4 as $d)
        {
            foreach($d[0] as $r)
            {
                if($r->rech_status == "SUCCESS" )
                {
                    $su_tot = floatval($su_tot) + floatval($r->rech_total);
                    $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                    $su++;
                }
                else if($r->rech_status == "FAILURE" )
                {
                    $fa_tot = floatval($fa_tot) + floatval($r->rech_total);
                    $fa++;
                }
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
            }
        }
        
        $total = ['re_tot' => $re_tot, 'ce_tot' => $ce_tot, 'su_tot' => $su_tot, 'fa_tot' => $fa_tot, 'pe_tot' => $pe_tot, 'no_su' => $su, 'no_fa' => $fa, 'no_pe' => $pe];
       
       
        $headings = ['NO', 'MOBILE', 'NETWORK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
        'TRN ID', 'API TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
        'O.BAL', 'C.BAL'];

        $j = 1;

        $content = [];

        $k = 0;

        foreach($rs4 as $f1)
        {
            $f = $f1[0];
                                                
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f[0]->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            if($f[0]->rech_status == "PENDING")
            {
                $reply_id = "";
                $reply_date = "";
                if($f[0]->userrecharge->rech_status == "PENDING")
                    $rech_status = "PENDING";
                else
                {
                    $rech_status = "FAILURE";
                    $reply_id = $f[0]->userrecharge->reply_opr_id;
                    $reply_date = $f[0]->userrecharge->reply_date;
                }
                
                $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);

            }
            else if ($f[0]->rech_status == "SUCCESS")
            {
                $reply_id = $f[0]->userrecharge->reply_opr_id;
                $reply_date = $f[0]->userrecharge->reply_date;
                $rech_status = "SUCCESS";
                $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);
            }
            else if ($f[0]->rech_status == "FAILURE")
            {
                $reply_id = $f[0]->userrecharge->reply_opr_id;
                $reply_date = $f[0]->userrecharge->reply_date;
                $rech_status = "FAILURE";
                $o_bal = floatval($f[0]->user_balance) - floatval($f[0]->rech_total);
                $z = 1;
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f[0]->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');


            if($z == 0)
            {
                $content[$k++] = [$j, $f[0]->rech_mobile, $net_name, $f[0]->rech_amount, 
                        $f[0]->rech_net_per."-".$f[0]->rech_net_per_amt."-".$f[0]->rech_net_surp, $f[0]->rech_total, 
                        $f[0]->trans_id, $f[0]->api_trans_id, $reply_id, $f[0]->rech_date, $reply_date, $rech_status, 
                        $o_bal, $c_bal];
            }
            else
            {
                $content[$k++] = [$j, $f[0]->rech_mobile, $net_name, $f[0]->rech_amount, 
                        '', $f[0]->rech_total, $f[0]->trans_id, '', '', '', '', $rech_status, 
                        $o_bal, $c_bal];

            }
                                        
            $j++;
        }

        $cc = [$headings, $content];

        $tit = $ob->user."_Recharge_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){

                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }


    public function getNormalData($trans_option, $trans_id)
    {
        $rech_2 = new UserRechargePaymentDetails;
        $data = null;

        if($trans_option == '1')
        {
            $data = $rech_2->with(['userrecharge'])->where(function($query){
                                                        return $query
                                                        ->where('rech_status', '=', 'PENDING')
                                                        ->orWhere('rech_status', '=', 'SUCCESS');
                                                    })
                                                    ->where('trans_id', '=', $trans_id)->get();
            
        }
        else if($trans_option == '2')
        {
            $data = $rech_2->with(['userrecharge'])->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])
                                                    ->get();
        
        }

        return $data;

    }

    public function getAPINormalData($trans_option, $trans_id)
    {
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $data = null;

        if($trans_option == '1')
        {
            $data = $rech_a_2->with(['userrecharge'])->where(function($query){
                                                        return $query
                                                        ->where('rech_status', '=', 'PENDING')
                                                        ->orWhere('rech_status', '=', 'SUCCESS');
                                                    })
                                                    ->where('trans_id', '=', $trans_id)->get();
            
        }
        else if($trans_option == '2')
        {
            $data = $rech_a_2->with(['userrecharge'])->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])
                                                    ->get();
        
        }

        return $data;

    }

}
