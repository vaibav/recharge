<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBankAgentDetails;
use App\Models\NetworkDetails;

use App\Libraries\GetCommon;
use App\Libraries\BankTransfer;
use App\Libraries\TransactionBank;
use App\Libraries\PercentageCalculation;

class MoneyTransferController extends Controller
{
    //
    public function index_money_new(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $msisdn = "0";
        $rem_name = "NONE";
        $data = [];
        $beni = [];

        if($request->session()->has('customer_msisdn'))
        {
            $msisdn = $request->session()->get('customer_msisdn');
            $res_data = $request->session()->get('result_data');
            $rem_name = $this->getRemitterName($msisdn, $ob->user);
            list($data, $beni) = $this->fetchCustomerDetails($res_data);
        }

        $agent_id = $this->getAgentid($ob->user);

        return view('user.money_send_new', ['user' => $ob, 'msisdn' => $msisdn, 'rem_name' => $rem_name, 'agent_id' => $agent_id, 'data' => $data, 'beni' => $beni]);
    }


    public function store(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $c_msisdn = trim($request->c_msisdn);
        $agent_id = trim($request->agent_id);
        $ben_id = trim($request->ben_id);
        $b_code = trim($request->bank_code);
        $b_accno = trim($request->cus_acc_no);
        $b_name = trim($request->cus_name);
        $b_mode = trim($request->bank_mode);
        $b_amt = trim($request->cus_amt);

        $trans_id = "VBMR".rand(10000000,99999999);
        $otp_status ="0";
        $rep_trans_id = "";
        $remarks ="";

        $this->validate($request, ['cus_acc_no' => 'required'], 
                                ['cus_acc_no.required' => ' Account No is required.']);

        if($b_code != "" && $b_accno != "" && $b_mode != "" && $b_amt != "")
        {
            $data = ['agent_id' => $agent_id,
                        'msisdn' => $c_msisdn,
                        'ben_id' => $ben_id,
                        'client_trans_id' => $trans_id,
                        'transfer_type' => $b_mode,
                        'amount' => $b_amt,
                        'beneficiary_id' => $ben_id,
                        'beneficiary_account_no' => $b_accno,
                        'beneficiary_bank_code' => $b_code,
                        'beneficiary_name' => $b_name,
                        'code' => $ob->code,
                        'user' => $ob->user,
                        'ubal' => $ob->ubal,
                        'api_trans_id' => "",
                        'r_mode' => 'WEB'
                    ];

            list($z, $msg, $otp_status, $rep_trans_id, $remarks, $res) = TransactionBank::add($data);

            if($z == 0)
                $op = $msg;
            else if($z == 1)
                $op = "Low User Balance...";
            else if($z == 2)
                $op = "Account is Inactivated...";
            else if($z == 3)
                $op = "Account No is Already Registered...";
            else if($z == 4)
                $op = $msg;
            else if($z == 5)
                $op = "PENDING...";
        }


        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'trans_id' => $trans_id, 'rep_trans_id' => $rep_trans_id, 'otp_status' => $otp_status, 'remarks' => $remarks ]);

        //return view('user.money_send', ['user' => $ob, 'msisdn' => $msisdn, 'rem_name' => $rem_name, 'agent_id' => $agent_id, 'data' => $data, 'beni' => $beni]);
    }

    public function store_otp(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $c_msisdn = trim($request->c_msisdn);
        $agent_id = trim($request->agent_id);
        $trans_id = trim($request->trans_id);
        $rep_trans_id = trim($request->rep_trans_id);
        $otp = trim($request->otp);
        
        $z1 = 0;
        $op = "";
        $otp_status ="0";
        $remarks ="";
        $trans_status = "";
        $msg = "";


        //echo $c_msisdn."--".$agent_id."--".$trans_id."--".$rep_trans_id."--".$otp;

       

        if($trans_id != "" && $rep_trans_id != "" && $otp != "")
        {
            $data = ['user_name' => $ob->user,
                        'msisdn' => $c_msisdn,
                        'agent_id' => $agent_id,
                        'trans_id' => $rep_trans_id,
                        'u_trans_id' => $trans_id,
                        'otp' =>  $otp,
                        'r_mode' => "WEB"
                        ];

            list($z, $msg, $trans_status) = TransactionBank::add_otp($data);

            if($z == 0)
            {
                $op = $msg;
                $z1 = 10;
            }
            else if($z == 1)
            {
                $op = "Mode web is not Selected...";
                $z1 = 11;
            }
            else if($z == 2)
            {
                $op = "Account is Inactivated...";
                $z1 = 12;
            }
            else if($z == 3)
            {
                $op = "Account No is Already Registered...";
                $z1 = 13;
            }
            else if($z == 4)
            {
                $op = $msg;
                $z1 = 14;
            }
            else if($z == 5)
            {
                $op =  "PENDING...";
                $z1 = 15;
            }
               
           
        }


        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1, 'trans_id' => $trans_id, 'rep_trans_id' => "", 'otp_status' => $trans_status, 'remarks' => "" ]);

        
    }

    public function getRemitterName($msisdn, $user_name)
    {
        $rem_1 = new UserBankRemitterDetails;

        $rem_name = "NONE";

        $d1 = $rem_1->select('user_rem_name')
                    ->where([['user_name', '=', $user_name], ['msisdn', '=', $msisdn]])->get();
        
        foreach($d1 as $d)
        {
            $rem_name = $d->user_rem_name;
        }
        
        return $rem_name;
    }

    public function getAgentid($user_name)
    {
        $agen_1 = new UserBankAgentDetails;

        $agent_id = "NONE";

        $d1 = $agen_1->select('agent_id')->where('user_name', '=', $user_name)->get();
        
        foreach($d1 as $d)
        {
            $agent_id = $d->agent_id;
        }
        
        return $agent_id;
    }

   

    public function fetchCustomerDetails($res_content)
    {
       
        $data = [];

        $result = $this->resultFormatter($res_content);

        $result = json_decode($result, true);

        $data['msisdn'] = "";
        $data['user_name'] = "";
        $data['total_limit'] = "";
        $data['consume_limit'] = "";
        $data['remaining_limit'] = "";
        $data['beneficiary'] = "";
        $beni = [];

        foreach($result as $key => $value)
        {
            if($key == "msisdn")
            {
                $data['msisdn'] = $value;
            }
            else if($key == "user_name")
            {
                $data['user_name'] = $value;
            }
            else if($key == "total_limit")
            {
                $data['total_limit'] = $value;
            }
            else if($key == "consume_limit")
            {
                $data['consume_limit'] = $value;
            }
            else if($key == "remaining_limit")
            {
                $data['remaining_limit'] = $value;
            }
            else if($key == "beneficiary")
            {
                $data['beneficiary'] = $value;
            }
        }
        
        $result1 = $this->resultFormatter($data['beneficiary']);

        //$result1 = json_decode($result1, true);

        if(!empty($result1)) 
        {
            foreach($result1 as $d)
            {
                if(!empty($d)) 
                {
                    $b_id = "";
                    $b_name = "";
                    $b_accno = "";
                    $b_bank = "";
                    $b_bcode = "";
                    $b_ifsc = "";

                    foreach($d as $key => $value)
                    {
                        if($key == "beneficiary_id")
                        {
                            $b_id = $value;
                        }
                        else if($key == "beneficiary_name")
                        {
                            $b_name = $value;
                        }
                        else if($key == "beneficiary_account_no")
                        {
                            $b_accno = $value;
                        }
                        else if($key == "beneficiary_bank_name")
                        {
                            $b_bank = $value;
                        }
                        else if($key == "beneficiary_bank_code")
                        {
                            $b_bcode = $value;
                        }
                        else if($key == "beneficiary_ifsc_code")
                        {
                            $b_ifsc = $value;
                        }
                    }

                    if($b_id != "" && $b_accno != "")
                    {
                        array_push($beni, ['b_id' => $b_id, 'b_name' => $b_name, 'b_accno' => $b_accno, 'b_bank' => $b_bank, 'b_bcode' => $b_bcode, 'b_ifsc' => $b_ifsc]);
                    }
                }
               
            }
            
        }

         
        return array($data, $beni);
    }

    public function getRetailerPer(Request $request)
    {
        $net_1 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $user_name = $ob->user;
        $mt_amount = trim($request->cus_amt);
        $r_mode = trim($request->r_mode);

        $d1 = [];
        if(floatval($mt_amount) > 5000)
        {
            $d1 = $net_1->select('net_code')->where('net_name', '=', "MONEY TRANSFER 1")->get();
        }
        else
        {
            $d1 = $net_1->select('net_code')->where('net_name', '=', "MONEY TRANSFER")->get();
        }


        $net_code = 0;

        if($d1->count() > 0)
        {
            $net_code = $d1[0]->net_code;
        }

        list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $mt_amount, $r_mode);

        $arr = ['amt' => $mt_amount, 'per' => $per, 'per_amt' => $peramt, 'sur' => $sur, 'netamt' => $netamt];
                    
        
        return response()->json($arr, 200);
    }

    public function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);

        return $res;
    }


}
