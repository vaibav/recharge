<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberPlat;

class BankRemitterVerifyController extends Controller
{
    //
    public function remitterCheck(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobileNo' => 'required|regex:/^[6789]\d{9}$/|integer|digits:10',
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $mobileNo  = trim($request->mobileNo);
        $amount    = "1";

        $remitterStatus = 0;
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";
        $remitter    = [];
        $beneficiary = [];

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$mobileNo.time();
        $sess = substr($sess,-20);
        
        list($resultStatus, $errorMsg) = $this->remitterVerify($mobileNo, $sess);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay.cgi";

            $querString = "SESSION=$sess\r\nType=5\r\nNUMBER=$mobileNo\r\n";
            $querString = $querString."AMOUNT=1.00\r\nAMOUNT_ALL=1.00";

            $response = CyberPlat::callAPI($payUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);

               

                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "ADDINFO")){
                        $addInfo = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }


                //$errorData  = CyberPlat::getSplitData($result[4]);
                //$statusData = CyberPlat::getSplitData($result[5]);

                if($errorData == 0 && $statusData == 0) 
                {
                    if($addInfo != "-") 
                    {
                        $normalData     = urldecode($addInfo);
                        $jsonData       = json_decode($normalData);
                        $remitterStatus = $jsonData->status;
                        $userData       = $jsonData->data;
                        $remitter       = $userData->remitter;

                        if(isset( $userData->beneficiary )) {
                            //key exists, do stuff
                            $beneficiary    = $userData->beneficiary;
                        }
                        


                    }

                    return response()->json(['status' => "SUCCESS", 'message' => 'success', 'remitter' =>  $remitter, 'beneficiary' => $beneficiary], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => "FAILURE", 'message' => $oprErrorMsg, 'remitter' =>  $remitter, 'beneficiary' => $beneficiary], 200);
                    }
                    else {
                        return response()->json(['status' => "FAILURE", 'message' => CyberPlat::getError($errorData), 'remitter' =>  $remitter, 'beneficiary' => $beneficiary], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => "FAILURE", 'message' => 'Bad Response', 'remitterStatus' =>  $remitterStatus], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => "FAILURE", 'message' => $errorMsg, 'remitterStatus' =>  $remitterStatus], 200); 
        }

    }


    public function remitterVerify($mobileNo, $sess)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $querString = "SESSION=$sess\r\nType=5\r\nNUMBER=$mobileNo\r\n";
        $querString = $querString."AMOUNT=1.00\r\nAMOUNT_ALL=1.00";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
               
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }

    public function getResult(Request $request)
    {
        $data = "%7B%22status%22%3A%22Transaction%20Successful%22%2C%22statuscode%22%3A%22TXN%22%2C%22data%22%3A%7B%22beneficiary%22%3A%5B%7B%22imps%22%3A%221%22%2C%22status%22%3A%221%22%2C%22account%22%3A%22039301506293%22%2C%22name%22%3A%22TUSHAR%20SINHA%22%2C%22mobile%22%3A%229892651668%22%2C%22ifsc%22%3A%22ICIC0000393%22%2C%22bank%22%3A%22ICICI%20BANK%20LIMITED%22%2C%22last_success_name%22%3A%22TUSHAR%20SINHA%22%2C%22id%22%3A%22412568%22%2C%22last_success_date%22%3A%2223-10-18%2015%3A41%22%2C%22last_success_imps%22%3A%221%22%7D%2C%7B%22imps%22%3A%221%22%2C%22status%22%3A%221%22%2C%22account%22%3A%22039301506293%22%2C%22name%22%3A%22TUSHAR%20SINHA%22%2C%22mobile%22%3A%229892651668%22%2C%22ifsc%22%3A%22ICIC0000339%22%2C%22bank%22%3A%22ICICI%20BANK%22%2C%22last_success_name%22%3A%22TUSHAR%20SINHA%22%2C%22id%22%3A%221021754%22%2C%22last_success_date%22%3A%2213-12-18%2012%3A06%22%2C%22last_success_imps%22%3A%221%22%7D%2C%7B%22imps%22%3A%220%22%2C%22status%22%3A%220%22%2C%22account%22%3A%22103001503633%22%2C%22name%22%3A%22BHAWANA%20SRIVASTAVA%22%2C%22mobile%22%3A%227738266323%22%2C%22ifsc%22%3A%22ICIC0001030%22%2C%22bank%22%3A%22ICICI%20BANK%22%2C%22last_success_name%22%3A%22%22%2C%22id%22%3A%221408640%22%2C%22last_success_date%22%3A%22%22%2C%22last_success_imps%22%3A%22%22%7D%2C%7B%22imps%22%3A%220%22%2C%22status%22%3A%220%22%2C%22account%22%3A%2220085265302%22%2C%22name%22%3A%22AWDHSH%20KHARWAR%22%2C%22mobile%22%3A%228898112727%22%2C%22ifsc%22%3A%22SBIN0004664%22%2C%22bank%22%3A%22STATE%20BANK%20OF%20INDIA%22%2C%22last_success_name%22%3A%22%22%2C%22id%22%3A%221416970%22%2C%22last_success_date%22%3A%22%22%2C%22last_success_imps%22%3A%22%22%7D%5D%2C%22remitter%22%3A%7B%22kycstatus%22%3A%220%22%2C%22consumedlimit%22%3A0%2C%22name%22%3A%22Tushar%22%2C%22remaininglimit%22%3A25000%2C%22mobile%22%3A%229892651668%22%2C%22is_verified%22%3A1%2C%22state%22%3A%22MAHARASHTRA%22%2C%22pincode%22%3A%22400708%22%2C%22kycdocs%22%3A%22REQUIRED%22%2C%22perm_txn_limit%22%3A5000%2C%22city%22%3A%22NAVI%20MUMBAI%22%2C%22id%22%3A%22119906%22%2C%22address%22%3A%22NAVI%20MUMBAI%20THANE%20MAHARASHTRA%22%7D%7D%7D";

        $normalData   = urldecode($data);
        $jsonData     = json_decode($normalData);
        $userData     = $jsonData->data;

        echo $normalData;
        //echo $jsonData;

        $remitter =  $userData->remitter;
        $beneficiary = $userData->beneficiary;

        print "<pre>";
        print_r($remitter);

        print "<pre>";
        print_r($beneficiary);

        echo "ID :".$remitter->id;

        foreach($beneficiary as $d)
        {
            echo $d->name."<br>";
        }


    }
    

    
}