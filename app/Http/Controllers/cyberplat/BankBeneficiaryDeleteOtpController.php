<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\CyberPlat;

class BankBeneficiaryDeleteOtpController extends Controller
{
    //
    public function benDelete(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'remId' => 'required',
            'benId' => 'required'
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $remId = trim($request->remId);
        $benId = trim($request->benId);
        $otc   = trim($request->otc);
      
        $amount     = "1";

        $isVerified = 0;
        $transId    = 0;
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$remId.time();
        $sess = substr($sess,-20);

        $querString = "SESSION=$sess\r\nType=23\r\nremId=$remId\r\nbenId=$benId\r\notc=$otc\r\n";
        $querString = $querString."AMOUNT=1.00\r\nAMOUNT_ALL=1.00";
        
        list($resultStatus, $errorMsg) = $this->benVerify($querString);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay.cgi";

            $response = CyberPlat::callAPI($payUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);


                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "ADDINFO")){
                        $addInfo = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }

                if($errorData == 0 && $statusData == 0) 
                {
                    return response()->json(['status' => 'SUCCESS', 'message' => 'success'], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => 'FAILURE', 'message' => $oprErrorMsg], 200);
                    }
                    else {
                        return response()->json(['status' => 'FAILURE', 'message' => CyberPlat::getError($errorData)], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => 'FAILURE', 'message' => 'Bad Response'], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => 'FAILURE', 'message' => $errorMsg], 200); 
        }

    }

    public function benVerify($querString)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }

    
}