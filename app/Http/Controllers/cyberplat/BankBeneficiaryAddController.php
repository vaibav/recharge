<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\CyberPlat;

class BankBeneficiaryAddController extends Controller
{
    //
    public function benAdd(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobileNo' => 'required|regex:/^[6789]\d{9}$/|integer|digits:10',
            'firstName' => 'required',
            'lastName' => 'required',
            'remId' => 'required|integer',
            'benAccount' => 'required|integer',
            'benIFSC' => 'required',
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $mobileNo   = trim($request->mobileNo);
        $firstName  = trim($request->firstName);
        $lastName   = trim($request->lastName);
        $remId      = trim($request->remId);
        $benAccount = trim($request->benAccount);
        $benIFSC    = trim($request->benIFSC);
        $amount     = "1";

        $isVerified = 0;
        $transId    = 0;
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$mobileNo.time();
        $sess = substr($sess,-20);

        $querString = "SESSION=$sess\r\nType=4\r\nNUMBER=$mobileNo\r\nlName=$lastName\r\nfName=$firstName\r\nremId=$remId\r\n";
        $querString = $querString."benAccount=$benAccount\r\nbenIFSC=$benIFSC\r\nAMOUNT=1.00\r\nAMOUNT_ALL=1.00";
        
        list($resultStatus, $errorMsg) = $this->benVerify($querString);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay.cgi";

            $response = CyberPlat::callAPI($payUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);


                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "ADDINFO")){
                        $addInfo = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }

                if($errorData == 0 && $statusData == 0) 
                {
                    if($addInfo != "-") 
                    {
                        $normalData   = urldecode($addInfo);
                        $jsonData     = json_decode($normalData);
                        $remitterData = $jsonData->data;
                        $beneficiary  = $remitterData->beneficiary;
                        $transId      = $beneficiary->id;
                    }

                    return response()->json(['status' => 'SUCCESS', 'message' => 'success',  'transId' => $transId], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => 'FAILURE', 'message' => $oprErrorMsg, 'transId' => $transId], 200);
                    }
                    else {
                        return response()->json(['status' => 'FAILURE', 'message' => CyberPlat::getError($errorData), 'transId' => $transId], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => 'FAILURE', 'message' => 'Bad Response', 'transId' => $transId], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => 'FAILURE', 'message' => $errorMsg, 'transId' => $transId], 200); 
        }

    }

    public function benVerify($querString)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }

    
}