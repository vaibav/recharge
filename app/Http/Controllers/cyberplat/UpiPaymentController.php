<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\CyberPlat;

class UpiPaymentController extends Controller
{
    //
    public function generatePayment(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'upiId' => 'required',
            'billNo' => 'required',
            'amount' => 'required|integer',
        ]);

        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $upiId   = trim($request->upiId);
        $billNo  = trim($request->billNo);
        $amount  = trim($request->amount);

        
        $isVerified  = 0;
        $transId     = 0;
        $errorData   = -1;
        $statusData  = -1;
        $authCode    = "-";
        $oprErrorMsg = "-";
        $oprId       = "0";
        $transStatus = "-";
        $errMsg      = "-";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$billNo.time();
        $sess = substr($sess,-20);

        $queryString  = "SESSION=$sess\r\nType=1\r\nMerchantId=ommuruga.ruthra-4@okhdfcbank\r\nMerchantName=Ruthra Moorthi\nSubMerchantId=\r\n";
        $queryString .= "SubMerchantName=\r\nBillNumber=$billNo\r\nNUMBER=$upiId\r\nCOMMENT=vaibav upi payment\r\n";
        $queryString .= "AMOUNT=$amount";
        
        list($resultStatus, $errorMsg) = $this->upiVerify($queryString);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/upi/upi_pay.cgi";

            $response = CyberPlat::callAPI($payUrl, $queryString);

            if ($response) 
            {
                $result = explode("\n", $response);

                print "<pre>";
                print_r($result);

                // file_put_contents(base_path().'/public/sample/bank_result_url.txt', print_r($result), FILE_APPEND);


                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "TRANSID")){
                        $transId = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "AUTHCODE")){
                        $authCode = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "TRNXSTATUS")){
                        $transStatus = CyberPlat::getSplitData($res);
                    
                    if(CyberPlat::checkString($res, "ERRMSG")){
                        $errMsg = CyberPlat::getSplitData($res);
                    }}
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }

                if($errorData == 0 && $statusData == 0) 
                {
                    
                    return response()->json(['status' => "SUCCESS", 'message' => 'success',  
                                            'transId' =>  $transId, 'authCode' => $authCode, 'transStaus' => $transStatus, 'msg' => $errorMsg], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => "FAILURE", 'message' => $oprErrorMsg, 
                                                    'transId' =>  '-', 'authCode' => '-', 'transStaus' => '-', 'msg' => '-'], 200);
                    }
                    else {
                        return response()->json(['status' => "FAILURE", 'message' => CyberPlat::getError($errorData), 
                                                    'transId' =>  '-', 'authCode' => '-', 'transStaus' => '-', 'msg' => '-'], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => "FAILURE", 'message' => 'Bad Response', 
                                                        'transId' =>  '-', 'authCode' => '-', 'transStaus' => '-', 'msg' => '-'], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => "FAILURE", 'message' => $errorMsg, 
                                                'transId' =>  '-', 'authCode' => '-', 'transStaus' => '-', 'msg' => '-'], 200); 
        }

    }

    public function upiVerify($querString)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/upi/upi_pay_check.cgi";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            print "<pre>";
                print_r($result);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }

    
}