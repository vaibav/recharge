<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberPlat;

use App\Models\AdDetails;

class EbController extends Controller
{
    //
    public function index(Request $request)
	{
       
        //$accountNo = "05141006847";
        //$accountNo = "05140003286";
        $accountNo = trim($request->accountno);
        $amount    = "1";

        $billStatus   = "-";
        $customerName = "-";
        $billAmount   = "-";
        $billDueDate  = "-";
        $finalStatus  = false;
        $finalMessage = "-";
        
        $checkUrl = "https://in.cyberplat.com/cgi-bin/bbps/bbps_pay_check.cgi/674";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$accountNo.time();
        $sess = substr($sess,-20);
        $querString = "SESSION=$sess\r\nAgentId=BD01BD11AGTA30005786\r\nChannel=Agent\r\nNUMBER=$accountNo\r\nPaymentMethod=Cash\r\nACCOUNT=\r\n";
        $querString = $querString."AMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nCOMMENT=Test recharge";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            print "<pre>";
            print_r($result);

            $errorData  = CyberPlat::getSplitData($result[4]);
            $statusData = CyberPlat::getSplitData($result[5]);
            
            if($errorData == 0 && $statusData == 0)
            {

                if($this->checkString($result[7], "ADDINFO")) 
                {
                    $userData = CyberPlat::getSplitData($result[7]);
                    $jsonData = json_decode($userData);

                    foreach($jsonData->billlist as $row) {
                        foreach($row as $key => $val) {
                            if($key == "billstatus") {
                                $billStatus = $val;
                            }
                            if($key == "customer_name") {
                                $customerName = $val;
                            }
                            if($key == "billamount") {
                                $billAmount = $val;
                            }
                            if($key == "billduedate") {
                                $billDueDate = $val;
                            }
                        }
                    }

                   
                    return response()->json(['status' => true, 'message' => 'success', 'billStatus' => $billStatus, 'billAmount' => $billAmount, 
                                                    'customerName' => $customerName, 'dueDate' => $billDueDate], 200);
                }
            }
            else {
                if($this->checkString($result[8], "OPERATOR_ERROR_MESSAGE")) 
                {
                    return response()->json(['status' => false, 'message' => $this->getSplitData($result[8]), 
                                                    'billStatus' => $billStatus, 'billAmount' => $billAmount, 
                                                    'customerName' => $customerName, 'dueDate' => $billDueDate], 200);
                }
            }
          
        } else {
            return response()->json(['status' => false, 'message' => 'Bad Response', 
                                                    'billStatus' => $billStatus, 'billAmount' => $billAmount, 
                                                    'customerName' => $customerName, 'dueDate' => $billDueDate], 200);
        }
        
    }

    public function payment(Request $request)
    {
        $accountNo = trim($request->accountno);
        $amount    = trim($request->amount);

        $billStatus = "-";
        $authCode   = "-";
        $transCode  = "-";
        $tranStatus = "-";
        $errorMsg   = "-";


        $checkUrl = "https://in.cyberplat.com/cgi-bin/bbps/bbps_pay.cgi/674";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$accountNo.time();
        $sess = substr($sess,-20);

        list($resultStatus, $errorMsg) = $this->Verify($accountNo, $sess, $amount);

        if($resultStatus)
        {
            //$sess = "FriJuly232021".$sessPrefix;
            $querString = "SESSION=$sess\r\nAgentId=BD01BD11AGTA30005786\r\nNUMBER=$accountNo\r\nACCOUNT=\r\n";
            $querString = $querString."AMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nTERMINAL_ID=123456\r\nTERM_ID=".CyberPlat::OP."\r\nCOMMENT=vaibav bill payment";

            //echo $querString;

            $response = CyberPlat::callAPI($checkUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);

                //dd($response);

                $errorData  = CyberPlat::getSplitData($result[4]);
                $statusData = CyberPlat::getSplitData($result[5]);
                
                if($errorData == 0 && $statusData == 0)
                {
                    if(CyberPlat::checkString($result[7], "ADDINFO")) 
                    {
                        $userData = CyberPlat::getSplitData($result[7]);
                        $jsonData = json_decode($userData);

                        $transCode = $jsonData->biller_approval_code;
                    }

                    if(CyberPlat::checkString($result[8], "AUTHCODE")) 
                    {
                        $authCode = CyberPlat::getSplitData($result[8]);
                    }

                    if(CyberPlat::checkString($result[9], "TRNXSTATUS")) 
                    {
                        $tranStatus = CyberPlat::getSplitData($result[9]);
                    }

                    return response()->json(['status' => true, 'message' => 'success', 'transCode' => $transCode,  'authCode' => $authCode, 'transStatus' => $tranStatus], 200);
                }
                else {
                    if(CyberPlat::checkString($result[8], "OPERATOR_ERROR_MESSAGE")) 
                    {
                        $errorMsg = $this->getSplitData($result[8]);
                    }
                    return response()->json(['status' => false, 'message' => $errorMsg, 'transCode' => $errorMsg, 'authCode' => $authCode, 'transStatus' => $tranStatus],  200);
                }
              
            } else {
                return response()->json(['status' => false, 'message' => $errorMsg, 'transCode' => $errorMsg, 'authCode' => '-', 'transStatus' => '-'], 200);
            }

        }
        else
        {
             return response()->json(['status' => false, 'message' => $errorMsg, 'transCode' => $errorMsg, 'authCode' => '-', 'transStatus' => '-'], 200);
        }
        

    }

    public function Verify($accountNo, $sess, $amount)
    {
        $resultStatus = false;
        $errorMessage = "None";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/bbps/bbps_pay_check.cgi/674";

        $querString = "SESSION=$sess\r\nNUMBER=$accountNo\r\nACCOUNT=\r\n";
        $querString = $querString."AMOUNT=$amount\r\nAMOUNT_ALL=$amount\r\nCOMMENT=Test recharge";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            //print "<pre>";
            //print_r($result);

            $errorData  = CyberPlat::getSplitData($result[4]);
            $statusData = CyberPlat::getSplitData($result[5]);
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
               
            }
             else 
             {
                if($this->checkString($result[8], "OPERATOR_ERROR_MESSAGE")) 
                {
                    $errorMessage = CyberPlat::getSplitData($result[8]);
                }
                else if($errorData == 21)
                {
                    $errorMessage = "Not enough funds for effecting the payment.";
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }

    public function callAPI($URI, $querString)
    {
        define('CERT','473F75E517169907C3918BCA2AB3D88B99CDB171');  //our testing key 
        define('CP_PASSWORD','Vaibav@143'); //our pwd

        $SD = 357355;
        $AP = 400135;
        $OP = 400136;

        $secKey = file_get_contents(base_path().'/public/cybor/private.key');
        $passwd = CP_PASSWORD;
        $serverCert = file_get_contents(base_path().'/public/cybor/mycert.pem');

        $querString = "CERT=".CERT."\r\nSD=$SD\r\nAP=$AP\r\nOP=$OP\r\n".$querString;

        // make SHA1RSA signature
        $pkeyid = openssl_pkey_get_private($secKey, $passwd);
        openssl_sign($querString, $signature, $pkeyid, OPENSSL_ALGO_SHA1);
        openssl_free_key($pkeyid);

        $encoded = base64_encode($signature);
        $encoded = chunk_split($encoded, 76, "\r\n");

        $signInMsg = "BEGIN\r\n" . $querString . "\r\nEND\r\nBEGIN SIGNATURE\r\n" . $encoded . "END SIGNATURE\r\n";

        $response = $this->get_query_result($signInMsg, $URI);

        return $response;
    }

    public function get_query_result($qs, $url)
    {
        $opts = array( 
            'http'=>array(
                'method'=>"POST",
                'header'=>array("Content-type: application/x-www-form-urlencoded\r\n".
                                "X-CyberPlat-Proto: SHA1RSA\r\n"),
                'content' => "inputmessage=".urlencode($qs)
            )
        ); 
        $context = stream_context_create($opts);    
        return file_get_contents($url, false, $context);
    }

    public function getSplitData($resultString)
    {
        if (strpos($resultString, '=') !== false) {
            $result = explode("=", $resultString);
            return $result[1];
        }
        else {
            return "";
        }
    }

    public function checkString($resultString, $parameter)
    {
        if (strpos($resultString, $parameter) !== false) {
            return true;
        }
        return false;
    }
}