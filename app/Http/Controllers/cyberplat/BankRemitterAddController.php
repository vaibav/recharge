<?php

namespace App\Http\Controllers\cyberplat;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberPlat;

use App\Models\AdDetails;

class BankRemitterAddController extends Controller
{
    //

    public function remitterCheck(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobileNo' => 'required|regex:/^[6789]\d{9}$/|integer|digits:10',
            'firstName' => 'required',
            'lastName' => 'required',
            'pinCode' => 'required|integer|digits:6',
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $mobileNo  = trim($request->mobileNo);
        $firstName = trim($request->firstName);
        $lastName  = trim($request->lastName);
        $pinCode   = trim($request->pinCode);
        $amount    = "1";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$mobileNo.time();
        $sess = substr($sess,-20);
        $querString = "SESSION=$sess\r\nType=0\r\nNUMBER=$mobileNo\r\nlName=$lastName\r\nfName=$firstName\r\nPin=$pinCode\r\n";
        $querString = $querString."AMOUNT=$amount\r\nAMOUNT_ALL=$amount";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            $errorData  = CyberPlat::getSplitData($result[4]);
            $statusData = CyberPlat::getSplitData($result[5]);

            if($errorData == 0 && $statusData == 0) {
                return response()->json(['status' => true, 'message' => 'Remitter Available'], 200);
            }
            else {
                if(CyberPlat::checkString($result[8], "OPERATOR_ERROR_MESSAGE"))  {
                    return response()->json(['status' => false, 'message' => CyberPlat::getSplitData($result[8])], 200);
                }
                else {
                    return response()->json(['status' => false, 'message' => 'Bad Response'], 200);
                }
            }

        }
        else {
            return response()->json(['status' => false, 'message' => 'Bad Response'], 200); 
        }
    }

    public function remitterAdd(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobileNo' => 'required|regex:/^[6789]\d{9}$/|integer|digits:10',
            'firstName' => 'required',
            'lastName' => 'required',
            'pinCode' => 'required|integer|digits:6',
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $mobileNo  = trim($request->mobileNo);
        $firstName = trim($request->firstName);
        $lastName  = trim($request->lastName);
        $pinCode   = trim($request->pinCode);
        $amount    = "1";

        $isVerified = 0;
        $transId    = 0;
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$mobileNo.time();
        $sess = substr($sess,-20);
        
        list($resultStatus, $errorMsg) = $this->remitterVerify($mobileNo, $firstName, $lastName, $pinCode, $sess);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay.cgi";

            $querString = "SESSION=$sess\r\nType=0\r\nNUMBER=$mobileNo\r\nlName=$lastName\r\nfName=$firstName\r\nPin=$pinCode\r\n";
            $querString = $querString."AMOUNT=1\r\nAMOUNT_ALL=1";

            $response = CyberPlat::callAPI($payUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);


                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "ADDINFO")){
                        $addInfo = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }


                //$errorData  = CyberPlat::getSplitData($result[4]);
                //$statusData = CyberPlat::getSplitData($result[5]);

                if($errorData == 0 && $statusData == 0) 
                {
                    if($addInfo != "-") 
                    {
                        $normalData   = urldecode($addInfo);
                        $jsonData     = json_decode($normalData);
                        $remitterData = $jsonData->data;

                        $remitter   = $remitterData->remitter;

                        $isVerified = $remitter->is_verified;
                        $transId    = $remitter->id;
                    }

                    return response()->json(['status' => "SUCCESS", 'message' => 'success', 'isVerified' => $isVerified,  'transId' => $transId], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => "FAILURE", 'message' => $oprErrorMsg,'isVerified' => $isVerified,  'transId' => $transId], 200);
                    }
                    else {
                        return response()->json(['status' => "FAILURE", 'message' => CyberPlat::getError($errorData), 'isVerified' => $isVerified,  'transId' => $transId], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => "FAILURE", 'message' => 'Bad Response', 'isVerified' => $isVerified,  'transId' => $transId], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => "FAILURE", 'message' => $errorMsg, 'isVerified' => $isVerified,  'transId' => $transId], 200); 
        }

    }


    public function remitterVerify($mobileNo, $firstName, $lastName, $pinCode, $sess)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $querString = "SESSION=$sess\r\nType=0\r\nNUMBER=$mobileNo\r\nlName=$lastName\r\nfName=$firstName\r\nPin=$pinCode\r\n";
        $querString = $querString."AMOUNT=1\r\nAMOUNT_ALL=1";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
               
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }


    public function remitterOtpCheck(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobileNo' => 'required|regex:/^[6789]\d{9}$/|integer|digits:10',
            'remId' => 'required',
            'otpNumber' => 'required',
        ]);
        if($validator->fails()){  
            $errors = $validator->errors();
            $message = '';
            foreach ($errors->all() as $error) {
               $message .= $error . ' ';
            }       
            $result         = ['Status' => false, 'message' => $message];
            return response()->json($result, 200);     
        }

        $mobileNo  = trim($request->mobileNo);
        $remId = trim($request->remId);
        $otpNumber  = trim($request->otpNumber);
        $amount    = "1";

        $isVerified = 0;
        $transId    = 0;
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $sessPrefix = rand(100, 300);
        $sess = $sessPrefix.$mobileNo.time();
        $sess = substr($sess,-20);
        
        list($resultStatus, $errorMsg) = $this->remitterOtpVerify($mobileNo, $remId, $otpNumber, $sess);

        if($resultStatus)
        {
            $payUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay.cgi";

            $querString = "SESSION=$sess\r\nType=24\r\nNUMBER=$mobileNo\r\nremId=$remId\r\notc=$otpNumber\r\n";
            $querString = $querString."AMOUNT=1.00\r\nAMOUNT_ALL=1.00";

            $response = CyberPlat::callAPI($payUrl, $querString);

            if ($response) 
            {
                $result = explode("\n", $response);


                foreach($result as $res)
                {
                    if(CyberPlat::checkString($res, "ERROR")) {
                        $errorData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "RESULT")){
                        $statusData = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "ADDINFO")){
                        $addInfo = CyberPlat::getSplitData($res);
                    }
                    if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                        $oprErrorMsg = CyberPlat::getSplitData($res);
                    }
                }


                //$errorData  = CyberPlat::getSplitData($result[4]);
                //$statusData = CyberPlat::getSplitData($result[5]);

                if($errorData == 0 && $statusData == 0) 
                {
                    if($addInfo != "-") 
                    {
                        $normalData   = urldecode($addInfo);
                        $jsonData     = json_decode($normalData);
                        $remitterData = $jsonData->data;

                        $remitter   = $remitterData->remitter;

                        $isVerified = $remitter->is_verified;
                        $transId    = $remitter->id;
                    }

                    return response()->json(['status' => "SUCCESS", 'message' => 'success', 'isVerified' => $isVerified,  'transId' => $transId], 200);
                }
                else {
                    if($oprErrorMsg != "-")  {
                        return response()->json(['status' => "FAILURE", 'message' => $oprErrorMsg,'isVerified' => $isVerified,  'transId' => $transId], 200);
                    }
                    else {
                        return response()->json(['status' => "FAILURE", 'message' => CyberPlat::getError($errorData), 'isVerified' => $isVerified,  'transId' => $transId], 200);
                    }
                }

            }
            else {
                return response()->json(['status' => "FAILURE", 'message' => 'Bad Response', 'isVerified' => $isVerified,  'transId' => $transId], 200); 
            }

        }
        else 
        {
            return response()->json(['status' => "FAILURE", 'message' => $errorMsg, 'isVerified' => $isVerified,  'transId' => $transId], 200); 
        }

    }

     public function remitterOtpVerify($mobileNo, $remId, $otpNumber, $sess)
    {
        $resultStatus = false;
        $errorMessage = "None";
        $errorData   = -1;
        $statusData  = -1;
        $addInfo     = "-";
        $oprErrorMsg = "-";

        $checkUrl = "https://in.cyberplat.com/cgi-bin/instp/instp_pay_check.cgi";

        $querString = "SESSION=$sess\r\nType=24\r\nNUMBER=$mobileNo\r\nremId=$remId\r\notc=$otpNumber\r\n";
        $querString = $querString."AMOUNT=1.00\r\nAMOUNT_ALL=1.00";

        $response = CyberPlat::callAPI($checkUrl, $querString);

        if ($response) 
        {
            $result = explode("\n", $response);

            foreach($result as $res)
            {
                if(CyberPlat::checkString($res, "ERROR")) {
                    $errorData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "RESULT")){
                    $statusData = CyberPlat::getSplitData($res);
                }
                if(CyberPlat::checkString($res, "OPERATOR_ERROR_MESSAGE")){
                    $oprErrorMsg = CyberPlat::getSplitData($res);
                }
            }
            
            if($errorData == 0 && $statusData == 0)
            {
                $resultStatus = true;
               
            }
            else 
            {
                if($oprErrorMsg != "-") 
                {
                    $errorMessage = $oprErrorMsg;
                }
                else 
                {
                    $errorMessage =  CyberPlat::getError($errorData);
                }
            }
        } 

        return [$resultStatus, $errorMessage];
    }
    

    
}