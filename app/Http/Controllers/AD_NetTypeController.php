<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\NetworkTypeDetails;
use App\Models\UserAccountBalanceDetails;

class AD_NetTypeController extends Controller
{
    //
    public function index(Request $request)
	{
        $data = NetworkTypeDetails::all();

        $ob = GetCommon::getUserDetails($request);

        return view('admin.ad_net_type', ['data' => $data, 'user' => $ob, 'title' => 'Network Type']);
		
    }

  
    public function store(Request $request)
	{
        $model = new NetworkTypeDetails;
        $op = "";


        $this->validate($request, [
            'net_type' => 'required'
        ],
        [
            'net_type.required' => ' The Network Type field is required.']);

        $cnt = $model->where('net_type_code', '=', $request->net_code)->count();
        
        if($cnt > 0)
        {
            // Update Record... 
            $model->where('net_type_code', '=', $request->net_code)->update(['net_type_name'=>$request->net_type, 'net_type_status'=>$request->net_type_status]);
            $op = "Record is updated Successfully...";
        }
        else
        {
            // Insert Record... 
            $model->net_type_tr_id = rand(100,999);
            $model->net_type_code = rand(100,999);
            $model->net_type_name = $request->net_type;
            $model->net_type_status = $request->net_type_status;

            $model->save();
            
            $op = "Record is inserted Successfully...";

        }

        
        $data = $model->all();

        //print_r($data);

        return redirect()->back()->with('result', [ 'output' => $op]);

		//return $request->net_type;
		
    }
    
    public function delete($net_code)
	{
        $model = new NetworkTypeDetails;
        $op = "";

        $cnt = $model->where('net_type_code', '=', $net_code)->count();
        
        if($cnt > 0)
        {
            // Delete Record... 
            $model->where('net_type_code', '=', $net_code)->update(['net_type_status' => '3']);
            //$model->where('net_type_code', '=', $net_code)->delete();
            $op = "Record is deleted Successfully...";
        }
        else
        {
                            
            $op = "No Record Found...";

        }

        $data = $model->all();


        return redirect()->back()->with('result', [ 'output' => $op]);
    }

}
