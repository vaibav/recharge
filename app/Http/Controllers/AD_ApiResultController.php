<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderResultDetails;
use App\Models\ApiProviderNetworkDetails;
use App\Models\NetworkDetails;

class AD_ApiResultController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = ApiProviderDetails::select('api_code','api_name')->orderby('api_name', 'asc')->get();

        return view('admin.ad_api_result', ['user' => $ob, 'api' => $d1, 'api2' => [], 'api_code' => ""]);
        
    }

    public function view($api_code, Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $d1 = ApiProviderDetails::select('api_code','api_name')->get();
        $d2 = ApiProviderResultDetails::select('api_res_tr_id','api_code', 'api_res_para_name', 'api_res_para_field', 'api_res_para_value')
                       ->where('api_code', '=', $api_code)->get();

        return view('admin.ad_api_result', ['user' => $ob, 'api' => $d1, 'api2' => $d2, 'api_code' => $api_code]);

        
    }

    public function store(Request $request)
	{
    
        $op = "No record Entered...";
        $cnt = 0;
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $res_tr_id = rand(1000,9999);
        $api_code = trim($request->api_code);
        $api_result_type = trim($request->api_result_type);
        $res_para_name = trim($request->api_res_para_name); 
        $res_para_field = trim($request->api_res_para_field); 
        $res_para_value = trim($request->api_res_para_value);
        
        if($res_para_value == "")
            $res_para_value = "-";

        if($res_para_name != 'STATUS 1')
        {
            if($res_para_name != 'STATUS 2')
            {
                if($res_para_name != 'DATA')
                {
                    if($res_para_name != 'ARRAY')
                    {
                        $cnt = ApiProviderResultDetails::where([['api_res_para_name', '=', $res_para_name], ['api_code', '=', $api_code]])->count();
                    }
                }
            }
        }

        if($cnt == 0)
        {

            // Insert API Result Record... 
            $data = [ 'api_res_tr_id' => $res_tr_id, 'api_code' => $api_code, 'api_result_type' => $api_result_type, 
                        'api_res_para_name' => $res_para_name, 'api_res_para_field' => $res_para_field, 'api_res_para_value' => $res_para_value,
                        'created_at' => $date_time, 'updated_at' => $date_time];
        
            $z =  ApiProviderResultDetails::insert($data);

            if($z > 0)
            {
                $op = "Api Result Entry is Added Successfully....";
            }
            
        }
        else
        {
            $op = "Error! Record is already inserted...";
        }

        
        return redirect()->back()->with('msg', $op);
    }

    public function delete($tr_id)
	{
        
        $op = "Error! No Action..";
        $api_code = 0;

        $d1 = ApiProviderResultDetails::where('api_res_tr_id', '=', $tr_id)->get();
        
        if($d1->count() > 0)
        {
            // Delete Record... 
            ApiProviderResultDetails::where('api_res_tr_id', '=', $tr_id)->delete();
            $op = "Record is deleted Successfully...";
            
        }
        else
        {
                            
            $op = "No Record Found...";

        }

       
        return redirect()->back()->with('msg', $op);

        
    }

    /**
     * Add Network Methods
     */

    public function index_network(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = ApiProviderDetails::orderby('api_name', 'asc')->get();
        $d2 = NetworkDetails::get();

        return view('admin.ad_api_provider_network', ['user' => $ob, 'api' => $d1, 'network' => $d2]);
        
    }

    public function get_api_network(Request $request)
	{
        
        $api_code = trim($request->api_code);
        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkDetails::get();
        $d2 = ApiProviderNetworkDetails::where('api_code', $api_code)->get();

        $str = "";

        $j = 1;
        foreach($d1 as $d)
        {
            $nt_short = "";
            $n_code = $d->net_code;
            foreach($d2 as $e)
            {
                if($e->net_code == $n_code) {
                    $nt_short = $e->api_net_short_code;
                }
            }

            $str = $str. "<tr><td style='font-size:12px;padding:2px 8px;'>";
            if($nt_short != "")
            {
                $str = $str. "<input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select[]' value = '".$d->net_code."' checked/></td>";
            }
            else
            {
                $str = $str. "<input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select[]' value = '".$d->net_code."' /></td>";

            }
            
            $str = $str. "<td style='font-size:12px;padding:5px 8px;'>".$d->net_name."</td>";

            $str = $str. "<td style='font-size:12px;padding:2px 8px;'>";
            $str = $str. "<input type='text' id='id_net_short_".$j."'  name='net_".$d->net_code."' value ='".$nt_short."' /></td></tr>";
            $j++;
        }

       echo $str;
        
    }

    public function store_network(Request $request)
	{
    
        $op = "No Entry is Added...";
        
        $ob = GetCommon::getUserDetails($request);

        $api_code = trim($request->api_code);
        $net_select = $request->net_select;
        $date_time = date("Y-m-d H:i:s");

        // Insert API Network Record... 
        $ar_2 = [];
        

        $j = 1;
        foreach($net_select as $net_code)
        {
            $n_short = trim($request['net_'.$net_code]);

            if($n_short != "")
            {
                $cnt = ApiProviderNetworkDetails::where('api_code', $api_code)->where('net_code', $net_code)->count();

                if($cnt == 0)
                {
                    $ar_1 = ['api_net_tr_id' => rand(100,999), 'api_code' => $api_code, 'net_code' => $net_code,
                    'api_net_short_code' => $n_short, 'api_net_per' => '0', 'api_net_surp' => "0", 'created_at' => $date_time, 'updated_at' => $date_time];

                    array_push($ar_2, $ar_1);
        
                    $j++;
                }
                else 
                {
                    $z = ApiProviderNetworkDetails::where('api_code', $api_code)->where('net_code', $net_code)
                                ->update(['api_net_short_code' => $n_short, 'updated_at' => $date_time]);
                    
                    $j++;
                }
            }
        }

        if($j > 1)
        {
            $z = ApiProviderNetworkDetails::insert($ar_2);

            if($z > 0)
            {
                $op = "Api Provider Network Entry is Added Successfully....";
            }
        }
 
        return redirect()->back()->with('msg', $op);
    }

}
