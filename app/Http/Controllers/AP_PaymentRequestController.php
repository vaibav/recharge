<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\GetMobileCommon;
use App\Libraries\Stock;

use App\Models\UserPaymentDetails;


class AP_PaymentRequestController extends Controller
{
    //
    public function index(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $upay_1->select('trans_id', 'user_amount', 'user_req_date', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks','trans_status')
        ->where('user_name', '=', $ob->user)->orderBy('id', 'desc')->limit(6)->get();

        return view('apipartner.payment_request_1', ['user' => $ob, 'pay1' => $d1]);
    		
    }

    public function store(Request $request)
	{
       
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
       
        $data = [];
        $data['user_code_req'] = $ob->code;
        $data['user_name_req'] = $ob->user;
        $data['user_amount'] = trim($request->user_amount);
        $data['user_remarks'] = trim($request->user_remarks);

        $op = Stock::stockRequest($data);

        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function index_android(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Apipartner...
            return view('android.ap_payment_request_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Apipartner";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store_android(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $user_amount = trim($request->user_amt);
        $user_remarks = trim($request->user_remarks);

        $z1 = 0;
        $z = 0;
       

        list($q, $user_name_req, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API PARTNER...
            $data = [];
            $data['user_code_req'] = $user_code;
            $data['user_name_req'] = $user_name_req;
            $data['user_amount'] = $user_amount;
            $data['user_remarks'] = $user_remarks;

            $op = Stock::stockRequest($data);

            return redirect()->back()->with('msg', $op);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API PARTNER...
            $user_name = strtoupper($user_name);
            $d1 = $upay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')->orderBy('id', 'desc')->get(); 

            return view('android.ap_payment_request_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'payment' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
