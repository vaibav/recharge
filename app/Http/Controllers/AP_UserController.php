<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\UserChainDetails;
use App\Models\UserAccountBalanceDetails;

class AP_UserController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;

        $rs = [];
        $j = 0;

        $ob = GetCommon::getUserDetails($request);
       
        $data = $user_1->with(['account', 'balance'])->where('user_code', '=', $ob->code)->get();
        $rs[$j][0] = $data;
        $rs[$j][1] = 0;
        $j++;

       
        return view('apipartner.user_view', ['user1' => $rs, 'user' => $ob]);

       

    }

    public function viewone_user($user_code, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        
        $data = $user_1->with(['account'])->where('user_code', '=', $user_code)->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('apipartner.user_view_one', ['user1' => $data, 'user' => $ob]);
    }

    public function viewnetwork_user($user_code, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_3 = new UserNetworkDetails;
        $net_o1 = new NetworkDetails;
        $user_name = "";
        
        $data = $user_3->with(['usernetwork'])->where('user_code', '=', $user_code)->get();
        $data1 = $user_1->select('user_name')->where('user_code', '=', $user_code)->get();
        $data2 = $net_o1->select('net_code','net_name')->get();

        foreach($data1 as $d)
        {
            $user_name = $d->user_name;
        }
        
        $ob = GetCommon::getUserDetails($request);
        return view('apipartner.user_view_network', ['user1' => $data, 'net_1' => $data2, 'user_name' => $user_name, 'user' => $ob]);
    }

    public function view_surplus(Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;

        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data3 = $net_1->select('net_tr_id', 'net_code', 'user_type', 'from_amount', 'to_amount', 'surplus_charge')
                        ->where('user_type', '=', 'API PARTNER')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('apipartner.network_surplus', ['network' => $data1, 'surplus' => $data3, 'user' => $ob]);
    
    }

}
