<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;
use App\Models\NetworkLineApipartnerDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;

class NetworkLineApipartnerController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkLineApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $user_1 = new UserAccountDetails;

        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->orderBy('net_name', 'asc')->get();
        $data2 = $api_1->select('api_code','api_name')->orderBy('api_name', 'asc')->get();
        $data3 = $net_1->select('net_line_tr_id', 'user_name', 'net_code', 'api_code', 'net_line_type', 'net_line_value_1', 'net_line_value_2',
                                'net_line_status')
                        ->where('net_code', '=', $net_code)->get();
        $data4 = $user_1->select('user_code','user_name')->where('user_type', '=', 'API PARTNER')->orderBy('user_name', 'asc')->get();
       
        $ob = $this->getUserDetails($request);

        return view('admin.networkline_apipartner', ['network' => $data1, 'user1' => $data4, 'api1' => $data2, 'net1' => $data3, 'net_code' => $net_code, 'user' => $ob, 'u_name' => '']);
    
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function view($net_code, $user_name, Request $request)
	{
        $net_1 = new NetworkLineApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $user_1 = new UserAccountDetails;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data2 = $api_1->select('api_code','api_name')->get();
        $data3 = $net_1->select('net_line_tr_id', 'user_name','net_code', 'api_code', 'net_line_type', 'net_line_value_1', 'net_line_value_2',
                                'net_line_status')
                        ->where([['net_code', '=', $net_code], ['user_name', '=', $user_name]])->get();
        $data4 = $user_1->with(['personal'])->select('user_code','user_name')->where('user_type', '=', 'API PARTNER')->get();
        
        $op = "";

        $ob = $this->getUserDetails($request);

        return view('admin.networkline_apipartner', ['network' => $data1, 'user1' => $data4, 'api1' => $data2, 'net1' => $data3, 'net_code' => $net_code, 'user' => $ob, 'u_name' => $user_name]);
        
    }

    public function store(Request $request)
	{
        $net_1 = new NetworkLineApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $z = 0;
        $op = "";

        $line_tr_id = rand(1000,9999);
        $user_name = trim($request->user_name);
        $net_code = trim($request->net_code);
        $api_code = trim($request->api_code);
        $line_type = trim($request->net_line_type); 
        $line_min = trim($request->net_line_min); 
        $line_max = trim($request->net_line_max);
        
        if($line_type == "ALL")
        {
            $cnt = $net_1->where([['user_name', '=', $user_name], ['net_code', '=', $net_code], ['net_line_type', '=', $line_type]])->count();
            if($cnt > 0)
            {
                $z = 1;
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Data is Already Available..</label>";
            }

            $line_min = "*";
            $line_max = "*";
        }
        if($line_type == "RANGE_VALUES")
        {
            if($line_min != "" && $line_max != "")
            {
                $cnt = $net_1->where([['user_name', '=', $user_name], ['net_code', '=', $net_code], ['api_code', '=', $api_code], 
                                        ['net_line_type', '=', $line_type], ['net_line_value_1', '=', $line_min], 
                                        ['net_line_value_2', '=', $line_max]])->count();
                if($cnt > 0)
                {
                    $z = 1;
                    $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Data is Already Available..</label>";
                }
            }
            else
            {
                $z = 1;
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Empty Range Values..</label>";
            }
           
        }
        if($line_type == "SPECIFIC_AMOUNT")
        {
            $date_time = date("Y-m-d H:i:s");

            if($line_min != "")
            {
                $d1 = explode(",", $line_min);
                if(sizeof($d1) > 0)
                {
                    $arr1 = [];
                    $arr2 = [];
                    $c = 0;
                    // Multiple Specific Elements
                    foreach($d1 as $d)
                    {
                        $cnt = $net_1->where([['user_name', '=', $user_name], ['net_code', '=', $net_code], ['api_code', '=', $api_code], 
                                        ['net_line_type', '=', $line_type], ['net_line_value_1', '=', $d]])->count();
                        if($cnt == 0)
                        {
                            $arr2 = array('net_line_tr_id' => rand(1000,9999), 'user_name' => $user_name, 'net_code' => $net_code, 'api_code' => $api_code,
                            'net_line_type' =>  $line_type, 'net_line_value_1' => $d, 'net_line_value_2' => $line_max, 'net_line_status' => 1,
                             'created_at' => $date_time, 'updated_at' => $date_time);
                            array_push($arr1, $arr2);
                            $c = 1;
                        }
                    }

                    if($c == 1)
                    {
                         // Insert  Record... 
                        $net_1->insert($arr1);
                    }
                   
                    $z = 2;
                }
                else
                {
                    // Single Specific Value
                    $cnt = $net_1->where([['user_name', '=', $user_name], ['net_code', '=', $net_code], ['api_code', '=', $api_code], 
                                        ['net_line_type', '=', $line_type], ['net_line_value_1', '=', $line_min]])->count();
                    if($cnt > 0)
                    {
                        $z = 1;
                        $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Data is Already Available..</label>";
                    }
                }

                
            }
            else
            {
                $z = 1;
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Empty Specific Amount..</label>";
            }

            $line_max = "*";
           
        }
        
        if($z == 0)
        {
            // Insert Record... 
            $net_1->net_line_tr_id = $line_tr_id;
            $net_1->user_name = $user_name;
            $net_1->net_code = $net_code;
            $net_1->api_code = $api_code;
            $net_1->net_line_type = $line_type;
            $net_1->net_line_value_1 = $line_min;
            $net_1->net_line_value_2 = $line_max;
            $net_1->net_line_status = 1;

            $net_1->save();
            
            $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Record is inserted Successfully...</label>";
           
        }
        else if($z == 2)
        {
            $op = "Record is inserted Successfully...";
        }
       

        
        return redirect()->back()->with('result', [ 'output' => $op]);

		
    }

    public function delete($tr_id)
	{
        $net_1 = new NetworkLineApipartnerDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $z = 0;
        $op = "";
        $net_code =  0;


        $cnt = $net_1->where('net_line_tr_id', '=', $tr_id)->count();
        
        if($cnt > 0)
        {
            // Get API Code
            $data3 = $net_1->select('net_code')->where('net_line_tr_id', '=', $tr_id)->get();

            foreach($data3 as $d)
            {
                $net_code = $d->net_code;
            }

            // Delete Record... 
            $net_1->where('net_line_tr_id', '=', $tr_id)->delete();
            $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Record is deleted Successfully...</label>";
        }
        else
        {
                            
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! No Record Found...</label>";

        }

       
        return redirect()->back()->with('result', [ 'output' => $op]);

        
    }

}
