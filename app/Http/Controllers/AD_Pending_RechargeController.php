<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\TransactionDirectReply;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

class AD_Pending_RechargeController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::with(['request'])
                        ->where('trans_type', '=', 'RECHARGE')->where([['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        $d2 = NetworkDetails::select('net_code','net_name')->where('net_status', '=', 1)->get();

        $d3 = ApiProviderDetails::select('api_code','api_name')->get();

        return view('admin.ad_pending_recharge', ['user' => $ob, 'recharge' => $d1, 'network' => $d2, 'api' => $d3]);

    }

    public function successUpdate($trans_id, $opr_id, Request $request)
    {
        
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::select('user_name', 'api_trans_id', 'api_code')
                            ->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            $zx = TransactionDirectReply::successUpdate($trans_id, $opr_id);

            if($zx == 0) {
                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }
       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);     

    }

    public function failureUpdate($trans_id, $opr_id, Request $request)
    {
        
        $z = 1;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::select('user_name', 'api_trans_id', 'api_code')->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING'], ['rech_option', '=', '0']])->get(); 

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $api_trans_id = $d1[0]->api_trans_id;

            if (strpos($trans_id, 'VBA') !== false) {
                $mode = "API";
            }

            $zx = TransactionDirectReply::failureUpdate($trans_id, $opr_id);

            if($zx == 0)
            {
                $z = 0;
                $op = "Success Update is executed successfully...";
            }

        }

       
        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
       
    }


    // Common Functions
    // New Functions 
    public function storeTransferupdate_1($trans_id, $opr_id, $s_status)
    {
        if($s_status == "FAILURE")
        {
            TransactionBankReply::failure_update($trans_id, $opr_id);
        }
        else if($s_status == "SUCCESS")
        {
            TransactionBankReply::success_update($trans_id, $opr_id, "0", "0", "1");
        }
        return 1;
    }

    public function storeTransferupdate_1_1($trans_id, $opr_id, $s_status)
    {
       
        if($s_status == "FAILURE")
        {
            TransactionBankReply::failure_update($trans_id, $opr_id);
        }
        if($s_status == "SUCCESS")
        {
            TransactionBankReply::success_update($trans_id, $opr_id, "0", "0", "2"); 
        }
        return "1";
    }

    
    
}
