<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\GetCommon;

use App\Models\UserPaymentDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;

use App\Models\PaymentLedgerDetails;

class PaymentLedgerController extends Controller
{
    //
    public function move_all(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
		
        $ob = GetCommon::getUserDetails($request);

        $d1 = $upay_1->get();

        $ar1 = [];

        $date_time = date("Y-m-d H:i:s");

        $op = "  Records are moved...";

        $j = 0;
        foreach($d1 as $d)
        {
            if($d->payment_mode != "OPENING_BALANCE")
            {
                $ar2 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $d->user_name, 'pay_type' => "C",
                    'pay_amount' => $d->grant_user_amount, 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $d->grant_user_name, 'pay_date' => $d->grant_date, 
                    'agent_name' => "-", 'pay_remarks' => $d->payment_mode,  'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
            
                array_push($ar1, $ar2);
                $j++;
            }
            
        }

        if($j != 0)
        {
            $upay_2->insert($ar1);

        }
        

        echo $j."-".$op;

    }
}
