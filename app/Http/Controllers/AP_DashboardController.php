<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportApipartner;

use App\Models\NetworkDetails;
use App\Models\AdminOfferDetails;

class AP_DashboardController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $net_2 = new NetworkDetails;
        $off_1 = new AdminOfferDetails;

        $ob = GetCommon::getUserDetails($request);
        
        $rs = RechargeReportApipartner::getRechargeDetailsApipartner($ob->user);

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();
        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('apipartner.dashboard',  ['user' => $ob, 'recharge' => $rs, 'network' => $d1, 'offer' => $off, 'in_active' => $d3]);
		
    }

    public function loadData(Request $request)
    {
        $ob = GetCommon::getUserDetails($request);

        $str = RechargeReportApipartner::getRechargeDetailsApipartner($ob->user);
        
        echo $str;
    }

    public function rech_data(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        list($rec_tot, $res_tot, $ref_tot, $trn_tot, $trs_tot, $trf_tot) = RechargeReportApipartner::getRechargeList($ob->user);
        
        $arr = ['res_tot' => $res_tot, 'ref_tot' => $ref_tot, 'rec_tot' => $rec_tot, 'trn_tot' => $trn_tot,
                    'trs_tot' => $trs_tot, 'trf_tot' => $trf_tot];
        
        return response()->json($arr, 200);
		
    }

    public function chart_data(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        list($ars, $arf) = RechargeReportApipartner::getRechargeGraphData($ob->user);
        
        $arr = ['res' => $ars, 'ref' => $arf];
        
        return response()->json($arr, 200);
		
    }
}
