<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;

use App\Models\UserAccountDetails;
use App\Models\ApipartnerBrowserDetails;

class ApipartnerBrowserController extends Controller
{
    //
    public function index(Request $request)
	{
        $uacc_1 = new UserAccountDetails;
        $api_1 = new ApipartnerBrowserDetails;
       
        $ob = GetCommon::getUserDetails($request);
        
        $d1 = $uacc_1->with(['personal'])->select('user_code','user_name')->where('user_type', '=', 'API PARTNER')
                                                ->orderBy('user_name', 'asc')->get();

        $d2 = $api_1->get();

        return view('admin.apipartner_browser', ['apipartner' => $d1, 'data' => $d2, 'user' => $ob]);
    
    }

    public function store(Request $request)
	{
        $api_1 = new ApipartnerBrowserDetails;
       
        $z = 0;
        $op = "";

        $trans_id = rand(1000,9999);
        $user_name = trim($request->user_name);
        $agent_type = trim($request->agent_type);


        $cnt = $api_1->where('user_name', '=', $user_name)->count();
        if($cnt > 0)
        {
            $api_1->where('user_name', '=', $user_name)->update(['agent_type' => $agent_type]);
            
            $op = "Entry is updated Successfully...";
        }
        else
        {
             // Insert Record... 
             $api_1->trans_id = $trans_id;
             $api_1->user_name = $user_name;
             $api_1->agent_type = $agent_type;
           
             $api_1->save();
             
             $op = "Entry is added Successfully...";

        }
       

        return redirect()->back()->with('result', $op);
	
    }
}
