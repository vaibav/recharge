<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\GetMobileCommon;
use App\Libraries\TransactionEB;
use App\Libraries\RechargeInfo;
use App\Libraries\SmsInterface;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;

use App\Models\UserRechargeBillDetails;
use App\Models\AllTransaction;
use App\Models\ApiProviderDetails;

class AndroidUserEbbillController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }


        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'BILL PAYMENT')->get();
        
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->getEbbillDetailsRetailer($user_name);

            return view('android.recharge_ebbill', ['network' => $d2, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function ebbill_recharge(Request $request)
	{
       
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code);
        $cons_no = trim($request->eb_cons_no);
        $cons_name = trim($request->eb_cons_name);
        $cons_mobile = trim($request->eb_cons_mobile);
        $cons_duedate = "10/10/2019";
        $cons_amount = trim($request->eb_cons_amount);
        $date_time = date("Y-m-d H:i:s");

        if($cons_name == "") {
            $cons_name = "MOBILE_NAME";
        }

        list($q, $user_name, $user_type1, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type1 != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {

            // Post Data
            $data = [];
            $data['net_code'] = $net_code;
            $data['cons_no'] = $cons_no;
            $data['cons_name'] = $cons_name;
            $data['cons_mobile'] = $cons_mobile;
            $data['cons_amount'] = $cons_amount;
            $data['cons_duedate'] = $cons_duedate;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";


            /*$z = RechargeBill::add($data);

            if($z == 0)
            {
                $op = "Bill Payment Process is Completed Successfully..";
                if($cons_mobile != "")
                {
                    $msg = "EB Bill Amount :".$cons_amount." has been paid for Cons.No :".$cons_no;
                    $zep_code = SmsInterface::callSms($user_name, $cons_mobile, $msg);
                }
            }
            else if($z == 3)
            {  
                $op = "Error! Low user Balance...";
            }
            else if($z == 4)
            {  
                $op = "Error! No Web Mode is Selected...";
            }
            else if($z == 5)
            {   
                $op = "Error! Recharge is already Pending for this Consumer No... ";
            }
            else if($z == 6)
            {   
                $op = "Error! This Account is below from Setup Fee...";
            }
            else if($z == 7)
            {
                $op = "Error! This Account is locked...";
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }*/

            $z = TransactionEB::recharge_ebbill($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
       
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }

   

    public function getEbbillDetailsRetailer($user_name)
    {
        $rech_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $rs = [];
        $d1 = $rech_1->where('user_name', '=', $user_name)->where('trans_type','BILL_PAYMENT')->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;
            $z = 0;

            $reply_id = $f->reply_id;
            $reply_date =$f->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);

            if($rech_status == "PENDING" && $rech_option == "1")
            {
                $status = "PENDING";
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
                $o_bal = floatval($f->ret_bal) - floatval($f->ret_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->ret_total);
            $c_bal = number_format($c_bal, 2, '.', '');

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 
            if($status == "SUCCESS") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p style='font-size: 14px;margin: 0;'>".$f->rech_amount."</p></div>";
          
              
              $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
              $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$net_name."</p>"; 
              $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$f->user_mobile."</p>"; 
              
              if($z == 0) {
                  $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 11px;'>".$reply_id."</p>"; 
              }
              else  {
                  $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 10px;'></p>";  
              }
  
              $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p>"; 


                //percentage amount
                $per = floatval($f->ret_net_per);
                $peramt1 = 0;
                $peramt2 = 0;
                if(floatval($per) != 0)
                {
                    $peramt1 = floatval($per) / 100;
                    $peramt1 = round($peramt1, 4);
            
                    $peramt2 = floatval($f->rech_amount) * floatval($peramt1);
                    $peramt2 = round($peramt2, 2);
                }
              
              $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
              $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;margin-top:3px;'>
                                          <i class='fas fa-percentage'></i> &nbsp;".$f->con_net_per. "--".number_format($peramt2,2, ".", "")."</p>";
                $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'>
                                          <i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->ret_net_surp,2, ".", "")."</p>";
              $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'>
                                          <i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->ret_total,2, ".", "")."</p>";
              $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
              $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($f->ret_bal,2, ".", "")."</p>";
             
              $str = $str."</div></div></div>";
                               
            
           
            
       
        }
        
        return $str; 
    }

    public function resultFormatter($result)
    {
        $res = "";
        $res = str_replace("\\\\\"", "###dq###", $result);
        $res = str_replace("\\", "", $res);
        $res = str_replace("###dq###", "\\\"", $res);
        $res = str_replace("\n", " ", $res);
        $res = str_replace("'", "", $res);
        $res = str_replace("(", " ", $res);
        $res = str_replace(")", " ", $res);

        return $res;
    }

}
