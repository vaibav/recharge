<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\ApipartnerIpaddressDetails;
use App\Models\UserAccountBalanceDetails;

class ApipartnerIpaddressController extends Controller
{
    //
    public function index(Request $request)
	{
            
        $ob = $this->getUserDetails($request);

        return view('user.apipartner_ipaddress', ['user' => $ob]);
    
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
        $ip_1 = new ApipartnerIpaddressDetails;
       
        $z = 0;
        $op = "";

        $ob = $this->getUserDetails($request);
        $user_name = $ob->user;

        // Post Data
        $trans_id = rand(1000,9999);
        $api_ip = trim($request->api_ip);

        $d1 = $ip_1->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $ip_1->where('user_name', '=', $user_name)->update(['ip_address' => $api_ip]);
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Record is updated Successfully..</label>";
        }
        else
        {
             // Insert Record... 
             $ip_1->trans_id = $trans_id;
             $ip_1->user_name = $user_name;
             $ip_1->ip_address = $api_ip;
             $ip_1->ip_status = 1;
            
             $ip_1->save();
             
             $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Record is inserted Successfully...</label>";

        }
       

        return redirect()->back()->with('result', [ 'output' => $op]);
	
    }

}
