<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Beneficiary;
use App\Libraries\BankTransfer;
use App\Libraries\BankMobileCommon;
use App\Libraries\PercentageCalculation;
use App\Libraries\CyberBankRemitter;
use App\Libraries\CyberBankBeneficiary;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankAgentDetails;
use App\Models\NetworkDetails;
use Session;

class AndroidUserBenRegisterController extends Controller
{
    //
    public function index($msisdn, $auth_token)
	{
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result, $remitter, $beneficiary, $isVerified) =BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                //list($data, $beni) = BankMobileCommon::fetchCustomerDetails($result);

                $d1 = $this->view_user($msisdn);

                $cx = BankMobileCommon::add_status($user_name, $msisdn, "BENADD");

                return view('android.recharge_money_ab_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 
                        'data' => $remitter, 'beni' => $beneficiary, 'beni1' => $beneficiary]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function add_view($msisdn, $auth_token)
	{
       
        $z1 = 0;
        $d2 = [];
        $bank = [];

        //Get Bank
        $apiUrl = "http://www.vaibavonline.com/vaibav/api/bank-data";

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result, $remitter, $beneficiary, $isVerified) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                if(!Session::has('bank'))
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($apiUrl);
                    $res_code = $res->getStatusCode();
                    $res_content = (string) $res->getBody();

                    if($res_code == 200) {
                        $jsonData     = json_decode($res_content);

                        usort($jsonData->bank, array( $this, 'compareBank' ));

                        foreach($jsonData->bank as $d)
                        {
                            array_push($bank, ['bankCode' => $d->bank_sort_name, 'bankName' => $d->bank_name, 'ifscShort' => $d->ifsc_alias, 
                                    'branchIfsc' => $d->branch_ifsc]);
                        }

                        Session::put('bank', $bank);

                       // print "<pre>";
                        //print_r($jsonData->bank);
                    }
                }
                else
                {
                    $bank = Session::get('bank');
                }

                return view('android.recharge_money_ab_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 
                        'data' => $remitter, 'beni' => $beneficiary, 'bank' => $bank]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view_user($msisdn)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $d1 = $ben_1->where('msisdn', '=', $msisdn)->where('ben_status', '!=', 'OTP FAILED')
                            ->where('ben_status', '!=', 'FAILED')->orderBy('id', 'desc')->get();
        
        return $d1;
    }

    public function store(Request $request)
	{
        $z = 0;
        $z1 = 0;
        $op = "";
        $trans_id = "0";
        $ben_id = "0";

        $auth_token   = trim($request->auth_token);
        $c_msisdn     = trim($request->c_msisdn);
        $remId        = trim($request->remId);
        $ben_acc_no   = trim($request->ben_acc_no);
        $ben_acc_name = trim($request->ben_acc_name);
        $bank_code    = trim($request->bank_code);
        $bank_ifsc    = trim($request->bank_ifsc);
        $agent_id     = "0";
        

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $data = ['user_name'    => $user_name,
                    'mobileNo'     => $c_msisdn,
                    'remId'        => $remId,
                    'firstName'    => $ben_acc_name,
                    'lastName'     => "-",
                    'benAccount'   => $ben_acc_no,
                    'benName'      => $ben_acc_name,
                    'benIFSC'      => $bank_ifsc,
                    'benBank'      => $bank_code,
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

            /*$data = ['user_name' => $user_name,
                    'msisdn' => $c_msisdn,
                    'beneficiary_account_no' => $ben_acc_no,
                    'beneficiary_name' => $ben_acc_name,
                    'beneficiary_bank_code' => $bank_code,
                    'beneficiary_ifsc_code' => $bank_ifsc,
                    'api_trans_id' => '-',
                    'r_mode' => 'WEB' ];*/

            list($z, $msg) = CyberBankBeneficiary::add($data);

            if($z == 0)
                $op = $msg;
            else if($z == 1)
                $op = "Mode web is not Selected...";
            else if($z == 2)
                $op = "Account is Inactivated...";
            else if($z == 3)
                $op = "Account No is Already Registered...";
            else if($z == 4)
                $op = $msg;
            else if($z == 5)
                $op = "PENDING...";
            
            return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'ben_acc_no' => $ben_acc_no, 'msisdn' => $c_msisdn,
                                                    'trans_id' => $trans_id, 'ben_id' => $ben_id]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
       

    }

    public function store_otp(Request $request)
	{

        $z1 = 0;
        $z = 0;
        $op = "";

        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
        $trans_id = trim($request->trans_id);
        $otp = trim($request->otp);

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $data = ['user_name' => $user_name, 'ben_id' => $ben_id, 'msisdn' => $msisdn,
                        'trans_id' => $trans_id, 'otp' => $otp, 'r_mode' => "WEB" ];
            
            list($z, $msg, $res) = Beneficiary::add_otp($data);
            if($z == 0)
            {
                $op = $msg;
                $z1 = 0;
            }
            else if($z == 1)
            {
                $op = "Server is Temporariy Shutdown...";
                $z1 = 1;
            }
            else if($z == 2)
            {
                $op = "Mode web is not Selected...";
                $z1 = 2;
            }
            else if($z == 3)
            {
                $op = "Account is Inactivated...";
                $z1 = 3;
            }
            else if($z == 4)
            {
                $op = "Already Registered...";
                $z1 = 4;
            }
            else if($z == 5)
            {
                $op = $msg;
                $z1 = 5;
            }
            else if($z == 6)
            {
                $op = "Wait! Pending....";
                $z1 = 6;
            }
           
        }
        else if($z1 == 7)
        {
            $op = "User is not Retailer";
        }
        else if($z1 == 8)
        {
            $op = "Invalid API token..";
        }

        $result = ['status' => $z1, 'message' => $op];

        return response()->json($result); 
	
    }

    public function send_otp(Request $request)
	{
        
        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
        $trans_id = trim($request->trans_id);

        $z = 0;
        $z1 = 0;
        $op = "";
        $trans_id_r = "";


        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $data = ['user_name' => $user_name, 'msisdn' => $msisdn, 'trans_id' => $trans_id,
                        'ben_id' => $ben_id, 'r_mode' => "WEB" ];
    
            list($z, $msg, $trans_id_r) = Beneficiary::resend_otp($data);
            if($z == 10)
            {
                $op = $msg;
                $z1 = 0;        // Resend Successfully...
            } 
            else if($z == 1)
            {
                $op = "Server is Temporariy Shutdown...";
                $z1 = 1;
            }
            else if($z == 2) 
            {
                $op = "Mode web is not Selected...";
                $z1 = 2;
            }
            else if($z == 3)
            {
                $op = "Account is Inactivated...";
                $z1 = 3;
            }
            else if($z == 4)
            {
                $op = "Already Registered...";
                $z1 = 4;
            }
            else if($z == 5)
            {
                $op = $msg;
                $z1 = 5;
            }
            else if($z == 6)
            {
                $op = "Wait! Pending....";
                $z1 = 6;
            }
            else
            {
                $op = "Unable to send otp...";
                $z1 = 7;
            }
           
        }
        else if($z1 == 1)
        {
            $op = "User is not Retailer";
            $z1 = 8;
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
            $z1 = 9;
        }

       
        $result = ['status' => $z1, 'message' => $op];

        return response()->json($result); 
	
    }

    public function check_account(Request $request)
	{

        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->agent_msisdn);
        $ben_acc_no = trim($request->ben_acc_no);
        $bank_code = trim($request->bank_code);
        $bank_ifsc = trim($request->bank_ifsc);

        $z = 0;
        $z1 = 0;
        $op = "";
        $ben_name = "";
        
        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;

            }
        }

        if($z1 == 0)
        {
            //Only Retailer...

             $data = ['user_name'  => $user_name,
                    'mobileNo'     => $msisdn,
                    'benAccount'   => $ben_acc_no,
                    'benIFSC'      => $bank_ifsc,
                    'bankCode'     => $bank_code,
                    'client_trans_id' => rand(100000,999999),
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

            /*$data = ['user_name' => $user_name, 'msisdn' => $msisdn, 'beneficiary_account_no' => $ben_acc_no,
                    'beneficiary_bank_code' => $bank_code, 'beneficiary_ifsc_code' => $bank_ifsc,
                    'client_trans_id' => rand(100000,999999), 'api_trans_id' => '-', 'r_mode' => 'WEB' ];*/

            list($z, $msg, $benName) = CyberBankBeneficiary::checkBeneficiary($data);
            //list($z, $op, $ben_name, $res) = Beneficiary::check_ben_account($data);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
            $z = 3;
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
            $z = 4;
        }


        $result = ['status' => $z, 'ben_name' => $benName, 'message' => $msg];
       
        return response()->json($result); 
	
    }

    public function compareBank($a, $b)
    {
        return strcmp($a->bank_name, $b->bank_name);
    }
}
