<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use Illuminate\Support\Str;

use App\Models\ApipartnerTokenDetails;

class AP_ApiTokenController extends Controller
{
    //
    public function index(Request $request)
	{
        $api_1 = new ApipartnerTokenDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $api_1->where('user_name', '=', $ob->user)->orderBy('id', 'desc')->limit(6)->get();

        return view('apipartner.api_token_1', ['user' => $ob, 'token' => $d1]);
    		
    }

    public function store(Request $request)
	{
        $api_1 = new ApipartnerTokenDetails;

        $ob = GetCommon::getUserDetails($request);

        $z = 0;

        $api_type = trim($request->api_type);
        $api_ip_address = trim($request->api_ip_address);
        $auth_token = uniqid(base64_encode(Str::random(30)));

        if($api_type != "" && $api_ip_address != "")
        {
            $api_1->trans_id = rand(10000,99999);
            $api_1->user_name = $ob->user;
            $api_1->api_type = $api_type;
            $api_1->api_ip_address = $api_ip_address;
            $api_1->auth_token = $auth_token;

            $api_1->save();

            $z = 1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $auth_token, 'msg' => $z]);
    		
    }
}
