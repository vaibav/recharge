<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;

class ApiproviderController extends Controller
{
    //
    public function index(Request $request)
	{
        $model = new NetworkDetails;
        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        $ob = GetCommon::getUserDetails($request);

        return view('admin.apiproviderrequest', ['network' => $data, 'user' => $ob]);
        
    }

   
    public function store(Request $request)
	{
        $model = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;
        
        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $op = "";


        $this->validate($request, [
            'pr_name' => 'required', 'pr_url' => 'required',
        ],
        [
            'pr_name.required' => ' The Provier Name field is required.', 
            'pr_url.required' => ' The Provider URL field is required.' ]);

        
        

        $pr_code = rand(100,999);
        $pr_name = trim($request->pr_name);
        $pr_url = trim($request->pr_url); 

        $cnt = $api_1->where('api_code', '=', $pr_code)->count();

        if($cnt == 0)
        {

             // Insert API Parameter Record... 
             $arr1 = [];
             $arr2 = [];
             $z1 = 0;
             $v1 = 0;
             $v2 = 0;
             $date_time = date("Y-m-d H:i:s");
 
             $j = 1;
             for($j = 1; $j <= 18; $j++)
             {
                 $p_type = trim($request['para_type_'.$j]);
                 $p_name = trim($request['para_name_'.$j]);
                 $p_value = trim($request['para_value_'.$j]);
 
                 if($p_type != "-" && $p_name != "")
                 {
                     $arr2 = array('api_para_tr_id' =>rand(100,999), 'api_code' => $pr_code, 'api_para_type' => $p_type,
                     'api_para_name' => $p_name, 'api_para_value' => $p_value, 'created_at' => $date_time, 'updated_at' => $date_time);
                     array_push($arr1, $arr2);
                     $z1 = 1;
                 }
                 else if($p_type != "-" && $p_name == "")
                 {
                     $v1 = 1;
                 }
 
             }
            
            // Insert API Network Record... 
            $arr3 = [];
            $arr4 = [];
            $z2 = 0;

            $j = 1;
            foreach($data as $f)
            {
                $n_select = trim($request['net_select_'.$j]);
                $n_code = trim($request['net_code_'.$j]);
                $n_name = trim($request['net_name_'.$j]);
                $n_short = trim($request['net_short_'.$j]);
                $n_per = trim($request['net_per_'.$j]);

                if($n_select == "on")
                {
                    if($n_short != "")
                    {
                        $arr3 = array('api_net_tr_id' => rand(100,999), 'api_code' => $pr_code, 'net_code' => $n_code,
                        'api_net_short_code' => $n_short, 'api_net_per' => $n_per, 'api_net_surp' => "0",
                        'created_at' => $date_time, 'updated_at' => $date_time);
                        array_push($arr4, $arr3);
                        $z2 = 1;
                    }
                    else
                    {
                        $v2 = 1;
                    }
                    
                }

                $j++;

            }


            if($v1 == 0)
            {
                if($v2 == 0)
                {
                    if($z1 > 0 && $z2 > 0)
                    {
                         // Insert API URL Record... 
                        $api_1->api_tr_id = rand(100,999);
                        $api_1->api_code = $pr_code;
                        $api_1->api_name = $pr_name;
                        $api_1->api_url = $pr_url;
                        $api_1->api_method = "GET";
                        $api_1->api_status = 1;
                        $api_1->save();

                        // Insert API Parameter Record... 
                        $api_2->insert($arr1);

                        // Insert API Network Record... 
                        $api_3->insert($arr4);
                        

                        $op = "Record is inserted...";
                    }
                   
                }
                else
                {
                    $op = "Error Network Short code may be Null...";
                }
            }
            else
            {
                $op = "API Parameter Name may be Null...";
            }
            
            
            
        }

        return redirect()->back()->with('msg', $op);
    }

    public function edit(Request $request)
	{
        $model = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        
        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $adata = $api_1->select('api_code','api_name')->orderBy('api_name', 'asc')->get();

        $ob = GetCommon::getUserDetails($request);

        return view('admin.apiproviderrequestedit', ['network' => $data, 'api' => $adata, 'user' => $ob]);
        
    }

    public function edit1($code, Request $request)
	{
        if($code != "")
        {
            $model = new NetworkDetails;
            $api_1 = new ApiProviderDetails;
            $api_2 = new ApiProviderParameterDetails;
            $api_3 = new ApiProviderNetworkDetails;

            $ob = GetCommon::getUserDetails($request);

            $data1 = $api_1->select('api_code','api_name', 'api_url')->where('api_code', '=', $code)->get();
            $data2 = $api_2->select('api_para_tr_id','api_para_type', 
                    'api_para_name', 'api_para_value')->where('api_code', '=', $code)->get();
            $data3 = $api_3->select('api_net_tr_id','net_code', 
                    'api_net_short_code', 'api_net_per', 'api_net_surp')->where('api_code', '=', $code)->get();
            
            $data4 = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
            
        
            return view('admin.apiproviderrequestedit1', ['network' => $data4, 
            'api1' => $data1, 'api2' => $data2, 'api3' => $data3, 'api_code' => $code, 'user' => $ob]);

        }
        else
        {
            $model = new NetworkDetails;
            $api_1 = new ApiProviderDetails;

            $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
            $adata = $api_1->select('api_code','api_name')->get();

            return view('admin.apiproviderrequestedit', ['network' => $data, 'api' => $adata, 'user' => $ob]);
        }
        
    }

    public function update(Request $request)
	{
        $model = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;
        
        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $op = "";


        $this->validate($request, [
            'pr_name' => 'required', 'pr_url' => 'required',
        ],
        [
            'pr_name.required' => ' The Provier Name field is required.', 
            'pr_url.required' => ' The Provider URL field is required.' ]);

        /*echo $request->pr_name."<br>";
        echo $request->pr_url."<br>";

        $j = 1;
        for($j = 1; $j <= 18; $j++)
        {
            echo $request['para_type_'.$j]."------".$request['para_name_'.$j]."------".$request['para_value_'.$j]."<br>";
        }

        $j = 1;
        foreach($data as $f)
        {
            echo $request['net_select_'.$j]."------".$request['net_code_'.$j]."------".$request['net_name_'.$j]."---".$request['net_short_'.$j]."---".$request['net_per_'.$j]."<br>";
            $j++;
        }*/
        

        
        $pr_code = trim($request->pr_code);
        $pr_name = trim($request->pr_name);
        $pr_url = trim($request->pr_url); 

        $cnt = $api_1->where('api_code', '=', $pr_code)->count();

        if($cnt > 0)
        {
            $v1 = 0;
            $v2 = 0;

            // Checking API Parameter Record is Null
            $j = 1;
            for($j = 1; $j <= 18; $j++)
            {
                if($request['para_type_'.$j] != "-" && $request['para_name_'.$j] == "")
                {
                    $v1 = 1;
                }
            }

            // Checking API Network Record is Null
            $j = 1;
            foreach($data as $f)
            {
                if($request['net_select_'.$j] == "on" && $request['net_short_'.$j] == "")
                {
                    $v2 = 1;
                }
                $j++;

            }

            $date_time = date("Y-m-d H:i:s");

            if($v1 == 0 && $v2 == 0)
            {
                // Delete Prior Unwanted Data in API Parameter
                $dx2 = $api_2->select('api_para_tr_id')->where('api_code', '=', $pr_code)->get();
                foreach($dx2 as $d)
                {
                    $j = 1;
                    $z = 0;
                    for($j = 1; $j <= 18; $j++)
                    {
                        $p_trid = trim($request['para_tr_id_'.$j]);
                        $p_type = trim($request['para_type_'.$j]);
                        if($d->api_para_tr_id == $p_trid && $p_type != "-")
                        {
                            $z = 1;
                        }
                    }
                    if($z == 0)
                    {
                        $api_2->where('api_para_tr_id', '=', $d->api_para_tr_id)->delete();
                    }
                }

                // Delete Prior Unwanted Data in API Network
                $dx3 = $api_3->select('net_code')->where('api_code', '=', $pr_code)->get();
                foreach($dx3 as $d)
                {
                    $j = 1;
                    $z = 0;
                    foreach($data as $f)
                    {
                        $n_select = trim($request['net_select_'.$j]);
                        $n_code = trim($request['net_code_'.$j]);
                        if($d->net_code == $n_code && $n_select == "on")
                        {
                            $z = 1;
                        }
                        $j++;
                    }
                    if($z == 0)
                    {
                        $api_3->where([['net_code', '=', $n_code], ['api_code', '=', $pr_code]])->delete();
                    }
                }

                // Update API Provider Details... 
                $api_1->where('api_code', '=', $pr_code)->update(['api_name'=>$pr_name, 'api_url'=>$pr_url]);

                // Update API Parameter Details...
                $j = 1;
                for($j = 1; $j <= 18; $j++)
                {
                    $p_trid = trim($request['para_tr_id_'.$j]);
                    $p_type = trim($request['para_type_'.$j]);
                    $p_name = trim($request['para_name_'.$j]);
                    $p_value = trim($request['para_value_'.$j]);
    
                    if($p_type != "-" && $p_name != "")
                    {
                        if($p_trid != '0')
                        {
                            // Update
                            $api_2->where([['api_para_tr_id', '=', $p_trid], ['api_code', '=', $pr_code]])
                              ->update(['api_para_type' => $p_type,
                              'api_para_name' => $p_name, 'api_para_value' => $p_value, 'updated_at' => $date_time]);

                        }
                        else
                        {
                            // Insert
                            $api_2->insert(['api_para_tr_id' =>rand(100,999), 'api_code' => $pr_code, 
                            'api_para_type' => $p_type, 'api_para_name' => $p_name, 'api_para_value' => $p_value, 
                            'created_at' => $date_time, 'updated_at' => $date_time]);
                        }
                        
                    }
                    
                }

                // Update API Network Details... 
                $j = 1;
                foreach($data as $f)
                {
                    $n_select = trim($request['net_select_'.$j]);
                    $n_code = trim($request['net_code_'.$j]);
                    $n_name = trim($request['net_name_'.$j]);
                    $n_short = trim($request['net_short_'.$j]);
                    $n_per = trim($request['net_per_'.$j]);

                    if($n_select == "on" && $n_short != "")
                    {
                        $cnt = $api_3->where([['net_code', '=', $n_code], ['api_code', '=', $pr_code]])->count();

                        if($cnt > 0)
                        {
                            // Update
                            $api_3->where([['net_code', '=', $n_code], ['api_code', '=', $pr_code]])
                              ->update(['api_net_short_code' => $n_short, 'api_net_per' => $n_per, 'api_net_surp' => "0",
                              'updated_at' => $date_time]);
                        }
                        else
                        {
                            // Insert
                            $api_3->insert(['api_net_tr_id' => rand(100,999), 'api_code' => $pr_code, 'net_code' => $n_code,
                            'api_net_short_code' => $n_short, 'api_net_per' => $n_per, 'api_net_surp' => "0",
                            'created_at' => $date_time, 'updated_at' => $date_time]);    
                        }
                        
                    }

                    $j++;

                }

                $op = "Record is Updated...";
            }
            else
            {
                $op = "Any one Parameter Value May be Null...";
            }
            
        }
        else
        {
            $op = "No Record is found for this API Code...";
        }

        return redirect()->back()->with('msg', $op);
    }
}
