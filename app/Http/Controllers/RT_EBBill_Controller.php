<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeNewDetails;

use App\Libraries\GetCommon;
use App\Libraries\RechargeBill;

class RT_EBBill_Controller extends Controller
{
    public function index(Request $request)
    {
        $eb_code = "0";

        $ebdata = [];

        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkTypeDetails::select('net_type_code', 'net_type_name')->where('net_type_name', '=', 'BILL PAYMENT')->get();
        $d2 = NetworkDetails::select('net_code','net_name','net_type_code')->where('net_status', '=', 1)->get();
        $d3 = UserRechargeBillDetails::where('user_name', $ob->user)->orderBy('id', 'desc')->limit(10)->get(); 

        foreach($d1 as $d)
        {
            $eb_code = $d->net_type_code;   
        }

        foreach($d2 as $d)
        {
            if($d->net_type_code == $eb_code) {
                array_push($ebdata, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
        }

      
        return view('retailer.nw_ebbill', ['eb' => $ebdata, 'ebdetail' => $d3, 'network' => $d2, 'user' => $ob]);
    }

    public function store_1(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $cons_no  = trim($request->eb_cons_no);
        $cons_amt = trim($request->eb_cons_amount);
        $cons_na  = trim($request->eb_cons_name);
        $cons_mob = trim($request->eb_cons_mobile);
        $cons_dd  = trim($request->eb_cons_duedate);
        $net_name = "";

        $d1 = NetworkDetails::select('net_code','net_name')->get();
        $d2 = UserRechargeBillDetails::where('user_name', $ob->user)->orderBy('id', 'desc')->limit(10)->get(); 

        foreach($d1 as $d)
        {
            if($d->net_code == $net_code) {
                $net_name = $d->net_name;
            }
        }
      
        return view('retailer.nw_ebbill_inter', ['net_code' => $net_code, 'net_name' => $net_name, 'cons_no' => $cons_no, 'cons_amt' => $cons_amt, 
                                    'cons_na' => $cons_na, 'cons_mob' => $cons_mob, 'cons_dd' => $cons_dd,  
                                    'ebdetail' => $d2, 'network' => $d1, 'user' => $ob]);
    }
   
    public function store_2(Request $request)
    {
        // Declarations
        $op = "";
        $z = 0;
      
        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $data = [];
        $data['net_code'] = trim($request->net_code);
        $data['cons_no'] = trim($request->cons_no);
        $data['cons_name'] = trim($request->cons_na);
        $data['cons_mobile'] = trim($request->cons_mob);
        $data['cons_amount'] = trim($request->cons_amt);
        $data['cons_duedate'] = trim($request->cons_dd);
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        file_put_contents("eb_data.txt", $data['cons_no']."--".$data['cons_name']."--".$data['cons_mobile']."--".$data['cons_amount']."--".$data['cons_duedate']);

        if($data['cons_name'] == "") {
            $data['cons_name'] = "WEB_NAME";
        }

        if($data['cons_duedate'] == "") {
            $data['cons_duedate'] = "04/05/2019";
        }
        
        $z = RechargeBill::add($data);

    
        if($z == 0)
        {
            $op = "Bill Payment Process is Completed Successfully..";
            $z1 = 10;
           
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Web Mode is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! This Account is locked...";
        }
        else if($z == 4)
        {   
            $op = "Error! This Account is below from Setup Fee...";
        }
        else if($z == 5)
        {
            $op = "Error! Recharge is already Pending for this Consumer No...";
        }
        else if($z == 6)
        {
            $op = "Sorry! Server is Temporarily shut down...";
        }
       
        return redirect('ebbill_nw')->with('result', [ 'output' => $op, 'msg' => $z]);

       
    }

 
}