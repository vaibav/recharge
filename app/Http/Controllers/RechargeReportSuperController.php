<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\NetworkDetails;

use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\UserAccountDetails;

use PDF;
use EXCEL;

class RechargeReportSuperController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserAccountDetails;

        $data1 = $user_1->select('user_code','user_name')->where('user_type', '=', 'SUPER DISTRIBUTOR')->get();

        $ob = $this->getUserDetails($request);
        return view('admin.adminrechargereport_super', ['user' => $ob, 'user1' => $data1]);
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_3 = new UserRechargePaymentParentDetails;
        $rech_a_3 = new UserRechargePaymentParentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = $this->getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($request->user_name);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);

        $rs = [];
        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                                ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get(); 
        
        $mode = "SUPER DISTRIBUTOR";
        $j = 0;
        foreach($d1 as $d)
        {
            $data = null;

            if($d->trans_type == 'WEB_RECHARGE')
            {               
                $data = $this->getNormalData($d->trans_option, $d->trans_id);
                $rs[$j][0] = $data;
                $j++;

            }
            else if($d->trans_type == 'API_RECHARGE')
            {               
                $data = $this->getAPINormalData($d->trans_option, $d->trans_id);
                $rs[$j][0] = $data;
                $j++;

            }
            
        }

        // Check User Name
        $rs1 = [];
        $j = 0;
        foreach($rs as $d)
        {
            foreach($d[0] as $r)
            {
                if($u_name != "" &&  $r->parent_name == $u_name)
                {
                    $rs1[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($u_name == "")
                {
                    $rs1[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }

        // Check Status
        $rs2 = [];
        $j = 0;
        if($u_status == "SUCCESS")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "SUCCESS" )
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else if($u_status == "FAILURE")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "PENDING" ||  $r->rech_status == "FAILURE")
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                    else if($u_mobile == "")
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                }
            }
        }

        $rs3 = [];
        $j = 0;
        // Recharge Amount
        foreach($rs2 as $d)
        {
            foreach($d[0] as $r)
            {
                if($u_amount != "" &&  $r->rech_amount == $u_amount)
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($u_amount == "")
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }

        
                        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

       
        //return response()->json($rs3, 200);
       
        $page = $request->page; // current page for pagination

        // manually slice array of product to display on page
        $perPage = 15;
        $offset = ($page-1) * $perPage;
        $products = array_slice($rs3, $offset, $perPage);

        // your pagination 
        $products = new Paginator($products, count($rs3), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);
        // use {{ $products->appends($_GET)->links() }} to dispaly your pagination

        return view('admin.adminrechargereport_super_view', ['user' => $ob, 'recharge' => $products, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'mode' => $mode]); 

        
        
    }

    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_3 = new UserRechargePaymentParentDetails;
        $rech_a_3 = new UserRechargePaymentParentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = $this->getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($request->user_name);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);

        $rs = [];
        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                                ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get(); 
        
        $mode = "SUPER DISTRIBUTOR";
        $j = 0;
        foreach($d1 as $d)
        {
            $data = null;

            if($d->trans_type == 'WEB_RECHARGE')
            {               
                $data = $this->getNormalData($d->trans_option, $d->trans_id);
                $rs[$j][0] = $data;
                $j++;

            }
            else if($d->trans_type == 'API_RECHARGE')
            {               
                $data = $this->getAPINormalData($d->trans_option, $d->trans_id);
                $rs[$j][0] = $data;
                $j++;

            }
            
        }

        // Check User Name
        $rs1 = [];
        $j = 0;
        foreach($rs as $d)
        {
            foreach($d[0] as $r)
            {
                if($u_name != "" &&  $r->parent_name == $u_name)
                {
                    $rs1[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($u_name == "")
                {
                    $rs1[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }

        // Check Status
        $rs2 = [];
        $j = 0;
        if($u_status == "SUCCESS")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "SUCCESS" )
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else if($u_status == "FAILURE")
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($r->rech_status == "PENDING" ||  $r->rech_status == "FAILURE")
                    {
                        if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                        else if($u_mobile == "")
                        {
                            $rs2[$j][0] = $d[0];
                            $j++;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            foreach($rs as $d)
            {
                foreach($d[0] as $r)
                {
                    if($u_mobile != "" &&  $r->rech_mobile == $u_mobile)
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                    else if($u_mobile == "")
                    {
                        $rs2[$j][0] = $d[0];
                        $j++;
                        break;
                    }
                }
            }
        }

        $rs3 = [];
        $j = 0;
        // Recharge Amount
        foreach($rs2 as $d)
        {
            foreach($d[0] as $r)
            {
                if($u_amount != "" &&  $r->rech_amount == $u_amount)
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
                else if($u_amount == "")
                {
                    $rs3[$j][0] = $d[0];
                    $j++;
                    break;
                }
            }
        }

        
                        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

       
        //return response()->json($rs3, 200);
       
        $page = $request->page; // current page for pagination

        // manually slice array of product to display on page
        $perPage = 15;
        $offset = ($page-1) * $perPage;
        $products = array_slice($rs3, $offset, $perPage);

        // your pagination 
        $products = new Paginator($products, count($rs3), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);
        // use {{ $products->appends($_GET)->links() }} to dispaly your pagination

       // return view('admin.adminrechargereport_super_view', ['user' => $ob, 'recharge' => $products, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'mode' => $mode]); 

        $headings = ['NO', 'SUPER DISTRIBUTOR NAME', 'DISTRIBUTOR NAME', 'USER NAME', 'MOBILE', 'NETWORK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL',  'MODE'];

        $j = 1;

        $content = [];

        $k = 0;

        foreach($rs3 as $f1)
        {
            $f = $f1[0];

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $rech_mode = "";
            $o_bal = 0;
            $z = 0;
            
            if(sizeof($f) > 1)
            {
                // Web Recharge
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($f[1]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                //print_r($f[1]);

                if($f[1]->super_parent_name != "NONE")
                {
                    if($f[1]->rech_status == "PENDING")
                    {
                        $reply_id = "";
                        $reply_date = "";
                        if($f[1]->userrechargeparent->rech_status == "PENDING")
                            $rech_status = "PENDING";
                        else
                        {
                            $rech_status = "FAILURE";
                            $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                            $reply_date = $f[1]->userrechargeparent->reply_date;
                        }
                            
                        $o_bal = floatval($f[1]->user_balance) + floatval($f[1]->rech_total);

                    }
                    else if ($f[1]->rech_status == "SUCCESS")
                    {
                        $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                        $reply_date = $f[1]->userrechargeparent->reply_date;
                        $rech_status = "SUCCESS";
                        $o_bal = floatval($f[1]->user_balance) + floatval($f[1]->rech_total);
                    }
                    else if ($f[1]->rech_status == "FAILURE")
                    {
                        $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                        $reply_date = $f[1]->userrechargeparent->reply_date;
                        $rech_status = "FAILURE";
                        $o_bal = floatval($f[1]->user_balance) - floatval($f[1]->rech_total);
                        $z = 1;
                    }

                    $rech_mode = $f[1]->userrechargeparent->rech_mode;
                }
                
                $o_bal = number_format($o_bal, 2, '.', '');
                $c_bal = floatval($f[1]->user_balance);
                $c_bal = number_format($c_bal, 2, '.', '');

                if($z == 0)
                {
                    $content[$k++] = [$j,$f[1]->super_parent_name, $f[1]->parent_name, $f[1]->user_name, $f[1]->rech_mobile, $net_name, $f[1]->rech_amount, 
                                        $f[1]->rech_net_per."-".$f[1]->rech_net_per_amt."-".$f[1]->rech_net_surp, $f[1]->rech_total, 
                                        $f[1]->trans_id, $reply_id, $f[1]->rech_date, $reply_date, $rech_status, 
                                        $o_bal, $c_bal,  $rech_mode];


                }
                else
                {
                    $content[$k++] = [$j,$f[1]->super_parent_name, $f[1]->parent_name, $f[1]->user_name, $f[1]->rech_mobile, $net_name, $f[1]->rech_amount, 
                                        '', $f[1]->rech_total, $f[1]->trans_id,'', '', '', $rech_status, 
                                        $o_bal, $c_bal,  $rech_mode];
                }
            }
            else
            {
                // API Recharge
                $z = 0;
                $net_name = "";
                foreach($network as $r)
                {
                    if($f[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                //print_r($f[0]);

                if($f[0]->super_parent_name == "NONE")
                {
                    if($f[0]->rech_status == "PENDING")
                    {
                        $reply_id = "";
                        $reply_date = "";
                        if($f[0]->userrechargeparent->rech_status == "PENDING")
                            $rech_status = "PENDING";
                        else
                        {
                            $rech_status = "FAILURE";
                            $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                            $reply_date = $f[0]->userrechargeparent->reply_date;
                        }
                            
                        $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);

                    }
                    else if ($f[0]->rech_status == "SUCCESS")
                    {
                        $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                        $reply_date = $f[0]->userrechargeparent->reply_date;
                        $rech_status = "SUCCESS";
                        $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);
                    }
                    else if ($f[0]->rech_status == "FAILURE")
                    {
                        $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                        $reply_date = $f[0]->userrechargeparent->reply_date;
                        $rech_status = "FAILURE";
                        $o_bal = floatval($f[0]->user_balance) - floatval($f[0]->rech_total);
                        $z = 1;
                    }

                    $rech_mode = $f[0]->userrechargeparent->rech_mode;
                }
                
                $o_bal = number_format($o_bal, 2, '.', '');
                $c_bal = floatval($f[0]->user_balance);
                $c_bal = number_format($c_bal, 2, '.', '');

                if($z == 0)
                {

                    $content[$k++] = [$j,$f[0]->parent_name, '', $f[0]->user_name, $f[0]->rech_mobile, $net_name, $f[0]->rech_amount, 
                                        $f[0]->rech_net_per."-".$f[0]->rech_net_per_amt."-".$f[0]->rech_net_surp, $f[0]->rech_total, 
                                        $f[0]->trans_id, $reply_id, $f[0]->rech_date, $reply_date, $rech_status, 
                                        $o_bal, $c_bal, $rech_mode];
                   
                }
                else
                {
                    $content[$k++] = [$j,$f[0]->parent_name, '', $f[0]->user_name, $f[0]->rech_mobile, $net_name, $f[0]->rech_amount, 
                                        '', $f[0]->rech_total, 
                                        $f[0]->trans_id, '','','', $rech_status, 
                                        $o_bal, $c_bal, $rech_mode];
                    
                }
            }
            
                                                    
            $j++;
        }
           
        $cc = [$headings, $content];

        $tit = "Recharge_details_super_distributor_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }

    public function getNormalData($trans_option, $trans_id)
    {
        $rech_3 = new UserRechargePaymentParentDetails;
        $data = null;

        if($trans_option == '1')
        {
            $data = $rech_3->with(['userrechargeparent'])->where(function($query){
                                                                return $query
                                                                ->where('rech_status', '=', 'PENDING')
                                                                ->orWhere('rech_status', '=', 'SUCCESS');
                                                            })
                                                            ->where('trans_id', '=', $trans_id)
                                                            ->where('rech_type', '=', 'WEB_RECHARGE')->get();
            
        }
        else if($trans_option == '2')
        {
            $data = $rech_3->with(['userrechargeparent'])->where([['trans_id', '=', $trans_id], 
                                                            ['rech_status', '=', 'FAILURE'],
                                                            ['rech_type', '=', 'WEB_RECHARGE']])
                                                            ->get();
        
        }

        return $data;

    }

    public function getAPINormalData($trans_option, $trans_id)
    {
        $rech_a_3 = new UserRechargePaymentParentApipartnerDetails;
        $data = null;

        if($trans_option == '1')
        {
            $data = $rech_a_3->with(['userrechargeparent'])->where(function($query){
                                                                return $query
                                                                ->where('rech_status', '=', 'PENDING')
                                                                ->orWhere('rech_status', '=', 'SUCCESS');
                                                            })
                                                            ->where('trans_id', '=', $trans_id)
                                                            ->where('rech_type', '=', 'API_RECHARGE')->get();
            
        }
        else if($trans_option == '2')
        {
            $data = $rech_a_3->with(['userrechargeparent'])->where([['trans_id', '=', $trans_id], 
                                                            ['rech_status', '=', 'FAILURE'],
                                                            ['rech_type', '=', 'API_RECHARGE']])
                                                            ->get();
        
        }
        return $data;

    }
}
