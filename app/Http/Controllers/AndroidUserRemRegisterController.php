<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\BankMobileCommon;
use App\Libraries\BankRemitter;
use App\Libraries\CyberBankRemitter;

use App\Models\UserBankRemitterDetails;

class AndroidUserRemRegisterController extends Controller
{
    //
    public function index($auth_token)
	{
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            return view('android.recharge_money_rm_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                    "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
       
        $z1 = 0;
        $d2 = [];

        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
        $user_rem_name = trim($request->user_rem_name);
        $user_rem_lname = trim($request->user_rem_lname);
        $user_address = trim($request->user_address);
        $user_city = trim($request->user_city);
        $user_state_code = trim($request->user_state_code);
        $user_pincode = trim($request->user_pincode);

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $data = ['user_name_1' => $user_name,
                    'user_name' => $user_rem_name,
                    'user_lname' => $user_rem_lname,
                    'user_address' => $user_address,
                    'user_city' => $user_city,
                    'user_state_code' => $user_state_code,
                    'user_pincode' => $user_pincode,
                    'mobileNo' => $msisdn,
                    'api_trans_id' => "",
                    'r_mode' => "WEB"
                ];

            /*$data = ['user_name_1' => $user_name, 'user_name' => $user_rem_name, 'user_address' => $user_address,
                    'user_city' => $user_city, 'user_state_code' => $user_state_code,
                        'user_pincode' => $user_pincode, 'msisdn' => $msisdn, 'api_trans_id' => '-', 'r_mode' => "WEB" ];

            list($z, $msg, $res) = BankRemitter::add($data);*/

            list($z, $msg, $res_content, $isVerified) = CyberBankRemitter::add($data);

            $result = [];

            if($z == 0)
            {
                if($isVerified == "0") {
                     return redirect('mobile_user_money_rm_otp_1/'.$msisdn.'/'. $auth_token);
                }
                else if($isVerified == "1") {
                    $op = "Entry is added Successfully...";
                    $z1 = 0;
                }
            }
            else if($z == 1)
            {
                $op = "Mode web is not Selected...";
                $z1 = 3;
            }
            else if($z == 2) {
                $op = "Account is Inactivated...";
                $z1 = 4;
            }
            else if($z == 3) {
                $op = "Already Registered...";
                $z1 = 5;
            }
            else if($z == 4) {
                $op = $msg;
                $z1 = 6;
            }
            else if($z == 5) {
                $op = "PENDING...";
                $z1 = 7;
            }
    
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'msisdn' => $msisdn]);
    }

    public function checkOtp($msisdn, $auth_token)
    {
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            return view('android.recharge_money_rm_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                    "auth_token" => $auth_token, 'msisdn' => $msisdn]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function otpStore(Request $request)
    {
       
        $z1 = 0;
        $d2 = [];

        $auth_token = trim($request->auth_token);
        $msisdn     = trim($request->msisdn);
        $otp        = trim($request->otp);     
        $remId      = "-";  

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            $details = UserBankRemitterDetails::select('rem_rep_opr_id')->where('msisdn', $msisdn)->first();

            if($details) {
                $remId = $details->rem_rep_opr_id;
            }

            $data = ['user_name_1' => $user_name,
                    'mobileNo' => $msisdn,
                    'otpNumber' => $otp,
                    'remId' => $remId,
                    'api_trans_id' => "",
                    'r_mode' => "WEB"
                ];

            list($z, $msg, $res_content) = CyberBankRemitter::userOtpCheck($data);

            $result = [];

            if($z == 0)
            {
                $op = "Entry is added Successfully...";
                return redirect('mobile_user_money_1?auth_token='.$auth_token);
            }
            else if($z == 1)
            {
                $op = "Mode web is not Selected...";
                $z1 = 3;
            }
            else if($z == 2) {
                $op = "Account is Inactivated...";
                $z1 = 4;
            }
            else if($z == 3) {
                $op = "Already Registered...";
                $z1 = 5;
            }
            else if($z == 4) {
                $op = $msg;
                $z1 = 6;
            }
            else if($z == 5) {
                $op = "PENDING...";
                $z1 = 7;
            }
    
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'msisdn' => $msisdn]);
    }
}
