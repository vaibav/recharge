<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\TransactionBankReply;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;

class AD_Pending_MoneyController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        $d2 = NetworkDetails::select('net_code','net_name')->where('net_status', '=', 1)->get();
        
        $d1 = AllTransaction::where('trans_type', '=', 'BANK_TRANSFER')->where('rech_status', '=', 'PENDING')->where('rech_option', '=', '0')->get(); 

        return view('admin.ad_pending_money', ['user' => $ob, 'bank' => $d1, 'network' => $d2]);

    }

    public function success_update($trans_id, $opr_id, $money_amt, Request $request)
    {
        $zx = 0;
        
        if(floatval($money_amt) <= 5000) {
            $zx = $this->storeTransferupdate_1($trans_id, $opr_id, "SUCCESS");
        }
        else if(floatval($money_amt) > 5000) {
            $zx = $this->storeTransferupdate_1($trans_id, $opr_id, "SUCCESS");
        }
 
        return redirect()->back()->with('msg', $zx);
       
    }

    public function failure_update($trans_id, $opr_id, $money_amt, Request $request)
    {
        $zx = 0;
        
        if(floatval($money_amt) <= 5000) {
            $zx = $this->storeTransferupdate_1($trans_id, $opr_id, "FAILURE");
        }
        else if(floatval($money_amt) > 5000) {
            $zx = $this->storeTransferupdate_1($trans_id, $opr_id, "FAILURE");
        }
 
        return redirect()->back()->with('msg', $zx);
       
    }


    // Common Functions
    // New Functions 
    public function storeTransferupdate_1($trans_id, $opr_id, $s_status)
    {
        if($s_status == "FAILURE")
        {
            TransactionBankReply::failure_update($trans_id, $opr_id);
        }
        else if($s_status == "SUCCESS")
        {
            TransactionBankReply::success_update($trans_id, $opr_id, "0", "0", "1");
        }
        return 1;
    }

    public function storeTransferupdate_1_1($trans_id, $opr_id, $s_status)
    {
       
        if($s_status == "FAILURE")
        {
            TransactionBankReply::failure_update($trans_id, $opr_id);
        }
        if($s_status == "SUCCESS")
        {
            TransactionBankReply::success_update($trans_id, $opr_id, "0", "0", "2"); 
        }
        return "1";
    }

    
    
}
