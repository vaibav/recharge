<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;
use App\Models\UserRechargeResultDetails;

use DB;
use PDF;
use EXCEL;

class AD_Reply_ReportController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = ApiProviderDetails::select('api_code','api_name')->orderBy('api_name', 'asc')->get();
        
        return view('admin.ad_reply_report_1', ['user' => $ob, 'api' => $d1]);
        
    }
   
    public function viewdate(Request $request)
    {
       
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $api_code = trim($request->api_code);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        if ($api_code != "-" && $api_code != "")
        {
            $d1 = UserRechargeResultDetails::where('a11', '=', $api_code)
                        ->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->paginate(15);
        }
        else
        {
            $d1 = UserRechargeResultDetails::whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'desc')->paginate(15);
        }
        
        $d2 = ApiProviderDetails::select('api_code','api_name')->orderBy('api_name', 'asc')->get();

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);
        
        return view('admin.ad_reply_report_2', ['user' => $ob, 'reply' => $d1, 'api' => $d2]);

    }
  
}
