<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\MobileUserDetails;

use App\Models\UserPersonalDetails;
use App\Models\UserChainDetails;
use App\Models\UserNetworkDetails;

use App\Models\NetworkDetails;

class AndroidUserController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_o1 = new NetworkDetails;

        $auth_token = trim($request->auth_token);

        $d1 = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        $user_name = "none";
        $user_type = "none";
        $user_ubal = "0";
       
        list($user_name, $user_type, $user_ubal) = $this->getUserDetails($auth_token);

        return view('android.user_creation', ['user_name' => $user_name, 'user_type' => $user_type, 'user_ubal' => $user_ubal, 'auth_token' => $auth_token, 'network' => $d1]);
    }
    

    public function getUserDetails($auth_token)
    {
        $mob_1 = new MobileUserDetails;

        $user_name = "none";
        $user_type = "none";
        $user_ubal = "0";

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();
        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
        }

        return array($user_name, $user_type, $user_ubal);
    }

    public function getCode($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $ux_code = 0;

        $d1 = $uacc_1->select('user_code')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_code = $d->user_code;
        }

        return $ux_code;
    }

    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    public function store(Request $request)
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_3 = new UserNetworkDetails;
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        // Declaration
        $file_name1 = "";
        $file_name2 = "";

        $auth_token = trim($request->auth_token);

        $user_name_p = "none";
        $user_type_p = "none";
        $user_ubal_p = "0";
       
        list($user_name_p, $user_type_p, $user_ubal_p) = $this->getUserDetails($auth_token);

        $parent_code = $this->getCode($user_name_p);
        $parent_name = $user_name_p;
        $parent_type = $user_type_p;

       

        $user_code = rand(1000,9999);
        $user_per_name = trim($request->user_per_name);
        $user_city = trim($request->user_city);
        $user_state = trim($request->user_state);
        $user_phone = trim($request->user_phone);
        $user_mobile = trim($request->user_mobile);
        $user_mail = trim($request->user_mail);
        $user_kyc = "AADHAR_CARD";

        $user_name = trim($request->user_name);
        $user_pwd = trim($request->user_pwd);
        $user_acc_type = trim($request->user_acc_type);
        $user_setup_fee = trim($request->user_setup_fee);
        
        //recharge type
        $user_rech = $request->user_rec_mode;
        $user_rec_mode = "";
        foreach ($user_rech as $c)
        { 
            $user_rec_mode = $user_rec_mode . $c. "@";
        }
        
        $user_api_url_1 = trim($request->user_api_url_1);
        $user_api_url_2 = trim($request->user_api_url_2);

        // Temporary File Name Storage
        $file_name1 = $user_code."_PHOTO.jpg";
        $file_name2 = $user_code."_KYC.jpg";

       
        
        // Insert Data
        $cnt = $user_1->where('user_code', '=', $user_code)->count();
        if($cnt == 0)
        {
            $cnt1 = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt1 == 0)
            {
                // Personal Details....
                $user_1->user_code = $user_code;
                $user_1->user_name = $user_name;
                $user_1->user_per_name = $user_per_name;
                $user_1->user_city = $user_city;
                $user_1->user_state = $user_state;
                $user_1->user_phone = $user_phone;
                $user_1->user_mobile = $user_mobile;
                $user_1->user_mail = $user_mail;
                $user_1->user_kyc = $user_kyc;
                $user_1->user_photo = $file_name1;
                $user_1->user_kyc_proof = $file_name2;

                $user_1->save();

                // Account Details.....
                $user_2->user_code = $user_code;
                $user_2->user_name = $user_name;
                $user_2->user_pwd = $user_pwd;
                $user_2->user_type = $user_acc_type;
                $user_2->user_setup_fee = $user_setup_fee;
                $user_2->parent_type = $parent_type;
                $user_2->parent_code = $parent_code;
                $user_2->parent_name = $parent_name;
                $user_2->user_rec_mode = $user_rec_mode;
                $user_2->user_api_url_1 = $user_api_url_1;
                $user_2->user_api_url_2 = $user_api_url_2;
                $user_2->user_status = 4;

                $user_2->save();

                // User Network Details.... 
                $arr1 = [];
                $arr2 = [];
                $date_time = date("Y-m-d H:i:s");
                $j = 1;
                foreach($data as $f)
                {
                    $n_code = trim($request['net_code_'.$j]);
                    $n_name = trim($request['net_name_'.$j]);
                    $n_perc = trim($request['net_per_'.$j]);
                    $n_surp = trim($request['net_surp_'.$j]);

                    if($n_perc != "")
                    {
                        $arr1 = array('user_code' => $user_code, 'user_name' => $user_name, 'net_code' => $n_code,
                        'user_net_per' => $n_perc, 'user_net_surp' => $n_surp, 
                        'created_at' => $date_time, 'updated_at' => $date_time);
                        array_push($arr2, $arr1);
                        
                    }

                    $j++;

                }

                $user_3->insert($arr2);

                // User Chain Details
                $d1 = $user_4->select('user_level', 'user_chain')->where('user_code', '=', $parent_code)->get();
                if($d1->count() > 0)
                {
                    $user_level = intval($d1[0]->user_level) + 1;
                    $user_chain = $d1[0]->user_chain."-".$user_code;
                }
                else
                {
                    $user_level = 1;
                    $user_chain = "1-".$user_code;
                }

                $user_4->user_code = $user_code;
                $user_4->user_name = $user_name;
                $user_4->parent_code = $parent_code;
                $user_4->parent_name = $parent_name;
                $user_4->user_level = $user_level;
                $user_4->user_chain = $user_chain;
                
                $user_4->save();

                // Account Balance Details.....
                $user_5->user_code = $user_code;
                $user_5->user_name = $user_name;
                $user_5->user_balance = "0.00";
                
                $user_5->save();

                $op = "User Entry is created successfully....";
              
            }
            else
            {
                $op = "User Name Already Exists...";
            }
        }
        else
        {
            $op = "User Code Already Exists....";
        }
        
       
        return redirect()->back()->with('msg', $op);
        
    }
}
