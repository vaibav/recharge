<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserRechargePaymentApipartnerDetails;

class PaymentCorrectController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $rech_1 = new UserRechargePaymentApipartnerDetails;
        
        $user_name = $request->user_name;
        $f_d = trim($request->f_date);
        $t_d = trim($request->t_date);
        
        $f_date = $f_d." 00:00:00";
        $t_date = $t_d." 23:59:59";

        $d1 = $rech_1->select(\DB::raw("COUNT(*) c, rech_date"))
                        ->where('user_name', '=', $user_name)
                        ->whereBetween('rech_date', [$f_date, $t_date])
                        ->groupBy("rech_date")
                        ->havingRaw("COUNT(*) > 1")
                        ->get();
        
        echo "<table border = '1' cellspacing = '0' cellpadding ='4'>";

        echo "<tr><td>NO</td><td>RECH TOTAL</td><td>U.BAL</td><td>STATUS</td><td>DATE</td><td>O.BAL</td><td>C.BAL</td><td>DEVIATION</td></tr>";
        
        $j = 1;
        foreach($d1 as $d)
        {
            $d2 = $rech_1->select('rech_total', 'user_balance', 'rech_date', 'rech_status')
                        ->where('user_name', '=', $user_name)
                        ->where('rech_date', '=', $d->rech_date)->get();

            $c = 0;
            $dev = "";
            foreach($d2 as $r)
            {
                $r_t = floatval($r->rech_total);
                $u_b = floatval($r->user_balance);

                $c_b = 0;
                $o_b = 0;
                if($d->rech_status = "SUCCESS" || $d->rech_status = "PENDING")
                {
                    $o_b = floatval($u_b) + floatval($r_t);
                    $c_b = floatval($u_b);
                }
                else if($d->rech_status = "FAILURE")
                {
                    $o_b = floatval($u_b) -  floatval($r_t);
                    $c_b = floatval($u_b);
                }

                if($c == $o_b)
                    $dev = "YES";

                $c = $o_b;

                echo "<tr><td>".$j."</td><td>".$r_t."</td><td>".$u_b."</td><td>".$r->rech_date."</td>";
                echo "<td>".$r->rech_status."</td><td>".$o_b."</td><td>".$c_b."</td><td>".$dev."</td></tr>";
                $j++;
            }
        }
                                                       
        
      
        
		
    }

}
