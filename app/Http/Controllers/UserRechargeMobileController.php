<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\MobileUserDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\ApiProviderDetails;

use App\Libraries\RechargeNewMobile;


class UserRechargeMobileController extends Controller
{
    //
    public function index_prepaid(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type1 = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }
        

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'PREPAID')->get();
        
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->getRechargeDetailsRetailer($user_name);

            return view('user.user_mobile_prepaid', ['network' => $d2, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        
       
        

        
        		
    }

    public function index_postpaid(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type1 = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }
        

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'POSTPAID')->get();
        
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->getRechargeDetailsRetailer($user_name);

            return view('user.user_mobile_postpaid', ['network' => $d2, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        
       
        

        
        		
    }
    
    
    public function index_dth(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type1 = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }
        

        $d1 = $net_1->select('net_type_code')->where('net_type_name', '=', 'DTH')->get();
        
        if($d1->count() > 0)
        {
            $code = $d1[0]->net_type_code;
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->getRechargeDetailsRetailer($user_name);

            return view('user.user_mobile_dth', ['network' => $d2, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        
       
        

        
        		
    }

    public function index_all(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $mob_1 = new MobileUserDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type1 = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];
        $d3 = [];
        $d4 = [];

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }
        

        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        
        if($d1->count() > 0)
        {
            foreach($d1 as $d)
            {
                if($d->net_type_name == "PREPAID") {
                    $pre_code = $d->net_type_code;
                }
                else if($d->net_type_name == "POSTPAID") {
                    $pos_code = $d->net_type_code;
                }
                else if($d->net_type_name == "DTH") {
                    $dth_code = $d->net_type_code;
                }
                
            }
            
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pre_code], ['net_status', '=', '1']])->get();
            
            $d3 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pos_code], ['net_status', '=', '1']])->get();

            $d4 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $dth_code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->getRechargeDetailsRetailer($user_name);

            return view('user.user_mobile_recharge', ['network1' => $d2, 'network2' => $d3, 'network3' => $d4, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        		
    }


    // Prepaid recharge
    public function prepaid_recharge(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        

        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_1);
        $user_mobile = trim($request->user_mobile_1);
        $user_amount = trim($request->user_amount_1);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            $z = RechargeNewMobile::add($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function postpaid_recharge(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        

        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_3);
        $user_mobile = trim($request->user_mobile_3);
        $user_amount = trim($request->user_amount_3);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            $z = RechargeNewMobile::add($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function dth_recharge(Request $request)
	{
        $mob_1 = new MobileUserDetails;
        

        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_2);
        $user_mobile = trim($request->user_mobile_2);
        $user_amount = trim($request->user_amount_2);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;

        $dc1 = $mob_1->select('user_name', 'user_type')->where('auth_token', '=', $auth_token)->get();

        if($dc1->count() > 0)
        {
            $user_name = $dc1[0]->user_name;
            $user_type1 = $dc1[0]->user_type;
            $user_ubal = $this->getBalance($user_name);
            $user_code = $this->getCode($user_name);

            if($user_type1 != "RETAILER")
                $z1 = 1;
            
        }
        else
        {
            $z1 = 2;
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            $z = RechargeNewMobile::add($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }



    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }

    public function getCode($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $ux_code = 0;

        $d1 = $uacc_1->select('user_code')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_code = $d->user_code;
        }

        return $ux_code;
    }

    public function getRechargeDetailsRetailer($user_name)
    {
        $rech_1 = new UserRechargeNewDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $rech_1->with(['recharge2'])->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
      // return response()->json($d1, 200);
        
        
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            $reply_id = $f->recharge2->reply_opr_id;
            $reply_date =$f->recharge2->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "<a class='btn-floating btn-small waves-effect waves-light blue'><i class='material-icons'>directions_run</i></a><br>";
                
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "<a class='btn-floating btn-small waves-effect waves-light red'><i class='material-icons'>clear</i></a><br>";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "<a class='btn-floating btn-small waves-effect waves-light red'><i class='material-icons'>clear</i></a><br>";
               
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "<a class='btn-floating btn-small waves-effect waves-light green white-text'><i class='material-icons'>check</i></a><br>";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            
                               
                                
           
            
            if($z == 0)
            {
                if(($j % 2) != 0)
                {
                    $str = $str."<div class = 'row' style='margin:4px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                }
                else
                {
                    $str = $str."<div class = 'row' style='margin:4px 0px;padding: 0px 0px;'>";
                }
                
                $str = $str."<div class='col s2 m2 center-align' style='padding: 12px 3px;'>".$status."</div>";   
            
                $str = $str . "<div class='col s7 m7' style='padding: 2px 3px;' >
                        
                          <p class = 'p_net'>".$net_name."</p>
                          <p class = 'p_mob'>".$f->rech_mobile."</p>
                          <p class = 'p_opr'>".$reply_id."</p>
                          
                        </div>
                        <div class='col s3 m3' style='padding: 2px 3px;'>
                             <p class = 'p_net'>&#x20B9;".$f->rech_amount."</p>
                            <p class = 'p_upa'>O:".$o_bal."</p>
                            <p class = 'p_dna'>C:".$c_bal."</p>
                           
                        </div>
                      </div>";

                $j++;
                
            }
            else
            {
                if(($j % 2) != 0)
                {
                    $str = $str."<div class = 'row' style='margin:4px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                }
                else
                {
                    $str = $str."<div class = 'row' style='margin:4px 0px;padding: 0px 0px;'>";
                }
                
                $str = $str." <div class='col s2 m2 center-align' style='padding: 12px 3px;'>
                        ".$status."
                      </div>";   
            
                $str = $str . "<div class='col s7 m7' style='padding: 2px 3px;' >
                        
                          <p class = 'p_net'>".$net_name."</p>
                          <p class = 'p_mob'>".$f->rech_mobile."</p>
                          <p class = 'p_opr'></p>
                          
                        </div>
                        <div class='col s3 m3' style='padding: 2px 3px;'>
                             <p class = 'p_net'>&#x20B9;".$f->rech_amount."</p>
                            <p class = 'p_upa'>O:".$o_bal."</p>
                            <p class = 'p_dna'>C:".$c_bal."</p>
                           
                        </div>
                      </div>";
            
                $j++;
                
            }  
            
            
            
            
           
        }
        
        return $str; 
    }
}
