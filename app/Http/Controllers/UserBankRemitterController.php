<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\BankRemitter;
use App\Libraries\Beneficiary;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBankAgentDetails;

class UserBankRemitterController extends Controller
{
    //
    public function index(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        $msisdn = trim($request->msisdn);

        return view('user.money_remitter_entry', ['user' => $ob, 'msisdn' => $msisdn]);
    
    }
    
    public function index_remitter_check(Request $request)
	{
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $rem_1->where('user_name', '=', $ob->user)->get();

        return view('user.money_user_check', ['user' => $ob, 'remitter' => $d1]);
    
    }

    public function store(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

        $this->validate($request, ['user_rem_name' => 'required',
                                    'user_address' => 'required',
                                    'user_city' => 'required',
                                    'user_pincode' => 'required',
                                    'msisdn' => 'required'
                                ], 
                                ['user_rem_name.required' => ' Remitter Name is required.',
                                'user_address.required' => ' Address is required.',
                                'user_city.required' => ' Remitter City is required.',
                                'user_pincode.required' => ' Pincode is required.',
                                'msisdn.required' => ' Remitter Mobile No is required.'
                                ]);


        //$agent_id = $this->getAgentId($ob->user);
        
        $msisdn = trim($request->msisdn);

        $data = ['user_name_1' => $ob->user,
                    'user_name' => trim($request->user_rem_name),
                    'user_address' => trim($request->user_address),
                    'user_city' => trim($request->user_city),
                    'user_state_code' => trim($request->user_state_code),
                    'user_pincode' => trim($request->user_pincode),
                    'msisdn' => $msisdn,
                    'api_trans_id' => "",
                    'r_mode' => "WEB"
                ];

            list($z, $msg, $res_content) = BankRemitter::add($data);
            
            $result = [];

            if($z == 0)
            {
                $op = "Entry is added Successfully...";
                $request->session()->put('customer_msisdn', $msisdn);
                $request->session()->put('result_data', $result);
                return redirect('bank_remitter_direct_check');
            }
            else if($z == 1)
                $op = "Mode web is not Selected...";
            else if($z == 2)
                $op = "Account is Inactivated...";
            else if($z == 3)
                $op = "Already Registered...";
            else if($z == 4)
                $op = $msg;
            else if($z == 5)
                $op = "PENDING...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
	
    }

    public function user_check(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 1;
        $op = "";

        $agent_id = $this->getAgentId($ob->user);
        $msisdn = trim($request->msisdn);

        $data = ['user_name_1' => $ob->user,
                    'msisdn' => $msisdn,
                    'r_mode' => "WEB"
                ];

        list($z, $op, $result) = BankRemitter::userCheck($data);

        if($z == 0)
        {
            $request->session()->put('customer_msisdn', $msisdn);
            $request->session()->put('result_data', $result);
            
            $y = Beneficiary::checkServerBeneficiary($result, $ob->user, $msisdn);

            return redirect('dashboard_money_new');
        }
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'msisdn' => $msisdn]);
	
    }

    public function user_check_direct(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 1;
        $op = "";
        $msisdn = "";

        $agent_id = $this->getAgentId($ob->user);

        if($request->session()->has('customer_msisdn'))
        {
            $msisdn = $request->session()->get('customer_msisdn');
        }
        

        $data = ['user_name_1' => $ob->user,
                    'msisdn' => $msisdn,
                    'r_mode' => "WEB"
                ];
        
        if($msisdn != "")
        {
            list($z, $op, $result) = BankRemitter::userCheck($data);

            if($z == 0)
            {
                $request->session()->put('customer_msisdn', $msisdn);
                $request->session()->put('result_data', $result);
                return redirect('dashboard_money_new');
            }
        }


        return redirect('money_user_check');
	
    }

    public function view_one(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $d1 = BankRemitter::view_user($ob->user);

        return view('user.bank_remitter_view', ['user' => $ob, 'data' =>$d1]);

    }

    public function view_all(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $d1 = BankRemitter::view_all();

        return view('admin.bank_remitter_view_all', ['user' => $ob, 'data' =>$d1]);

    }

    public static function getAgentId($user_name)
    {
        $agen_1 = new UserBankAgentDetails;
        
        $agent_id = 0;

        $d1 = $agen_1->where('user_name', '=', $user_name)
                        ->where('agent_status', '=', 'SUCCESS')->get();

        if($d1->count() > 0)
        {
            $agent_id = $d1[0]->agent_id;
        }

        return $agent_id;
    }

    public function checkRemitterMobile($rem_mobile, Request $request)
    {
        $user_1 = new UserBankRemitterDetails;
        $cnt = 0;

        $cnt = $user_1->where('msisdn', '=', $rem_mobile)->count();

        echo $cnt;
    }

    public function update_success(Request $request, $rem_id, $opr_id)
    {

        $ob = GetCommon::getUserDetails($request);

        $z = BankRemitter::update($rem_id, $opr_id, "SUCCESS");

        if($z == 1)
            $op = "Entry is Updated Successfully...";
        else if($z == 2)
            $op = "Invalid Entry...";
        else
            $op = "No Entry is Added...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function update_failure(Request $request, $rem_id, $opr_id)
    {

        $ob = GetCommon::getUserDetails($request);

        $z = BankRemitter::update($rem_id, $opr_id, "FAILURE");

        if($z == 1)
            $op = "Entry is Updated Successfully...";
        else if($z == 2)
            $op = "Invalid Entry...";
        else
            $op = "No Entry is Added...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

}
