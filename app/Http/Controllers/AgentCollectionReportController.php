<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class AgentCollectionReportController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        return view('agent.collection_report', ['user' => $ob]);
        
    }

    public function view(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'from_date' => 'required',
            'to_date' => 'required'
        ],
        [
            'from_date.required' => ' The From Date is required.',
            'to_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $agent_name = $ob->user;
        $agent_name = strtoupper($agent_name);

        $d1 = $pay_1->whereRaw('upper(agent_name) = ?',[$agent_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->get(); 
    
        return view('agent.collection_report_view', ['user' => $ob, 'collection' => $d1]);
        
    }
}
