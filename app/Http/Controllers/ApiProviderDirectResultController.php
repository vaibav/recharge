<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderDirectResultDetails;
use App\Models\UserAccountBalanceDetails;

class ApiProviderDirectResultController extends Controller
{
    //
    public function index(Request $request)
	{
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderDirectResultDetails;

        $api_code = 0;

        $data1 = $api_1->select('api_code','api_name')->get();
        $data2 = $api_2->select('api_res_tr_id','api_code', 'api_res_para_name', 'api_res_para_field', 'api_res_para_value')
                       ->where('api_code', '=', $api_code)->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.apiproviderdirectresult', ['api1' => $data1, 'api2' => $data2, 'api_code' => $api_code, 'user' => $ob]);

        
    }

    public function view($api_code, Request $request)
	{
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderDirectResultDetails;

       
        $data1 = $api_1->select('api_code','api_name')->get();
        $data2 = $api_2->select('api_res_tr_id','api_code', 'api_res_para_name', 'api_res_para_field', 'api_res_para_value')
                       ->where('api_code', '=', $api_code)->get();

        
        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.apiproviderdirectresult', ['api1' => $data1, 'api2' => $data2, 'api_code' => $api_code, 'user' => $ob]);

    }

    public function store(Request $request)
	{
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderDirectResultDetails;
        
        $op = "";


        $res_tr_id = rand(1000,9999);
        $api_code = trim($request->api_code);
        $res_para_name = trim($request->api_res_para_name); 
        $res_para_field = trim($request->api_res_para_field); 
        $res_para_value = trim($request->api_res_para_value);

        if($res_para_value == "")
            $res_para_value = "-";

        $cnt = 0;
        if($res_para_name != 'STATUS 1')
        {
            if($res_para_name != 'STATUS 2')
            {
                if($res_para_name != 'DATA')
                {
                    if($res_para_name != 'ARRAY')
                    {
                        $cnt = $api_2->where([['api_res_para_name', '=', $res_para_name], ['api_code', '=', $api_code]])->count();
                    }
                }
            }
        }

        if($cnt == 0)
        {

            // Insert API Result Record... 
            $api_2->api_res_tr_id = $res_tr_id;
            $api_2->api_code = $api_code;
            $api_2->api_res_para_name = $res_para_name;
            $api_2->api_res_para_field = $res_para_field;
            $api_2->api_res_para_value = $res_para_value;
            $api_2->save();           
            
            $op = "Record is inserted...";
            
        }
        else
        {
            $op = "Error! Record is already inserted...";
        }

        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function delete($tr_id)
	{
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderDirectResultDetails;

        $api_code = 0;

        $cnt = $api_2->where('api_res_tr_id', '=', $tr_id)->count();
        
        if($cnt > 0)
        {
            // Get API Code
            $data3 = $api_2->select('api_code')->where('api_res_tr_id', '=', $tr_id)->get();

            foreach($data3 as $d)
            {
                $api_code = $d->api_code;
            }

            // Delete Record... 
            $api_2->where('api_res_tr_id', '=', $tr_id)->delete();
            $op = "Record is deleted Successfully...";
        }
        else
        {
                            
            $op = "No Record Found...";

        }

       
        return redirect()->back()->with('result', [ 'output' => $op]);

        
    }
}
