<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportAdmin;

use App\Models\AllTransaction;
use App\Models\UserAccountDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

use DB;
use PDF;
use EXCEL;

class AD_Rech_ReportController extends Controller
{
    //
    public function index(Request $request)
    {
        $user_1 = new UserAccountDetails;
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $data1 = $user_1->with(['personal'])->select('user_code','user_name')->where('user_type', '=', 'RETAILER')
                                                           ->orWhere('user_type', '=', 'API PARTNER')->orderBy('user_name')->get();      
        $d1 = $net_2->select('net_code','net_name')->get();

        $dx1 = $api_1->select('api_code','api_name')->where('api_status', '=', 1)->get();
        

        $ob = GetCommon::getUserDetails($request);

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        $bil_code = "0";
        $mny_code = "0";

        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();
        foreach($d1 as $d)
        {
            if($d->net_type_name == "PREPAID") {
                $pre_code = $d->net_type_code;
            }
            else if($d->net_type_name == "POSTPAID") {
                $pos_code = $d->net_type_code;
            }
            else if($d->net_type_name == "DTH") {
                $dth_code = $d->net_type_code;
            }
            else if($d->net_type_name == "BILL PAYMENT") {
                $bil_code = $d->net_type_code;
            }
            else if($d->net_type_name == "MONEY TRANSFER") {
                $mny_code = $d->net_type_code;
            }
            
        }

        $code = [];
        $code[0] = $pre_code;
        $code[1] = $pos_code;
        $code[2] = $dth_code;

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', '1')
                            ->where(function($query) use ($code){
                                return $query
                                ->where('net_type_code', '=', $code[0])
                                ->orWhere('net_type_code', '=', $code[1])
                                ->orWhere('net_type_code', '=', $code[2]);
                            })->get();

        $ar_web = [];
        foreach($d2 as $d)
        {
            array_push($ar_web, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_web1 = json_encode($ar_web);

        $d3 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $bil_code], ['net_status', '=', '1']])->get();

        $ar_bill = [];
        foreach($d3 as $d)
        {
            array_push($ar_bill, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_bill1 = json_encode($ar_bill);

        //-------------------------------------------

        $d4 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $mny_code], ['net_status', '=', '1']])->get();

        $ar_money = [];
        foreach($d4 as $d)
        {
            array_push($ar_money, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_money1 = json_encode($ar_money);

        return view('admin.ad_rech_report_1', ['user' => $ob, 'user1' => $data1, 'web' => $ar_web1, 'bill' => $ar_bill1, 'money' => $ar_money1, 'api' => $dx1]);
        
    }
   
   public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $u_name = trim($request->user_name);
        $rech_type = trim($request->rech_type);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
        $u_api_code_1 = trim($request->api_code);
        

        
        $dc4 = RechargeReportAdmin::getRechargeReportDetails_new_2($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, $u_api_code_1);

        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();

        //Total Calculation-------------------------------------------------------
        //recharge
        $wsua_tot = 0;
        $wsut_tot = 0;
        $wfua_tot = 0;
        $wfut_tot = 0;
        $wrea_tot = 0;
        $wret_tot = 0;
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
        //money
        $msua_tot = 0;
        $msut_tot = 0;
        $mfua_tot = 0;
        $mfut_tot = 0;
        $mrea_tot = 0;
        $mret_tot = 0;

        $dc7 = $dc4;

      /*  foreach($dc7 as $d)
        {
            
            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    
                    $rech_status = "";
                    $status = "";
                    $r_tot = 0;
                    $r_amt = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                        $r_amt = $d->newrecharge1[0]->rech_amount;
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                        $r_amt = $d->newrecharge1[1]->rech_amount;
                    }
                    if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $wfua_tot = floatval($wfua_tot) + floatval($r_amt);
                        $wfut_tot = floatval($wfut_tot) + floatval($r_tot);
                        $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                        $wret_tot = floatval($wret_tot) + floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $wsua_tot = floatval($wsua_tot) + floatval($r_amt);
                        $wsut_tot = floatval($wsut_tot) + floatval($r_tot);
                        $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                        $wret_tot = floatval($wret_tot) + floatval($r_tot);
                    }

                }
                
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;
                $r_amt = 0;
                

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $r_amt = $d->billpayment[0]->con_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $r_amt = $d->billpayment[1]->con_amount;
                }
                
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $efua_tot = floatval($efua_tot) + floatval($r_amt);
                    $efut_tot = floatval($efut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $esua_tot = floatval($esua_tot) + floatval($r_amt);
                    $esut_tot = floatval($esut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
               
                $rech_status = "";
                $status = "";
                $r_tot = 0;
                $r_amt = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $r_amt = $d->moneytransfer[0]->mt_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $r_amt = $d->moneytransfer[1]->mt_amount;
                    
                }
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $mfua_tot = floatval($mfua_tot) + floatval($r_amt);
                    $mfut_tot = floatval($mfut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $msua_tot = floatval($msua_tot) + floatval($r_amt);
                    $msut_tot = floatval($msut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
               
            }                                        
                            
        }*/

        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
        $u_api_code_1 = trim($request->api_code);

        if($rech_type == "ALL")
        {
            $q = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'RECHARGE');

            $wrea_tot = $q->sum('rech_amount');
            $wrea_tot = round($wrea_tot, 2);
            $wret_tot = $q->sum('ret_total');
            $wret_tot = round($wret_tot, 2);

            $wsua_tot = $q->where('rech_status', 'SUCCESS')->sum('rech_amount');
            $wsua_tot = round($wsua_tot, 2);
            $wsut_tot = $q->where('rech_status', 'SUCCESS')->sum('ret_total');
            $wsut_tot = round($wsut_tot, 2);

            $wfua_tot = floatval($wrea_tot) - floatval($wsua_tot);
            $wfut_tot = floatval($wret_tot) - floatval($wsut_tot);

            $wrea_tot = $q->where('rech_status', 'SUCCESS')->count();

            //Money Transfer
            $p = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'BANK_TRANSFER');

            $mrea_tot = $p->sum('rech_amount');
            $mrea_tot = round($mrea_tot, 2);
            $mret_tot = $p->sum('ret_total');
            $mret_tot = round($mret_tot, 2);

            $msua_tot = $p->where('rech_status', 'SUCCESS')->sum('rech_amount');
            $msua_tot = round($msua_tot, 2);
            $msut_tot = $p->where('rech_status', 'SUCCESS')->sum('ret_total');
            $msut_tot = round($msut_tot, 2);

            $mfua_tot = floatval($mrea_tot) - floatval($msua_tot);
            $mfut_tot = floatval($mret_tot) - floatval($msut_tot);

            $mrea_tot = $p->where('rech_status', 'SUCCESS')->count();


        }
        else if($rech_type == "RECHARGE")
        {
            $q = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'RECHARGE');

            if($u_name != "") {
                $q = $q->where('user_name' , $u_name);
            }

            if($u_status != "" && $u_status != "-") {
                $q = $q->where('rech_status' , $u_status);
            }

            if($u_mobile != "") {
                $q = $q->where('rech_mobile' , $u_mobile);
            }

            if($u_net_code_1 != "" && $u_net_code_1 != "-") {
                $q = $q->where('net_code' , $u_net_code_1);
            }

            if($u_api_code_1 != "" && $u_api_code_1 != "-") {
                $q = $q->where('api_code' , $u_api_code_1);
            }

            $wrea_tot = $q->sum('rech_amount');
            $wrea_tot = round($wrea_tot, 2);
            $wret_tot = $q->sum('ret_total');
            $wret_tot = round($wret_tot, 2);

            $wsua_tot = $q->where('rech_status', 'SUCCESS')->sum('rech_amount');
            $wsua_tot = round($wsua_tot, 2);
            $wsut_tot = $q->where('rech_status', 'SUCCESS')->sum('ret_total');
            $wsut_tot = round($wsut_tot, 2);

            $wfua_tot = floatval($wrea_tot) - floatval($wsua_tot);
            $wfut_tot = floatval($wret_tot) - floatval($wsut_tot);

            $wrea_tot = $q->where('rech_status', 'SUCCESS')->count();
        }    
        else if($rech_type == "BANK_TRANSFER")
        {
            $p = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'BANK_TRANSFER');

            $mrea_tot = $p->sum('rech_amount');
            $mrea_tot = round($mrea_tot, 2);
            $mret_tot = $p->sum('ret_total');
            $mret_tot = round($mret_tot, 2);

            $msua_tot = $p->where('rech_status', 'SUCCESS')->sum('rech_amount');
            $msua_tot = round($msua_tot, 2);
            $msut_tot = $p->where('rech_status', 'SUCCESS')->sum('ret_total');
            $msut_tot = round($msut_tot, 2);

            $mfua_tot = floatval($mrea_tot) - floatval($msua_tot);
            $mfut_tot = floatval($mret_tot) - floatval($msut_tot);

            $mrea_tot = $p->where('rech_status', 'SUCCESS')->count();
        }    

       

        $total = ['wsua_tot' => $wsua_tot, 'wsut_tot' => $wsut_tot, 'wfua_tot' => $wfua_tot, 'wfut_tot' => $wfut_tot, 
                    'wrea_tot' => $wrea_tot, 'wret_tot' => $wret_tot,
                    'esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot,
                    'msua_tot' => $msua_tot, 'msut_tot' => $msut_tot, 'mfua_tot' => $mfua_tot, 'mfut_tot' => $mfut_tot, 
                    'mrea_tot' => $mrea_tot, 'mret_tot' => $mret_tot];

        // Pagination-------------------------------------------------------------
       /* $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }
        

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);*/

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $dc4->setPath($cus_url);
        
        return view('admin.ad_rech_report_2', ['user' => $ob, 'recharge' => $dc4, 'd2' => $d2, 'd3' => $d3, 'total' => $total]);

    }
  
}
