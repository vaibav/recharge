<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetAPICommon;
use App\Libraries\BankTransfer;

class AP_API_MoneyTransferController extends Controller
{
    //
    public function transfer(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $beneficiary_id = trim($request->beneficiary_id);
        $beneficiary_account_no = trim($request->beneficiary_account_no);
        $beneficiary_name = trim($request->beneficiary_name);
        $beneficiary_bank_code = trim($request->beneficiary_bank_code);
        $beneficiary_ifsc_code = trim($request->beneficiary_ifsc_code);
        $api_trans_id = trim($request->client_trans_id);
        $transfer_type = trim($request->transfer_type);
        $amount = trim($request->amount);
        $msisdn = trim($request->msisdn);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        $trans_id = "VBM".rand(10000000,99999999);
        $otp_status ="0";
        $rep_trans_id = "";
        $remarks ="";

        if($q == 1)
        {
            // Valid API Partner
            $data = ['agent_id' => "0",
                        'msisdn' => $msisdn,
                        'ben_id' => $beneficiary_id,
                        'client_trans_id' => $trans_id,
                        'transfer_type' => $transfer_type,
                        'amount' => $amount,
                        'beneficiary_id' => $beneficiary_id,
                        'beneficiary_account_no' => $beneficiary_account_no,
                        'beneficiary_bank_code' => $beneficiary_bank_code,
                        'beneficiary_name' => $beneficiary_name,
                        'code' => $user_code,
                        'user' => $user_name,
                        'ubal' => $user_ubal,
                        'api_trans_id' => $api_trans_id,
                        'r_mode' => 'API'
                    ];
            
            $v = [];
            $v = $this->checkValues($data);

            if($v[0] == 0)
            {
                list($z, $msg, $otp_status, $rep_trans_id, $remarks, $res_content) = BankTransfer::add($data);

                $result = (array)json_decode($res_content);
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function checkValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['beneficiary_account_no']))
        {
            $v[0] = 2;
            $v[1] = "Invalid Account No";
        }
        else if($data['beneficiary_id'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary Id";
        }
        else if($data['transfer_type'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid transfer_type";
        }
        else if(!GetAPICommon::checkNumber($data['amount']))
        {
            $v[0] = 2;
            $v[1] = "Invalid Amount...";
        }
        else if($data['api_trans_id'] == "")
        {
            $v[0] = 2;
            $v[1] = "Invalid Beneficiary ID";
        }

        return $v;
    }
}
