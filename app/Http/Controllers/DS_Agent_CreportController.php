<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class DS_Agent_CreportController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
       
        $d1 = CollectionCommon::getArea($ob->user);


        return view('distributor.agent_collection_pending_1', ['user' => $ob, 'user_details' => $d1]);
        
    }

    public function view(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;


        $area_name = trim($request->area_name);

        $ob = GetCommon::getUserDetails($request);

        $parent = $this->getParent($ob->user);

        if($area_name == "All") {
            $d2 = CollectionCommon::getAllUser($ob->user);
        }
        else
        {
            $d2 = CollectionCommon::getUser($ob->user, $area_name);
        }

        $details = [];

        foreach($d2 as $d)
        {
    
            $user_name = strtoupper($d[0]);

            $de_tot = 0;
            $ce_tot = 0;
            $pe_tot = 0;

            // debit (taken)
            $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // credit (paid)
            $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // round two digits fraction
            $de_tot = round($de_tot, 2);
            $ce_tot = round($ce_tot, 2);

            $pe_tot = floatval($de_tot) - floatval($ce_tot);

            $de_tot = $this->convertNumber($de_tot);
            $ce_tot = $this->convertNumber($ce_tot);
            $pe_tot = $this->convertNumber($pe_tot);
            
            if($pe_tot != "0.00")
            {
                array_push($details, ['user_name' => $user_name, 'per_name' => $d[1], 'user_mobile' => $d[2], 
                                    'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot]);
            }
            
        }
                                    

        return view('distributor.agent_collection_pending_2', ['user' => $ob, 'c_details' => $details, 'area_name' => $area_name]);
        
    }

    public function getParent($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $parent = "admin";

        $d1 = $uacc_1->select('parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $parent = $d1[0]->parent_name;
        }
        
        return $parent;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
