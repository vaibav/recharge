<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\GetCommon;
use App\Models\UserBeneficiaryAccVerifyDetails;

class RetMoneyVerifyController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.report_money_verify', ['user' => $ob]);
        
    }

    public function viewdate(Request $request)
    {
        $bank_2 = new UserBeneficiaryAccVerifyDetails;
       
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $user_name = $ob->user;
        $user_name = strtoupper($user_name);
      
        $d1 = $bank_2->whereBetween('ben_date', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$user_name])
                            ->orderBy('id', 'desc')->paginate(15);

        $tot = $bank_2->whereBetween('ben_date', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$user_name])
                            ->where('ben_status', '=', 'SUCCESS')
                            ->sum('ben_surplus');
    
        $cus_url = $request->url()."?".$request->getQueryString();

        $d1->setPath($cus_url);

        return view('retailer.report_money_verify_view', ['user' => $ob, 'money' => $d1, 'from_date' => $date_1, 'to_date' => $date_2, 'total' => $tot]); 

    }
}
