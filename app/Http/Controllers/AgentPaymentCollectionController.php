<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class AgentPaymentCollectionController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;


        $area_name = "NONE";

        $ob = GetCommon::getUserDetails($request);

        $d1 = $area_1->select('area_name')->where('user_name', '=', $ob->user)->where('area_status', '=', '1')->orderBy('user_name', 'asc')->get();

        if($d1->count() > 0)
        {
            $area_name = $d1[0]->area_name;
        }

        

       
        $d2 = CollectionCommon::getUser("SUPER DISTRIBUTOR", $area_name);

        $details = [];

        foreach($d2 as $d)
        {
            $user_name = strtoupper($d[0]);

            $de_tot = 0;
            $ce_tot = 0;
            $pe_tot = 0;

            // debit (taken)
            $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // credit (paid)
            $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // round two digits fraction
            $de_tot = round($de_tot, 2);
            $ce_tot = round($ce_tot, 2);

            $pe_tot = floatval($de_tot) - floatval($ce_tot);

            $de_tot = $this->convertNumber($de_tot);
            $ce_tot = $this->convertNumber($ce_tot);
            $pe_tot = $this->convertNumber($pe_tot);
            
            if($pe_tot != "0.00")
            {
                array_push($details, ['user_name' => $user_name, 'per_name' => $d->user_per_name, 'user_mobile' => $d->user_mobile, 
                                    'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot]);
            }
            
        }
                                    

        return view('agent.collection_details', ['user' => $ob, 'c_details' => $details, 'area_name' => $area_name]);
        
    }


    public function pay_agent(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;


        $area_name = "NONE";

        $ob = GetCommon::getUserDetails($request);

        $d1 = $area_1->select('area_name')->where('user_name', '=', $ob->user)->where('area_status', '=', '1')->orderBy('user_name', 'asc')->get();

        if($d1->count() > 0)
        {
            $area_name = $d1[0]->area_name;
        }

        // Get parent
        $parent = $this->getParent($ob->user);

        $d2 = CollectionCommon::getUser($parent, $area_name);

    
        $user_details = ['user_name' => "0", 'c_details' => [], 
                                'de_tot' => "0", 'ce_tot' => "0", 'pe_tot' => "0"];


        return view('agent.pay_amount', ['user' => $ob, 'user1' => $d2, 'area_name' => $area_name, 'user_details' => $user_details]);
        
    }

    public function pay_agent_1($user_name, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;


        $area_name = "NONE";

        $ob = GetCommon::getUserDetails($request);

        $d1 = $area_1->select('area_name')->where('user_name', '=', $ob->user)->where('area_status', '=', '1')->get();

        if($d1->count() > 0)
        {
            $area_name = $d1[0]->area_name;
        }

       
        $parent = $this->getParent($ob->user);

        $d2 = CollectionCommon::getUser($parent, $area_name);


        //---------------------------------------

        $user_name = strtoupper($user_name);

        $de_tot = 0;
        $ce_tot = 0;
        $pe_tot = 0;

        // debit (taken)
        $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                    ->where('pay_type', '=', 'C')
                    ->where('pay_status', '=', '1')
                    ->sum('pay_amount'); 

        // credit (paid)
        $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                    ->where('pay_type', '=', 'D')
                    ->where('pay_status', '=', '1')
                    ->sum('pay_amount'); 

        // round two digits fraction
        $de_tot = round($de_tot, 2);
        $ce_tot = round($ce_tot, 2);

        $pe_tot = floatval($de_tot) - floatval($ce_tot);

        $de_tot = $this->convertNumber($de_tot);
        $ce_tot = $this->convertNumber($ce_tot);
        $pe_tot = $this->convertNumber($pe_tot);

        $d3 = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where(function($query) {
                            return $query
                            ->where('pay_type', '=', 'C')
                            ->orWhere('pay_type', '=', 'D');
                        })
                        ->orderBy('id', 'asc')->get();
        
        $user_details = ['user_name' => $user_name, 'c_details' => $d3, 
                            'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot];


        return view('agent.pay_amount', ['user' => $ob, 'user1' => $d2, 'area_name' => $area_name, 'user_details' => $user_details]);
        
    }


    public function pay_agent_store(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $z = 0;
        $op = "Not Paid";

        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $trans_id = "D".rand(100000,999999);
        $bill_no = rand(100000,999999);
        $user_name = trim($request->user_name);
        $pay_amt = trim($request->pay_amt);
        $pay_date = trim($request->pay_date);
        $pay_mode = trim($request->pay_mode);
        $pay_option = trim($request->pay_option);
        $date_time = date("Y-m-d H:i:s");
        $parent = $this->getParent($ob->user);

        if($pay_option == "") {
            $pay_option = "-";
        }

        $pay_date = $pay_date." ".date("H:i:s");

        // Upload Image
        $file_name1 = "-";

        if ($request->hasFile('pay_image')) {
            //
            $photo = $request->file('pay_image');
            $extension = $photo->getClientOriginalExtension();
            $file_name1 = $trans_id."_BANK.".$extension;
            $photo->move(public_path("/uploadbankreceipt"), $file_name1);
        }

        $ar1 = ['trans_id' => $trans_id, 'bill_no' => $bill_no, 'user_name' => $user_name, 'pay_type' => "D",
                    'pay_amount' => $pay_amt, 'pay_mode' => $pay_mode, 'pay_option' => $pay_option, 'pay_image' => $file_name1,
                    'from_user' => $parent, 'pay_date' => $pay_date, 
                    'agent_name' => $ob->user, 'pay_remarks' => "PAID_PAYMENT", 'pay_status' => '0', 'created_at' => $date_time, 'updated_at' => $date_time];
        

        $i = $pay_1->insert($ar1);

        if($i > 0)
        {
            $op = "Amount is paid Successfully....";
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
        
    }


    public function index_admin(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
       
        $d1 = CollectionCommon::getArea($ob->user);


        return view('admin.collection', ['user' => $ob, 'user_details' => $d1]);
        
    }

    public function view_admin(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;


        $area_name = trim($request->area_name);

        $ob = GetCommon::getUserDetails($request);

        $parent = $this->getParent($ob->user);

        if($area_name == "All") {
            $d2 = CollectionCommon::getAllUser($ob->user);
        }
        else
        {
            $d2 = CollectionCommon::getUser($ob->user, $area_name);
        }

        $details = [];

        foreach($d2 as $d)
        {
    
            $user_name = strtoupper($d[0]);

            $de_tot = 0;
            $ce_tot = 0;
            $pe_tot = 0;

            // debit (taken)
            $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // credit (paid)
            $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // round two digits fraction
            $de_tot = round($de_tot, 2);
            $ce_tot = round($ce_tot, 2);

            $pe_tot = floatval($de_tot) - floatval($ce_tot);

            $de_tot = $this->convertNumber($de_tot);
            $ce_tot = $this->convertNumber($ce_tot);
            $pe_tot = $this->convertNumber($pe_tot);
            
            if($pe_tot != "0.00")
            {
                array_push($details, ['user_name' => $user_name, 'per_name' => $d[1], 'user_mobile' => $d[2], 
                                    'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot]);
            }
            
        }
                                    

        return view('admin.collection_view', ['user' => $ob, 'c_details' => $details, 'area_name' => $area_name]);
        
    }

    public function getParent($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $parent = "admin";

        $d1 = $uacc_1->select('parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $parent = $d1[0]->parent_name;
        }
        
        return $parent;
    }

    public function getParentType($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $parent = "admin";

        $d1 = $uacc_1->select('parent_type')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $parent = $d1[0]->parent_type;
        }
        
        return $parent;
    }

    

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
