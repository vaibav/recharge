<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class DS_ANDR_Agent_CpendingController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
       
        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = CollectionCommon::getArea($user_name);

            return view('android.ds_agent_collection_pending_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'area_details' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $area_name = trim($request->area_name);

        $z1 = 0;
        $d2 = [];
       
        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $parent = $this->getParent($user_name);

            if($area_name == "All") {
                $d2 = CollectionCommon::getAllUser($user_name);
            }
            else
            {
                $d2 = CollectionCommon::getUser($user_name, $area_name);
            }
            
            $details = [];

            foreach($d2 as $d)
            {
                $user_name1 = strtoupper($d[0]);
    
                $de_tot = 0;
                $ce_tot = 0;
                $pe_tot = 0;
    
                // debit (taken)
                $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name1])
                            ->where('pay_type', '=', 'C')
                            ->where('pay_status', '=', '1')
                            ->sum('pay_amount'); 
    
                // credit (paid)
                $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name1])
                            ->where('pay_type', '=', 'D')
                            ->where('pay_status', '=', '1')
                            ->sum('pay_amount'); 
    
                // round two digits fraction
                $de_tot = round($de_tot, 2);
                $ce_tot = round($ce_tot, 2);
    
                $pe_tot = floatval($de_tot) - floatval($ce_tot);
    
                $de_tot = $this->convertNumber($de_tot);
                $ce_tot = $this->convertNumber($ce_tot);
                $pe_tot = $this->convertNumber($pe_tot);
                
                if($pe_tot != "0.00")
                {
                    array_push($details, ['user_name' => $user_name1, 'per_name' => $d[1], 'user_mobile' => $d[2], 
                                        'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot]);
                }
                
            }

            return view('android.ds_agent_collection_pending_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'c_details' => $details, 'area_name' => $area_name]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

    }

    public function getParent($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $parent = "admin";

        $d1 = $uacc_1->select('parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $parent = $d1[0]->parent_name;
        }
        
        return $parent;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
