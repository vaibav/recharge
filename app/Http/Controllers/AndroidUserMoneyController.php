<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\GetMobileCommon;
use App\Libraries\BankRemitter;
use App\Libraries\BankTransfer;
use App\Libraries\BankMobileCommon;

use App\Models\UserBankMobileStatusDetails;


class AndroidUserMoneyController extends Controller
{
    //
    public function index(Request $request)
	{
       
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($cx, $ms) = $this->screen_status($user_name);

            if($cx == 0) {
                //Login
                return view('android.recharge_money_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
            }
            else if($cx == 1) {
                //Money Transfer
                return redirect("mobile_user_money_tr_1/".$ms."/".$auth_token);
            }
            else if($cx == 2) {
                //Add Benificiary
                return redirect("mobile_user_money_ab_1/".$ms."/".$auth_token);
            }
            else if($cx == 3) {
                //delete Benificiary
                return redirect("mobile_user_money_db_1/".$ms."/".$auth_token);
            }

           
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function logout(Request $request)
	{
       
        $auth_token = trim($request->auth_token);
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $cx = BankMobileCommon::add_status($user_name, "", "LOGIN");
            return redirect('mobile_user_money_1?auth_token='.$auth_token);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function user_check(Request $request)
	{
       
        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        //echo $auth_token."---".$msisdn."<br>";

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result, $remitter, $beneficiary, $isVerified) =BankMobileCommon::user_validation($user_name, $msisdn);

            //echo $z."<br>";
            //echo $isVerified."<br>";
            if($z == 0)
            {
                if($isVerified == "1") {
                     //list($data, $beni) = BankMobileCommon::fetchCustomerDetails($remitter);

                     $cx = BankMobileCommon::add_status($user_name, $msisdn, "MONEY");

                    

                    return view('android.recharge_money_tr', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                            "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 'data' => $remitter, 'beni' => $beneficiary]);
                }
               
                

                
            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            $op = "User is not Retailer";
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
            $z = $z1;
        }

        //echo $op."--".$z;

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }

    public function dashboard($msisdn, $auth_token, Request $request)
	{
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            return view('android.recharge_money_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn]);
        }
        else if($z1 == 1)
        {
            $op = "User is not Retailer";
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
            $z = $z1;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }


    public function screen_status($user_name)
	{
        $umob_1 = new UserBankMobileStatusDetails;

        $d1 = $umob_1->where('user_name', '=', $user_name)->get();

        $z = 0;
        $ms = "";

        if($d1->count() > 0) {
            // Already login
            $st = $d1[0]->screen_type;
            $ms = $d1[0]->msisdn;
            if($st == "LOGIN") 
            {
                $z = 0;
            }
            else if($st == "MONEY")
            {
                $z = 1;
            }
            else if($st == "BENADD")
            {
                $z = 2;
            }
            else if($st == "BENDEL")
            {
                $z = 3;
            }

        }
        else {
            // Fresh Login
            $umob_1->trans_id = rand(10000,99999);
            $umob_1->user_name = $user_name;
            $umob_1->msisdn = $ms;
            $umob_1->screen_type = "LOGIN";

            $umob_1->save();
        }

        return array($z, $ms);
    }

    
   
}
