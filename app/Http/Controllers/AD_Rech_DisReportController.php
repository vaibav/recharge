<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportSuperDistributor;

use App\Models\AllTransaction;
use App\Models\UserAccountDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

use DB;
use PDF;
use EXCEL;

class AD_Rech_DisReportController extends Controller
{
    //
    public function index(Request $request)
    {
        $user_1 = new UserAccountDetails;
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $data1 = $user_1->with(['personal'])->select('user_code','user_name')->where('user_type', '=', 'DISTRIBUTOR')
                                                           ->orWhere('user_type', '=', 'SUPER DISTRIBUTOR')->orderBy('user_name')->get();      
        $d1 = $net_2->select('net_code','net_name')->get();
        
        $ob = GetCommon::getUserDetails($request);

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        $bil_code = "0";
        $mny_code = "0";

        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();
        foreach($d1 as $d)
        {
            if($d->net_type_name == "PREPAID") {
                $pre_code = $d->net_type_code;
            }
            else if($d->net_type_name == "POSTPAID") {
                $pos_code = $d->net_type_code;
            }
            else if($d->net_type_name == "DTH") {
                $dth_code = $d->net_type_code;
            }
            else if($d->net_type_name == "BILL PAYMENT") {
                $bil_code = $d->net_type_code;
            }
            else if($d->net_type_name == "MONEY TRANSFER") {
                $mny_code = $d->net_type_code;
            }
            
        }

        $code = [];
        $code[0] = $pre_code;
        $code[1] = $pos_code;
        $code[2] = $dth_code;

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', '1')
                            ->where(function($query) use ($code){
                                return $query
                                ->where('net_type_code', '=', $code[0])
                                ->orWhere('net_type_code', '=', $code[1])
                                ->orWhere('net_type_code', '=', $code[2]);
                            })->get();

        $ar_web = [];
        foreach($d2 as $d)
        {
            array_push($ar_web, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_web1 = json_encode($ar_web);

        $d3 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $bil_code], ['net_status', '=', '1']])->get();

        $ar_bill = [];
        foreach($d3 as $d)
        {
            array_push($ar_bill, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_bill1 = json_encode($ar_bill);

        //-------------------------------------------

        $d4 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $mny_code], ['net_status', '=', '1']])->get();

        $ar_money = [];
        foreach($d4 as $d)
        {
            array_push($ar_money, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_money1 = json_encode($ar_money);

        return view('admin.ad_rech_dis_report_1', ['user' => $ob, 'user1' => $data1, 'web' => $ar_web1, 'bill' => $ar_bill1, 'money' => $ar_money1]);
        
    }
   
   public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $u_name = trim($request->user_name);
        $rech_type = trim($request->rech_type);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
        $u_type = "-";

        $c1 = UserAccountDetails::select('user_type')->where('user_name', $u_name)->get();  

        if($c1->count() > 0) {
            $u_type = $c1[0]->user_type;
        }
        
        $dc4 = RechargeReportSuperDistributor::getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, "-", $u_status, $u_mobile, $u_amount, $u_net_code_1, $u_type);

       

        $d2 = $net_2->select('net_code','net_name')->get();

        //Total Calculation-------------------------------------------------------
        //recharge
        $wsua_tot = 0;
        $wsut_tot = 0;
        $wfua_tot = 0;
        $wfut_tot = 0;
        $wrea_tot = 0;
        $wret_tot = 0;
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
        //money
        $msua_tot = 0;
        $msut_tot = 0;
        $mfua_tot = 0;
        $mfut_tot = 0;
        $mrea_tot = 0;
        $mret_tot = 0;

        $dc7 = $dc4;

     

        $q = AllTransaction::whereBetween('rech_date', [$f_date, $t_date]);

        $wrea_tot = $q->sum('rech_amount');
        $wrea_tot = round($wrea_tot, 2);
        $wret_tot = $q->sum('ret_total');
        $wret_tot = round($wret_tot, 2);

        $wsua_tot = $q->where('rech_status', 'SUCCESS')->sum('rech_amount');
        $wsua_tot = round($wsua_tot, 2);
        $wsut_tot = $q->where('rech_status', 'SUCCESS')->sum('ret_total');
        $wsut_tot = round($wsut_tot, 2);

        $wfua_tot = floatval($wrea_tot) - floatval($wsua_tot);
        $wfut_tot = floatval($wret_tot) - floatval($wsut_tot);
       

        $total = ['wsua_tot' => $wsua_tot, 'wsut_tot' => $wsut_tot, 'wfua_tot' => $wfua_tot, 'wfut_tot' => $wfut_tot, 
                    'wrea_tot' => $wrea_tot, 'wret_tot' => $wret_tot,
                    'esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot,
                    'msua_tot' => $msua_tot, 'msut_tot' => $msut_tot, 'mfua_tot' => $mfua_tot, 'mfut_tot' => $mfut_tot, 
                    'mrea_tot' => $mrea_tot, 'mret_tot' => $mret_tot];

        // Pagination-------------------------------------------------------------
       /* $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }
        

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);*/

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $dc4->setPath($cus_url);
        
        return view('admin.ad_rech_dis_report_2', ['user' => $ob, 'recharge' => $dc4, 'd2' => $d2, 'u_name' => $u_name, 'u_type' => $u_type, 'total' => $total]);

    }
  
}
