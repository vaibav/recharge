<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\UserAccountDetails;

use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeNewParentDetails;

use App\Models\BackupRechargeParentDetails;
use App\Models\UserOldAccountDetails;

class AD_Backup_Dis_Controller extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = BackupRechargeParentDetails::select('rech_date')->orderby('id', 'desc')->limit(1)->get();

        if($d1->count() > 0) {
            $rech_date = $d1[0]->rech_date;
        }

        return view('admin.ad_backup_distributor', ['user' => $ob, 'rech_date' => $rech_date]);
        
    }

    public function store(Request $request)
	{
    
        $tran_1 = new TransactionAllParentDetails;
        $rech_1 = new UserRechargeNewParentDetails;
        $back_1 = new BackupRechargeParentDetails;
    
        $op = 0;
        $j = 0;
        $k = 0;
        $date_time = date('Y-m-d H:i:s');
        $time = "0 Seconds..";
        
        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'f_date' => 'required', 
        ],
        [
            'f_date.required' => ' From Date is required.'
             ]);

        $date_1 = trim($request->f_date);
        $date_2 = $date_1;

        $f_date = $date_1." 00:00:00";
        $t_date = $date_1." 23:59:59";

        if($f_date != "" && $t_date != "")
        {
            $start = microtime(true);

            $d1 = $rech_1->with(['backup'])
                                ->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('rech_status', 'SUCCESS')
                                ->where('rech_total', '!=', '0.00')
                                ->get(); 

            $cnt1 = $d1->count();
            $data_x = [];

            $j = 0;
            $k = 0;
            $m = 0;
            foreach($d1 as $d)
            {
                $trans_id = $d->trans_id;
                $parent_name = $d->parent_name;
                $child_name = $d->child_name;
                $user_name = $d->user_name;
                $rech_total = $d->rech_total;
                $rech_status = $d->rech_status;

                if($rech_status == "SUCCESS")
                {
                    if($rech_total != "0.00")
                    {
                        if($d->backup == null)
                        {
                            $data = [ 
                                'trans_id' => $trans_id,
                                'parent_name' => $parent_name,
                                'child_name' => $child_name,
                                'user_name' => $user_name,
                                'net_code' => $d->net_code,
                                'rech_mobile' => $d->rech_mobile,
                                'rech_amount' => $d->rech_amount,
                                'rech_total' => $rech_total,
                                'user_balance' => $d->user_balance,
                                'rech_date' => $d->rech_date,
                                'rech_status' => $rech_status,
                                'rech_type' => $d->rech_type,
                                'created_at' => $date_time,
                                'updated_at' => $date_time
                            ];
        
                            array_push($data_x, $data);
                            $j++;
                            $k++;

                        }
                        else {
                            $j++;
                            $m++;
                        }
                    }
                    
                    

                }
               
            }

            $v = $back_1->insert($data_x);

            if($v > 0)
            {
                //DELETE ENTRIES
                if($cnt1 == $j)
                {
                    $q1 = UserRechargeNewParentDetails::whereBetween('rech_date', [$f_date, $t_date])->delete();
                    $q2 = TransactionAllParentDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'WEB_RECHARGE')->delete();
                    $q3 = TransactionAllParentDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'API_RECHARGE')->delete();

                }
            }

            //DELETE ENTRIES (ALL ENTRIES ALREADY INSERTED IN BACKUP DETAILS)
            if($cnt1 == $m)
            {
                $q1 = UserRechargeNewParentDetails::whereBetween('rech_date', [$f_date, $t_date])->delete();
                $q2 = TransactionAllParentDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'WEB_RECHARGE')->delete();
                $q3 = TransactionAllParentDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'API_RECHARGE')->delete();

            }

            $time = microtime(true) - $start;

        }
        
        
        $op = $j." Entries backup successfully....<br> Recharge Entries:".$cnt1."---Backup Entries :".$j."<br> Execution Time :".$time;

        return redirect()->back()->with('msg', $op);
    }


   

}
