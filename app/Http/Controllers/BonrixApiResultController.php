<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\BonrixApiResultDetails;

class BonrixApiResultController extends Controller
{
    //
    public function index(Request $request)
	{
		      
        $bon_1 = new BonrixApiResultDetails;
        //$data = $bon_1->where('result_status', '=', "1")->get();
        $data = $bon_1->orderBy('id', 'desc')->limit(15)->get();
        $op = "";

        $ob = $this->getUserDetails($request);

        return view('admin.bonrixapiresult1', ['data' => $data, 'user' => $ob]);
		
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
		      
        $bon_1 = new BonrixApiResultDetails;
        $ob = $this->getUserDetails($request);
        $str = "";

        $d1 = $bon_1->where('result_status', '=', "1")->get();

        foreach($d1 as $d)
        {
            $res_url = $this->convert_text($d->result_url);
            echo "<iframe src=".$res_url." id='ab' style='display:none;'></iframe>";
            $bon_1->where('trans_id', '=', $d->trans_id)->update(['result_status' => "2"]);
        }
        $op = "";

        $data = $bon_1->orderBy('id', 'desc')->limit(15)->get();


        $j = 1;
        foreach($data as $f)
        {
            $str = $str. "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
            $str = $str. "<td  style='font-size:12px;padding:7px 8px;width:15%;'>".$f->trans_id."</td>";
            $str = $str. "<td  style='font-size:12px;padding:7px 8px;width:35%;'>".$f->result_url."</td>";
            if( $f->result_status == "1")
                $str = $str. "<td  style='font-size:12px;padding:7px 8px;'>PENDING</td>";
            else
                $str = $str. "<td  style='font-size:12px;padding:7px 8px;'>SUCCESS</td>";
            $str = $str."</tr>";
            $j++;
            
        }

        echo $str;
		
    }

    public function convert_text($text) {

        $t = $text;
        
        $specChars = array(
            '!' => '%21',    '"' => '%22',
            '#' => '%23',    '$' => '%24',        
            ')' => '%29',    '(' => '%28',
            '<' => '%3C',    '>' => '%3E',
            '@' => '%40',    '[' => '%5B',
            ']' => '%5D',    '^' => '%5E',
            '`' => '%60',    '{' => '%7B',
            '}' => '%7D',    '~' => '%7E',
            ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }
}
