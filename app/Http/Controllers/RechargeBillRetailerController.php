<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use stdClass;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Libraries\RechargeReportRetailer;

use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllDetails;
use App\Models\ApiProviderDetails;
use App\Models\UserRechargeBillDetails;

use PDF;
use EXCEL;


class RechargeBillRetailerController extends Controller
{
    //
    public function index(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.rechargereport_bill', ['user' => $ob]);
        
    }

    public function index_api(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        return view('user.rechargereport_bill_apipartner', ['user' => $ob]);
        
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
       
        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($ob->user);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
         

        $dc4 = RechargeReportRetailer::getRechargeReportDetails_bill($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, "", "");
        

        $d2 = $net_2->select('net_code','net_name')->get();

        //-----------------------------------------------------
        //Total Calculation-------------------------------------------------------
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
      
        foreach($dc4 as $d)
        {
            if($d->trans_type == "BILL_PAYMENT")
            {
                
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;
                $r_amt = 0;
                

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $r_amt = $d->billpayment[0]->con_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $r_amt = $d->billpayment[1]->con_amount;
                }
                
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $efua_tot = floatval($efua_tot) + floatval($r_amt);
                    $efut_tot = floatval($efut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $esua_tot = floatval($esua_tot) + floatval($r_amt);
                    $esut_tot = floatval($esut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                
            }                                                    
        }

        $total = ['esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot];
       
       
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        $rs = [];
                               
        return view('retailer.rechargereport_bill_view', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2, 'total' => $total]); 

    }

    public function viewdate_api(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($ob->user);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
                               
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        
        
        $d2 = $net_2->select('net_code','net_name')->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        return view('user.rechargereport_bill_view', ['user' => $ob, 'recharge' => $dc4, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'api' => $dx3, 'total' => '0']); 

    }


    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        
        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($ob->user);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        
        
        $dc4 = RechargeReportRetailer::getRechargeReportDetails_bill($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, "", "");
        

        $d2 = $net_2->select('net_code','net_name')->get();
       
        
        

        $headings = ['NO', 'EB CONN NO', 'NETWORK', 'EB DUE AMT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
        $str = "";
        $content = [];

        $k = 0;
        foreach($dc4 as $d)
        {
           
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "PENDING";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
            
                if($d->trans_option == 1)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount, 
                        $d->billpayment[0]->con_net_per."-".$d->billpayment[0]->con_net_per_amt."-".$d->billpayment[0]->con_net_surp,
                        $d->billpayment[0]->con_total, $d->trans_id, $d->billpayment[0]->reply_opr_id,
                        $d->billpayment[0]->created_at, $d->billpayment[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount,'',
                        $d->billpayment[0]->con_total, $d->trans_id, '','','',
                        $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
               
            } 
                                               
                  
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $u_name."_Recharge_bill_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }

    public function viewdate_excel_api(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($ob->user);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
                              
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        

        $d2 = $net_2->select('net_code','net_name')->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        

        $headings = ['NO', 'EB CONN NO', 'NETWORK', 'EB DUE AMT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
        $str = "";
        $content = [];

        $k = 0;
        foreach($dc4 as $d)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($d->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $api_name = "";
            foreach($dx3 as $r)
            {
                if($d->api_code == $r->api_code)
                    $api_name = $r->api_name;
            }

            $rech_status = "";
            $status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;

            $rech_option = $d->con_option;
            $rech_status = $d->con_status;
            $r_tot = $d->con_total;
            $u_bal = $d->user_balance;

            

            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
                //$o_bal = floatval($u_bal) - floatval($r_tot);
                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                //$u_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            

            
            if($rech_option == 2 && $rech_status == "FAILURE")
            {
                $content[$k++] = [$j, $d->con_acc_no, $net_name, $d->con_amount, 
                            '', $d->con_total, $d->trans_id, 
                             $d->reply_opr_id, '', '', $status, 
                            $o_bal, $u_bal];
                
            }
            else  
            {

                $content[$k++] = [$j, $d->con_acc_no, $net_name, $d->con_amount, 
                                    $d->con_net_per."-".$d->con_net_per_amt."-".$d->con_net_surp, $d->con_total, $d->trans_id, 
                                    $d->reply_opr_id, $d->created_at, $d->reply_date, $status, 
                                    $o_bal, $u_bal];
                
            }
            
                                                    
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $u_name."_Recharge_bill_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }


}
