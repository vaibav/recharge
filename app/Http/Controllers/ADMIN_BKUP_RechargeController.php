<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserAccountDetails;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserRechargeNewParentDetails;

use App\Models\BackupRechargeDetails;
use App\Models\UserOldAccountDetails;

class ADMIN_BKUP_RechargeController extends Controller
{
    //
    public function store(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $back_1 = new BackupRechargeDetails;
        
        // Post Data
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $date_time = date('Y-m-d H:i:s');

        if($f_date != "" && $t_date != "")
        {
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                                ->whereBetween('created_at', [$f_date, $t_date])
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'WEB_RECHARGE')
                                    ->orWhere('trans_type', '=', 'API_RECHARGE');
                                })
                                ->get(); 

            $j = 1;
            foreach($d1 as $d)
            {
                if($d->trans_option == 1)
                {
                    $trans_id = $d->trans_id;
                    $user_name = $d->newrecharge1[0]->user_name;
                    $net_code = $d->newrecharge1[0]->net_code;
                    $rech_mobile = $d->newrecharge1[0]->rech_mobile;
                    $rech_amount = $d->newrecharge1[0]->rech_amount;
                    $rech_total = $d->newrecharge1[0]->rech_total;
                    $u_bal = $d->newrecharge1[0]->user_balance;
                    $rech_date = $d->newrecharge1[0]->rech_date;
                    $rech_status = $d->newrecharge1[0]->rech_status;
                    $reply_id = "NA";
                    $api_code = "0";
                    if($d->newrecharge2 != null)
                    {
                        $reply_id = $d->newrecharge2->reply_opr_id;
                        $api_code = $d->newrecharge2->api_code;
                    }
                   
                    
                    if($rech_status == "SUCCESS")
                    {
                        $cnt = $back_1->where('trans_id', '=', $trans_id)->where('user_name', '=', $user_name)->count();

                        if($cnt == 0)
                        {
                            $data = [ 
                                'trans_id' => $trans_id,
                                'user_name' => $user_name,
                                'net_code' => $net_code,
                                'rech_mobile' => $rech_mobile,
                                'rech_amount' => $rech_amount,
                                'rech_total' => $rech_total,
                                'user_balance' => $u_bal,
                                'rech_date' => $rech_date,
                                'rech_status' => $rech_status,
                                'reply_id' => $reply_id,
                                'api_code' => $api_code,
                                'created_at' => $date_time,
                                'updated_at' => $date_time
                            ];
        
                            $back_1->insert($data);

                            echo $j."----".$trans_id."----".$user_name."----".$net_code."----".$rech_mobile."---".$rech_amount;
                            echo $rech_total."----".$u_bal."----".$rech_date."----".$rech_status."---".$reply_id."---".$api_code."<br>";
                            $j++;
                        }


                      
                    }
                    
                    
                }
             
               
            }
        
        }

       
    }

    public function delete(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $back_1 = new BackupRechargeDetails;
        $rech_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeNewStatusDetails;
        
        // Post Data
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $date_time = date('Y-m-d H:i:s');

        if($f_date != "" && $t_date != "")
        {
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                                ->whereBetween('created_at', [$f_date, $t_date])
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'WEB_RECHARGE')
                                    ->orWhere('trans_type', '=', 'API_RECHARGE');
                                })
                                ->get(); 

            $j = 1;
            foreach($d1 as $d)
            {
                if($d->trans_option == 1)
                {
                    $trans_id = $d->trans_id;
                    $user_name = $d->newrecharge1[0]->user_name;
                    
                    $rech_status = $d->newrecharge1[0]->rech_status;
                    $rech_option = $d->newrecharge1[0]->rech_option;
                   
                    
                    if($rech_status == "SUCCESS")
                    {
                        $cnt = $back_1->where('trans_id', '=', $trans_id)->where('user_name', '=', $user_name)->count();

                        if($cnt > 0)
                        {
                            $p = $tran_1->where('trans_id', '=', $trans_id)
                                        ->where('user_name', '=', $user_name)
                                        ->where('trans_option', '=', '1')->delete();

                            $q = $rech_1->where('trans_id', '=', $trans_id)
                                        ->where('user_name', '=', $user_name)->delete();

                            $r = $rech_2->where('trans_id', '=', $trans_id)->delete();
                            
                            if($p > 0 && $q > 0 && $r > 0)
                                $j++;
                        }

                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $p = $tran_1->where('trans_id', '=', $trans_id)
                                    ->where('user_name', '=', $user_name)
                                    ->where('trans_option', '=', '1')->delete();

                        $q = $rech_1->where('trans_id', '=', $trans_id)
                                    ->where('user_name', '=', $user_name)
                                    ->where('rech_status', '=', $rech_status)
                                    ->where('rech_option', '=', $rech_option)->delete();

                        $r = $rech_2->where('trans_id', '=', $trans_id)->delete();
                        
                        if($p > 0 && $q > 0)
                            $j++;

                    }
                    
                    
                }
                if($d->trans_option == 2)
                {
                    $trans_id = $d->trans_id;
                    $user_name = $d->user_name;
                    
                    $rech_status = $d->newrecharge1[1]->rech_status;
                    $rech_option = $d->newrecharge1[1]->rech_option;
                   
                    
                    if($rech_status == "FAILURE" && $rech_option == "2")
                    {
                        $p = $tran_1->where('trans_id', '=', $trans_id)
                                    ->where('user_name', '=', $user_name)
                                    ->where('trans_option', '=', '2')->delete();

                        $q = $rech_1->where('trans_id', '=', $trans_id)
                                    ->where('user_name', '=', $user_name)
                                    ->where('rech_status', '=', $rech_status)
                                    ->where('rech_option', '=', $rech_option)->delete();

                        $r = $rech_2->where('trans_id', '=', $trans_id)->delete();
                        
                        if($p > 0 && $q > 0)
                            $j++;

                    }
                    
                    
                }
             
               
            }
        
        }

       
    }

    public function add_old(Request $request)
	{
        $u_old = new UserOldAccountDetails;
        
        // Post Data
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $date_time = date('Y-m-d H:i:s');

        $dx = UserAccountDetails::select('user_name', 'user_type')
                ->where('user_type', '=', 'RETAILER')
                ->orWhere('user_type', '=', 'API PARTNER')
                ->orderBy('id', 'asc')->get(); 

        $j = 1;
        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>SUCCESS RECHARGE</th></tr>";
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $ce_tot = 0;
           
            $ux_name =strtoupper($ux_name);

            // User Recharge Details
            $ce_tot = UserRechargeNewDetails::whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$ux_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 

            $ce_tot = round($ce_tot, 2);

            if($ce_tot != 0)
            {
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $j . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_name . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_type . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($ce_tot, 2, ".", "") . "</td></tr>";
            
                $j++;

                $cnt = $u_old->where('user_name', '=', $ux_name)->where('from_date', '=', $f_date)->where('too_date', '=', $t_date)->count();

                if($cnt == 0)
                {
                    $data = [ 
                        'trans_id' => rand(10000,99999),
                        'user_name' => $ux_name,
                        'from_date' => $f_date,
                        'too_date' => $t_date,
                        'rech_type' => "RECHARGE",
                        'user_amount' => $ce_tot,
                        'created_at' => $date_time,
                        'updated_at' => $date_time
                    ];

                    $u_old->insert($data);
                }
            
            }
            
        }

        echo $str;
                

        
       
    }
}
