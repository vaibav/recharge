<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;
use App\Models\UserPaymentDetails;

class ADMIN_StockReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $user_1->with(['personal'])->where('user_name', '!=', $ob->user)->orderBy('user_name', 'asc')->get();

        return view('admin.user_stock_1', ['user' => $ob, 'users' => $d1]);
        
    }

    public function viewdate(Request $request)
    {
        $user_1 = new UserAccountDetails;
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $user_name = trim($request->user_name);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
        $data = [];
        $o_balance = 0;
        $c_sales_re = 0;
        $c_sales_eb = 0;
        $c_sales_mo = 0;
        $c_sales_mv = 0;
        $old_re = 0;

        $c1 = $user_1->select('user_type')->where('user_name', '=', $user_name)->get();

        if($c1->count() > 0)
        {
            $user_type = $c1[0]->user_type;

            if($user_type == "SUPER DISTRIBUTOR" || $user_type == "DISTRIBUTOR")
            {
                // Opening Balanace
                $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $user_name);
                $o_balance = Stock::stockCustomOpeningBalance($user_name);
                $o_balance = floatval($o_balance) + floatval($o_balance1);
                
                $o_sales_re = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00",$f_date, "RECHARGE");
                $o_sales_eb = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00", $f_date, "BILL_PAYMENT");
                $o_sales_mo = Stock::getDistributorSales($user_name, "2018-10-01 00:00:00",$f_date, "BANK_TRANSFER");
                $o_sales_mv = 0;
                $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));
        
                // current sales...
                $c_sales_re = Stock::getDistributorSales($user_name, $f_date, $t_date, "RECHARGE");
                $c_sales_eb = Stock::getDistributorSales($user_name, $f_date, $t_date, "BILL_PAYMENT");
                $c_sales_mo = Stock::getDistributorSales($user_name, $f_date, $t_date, "BANK_TRANSFER");
                $c_sales_mv = 0;
        
                
               
            }
            else if($user_type == "RETAILER" || $user_type == "API PARTNER")
            {
                // Opening Balanace
                $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $user_name);
                $o_balance = Stock::stockCustomOpeningBalance($user_name);
                $o_balance = floatval($o_balance) + floatval($o_balance1);
                
                $o_sales_re = Stock::getRetailerRechargeOpeningSales($user_name, "2018-10-01 00:00:00",$f_date);
                $o_sales_eb = Stock::getRetailerBill($user_name, "2018-10-01 00:00:00", $f_date);
                $o_sales_mo = Stock::getMoneyOpeningSales($user_name, "2018-10-01 00:00:00",$f_date);
                $o_sales_mv = Stock::getMoneyVerifyOpeningSales($user_name, "2018-10-01 00:00:00",$f_date);
                $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));
        
                $old_re = Stock::getRetailerOldRecharge($user_name);

                // current sales...
                $c_sales_re = Stock::getRetailerRechargeOpeningSales($user_name, $f_date, $t_date);
                $c_sales_eb = Stock::getRetailerBill($user_name, $f_date, $t_date);
                $c_sales_mo = Stock::getMoneyOpeningSales($user_name, $f_date, $t_date);
                $c_sales_mv = Stock::getMoneyVerifyOpeningSales($user_name, $f_date, $t_date);
        
               
            }
        }

        $data = Stock::stockData($user_name, $f_date, $t_date);


        return view('admin.user_stock_2', ['pay1' => $data, 'obalance' => $o_balance, 'bill' => $c_sales_eb, 'ce_tot' => $c_sales_re, 
                                            'money' => $c_sales_mo, 'mverify' => $c_sales_mv, 
                                            'old_re' => $old_re, 'user_name_p' => $user_name, 'user' => $ob]); 

        
    }
}
