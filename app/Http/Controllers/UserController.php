<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;

use App\Models\NetworkPackageDetails;

class UserController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $net_o1 = new NetworkDetails;
        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        $d1 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.user', ['network' => $data, 'package' => $d1, 'user' => $ob]);
        
		
    }

   

    public function store(Request $request)
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        // Declaration
        $file_name1 = "";
        $file_name2 = "";


        // Validation
        $this->validate($request, [
            'user_per_name' => 'required',
            'user_city' => 'required',
            'user_mobile' => 'required',
            'user_name' => 'required|without_spaces',
            'user_pwd' => 'required'
        ],
        [
            'user_per_name.required' => ' The Name is required.',
            'user_city.required' => ' The user city is required.',
            'user_mobile.required' => ' The user mobile is required.',
            'user_name.required' => ' The user name is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.',
            'user_pwd.required' => ' The password is required.'
            ]);

        $user_code = rand(1000,9999);
        $user_per_name = trim($request->user_per_name);
        $user_city = trim($request->user_city);
        $user_state = trim($request->user_state);
        $user_phone = trim($request->user_phone);
        $user_mobile = trim($request->user_mobile);
        $user_mail = trim($request->user_mail);
        $user_kyc = trim($request->user_kyc);
        $pack_id = trim($request->pack_id);

        $user_name = trim($request->user_name);
        $user_pwd = trim($request->user_pwd);
        $user_acc_type = trim($request->user_acc_type);
        $user_setup_fee = trim($request->user_setup_fee);
        
        //recharge type
        $user_rech = $request->user_rec_mode;
        $user_rec_mode = "";
        foreach ($user_rech as $c)
        { 
            $user_rec_mode = $user_rec_mode . $c. "@";
        }
        
        $user_api_url_1 = trim($request->user_api_url_1);
        $user_api_url_2 = trim($request->user_api_url_2);

        // Temporary File Name Storage
        $file_name1 = $user_code."_PHOTO.jpg";
        $file_name2 = $user_code."_KYC.jpg";

        if ($request->hasFile('user_photo')) {
            //
            $photo = $request->file('user_photo');
            $extension = $photo->getClientOriginalExtension();
            $file_name1 = $user_code."_PHOTO.".$extension;
            $photo->move(public_path("/uploadphoto"), $file_name1);
        }
       

        if ($request->hasFile('user_kyc_proof')) {
            //
            $kyc_proof = $request->file('user_kyc_proof');
            $extension = $kyc_proof->getClientOriginalExtension();
            $file_name2 = $user_code."_KYC.".$extension;
            $kyc_proof->move(public_path("/uploadkyc"), $file_name2);
        }
       
        
        // Insert Data
        $cnt = $user_1->where('user_code', '=', $user_code)->count();
        if($cnt == 0)
        {
            $cnt1 = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt1 == 0)
            {
                // Personal Details....
                $user_1->user_code = $user_code;
                $user_1->user_name = $user_name;
                $user_1->user_per_name = $user_per_name;
                $user_1->user_city = $user_city;
                $user_1->user_state = $user_state;
                $user_1->user_phone = $user_phone;
                $user_1->user_mobile = $user_mobile;
                $user_1->user_mail = $user_mail;
                $user_1->user_kyc = $user_kyc;
                $user_1->user_photo = $file_name1;
                $user_1->user_kyc_proof = $file_name2;

                $user_1->save();

                // Account Details.....
                $user_2->user_code = $user_code;
                $user_2->user_name = $user_name;
                $user_2->user_pwd = $user_pwd;
                $user_2->user_type = $user_acc_type;
                $user_2->user_setup_fee = $user_setup_fee;
                $user_2->parent_type = "ADMIN";
                $user_2->parent_code = "1";
                $user_2->parent_name = "admin";
                $user_2->pack_id = $pack_id;
                $user_2->user_rec_mode = $user_rec_mode;
                $user_2->user_api_url_1 = $user_api_url_1;
                $user_2->user_api_url_2 = $user_api_url_2;
                $user_2->user_status = 1;

                $user_2->save();

                // Account Balance Details.....
                $user_5->user_code = $user_code;
                $user_5->user_name = $user_name;
                $user_5->user_balance = "0.00";
                
                $user_5->save();

                 
                // User Network Details.... 
                /*$arr1 = [];
                $arr2 = [];
                $date_time = date("Y-m-d H:i:s");
                $j = 1;
                foreach($data as $f)
                {
                    $n_code = trim($request['net_code_'.$j]);
                    $n_name = trim($request['net_name_'.$j]);
                    $n_perc = trim($request['net_per_'.$j]);
                    $n_surp = trim($request['net_surp_'.$j]);

                    if($n_perc != "")
                    {
                        $arr1 = array('user_code' => $user_code, 'user_name' => $user_name, 'net_code' => $n_code,
                        'user_net_per' => $n_perc, 'user_net_surp' => $n_surp, 
                        'created_at' => $date_time, 'updated_at' => $date_time);
                        array_push($arr2, $arr1);
                        
                    }

                    $j++;

                }*/

                

                // User Chain Details
                $user_level = "1";
                $user_chain = "1-".$user_code;

                $user_4->user_code = $user_code;
                $user_4->user_name = $user_name;
                $user_4->parent_code = "1";
                $user_4->parent_name = "admin";
                $user_4->user_level = $user_level;
                $user_4->user_chain = $user_chain;
                
                $user_4->save();

                $op = "User Entry is created successfully....";
              
            }
            else
            {
                $op = "User Name Already Exists...";
            }
        }
        else
        {
            $op = "User Code Already Exists....";
        }
        
        
         return redirect()->back()->with('msg', $op);
        
    }

    public function update(Request $request)
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_4 = new UserChainDetails;

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        // Declaration
        $file_name1 = "";
        $file_name2 = "";

       
        $op = "NONE";

        // Validation
        $this->validate($request, [
            'user_per_name' => 'required',
            'user_city' => 'required',
            'user_phone' => 'required',
            'user_mail' => 'required',
            'user_state' => 'required',
            'user_mobile' => 'required',
           
        ],
        [
            'user_per_name.required' => ' The Name is required.',
            'user_city.required' => ' The user city is required.',
            'user_mobile.required' => ' The user mobile is required.',
            'user_phone.required' => ' The user Landline is required.',
            'user_mail.required' => ' The user mail id is required.',
            'user_state.required' => ' The user state is required.',
            
            ]);

        $user_code = trim($request->user_code);
        $user_per_name = trim($request->user_per_name);
        $user_city = trim($request->user_city);
        $user_state = trim($request->user_state);
        $user_phone = trim($request->user_phone);
        $user_mobile = trim($request->user_mobile);
        $user_mail = trim($request->user_mail);
        $user_kyc_proof = trim($request->user_kyc);
        //$user_kyc = trim($request->user_kyc);

        $pack_id = trim($request->pack_id);
        $user_rec_mode = trim($request->user_rech_mode);
        $user_api_url_1 = trim($request->user_api_url_1);
        $user_api_url_2 = trim($request->user_api_url_2);

        
       
        
        // Insert Data
        $cnt = $user_1->where('user_code', '=', $user_code)->count();
        if($cnt > 0)
        {
            $a = $user_1->where('user_code', '=', $user_code)
                    ->update(['user_per_name' => $user_per_name, 
                                'user_city' => $user_city,
                                'user_state' => $user_state,
                                'user_phone' => $user_phone,
                                'user_mobile' => $user_mobile,
                                'user_mail' => $user_mail,
                                'user_kyc' => $user_kyc_proof
                                ]);
            
            $b = $user_2->where('user_code', '=', $user_code)
                        ->update(['pack_id' => $pack_id, 'user_rec_mode' => $user_rec_mode,
                                    'user_api_url_1' => $user_api_url_1,
                                    'user_api_url_2' => $user_api_url_2
                                    ]);
            
            if($a >0 && $b > 0)
                $op = "User Entry is updated successfully....";
            else
                $op = "Error! Try again later...";

           
        }
        else
        {
            $op = "User Code Already Exists....";
        }
               
        
        return redirect()->back()->with('msg', $op); 
        
    }

    public function update_network(Request $request)
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        //$user_3 = new UserNetworkDetails;
        $user_4 = new UserChainDetails;

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        $op = "NONE";
        $user_name = "NONE";
        $arr1 = [];
        $date_time = date("Y-m-d H:i:s");

        $user_code = trim($request->user_code);

        $d1 = $user_1->select('user_name')->where('user_code', '=', $user_code)->get();
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
        }

        $i = 0;
        foreach($data as $d)
        {
            $net_code = trim($request['code_'.$d->net_code]);
            $net_pers = trim($request['net_per_'.$net_code]);
            $net_surp = trim($request['net_surp_'.$net_code]);

            //echo $user_code."---".$net_code."---".$net_pers."---".$net_surp."<br>";

            $cnt = $user_3->where([['user_code', '=', $user_code], ['net_code', '=', $net_code]])->count();
            if($cnt > 0)
            {
                $a = $user_3->where([['user_code', '=', $user_code], ['net_code', '=', $net_code]])
                        ->update(['user_net_per' => $net_pers, 
                                    'user_net_surp' => $net_surp
                                    ]);
                $i++;
            }
            else
            {
                $arr1 = array('user_code' => $user_code, 'user_name' => $user_name, 'net_code' => $net_code,
                        'user_net_per' => $net_pers, 'user_net_surp' => $net_surp, 
                        'created_at' => $date_time, 'updated_at' => $date_time);
                
                $user_3->insert($arr1);
            }
        }

        if($i > 0)
            $op = "User Network Entry is updated successfully....";
        else
            $op = "Error! No Update...";
       
        //echo $op;
        

        
        
        return redirect()->back()->with('msg', $op); 
        
    }

    public function update_photo(Request $request)
	{
        
        // Declaration
        $file_name1 = "";
        $file_name2 = "";

        $op = "NONE";

        $user_code = trim($request->user_code);
       

        $a = 0;
        $b = 0;

        if ($request->hasFile('user_photo')) {
            //
            $photo = $request->file('user_photo');
            $extension = $photo->getClientOriginalExtension();
            $file_name1 = $user_code."_PHOTO.".$extension;
            $photo->move(public_path("/uploadphoto"), $file_name1);
            $a = 1;
        }
       

        if ($request->hasFile('user_kyc_proof')) {
            //
            $kyc_proof = $request->file('user_kyc_proof');
            $extension = $kyc_proof->getClientOriginalExtension();
            $file_name2 = $user_code."_KYC.".$extension;
            $kyc_proof->move(public_path("/uploadkyc"), $file_name2);
            $b = 1;
        }

        if($a >0 || $b > 0)
            $op = "User Entry is updated successfully....";
        else
            $op = "Error! No Photo is Uploaded...";
       
        return redirect()->back()->with('msg', $op); 
        
    }

    public function update_user_status($user_code, Request $request)
	{
        $user_2 = new UserAccountDetails;
        
        $op = "NONE";
        $user_name = "NONE";
        $stat = "1";
        $date_time = date("Y-m-d H:i:s");

        
        $d1 = $user_2->select('user_status')->where('user_code', '=', $user_code)->get();
        if($d1->count() > 0)
        {
            $status = $d1[0]->user_status;

            if($status == "1")
            {
                $stat = "2";
            }
            else if($status == "2")
            {
                $stat = "1";
            }
            $a = $user_2->where('user_code', '=', $user_code)->update(['user_status' => $stat]);
            $op = "Status is Updated Successfully....";
        }
        else
        {
            $op = "Error! No Update...";
        }

                
        return redirect()->back()->with('msg', $op); 
        
    }

    public function viewall(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_3 = new UserAccountBalanceDetails;

       /* $data = $user_1->with(['userAccounts','userNetwork'])
                        ->select('user_code','user_name','user_per_name','user_kyc')->get();  */
        
        $data = $user_2->where('user_status', '!=', '3')->get();

        $data1 = $user_1->all();
        $data2 = $user_3->all();

        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.user_view', ['user1' => $data, 'user2' => $data1, 'user3' => $data2, 'user' => $ob]);
    }

    public function viewone($user_code, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        

       /* $data = $user_1->with(['userAccounts','userNetwork'])
                        ->select('user_code','user_name','user_per_name','user_kyc')->get();  */
        
        $data = $user_1->with(['account'])->where('user_code', '=', $user_code)->get();

        $d1 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.user_view_one', ['user1' => $data, 'package' => $d1, 'user' => $ob]);
    }

    public function viewnetwork($user_code, Request $request)
	{
        $userName = "-";
        $packId   = "-";

        $userDetails = UserAccountDetails::select('user_name', 'pack_id')->where('user_code', $user_code)->first();

        if($userDetails){
            $userName = $userDetails->user_name;
            $packId   = $userDetails->pack_id;
        }

        $packDetails    = NetworkPackageDetails::where('pack_id', $packId)->get();
        $networkDetails = NetworkDetails::select('net_code', 'net_name')->get();

        $ob = GetCommon::getUserDetails($request);

        return view('admin.user_view_network', ['user' => $ob, 'packDetails' => $packDetails, 'networkDetails' => $networkDetails]);
    }

    public function storeAdmin()
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        // Declaration
        $file_name1 = "";
        $file_name2 = "";


        // Validation
        

        $user_code = 1;
        $user_per_name = "admin";
        $user_city = "madurai";
        $user_state = "Tamil Nadu";
        $user_phone = "9894492775";
        $user_mobile = "9894492775";
        $user_mail = "sbalamca07@gmail.com";
        $user_kyc = "NONE";

        $user_name = "admin";
        $user_pwd = "12345";
        $user_acc_type = "ADMIN";
        $user_setup_fee = "100";
        
        //recharge type
       
        $user_rec_mode = "WEB@GPRS@API@SMS";
        $user_api_url_1 = "*";
        $user_api_url_2 = "*";

       
        
        // Insert Data
        $cnt = $user_1->where('user_code', '=', $user_code)->count();
        if($cnt == 0)
        {
            $cnt1 = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt1 == 0)
            {
                // Personal Details....
                $user_1->user_code = $user_code;
                $user_1->user_name = $user_name;
                $user_1->user_per_name = $user_per_name;
                $user_1->user_city = $user_city;
                $user_1->user_state = $user_state;
                $user_1->user_phone = $user_phone;
                $user_1->user_mobile = $user_mobile;
                $user_1->user_mail = $user_mail;
                $user_1->user_kyc = $user_kyc;
                $user_1->user_photo = $file_name1;
                $user_1->user_kyc_proof = $file_name2;

                $user_1->save();

                // Account Details.....
                $user_2->user_code = $user_code;
                $user_2->user_name = $user_name;
                $user_2->user_pwd = $user_pwd;
                $user_2->user_type = $user_acc_type;
                $user_2->user_setup_fee = $user_setup_fee;
                $user_2->parent_type = "ADMIN";
                $user_2->parent_code = "1";
                $user_2->parent_name = "admin";
                $user_2->user_rec_mode = $user_rec_mode;
                $user_2->user_api_url_1 = $user_api_url_1;
                $user_2->user_api_url_2 = $user_api_url_2;
                $user_2->user_status = 1;

                $user_2->save();

               

                $op = "User Entry is created successfully....";
              
            }
            else
            {
                $op = "User Name Already Exists...";
            }
        }
        else
        {
            $op = "User Code Already Exists....";
        }
        
       echo $op;
    }

    

    public function getresult(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.result', ['user' => $ob]);
		
    }

    public function delete($user_code, Request $request)
	{
        $user_2 = new UserAccountDetails;

        $op = "NONE";
        $user_name = "NONE";
        $stat = "3";
        $date_time = date("Y-m-d H:i:s");

    
        $d1 = $user_2->select('user_name', 'user_status')->where('user_code', '=', $user_code)->get();
        if($d1->count() > 0)
        {
            if($d1[0]->user_name != "admin")
            {
                $a = $user_2->where('user_code', '=', $user_code)->update(['user_status' => $stat]);
                $op = "User Account is Deleted Successfully....";
            }
            else
            {
                $op = "Admin Account is not deleted...";
            }
        }
        else
        {
            $op = "Error! No Delete...";
        }

                
        return redirect()->back()->with('msg', $op); 
        
    }

    public function checkUserName($user_name, Request $request)
    {
        $user_2 = new UserAccountDetails;
        $cnt = 0;

        $cnt = $user_2->where('user_name', '=', $user_name)->count();

        echo $cnt;
    }

    public function checkUserMobile($user_mobile, Request $request)
    {
        $user_1 = new UserPersonalDetails;
        $cnt = 0;

        $cnt = $user_1->where('user_mobile', '=', $user_mobile)->count();

        echo $cnt;
    }
}
