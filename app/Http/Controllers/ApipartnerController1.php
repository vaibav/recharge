<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;
use App\Models\NetworkLineApipartnerDetails;

use App\Models\ApiProviderDetails;
use App\Models\ApiProviderParameterDetails;
use App\Models\ApiProviderNetworkDetails;

use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;

use App\Models\UserRechargeApipartnerDetails;
use App\Models\UserRechargeRequestApipartnerDetails;
use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

class ApipartnerController extends Controller
{
    //
    public function store(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;

        $d1 = $request->all();
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "pin")
            {
                $user_pwd = $value;
            }
            else if($key == "opr")
            {
                $operator = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
            else if($key == "amt")
            {
                $user_amount = $value;
            }
            else if($key == "trid")
            {
                $user_trid = $value;
            }
        }

        $ob = $this->getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;

        list($net_code, $net_type_code) = $this->getNetworkCode($operator);

        $trans_id = rand(10000000,99999999);
        $date_time = date("Y-m-d H:i:s");

       /* echo "User Code :".$user_code."<BR>";
        echo "User Name :".$user_name."<BR>";
        echo "User PWD :".$user_pwd."<BR>";
        echo "User Balance :".$user_ubal."<BR>";
        echo "Operator :".$operator."<BR>";
        echo "User Mobile :".$user_mobile."<BR>";
        echo "User Amount :".$user_amount."<BR>";
        echo "User Trid :".$user_trid."<BR>";
        echo "Network Code :".$net_code."<BR>";
        echo "Network Type Code :".$net_type_code."<BR>";
        echo "Transaction Id :".$trans_id."<BR>";
        echo "date time :".$date_time."<BR>";*/

        /*
        |--------------------------------------------------------------------------
        | Recharge Process
        |--------------------------------------------------------------------------
        |   1. Check Valid User and Valid API Partner and User Account Status
        |   2. Check User Balance  
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        // 1.Check User Balance
        if(floatval($user_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }
        
        // Get Network Line
        list($api_code, $v) = $this->getNetworkLine($net_code, $user_amount, $user_name);
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }

        // Check Username status
        if($ob->status == 3)
        {
            $z = 3;
        }
        else if($ob->status == 4)
        {
            $z = 4;
        }

        // Check Mobile Status
        $zy =$this->checkMobileStatus($user_name, $user_mobile, $user_amount);
        if ($zy == 6)
        {
            $z = 6;
        }

        if($z == 0)
        {
             // Echo Data
           /* echo "Transaction ID :".$trans_id."<br>"; 
            echo "Network Type Code :".$net_type_code."<br>";
            echo "Network Code :".$net_code."<br>";
            echo "Mobile No :".$user_mobile."<br>";
            echo "Recharge Amount :".$user_amount."<br>";
            echo "API Code :".$api_code."<br>";
            echo "Network Line Type :".$v."<br>";*/

            $api_url = $this->generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);

            //echo "URL :".$api_url."<br>";

            if($api_url != "NONE")
            {
                
                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                if($res->getStatusCode() == 200)
                {
                    // Do Payment Process
                    // Network Percentage Calculation
                    list($per, $peramt, $sur, $netamt) = $this->getRetailerPercentage($user_name, $net_code, $user_amount);
                    /*echo "<br>Retailer Network Percentage Amount<br>";
                    echo "Recharge Amount   :".$user_amount."<br>";
                    echo "Percentage        :".$per."<br>";
                    echo "Percentage Amount :".number_format($peramt,2)."<br>";
                    echo "Surplus Charge    :".number_format($sur,2)."<br>";
                    echo "Net Amount        :".number_format($netamt,2)."<br>";*/

                    // User Recharge Details
                    $rech = new stdClass();
                    $rech->trans_id = $trans_id;
                    $rech->api_trans_id = $user_trid;
                    $rech->user_name = $user_name;
                    $rech->net_code = $net_code;
                    $rech->rech_mobile = $user_mobile;
                    $rech->rech_amount = $user_amount;
                    $rech->recharge_total = $this->convertNumberFormat($netamt);
                    $rech->rech_date = $date_time;
                    $rech->rech_status = "PENDING";
                    $rech->rech_mode = "API";
                    $rech->api_code = $api_code;
                    $rech->trans_bid = $trans_id;
                    $rech->reply_opr_id = "";
                    $rech->reply_date = $date_time;

                    $zx = $this->insertRecharge($rech);
                    if($zx == 1)
                    {
                        // Do All other Insertions...
                        // User Recharge Request Details
                        $rech_r = new stdClass();
                        $rech_r->trans_id = $trans_id;
                        $rech_r->api_trans_id = $user_trid;
                        $rech_r->user_name = $user_name;
                        $rech_r->net_code = $net_code;
                        $rech_r->rech_mobile = $user_mobile;
                        $rech_r->rech_amount = $user_amount;
                        $rech_r->rech_date = $date_time;
                        $rech_r->rech_status = "PENDING";
                        $rech_r->rech_mode = "API";
                        $rech_r->api_code = $api_code;
                        $rech_r->rech_req_status = $res->getBody();

                        $zx1 = $this->insertRechargeRequest($rech_r);

                         // User Payment Details
                        $rech_p1 = new stdClass();
                        $rech_p1->trans_id = $trans_id;
                        $rech_p1->api_trans_id = $user_trid;
                        $rech_p1->user_name = $user_name;
                        $rech_p1->net_code = $net_code;
                        $rech_p1->rech_mobile = $user_mobile;
                        $rech_p1->rech_amount = $user_amount;
                        $rech_p1->rech_net_per = $per;
                        $rech_p1->rech_net_per_amt = $this->convertNumberFormat($peramt);
                        $rech_p1->rech_net_surp = $this->convertNumberFormat($sur);
                        $rech_p1->rech_total = $this->convertNumberFormat($netamt);
                        $rech_p1->user_balance = 0;
                        $rech_p1->rech_date = $date_time;
                        $rech_p1->rech_status = "PENDING";
                        $rech_p1->rech_type = "API_RECHARGE";
                        $rech_p1->created_at = $date_time;
                        $rech_p1->updated_at = $date_time;

                        $zp1 = $this->insertRechargePayment($rech_p1);

                        // Parent Network Percentage Calculation
                        list($parent_type, $parent_name) = $this->getParent($user_name);
                        list($per_d, $peramt_d, $sur_d, $netamt_d) = $this->getDistributorPercentage($parent_name, $net_code, $user_amount);
                        /*echo "<br>Distributor Network Percentage Amount<br>";
                        echo "Recharge Amount   :".$user_amount."<br>";
                        echo "Percentage        :".$per_d."<br>";
                        echo "Percentage Amount :".number_format($peramt_d,2)."<br>";
                        echo "Surplus Charge    :".number_format($sur_d,2)."<br>";
                        echo "Net Amount        :".number_format($netamt_d,2)."<br>";*/

                        // Distributor Payment Details
                        $rech_p2 = new stdClass();
                        $rech_p2->trans_id = $trans_id;
                        $rech_p2->api_trans_id = $user_trid;
                        $rech_p2->super_parent_name = "NONE";
                        $rech_p2->parent_name = $parent_name;
                        $rech_p2->user_name = $user_name;
                        $rech_p2->net_code = $net_code;
                        $rech_p2->rech_mobile = $user_mobile;
                        $rech_p2->rech_amount = $user_amount;
                        $rech_p2->rech_net_per = $per_d;
                        $rech_p2->rech_net_per_amt = $this->convertNumberFormat($peramt_d);
                        $rech_p2->rech_net_surp = $this->convertNumberFormat($sur_d);
                        $rech_p2->rech_total = $this->convertNumberFormat($netamt_d);
                        $rech_p2->user_balance = 0;
                        $rech_p2->rech_date = $date_time;
                        $rech_p2->rech_status = "PENDING";
                        $rech_p2->rech_type = "API_RECHARGE";
                        $rech_p2->created_at = $date_time;
                        $rech_p2->updated_at = $date_time;

                        $zp2 = $this->insertRechargePaymentDistributor($rech_p2, $rech_p2->parent_name);
                        $zp22 = $this->insertTransactionParent($trans_id, $rech_p2->parent_name);

                        list($parent_type_s, $parent_name_s) = $this->getParent($parent_name);
                        if($parent_name_s != "NONE" && $parent_name_s != "admin")
                        {
                            list($per_s, $peramt_s, $sur_s, $netamt_s) = $this->getDistributorPercentage($parent_name_s, $net_code, $user_amount);
                            /*echo "<br>Super Distributor Network Percentage Amount<br>";
                            echo "Recharge Amount   :".$user_amount."<br>";
                            echo "Percentage        :".$per_s."<br>";
                            echo "Percentage Amount :".number_format($peramt_s,2)."<br>";
                            echo "Surplus Charge    :".number_format($sur_s,2)."<br>";
                            echo "Net Amount        :".number_format($netamt_s,2)."<br>";*/

                            // Super Distributor Payment Details
                            $rech_p3 = new stdClass();
                            $rech_p3->trans_id = $trans_id;
                            $rech_p3->api_trans_id = $user_trid;
                            $rech_p3->super_parent_name = $parent_name_s;
                            $rech_p3->parent_name = $parent_name;
                            $rech_p3->user_name = $user_name;
                            $rech_p3->net_code = $net_code;
                            $rech_p3->rech_mobile = $user_mobile;
                            $rech_p3->rech_amount = $user_amount;
                            $rech_p3->rech_net_per = $per_s;
                            $rech_p3->rech_net_per_amt = $this->convertNumberFormat($peramt_s);
                            $rech_p3->rech_net_surp = $this->convertNumberFormat($sur_s);
                            $rech_p3->rech_total = $this->convertNumberFormat($netamt_s);
                            $rech_p3->user_balance = 0;
                            $rech_p3->rech_date = $date_time;
                            $rech_p3->rech_status = "PENDING";
                            $rech_p3->rech_type = "API_RECHARGE";
                            $rech_p3->created_at = $date_time;
                            $rech_p3->updated_at = $date_time;

                            $zp3 = $this->insertRechargePaymentDistributor($rech_p3, $rech_p3->super_parent_name);
                            $zp33 = $this->insertTransactionParent($trans_id, $rech_p3->super_parent_name);

                        }

                        $zt = $this->insertTransaction($trans_id, $user_name);
                        $op = "Recharge Process has Completed...";
                        
                    }
                    // End DO payment Process
                }
                else
                {
                    $z = 5;
                }

            }
            // Url Success Ends.....
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! Low user Balance...";
        }
        else if($z == 4)
        {   
            $op = "Error! This Account is locked...";
        }
        else if($z == 5)
        {
            $op = "Error! API Provider is Unavailable...";
        }
        else if($z == 6)
        {
            $op = "Error! Pending Status for this Mobile No & amt...";
        }

        $net_ubal = $this->getUserBalance($user_name);

        $resultdata = array('status' => $op, 'balance' => $net_ubal);
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($resultdata);

        

    }


    public function storeReprocess(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;

        $d1 = $request->all();
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "trid")
            {
                $trans_id = $value;
            }
            else if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "net_code")
            {
                $net_code = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
            else if($key == "amt")
            {
                $user_amount = $value;
            }
            else if($key == "api_trid")
            {
                $user_trid = $value;
            }
            
        }

        $ob = $this->getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;

        $date_time = date("Y-m-d H:i:s");

       /* echo "User Code :".$user_code."<BR>";
        echo "User Name :".$user_name."<BR>";
        echo "User PWD :".$user_pwd."<BR>";
        echo "User Balance :".$user_ubal."<BR>";
        echo "Operator :".$operator."<BR>";
        echo "User Mobile :".$user_mobile."<BR>";
        echo "User Amount :".$user_amount."<BR>";
        echo "User Trid :".$user_trid."<BR>";
        echo "Network Code :".$net_code."<BR>";
        echo "Network Type Code :".$net_type_code."<BR>";
        echo "Transaction Id :".$trans_id."<BR>";
        echo "date time :".$date_time."<BR>";*/

        /*
        |--------------------------------------------------------------------------
        | Recharge Process
        |--------------------------------------------------------------------------
        |   1. Check Valid User and Valid API Partner and User Account Status
        |   2. Check User Balance  
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        
        // Get Network Line
        list($api_code, $v) = $this->getNetworkLine($net_code, $user_amount, $user_name);
        if($api_code == 0)
        {
            $z = 2;     //No Network Line is Selected...
        }

        
        
        if($z == 0)
        {
            
            $api_url = $this->generateAPI($api_code, $net_code, $user_mobile, $user_amount, $trans_id);

            //echo "URL :".$api_url."<br>";

            if($api_url != "NONE")
            {
                
                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                if($res->getStatusCode() == 200)
                {
                   

                    $op = "Recharge Process has Completed...";

                   
                    // End DO payment Process
                }
                else
                {
                    $z = 5;
                }

            }
            // Url Success Ends.....
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else
        {
            $op = "Error! ";
        }

        $net_ubal = $this->getUserBalance($user_name);

        $resultdata = array('status' => $op, 'balance' => $net_ubal);
        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode($resultdata);

        

    }

    public function getUserAccountDetails($user_name, $user_pwd)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;
        $ob->status = 0;

        $uacc_1 = new UserAccountBalanceDetails;
        $user_2 = new UserAccountDetails;

        $user_rec_mode = "";
        $user_status = 25;

        $d2 = $user_2->select('user_code', 'user_type', 'user_rec_mode', 'user_status')->where([['user_name', '=', $user_name], ['user_pwd', '=', $user_pwd]])->get();
        foreach($d2 as $d)
        {
            $ob->user = $user_name;
            $ob->code = $d->user_code;
            $ob->mode = $d->user_type;
            $user_rec_mode = $d->user_rec_mode;
            $user_status = $d->user_status;
            $ob->status = 0;
        }
        if (strpos($user_rec_mode, 'API') !== false) {
            // do nothing
            $x3 = 0;    // test variable
        }
        else
        {
            $ob->status = 3;
        }
        // Check User Account Status
        if($user_status != 1)
        {
            $ob->status = 4;
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getUserBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_balance;
        }

        return $user_ubal;
    }

    public function getNetworkCode($net_short)
    {
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $net_type_code = 0;


        $d1 = $net_1->select('net_code', 'net_type_code')->where('net_short_code', '=', $net_short)->get();
        foreach($d1 as $d)
        {
            $net_code = $d->net_code;
            $net_type_code = $d->net_type_code;
        }

        return array($net_code, $net_type_code);
    }

    public function getNetworkLine($net_code, $amt, $user_name)
    {
        $net_3 = new NetworkLineApipartnerDetails;
        $api_code = 0;
        $zx = 0;
        $v = 0;

        // Specific Amount
        $d1 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'SPECIFIC_AMOUNT'],  
                             ['net_line_value_1', '=', $amt], ['net_line_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $api_code = $d1[0]->api_code;
            $v = 1;
        }
        else
        {
            // Range Values
            $d2 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'RANGE_VALUES'], ['net_line_status', '=', 1]])->get();

            $zx = 0;
            if($d2->count() > 0)
            {
                foreach($d2 as $d)
                {
                    $v1 = $d->net_line_value_1;
                    $v2 = $d->net_line_value_2;

                    if($v1 != "" & $v2 != "")
                    {
                        if($v1 != "*" & $v2 != "*")
                        {
                            if((floatval($amt) >= floatval($v1)) && (floatval($amt) <= floatval($v2)))
                            {
                                $api_code = $d->api_code;
                                $zx = 1;
                                $v = 2;
                                break;
                            }
                        }
                    }

                }
            }

            // All Values
            if($zx == 0)
            {
                $d3 = $net_3->where([['net_code', '=', $net_code], ['user_name', '=', $user_name], ['net_line_type', '=', 'ALL'], ['net_line_status', '=', 1]])->get();
                if($d3->count() > 0)
                {
                    $api_code = $d3[0]->api_code;
                    $v = 3;
                }
            }

        }

        return array($api_code, $v);
    }

    public function generateAPI($api_code, $net_code, $mobile, $amt, $trans_id)
    {
        $api_1 = new ApiProviderDetails;
        $api_2 = new ApiProviderParameterDetails;
        $api_3 = new ApiProviderNetworkDetails;

        $url = "NONE";
        $url_link = "";
        $api_net_code = "";
        $zx = 0;

        $d1 = $api_1->select('api_url')->where([['api_code', '=', $api_code], ['api_status', '=', 1]])->get();
        if($d1->count() > 0)
        {
            $url_link = $d1[0]->api_url;
            $url_link = $url_link."?";

            // Get Network Code Details
            $d3 = $api_3->select('api_net_short_code')->where([['api_code', '=', $api_code], ['net_code', '=', $net_code]])->get();
            if($d3->count() > 0)
            {
                $api_net_code = $d3[0]->api_net_short_code;

                 // Get Paramenters
                $d2 = $api_2->select('api_para_type', 'api_para_name', 'api_para_value')->where('api_code', '=', $api_code)->get();
                if($d2->count() > 0)
                {
                    foreach($d2 as $d)
                    {
                        if ($d->api_para_type == "PRE_DEFINED")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$d->api_para_value."&";
                        }
                        else if ($d->api_para_type == "MOBILE_NO")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$mobile."&";
                        }
                        else if ($d->api_para_type == "AMOUNT")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$amt."&";
                        }
                        else if ($d->api_para_type == "NETWORK")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$api_net_code."&";
                        }
                        else if ($d->api_para_type == "TRANSACTION_ID")
                        {
                            $url_link = $url_link.$d->api_para_name."=".$trans_id."&";
                        }
                    }
                }
                else
                {
                    $zx = 1;
                }
            }
            else
            {
                $zx = 1;
            }

        }
        else
        {
            $zx = 1;
        }

        if($zx == 0)
        {
            $url = $url_link;
            $url = rtrim($url, "& ");
        }

        return $url;
    }

    public function checkMobileStatus($user_name, $mobile, $amount)
    {
        $rech_1 = new UserRechargeApipartnerDetails;
        $z = 0;

        $d1 = $rech_1->where([['user_name', '=', $user_name], 
                                ['rech_mobile', '=', $mobile],
                                ['rech_amount', '=', $amount],
                                ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            $z = 6;
        }

        return $z;
    }

    public function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public function getRetailerPercentage($user_name, $net_code, $amount)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
        if($d1->count() > 0)
        {
           $per = $d1[0]->user_net_per;
           //$sur = $d1[0]->user_net_surp;
        }

        $sur = $this->getSurplusCharge($net_code, $amount);

        // Calculation
        list($peramt1, $peramt2, $netamt) = $this->recharge_calculation($amount, $amount, $per, $sur);

        return array($per, $peramt2, $sur, $netamt);
    }

    public function getDistributorPercentage($user_name, $net_code, $amount)
    {
        $user_3 = new UserNetworkDetails;
        $per = 0;
        $sur = 0;
        $peramt1 = 0;
        $peramt2 = 0;
        $netamt = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_3->select('user_net_per', 'user_net_surp')->where([['user_name', '=', $user_name], ['net_code', '=', $net_code]])->get();
        if($d1->count() > 0)
        {
           $per = $d1[0]->user_net_per;
           //$sur = $d1[0]->user_net_surp;
        }

        $sur = $this->getSurplusCharge($net_code, $amount);

        // Calculation
        list($peramt1, $peramt2, $netamt) = $this->recharge_calculation($amount, 0, $per, $sur);

        return array($per, $peramt2, $sur, $netamt);
    }

    public function getSurplusCharge($net_code, $amount)
    {
        $net_2 = new NetworkSurplusDetails;
        $surp = 0;
        
        $d1 = $net_2->select('from_amount', 'to_amount', 'surplus_charge')->where('net_code', '=', $net_code)->get();
        foreach($d1 as $d)
        {
            $f_amt = $d->from_amount;
            $t_amt = $d->to_amount;

            if(floatval($f_amt) <= floatval($amount) && floatval($t_amt) >= floatval($amount))
            {
                $surp = $d->surplus_charge;
                break;
            }
        }
        
        return $surp;
    }

    public function recharge_calculation($amt, $amt1, $per, $sur)
    {
        $peramt1 = 0;
        $peramt2 = 0;

        if(floatval($per) != 0)
        {
            $peramt1 = floatval($per) / 100;
            $peramt1 = round($peramt1, 3);
    
            $peramt2 = floatval($amt) * floatval($peramt1);
            $peramt2 = round($peramt2, 2);
        }
       

        $netamt = (floatval($amt1) - floatval($peramt2)) +floatval($sur);
        $netamt = round($netamt, 2);

        return array($peramt1, $peramt2, $netamt);
    }

    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }


    //INSERT FUNCTIONS
    public function insertRecharge($rech)
    {
        $rech_1 = new UserRechargeApipartnerDetails;
        $zx = 0;

        $rech_1->trans_id = $rech->trans_id;
        $rech_1->api_trans_id = $rech->api_trans_id;
        $rech_1->user_name = $rech->user_name;
        $rech_1->net_code = $rech->net_code;
        $rech_1->rech_mobile = $rech->rech_mobile;
        $rech_1->rech_amount = $rech->rech_amount;
        $rech_1->rech_total = $rech->recharge_total;
        $rech_1->rech_date = $rech->rech_date;
        $rech_1->rech_status = $rech->rech_status;
        $rech_1->rech_mode = $rech->rech_mode;
        $rech_1->api_code = $rech->api_code;
        $rech_1->trans_bid = $rech->trans_bid;
        $rech_1->reply_opr_id = $rech->reply_opr_id;
        $rech_1->reply_date = $rech->reply_date;

        if($rech_1->save())
        {
            $zx = 1;
        }
        
        return $zx;
    }

    public function insertRechargeRequest($rech_r)
    {
        $rech_2 = new UserRechargeRequestApipartnerDetails;
        $zx = 0;

        $rech_2->trans_id =$rech_r->trans_id;
        $rech_2->api_trans_id =$rech_r->api_trans_id;
        $rech_2->user_name = $rech_r->user_name;
        $rech_2->net_code = $rech_r->net_code;
        $rech_2->rech_mobile = $rech_r->rech_mobile;
        $rech_2->rech_amount = $rech_r->rech_amount;
        $rech_2->rech_date = $rech_r->rech_date;
        $rech_2->rech_status = $rech_r->rech_status;
        $rech_2->rech_mode = $rech_r->rech_mode;
        $rech_2->api_code = $rech_r->api_code;
        $rech_2->rech_req_status = $rech_r->rech_req_status;

        if($rech_2->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertRechargePayment($rech_p1)
    {
        $rech_3 = new UserRechargePaymentApipartnerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $rech_p1->user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1->rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = $this->convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $rech_p1->user_name)
                                ->update(['user_balance'=>$net_bal, 'updated_at'=>$rech_p1->updated_at]);
            
            // Insert Payment Data
            $pay_1 = array('trans_id' => $rech_p1->trans_id,
                                'api_trans_id' => $rech_p1->api_trans_id, 
                                'user_name' => $rech_p1->user_name, 
                                'net_code' => $rech_p1->net_code,
                                'rech_mobile' =>  $rech_p1->rech_mobile, 
                                'rech_amount' => $rech_p1->rech_amount, 
                                'rech_net_per' => $rech_p1->rech_net_per, 
                                'rech_net_per_amt' => $rech_p1->rech_net_per_amt,
                                'rech_net_surp' => $rech_p1->rech_net_surp, 
                                'rech_total' => $rech_p1->rech_total, 
                                'user_balance' => $net_bal,
                                'rech_date' => $rech_p1->rech_date,
                                'rech_status' => $rech_p1->rech_status,
                                'rech_type' => $rech_p1->rech_type,
                                'created_at' => $rech_p1->created_at, 'updated_at' => $rech_p1->updated_at);
            
            if($rech_3->insert($pay_1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public function insertRechargePaymentDistributor($rech_p1, $user_name)
    {
        $rech_4 = new UserRechargePaymentParentApipartnerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;
        $u_bal = 0;
        $net_bal = 0;
        $pay_1 = [];

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_p1->rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = $this->convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance'=>$net_bal, 'updated_at'=>$rech_p1->updated_at]);
            
            // Insert Payment Data
            $pay_1 = array('trans_id' => $rech_p1->trans_id,
                                'api_trans_id' => $rech_p1->api_trans_id, 
                                'super_parent_name' => $rech_p1->super_parent_name,
                                'parent_name' => $rech_p1->parent_name,
                                'user_name' => $rech_p1->user_name, 
                                'net_code' => $rech_p1->net_code,
                                'rech_mobile' =>  $rech_p1->rech_mobile, 
                                'rech_amount' => $rech_p1->rech_amount, 
                                'rech_net_per' => $rech_p1->rech_net_per, 
                                'rech_net_per_amt' => $rech_p1->rech_net_per_amt,
                                'rech_net_surp' => $rech_p1->rech_net_surp, 
                                'rech_total' => $rech_p1->rech_total, 
                                'user_balance' => $net_bal,
                                'rech_date' => $rech_p1->rech_date,
                                'rech_status' => $rech_p1->rech_status,
                                'rech_type' => $rech_p1->rech_type,
                                'created_at' => $rech_p1->created_at, 'updated_at' => $rech_p1->updated_at);
            
            if($rech_4->insert($pay_1))
            {
                $zx =1;
            }
            
        }

        return $zx;
    }

    public function insertTransaction($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "API_RECHARGE";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertTransactionParent($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "API_RECHARGE";
        $tran_1->trans_option = '1';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }
}
