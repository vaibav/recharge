<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Libraries\GetCommon;
use App\Libraries\RechargeReportRetailer;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserComplaintDetails;

use App\Models\UserRechargeNewDetails; 
use App\Models\UserRechargeNewParentDetails;

class UserComplaintController extends Controller
{
    //
    public function index_retailer(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('retailer.complaint_1', ['user' => $ob]);
        
    }

    public function index_distributor(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('user.complaint_distributor', ['user' => $ob]);
        
    }

    public function index_apipartner(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('user.complaint_apipartner', ['user' => $ob]);
        
    }

    public function normal_retailer(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('retailer.complaint_normal', ['user' => $ob]);
        
    }

    public function recharge_retailer(Request $request)
	{
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $mobile = trim($request->user_mobile);

        if($date_1 != "" && $date_2 != "")
        {
            $f_date = $date_1." 00:00:00";
            $t_date = $date_2." 23:59:59";
        }
        else
        {
            $f_date = "";
            $t_date = "";
        }
        

        $dc4 = RechargeReportRetailer::getRechargeComplaint($f_date, $t_date, $ob->user, $mobile);

        $d2 = $net_2->select('net_code','net_name')->get();
        
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('retailer.complaint_recharge', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2]); 

    }
    

    public function store_retailer(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;


        $this->validate($request, [
            'user_complaint' => 'required'
        ],
        [
            'user_complaint.required' => 'Complaint field is required.'
            ]);

        $ob = GetCommon::getUserDetails($request);
        
        // Insert Record... 
        $comp_1->trans_id = rand(10000,99999);
        $comp_1->user_name = $ob->user;
        $comp_1->user_type = $ob->mode;
        $comp_1->comp_type = "NORMAL";
        $comp_1->rech_trans_id = "-";
        $comp_1->rech_mobile = trim($request->rech_mobile);
        $comp_1->rech_amount = trim($request->rech_amount);
        $comp_1->user_complaint = trim($request->user_complaint);
        $comp_1->admin_reply = "";
        $comp_1->reply_status = 1;
        $comp_1->reply_date = date("Y-m-d H:i:s");

        $comp_1->save();
        
        $op = 1;

       
        return redirect()->back()->with('msg', $op);

		
		
    }

    public function store_retailer_recharge(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;

        $ob = GetCommon::getUserDetails($request);

        $rech_tr_id = trim($request->trid);
        $rech_mobile = trim($request->mob);
        $rech_amount = trim($request->amt);
        $user_complaint = trim($request->cmp);


        //file_put_contents('complaint.txt', $rech_tr_id."--".$rech_mobile."--".$rech_amount."--".$user_complaint);
        
        // Insert Record... 
        $comp_1->trans_id = rand(10000,99999);
        $comp_1->user_name = $ob->user;
        $comp_1->user_type = $ob->mode;
        $comp_1->comp_type = "RECHARGE";
        $comp_1->rech_trans_id = $rech_tr_id;
        $comp_1->rech_mobile = $rech_mobile;
        $comp_1->rech_amount = $rech_amount;
        $comp_1->user_complaint = $user_complaint;
        $comp_1->admin_reply = "";
        $comp_1->reply_status = 1;
        $comp_1->reply_date = date("Y-m-d H:i:s");

        $comp_1->save();
        
        $op = 1;

       
        return redirect()->back()->with('msg', $op);

		
		
    }
    
    public function user_view(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->where('user_name', '=', $ob->user)->get();

        return view('retailer.complaint_view', ['comp1' => $data, 'user' => $ob]);
        
    }

    //ADMIN ACCEPT PROCESS
    public function admin_view(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->where('reply_status', '=', '1')->get();

        return view('admin.complaintaccept', ['comp1' => $data, 'user' => $ob]);
        
    }

    public function complaint_update($trans_id, $admin_reply, Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;

        $ob = GetCommon::getUserDetails($request);

        $date_time = date("Y-m-d H:i:s");
        $d1 = $comp_1->where('trans_id', '=', $trans_id)->get();
        if($d1->count() > 0)
        {
            $comp_1->where('trans_id', '=', $trans_id)
                                ->update(['admin_reply' => $admin_reply,
                                'reply_status' => '2',
                                'reply_date' => $date_time,
                                'updated_at' => $date_time]);
            $op = 1;
        }

    
        return redirect()->back()->with('msg', $op);

    }

    public function admin_view1(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->get();

        return view('admin.complaint_view', ['comp1' => $data, 'user' => $ob]);
        
    }

    public function complaint_refund($trans_id, $admin_reply, $rech_trans_id, Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $rech_1 = new UserRechargeNewDetails;
        $rech_p_1 = new UserRechargeNewParentDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $op = 0;
        $date_time = date("Y-m-d H:i:s");

        // Refund Process
        $d1 = $rech_1->select('rech_status')->where('trans_id', '=', $rech_trans_id)->get();

        if($d1->count() > 0)
        {
            $rech_status = $d1[0]->rech_status;

            if($rech_status == "SUCCESS")
            {
                $rech_1->where('trans_id', '=', $rech_trans_id)
                            ->update(['rech_status' => 'PENDING', 'rech_option' => '0', 'updated_at' => $date_time]);

                $rech_p_1->where('trans_id', '=', $rech_trans_id)
                            ->update(['rech_status' => 'PENDING', 'rech_option' => '0', 'updated_at' => $date_time]);

                $oprtr_id = "refund_success";

                $url = url('/')."/pendingreport_failure_1/".$rech_trans_id."/".$oprtr_id;
                
                try
                {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($url);
                    $res_code = $res->getStatusCode();
                    $res_content_1 =$res->getBody();
                }
                catch (\GuzzleHttp\Exception\ClientException $e) {
                    $res_content = "{'status':'failure','tnx_id':'API URL error...'}";
                }

            }

        }


        $d2 = $comp_1->where('trans_id', '=', $trans_id)->get();
        if($d2->count() > 0)
        {
            $comp_1->where('trans_id', '=', $trans_id)
                                ->update(['admin_reply' => $admin_reply,
                                'reply_status' => '2',
                                'reply_date' => $date_time,
                                'updated_at' => $date_time]);
            $op = 1;
        }

    
        return redirect()->back()->with('msg', $op);

    }
    
}
