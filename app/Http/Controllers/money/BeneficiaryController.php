<?php

namespace App\Http\Controllers\money;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberBankRemitter;
use App\Libraries\CyberBankBeneficiary;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;

class BeneficiaryController extends Controller
{
    //
    public function registerBeneficiary(Request $request)
    {
        $mobileNo     = "0";
        $remitter     = [];
        $bank         = [];

        $ob = GetCommon::getUserDetails($request);

        if($request->session()->has('mobileNo'))
        {
            $mobileNo    = $request->session()->get('mobileNo');
            $remitter    = $request->session()->get('remitter');
        }

        //Get Bank
        $apiUrl = "http://www.vaibavonline.com/vaibav/api/bank-data";

        try
        {
            if(!$request->session()->has('bank'))
            {
                $client = new \GuzzleHttp\Client();
                $res = $client->get($apiUrl);
                $res_code = $res->getStatusCode();
                $res_content = (string) $res->getBody();

                if($res_code == 200) {
                    $jsonData     = json_decode($res_content);

                    usort($jsonData->bank, array( $this, 'compareBank' ));

                    foreach($jsonData->bank as $d)
                    {
                        array_push($bank, ['bankCode' => $d->bank_sort_name, 'bankName' => $d->bank_name, 'ifscShort' => $d->ifsc_alias, 
                                'branchIfsc' => $d->branch_ifsc]);
                    }

                    $request->session()->put('bank', $bank);

                   // print "<pre>";
                    //print_r($jsonData->bank);
                }
            }
            else
            {
                $bank = $request->session()->get('bank');
            }
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            $res_content = "{'status':'failure','Message':'".$e->getMessage()."'}";
        }

        return view('retailer.money.ben_entry', ['user' => $ob, 'mobileNo' => $mobileNo, 'remitter' => $remitter, 'bank' => $bank]);
    }

    public function storeBeneficiary(Request $request)
    {
        $mobileNo     = trim($request->mobileNo);
        $firstName    = trim($request->firstName);
        $remId        = trim($request->remId);
        $benAccount   = trim($request->ben_acc_no);
        $benName      = trim($request->ben_acc_name);
        $benIFSC      = trim($request->bank_ifsc);
        $benBank      = trim($request->bank_code);
        $remitter     = [];

        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, ['ben_acc_no' => 'required', 'bank_ifsc' => 'required'], 
                                ['ben_acc_no.required' => ' Account No is required.', 'bank_ifsc.required' => ' IFSC Code is required.']);

        $data = [   'user_name'    => $ob->user,
                    'mobileNo'     => $mobileNo,
                    'remId'        => $remId,
                    'firstName'    => $firstName,
                    'lastName'     => "-",
                    'benAccount'   => $benAccount,
                    'benName'      => $benName,
                    'benIFSC'      => $benIFSC,
                    'benBank'      => $benBank,
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

       

        list($z, $msg) = CyberBankBeneficiary::add($data);

        //echo $z."--".$msg;

        return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z ]);
    }

    public function deleteBeneficiary(Request $request, $remId, $mobileNo, $benId)
    {
        $remitter     = [];

        $ob = GetCommon::getUserDetails($request);

        $data = [   'user_name'    => $ob->user,
                    'remId'        => $remId,
                    'mobileNo'     => $mobileNo,
                    'benId'        => $benId,
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

       

        list($z, $msg) = CyberBankBeneficiary::delete($data);

        //echo $z."--".$msg;
        

        return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z, 'benId' => $benId]);
    }

    public function deleteBeneficiaryOtp(Request $request, $remId, $mobileNo, $benId, $otc)
    {

        $ob = GetCommon::getUserDetails($request);

        $data = [   'user_name'    => $ob->user,
                    'remId'        => $remId,
                    'mobileNo'     => $mobileNo,
                    'benId'        => $benId,
                    'otc'          => $otc,
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

       
        list($z, $msg) = CyberBankBeneficiary::deleteOtp($data);
        

        return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z]);
    }

    public function checkBeneficiary(Request $request)
    {
        
        $benAccount = trim($request->ben_acc_no);
        $benIFSC    = trim($request->bank_ifsc);
        $mobileNo   = trim($request->agent_msisdn);
        $bankCode   = trim($request->bank_code);

        $ob = GetCommon::getUserDetails($request);

        $data = [   'user_name'    => $ob->user,
                    'mobileNo'     => $mobileNo,
                    'benAccount'   => $benAccount,
                    'benIFSC'      => $benIFSC,
                    'benBank'     => $bankCode,
                    'api_trans_id' => '-',
                    'rechMode'     => 'WEB'
                ];

       
        list($z, $msg, $benName) = CyberBankBeneficiary::checkBeneficiary($data);

        $result = ['status' => $z, 'ben_name' => $benName, 'message' => $msg];
        
         return response()->json($result); 
    }

    public function  getBankIFSC(Request $request, $bankCode)
    {

        $bankIFSC = "-";

        if($request->session()->has('bank'))
        {
            $bank = $request->session()->get('bank');

            foreach($bank as $d)
            {
                if($d['bankCode'] == $bankCode)
                {
                    $bankIFSC = $d['branchIfsc'];
                }
            }
        }
        
        echo $bankIFSC;

    }

    public function  getBankName(Request $request, $bankLetter)
    {

        $bankNames = "-";

        if($request->session()->has('bank'))
        {
            $bank = $request->session()->get('bank');

            foreach($bank as $d)
            {
                $bankName = $d['bankName'];
                if($bankName[0] == $bankLetter)
                {
                    $bankNames = $bankNames."<option value ='".$d['bankCode']."'>".$d['bankName']."</option>";
                }
            }
        }
        
        echo $bankNames;

    }

    public function compareBank($a, $b)
    {
        return strcmp($a->bank_name, $b->bank_name);
    }
}