<?php

namespace App\Http\Controllers\money;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberBankTransferNew;
use App\Libraries\CyberBankRemitter;

use App\Models\UserBankRemitterDetails;

class TransferController extends Controller
{
    //
    public function indexTransfer(Request $request)
    {
       
        $mobileNo     = "0";
        $remitterName = "NONE";
        $remitter     = [];
        $beneficiary  = [];

        $ob = GetCommon::getUserDetails($request);

        if($request->session()->has('mobileNo'))
        {
            $mobileNo    = $request->session()->get('mobileNo');
            //$remitter    = $request->session()->get('remitter');
            //$beneficiary = $request->session()->get('beneficiary');

            list($z, $op, $result, $remitter, $beneficiary, $isVerified) = CyberBankRemitter::userCheck($mobileNo);

            $request->session()->put('remitter', $remitter);
            $request->session()->put('beneficiary', $beneficiary);
        }

        return view('retailer.money.transfer', ['user' => $ob, 'mobileNo' => $mobileNo, 'remitter' => $remitter, 'beneficiary' => $beneficiary]);
    }

    public function store(Request $request)
    {
        $ob = GetCommon::getUserDetails($request);

        $mobileNo       = trim($request->c_msisdn);
        $beneId         = trim($request->ben_id);
        $bankName       = trim($request->bank_name);
        $bankIFSC       = trim($request->bank_code);
        $accountNo      = trim($request->cus_acc_no);
        $customerName   = trim($request->cus_name);
        $transferMode   = trim($request->bank_mode);
        $transferAmount = trim($request->cus_amt);

       // echo $beneId."---".$bankName."---".$bankIFSC."---".$accountNo."---".$customerName."---".$transferMode."---".$transferAmount."<br>";

        $z = -1;
        $msg = "No Transaction";
        $otp_status = "-1";

        $remitter    = $request->session()->get('remitter');

       


        if(floatval($transferAmount) <= 5000)
        {
            $transId = "VBMR".rand(10000000,99999999);
            $otp_status ="0";
            $rep_trans_id = "";
            $remarks ="";

            $this->validate($request, ['cus_acc_no' => 'required'], ['cus_acc_no.required' => ' Account No is required.']);

            if($bankIFSC != "" && $accountNo != "" && $transferMode != "" && $transferAmount != "" && $beneId != "" && $mobileNo != "")
            {
                $data = [   'clientTransId' => $transId,
                            'mobileNo' => $mobileNo,
                            'benId' => $beneId,
                            'bankName' => $bankName,
                            'bankIFSC' => $bankIFSC,
                            'accountNo' => $accountNo,
                            'customerName' => $customerName,
                            'routingType' => $transferMode,
                            'amount' => $transferAmount,
                            'userCode' => $ob->code,
                            'userName' => $ob->user,
                            'userBal' => $ob->ubal,
                            'apiTransId' => "",
                            'rechMode' => 'WEB',
                            'remitter' => $remitter
                        ];

                list($z, $msg, $otp_status) = CyberBankTransferNew::add($data);
            }
        }
        else 
        {
            $reminderAmount = floatval($transferAmount) % 5000;
            $transferAmount = floatval($transferAmount) - floatval($reminderAmount);
            $numberofTimes  = floatval($transferAmount) / 5000;

            if(floatval($reminderAmount) > 0)
            {
                $transId = "VBMR".rand(10000000,99999999);
                $otp_status ="0";
                $rep_trans_id = "";
                $remarks ="";

                $this->validate($request, ['cus_acc_no' => 'required'], ['cus_acc_no.required' => ' Account No is required.']);

                if($bankIFSC != "" && $accountNo != "" && $transferMode != "" && $transferAmount != "" && $beneId != "" && $mobileNo != "")
                {
                    $data = [   'clientTransId' => $transId,
                                'mobileNo' => $mobileNo,
                                'benId' => $beneId,
                                'bankName' => $bankName,
                                'bankIFSC' => $bankIFSC,
                                'accountNo' => $accountNo,
                                'customerName' => $customerName,
                                'routingType' => $transferMode,
                                'amount' => $reminderAmount,
                                'userCode' => $ob->code,
                                'userName' => $ob->user,
                                'userBal' => $ob->ubal,
                                'apiTransId' => "",
                                'rechMode' => 'WEB',
                                'remitter' => $remitter
                            ];

                    list($z, $msg, $otp_status) = CyberBankTransfer::add($data);
                }
            }

            //Times
            for($i = 1; $i <= $numberofTimes; $i++)
            {
                $transId = "VBMR".rand(10000000,99999999);
                $otp_status ="0";
                $rep_trans_id = "";
                $remarks ="";

                $this->validate($request, ['cus_acc_no' => 'required'], ['cus_acc_no.required' => ' Account No is required.']);

                if($bankIFSC != "" && $accountNo != "" && $transferMode != "" && $transferAmount != "" && $beneId != "" && $mobileNo != "")
                {
                    $data = [   'clientTransId' => $transId,
                                'mobileNo' => $mobileNo,
                                'benId' => $beneId,
                                'bankName' => $bankName,
                                'bankIFSC' => $bankIFSC,
                                'accountNo' => $accountNo,
                                'customerName' => $customerName,
                                'routingType' => $transferMode,
                                'amount' => "5000",
                                'userCode' => $ob->code,
                                'userName' => $ob->user,
                                'userBal' => $ob->ubal,
                                'apiTransId' => "",
                                'rechMode' => 'WEB',
                                'remitter' => $remitter
                            ];

                    list($z, $msg, $otp_status) = CyberBankTransfer::add($data);
                }
            }
        }

       


        return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z ]);

        //return view('user.money_send', ['user' => $ob, 'msisdn' => $msisdn, 'rem_name' => $rem_name, 'agent_id' => $agent_id, 'data' => $data, 'beni' => $beni]);


    }
}