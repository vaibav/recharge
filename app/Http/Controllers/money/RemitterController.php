<?php

namespace App\Http\Controllers\money;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CyberBankRemitter;

use App\Models\UserBankRemitterDetails;

class RemitterController extends Controller
{
    //
    public function registerRemiiter(Request $request)
    {
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $rem_1->where('user_name', '=', $ob->user)->get();

        return view('retailer.money.check', ['user' => $ob, 'remitter' => $d1]);
    }

    public function registerRemitterCheck(Request $request)
    {
        $ob = GetCommon::getUserDetails($request);

        $mobileNo = trim($request->msisdn);

        list($z, $op, $result, $remitter, $beneficiary, $isVerified) = CyberBankRemitter::userCheck($mobileNo);

        if($z == 0)
        {
            if($isVerified == "0") {
                     return redirect('bank_remitter_otp_check?msisdn='.$mobileNo);
            }

            $request->session()->put('mobileNo', $mobileNo);
            //$request->session()->put('remitter', $remitter);
            //$request->session()->put('beneficiary', $beneficiary);
            
            //$y = Beneficiary::checkServerBeneficiary($result, $ob->user, $msisdn);

            return redirect('money-dashboard');
        }

         return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z, 'msisdn' => $mobileNo]);
    }

    public function registerRemiiterAdd(Request $request)
    {
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $msisdn = trim($request->msisdn);

        return view('retailer.money.remitter_entry', ['user' => $ob, 'msisdn' => $msisdn]);
    }

    public function registerRemiiterStore(Request $request)
    {
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

        $this->validate($request, ['user_rem_name' => 'required',
                                    'user_rem_lname' => 'required',
                                    'user_address' => 'required',
                                    'user_city' => 'required',
                                    'user_pincode' => 'required',
                                    'msisdn' => 'required'
                                ], 
                                ['user_rem_name.required' => ' Remitter Name is required.',
                                'user_rem_lname.required' => ' Remitter Last Name is required.',
                                'user_address.required' => ' Address is required.',
                                'user_city.required' => ' Remitter City is required.',
                                'user_pincode.required' => ' Pincode is required.',
                                'msisdn.required' => ' Remitter Mobile No is required.'
                                ]);


        //$agent_id = $this->getAgentId($ob->user);
        
        $msisdn = trim($request->msisdn);

        $data = ['user_name_1' => $ob->user,
                    'user_name' => trim($request->user_rem_name),
                    'user_lname' => trim($request->user_rem_lname),
                    'user_address' => trim($request->user_address),
                    'user_city' => trim($request->user_city),
                    'user_state_code' => trim($request->user_state_code),
                    'user_pincode' => trim($request->user_pincode),
                    'mobileNo' => $msisdn,
                    'api_trans_id' => "",
                    'r_mode' => "WEB"
                ];


            list($z, $msg, $res_content, $isVerified) = CyberBankRemitter::add($data);

           
            
           $result = [];

            if($z == 0)
            {
                if($isVerified == "0") {
                     return redirect('bank_remitter_otp_check?msisdn='.$msisdn);
                }
                else if($isVerified == "1") {
                    $request->session()->put('mobileNo', $msisdn);
                      return redirect('money-dashboard');
                }
               
            }
            else if($z == 1)
                $op = "Mode web is not Selected...";
            else if($z == 2)
                $op = "Account is Inactivated...";
            else if($z == 3)
                $op = "Already Registered...";
            else if($z == 4)
                $op = $msg;
            else if($z == 5)
                $op = "PENDING...";
       
       

        //return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    
    }

    public function registerRemiiterOtp(Request $request)
    {
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        $msisdn = "0";

        $msisdn = trim($request->msisdn);

        return view('retailer.money.remitter_otp', ['user' => $ob, 'msisdn' => $msisdn]);
    }

    public function registerRemiiterOtpStore(Request $request)
    {
              
        $ob = GetCommon::getUserDetails($request);

        $z     = 0;
        $op    = "";
        $remId = "-";

        $this->validate($request, ['otp' => 'required'], ['otp.required' => ' OTP is required.']);

        //$agent_id = $this->getAgentId($ob->user);
        
        $msisdn = trim($request->msisdn);
        $otp    = trim($request->otp);

        $details = UserBankRemitterDetails::select('rem_rep_opr_id')->where('msisdn', $msisdn)->first();

        if($details) {
            $remId = $details->rem_rep_opr_id;
        }

        $data = ['user_name_1' => $ob->user,
                    'mobileNo' => $msisdn,
                    'otpNumber' => $otp,
                    'remId' => $remId,
                    'api_trans_id' => "",
                    'r_mode' => "WEB"
                ];

      

        list($z, $msg, $res_content) = CyberBankRemitter::userOtpCheck($data);
            
       $result = [];

        if($z == 0)
        {
            $op = "Entry is added Successfully...";
            return redirect('money_user_check');
        }
        else if($z == 1)
            $op = "Mode web is not Selected...";
        else if($z == 2)
            $op = "Account is Inactivated...";
        else if($z == 3)
            $op = "Already Registered...";
        else if($z == 4)
            $op = $msg;
        else if($z == 5)
            $op = "PENDING...";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    
    }

    

    
}