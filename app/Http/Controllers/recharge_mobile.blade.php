<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobile Recharge Portal">
    <title>VaibavOnline</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="{{asset('css/r_style.css')}}" rel="stylesheet">
    <link href="{{asset('css/v_tab.css')}}" rel="stylesheet">
 

</head>

<body>
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <img src = "{{ asset('img/brand/logo.png') }}" style = "height:40px; width:90px;" class="responsive-img">
        <div id="close-sidebar">
          <i class="fas fa-bars"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="{{ asset('uploadphoto/user.jpg') }}"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name">{{$user->user}}
            
          </span>
          <span class="user-role">{{$user->mode}}</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Bal : {{$user->ubal}}</span>
          </span>
        </div>
      </div>
      
      
      <div class="sidebar-menu">
          <ul>
            <li class="sidebar">
                <a href="{{url('dashboard_retailer')}}"><i class="fa fa-tachometer-alt"></i><span>Dashboard</span></a>
            </li>
      
            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-tachometer-alt"></i><span>User</span></a>
                <div class="sidebar-submenu">
                  <ul>
                      <li><a href="{{url('user_one')}}">User Details</a></li>
                      <li><a href="{{url('surplus_retailer')}}">Network Surplus Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-shopping-cart"></i><span>Payment</span></a>
                <div class="sidebar-submenu">
                  <ul>
                      <li><a href="{{url('paymentrequest_retailer')}}">Request</a></li>
                      <li><a href="{{url('stockdetails_retailer')}}">Stock Details</a></li>
                      <li><a href="{{url('report_money_verify')}}">Benificiary Verify Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="far fa-gem"></i><span>Money Transfer</span></a>
                <div class="sidebar-submenu">
                  <ul>
                    <li><a href="{{url('money_user_check')}}">Go</a></li>
                    <li><a href="{{url('ben_view')}}">Beneficiary</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-chart-line"></i><span>Report</span></a>
                <div class="sidebar-submenu">
                  <ul>
                  <li><a href="{{url('rechargedetails_retailer')}}">Recharge Details</a></li>
                    <li><a href="{{url('recharge_bill_retailer')}}">EBBill Details</a></li>
                    <li><a href="{{url('recharge_money_retailer')}}">Money Transfer Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-globe"></i><span>Collection</span></a>
                <div class="sidebar-submenu">
                  <ul>
                        <li><a href="{{url('collection_retailer')}}">Collection Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-bell"></i><span>Complaint</span></a>
                <div class="sidebar-submenu">
                  <ul>
                    <li><a href="{{url('complaint_retailer')}}">Complaint Entry</a></li>
                    <li><a href="{{url('complaint_view_retailer')}}">Complaint Details</a></li>
                  </ul>
                </div>
            </li>
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>
      <a href="{{url('changepassword_retailer')}}">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="{{url('logout')}}">
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
        <div class="card ">
            
<div class="card-body " style="padding: 5px 10px;">

    <div class="row">
        <div class="col-md-8 px-2">
            <button type="button" class="btn btnx" id="a1">Prepaid</button>
            <button type="button" class="btn btnx" id="a2">Dth</button>
            <button type="button" class="btn btnx" id="a3">Postpaid</button>
            <button type="button" class="btn btnx" id="a4">EBBill</button>
        </div>
        <div class="col-md-3 px-2">
            
        </div>
        <div class="col-md-3 px-2">
            
        </div>
        <div class="col-md-3 px-2">
            
        </div>
    </div>

    <br>
    <div class="card" style="border-radius: 15px;" id="rech_1">
    <form id="rech_form1" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        
        <div class="col-md-3  px-2">
            <div class="form-group">
                <label>Mobile No</label>
                <input type="text" class="form-control" placeholder="" id="id_user_mobile_1" name="user_mobile_1">
            </div>
        </div>
    
        <div class="col-md-3 px-2">
            <div class="form-group">
                <label>Network</label> 
                <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                        style="text-decoration: none;"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img id = "nt1_x" src = "prepaid/1.jpg" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                    <input type = "hidden" id = "net_code_1" name = "net_code_1" value="0">
                </a>
                <div class="dropdown-menu" aria-labelledby="purchase" >
                    <?php
                        foreach($network1 as $f)
                        {
                            $path = asset('networkimage/'.$f->net_code.'.jpg');

                            echo "<a class='dropdown-item' id = 'nt1_".$f->net_code."' href='#'>
                                    <img src = '".$path."' style = 'height:25px; width:90px;'/></a>";
                            
                           
                            
                        }   
                    ?> 
                   
                  
                </div>
            </div>
        </div>
    
        <div class="col-md-2 px-2">
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control" placeholder="" id="id_user_amount_1" name="user_amount_1">
            </div>
        </div>
    
        <div class="col-md-4 px-2">

            <div class="row" style="margin-top:20px;">
                <div class="col-md-5">
                        <button type="button" class="btn btny" id="btn_submit_1">Recharge</button>
                </div>
                <div class="col-md-6 text-right" style="padding:2px 5px;">
                    <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_121">121</a>
                    <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_all">plan</a>
                </div>
                
            </div>
        </div>
      
    </div>
    </form> 
    </div>

    <!-- DTH-->
    <div class="card" style="border-radius: 15px;display:none;" id="rech_2">
            <form id="rech_form2" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                
                <div class="col-md-3  px-2">
                    <div class="form-group">
                        <label>Mobile No</label>
                        <input type="text" class="form-control" placeholder="" id="id_user_mobile_2" name="user_mobile_2">
                    </div>
                </div>
            
                <div class="col-md-3 px-2">
                    <div class="form-group">
                        <label>Network</label> 
                        <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                                style="text-decoration: none;"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img id = "nt1_x" src = "prepaid/1.jpg" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                            <input type = "hidden" id = "net_code_2" name = "net_code_2" value="0">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="purchase" >
                            <a class="dropdown-item" id = "nt1_857" href="#"><img src = "prepaid/857.jpg" style = "height:25px; width:90px;"/></a>
                            <a class="dropdown-item" id = "nt1_636" href="#"><img src = "prepaid/636.jpg" style = "height:25px; width:90px;"/></a>
                            <a class="dropdown-item" id = "nt1_636" href="#"><img src = "prepaid/636.jpg" style = "height:25px; width:90px;"/></a>
                            <a class="dropdown-item" id = "nt1_510" href="#"><img src = "prepaid/510.jpg" style = "height:25px; width:90px;"/></a>
                            <a class="dropdown-item" id = "nt1_372" href="#"><img src = "prepaid/372.jpg" style = "height:25px; width:90px;"/></a>
                            <a class="dropdown-item" id = "nt1_781" href="#"><img src = "prepaid/781.jpg" style = "height:25px; width:90px;"/></a>
                        </div>
                    </div>
                </div>
            
                <div class="col-md-2 px-2">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control" placeholder="" id="id_user_amount_2" name="user_amount_2">
                    </div>
                </div>
            
                <div class="col-md-4 px-2">
        
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-5">
                                <button type="button" class="btn btny" id="btn_submit_2">Recharge</button>
                        </div>
                        <div class="col-md-6 text-right" style="padding:2px 5px;">
                            <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_info">121</a>
                           
                        </div>
                        
                    </div>
                </div>
              
            </div>
            </form> 
    </div>
    
    <!-- Postpaid-->
    <div class="card" style="border-radius: 15px;display: none;" id="rech_3">
    <form id="rech_form3" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        
        <div class="col-md-3  px-2">
            <div class="form-group">
                <label>Mobile No</label>
                <input type="text" class="form-control" placeholder="" id="id_user_mobile_3" name="user_mobile_3">
            </div>
        </div>
    
        <div class="col-md-3 px-2">
            <div class="form-group">
                <label>Network</label> 
                <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                        style="text-decoration: none;"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img id = "nt1_x" src = "prepaid/1.jpg" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                    <input type = "hidden" id = "net_code_3" name = "net_code_3" value="0">
                </a>
                <div class="dropdown-menu" aria-labelledby="purchase" >
                    <a class="dropdown-item" id = "nt1_857" href="#"><img src = "prepaid/857.jpg" style = "height:25px; width:90px;"/></a>
                    <a class="dropdown-item" id = "nt1_636" href="#"><img src = "prepaid/636.jpg" style = "height:25px; width:90px;"/></a>
                    <a class="dropdown-item" id = "nt1_636" href="#"><img src = "prepaid/636.jpg" style = "height:25px; width:90px;"/></a>
                    <a class="dropdown-item" id = "nt1_510" href="#"><img src = "prepaid/510.jpg" style = "height:25px; width:90px;"/></a>
                    <a class="dropdown-item" id = "nt1_372" href="#"><img src = "prepaid/372.jpg" style = "height:25px; width:90px;"/></a>
                    <a class="dropdown-item" id = "nt1_781" href="#"><img src = "prepaid/781.jpg" style = "height:25px; width:90px;"/></a>
                </div>
            </div>
        </div>
    
        <div class="col-md-2 px-2">
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control" placeholder="" id="id_user_amount_3" name="user_amount_3">
            </div>
        </div>
    
        <div class="col-md-4 px-2">

            <div class="row" style="margin-top:20px;">
                <div class="col-md-5">
                        <button type="button" class="btn btny" id="btn_submit_3">Recharge</button>
                </div>
                <div class="col-md-6 text-right" style="padding:2px 5px;">
                  
                </div>
                
            </div>
        </div>
        
    </div>
    </form> 
    </div>
    
   
    <!-- Reports-->
    <div class="row" style="height:410px;overflow-y: scroll;">
        <div class="col-md-4 px-2">
            <!--Report starts-->
           
                <div style='margin:10px 10px;border-radius:15px;' class='card '>
                    <div class='row ' style='padding:1px 3px;'>
                        <div class='col-2 col-sm-2 col-md-2 text-center m-0 mt-2 pl-1 pr-1' >
                            <a class='btn btn-circle green text-white' >
                                <i class='small material-icons '>check</i></a><br>
                                <p class='m-0'>65</p>
                        </div>
                        <div class='col-6 col-sm-6 col-md-6 text-left m-0 pl-1 pr-1' >
                            <p class='text-muted m-1' >VODAFONE</p>
                            <p class='text-muted m-1' >6384513896</p>
                            <p class='text-muted' style='margin:3px 5px;font-size: 11px;'>TNH1911051417220040</p>
                            <p class='text-muted' style='margin:2px 5px;font-size: 10px;'>2019-11-05 14:19:39</p>
                        </div>
                        <div class='col-3 col-sm-3 col-md-3 text-left m-0 pl-1 pr-1' >
                            <p class='text-muted' style='margin:3px 2px;font-size: 11px;margin-top:5px;'>
                            % &nbsp;0.00--0.00</p>
                            <p class='text-muted' style='margin:3px 2px;font-size: 11px;'>
                            <i class='fas fa-rupee-sign'></i> &nbsp;65.00</p>
                            <p class='text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-up'></i> &nbsp;6315.00</p>
                            <p class='text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;6250.00</p>
                        </div>
                    </div>
                </div>

                <div style="margin:10px 10px;border-radius:15px;" class="card ">
                    <div class="row " style="padding:1px 3px;">
                        <div class="col-2 col-sm-2 col-md-2 text-center" style="margin:4px 0px;padding: 5px 3px;">
                            <a class="btn btn-circle red text-white" style="padding-top: 6px;">
                                <i class="small material-icons ">close</i></a><br>
                                <p style="font-size: 14px;margin: 0;">65</p>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 text-left" style="margin:0px 0px;padding: 0px 4px;">
                            <p class="text-muted" style="margin:3px 5px;font-size: 14px;">VODAFONE</p>
                            <p class="text-muted" style="margin:3px 5px;font-size: 14px;">6384513896</p>
                            <p class="text-muted" style="margin:3px 5px;font-size: 11px;">TNH1911051417220040</p>
                            <p class="text-muted" style="margin:2px 5px;font-size: 10px;">2019-11-05 14:19:39</p>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3 text-left" style="margin:0px 0px;padding: 0px 4px;">
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;margin-top:5px;">
                            % &nbsp;0.00--0.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;">
                            <i class="fas fa-rupee-sign"></i> &nbsp;65.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;"><i class="far fa-thumbs-up"></i> &nbsp;6315.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;"><i class="far fa-thumbs-down"></i> &nbsp;6250.00</p>
                        </div>
                    </div>
                </div>

                <div style="margin:10px 10px;border-radius:15px;" class="card ">
                    <div class="row " style="padding:1px 3px;">
                        <div class="col-2 col-sm-2 col-md-2 text-center" style="margin:4px 0px;padding: 5px 3px;">
                            <a class="btn btn-circle blue text-white" style="padding-top: 6px;">
                                <i class="small material-icons ">close</i></a><br>
                                <p style="font-size: 14px;margin: 0;">65</p>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 text-left" style="margin:0px 0px;padding: 0px 4px;">
                            <p class="text-muted" style="margin:3px 5px;font-size: 14px;">VODAFONE</p>
                            <p class="text-muted" style="margin:3px 5px;font-size: 14px;">6384513896</p>
                            <p class="text-muted" style="margin:3px 5px;font-size: 11px;">TNH1911051417220040</p>
                            <p class="text-muted" style="margin:2px 5px;font-size: 10px;">2019-11-05 14:19:39</p>
                        </div>
                        <div class="col-3 col-sm-3 col-md-3 text-left" style="margin:0px 0px;padding: 0px 4px;">
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;margin-top:5px;">
                            % &nbsp;0.00--0.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;">
                            <i class="fas fa-rupee-sign"></i> &nbsp;65.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;"><i class="far fa-thumbs-up"></i> &nbsp;6315.00</p>
                            <p class="text-muted" style="margin:3px 2px;font-size: 11px;"><i class="far fa-thumbs-down"></i> &nbsp;6250.00</p>
                        </div>
                    </div>
                </div>

                    
            <!-- end-->
        </div>

        <!-- Offers 121 - Mobile Plans -->
        <div class="col-md-8 px-2">
            <div class='row'>
                <div class='col-md-3 px-2 vertical-tabs'>
                    <ul class='nav nav-tabs' role='tablist'>
                        <li class='nav-item'>
                            <a class='nav-link active' data-toggle='tab' href='#pag1' role='tab' >Topup</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag2' role='tab' >Plan</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag3' role='tab' >Página 3</a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag4' role='tab' >Página 4</a>
                        </li>
                
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag5' role='tab' >Página 5</a>
                        </li>
                
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag6' role='tab' >Página 6</a>
                        </li>
                
                        <li class='nav-item'>
                            <a class='nav-link' data-toggle='tab' href='#pag7' role='tab' >Página 7</a>
                        </li>
                    </ul>
                </div>

                <div class='col-md-9 px-2 tab-content'>
                    <div class='tab-pane active' id='pag1' role='tabpanel'>
                        <div class='sv-tab-panel'>
                            <div class = 'card mt-3 mb-3 mr-2 ml-2'  >
                                <div class='row p-1' >
                                    <div class='col-md-9 text-left m-0 pl-1 pr-1'>
                                        <p class='text-muted p-2' >
                                                ISD Calls to Canada + USA @ Rs. 1/min, UK @ Rs. 1.50/min, 
                                                Malaysia + Singapore @ Rs. 2.50/min, China + South Korea @ Rs. 4/min, 
                                                France + Greece + Romania + Russia + Spain @ Rs. 5/min, Bahrain + Kuwait + Saudi Arabia + Sri Lanka @ Rs. 6.50/min, UK @ Rs. 7/min, Yemen @ Rs. 8.99/min, UAE @ Rs. 9/min - ISD calls to Australia @ Rs. 6.99/min for selected ISD Codes - Reduced ISD rates for other selected countries
                                        </p>
                                        
                                    </div>
                                    <div class='col-md-2 text-center mt-4' >
                                        <a class='btn btn-circle green text-white'>65</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class='tab-pane' id='pag2' role='tabpanel'>
                            <div class='sv-tab-panel'>
                                <div class = 'card mt-3 mb-3 mr-2 ml-2'  >
                                    <div class='row p-1' >
                                        <div class='col-md-9 text-left m-0 pl-1 pr-1'>
                                            <p class='text-muted p-2' >
                                                    ISD Calls to Canada + USA @ Rs. 1/min, UK @ Rs. 1.50/min, 
                                                    Malaysia + Singapore @ Rs. 2.50/min, China + South Korea @ Rs. 4/min, 
                                                   
                                            </p>
                                            
                                        </div>
                                        <div class='col-md-2 text-center mt-4' >
                                            <a class='btn btn-circle green text-white'>65</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                          <div class='tab-pane' id='pag3' role='tabpanel'>
                            <div class='sv-tab-panel'>
                              <h3>TAB 3</h3>
                              <p>CONTEUDO 3</p>
                            </div>
                          </div>
                          <div class='tab-pane' id='pag4' role='tabpanel'>
                            <div class='sv-tab-panel'>
                             <h3>TAB 4</h3>
                              <p>CONTEUDO 4</p>
                            </div>
                          </div>
                  
                          <div class='tab-pane' id='pag5' role='tabpanel'>
                            <div class='sv-tab-panel'>
                              <h3>TAB 5</h3>
                              <p>CONTEUDO 5</p>
                            </div>
                          </div>
                  
                          <div class='tab-pane' id='pag6' role='tabpanel'>
                            <div class='sv-tab-panel'>
                              <h3>TAB 6</h3>
                              <p>CONTEUDO 6</p>
                            </div>
                          </div>
                  
                          <div class='tab-pane' id='pag7' role='tabpanel'>
                            <div class='sv-tab-panel'>
                              <h3>TAB 7</h3>
                              <p>CONTEUDO 7</p>
                            </div>
                          </div>

                </div>
            </div>
            
        </div>
    </div>
    
    
    
        
    
        
        
</form>
</div>

        </div>
    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="style.js" ></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#a1').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').show("slow"); $('#rech_2').hide("slow"); $('#rech_3').hide("slow"); 
            });

            $('#a2').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_2').show("slow"); $('#rech_3').hide("slow"); 
            });

            $('#a3').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_2').hide("slow"); $('#rech_3').show("slow"); 
            });

            

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("nt1_") !== -1)
                {
                   var at = gid.split("nt1_");
                   $("#net_code_1").val(at[1]);
                   var url = "{{ asset('networkimage/" + at[1] +".jpg')}}";
                   $("#nt1_x").attr("src", url);
                   alert(url);
                }
                if(gid.indexOf("nt2_") !== -1)
                {
                   var at = gid.split("nt2_");
                   $("#net_code_2").val(at[1]);
                   $("#nt2_x").attr("src", "prepaid/" + at[1] +".jpg");
                   alert($("#net_code_2").val());
                }
                if(gid.indexOf("nt3_") !== -1)
                {
                   var at = gid.split("nt3_");
                   $("#net_code_3").val(at[1]);
                   $("#nt3_x").attr("src", "prepaid/" + at[1] +".jpg");
                   alert($("#net_code_3").val());
                }
               
         
            });
        });
    </script>
</body>

</html>