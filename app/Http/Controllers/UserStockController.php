<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserPaymentDetails;
use App\Models\TransactionAllDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;

use App\Models\UserBankTransferDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;

use PDF;
use EXCEL;

class UserStockController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('user.userstock', ['user' => $ob]);
    		
    }

    public function index_distributor(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('user.userstock_distributor', ['user' => $ob]);
    		
    }

    public function index_retailer(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.stock', ['user' => $ob]);
    		
    }

    public function index_apipartner(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('user.userstock_apipartner', ['user' => $ob]);
    		
    }

    
    public function viewdate(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        
        $d1 = $this->getDistributorRecharge($ob->user, $f_date, $t_date);
        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $o_sales = $this->getDistributorRechargeOpeningSales($ob->user, "2018-10-01 00:00:00", $f_date);

        $c_sales = $this->getDistributorRechargeOpeningSales($ob->user, $f_date, $t_date);

        $o_bill = $this->getDistributorBill($ob->user, "2018-10-01 00:00:00", $f_date);

        $o_bill_re = $this->getDistributorBill($ob->user, $f_date, $t_date);
        
        $o_balance = floatval($o_balance) - (floatval($o_sales) + floatval($o_bill));

        // Manual Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('user.userstock_view', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 'bill' => $o_bill_re, 'ce_tot' => $c_sales, 'network' => $d2, 'user' => $ob]); 

        
    }

    public function viewdate_distributor(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        
        $d1 = $this->getDistributorRecharge($ob->user, $f_date, $t_date);
        
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $o_sales = $this->getDistributorRechargeOpeningSales($ob->user, "2018-10-01 00:00:00", $f_date);

        $c_sales = $this->getDistributorRechargeOpeningSales($ob->user, $f_date, $t_date);

        $o_bill = $this->getDistributorBill($ob->user, "2018-10-01 00:00:00", $f_date);

        $o_bill_re = $this->getDistributorBill($ob->user, $f_date, $t_date);
        
        $o_balance = floatval($o_balance) - (floatval($o_sales) + floatval($o_bill));

        // Manual Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('user.userstock_view_distributor', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 'bill' => $o_bill_re,  'ce_tot' => $c_sales, 'network' => $d2, 'user' => $ob]); 

        
    }

    public function viewdate_retailer(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Opening Balanace
        $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob->user);
        $o_balance = Stock::stockCustomOpeningBalance($ob->user);
        $o_balance = floatval($o_balance) + floatval($o_balance1);
        
        $o_sales_re = Stock::getRetailerRechargeOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_sales_eb = Stock::getRetailerBill($ob->user, "2018-10-01 00:00:00", $f_date);
        $o_sales_mo = Stock::getMoneyOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_sales_mv = Stock::getMoneyVerifyOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));

        $old_re = Stock::getRetailerOldRecharge($ob->user);
        
        // current sales...
        $c_sales_re = Stock::getRetailerRechargeOpeningSales($ob->user, $f_date, $t_date);
        $c_sales_eb = Stock::getRetailerBill($ob->user, $f_date, $t_date);
        $c_sales_mo = Stock::getMoneyOpeningSales($ob->user, $f_date, $t_date);
        $c_sales_mv = Stock::getMoneyVerifyOpeningSales($ob->user, $f_date, $t_date);

        $data = Stock::stockData($ob->user, $f_date, $t_date);
        $d1 = Stock::getRetailerRecharge($ob->user, $f_date, $t_date);
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        // Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('retailer.stock_view', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 
                                            'network' => $d2, 'bill' => $c_sales_eb, 'ce_tot' => $c_sales_re, 
                                            'money' => $c_sales_mo, 'mverify' => $c_sales_mv, 'old_re' => $old_re, 'user' => $ob]); 

        
    }

    public function viewdate_retailer_excel(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
       

        $o_sales = $this->getRetailerRechargeOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);

        $rech = $this->getRetailerRechargeOpeningSales($ob->user, $f_date, $t_date);

        $o_bill = $this->getRetailerBill($ob->user, "2018-10-01 00:00:00", $f_date);

        $bill = $this->getRetailerBill($ob->user, $f_date, $t_date);

        $o_balance = floatval($o_balance) - (floatval($o_sales) + floatval($o_bill));

        $headings = ['NO', 'DATE', 'USER', 'MODE', 'PURCHASE', 'SALES', 'BALANCE']; 

        $j = 1;

        $content = [];

        $k = 0;

        $user = $ob;                          
        $debit = 0;
        $credit = 0;
        $balance = 0;
        if($o_balance != 0)
        {
            $balance = floatval($balance) + floatval($o_balance);

            $content[$k++] = ['', '', 'OPENING BALANCE', '', '', '', $balance]; 

           
        }
        foreach($data as $f)
        {
            if($f->user_name == $user->user && $f->trans_status == 1)
            {
                // Debit +
                $debit = floatval($debit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) + floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->grant_user_name, $f->payment_mode, 
                                    $f->grant_user_amount, '', $balance]; 
                
                
            }
            else if($f->grant_user_name == $user->user && $f->trans_status == 1)
            {
                // Credit -
                $credit = floatval($credit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) - floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->user_name, $f->payment_mode, 
                                    '', $f->grant_user_amount, $balance]; 
                
            }
            $j++;
        
        }

        if($bill != '0')
        {
            $credit = floatval($credit) + floatval($bill);
            $balance = floatval($balance) - floatval($bill);

            $content[$k++] = [$j, '', 'EBBILL RECHARGE', '',  '', $bill, $balance]; 
            
            $j++;
        
        }

        $credit = floatval($credit) + floatval($rech);
        $balance = floatval($balance) - floatval($rech);
        $content[$k++] = [$j, '', 'RECHARGE', '',  '', $rech, $balance]; 
       
        $cc = [$headings, $content];

        $tit = "User_stock_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');

    }

    public function viewdate_apipartner(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);
        

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('trans_status', '=', 1)
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
        $d1 = $this->getRetailerRecharge($ob->user, $f_date, $t_date);

        $o_sales = $this->getRetailerRechargeOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);

        $c_sales = $this->getRetailerRechargeOpeningSales($ob->user, $f_date, $t_date);

        $o_bill = $this->getRetailerBill($ob->user, "2018-10-01 00:00:00", $f_date);

        $o_bill_re = $this->getRetailerBill($ob->user, $f_date, $t_date);

        $o_balance = floatval($o_balance) - (floatval($o_sales) + floatval($o_bill));

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        // Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('user.userstock_view_apipartner', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 'network' => $d2, 'bill' => $o_bill_re, 'ce_tot' => $c_sales, 'user' => $ob]); 

        
    }

    public function viewdate_apipartner_excel(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $o_balance = $this->stockCustomOpeningBalance($ob, $upay_1);

        $o_balance1 = $this->stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob, $upay_1);

        $o_balance = floatval($o_balance) + floatval($o_balance1);

        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
                        
       
        $o_sales = $this->getApipartnerRechargeOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);

        $rech = $this->getApipartnerRechargeOpeningSales($ob->user, $f_date, $t_date);

        $o_bill = $this->getRetailerBill($ob->user, "2018-10-01 00:00:00", $f_date);

        $bill = $this->getRetailerBill($ob->user, $f_date, $t_date);

        $o_balance = floatval($o_balance) - (floatval($o_sales) + floatval($o_bill));

        $d2 = $net_2->select('net_code','net_name')->get();

        
        $headings = ['NO', 'DATE', 'USER', 'MODE', 'PURCHASE', 'SALES', 'BALANCE']; 

        $j = 1;

        $content = [];

        $k = 0;

        $user = $ob;                          
        $debit = 0;
        $credit = 0;
        $balance = 0;
        if($o_balance != 0)
        {
            $balance = floatval($balance) + floatval($o_balance);

            $content[$k++] = ['', '', 'OPENING BALANCE', '', '', '', $balance]; 

           
        }
        foreach($data as $f)
        {
            if($f->user_name == $user->user && $f->trans_status == 1)
            {
                // Debit +
                $debit = floatval($debit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) + floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->grant_user_name, $f->payment_mode, 
                                    $f->grant_user_amount, '', $balance]; 
                
                
            }
            else if($f->grant_user_name == $user->user && $f->trans_status == 1)
            {
                // Credit -
                $credit = floatval($credit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) - floatval($f->grant_user_amount);

                $content[$k++] = [$j, $f->grant_date, $f->user_name, $f->payment_mode, 
                                    '', $f->grant_user_amount, $balance]; 
                
            }
            $j++;
        
        }

        if($bill != '0')
        {
            $credit = floatval($credit) + floatval($bill);
            $balance = floatval($balance) - floatval($bill);

            $content[$k++] = [$j, '', 'EBBILL RECHARGE', '',  '', $bill, $balance]; 
            
            $j++;
        
        }

        $credit = floatval($credit) + floatval($rech);
        $balance = floatval($balance) - floatval($rech);
        $content[$k++] = [$j, '', 'RECHARGE', '',  '', $rech, $balance]; 
       
        $cc = [$headings, $content];

        $tit = $ob->user."_stock_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
    }

    public function stockCustomOpeningBalance($ob, $upay_1)
    {
        
        $d1 = $upay_1->select('grant_user_amount')->where('user_name', '=', $ob->user)
                        ->where('payment_mode', '=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)->get();
                        
        
        $balance = 0;

        if($d1->count() > 0)
        {
            $balance = $d1[0]->grant_user_amount;
        }
        
        return $balance;
    }

    public function stockOpeningBalance($f_date, $t_date, $ob, $upay_1)
    {
        
        $data = $upay_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_name', '=', $ob->user)
                            ->orWhere('grant_user_name', '=', $ob->user);
                        })
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')
                        ->where('trans_status', '=', 1)
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->orderBy('id', 'asc')->get();
        
        $balance = 0;

        foreach($data as $f)
        {
            if($f->user_name == $ob->user && $f->trans_status == 1)
            {
                // Debit +
                $balance = floatval($balance) + floatval($f->grant_user_amount);
            }
            else if($f->grant_user_name == $ob->user && $f->trans_status == 1)
            {
                // Credit -
                $balance = floatval($balance) - floatval($f->grant_user_amount);
            }
        }
        
        return $balance;
    }

    // RECHARGE DETAILS
    public function getRetailerRecharge($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewDetails;

        $d1 = $rech_n_1->with(['recharge2'])->where('user_name', '=', $user_name)
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->orderBy('id', 'asc')->paginate(15); 
        
        
        return $d1;
        
    }

    public function getRetailerBill($user_name, $f_date, $t_date)
    {
       $rech_2 = new UserRechargeBillDetails;
      
       
       $re_tot = 0;
      

       $re_tot = $rech_2->whereBetween('created_at', [$f_date, $t_date])
                           ->where('user_name', '=', $user_name)
                           ->where('con_status', '=', 'SUCCESS')
                           ->sum('con_total'); 
       
      
       
       $re_tot = round($re_tot, 2);
        
       return $re_tot;
        
    }

    public function getRetailerRechargeOpeningSales($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewDetails;
        $ce_tot = 0;
        
        $user_name = strtoupper($user_name);

        $ce_tot = $rech_n_1->with(['recharge2'])->whereRaw('upper(user_name) = ?',[$user_name])
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->where('rech_status', '=', 'SUCCESS')
                        ->sum('rech_total'); 
        
        
        return $ce_tot;
        
    }

    public function getMoneyOpeningSales($user_name, $f_date, $t_date)
    {
        $bank_1 = new UserBankTransferDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_1->whereBetween('mt_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('mt_status', '=', 'SUCCESS')
                                ->sum('mt_total');

        
        
        return $ce_tot;
        
    }

    public function getMoneyVerifyOpeningSales($user_name, $f_date, $t_date)
    {
        $bank_2 = new UserBeneficiaryAccVerifyDetails;
        $ce_tot = 0;

        $user_name = strtoupper($user_name);
        
        $ce_tot = $bank_2->whereBetween('ben_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$user_name])
                                ->where('ben_status', '=', 'SUCCESS')
                                ->sum('ben_surplus');          

        
        
        return $ce_tot;
        
    }

    //DISTRIBUTOR RECHARGE DETAILS
    public function getDistributorRecharge($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;

        $d1 = $rech_n_1->with(['newparentrecharge2'])->where('parent_name', '=', $user_name)
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->orderBy('id', 'asc')->paginate(30); 
        
        
        return $d1;
        
        
    }

    public function getDistributorBill($user_name, $f_date, $t_date)
     {
        $rech_2 = new UserRechargePaymentParentDetails;
       
        
        $re_tot = 0;
       
 
        $re_tot = $rech_2->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(parent_name) = ?',[$user_name])
                            ->where('super_parent_name', '=', 'NONE')
                            ->where('rech_type', '=', 'BILL_PAYMENT')
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 
        
       
        
        $re_tot = round($re_tot, 2);
         
        return $re_tot;
         
     }

    public function getDistributorRechargeOpeningSales($user_name, $f_date, $t_date)
    {
        $rech_n_1 = new UserRechargeNewParentDetails;
        $ce_tot = 0;
        
        $ce_tot = $rech_n_1->with(['newparentrecharge2'])
                        ->where('parent_name', '=', $user_name)
                        ->where('rech_status', '=', 'SUCCESS')
                        ->whereBetween('created_at', [$f_date, $t_date]) 
                        ->sum('rech_total'); 

        return $ce_tot;
        
    }

     //DISTRIBUTOR RECHARGE DETAILS
     public function getSuperDistributorRecharge($user_name, $f_date, $t_date)
     {
         $tran_1 = new TransactionAllParentDetails;
         
 
         $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                 ->with(['websuperparentrecharge1', 'websuperparentrecharge2', 'websuperparentapirecharge1', 'websuperparentapirecharge2'])
                 ->whereBetween('created_at', [$f_date, $t_date])
                 ->where('user_name', '=', $user_name)
                 ->orderBy('id', 'asc')->get(); 
        
        $d2 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
            ->with(['websuperparentrecharge1', 'websuperparentrecharge2', 'websuperparentapirecharge1', 'websuperparentapirecharge2'])
            ->whereBetween('created_at', [$f_date, $t_date])
            ->where('user_name', '=', $user_name)
            ->orderBy('id', 'asc')->paginate(30); 

         
         return array($d1, $d2);
         
     }

     public function getSuperDistributorBill($user_name, $f_date, $t_date)
     {
        $rech_2 = new UserRechargePaymentParentDetails;
        $rech_a_2 = new UserRechargePaymentParentApipartnerDetails;
        
        $re_tot = 0;
        $ae_tot = 0;
        $ne_tot = 0;
 
        $re_tot = $rech_2->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(super_parent_name) = ?',[$user_name])
                            ->where('rech_type', '=', 'BILL_PAYMENT')
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 
        
        $ae_tot = $rech_a_2->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(parent_name) = ?',[$user_name])
                            ->where('rech_type', '=', 'BILL_PAYMENT')
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 
        
        $re_tot = round($re_tot, 2);
        $ae_tot = round($ae_tot, 2);

        $ne_tot = floatval($re_tot) + floatval($ae_tot);

        $ne_tot = round($ne_tot, 2);

         
         return $ne_tot;
         
     }

     

    public function getSuperDistributorRechargeOpeningSales($user_name, $f_date, $t_date)
    {
        $tran_1 = new TransactionAllParentDetails;
        $ce_tot = 0;
        

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['websuperparentrecharge1', 'websuperparentrecharge2', 'websuperparentapirecharge1', 'websuperparentapirecharge2'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->where('user_name', '=', $user_name)
                ->orderBy('id', 'asc')->get(); 
        
        foreach($d1 as $d)
        {
            
            if($d->trans_type == "WEB_RECHARGE" )
            {
                if($d->trans_option == 1 && $d->websuperparentrecharge1[0]->rech_status == "SUCCESS")
                {
                    $ce_tot = floatval($ce_tot) + floatval($d->websuperparentrecharge1[0]->rech_total);
                }
               
            }
            else if($d->trans_type == "API_RECHARGE" )
            {
                if($d->trans_option == 1 && $d->websuperparentapirecharge1[0]->rech_status == "SUCCESS")
                {
                    $ce_tot = floatval($ce_tot) + floatval($d->websuperparentapirecharge1[0]->rech_total);
                }
            }
                                            
        }

        return $ce_tot;
        
    }


    //API PARTNER RECHARGE STOCK DETAILS
    public function getApipartnerRecharge($user_name, $f_date, $t_date)
    {
        $tran_1 = new TransactionAllDetails;
        

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['apirecharge1', 'apirecharge2'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->where('user_name', '=', $user_name)
                ->where('trans_type', '=', 'API_RECHARGE')
                ->orderBy('id', 'asc')->paginate(30); 
        
        $d2 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
            ->with(['apirecharge1', 'apirecharge2'])
            ->whereBetween('created_at', [$f_date, $t_date])
            ->where('user_name', '=', $user_name)
            ->where('trans_type', '=', 'API_RECHARGE')
            ->orderBy('id', 'asc')->get(); 
        
        return array($d1, $d2);
        
    }

    public function getApipartnerRechargeOpeningSales($user_name, $f_date, $t_date)
    {
        $tran_1 = new TransactionAllDetails;
        $ce_tot = 0;
        

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['apirecharge1', 'apirecharge2'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->where('user_name', '=', $user_name)
                ->where('trans_type', '=', 'API_RECHARGE')
                ->orderBy('id', 'asc')->get(); 
        
        
        foreach($d1 as $d)
        {
            
            if($d->trans_type == "API_RECHARGE")
            {
                if($d->trans_option == 1 && $d->apirecharge2->rech_status == "SUCCESS")
                {
                    $ce_tot = floatval($ce_tot) + floatval($d->apirecharge1[0]->rech_total);
                }
               
            }
                                            
        }

        return $ce_tot;
        
    }

}
