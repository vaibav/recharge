<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\GetCommon;
use App\Libraries\RechargeInfo;

use App\Models\NetworkDetails;
use App\Models\RechargeInfoDetails;

class ApipartnerInterfaceController extends Controller
{
    //
    public function store(Request $request)
	{
	    $ri = new RechargeInfoDetails;
	    
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $info_type = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;
        $date_time = date("Y-m-d H:i:s");

        $d1 = $request->all();
        $ip_add = $request->ip();
        $userAgent = request()->header('User-Agent');
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "pin")
            {
                $user_pwd = $value;
            }
            else if($key == "opr")
            {
                $operator = $value;
            }
            else if($key == "type")
            {
                $info_type = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
           
        }

        file_put_contents("server_ip_interface.txt", $ip_add."--".$userAgent."--".$user_name."--".$info_type."--".$user_mobile."--".$date_time.PHP_EOL, FILE_APPEND);

        $ob = GetCommon::getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;
        $user_stat = $ob->status;

        // User status
        if($user_stat == 0)
        {
            $z = 1;         //Invalid API Partner...
        }
        else if($user_stat == 3)
        {
            $z = 2;         //User is not API Partner...
        }
        else if($user_stat == 4)
        {
            $z = 3;         //Inactive API Partner...
        }

        // Get Network code
        $net_code = $this->getNetCode($operator);
        if($net_code == 0)
        {
            $z = 4;         //Invalid Network...
        }
        else  if($net_code == 1)
        {
            $z = 5;         //Inactive Network...
        }


        if($z == 0)
        {
            if (!is_numeric($user_mobile)) 
            {
                $z = 6;         //Mobile or EBCons No is not a valid no...
            }
        }

        if($z == 0)
        {
            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, $info_type);
            if($api_code == 0)
            {
                $z = 7;     //No Network Line is Selected...
                $op = "No plans Available in this Network...";
            }


            if($z == 0)
            {
                $api_url = RechargeInfo::generateInfoAPI($api_code, $net_code, $user_mobile);

                $client = new \GuzzleHttp\Client();
                $res = $client->get($api_url);
                $res_code = $res->getStatusCode();
                $op = json_decode($res->getBody());
                
                $ri->trans_id = "A".rand(10000,99999);
                $ri->user_name = $user_name;
                $ri->rech_type = $info_type;
                $ri->rech_mobile = $user_mobile;
                
                $ri->save();
            }
        }
        else if($z == 1)
        {
            $op = "Error! Invalid API Partner...";
        }
        else if($z == 2)
        {
            $op = "Error! User is not API Partner....";
        }
        else if($z == 3)
        {
            $op = "Error! Inactive API Partner....";
        }
        else if($z == 4)
        {
            $op = "Error! Invalid Network....";
        }
        else if($z == 5)
        {
            $op = "Error! Inactive Network...";
        }
        else if($z == 6)
        {
            $op = "Error! Mobile or EBCons No is not a valid no...";
        }
        else if($z == 7)
        {
            $op = "Error! No plans Available in this Network...";
        }

        return response()->json($op, 200);

    }





    /**
     * Get Net Code
     */
    public function getNetCode($net_short)
    {
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $net_status = 0;

        
        $d1 = $net_1->select('net_code', 'net_status')->where('net_short_code', '=', $net_short)->get();
        if($d1->count() > 0)
        {
           $net_code = $d1[0]->net_code;
           $net_status = $d1[0]->net_status;

           if($net_status != 1)
                $net_code = 1;
        }

        return $net_code;
    }

}
