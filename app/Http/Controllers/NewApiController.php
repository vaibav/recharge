<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;


use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\ApipartnerBrowserDetails;

use App\Libraries\RechargeMobile;
use App\Libraries\RechargeBill;
use App\Libraries\GetCommon;
use App\Libraries\ApiUrl;
use App\Libraries\RechargeNewMobile;

use App\Libraries\Transaction;

class NewApiController extends Controller
{
    //

    public function insertRecharge(Request $request)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $data = [];

        $user_name = "NONE";
        $user_pwd = "NONE";
        $operator = "NONE";
        $user_mobile = "";
        $user_amount = "";
        $user_trid = "";
        $user_ubal = 0;
        $net_ubal = 0;
        $date_time = date("Y-m-d H:i:s");

        $d1 = $request->all();
        $ip_add = $request->ip();
        $userAgent = request()->header('User-Agent');
        foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if($key == "uname")
            {
                $user_name = $value;
            }
            else if($key == "pin")
            {
                $user_pwd = $value;
            }
            else if($key == "opr")
            {
                $operator = $value;
            }
            else if($key == "mob")
            {
                $user_mobile = $value;
            }
            else if($key == "amt")
            {
                $user_amount = $value;
            }
            else if($key == "trid")
            {
                $user_trid = $value;
            }
        }

        //Store log
        $log_data = $user_name."^^^^^".$date_time."^^^^^API^^^^^".$user_mobile."^^^^^".$ip_add."^^^^^".$userAgent;
        file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);
         
        //file_put_contents("server_ip.txt", $ip_add."--".$userAgent."--".$user_name."--".$user_mobile."--".$user_amount."--".$date_time.PHP_EOL, FILE_APPEND);
        
        $ob = GetCommon::getUserAccountDetails($user_name, $user_pwd);
        $user_code = $ob->code;
        $user_ubal = $ob->ubal;
        $user_stat = $ob->status;

        list($net_code, $net_type_code, $net_status) = $this->getNetworkCode($operator);

        
        $zy = $this->isBrowserRequest($userAgent, $user_name);
        if($zy == 1)
        {
            $z = 16;        // Browser Request is not Allowed..
        }

        // User status
        if($user_stat == 0)
        {
            $z = 13;         //Invalid API Partner...
        }
        else if($user_stat == 3)
        {
            $z = 14;         //User is not API Partner...
        }
        else if($user_stat == 4)
        {
            $z = 15;         //Inactive API Partner...
        }
    

        // TNEB Recharge
        if($operator == "TNEB")
        {
            $z = 30;
        }

        
        // 0.Inactive Network
        if($net_status > 1 && $z == 0)
        {
            $z = 8;     //Inactive Network...
        }

        // Check Mobile no
        if($user_mobile == "" && $z == 0)
        {
            $z = 9;     //Empty Mobile No....
        }

        // Check Amount
        if($user_amount == "" && $z == 0)
        {
            $z = 10;     //Empty Amount....
        }

        // Check Transaction Id
        if($user_trid == "" && $z == 0)
        {
            $z = 11;     //Empty Transaction id....
        }

        if (!is_numeric($user_amount) && $z == 0) 
        {
            $z = 12;     //Amount is not a valid no...
        } 

        
        if($z == 0)
        {
            // Post Data
            $data = [];
            $data['net_type_code'] = $net_type_code;
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "API";
            $data['api_trans_id'] = $user_trid;

            //list($z1, $trans_id) = RechargeMobile::add($data);
            
            //$z1 = RechargeNewMobile::add($data);

            $z1 = Transaction::recharge_mobile($data);

            if($z1 == 0)
            {
                $op = "Recharge Request is Accepted..";
            }
            else if($z1 == 1)
            {  
                $op = "Failed! Low user Balance...";
            }
            else if($z1 == 2)
            {  
                $op = "Failed! No Network Line is Selected...";
            }
            else if($z1 == 3)
            {   
                $op = "Failed! Mode web is not Selected...";
            }
            else if($z1 == 4)
            {   
                $op = "Failed! Account is Inactive...";
            }
            else if($z1 == 5)
            {
                $op = "Failed! Account Balance is low from Setup Fee...";
            }
            else if($z1 == 6)
            {
                $op = "Failed! Recharge Status is already Pending for this Mobile No...";
            }
            else if($z1 == 7)
            {
                $op = "Failed! Wait for 10 Minutes...";
            }
            else if($z1 == 8)
            {
                $op = "Failed! Server is Temporarily shut down...";
            }

            $z = $z1;
        }
        else if($z == 8)
        {
            $op = "Failed! Inactive Network...";
        }
        else if($z == 9)
        {
            $op = "Failed! Empty Mobile No....";
        }
        else if($z == 10)
        {
            $op = "Failed! Empty Amount....";
        }
        else if($z == 11)
        {
            $op = "Failed! Empty Transaction id....";
        }
        else if($z == 12)
        {
            $op = "Failed! Amount is not a valid no...";
        }
        else if($z == 13)
        {
            $op = "Failed! Invalid API Partner...";
        }
        else if($z == 14)
        {
            $op = "Failed! User is not API Partner...";
        }
        else if($z == 15)
        {
            $op = "Failed! Your account is in Inactive State...";
        }
        else if($z == 16)
        {
            $op = "Failed! Browser Request is not Allowed...";
        }
        else if($z == 30)
        {
            // Bill Payment
            $dc = [];
            $dc['net_code'] = $net_code;
            $dc['cons_no'] = $user_mobile;
            $dc['cons_name'] = "API_NAME";
            $dc['cons_mobile'] = "9898989898";
            $dc['cons_amount'] = $user_amount;
            $dc['cons_duedate'] = "01/04/2019";
            $dc['code'] = $user_code;
            $dc['user'] = $user_name;
            $dc['ubal'] = $user_ubal;
            $dc['mode'] = "API";
            $dc['api_trans_id'] = $user_trid;

            $zb = RechargeBill::add($dc);

            if($zb == 0)
            {
                $op = "Bill Payment Request is Accepted...";
            }
            else if($zb == 1)
            {  
                $op = "Failed! Low user Balance...";
            }
            else if($zb == 2)
            {  
                $op = "Failed! No Web Mode is Selected...";
            }
            else if($zb == 3)
            {   
                $op = "Failed! This Account is locked...";
            }
            else if($zb == 4)
            {   
                $op = "Failed! This Account is below from Setup Fee...";
            }
            else if($zb == 5)
            {
                $op = "Failed! Recharge is already Pending for this Consumer No...";
            }
            else if($zb == 6)
            {
                $op = "Failed! Server is Temporarily shut down...";
            }
        }

        $net_ubal = $this->getUserBalance($user_name);

        if($z == 12)
        {
            $resultdata = array('status' => $op, 'balance' => '0');
        }
        else
        {
            $resultdata = array('status' => $op, 'balance' => $net_ubal);
        }
        
        
        return response()->json($resultdata, 200);
    }

    public function getUserBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $user_ubal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $user_ubal = $d->user_balance;
        }

        return $user_ubal;
    }

    public function getNetworkCode($net_short)
    {
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $net_type_code = 0;
        $net_status = 0;


        $d1 = $net_1->select('net_code', 'net_type_code', 'net_status')->where('net_short_code', '=', $net_short)->get();
        foreach($d1 as $d)
        {
            $net_code = $d->net_code;
            $net_type_code = $d->net_type_code;
            $net_status = $d->net_status;
        }

        return array($net_code, $net_type_code, $net_status);
    }

   
   
    public function isBrowserRequest($userAgent, $user_name)
    {
        $api_1 = new ApipartnerBrowserDetails;

        $browsers = ['Opera', 'Mozilla', 'Firefox', 'Chrome', 'Edge'];

        $isBrowser = 0;

        $z = 0;

        foreach($browsers as $browser){
            if(strpos($userAgent, $browser) !==  false){
            $isBrowser = 1;
            break;
            }
        }

        $d1 = $api_1->where('user_name', '=', $user_name)->get();

        if($d1->count() > 0)
        {
            $agent = $d1[0]->agent_type;

            if($agent == "APPLICATION" && $isBrowser == 1)
            {
                $z = 1;
            }

        }

        return $z;
    }

}