<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\UserChainDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkLineApipartnerDetails;
use App\Models\ApipartnerBrowserDetails;

use App\Models\UserPaymentDetails;
use App\Models\PaymentLedgerDetails;
use App\Models\UserComplaintDetails;
use App\Models\MobileUserDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;
use App\Models\BackupRechargeDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserBankTransferDetails;

use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;

use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;


class UserDeleteController extends Controller
{
    //
    public function delete(Request $request, $user_code)
	{
        $uacc_1 = new UserAccountDetails;
       
        $ob = GetCommon::getUserDetails($request);
        $date_time = date("Y-m-d H:i:s");
        
        $z = 0;
        
        $d1 = $uacc_1->where('user_code', '=', $user_code)->get();

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;
            $user_type = $d1[0]->user_type;

            if($user_name != "admin")
            {
                if($user_type == "RETAILER")
                {
                    if($user_name != "DELETE_RT")
                    {
                        $new_user_name = "DELETE_RT";
    
                        $this->RetailerDelete($user_name, $new_user_name, $user_type);
                       
                        $op = "User Account is Deleted Successfully....";
                                
                    }
                    else
                    {
                        $op = "This is Reserved User and Not Delete....";
                    }
                }
                else if($user_type == "DISTRIBUTOR")
                {
                    if($user_name != "DELETE_DS")
                    {
                        $new_user_name = "DELETE_DS";
                        $new_user_code = $this->getUserCode($new_user_name);
    
                        $this->DistributorDelete($user_name, $user_code, $new_user_name, $new_user_code, $user_type);
        
                        $op = "User Account is Deleted Successfully....";
                                
                    }
                    else
                    {
                        $op = "This is Reserved User and Not Delete....";
                    }
                }
                else if($user_type == "SUPER DISTRIBUTOR")
                {
                    if($user_name != "DELETE_SD")
                    {
                        $new_user_name = "DELETE_SD";
                        $new_user_code = $this->getUserCode($new_user_name);
    
                        $this->DistributorDelete($user_name, $user_code, $new_user_name, $new_user_code, $user_type);
                        
                        $op = "User Account is Deleted Successfully....";
                                
                    }
                    else
                    {
                        $op = "This is Reserved User and Not Delete....";
                    }
                }
                else if($user_type == "API PARTNER")
                {
                    if($user_name != "DELETE_AP")
                    {
                        $new_user_name = "DELETE_AP";
    
                        $this->RetailerDelete($user_name, $new_user_name, $user_type);
                        
                        $op = "User Account is Deleted Successfully....";
                                
                    }
                    else
                    {
                        $op = "This is Reserved User and Not Delete....";
                    }
                }
            }
            else
            {
                $op = "You cannot delete Admin account....";
            }
           
        }
        else
        {
            $op = "User Name is not Found....";
        }

        
        return redirect()->back()->with('msg', $op); 
        
		
    }

    public function RetailerDelete($user_name, $new_user_name, $user_type)
    {
        $upay_1 = new UserPaymentDetails;
        $pay_1 = new PaymentLedgerDetails;
        $ucom_1 = new UserComplaintDetails;
        $umob_1 = new MobileUserDetails;

        $uacc_1 = new UserAccountDetails;
        $uchn_1 = new UserChainDetails;
        $uacb_1 = new UserAccountBalanceDetails;
        $unet_1 = new UserNetworkDetails;
        $uper_1 = new UserPersonalDetails;

        $tran_1 = new TransactionAllDetails;
        $tran_2 = new TransactionAllParentDetails;
       
        $rech_n_1 = new UserRechargeNewDetails;
        $back_1 = new BackupRechargeDetails;
        $rech_e_1 = new UserRechargeBillDetails;
        $rech_b_1 = new UserBankTransferDetails;
        $rech_n_2 = new UserRechargeNewParentDetails;

        $rem_1 = new UserBankRemitterDetails;
        $ben_1 = new UserBeneficiaryDetails;
        $ben_2 = new UserBeneficiaryAccVerifyDetails;

        $net_2 = new NetworkLineApipartnerDetails;
        $api_1 = new ApipartnerBrowserDetails;

        $date_time = date("Y-m-d H:i:s");

        // Transfer Account Balance
        $bb = $this->transferAccountBalance($user_name, $new_user_name);


        // Change Payment Details
        $x = $upay_1->where('user_name', '=', $user_name)
            ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $upay_1->where('grant_user_name', '=', $user_name)
            ->update(['grant_user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        // Change Transaction Details
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "REMOTE_PAYMENT")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
 
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "FUND_TRANSFER")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "REFUND_PAYMENT")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "USER_RECHARGE")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        // Change Recharge Data
        $user_name_1 = strtoupper($user_name);

        $x = $rech_n_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $rech_e_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $rech_b_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $rech_n_2->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        
        // Change Transaction Data
        if($user_type == "RETAILER") {
            $x = $tran_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                        ->where('trans_type', '=', "WEB_RECHARGE")
                        ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        }
        else if($user_type == "API PARTNER") {
            $x = $tran_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                        ->where('trans_type', '=', "API_RECHARGE")
                        ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        }
       

        $x = $tran_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->where('trans_type', '=', "BILL_PAYMENT")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $tran_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->where('trans_type', '=', "BANK_TRANSFER")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        // Change Money Transfer Remitter, Beneficiary Details
        $x = $rem_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $ben_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $ben_2->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        if($user_type == "API PARTNER") {

            $x = $net_2->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

            $x = $api_1->where('user_name', '=', $user_name)->delete();
        }

        //Backup recharge 
        $n_user_name = $user_name."_OLD";

        $x = $back_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $n_user_name, 'updated_at'=>$date_time ]);


        // Change Payment Ledger - Collection 
        $x = $pay_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);


        // Delete User Account Data
        $g = $uchn_1->where('user_name', '=', $user_name)->delete();
        $h = $uacb_1->where('user_name', '=', $user_name)->delete();
        $i = $unet_1->where('user_name', '=', $user_name)->delete();
        $j = $uacc_1->where('user_name', '=', $user_name)->delete();
        $k = $uper_1->where('user_name', '=', $user_name)->delete();
        $l = $ucom_1->where('user_name', '=', $user_name)->delete();
        $m = $umob_1->where('user_name', '=', $user_name)->delete();

    }

    public function DistributorDelete($user_name, $user_code, $new_user_name, $new_user_code, $user_type)
    {
        $upay_1 = new UserPaymentDetails;
        $pay_1 = new PaymentLedgerDetails;
        $ucom_1 = new UserComplaintDetails;
        $umob_1 = new MobileUserDetails;

        $uacc_1 = new UserAccountDetails;
        $uchn_1 = new UserChainDetails;
        $uacb_1 = new UserAccountBalanceDetails;
        $unet_1 = new UserNetworkDetails;
        $uper_1 = new UserPersonalDetails;

        $tran_1 = new TransactionAllDetails;
        $tran_2 = new TransactionAllParentDetails;
       
        $rech_n_1 = new UserRechargeNewDetails;
        $rech_e_1 = new UserRechargeBillDetails;
        $rech_b_1 = new UserBankTransferDetails;
        $rech_n_2 = new UserRechargeNewParentDetails;

        $rem_1 = new UserBankRemitterDetails;
        $ben_1 = new UserBeneficiaryDetails;
        $ben_2 = new UserBeneficiaryAccVerifyDetails;

        $net_2 = new NetworkLineApipartnerDetails;
        $api_1 = new ApipartnerBrowserDetails;

        $date_time = date("Y-m-d H:i:s");

        // Transfer Account Balance
        $bb = $this->transferAccountBalance($user_name, $new_user_name);


        // Change Payment Details
        $x = $upay_1->where('user_name', '=', $user_name)
            ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        $x = $upay_1->where('grant_user_name', '=', $user_name)
            ->update(['grant_user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        // Change Transaction Details
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "REMOTE_PAYMENT")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
 
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "FUND_TRANSFER")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        
        $x = $tran_1->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "REFUND_PAYMENT")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
        
        $x = $tran_2->where('user_name', '=', $user_name)
                ->where('trans_type', '=', "USER_RECHARGE")
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);

        // Change Recharge Data
        $user_name_1 = strtoupper($user_name);

        $x = $rech_n_2->whereRaw('upper(parent_name) = ?',[$user_name_1])
                                        ->update(['parent_name' => $new_user_name, 'updated_at'=>$date_time ]);
                        
        $x = $rech_n_2->whereRaw('upper(child_name) = ?',[$user_name_1])
                                        ->update(['child_name' => $new_user_name, 'updated_at'=>$date_time ]);

        
        // Change Transaction Data
        $x = $tran_2->whereRaw('upper(user_name) = ?',[$user_name_1])
                        ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);
       

        // Change Child Details
        $y = $this->changeChildParent($user_name, $user_code, $new_user_name, $new_user_code, $user_type);

        
        // Change Payment Ledger - Collection 
        $x = $pay_1->whereRaw('upper(user_name) = ?',[$user_name_1])
                ->update(['user_name' => $new_user_name, 'updated_at'=>$date_time ]);


        // Delete User Account Data
        $g = $uchn_1->where('user_name', '=', $user_name)->delete();
        $h = $uacb_1->where('user_name', '=', $user_name)->delete();
        $i = $unet_1->where('user_name', '=', $user_name)->delete();
        $j = $uacc_1->where('user_name', '=', $user_name)->delete();
        $k = $uper_1->where('user_name', '=', $user_name)->delete();
        $l = $ucom_1->where('user_name', '=', $user_name)->delete();
        $m = $umob_1->where('user_name', '=', $user_name)->delete();

    }


    public function transferAccountBalance($user_name, $new_user_name)
    {
        $uacb_1 = new UserAccountBalanceDetails;

        $user_ubal = 0;

        $d2 = $uacb_1->where('user_name', '=', $user_name)->get();
        if($d2->count() > 0)
        {
            $user_ubal = $d2[0]->user_balance;

            if(floatval($user_ubal) > 0)
            {
                $bb = $this->UpdateAccountBalance($new_user_name, $user_ubal);
            }

        }
        
        return $user_ubal;
    }


    public function UpdateAccountBalance($new_user_name, $amt)
    {
        $uacb_1 = new UserAccountBalanceDetails;

        $d2 = $uacb_1->where('user_name', '=', $new_user_name)->get();
        if($d2->count() > 0)
        {
            $user_ubal = $d2[0]->user_balance;

            $user_ubal = floatval($user_ubal) + floatval($amt);

            $a = $uacb_1->where('user_name', '=', $new_user_name)
                                ->update(['user_balance' => $this->convertNumber($user_ubal) ]);
        }
        
        return 1;
    }

    public function getUserCode($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $user_code = 0;

        $d2 = $uacc_1->select('user_code')->where('user_name', '=', $user_name)->get();
        if($d2->count() > 0)
        {
            $user_code = $d2[0]->user_code;

        }
        
        return $user_code;
    }

    public function changeChildParent($old_user_name, $old_user_code, $new_user_name, $new_user_code, $new_user_type)
    {
        $uacc_1 = new UserAccountDetails;
        $uchn_1 = new UserChainDetails;
        $c_user_code = 0;
        $c_user_name = "";
        $z = 0;

        // Get New user-name chain value
        $user_chain = "";
        $user_level = 0;
        $d2 = $uchn_1->select('user_level', 'user_chain')->where('user_name', '=', $new_user_name)->get();
        if($d2->count() > 0)
        {
            $user_chain = $d2[0]->user_chain;
            $user_level = $d2[0]->user_level;
        }

        $d1 = $uacc_1->select('user_code', 'user_name')->where('parent_name', '=', $old_user_name)->get();
        foreach($d1 as $d)
        {
            $c_user_code = $d->user_code;
            $c_user_name = $d->user_name;

            if($user_chain != "")
            {
                $user_level_1 = intval($user_level) + 1;
                $user_chain_1 = $user_chain."-".$c_user_code;

                // Chain Update
                $uchn_1->where('user_code', '=', $c_user_code)->update(['parent_code' => $new_user_code, 
                                                                  'parent_name' => $new_user_name, 
                                                                  'user_level' => $user_level_1, 
                                                                  'user_chain' => $user_chain_1]);
                
                // Account Update
                $uacc_1->where('user_code', '=', $c_user_code)->update(['parent_code' => $new_user_code, 
                                                                  'parent_name' => $new_user_name, 
                                                                  'parent_type' => $new_user_type]);
                        
                $z++;
            }


        }  

        return $z;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
