<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\BankAgent;

use App\Models\UserBankAgentDetails;

class UserBankAgentController extends Controller
{
    //
    public function index(Request $request)
	{
          
        $ob = GetCommon::getUserDetails($request);

        return view('user.bank_agent_entry', ['user' => $ob]);
    
    }

    public function index_otp(Request $request)
	{
        $agen_1 = new UserBankAgentDetails;

        $ob = GetCommon::getUserDetails($request);

        //echo $ob->user;

        $d1 = $agen_1->where('user_name', '=', $ob->user)->where('agent_status', '=', 'OTP SUCCESS')->get();

        //print_r($d1);

        return view('user.bank_agent_otp_entry', ['user' => $ob, 'bank' => $d1]);
    
    }

    public function store(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

        $this->validate($request, ['agent_name' => 'required',
                                    'agent_cname' => 'required',
                                    'agent_address' => 'required',
                                    'agent_city' => 'required',
                                    'agent_pincode' => 'required',
                                    'agent_msisdn' => 'required'
                                ], 
                                ['agent_name.required' => ' Agent Name is required.',
                                'agent_cname.required' => ' Company Name is required.',
                                'agent_address.required' => ' Address is required.',
                                'agent_city.required' => ' City is required.',
                                'agent_pincode.required' => ' Pincode is required.',
                                'agent_msisdn.required' => ' Agent Mobile No is required.'
                                ]);


        $data = ['user_name' => $ob->user,
                'agent_name' => trim($request->agent_name),
                'agent_cname' => trim($request->agent_cname),
                'agent_address' => trim($request->agent_address),
                'agent_city' => trim($request->agent_city),
                'agent_state_code' => trim($request->agent_state_code),
                'agent_pincode' => trim($request->agent_pincode),
                'agent_msisdn' => trim($request->agent_msisdn),
                'r_mode' => "WEB"
                ];
        
        //file_put_contents("bank_details.txt", print_r($data, true));

        $z = BankAgent::add($data);

        if($z == 0)
            $op = "Entry is added Successfully...Please Enter OTP";
        else if($z == 1)
            $op = "Server is Temporariy Shutdown...";
        else if($z == 2)
            $op = "Mode web is not Selected...";
        else if($z == 3)
            $op = "Account is Inactivated...";
        else if($z == 4)
            $op = "Already Registered...";
        else if($z == 5)
            $op = "Error! Failure...";
        else if($z == 6)
            $op = "Wait! Pending....";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
	
    }

    public function store_otp(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

        $this->validate($request, ['otp' => 'required'], ['otp.required' => ' Otp is required.' ]);


        $data = ['user_name' => $ob->user,
                'agent_msisdn' => trim($request->agent_msisdn),
                'trans_id' => trim($request->trans_id),
                'otp' => trim($request->otp),
                'r_mode' => "WEB"
                ];
        
        file_put_contents("bank_details.txt", print_r($data, true));

        $z = BankAgent::add_otp($data);

        if($z == 0)
            $op = "Entry is added Successfully...";
        else if($z == 1)
            $op = "Server is Temporariy Shutdown...";
        else if($z == 2)
            $op = "Mode web is not Selected...";
        else if($z == 3)
            $op = "Account is Inactivated...";
        else if($z == 4)
            $op = "Already Registered...";
        else if($z == 5)
            $op = "Error! Failure...";
        else if($z == 6)
            $op = "Wait! Pending....";
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
	
    }

    public function send_otp(Request $request)
	{
              
        $ob = GetCommon::getUserDetails($request);

        $z = 0;
        $op = "";

       


        $data = ['user_name' => $ob->user,
                'agent_msisdn' => trim($request->agent_msisdn),
                'trans_id' => trim($request->trans_id),
                'otp' => trim($request->otp),
                'r_mode' => "WEB"
                ];
        

        list($z, $msg) = BankAgent::resend_otp($data);

        $z1 = 0;
        if($z == 10)
        {
            $op = $msg;
            $z1 = 10;
        } 
        else if($z == 1)
        {
            $op = "Server is Temporariy Shutdown...";
            $z1 = 11;
        }
        else if($z == 2) 
        {
            $op = "Mode web is not Selected...";
            $z1 = 12;
        }
        else if($z == 3)
        {
            $op = "Account is Inactivated...";
            $z1 = 13;
        }
        else if($z == 4)
        {
            $op = "Already Registered...";
            $z1 = 14;
        }
        else if($z == 5)
        {
            $op = $msg;
            $z1 = 15;
        }
        else if($z == 6)
        {
            $op = "Wait! Pending....";
            $z1 = 16;
        }
        else
        {
            $op = "Unable to send otp...";
            $z1 = 17;
        }
       
       

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z1]);
	
    }

    public function view_one(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $d1 = BankAgent::view_user($ob->user);

        $status = "PENDING";

        foreach($d1 as $d)
        {
            if($d->agent_status == "SUCCESS" || $d->agent_status == "OTP SUCCESS")
            {
                $status = "SUCCESS";
                break;
            }
        }

        return view('user.bank_agent_view', ['user' => $ob, 'data' =>$d1, 'status' => $status]);

    }

    public function view_all(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $d1 = BankAgent::view_all();

        return view('admin.bank_agent_view_all', ['user' => $ob, 'data' =>$d1]);

    }


}
