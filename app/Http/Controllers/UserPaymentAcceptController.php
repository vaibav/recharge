<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\SmsInterface;

use App\Models\UserPersonalDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\UserChainDetails;
use App\Models\TransactionAllDetails;

use App\Models\PaymentLedgerDetails;

class UserPaymentAcceptController extends Controller
{
    //
    public function index(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
            
        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        $data = $upay_1->select('trans_id','user_name', 'user_amount', 'user_req_date', 'payment_mode', 'user_remarks')
        ->where([['grant_user_name', '=', $user_name], ['trans_status', '=', 0]])->orderBy('id', 'desc')->get();

        return view('user.paymentaccept', ['pay1' => $data, 'user' => $ob]);
        
    }

    

    public function store($trans_id, $trans_amt, Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
        $user_code_grnt = $ob->code;
        $user_name_grnt = $ob->user;

        // get Data
        $user_name = "NONE";
        $date_time = date("Y-m-d H:i:s");
        $d1 = $upay_1->select('user_name')->where([['trans_id', '=', $trans_id], ['trans_status', '=', 0]])->get();
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;

             // Insert Data
            $cnt = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt > 0)
            {
                // Check Admin balance Amount
                $u_bal_1 = "";
                $u_bal_2 = "";
                $flg_1 = 0;
                $net_bal_1 = 0;
                $net_bal_2 = 0;

                $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name_grnt)->get();
                if($d1->count() > 0)
                {
                    $u_bal_1 = $d1[0]->user_balance;
                }

                $setup_fee = GetCommon::getSetupFee($user_name_grnt);
                if(floatval($setup_fee) >= floatval($u_bal_1))
                {
                    $flg_1 = 3;      //User Balance is less than setup Fee...
                }

                if($u_bal_1 != "" && $flg_1 == 0)
                {
                    if(floatval($u_bal_1) < floatval($trans_amt))
                    {
                        $flg_1 = 1;     // Granted user has less Amount...
                    }
                    else
                    {
                        // Deduct Amount from user name...
                        $net_bal_1 = floatval($u_bal_1) - floatval($trans_amt);

                        $net_bal_1 = $this->convertNumber($net_bal_1);
                                                
                        $uacc_1->where('user_name', '=', $user_name_grnt)
                                ->update(['user_balance'=>$net_bal_1, 'updated_at'=>$date_time]);
                    }
                }
                else
                {
                    $flg_1 = 2;         // Granted User has No Amount...(No Entry..)
                }
                

                if($flg_1 == 0)
                {
                    // Add amount to user balance Account
                    $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    foreach($d2 as $d)
                    {
                        $u_bal_2 = $d->user_balance;
                    }

                    if($u_bal_2 != "")
                    {
                        // Add Amount to user name...
                        $net_bal_2 = floatval($u_bal_2) + floatval($trans_amt);

                        $net_bal_2 = $this->convertNumber($net_bal_2);
                        
                        $uacc_1->where('user_name', '=', $user_name)
                                    ->update(['user_balance'=>$net_bal_2, 'updated_at'=>$date_time]);
                    }
                    else
                    {
                        $net_bal_2 = floatval($trans_amt);         // Granted User has No Amount (No Entry...Insert It..)

                        $net_bal_2 = $this->convertNumber($net_bal_2);
                        
                        $uacc_1->user_code = $user_code;
                        $uacc_1->user_name = $user_name;
                        $uacc_1->user_balance = $net_bal_2;
                        $uacc_1->save();
                    }

                    $payment_mode = "FUND_TRANSFER";

                    // Update Amount Transfer Details....
                    $upay_1->where('trans_id', '=', $trans_id)
                                    ->update(['grant_user_name'=>$user_name_grnt, 
                                            'grant_user_amount' => $this->convertNumber($trans_amt),
                                            'grant_date' => $date_time,
                                            'trans_status' => 1,
                                            'updated_at'=>$date_time]);
                    
                    
                     // Payment Ledger
                    $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name, 'pay_type' => "C",
                    'pay_amount' => $this->convertNumber($trans_amt), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                    'from_user' => $user_name_grnt, 'pay_date' => $date_time, 
                    'agent_name' => "-", 'pay_remarks' => $payment_mode, 'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];
    
                    $upay_2->insert($ar1);


                    // Transaction Details.....
                    $tran_1->trans_id = $trans_id;
                    $tran_1->user_name = $user_name;
                    $tran_1->trans_type = $payment_mode;
                    $tran_1->trans_option = '1';
                    
                    $tran_1->save();

                    $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
                    if($dx2->count() > 0)
                    {
                        $msg = "Amount ".$this->convertNumber($trans_amt)." is transferred successfully at ".date('d/m/Y H:i:s');
                        SmsInterface::callSms($user_name, $dx2[0]->user_mobile, $msg);
                    }

                    $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Amount is transferred successfully...</label>";
                }
                else if($flg_1 == 1)
                {
                    $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your Account Balance is Low...</label>";
                }
                else if($flg_1 == 2)
                {
                    $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your have no Account Balance...</label>";
                }   
                else if($flg_1 == 3)
                {
                    $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your have Low Account Balance in SetupFee...</label>";
                }   
            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! User Name does not Exists...</label>";
            }
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! This transaction is already finished...</label>";
        }
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function delete($trans_id, Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
        $user_code_grnt = $ob->code;
        $user_name_grnt = $ob->user;

        // get Data
        $user_name = "NONE";
        $date_time = date("Y-m-d H:i:s");
        $d1 = $upay_1->select('user_name')->where([['trans_id', '=', $trans_id], ['trans_status', '=', 0]])->get();
        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;

             // Insert Data
            $cnt = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt > 0)
            {
                // Check Admin balance Amount
                $u_bal_1 = "";
                $u_bal_2 = "";
                $flg_1 = 0;
                $net_bal_1 = 0;
                $net_bal_2 = 0;
                $trans_amt = 0;
              
                $payment_mode = "FUND_TRANSFER";

                // Update Amount Transfer Details....
                $upay_1->where('trans_id', '=', $trans_id)
                                ->update(['grant_user_name'=>$user_name_grnt, 
                                        'grant_user_amount' => $this->convertNumber($trans_amt),
                                        'grant_date' => $date_time,
                                        'trans_status' => 2,
                                        'updated_at'=>$date_time]);
                


                // Transaction Details.....
               /* $tran_1->trans_id = $trans_id;
                $tran_1->user_name = $user_name;
                $tran_1->trans_type = $payment_mode;
                
                $tran_1->save();*/

                $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Transaction is cancelled successfully...</label>";
            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! User Name does not Exists...</label>";
            }
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! This transaction is already finished...</label>";
        }
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

   
}
