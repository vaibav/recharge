<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\AllTransaction;
use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;

class RT_Pay_Req_Controller extends Controller
{
    //
    public function index(Request $request)
    {
          
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('user_name', '=', $ob->user)
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                    ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                    ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                                })->orderBy('id', 'desc')->limit(6)->get();

        return view('retailer.nw_pay_request', ['user' => $ob, 'pay' => $d1]);
        
    }


    public function store(Request $request)
    {
        
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
        $user_code_req = $ob->code;
        $user_name_req = $ob->user;
        $z = 0;
        $op = "";
 

        // Get Parent User Name
        $user_name_grnt = "None";
        $d1 = UserAccountDetails::select('parent_name')->where('user_name', '=', $user_name_req)->get();
        if($d1->count() > 0)
        {
            $user_name_grnt = $d1[0]->parent_name;
        }

        $data['trans_id'] = rand(100000,999999);
        $data['user_name_req'] = $user_name_req;
        $data['user_name_grnt'] = $user_name_grnt;
        $data['user_amount'] = trim($request->user_amount);
        $data['user_remarks'] = trim($request->user_remarks);
        $data['payment_mode'] = "FUND_TRANSFER";
        $date_time = date("Y-m-d H:i:s");

        $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

        $net_code = '0';
        if($d1->count() > 0) {
            $net_code = $d1[0]->net_code;
        }

        $data['net_code'] = $net_code;

        list($status, $op) = Stock::PaymentRequest($data);

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);
    }

    
}
