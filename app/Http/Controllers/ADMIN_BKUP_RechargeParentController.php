<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use DB;

use App\Models\UserRechargeNewParentDetails;
use App\Models\BackupRechargeParentDetails;

use App\Models\UserAccountDetails;
use App\Models\UserOldAccountDetails;

class ADMIN_BKUP_RechargeParentController extends Controller
{
    //
    public function store(Request $request)
	{
        $rech_n_1 = new UserRechargeNewParentDetails;
        $back_1 = new BackupRechargeParentDetails;
        
        // Post Data
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $date_time = date('Y-m-d H:i:s');

        if($f_date != "" && $t_date != "")
        {
            
            $d1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])->get();
                          
            $j = 1;
            foreach($d1 as $d)
            {
                $trans_id = $d->trans_id;
                $parent_name = $d->parent_name;
                $child_name = $d->child_name;
                $user_name = $d->user_name;
                $net_code = $d->net_code;
                $rech_mobile = $d->rech_mobile;
                $rech_amount = $d->rech_amount;
                $rech_total = $d->rech_total;
                $u_bal = $d->user_balance;
                $rech_date = $d->rech_date;
                $rech_status = $d->rech_status;
                $rech_type = $d->rech_type;

                if($rech_status == "SUCCESS")
                {
                    $cnt = $back_1->where('trans_id', '=', $trans_id)->where('parent_name', '=', $parent_name)->count();

                    if($cnt == 0)
                    {
                        $data = [ 
                            'trans_id' => $trans_id,
                            'parent_name' => $parent_name,
                            'child_name' => $child_name,
                            'user_name' => $user_name,
                            'net_code' => $net_code,
                            'rech_mobile' => $rech_mobile,
                            'rech_amount' => $rech_amount,
                            'rech_total' => $rech_total,
                            'user_balance' => $u_bal,
                            'rech_date' => $rech_date,
                            'rech_status' => $rech_status,
                            'rech_type' => $rech_type,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        ];
    
                        $back_1->insert($data);

                        echo $j."----".$trans_id."----".$parent_name."---".$child_name."---".$user_name."----".$net_code."----";
                        echo $rech_mobile."---".$rech_amount."---".$rech_total."----".$u_bal."----".$rech_date."----";
                        echo $rech_status."---".$rech_type."<br>";
                        $j++;
                    }


                    
                }
             
               
            }
        
        }

       
    }

    public function add_old(Request $request)
	{
        $u_old = new UserOldAccountDetails;
        $back_1 = new BackupRechargeParentDetails;
        
        // Post Data
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $date_time = date('Y-m-d H:i:s');

        $dx = UserAccountDetails::select('user_name', 'user_type')
                ->where('user_type', '=', 'DISTRIBUTOR')
                ->orWhere('user_type', '=', 'SUPER DISTRIBUTOR')
                ->orderBy('id', 'asc')->get(); 

        $j = 1;
        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>SUCCESS RECHARGE</th></tr>";
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $ce_tot = 0;
           
            $ux_name =strtoupper($ux_name);


            $ce_tot = UserRechargeNewParentDetails::whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(parent_name) = ?',[$ux_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 

         
            $ce_tot = round($ce_tot, 2);

            if($ce_tot != 0)
            {
                
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $j . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_name . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_type . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($ce_tot, 2, ".", "") . "</td></tr>";
            
                $j++;
                $cnt = $u_old->where('user_name', '=', $ux_name)->where('from_date', '=', $f_date)->where('too_date', '=', $t_date)->count();

                if($cnt == 0)
                {
                    $data = [ 
                        'trans_id' => rand(10000,99999),
                        'user_name' => $ux_name,
                        'from_date' => $f_date,
                        'too_date' => $t_date,
                        'rech_type' => "RECHARGE",
                        'user_amount' => $ce_tot,
                        'created_at' => $date_time,
                        'updated_at' => $date_time
                    ];

                    $u_old->insert($data);
                }
            
            }
            
        }

        echo $str;
                

        
       
    }
}
