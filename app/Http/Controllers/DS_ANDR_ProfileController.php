<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Libraries\GetMobileCommon;

use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\NetworkSurplusDetails;

class DS_ANDR_ProfileController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $user_1->with(['account'])->where('user_name', '=', $user_name)->get();
            
            foreach($d1 as $d)
            {
                array_push($data, ['user_code' => $d->user_code, 
                                    'user_per_name' => $d->user_per_name,
                                    'user_city' => $d->user_city,
                                    'user_mobile' => $d->user_mobile,
                                    'user_setup_fee' => $d->account->user_setup_fee,
                                    'user_type' => $d->account->user_type,
                                    'parent_name' => $d->account->parent_name,
                                    'user_rec_mode' => $d->account->user_rec_mode,
                                    'user_api_url_1' => $d->account->user_api_url_1,
                                    'user_api_url_2' => $d->account->user_api_url_2 ]);
                                               
            }
            

            return view('android.ds_profile_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'profile' => $data]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function percentage(Request $request)
	{
        $user_3 = new UserNetworkDetails;
        $net_1 = new NetworkDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $user_3->where('user_name', '=', $user_name)->get();
            $d2 = $net_1->get();
            
            return view('android.ds_profile_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'profile' => $d1, 'network' => $d2]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function changePassword(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
                      
            return view('android.ds_password_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function changePasswordStore(Request $request)
	{
        $user_2 = new UserAccountDetails;

        $auth_token = trim($request->auth_token);
        $new_pwd = trim($request->new_pwd);

        $z1 = 0;
        $op = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $password = bcrypt($new_pwd);
            $a = $user_2->where('user_name', '=', $user_name)->update(['user_pwd' => $password]);
                      
            if($a > 0)
                $op = 1;
            
        }
        else if($z1 == 1)
        {
            $op = 2;
        }
        else if($z1 == 2)
        {
            $op = 3;
        }

        return redirect()->back()->with('msg', $op);
    }
}
