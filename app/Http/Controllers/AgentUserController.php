<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\AdminOfferDetails;
use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;

class AgentUserController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_2 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_3 = new UserNetworkDetails;
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;
        
        $ob = GetCommon::getUserDetails($request);

        $rs = [];
        $user_code = $ob->code;
       

        $j = 0;
        $data = $user_1->with(['account', 'balance'])->where('user_code', '=', $ob->code)->get();
        $rs[$j][0] = $data;
        $rs[$j][1] = 0;
        $j++;

        return view('agent.user_view', ['user' => $ob, 'user1' => $rs]);
        		
    }

    public function viewone_user($user_code, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        
        $data = $user_1->with(['account'])->where('user_code', '=', $user_code)->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('agent.user_view_one', ['user1' => $data, 'user' => $ob]);
    }

}
