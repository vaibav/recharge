<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class DS_ANDR_Agent_CverifyController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_2 = new UserAccountDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $user_2->with(['personal'])->where('user_type', '=', "AGENT")->where('parent_name', '=', $user_name)->get();

            return view('android.ds_agent_collection_verify_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'agent_details' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $agent_name = trim($request->agent_name);
        $agent_name = strtoupper($agent_name);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $f_date = $date_1." 00:00:00";
            $t_date = $date_2." 23:59:59";
            
            $d1 = $pay_1->whereRaw('upper(agent_name) = ?',[$agent_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '0')
                        ->get(); 

            return view('android.ds_agent_collection_verify_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'collection' => $d1, 'agent_name' => $agent_name]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function successupdate(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);

        $z1 = 0;
        $date_time = date("Y-m-d H:i:s");

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $pay_1->where('trans_id', '=', $trans_id)->get(); 
        
            if($d1->count() > 0)
            {
                $pay_1->where('trans_id', '=', $trans_id)->update(['pay_status' => '1', 'updated_at' => $date_time]);
                    
                $op = "Success Update is executed successfully...";

            }
            else
            {
                $op = "Not Updated...";
            }
 
        }
        else if($z1 == 1)
        {
            $op = "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
        }

        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function failureupdate(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);

        $z1 = 0;
        $date_time = date("Y-m-d H:i:s");

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $pay_1->where('trans_id', '=', $trans_id)->get(); 
        
            if($d1->count() > 0)
            {
                $pay_1->where('trans_id', '=', $trans_id)->update(['pay_status' => '2', 'updated_at' => $date_time]);
                    
                $op = "Failure Update is executed successfully...";

            }
            else
            {
                $op = "Not Updated...";
            }
 
        }
        else if($z1 == 1)
        {
            $op = "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
        }

        return redirect()->back()->with('result', [ 'output' => $op]);
    }
}
