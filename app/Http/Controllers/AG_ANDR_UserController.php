<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\NetworkSurplusDetails;

class AG_ANDR_UserController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT...
            $d1 = $user_1->with(['account'])->where('user_name', '=', $user_name)->get();
            
            foreach($d1 as $d)
            {
                array_push($data, ['user_code' => $d->user_code, 
                                    'user_per_name' => $d->user_per_name,
                                    'user_city' => $d->user_city,
                                    'user_mobile' => $d->user_mobile,
                                    'user_setup_fee' => $d->account->user_setup_fee,
                                    'user_type' => $d->account->user_type,
                                    'parent_name' => $d->account->parent_name,
                                    'user_rec_mode' => $d->account->user_rec_mode,
                                    'user_api_url_1' => $d->account->user_api_url_1,
                                    'user_api_url_2' => $d->account->user_api_url_2 ]);
                                               
            }
            

            return view('android.ag_profile_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'profile' => $data]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function dashboard(Request $request)
	{
       
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT...
            
            return view('android.dashboard_ag_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
