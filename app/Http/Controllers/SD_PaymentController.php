<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;
use App\Models\NetworkDetails;

use App\Models\PaymentLedgerDetails;
use App\Models\AllTransaction;

class SD_PaymentController extends Controller
{
    //
    public function remote_index(Request $request)
	{
        
        $user_4 = new UserChainDetails;
        $upay_1 = new UserPaymentDetails;
        $uper_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        $data = $user_4->select('user_code', 'user_name')->where('parent_code', '=', $user_code)->orderBy('user_name', 'asc')->get();
        $data1 = $upay_1->select('trans_id','user_name', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks')
        ->where('grant_user_name', '=', $user_name)->orderBy('id', 'desc')->limit(6)->get();
        $d2 = $uper_1->select('user_code','user_name', 'user_per_name')->get();

        return view('sdistributor.remote_payment', ['user1' => $data, 'pay1' => $data1, 'user' => $ob, 'user2' => $d2]);
        
		
    }

    public function store(Request $request)
	{
    
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);

        $data = [];
        $result = [];
        $status = 0;
        $op = "NONE";

        $user_name_req = trim($request->user_name);

        if($user_name_req != "admin")
        {
            $data['trans_id'] = rand(100000,999999);
            $data['user_name_req'] = $user_name_req;
            $data['user_name_grnt'] = $ob->user;
            $data['user_amount'] = trim($request->user_amount);
            $data['user_remarks'] = trim($request->user_remarks);
            $data['payment_mode'] = "REMOTE_PAYMENT";

            $d1 = NetworkDetails::select('net_code')->where('net_name', 'REMOTE_PAYMENT')->get();

            $net_code = '0';
            if($d1->count() > 0) {
                $net_code = $d1[0]->net_code;
            }

            $data['net_code'] = $net_code;

            list($status, $op) = Stock::PaymentGrant($data);
        }
        else
        {
            $status = 1;
            $op = 'Error! Request User Name is not Admin...';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);

    }

    public function request_index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('user_name', '=', $ob->user)
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                    ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                    ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                                })->orderBy('id', 'desc')->limit(6)->get();

        return view('sdistributor.payment_request_1', ['user' => $ob, 'pay1' => $d1]);
    		
    }

    public function store_request(Request $request)
	{
       
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);

        // Get Parent User Name
        $user_name_grnt = "None";
        $d1 = UserAccountDetails::select('parent_name')->where('user_name', '=', $ob->user)->get();
        if($d1->count() > 0)
        {
            $user_name_grnt = $d1[0]->parent_name;
        }
       
        $data = [];
        $data['trans_id'] = rand(100000,999999);
        $data['user_name_req'] = $ob->user;
        $data['user_name_grnt'] = $user_name_grnt;
        $data['user_amount'] = trim($request->user_amount);
        $data['user_remarks'] = trim($request->user_remarks);
        $data['payment_mode'] = "FUND_TRANSFER";

        $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

        $net_code = '0';
        if($d1->count() > 0) {
            $net_code = $d1[0]->net_code;
        }

        $data['net_code'] = $net_code;

        list($status, $op) = Stock::PaymentRequest($data);

        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function index_accept(Request $request)
	{    
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('parent_name', $ob->user)
                                ->where('trans_type', 'FUND_TRANSFER')
                                ->where('rech_status', 'PENDING')
                                ->orderBy('id', 'desc')->get();

        return view('sdistributor.payment_accept_1', ['pay1' => $d1, 'user' => $ob]);
        
    }

    public function store_accept($trans_id, $trans_amt, Request $request)
	{
    
        $ob = GetCommon::getUserDetails($request);

        $data = [];
        $result = [];
        $status = 0;
        $op = "NONE";

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
        if($d1->count() > 0)
        {
            $user_name_req = $d1[0]->user_name;

            if($user_name_req != "admin")
            {
                $data['trans_id'] = $trans_id;
                $data['user_name_req'] = $user_name_req;
                $data['user_name_grnt'] = $ob->user;
                $data['user_amount'] = $trans_amt;
                $data['user_remarks'] = $d1[0]->reply_id."- done";
                $data['payment_mode'] = "FUND_TRANSFER";

                $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

                $net_code = '0';
                if($d1->count() > 0) {
                    $net_code = $d1[0]->net_code;
                }

                $data['net_code'] = $net_code;

                list($status, $op) = Stock::paymentGrant($data);
            }
            else
            {
                $status = 1;
                $op = 'Error! Request User Name is not Admin...';
            }

        }
        else
        {
            $status = 2;
            $op = 'Error! Transaction Id is not found...';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);

    }

    public function store_delete($trans_id, Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $status = 0;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
        if($d1->count() > 0)
        {
            AllTransaction::where('trans_id', '=', $trans_id)
                                ->update(['rech_status'=> 'FAILURE', 'rech_option' => '2', 'reply_date' => $date_time, 'updated_at'=> $date_time ]);

            $status = 0;
            $op = 'Payment Transfer is cancelled Successfully...';

        }
        else
        {
            $status = 2;
            $op = 'Error! Transaction Id is not found...';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);

    }

    public function getCode($user_name)
    {
        $user_1 = new UserPersonalDetails;
        $code = 0;

        $d1 = $user_1->select('user_code')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $bal = $d1[0]->user_code;
        }
        
        return $code;
    }
}
