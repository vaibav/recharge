<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\SmsInterface;

use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;

use App\Models\PaymentLedgerDetails;

class UserRemotePaymentController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $user_4 = new UserChainDetails;
        $upay_1 = new UserPaymentDetails;
        $uper_1 = new UserPersonalDetails;
        
        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        $data = $user_4->select('user_code', 'user_name')->where('parent_code', '=', $user_code)->orderBy('user_name')->get();
        $data1 = $upay_1->select('trans_id','user_name', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks')
        ->where('grant_user_name', '=', $user_name)->orderBy('id', 'desc')->limit(6)->get();
        $d2 = $uper_1->select('user_code','user_name', 'user_per_name')->get();

        return view('user.userremotepayment', ['user1' => $data, 'pay1' => $data1, 'user' => $ob, 'user2' => $d2]);
        
		
    }

    

    public function store(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $upay_1 = new UserPaymentDetails;
        $upay_2 = new PaymentLedgerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $user_1 = new UserPersonalDetails;
        $user_4 = new UserChainDetails;
        
        // Validation
        $this->validate($request, [
            'user_name' => 'required',
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_name.required' => ' The user name is required.',
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.'
        ]);

        $ob = GetCommon::getUserDetails($request);
        $user_code_grnt = $ob->code;
        $user_name_grnt = $ob->user;

        // Post Data
        //$user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_code = trim($request->user_code);
        $user_name = trim($request->user_name);
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        // Insert Data
        $cnt = $user_1->where('user_name', '=', $user_name)->count();
        if($cnt > 0)
        {
            // Check Admin balance Amount
            $u_bal_1 = "";
            $u_bal_2 = "";
            $flg_1 = 0;
            $net_bal_1 = 0;
            $net_bal_2 = 0;

            $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name_grnt)->get();
            foreach($d1 as $d)
            {
                $u_bal_1 = $d->user_balance;
            }

            $setup_fee = GetCommon::getSetupFee($user_name_grnt);
            if(floatval($setup_fee) >= floatval($u_bal_1))
            {
                $flg_1 = 3;      //User Balance is less than setup Fee...
            }

            if($user_name != "admin")           // To User is not Admin
            {
                if($u_bal_1 != "" && $flg_1 == 0)
                {
                    if(floatval($u_bal_1) < floatval($user_amount))
                    {
                        $flg_1 = 1;     // Granted user has less Amount...
                    }
                    else
                    {
                        // Deduct Amount from user name...
                        $net_bal_1 = floatval($u_bal_1) - floatval($user_amount);

                        $net_bal_1 = $this->convertNumber($net_bal_1);
                                                
                        $uacc_1->where('user_name', '=', $user_name_grnt)
                               ->update(['user_balance'=>$net_bal_1, 'updated_at'=>$date_time]);
                    }
                }
                else
                {
                    $flg_1 = 2;         // Granted User has No Amount...(No Entry..)
                }
            }
            

            if($flg_1 == 0)
            {
                 // Add amount to user balance Account
                $d2 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                foreach($d2 as $d)
                {
                    $u_bal_2 = $d->user_balance;
                }

                if($u_bal_2 != "")
                {
                    // Add Amount to user name...
                    $net_bal_2 = floatval($u_bal_2) + floatval($user_amount);

                    $net_bal_2 = $this->convertNumber($net_bal_2);
                    
                    $uacc_1->where('user_name', '=', $user_name)
                                ->update(['user_balance'=>$net_bal_2, 'updated_at'=>$date_time]);
                }
                else
                {
                    $net_bal_2 = floatval($user_amount);         // Granted User has No Amount (No Entry...Insert It..)

                    $net_bal_2 = round($net_bal_2, 2);
                    $net_bal_2 = number_format($net_bal_2, 2, '.', '');
                    
                    $uacc_1->user_code = $user_code;
                    $uacc_1->user_name = $user_name;
                    $uacc_1->user_balance = $net_bal_2;
                    $uacc_1->save();
                }

                $payment_mode = "REMOTE_PAYMENT";
                

                // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = $this->convertNumber($user_amount);
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 1;
            
                $upay_1->save();
                
                // Payment Ledger
                $ar1 = ['trans_id' => "C".rand(100000,999999), 'bill_no' => "-", 'user_name' => $user_name, 'pay_type' => "C",
                'pay_amount' => $this->convertNumber($user_amount), 'pay_mode' => "CASH", 'pay_option' => "-", 'pay_image' => "-",
                'from_user' => $user_name_grnt, 'pay_date' => $date_time, 
                'agent_name' => "-", 'pay_remarks' => $payment_mode, 'pay_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time];

                $upay_2->insert($ar1);


                // Transaction Details.....
                $tran_1->trans_id = $trans_id;
                $tran_1->user_name = $user_name;
                $tran_1->trans_type = $payment_mode;
                $tran_1->trans_option = '1';
                
                $tran_1->save();

                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".$this->convertNumber($user_amount)." is transferred successfully at ".date('d/m/Y H:i:s');
                    SmsInterface::callSms($user_name, $dx2[0]->user_mobile, $msg);
                }

                $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Amount is transferred successfully...</label>";
            }
            else if($flg_1 == 1)
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your Account Balance is Low...</label>";
            }
            else if($flg_1 == 2)
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your have no Account Balance...</label>";
            } 
            else if($flg_1 == 3)
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Your have Low Account Balance in SetupFee...</label>";
            }   
        }
        else
        {
            $op = "User Name does not Exists...";
        }

        
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

    

    
}
