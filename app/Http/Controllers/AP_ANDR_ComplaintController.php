<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;

use App\Models\UserComplaintDetails;

use App\Models\NetworkDetails;
use App\Models\NetworkTypeDetails;
use App\Models\ApiProviderDetails;

class AP_ANDR_ComplaintController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            return view('android.ap_complaint_entry_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;

        $auth_token = trim($request->auth_token);
        $f_date = trim($request->f_date);
        $t_date = trim($request->t_date);
        $rech_type = 'WEB_RECHARGE';
        $user_mobile = trim($request->user_mobile);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API PARTNER...
            $d1 = $this->getRechargeDetailsApipartner_1($user_name, $f_date, $t_date, $rech_type, $user_mobile);
                   
            return view('android.ap_complaint_entry_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'recharge' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not API PARTNER";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;
        $z1 = 0;

        $auth_token = trim($request->auth_token);
        $rech_tr_id = trim($request->trid);
        $rech_mobile = trim($request->mob);
        $rech_amount = trim($request->amt);
        $user_complaint = trim($request->cmp);

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $comp_1->trans_id = rand(10000,99999);
            $comp_1->user_name = $user_name;
            $comp_1->user_type = $user_type;
            $comp_1->comp_type = "RECHARGE";
            $comp_1->rech_trans_id = $rech_tr_id;
            $comp_1->rech_mobile = $rech_mobile;
            $comp_1->rech_amount = $rech_amount;
            $comp_1->user_complaint = $user_complaint;
            $comp_1->admin_reply = "";
            $comp_1->reply_status = 1;
            $comp_1->reply_date = date("Y-m-d H:i:s");

            $comp_1->save();
            
            $op = 1;
                   
        }
        else if($z1 == 1)
        {
            $op = 2;
        }
        else if($z1 == 2)
        {
            $op = 3;
        }

        
        return redirect()->back()->with('msg', $op);
		
    }

    public function view_complaint(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;
        $z1 = 0;

        $auth_token = trim($request->auth_token);
       

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $user_name = strtoupper($user_name);
            $d1 = $comp_1->whereRaw('upper(user_name) = ?',[$user_name])->orderBy('id', 'desc')->get(); 

            return view('android.ap_complaint_entry_3', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'complaint' => $d1]);
            
                   
        }
        else if($z1 == 1)
        {
            $op = 2;
        }
        else if($z1 == 2)
        {
            $op = 3;
        }

        
        return redirect()->back()->with('msg', $op);
		
    }

    public function getRechargeDetailsApipartner_1($user_name, $f_date, $t_date, $rech_type, $user_mobile)
    {
        $tran_1 = new TransactionAllDetails;
        $net_2 = new NetworkDetails;
        
        $user_name = strtoupper($user_name);

        $d1 = [];

        if($f_date != "" && $t_date != "")
        {
            $f_date = $f_date." 00:00:00";
            $t_date = $t_date." 23:59:59";
        }

        if($f_date != "" && $t_date != "" && $rech_type == "WEB_RECHARGE")
        {
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'API_RECHARGE')
                    ->orderBy('id', 'desc')->get(); 
            
            if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->newrecharge1[0]->rech_mobile == $user_mobile;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $d1 = $tran_1->with(['billpayment'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BILL_PAYMENT')
                    ->orderBy('id', 'desc')->get(); 

            if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->billpayment[0]->con_acc_no == $user_mobile;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $d1 = $tran_1->with(['moneytransfer'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BANK_TRANSFER')
                    ->orderBy('id', 'desc')->get(); 

            if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->moneytransfer[0]->bk_acc_no == $user_mobile;
                });
            }
        }
        
       
      $d2 = $net_2->select('net_code','net_name')->get();
      
      $str = "";
      
      $j = 1;
      foreach($d1 as $d)
      {
         
          if($d->trans_type == "API_RECHARGE")
          {
              if(sizeof($d->newrecharge1) > 0)
              {
                  $net_name = "";
                  foreach($d2 as $r)
                  {
                      if($d->newrecharge1[0]->net_code == $r->net_code)
                          $net_name = $r->net_name;
                  }
  
                  $rech_status = "";
                 
                  if($d->trans_option == 1)
                  {
                      $rech_status = $d->newrecharge1[0]->rech_status;
                      $rech_option = $d->newrecharge1[0]->rech_option;
                      
                  }
                  else if($d->trans_option == 2)
                  {
                      $rech_status = $d->newrecharge1[1]->rech_status;
                      $rech_option = $d->newrecharge1[1]->rech_option;
                      
                  }
                  if ($rech_status == "SUCCESS")
                  {
                        $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                        $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                        $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                    
                        $str = $str."<p style='font-size: 20px;margin: 0;' id = 'amt_".$d->trans_id."'>".$d->newrecharge1[0]->rech_amount."</p>
                                        <label style='font-size: 10px'>".$rech_status."</label> "; 
                        $str = $str."</div><div class='col-6 col-sm-6 col-md-6 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                        $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                        $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;' 
                                        id = 'mob_".$d->trans_id."'>".$d->newrecharge1[0]->rech_mobile."</p>";
                        $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->newrecharge2->reply_opr_id."</p>";
                        $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->newrecharge1[0]->rech_date."</p>"; 
                        
                        $str = $str."</div><div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                        $str = $str."<div class='row' style='margin:0px 5px;'>
                                        <div class='col-12 col-sm-12 col-md-12' style='padding:2px 6px;padding-top: 10px;padding-bottom: 6px;'>
                                                <input class='form-control curve-border-1' type='text' 
                                                placeholder = 'Complaint' id='cmp_".$d->trans_id."' name='cmp_".$d->trans_id."'>
                                        </div>
                                    </div>
                                    <div class='row' style='margin:0px 5px;'>
                                        <div class='col-12 col-sm-12 col-md-12 ' style='padding:2px 4px;padding-left:10px;'>
                                            <a class='btn btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                            id='rech_".$d->trans_id."' style = 'padding:4px 8px;'>send</a>
                                        </div>
                                        
                                    </div>";
                        $str = $str."</div></div></div>";
                      
                  }
                  
              }
             
              
          }
          if($d->trans_type == "BILL_PAYMENT")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->billpayment[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $rech_status = "";
              
              if($d->trans_option == 1)
              {
                  $rech_status = $d->billpayment[0]->con_status;
                  $rech_option = $d->billpayment[0]->con_option;          
                 
              }
              else if($d->trans_option == 2)
              {  
                  $rech_status = $d->billpayment[1]->con_status;
                  $rech_option = $d->billpayment[1]->con_option;          
              }
              
              if ($rech_status == "SUCCESS")
              {
                    $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                    $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>";
                
                    $str = $str."<p style='font-size: 20px;margin: 0;'>".$d->billpayment[0]->con_amount."</p><label style='font-size: 10px'>".$rech_status."</label> "; 
                    $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                    $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                    $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->billpayment[0]->con_acc_no."</p>"; 
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->billpayment[0]->reply_opr_id."</p>"; 
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->billpayment[0]->created_at."</p>"; 
                    
                    $str = $str."</div><div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                    $str = $str."<div class='row' style='margin:0px 5px;'>
                                    <div class='col-12 col-sm-12 col-md-12' style='padding:2px 6px;padding-top: 10px;padding-bottom: 6px;'>
                                            <input class='form-control curve-border-1' type='number' 
                                            placeholder = 'Complaint' id='cmp_".$d->trans_id."' name='cmp_".$d->trans_id."'>
                                    </div>
                                </div>
                                <div class='row' style='margin:0px 5px;'>
                                    <div class='col-12 col-sm-12 col-md-12 ' style='padding:2px 4px;padding-left:10px;'>
                                        <a class='btn btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                        id='bill_".$d->trans_id."' style = 'padding:4px 8px;'>send</a>
                                    </div>
                                    
                                </div>";
                    $str = $str."</div></div></div>";
                    
                  
              }
              
          } 
          if($d->trans_type == "BANK_TRANSFER")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->moneytransfer[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $rech_status = "";
              $b_code = "";

              if($d->trans_option == 1)
              {
                  $b_code = $d->moneytransfer[0]->bk_code;
                  $rech_status = $d->moneytransfer[0]->mt_status;
                  $rech_option = $d->moneytransfer[0]->mt_option;          
              }
              else if($d->trans_option == 2)
              {  
                  $b_code = $d->moneytransfer[1]->bk_code;
                  $rech_status = $d->moneytransfer[1]->mt_status;
                  $rech_option = $d->moneytransfer[1]->mt_option;          
                  
              }

              $bank = "";
                if($b_code == "UTIB") {
                    $bank = "AXIS BANK";
                }
                else if($b_code == "BARB") {
                    $bank = "BANK OF BARODA";
                }
                else if($b_code == "CNRB") {
                    $bank = "CANARA BANK";
                }
                else if($b_code == "CORP") {
                    $bank = "CORPORATION BANK";
                }
                else if($b_code == "CIUB") {
                    $bank = "CITY UNION BANK";
                }
                else if($b_code == "HDFC") {
                    $bank = "HDFC BANK";
                }
                else if($b_code == "IBKL") {
                    $bank = "IDBI BANK LTD";
                }
                else if($b_code == "IDIB") {
                    $bank = "INDIAN BANK";
                }
                else if($b_code == "IOBA") {
                    $bank = "INDIAN OVERSEAS BANK";
                }
                else if($b_code == "ICIC") {
                    $bank = "ICICI BANK";
                }
                else if($b_code == "KVBL") {
                    $bank = "KARUR VYSYA BANK";
                }
                else if($b_code == "SBIN") {
                    $bank = "STATE BANK OF INDIA";
                }
                else if($b_code == "SYNB") {
                    $bank = "SYNDICATE BANK";
                }
                else if($b_code == "TMBL") {
                    $bank = "TAMILNADU MERCANTILE BANK";
                }
                else if($b_code == "VIJB") {
                    $bank = "VIJAYA BANK";
                }
                else if($b_code == "") {
                    $bank = $b_code;
                }
                else {
                    $bank = $b_code;
                }
             
              if ($rech_status == "SUCCESS")
              {
                    $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                    $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                    
                
                    $str = $str."<p style='font-size: 16px;margin: 0;'>".$d->moneytransfer[0]->mt_amount."</p><label style='font-size: 10px'>".$rech_status."</label> "; 
                    $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                    $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$bank."</p>"; 
                    $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->moneytransfer[0]->bk_acc_no."</p>";
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->moneytransfer[0]->mt_reply_id."</p>"; 
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->moneytransfer[0]->mt_date."</p>";  
                    
                    $str = $str."</div><div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                    $str = $str."<div class='row' style='margin:0px 5px;'>
                                    <div class='col-12 col-sm-12 col-md-12' style='padding:2px 6px;padding-top: 10px;padding-bottom: 6px;'>
                                            <input class='form-control curve-border-1' type='number' 
                                            placeholder = 'Complaint' id='cmp_".$d->trans_id."' name='cmp_".$d->trans_id."'>
                                    </div>
                                </div>
                                <div class='row' style='margin:0px 5px;'>
                                    <div class='col-12 col-sm-12 col-md-12 ' style='padding:2px 4px;padding-left:10px;'>
                                        <a class='btn btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                        id='money_".$d->trans_id."' style = 'padding:4px 8px;'>send</a>
                                    </div>
                                    
                                </div>";
                    $str = $str."</div></div></div>";
                    
                    
                  
              }
             
          }                                        
                
          $j++;
      }
        
      
        return $str; 
    }
}
