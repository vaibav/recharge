<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

class AndroidRetDashboardController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            
            return view('android.dashboard_rt_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }
}
