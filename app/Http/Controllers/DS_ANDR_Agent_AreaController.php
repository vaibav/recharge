<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;

class DS_ANDR_Agent_AreaController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = CollectionCommon::getArea($user_name);

            $d2 = $user_2->with(['personal'])->where('user_type', '=', "AGENT")->where('parent_name', '=', $user_name)->get();

            $d3 = $area_1->where('area_status', '=', "1")->get();

            return view('android.ds_agent_allocation_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'user_details' => $d1, 
                                'agent_details' => $d2, 'area_details' => $d3]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;

        $auth_token = trim($request->auth_token);
        $agent_name = trim($request->agent_name);
        $area_name = trim($request->area_name);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = $area_1->select('area_name')->where('user_name', '=', $agent_name)->orderBy('id', 'desc')->limit(1)->get();

            if($d1->count() > 0)
            {
                if($d1[0]->area_name == $area_name)
                {
                    $x = 1;
                }
                else
                {
                    $area_1->where('user_name', '=', $agent_name)->update(['area_status' => '0']);
                }
            }
            

            if($x == 0)
            {
                // Insert Record... 
                $area_1->trans_id = rand(10000,99999);
                $area_1->user_name = $agent_name;
                $area_1->area_name = $area_name;
                $area_1->area_status = 1;
        
                $area_1->save();
                
                $op = 1;
            }
            else
            {
                $op = 2;
            }
            
        }
        else if($z1 == 1)
        {
            $op = 3;
        }
        else if($z1 == 2)
        {
            $op = 4;
        }

        return redirect()->back()->with('msg', $op);
    }

    public function view(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $agent = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = CollectionCommon::getArea($user_name);

            $d2 = $user_2->where('user_type', '=', "AGENT")->where('parent_name', '=', $user_name)->get();

            $j = 0;
            foreach($d2 as $d)
            {
                $d3 = $area_1->select('area_name')->where('user_name', '=', $d->user_name)->where('area_status', '=', "1")->get();

                if($d3->count() > 0)
                {
                    $agent[$j][0] = $d->user_name;
                    $agent[$j][1] = $d3[0]->area_name;
                    $j++;
                }
            }

            return view('android.ds_agent_allocation_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'agent' => $agent ]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function dashboard(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
           
            return view('android.ds_agent_dashboard', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

}
