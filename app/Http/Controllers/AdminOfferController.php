<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\AdminOfferDetails;

class AdminOfferController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = $this->getUserDetails($request);
        
        return view('admin.offer_entry', ['user' => $ob]);
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
        $off_1 = new AdminOfferDetails;
        $op = 0;


        $this->validate($request, [
            'offer_detail' => 'required'
        ],
        [
            'offer_detail.required' => ' Offer Detail field is required.'
            ]);

        $ob = $this->getUserDetails($request);
        
        // Insert Record... 
        $off_1->trans_id = rand(10000,99999);
        $off_1->offer_details = trim($request->offer_detail);
        $off_1->user_type = trim($request->user_type);
        $off_1->offer_status = 1;
        

        $off_1->save();
        
        $op = 1;

        return redirect()->back()->with('msg', $op);

    }

    public function offer_inactive($trans_id, Request $request)
	{
        $off_1 = new AdminOfferDetails;
        $op = 0;

        $ob = $this->getUserDetails($request);
        
        $date_time = date("Y-m-d H:i:s");
        $d1 = $off_1->where('trans_id', '=', $trans_id)->get();
        if($d1->count() > 0)
        {
            $status = $d1[0]->offer_status;

            if ($status == 1)
            {
                $off_1->where('trans_id', '=', $trans_id)
                                ->update(['offer_status' => '2', 'updated_at' => $date_time]);
            }
            else if ($status == 2)
            {
                $off_1->where('trans_id', '=', $trans_id)
                                ->update(['offer_status' => '1', 'updated_at' => $date_time]);
            }
            $op = 1;
        }
        

        return redirect()->back()->with('msg', $op);

    }

    public function offer_delete($trans_id, Request $request)
	{
        $off_1 = new AdminOfferDetails;
        $op = 0;

        $ob = $this->getUserDetails($request);
        
        
        $d1 = $off_1->where('trans_id', '=', $trans_id)->get();
        if($d1->count() > 0)
        {
            $off_1->where('trans_id', '=', $trans_id)->delete();
            $op = 2;
        }
        

        return redirect()->back()->with('msg', $op);

    }

    public function admin_view(Request $request)
	{
        $off_1 = new AdminOfferDetails;
            
        $ob = $this->getUserDetails($request);
        
        $data = $off_1->get();

        return view('admin.offer_view', ['off1' => $data, 'user' => $ob]);
        
    }
}
