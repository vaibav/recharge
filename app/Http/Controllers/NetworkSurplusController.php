<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\NetworkSurplusDetails;
use App\Models\UserAccountBalanceDetails;

class NetworkSurplusController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;
        
        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->orderBy('net_name', 'asc')->get();
        $data3 = $net_1->select('net_tr_id', 'net_code', 'user_type', 'from_amount', 'to_amount', 'surplus_charge')
        ->where('net_code', '=', $net_code)->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.networksurplus', ['network' => $data1, 'net1' => $data3, 'user' => $ob, 'net_code' => $net_code]);
    
    }

   

    public function store(Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;
        
        $z = 0;
        $op = "";

        $net_tr_id = rand(1000,9999);
        $net_code = trim($request->net_code);
        $user_type = trim($request->user_type);
        $from_amount = trim($request->from_amount);
        $to_amount = trim($request->to_amount); 
        $surplus_charge = trim($request->surplus_charge); 

        // Validation
        $this->validate($request, [
            'from_amount' => 'required', 'to_amount' => 'required', 'surplus_charge' => 'required'
        ],
        [
            'from_amount.required' => ' The From Amount field is required.',
            'to_amount.required' => ' The To Amount field is required.', 
            'surplus_charge.required' => ' The Surplus Charge field is required.' ]);
        
        $cnt = $net_1->where([['net_code', '=', $net_code], ['user_type', '=', $user_type], ['from_amount', '=', $from_amount], ['to_amount', '=', $to_amount]])->count();
        $cnt1 = $net_1->where([['net_code', '=', $net_code], ['user_type', '=', $user_type], ['from_amount', '=', $from_amount]])->count();
        if($cnt == 0)
        {
            if($cnt1 == 0)
            {
                if(floatval($from_amount) > floatval($to_amount))
                {
                    $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! From Amount is greater than to Amount...</label>";
                }
                else
                {
                    // Insert Record... 
                    $net_1->net_tr_id = $net_tr_id;
                    $net_1->net_code = $net_code;
                    $net_1->user_type = $user_type;
                    $net_1->from_amount = $from_amount;
                    $net_1->to_amount = $to_amount;
                    $net_1->surplus_charge = $surplus_charge;
    
                    $net_1->save();
    
                    $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Entry is Added Successfully...</label>";
    
                }
            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! From Amount Entry is already Added...</label>";
            }
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Entry is already Added...</label>";
        }
        
        return redirect()->back()->with('result', [ 'output' => $op]);

		
    }

    public function view($net_code, Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data3 = $net_1->select('net_tr_id', 'net_code', 'user_type', 'from_amount', 'to_amount', 'surplus_charge')
                        ->where('net_code', '=', $net_code)->get();
        $op = "";

        $ob = GetCommon::getUserDetails($request);

        return view('admin.networksurplus', ['network' => $data1, 'net1' => $data3, 'net_code' => $net_code, 'user' => $ob]);
        
    }

    public function delete($tr_id, Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
       
        $z = 0;
        $op = "";
        $net_code =  0;


        $cnt = $net_1->where('net_tr_id', '=', $tr_id)->count();
        
        if($cnt > 0)
        {
            
            
            // Delete Record... 
            $net_1->where('net_tr_id', '=', $tr_id)->delete();
            $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Entry is Deleted Successfully...</label>";
        }
        else
        {
                            
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! No Record Found...</label>";

        }

       
        return redirect()->back()->with('result', [ 'output' => $op]);

        
    }

    public function view_retailer(Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;

        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data3 = $net_1->select('net_tr_id', 'net_code', 'user_type', 'from_amount', 'to_amount', 'surplus_charge')
                        ->where('user_type', '=', 'RETAILER')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.network_surplus', ['network' => $data1, 'surplus' => $data3, 'user' => $ob]);
    
    }

    public function view_apipartner(Request $request)
	{
        $net_1 = new NetworkSurplusDetails;
        $net_2 = new NetworkDetails;

        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data3 = $net_1->select('net_tr_id', 'net_code', 'user_type', 'from_amount', 'to_amount', 'surplus_charge')
                        ->where('user_type', '=', 'API PARTNER')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('user.network_surplus_apipartner', ['network' => $data1, 'surplus' => $data3, 'user' => $ob]);
    
    }

   

}
