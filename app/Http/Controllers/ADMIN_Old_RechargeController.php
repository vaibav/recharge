<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\BackupRechargeDetails;
use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

class ADMIN_Old_RechargeController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $user_1->with(['personal']) ->where(function($query){
                                                return $query
                                                ->where('user_type', '=', 'RETAILER')
                                                ->orWhere('user_type', '=', 'API PARTNER');
                                            })->orderBy('user_name', 'asc')->get();

        return view('admin.old_recharge_1', ['user' => $ob, 'users' => $d1]);
        
    }

    public function viewdate(Request $request)
	{
		
        $rech_1 = new BackupRechargeDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $user_name = trim($request->user_name);
       

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $user_name =strtoupper($user_name);

        $d1 = $rech_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(user_name) = ?',[$user_name])
                        ->orderBy('id', 'desc')->paginate(15);
        
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();

        $wsua_tot = $rech_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(user_name) = ?',[$user_name])
                        ->sum('rech_amount');

        $wsut_tot = $rech_1->whereBetween('rech_date', [$f_date, $t_date])
                        ->whereRaw('upper(user_name) = ?',[$user_name])
                        ->sum('rech_total');

        $wsua_tot = round($wsua_tot, 2);
        $wsut_tot = round($wsut_tot, 2);

        $total = ['wsua_tot' => $wsua_tot, 'wsut_tot' => $wsut_tot]; 

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);
        
        return view('admin.old_recharge_2', ['user' => $ob, 'recharge' => $d1, 'd2' => $d2, 'd3' => $d3, 'total' => $total]);

    }
}
