<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\Stock;


use App\Models\UserPaymentDetails;

use App\Models\NetworkDetails;


class AP_StockController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('apipartner.stock_1', ['user' => $ob]);
    		
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";


        // Opening Balanace
        $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob->user);
        $o_balance = Stock::stockCustomOpeningBalance($ob->user);
        $o_balance = floatval($o_balance) + floatval($o_balance1);

        // Opening Sales
        $o_sales_re = Stock::getRetailerRechargeOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_sales_eb = Stock::getRetailerBill($ob->user, "2018-10-01 00:00:00", $f_date);
        $o_sales_mo = Stock::getMoneyOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_sales_mv = Stock::getMoneyVerifyOpeningSales($ob->user, "2018-10-01 00:00:00",$f_date);
        $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));

        $old_re = Stock::getRetailerOldRecharge($ob->user);

        // current sales...
        $c_sales_re = Stock::getRetailerRechargeOpeningSales($ob->user, $f_date, $t_date);
        $c_sales_eb = Stock::getRetailerBill($ob->user, $f_date, $t_date);
        $c_sales_mo = Stock::getMoneyOpeningSales($ob->user, $f_date, $t_date);
        $c_sales_mv = Stock::getMoneyVerifyOpeningSales($ob->user, $f_date, $t_date);

        $data = Stock::stockData($ob->user, $f_date, $t_date);

        $d1 = Stock::getRetailerRecharge($ob->user, $f_date, $t_date);
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        // Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('apipartner.stock_2', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 
                                            'network' => $d2, 'bill' => $c_sales_eb, 'ce_tot' => $c_sales_re, 
                                            'money' => $c_sales_mo, 'mverify' => $c_sales_mv, 'old_re' => $old_re, 'user' => $ob]); 

        
    }

   

}
