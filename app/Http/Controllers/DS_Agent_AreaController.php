<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;

class DS_Agent_AreaController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;

        $ob = GetCommon::getUserDetails($request);
       
        $d1 = CollectionCommon::getArea($ob->user);

        $d2 = $user_2->with(['personal'])->where('user_type', '=', "AGENT")->where('parent_name', '=', $ob->user)->get();

        $d3 = $area_1->where('area_status', '=', "1")->get();

        return view('distributor.agent_allocation', ['user' => $ob, 'user_details' => $d1, 'agent_details' => $d2, 'area_details' => $d3]);
        
    }

    public function store(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $area_1 = new AgentAreaDetails;
        
        $z = 0;
        $x = 0;
        $op = "NONE";

        $ob = GetCommon::getUserDetails($request);

        $agent_name = trim($request->agent_name);
        $area_name = trim($request->area_name);
        
        $d1 = $area_1->select('area_name')->where('user_name', '=', $agent_name)->orderBy('id', 'desc')->limit(1)->get();

        if($d1->count() > 0)
        {
            if($d1[0]->area_name == $area_name)
            {
                $x = 1;
            }
            else
            {
                $area_1->where('user_name', '=', $agent_name)->update(['area_status' => '0']);
            }
        }
        

        if($x == 0)
        {
            // Insert Record... 
            $area_1->trans_id = rand(10000,99999);
            $area_1->user_name = $agent_name;
            $area_1->area_name = $area_name;
            $area_1->area_status = 1;
    
            $area_1->save();
            
            $op = "Entry is added Successfully....";
        }
        else
        {
            $op = "Already Same Area is Allocated....";
            $z = 1;
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
        		
    }
}
