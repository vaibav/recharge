<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

class AdminPinController extends Controller
{
    //
    public function index(Request $request)
	{
        $result = [];

        $ob = GetCommon::getUserDetails($request);

        $pin = trim($request->pin);

        if ( $pin == "Vaibav@9876")
        {
            $result = ["output" => "success", "status" => 200];
        }
        else
        {
            $result = ["output" => "failed", "status" => 300];
        }
                
        return response()->json($result, 200);
    }
}
