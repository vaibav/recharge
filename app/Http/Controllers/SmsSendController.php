<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Libraries\GetCommon;
use App\Libraries\SmsInterface;

use App\Models\UserAccountBalanceDetails;
use App\Models\MobileSmsDetails;
use App\Models\ApiProviderDetails;

class SmsSendController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.smssend', ['user' => $ob]);
        
		
    }

   

    public function store(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

         // Validation
         $this->validate($request, [
            'user_mobile' => 'required', 'user_message' => 'required'
        ],
        [
            'user_mobile.required' => ' The Mobile No is required.',
            'user_message.required' => ' The Message is required.',
            ]);

        $user_mobile = trim($request->user_mobile);
        $user_message = trim($request->user_message);
       
        $rez = SmsInterface::callSms('admin', $user_mobile, $user_message);
        
        if($rez == 200)
            $op = 1;

        

        return redirect()->back()->with('msg', $op);
        
    }

    public function view(Request $request)
	{
                                        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.adminsmsreport', ['user' => $ob]);
        
    }


    public function viewdate(Request $request)
    {
        $mob_1 = new MobileSmsDetails;
        $api_1 = new ApiProviderDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        
        $d1 = $mob_1->whereBetween('created_at', [$f_date, $t_date])
                        ->orderBy('id', 'desc')->get(); 
        
        
        $d2 = $api_1->select('api_code','api_name')->get();
        
        return view('admin.adminsmsreport_view', ['user' => $ob, 'sms' => $d1, 'from_date' => $date_1, 'to_date' => $date_2, 'api' => $d2, 'total' => '0']); 

        
    }


}
