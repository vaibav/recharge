<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\GetMobileCommon;

use App\Models\UserComplaintDetails;

class SD_ComplaintController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('sdistributor.complaint_1', ['user' => $ob]);
        
    }

    public function store(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
        $op = 0;


        $this->validate($request, [
            'user_complaint' => 'required'
        ],
        [
            'user_complaint.required' => 'Complaint field is required.'
            ]);

        $ob = GetCommon::getUserDetails($request);
        
        // Insert Record... 
        $comp_1->trans_id = rand(10000,99999);
        $comp_1->user_name = $ob->user;
        $comp_1->user_type = $ob->mode;
        $comp_1->comp_type = "NORMAL";
        $comp_1->rech_trans_id = "-";
        $comp_1->rech_mobile = trim($request->rech_mobile);
        $comp_1->rech_amount = trim($request->rech_amount);
        $comp_1->user_complaint = trim($request->user_complaint);
        $comp_1->admin_reply = "";
        $comp_1->reply_status = 1;
        $comp_1->reply_date = date("Y-m-d H:i:s");

        $comp_1->save();
        
        $op = 1;

       
        return redirect()->back()->with('msg', $op);

		
		
    }

    public function user_view(Request $request)
	{
        $comp_1 = new UserComplaintDetails;
            
        $ob = GetCommon::getUserDetails($request);
        
        $data = $comp_1->where('user_name', '=', $ob->user)->get();

        return view('sdistributor.complaint_2', ['comp1' => $data, 'user' => $ob]);
        
    }

   
}
