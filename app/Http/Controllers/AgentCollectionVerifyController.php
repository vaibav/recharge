<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class AgentCollectionVerifyController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_2 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);

        $d1 = $user_2->with(['personal'])->where('user_type', '=', "AGENT")->where('parent_name', '=', $ob->user)->get();

        return view('admin.agent_collection_verify', ['user' => $ob, 'agent_details' => $d1]);
        
    }

    public function view(Request $request)
	{
        $user_2 = new UserAccountDetails;
        $pay_1 = new PaymentLedgerDetails;

        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'from_date' => 'required',
            'to_date' => 'required'
        ],
        [
            'from_date.required' => ' The From Date is required.',
            'to_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        $agent_name = trim($request->agent_name);
        $agent_name = strtoupper($agent_name);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $d1 = $pay_1->whereRaw('upper(agent_name) = ?',[$agent_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->get(); 
    
        return view('admin.agent_collection_verify_view', ['user' => $ob, 'collection' => $d1, 'agent_name' => $agent_name]);
        
    }

    public function successUpdate($trans_id, Request $request)
	{
		
        $pay_1 = new PaymentLedgerDetails;
        
        $z = 0;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = $pay_1->where('trans_id', '=', $trans_id)->get(); 
        
        if($d1->count() > 0)
        {
            $pay_1->where('trans_id', '=', $trans_id)->update(['pay_status' => '1', 'updated_at' => $date_time]);
                
            $op = "Success Update is executed successfully...";

        }
        else
        {
            $z = 1;

            $op = "Not Updated...";
           
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    public function failureUpdate($trans_id, Request $request)
	{
		
        $pay_1 = new PaymentLedgerDetails;
        
        $z = 0;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = $pay_1->where('trans_id', '=', $trans_id)->get(); 
        
        if($d1->count() > 0)
        {
            $pay_1->where('trans_id', '=', $trans_id)->update(['pay_status' => '2', 'updated_at' => $date_time]);
                
            $op = "Success Update is executed successfully...";

        }
        else
        {
            $z = 1;

            $op = "Not Updated...";
           
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }
}
