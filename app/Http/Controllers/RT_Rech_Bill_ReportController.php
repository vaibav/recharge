<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportRetailer;

use App\Models\NetworkDetails;
use App\Models\NetworkTypeDetails;

use PDF;
use EXCEL;

class RT_Rech_Bill_ReportController extends Controller
{
    //
    public function index(Request $request)
    {
          
        $ob = GetCommon::getUserDetails($request);

        return view('retailer.nw_recharge_bill_report_1', ['user' => $ob]);
        
    }


    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
       
       $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = trim($ob->user);
        //$u_status = trim($request->rech_status);
        $u_status = "-";
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
         

        $dc4 = RechargeReportRetailer::getRechargeReportDetails_bill($f_date, $t_date, $u_name, $u_status, $u_mobile, $u_amount, "", "");
        

        $d2 = $net_2->select('net_code','net_name')->get();

        //-----------------------------------------------------
        //Total Calculation-------------------------------------------------------
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
      
        foreach($dc4 as $d)
        {
            if($d->trans_type == "BILL_PAYMENT")
            {
                
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;
                $r_amt = 0;
                

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $r_amt = $d->billpayment[0]->con_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $r_amt = $d->billpayment[1]->con_amount;
                }
                
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $efua_tot = floatval($efua_tot) + floatval($r_amt);
                    $efut_tot = floatval($efut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $esua_tot = floatval($esua_tot) + floatval($r_amt);
                    $esut_tot = floatval($esut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                
            }                                                    
        }

        $total = ['esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot];
       
       
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        $rs = [];
                               
        return view('retailer.nw_recharge_bill_report_2', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2, 'total' => $total]); 
        
    }

   

    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $rech_type = trim($request->rech_type);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
       
        $dc4 = RechargeReportAdmin::getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, "");

      
       

        $d2 = $net_2->select('net_code','net_name')->get();
       

        $headings = ['NO', 'MOBILE', 'NETWORK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
         
        $content = [];

        $k = 0;

        foreach($dc4 as $d)
        {
           
            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    $api_name = "";
                    $reply_id = "NA";
                    $reply_date = "";
                   

                    $reply_id = $d->newrecharge2->reply_opr_id;
                    $reply_date = $d->newrecharge2->reply_date;
                   

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $u_bal = $d->newrecharge1[0]->user_balance;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $u_bal = $d->newrecharge1[1]->user_balance;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                    }
                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "PENDING";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "SUCCESS";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }

                    $mode = "WEB";
                    if($d->trans_id != "")
                    {
                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                        $r_l = $matches[0][0];

                        $r_l = substr($r_l, -1);

                        if($r_l == "R")
                            $mode = "WEB";
                        else if($r_l == "A")
                            $mode = "API";
                        else if($r_l == "G")
                            $mode = "GPRS";
                        else if($r_l == "S")
                            $mode = "SMS";

                    }

                    
                    if($d->trans_option == 1)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, 
                        $d->newrecharge1[0]->rech_net_per."-".$d->newrecharge1[0]->rech_net_per_amt."-".$d->newrecharge1[0]->rech_net_surp,
                        $d->newrecharge1[0]->rech_total, $d->trans_id, $reply_id,
                        $d->newrecharge1[0]->rech_date, $reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                    else if($d->trans_option == 2)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, '',$d->newrecharge1[1]->rech_total,
                        $d->trans_id, '','','', $status,
                        number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                   
                }
                
               
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
               

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "PENDING";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
            
                if($d->trans_option == 1)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount, 
                        $d->billpayment[0]->con_net_per."-".$d->billpayment[0]->con_net_per_amt."-".$d->billpayment[0]->con_net_surp,
                        $d->billpayment[0]->con_total, $d->trans_id, $d->billpayment[0]->reply_opr_id,
                        $d->billpayment[0]->created_at, $d->billpayment[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount,'',
                        $d->billpayment[0]->con_total, $d->trans_id, '','','',
                        $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
               
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
                $mode = "WEB";
                if($d->trans_id != "")
                {
                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                    $r_l = $matches[0][0];

                    $r_l = substr($r_l, -1);

                    if($r_l == "R")
                        $mode = "WEB";
                    else if($r_l == "A")
                        $mode = "API";
                    else if($r_l == "G")
                        $mode = "GPRS";
                    else if($r_l == "S")
                        $mode = "SMS";

                }
                
                if($d->trans_option == 1)
                {
                   
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                        $d->moneytransfer[0]->mt_amount, 
                        $d->moneytransfer[0]->mt_per."-".$d->moneytransfer[0]->mt_per_amt."-".$d->moneytransfer[0]->mt_surp,
                        $d->moneytransfer[0]->mt_total, $d->trans_id, $d->moneytransfer[0]->mt_reply_id,
                        $d->moneytransfer[0]->mt_date, $d->moneytransfer[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];

                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                    $d->moneytransfer[0]->mt_amount, '',
                    $d->moneytransfer[0]->mt_total, $d->trans_id, '','','',
                    $status, number_format($o_bal,2, ".", ""),
                    number_format($u_bal,2, ".", "")];

                }
               
            }                                        
                  
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $ob->user."_Recharge_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');


    }

}
