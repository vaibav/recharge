<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;

use App\Models\AdminOfferDetails;
use App\Models\NetworkDetails;

class AgentDashboardController extends Controller
{
    //
    public function index(Request $request)
	{
        $off_1 = new AdminOfferDetails;
        $net_2 = new NetworkDetails;
        
        $ob = GetCommon::getUserDetails($request);


        $d3 = $net_2->select('net_name')->where('net_status', '=', 2)->get();
        $off = $off_1->where(function($query) use ($ob){
                            return $query
                            ->where('user_type', '=', 'ALL')
                            ->orWhere('user_type', '=', $ob->mode);
                        })
                        ->where('offer_status', '=', '1')->get();

        return view('agent.dashboard', ['user' => $ob, 'offer' => $off, 'in_active' => $d3]);
        		
    }
}
