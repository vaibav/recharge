<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\CollectionCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class AG_ANDR_CollectionController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];
        $area_name = "NONE";

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $d1 = $area_1->select('area_name')->where('user_name', '=', $user_name)->where('area_status', '=', '1')->get();

            if($d1->count() > 0)
            {
                $area_name = $d1[0]->area_name;
            }

            // Get parent
            $parent = $this->getParent($user_name);

            $d2 = CollectionCommon::getUser($parent, $area_name);

            return view('android.ag_collection_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'user1' => $d2, 'area_name' => $area_name]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $user_name_p = trim($request->user_name);
        $pay_amt = trim($request->pay_amt);
        $pay_date = trim($request->pay_date);
        $pay_mode = trim($request->pay_mode);
        $pay_option = trim($request->pay_option);

        $z1 = 0;
        $z = 0;
        $d2 = [];
        $date_time = date("Y-m-d H:i:s");
        $trans_id = "D".rand(100000,999999);
        $bill_no = rand(100000,999999);
        $pay_date = $pay_date." ".date("H:i:s");

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $parent = $this->getParent($user_name);
            $file_name1 = "-";

            if ($request->hasFile('pay_image')) {
                //
                $photo = $request->file('pay_image');
                $extension = $photo->getClientOriginalExtension();
                $file_name1 = $trans_id."_BANK.".$extension;
                $photo->move(public_path("/uploadbankreceipt"), $file_name1);
            }

            $ar1 = ['trans_id' => $trans_id, 'bill_no' => $bill_no, 'user_name' => $user_name_p, 'pay_type' => "D",
                        'pay_amount' => $pay_amt, 'pay_mode' => $pay_mode, 'pay_option' => $pay_option, 'pay_image' => $file_name1,
                        'from_user' => $parent, 'pay_date' => $pay_date, 
                        'agent_name' => $user_name, 'pay_remarks' => "PAID_PAYMENT", 'pay_status' => '0', 
                        'created_at' => $date_time, 'updated_at' => $date_time];
            

            $i = $pay_1->insert($ar1);

            if($i > 0)
            {
                $z = 0;
                $op = "Amount is paid Successfully....";
            }
            
        }
        else if($z1 == 1)
        {
            $z = 1;
            $op = "User is not AGENT";
        }
        else if($z1 == 2)
        {
            $z = 2;
            $op = "Invalid API token..";
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);
    }

    public function getUserPaidAmount(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $user_name_p = trim($request->user_name);

        $z1 = 0;
        $de_tot = 0;
        $ce_tot = 0;
        $pe_tot = 0;
        $area_name = "NONE";
        $user_details = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $d1 = $area_1->select('area_name')->where('user_name', '=', $user_name)->where('area_status', '=', '1')->get();

            if($d1->count() > 0)
            {
                $area_name = $d1[0]->area_name;
            }

        
            $parent = $this->getParent($user_name);

            $d2 = CollectionCommon::getUser($parent, $area_name);

            //---------------------------------------

            $user_name_p = strtoupper($user_name_p);

            
            // debit (taken)
            $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name_p])
                        ->where('pay_type', '=', 'C')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // credit (paid)
            $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name_p])
                        ->where('pay_type', '=', 'D')
                        ->where('pay_status', '=', '1')
                        ->sum('pay_amount'); 

            // round two digits fraction
            $de_tot = round($de_tot, 2);
            $ce_tot = round($ce_tot, 2);

            $pe_tot = floatval($de_tot) - floatval($ce_tot);

            $de_tot = $this->convertNumber($de_tot);
            $ce_tot = $this->convertNumber($ce_tot);
            $pe_tot = $this->convertNumber($pe_tot);
            
            $user_details = ['user_name' => $user_name_p, 'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot];

        }
        else 
        {
            $user_details = ['user_name' => $user_name_p, 'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot];
        }

        return response()->json($user_details); 
       
    }

    public function getReport(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $de_tot = 0;
        $ce_tot = 0;
        $pe_tot = 0;
        $area_name = "NONE";
        $user_details = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $user_name = strtoupper($user_name);

            $d1 = $pay_1->whereRaw('upper(agent_name) = ?',[$user_name])
                        ->where('pay_type', '=', 'D')
                        ->orderBy('id', 'desc')->limit(25)->get();
            
            return view('android.ag_collection_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'collection' => $d1]);
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
       
    }

    public function report_1(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];
        $area_name = "NONE";

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $d1 = $area_1->select('area_name')->where('user_name', '=', $user_name)->where('area_status', '=', '1')->get();

            if($d1->count() > 0)
            {
                $area_name = $d1[0]->area_name;
            }

            // Get parent
            $parent = $this->getParent($user_name);

            $d2 = CollectionCommon::getUser($parent, $area_name);

            return view('android.ag_collection_rp_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'user1' => $d2, 'area_name' => $area_name]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function report_2(Request $request)
	{
        $pay_1 = new PaymentLedgerDetails;

        $auth_token = trim($request->auth_token);
        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $z1 = 0;
        $de_tot = 0;
        $ce_tot = 0;
        $pe_tot = 0;
        $area_name = "NONE";
        $user_details = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "AGENT") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only AGENT..
            $user_name = strtoupper($user_name);
            $f_date = $date_1." 00:00:00";
            $t_date = $date_2." 23:59:59";

            $d1 = $pay_1->whereRaw('upper(agent_name) = ?',[$user_name])
                        ->whereBetween('pay_date', [$f_date, $t_date])
                        ->where('pay_type', '=', 'D')
                        ->orderBy('id', 'desc')->limit(25)->get();
            
            return view('android.ag_collection_rp_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'collection' => $d1]);
        }
        else if($z1 == 1)
        {
            echo "User is not AGENT";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
       
    }

    public function getParent($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $parent = "admin";

        $d1 = $uacc_1->select('parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $parent = $d1[0]->parent_name;
        }
        
        return $parent;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
