<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\SmsInterface;
use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;
use App\Models\UserChainDetails;

class UserPaymentRequestController extends Controller
{
    //
    public function index(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        
        $data1 = $upay_1->select('trans_id', 'user_amount', 'user_req_date', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks','trans_status')
        ->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(6)->get();

        return view('user.userpaymentrequest', ['pay1' => $data1, 'user' => $ob]);
        
    }

    public function index_retailer(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        
        $data1 = $upay_1->select('trans_id', 'user_amount', 'user_req_date', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks','trans_status')
        ->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(6)->get();

        return view('retailer.payment_request', ['pay1' => $data1, 'user' => $ob]);
        
    }

    public function index_apipartner(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
        $user_name = $ob->user;

        
        $data1 = $upay_1->select('trans_id', 'user_amount', 'user_req_date', 'grant_user_amount', 'grant_date', 'payment_mode', 'user_remarks','trans_status')
        ->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(6)->get();

        return view('user.userpaymentrequest_apipartner', ['pay1' => $data1, 'user' => $ob]);
        
    }

   

    public function store(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
        $user_code_req = $ob->code;
        $user_name_req = $ob->user;

        // Post Data
        //$user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_code = $user_code_req;
        $user_name = $user_name_req;
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        // Insert Data
        $cnt = $user_1->where('user_name', '=', $user_name_req)->count();
        if($cnt > 0)
        {
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = $user_2->select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            $payment_mode = "FUND_TRANSFER";

            // Check Already Requested Amount Pending
            $cx = $upay_1->where([['user_name', '=', $user_name_req], ['trans_status', '=', 0]])->count();
            if($cx == 0)
            {
                 // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name_req;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = "";
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 0;
            
                $upay_1->save();
            
                $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Payment Request has been Sent...</label>";

                // Send Sms...
                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_grnt)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".$this->convertNumber($user_amount)." is requested from ".$user_name_req." at ".date('d/m/Y H:i:s');
                    SmsInterface::callSms($user_name_req, $dx2[0]->user_mobile, $msg);
                }
            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Requested Amount is Already Pending...</label>";
            }   
           
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! User Name does not Exists...</label>";
        }

        
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function store_retailer(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
        $user_code_req = $ob->code;
        $user_name_req = $ob->user;

        // Post Data
        //$user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_code = $user_code_req;
        $user_name = $user_name_req;
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        // Insert Data
        $cnt = $user_1->where('user_name', '=', $user_name_req)->count();
        if($cnt > 0)
        {
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = $user_2->select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            $payment_mode = "FUND_TRANSFER";

            // Check Already Requested Amount Pending
            $cx = $upay_1->where([['user_name', '=', $user_name_req], ['trans_status', '=', 0]])->count();
            if($cx == 0)
            {
                 // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name_req;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = "";
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 0;
            
                $upay_1->save();
            
                $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Payment Request has been Sent...</label>";

                // Send Sms...
                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_grnt)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".$this->convertNumber($user_amount)." is requested from ".$user_name_req." at ".date('d/m/Y H:i:s');
                    $this->sendSms($dx2[0]->user_mobile, $user_name, $msg);
                }
            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Requested Amount is Already Pending...</label>";
            }   
           
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! User Name does not Exists...</label>";
        }

        
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function store_apipartner(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        
        // Validation
        $this->validate($request, [
            'user_amount' => 'required',
            'user_remarks' => 'required'
        ],
        [
            'user_amount.required' => ' The Amount is required.',
            'user_remarks.required' => ' The remarks is required.',
        ]);

        $ob = GetCommon::getUserDetails($request);
        $user_code_req = $ob->code;
        $user_name_req = $ob->user;

        // Post Data
        //$user_name_grnt = "admin";
        $trans_id = rand(100000,999999);
        $user_code = $user_code_req;
        $user_name = $user_name_req;
        $user_amount = trim($request->user_amount);
        $user_remarks = trim($request->user_remarks);
        $date_time = date("Y-m-d H:i:s");

        // Insert Data
        $cnt = $user_1->where('user_name', '=', $user_name_req)->count();
        if($cnt > 0)
        {
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = $user_2->select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            $payment_mode = "FUND_TRANSFER";

            // Check Already Requested Amount Pending
            $cx = $upay_1->where([['user_name', '=', $user_name_req], ['trans_status', '=', 0]])->count();
            if($cx == 0)
            {
                 // Amount Transfer Details....
                $upay_1->trans_id = $trans_id;
                $upay_1->user_name = $user_name_req;
                $upay_1->user_amount = $this->convertNumber($user_amount);
                $upay_1->user_req_date = $date_time;
                $upay_1->grant_user_name = $user_name_grnt;
                $upay_1->grant_user_amount = "";
                $upay_1->grant_date = $date_time;
                $upay_1->payment_mode = $payment_mode;
                $upay_1->user_remarks = $user_remarks;
                $upay_1->trans_status = 0;
            
                $upay_1->save();
            
                $op = "<label style='color:orange;font-weight:bold;font-size:16px;' >Payment Request has been Sent...</label>";

                // Send Sms...
                $dx2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name_grnt)->get();
                if($dx2->count() > 0)
                {
                    $msg = "Amount ".$this->convertNumber($user_amount)." is requested from ".$user_name_req." at ".date('d/m/Y H:i:s');
                    $this->sendSms($dx2[0]->user_mobile, $user_name, $msg);
                }

            }
            else
            {
                $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Requested Amount is Already Pending...</label>";
            }   
           
        }
        else
        {
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! User Name does not Exists...</label>";
        }

        
        
        //return view('admin.adminremotepayment', ['user1' => $data, 'pay1' => $data1, "output" => $op]);
        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

    public function sendSms($mobile, $user_name, $msg)
    {
        //Sms sending
        $client = new \GuzzleHttp\Client();

       $response = $client->request('GET', 'http://SMS.VAIBAVONLINE.IN/rest/services/sendSMS/sendGroupSms?', [
            'query' => ['AUTH_KEY'    =>  '519a6bdfbaca597c541d7dd8285ca24',
            'message'    =>  $msg,
            'senderId'  =>  'VAIBAV',
            'routeId'   =>  '1',
            'mobileNos'  =>  $mobile,
            'smsContentType'    =>  'english',
            ]
        ]);

        
        $res_code = $response->getStatusCode();
        $res_content =$response->getBody();
        
        //return $res_code;
    }

}
