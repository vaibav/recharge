<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportAdmin;

use App\Models\AllTransaction;
use App\Models\AllRequest;
use App\Models\UserAccountDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

use DB;
use PDF;
use EXCEL;

class AD_Rech_ReqReportController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.ad_rech_req_report_1', ['user' => $ob]);
        
    }
   
   public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $rech_type = trim($request->rech_type);
        
        if($rech_type == "ALL")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }
        else if($rech_type == "RECHARGE")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'RECHARGE')
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }
        else if($rech_type == "BANK_TRANSFER")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }

        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);
        
        return view('admin.ad_rech_req_report_2', ['user' => $ob, 'recharge' => $d1, 'd2' => $d2, 'd3' => $d3]);

    }

    public function viewexcel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $api_1 = new AllTransaction;
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $rech_type = trim($request->rech_type);
        
        if($rech_type == "ALL")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }
        else if($rech_type == "RECHARGE")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'RECHARGE')
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }
        else if($rech_type == "BANK_TRANSFER")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')
                            ->paginate(30);
        }
        

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);
        
        return view('admin.ad_rech_req_report_2', ['user' => $ob, 'recharge' => $d1, 'd2' => $d2, 'd3' => $d3]);

    }
  
}
