<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserNetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;

class AdminApprovalController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_3 = new UserAccountBalanceDetails;

        
        $data = $user_2->where('user_status', '=', '4')->get();

        $data1 = $user_1->all();
        $data2 = $user_3->all();

        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.user_approval_view', ['user1' => $data, 'user2' => $data1, 'user3' => $data2, 'user' => $ob]);
    }

    public function update_user_approval($user_code, Request $request)
	{
        $user_2 = new UserAccountDetails;
        
        $op = "NONE";
        $user_name = "NONE";
        $stat = "1";
        $date_time = date("Y-m-d H:i:s");

        
        $d1 = $user_2->select('user_status')->where('user_code', '=', $user_code)->get();
        if($d1->count() > 0)
        {
            $stat = "1";

            $a = $user_2->where('user_code', '=', $user_code)->update(['user_status' => $stat]);

            $op = "Status is Updated Successfully....";
        }
        else
        {
            $op = "Error! No Update...";
        }

                
        return redirect()->back()->with('msg', $op); 
        
    }

}
