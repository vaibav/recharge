<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

// User
use App\Models\UserAccountDetails;
use App\Models\UserPersonalDetails;
use App\Models\MobileUserDetails;
use App\Models\UserAccountBalanceDetails;

class MobileUserController extends Controller
{
    //
    public function index(Request $request)
	{
        $mob_1 = new MobileUserDetails;

        $ob = $this->getUserDetails($request);


        $d1 = $mob_1->all();

        return view('admin.mobile_user', ['data' => $d1, 'user' => $ob]);
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store_delete(Request $request, $auth_token)
	{
        $mob_1 = new MobileUserDetails;

        $ob = $this->getUserDetails($request);


        $op = 0;
        $z = "NO ACTION";

        $d1 = $mob_1->where('auth_token', '=', $auth_token)->get();

        if($d1->count() > 0)
        {
            $mob_1->where('auth_token', '=', $auth_token)->delete();
            $op = 1;
            $z = "Record is deleted successfully...";
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);;
    }

    public function store_update(Request $request, $auth_token)
	{
        $mob_1 = new MobileUserDetails;
        $user_2 = new UserAccountDetails;

        $ob = $this->getUserDetails($request);


        $op = 0;
        $z = "NO ACTION";

        $d1 = $mob_1->where('auth_token', '=', $auth_token)->get();

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;

            $d2 = $user_2->select('user_name')->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $user_name_1 = $d2[0]->user_name;

                // Update user name
                $mob_1->where('auth_token', '=', $auth_token)->update([ 'user_name' => $user_name_1]);
                
                $op = 1;
                $z = "Record is updated successfully...";
                
            }

            
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);;
    }

}
