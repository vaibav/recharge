<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\GetMobileCommon;
use App\Libraries\RechargeNewMobile;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\ApiProviderDetails;

use App\Models\AllTransaction;

use App\Libraries\Transaction;

class AndroidUserRechargeController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];
        $d3 = [];
        $d4 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);
        $adx = GetMobileCommon::getAdver();

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }


        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        
        if($d1->count() > 0)
        {
            foreach($d1 as $d)
            {
                if($d->net_type_name == "PREPAID") {
                    $pre_code = $d->net_type_code;
                }
                else if($d->net_type_name == "POSTPAID") {
                    $pos_code = $d->net_type_code;
                }
                else if($d->net_type_name == "DTH") {
                    $dth_code = $d->net_type_code;
                }
                
            }
            
            
            $d2 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pre_code], ['net_status', '=', '1']])->get();
            
            $d3 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $pos_code], ['net_status', '=', '1']])->get();

            $d4 = $net_2->select('net_code','net_name')
                    ->where([['net_type_code', '=', $dth_code], ['net_status', '=', '1']])->get();
            
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->retailerDashboard($user_name);

            return view('android.recharge_mobile', ['adver' => $adx, 'network1' => $d2, 'network2' => $d3, 'network3' => $d4, 'recharge' => $rs, 'user_name' => $user_name, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

        		
    }

    public function ret_det(Request $request)
    {

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];
        $d3 = [];
        $d4 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $rs = $this->retailerDashboard($user_name);

            echo $rs;

            
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    // Prepaid recharge
    public function prepaid_recharge(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_1);
        $user_mobile = trim($request->user_mobile_1);
        $user_amount = trim($request->user_amount_1);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;
        

        list($q, $user_name, $user_type1, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        //Store log
        $date_time = date("Y-m-d H:i:s");
        $ip = $request->ip();
        $ua = $request->server('HTTP_USER_AGENT');
        $log_data = $user_name."^^^^^".$date_time."^^^^^GPRS^^^^^".$user_mobile."^^^^^".$ip."^^^^^".$ua;
        file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type1 != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            //$z = RechargeNewMobile::add($data);

            $z = Transaction::recharge_mobile($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    // Postpaid Recharge
    public function postpaid_recharge(Request $request)
	{
        
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_3);
        $user_mobile = trim($request->user_mobile_3);
        $user_amount = trim($request->user_amount_3);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;

        list($q, $user_name, $user_type1, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type1 != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //list($z, $trans_id) = RechargeMobile::add($data);

            //$z = RechargeNewMobile::add($data);

            $z = Transaction::recharge_mobile($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }

    // Dth Recharge
    public function dth_recharge(Request $request)
	{
        // Declarations
        $op = "";
        $z = 0;
        $z1 = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $resx = new stdClass();
        $resulx = [];
        $m_z = 0;

        // Post Data
        $net_type_code = "28";
        $auth_token = trim($request->auth_token);
        $net_code = trim($request->net_code_2);
        $user_mobile = trim($request->user_mobile_2);
        $user_amount = trim($request->user_amount_2);
        $date_time = date("Y-m-d H:i:s");

        //echo $auth_token."<br>".$net_code."---".$user_mobile."---".$user_amount;

        list($q, $user_name, $user_type1, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

         //Store log
         $date_time = date("Y-m-d H:i:s");
         $ip = $request->ip();
         $ua = $request->server('HTTP_USER_AGENT');
         $log_data = $user_name."^^^^^".$date_time."^^^^^GPRS^^^^^".$user_mobile."^^^^^".$ip."^^^^^".$ua;
         //file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);
 

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type1 != "RETAILER") {
                $z1 = 1;
            }
        }


        if($z1 == 0)
        {
             // Post Data
            $data = [];
            $data['net_type_code'] = "30";
            $data['net_code'] = $net_code;
            $data['user_mobile'] = $user_mobile;
            $data['user_amount'] = $user_amount;
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "GPRS";
            $data['api_trans_id'] = "";

            //$z = RechargeNewMobile::add($data);

            $z = Transaction::recharge_mobile($data);

            if($z == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 605;
            }
            else if($z == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 606;
            }
            else if($z == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 607;
            }
            else if($z == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 608;
            }
            else if($z == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 609;
            }
            else if($z == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 610;
            }
            else if($z == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 611;
            }
            else if($z == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
            }
        }
        else if($z1 == 1)
        {
            $op = "Error! You are not retailer";
            $m_z = 612;
            $z = $z1;
        }
        else if($z1 == 2)
        {
            $op = "Error! Invalid API Token...";
            $m_z = 613;
            $z = $z1;
        }
        

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);

    }


    public function getRechargeDetailsRetailer($user_name)
    {
        $rech_1 = new UserRechargeNewDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $rech_1->with(['recharge2'])->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
      // return response()->json($d1, 200);
        
        
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            $reply_id = $f->recharge2->reply_opr_id;
            $reply_date =$f->recharge2->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
               
                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
               
                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->user_balance);
            $c_bal = number_format($c_bal, 2, '.', '');

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 
            if($status == "SUCCESS") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p style='font-size: 14px;margin: 0;'>".$f->rech_amount."</p></div>";
        
            
            $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
            $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$net_name."</p>"; 
            $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$f->rech_mobile."</p>"; 
            
            if($z== 0) {
                $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 11px;'>".$reply_id."</p>"; 
            }
            else 
            {
                $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 10px;'></p>";  
            }

            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p>"; 
            
            $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;margin-top:5px;'>
                                        <i class='fas fa-percentage'></i> &nbsp;".$f->rech_net_per. "--".number_format($f->rech_net_per_amt,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'>
                                        <i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->rech_total,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($c_bal,2, ".", "")."</p>";
            
            $str = $str."</div></div></div>";       
                               
     
        }
        
        return $str; 
    }

    public function retailerDashboard($user_name)
    {
        $rech_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        

        $rs = [];
        $d1 = $rech_1->where('user_name', '=', $user_name)->orderBy('id', 'desc')->limit(10)->get(); 
        //$d1 = $rech_1->orderBy('id', 'desc')->limit(10)->get(); 
        $d2 = $net_2->select('net_code','net_name')->get();
        $d3 = $api_1->select('api_code','api_name')->get();
        
      // return response()->json($d1, 200);
        
        
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($f->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $z = 0;

            $reply_id = $f->reply_id;
            $reply_date =$f->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
               
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
               
                $o_bal = floatval($f->ret_bal) - floatval($f->ret_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
               
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->ret_bal);
            $c_bal = number_format($c_bal, 2, '.', '');

            //percentage amount
            $per = floatval($f->ret_net_per);
            $peramt1 = 0;
            $peramt2 = 0;
            if(floatval($per) != 0)
            {
                $peramt1 = floatval($per) / 100;
                $peramt1 = round($peramt1, 4);
        
                $peramt2 = floatval($f->rech_amount) * floatval($peramt1);
                $peramt2 = round($peramt2, 2);
            }

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 
            if($status == "SUCCESS") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'><i class='small material-icons '>check</i></a><br>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'><i class='small material-icons '>clear</i></a><br>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='small material-icons '>directions_run</i></a><br>"; 
            }

            $str = $str."<p style='font-size: 14px;margin: 0;'>".$f->rech_amount."</p></div>";
        
            
            $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
            $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$net_name."</p>"; 
            $str = $str."<p class = 'text-body' style='margin:3px 5px;font-size: 14px;'>".$f->rech_mobile."</p>"; 
            
            if($z== 0) {
                $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 11px;'>".$reply_id."</p>"; 
            }
            else 
            {
                $str = $str."<p class = 'text-muted' style='margin:3px 5px;font-size: 10px;'></p>";  
            }

            $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$f->rech_date."</p>"; 
            
            $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;margin-top:5px;'>
                                        <i class='fas fa-percentage'></i> &nbsp;".$f->ret_net_per. "--".number_format($peramt2, 2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'>
                                        <i class='fas fa-rupee-sign'></i> &nbsp;".number_format($f->ret_total,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-up'></i> &nbsp;".number_format($o_bal,2, ".", "")."</p>";
            $str = $str."<p class = 'text-muted' style='margin:3px 2px;font-size: 11px;'><i class='far fa-thumbs-down'></i> &nbsp;".number_format($c_bal,2, ".", "")."</p>";
            
            $str = $str."</div></div></div>";       
                               
     
        }
        
        return $str; 
    }

}
