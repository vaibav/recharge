<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\GetAPICommon;
use App\Libraries\RechargeInfo;
use App\Libraries\RechargeOffers;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\AdminOfferDetails;
use App\Models\UserRechargeNewDetails;

use App\Models\RechargeInfoDetails;

class RT_ANDR_RechOfferController extends Controller
{
    //
    public function m_offer(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = trim($request->user_mobile);

        $net_type_code = "0";
        $rdata = [];
        $str = 'No Plan Available for this Mobile..';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getOffer121($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999));

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (!empty($dx)) 
                {
                    if (array_key_exists('records', $dx)) {
                        $data = (array)$dx['records'];
                        
                        foreach($data as $d)
                        {
                            $c = (array)$d;
                            $key = "";
                            $val = "";
                            if (array_key_exists('desc', $c)) {
                                $key = $c['desc'];
                            }
                            if (array_key_exists('rs', $c)) {
                                $val = $c['rs'];
                            }

                            array_push($rdata, ['desc' => $key, 'price' => $val]);
                        }
                        
                    }
                    else
                    {
                        array_push($rdata, ['desc' => "No Plan Available for this Mobile..", 'price' => ""]);
                    }

                }

                //display data
                if(!empty($rdata)) 
                {
                    $str = $str . "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                    foreach($rdata as $d)
                    {
                        $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$d['desc'].'</td>';
                        $str = $str . '<td style = "font-size:14px;padding:4px 4px;">
                        <a class="floating btn-small waves-effect waves-light #ff6d00 blue accent-4 white-text" 
                        id="id_off" style="padding:5px 5px;">'.$d['price'].'</a>';
                        $str = $str . '</td></tr>';
                    }
                    $str = $str . "</tbody>";
                }

                
            }
            else
            {
                $str =  "No Plan Available for this Mobile..";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "No Plan Available for this Mobile..";
        }
    
        return $str;
	
    }

    public function m_plan(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = "0";

        $net_type_code = "0";
        $rdata = [];
        $str = "No Plans Available for this Network...";

        if($net_code != "")
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "MOBILE_PLAN");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getPlan($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999), $net_code);

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (!empty($dx)) 
                {
                    if (array_key_exists('records', $dx)) {
                        $data = (array)$dx['records'];
                        
                        $str = "<thead><tr><th>Description</th><th>Amount</th></tr></thead><tbody>";
                        foreach($data as $key => $value)
                        {
                            $str = $str . '<tr><td class="left-align" style = "font-size:14px;padding:4px 4px;font-weight:bold;background-color: #E8DAEF">'.$key.'</td><td style = "background-color: #E8DAEF"></td>';
    
                            foreach($value as $v)
                            {
                                $str = $str . '<tr>';
                                $sx = "";
                                $sy = "";
                                foreach($v as $k1 => $v1)
                                {
                                   
                                    if($k1 == "desc")
                                    {
                                        $sx = '<td class="left-align" style = "font-size:14px;padding:4px 4px;">'.$v1.'</td>';
                                    }
                                    else if($k1 == "rs")
                                    {
                                        $sy = '<td style = "font-size:14px;padding:4px 4px;">
                                        <a class="floating btn-small waves-effect waves-light #ff6d00 blue accent-4 white-text" 
                                        id="id_off">'.$v1.'</a></td>';
                                    }
                                }
                                $str = $str . $sx.$sy. '</tr>';
                            }
                        }
                        
                    }
                    
                    
                }
            
            }

        }

    
        return $str;
	
    }

    public function m_dth(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = trim($request->user_mobile);

        $net_type_code = "0";
        $str = '';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "DTH_INFO");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::getDth($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999));

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
                    
                    foreach($data as $key => $value)
                    {
                        foreach($value as $key1 => $value1)
                        {
                            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key1.'</td>';
                            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.(string)$value1.'</td></tr>';
                        }
                       
                    }
                    
                }
            }
            else
            {
                $str =  "No Details Available....";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "No Details Available....";
        }
    
        return $str;
	
    }

    public function check_eb(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mobile = trim($request->con_no);

        $net_type_code = "0";
        $cus_amnt = "0";
        $cus_name = "";
        $cus_dued = "";

        $str = '';

        if($net_code != "" && GetAPICommon::checkNumber($mobile))
        {

            list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "BILL_CHECK");
            if($api_code != 0)
            {

                $res_content = RechargeOffers::checkEB($api_code, $net_code, $mobile, $ob->user, "W".rand(10000,99999), $net_code);

                $dx = RechargeOffers::jsonFormatter($res_content);

                $dx = (array)json_decode($res_content);

                if (array_key_exists('records', $dx)) {
                    $data = (array)$dx['records'];
    
                    file_put_contents(base_path().'/public/sample/eb_data.txt',print_r($data,true), FILE_APPEND);
                    if (!empty($data)) 
                    {
                        $data = (array)$data[0];
                        if (array_key_exists('Billamount', $dx)) {
                            $cus_amnt = trim($data['Billamount']);
                            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">Bill Amount</td>';
                        $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$data['Billamount'].'</td></tr>';
                        }
                        else {
                            $cus_amnt = 0;
                        }

                        
    
                        
                        
    
                        foreach($data as $key => $value)
                        {
                            if (strpos($key, 'Name') !== false) {
                                $cus_name = trim($value);
                            }
                            if (strpos($key, 'Due') !== false) {
                                $cus_dued = trim($value);
                            }
    
                            $str = $str . '<tr><td style="border:1px solid #f86c6b;font-size:14px;padding:7px 8px;font-weight:bold;" class="bg-danger align-items-center">'.$key.'</td>';
                            $str = $str . '<td style="border:1px solid #f86c6b;font-size:12px;padding:7px 8px;text-align:left;" class="text-value-sm text-danger">'.$value.'</td></tr>';
    
                        
                        }
                        
                        
                    }
                   
    
                    
                   
                    $str = $str . "";
                }
            }
            else
            {
                $str =  "No Details Available....";
            }

        }
        else if(!GetAPICommon::checkNumber($mobile))
        {
            $str =  "No Details Available....";
        }

        $str = $cus_amnt."*".$cus_name."*".$cus_dued."*".$str;
    
        return $str;
	
    }

    
}
