<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserAccountBalanceDetails;
use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;
use App\Models\UserRechargeNewDetails;

use App\Libraries\GetCommon;
use App\Libraries\RechargeMobile;
use App\Libraries\RechargeNewMobile;
use App\Libraries\ApiUrl;
use App\Libraries\RechargeReportRetailer;

use App\Libraries\Transaction;
use App\Models\AllTransaction;

class RT_Recharge_Controller extends Controller
{
    public function index(Request $request)
    {
        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";

        $prepaid = [];
        $postpaid = [];
        $dth = [];

        $ob = GetCommon::getUserDetails($request);

        $d1 = NetworkTypeDetails::select('net_type_code', 'net_type_name')->get();
        $d2 = NetworkDetails::select('net_code','net_name','net_type_code')->where('net_status', '=', 1)->get();
        $d3 = AllTransaction::with(['request'])->where('user_name', '=', $ob->user)->orderBy('id', 'desc')->limit(10)->get(); 

        foreach($d1 as $d)
        {
            if($d->net_type_name == "PREPAID") {
                $pre_code = $d->net_type_code;
            }
            else if($d->net_type_name == "POSTPAID") {
                $pos_code = $d->net_type_code;
            }
            else if($d->net_type_name == "DTH") {
                $dth_code = $d->net_type_code;
            }
            
        }

        foreach($d2 as $d)
        {
            if($d->net_type_code == $pre_code) {
                array_push($prepaid, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            else if($d->net_type_code == $pos_code) {
                array_push($postpaid, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            else if($d->net_type_code == $dth_code) {
                array_push($dth, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }
            
        }

        $offers = "";
        foreach($ob->offer as $offer)
        {
            $offers .= $offer->offer_details."   ";
        }

      
        return view('retailer.nw_recharge', 
                                ['pre' => $prepaid, 'pos' => $postpaid, 'dth' => $dth, 'recharge' => $d3, 'network' => $d2, 'user' => $ob, 
                                                    'offers' => $offers]);
    }

    public function store_1(Request $request)
    {

        $ob = GetCommon::getUserDetails($request);

        $net_code = trim($request->net_code);
        $mob_no   = trim($request->user_mobile);
        $rech_amt = trim($request->user_amount);
        $net_name = "";

        $d1 = NetworkDetails::select('net_code','net_name')->get();
        $d2 = UserRechargeNewDetails::with(['recharge2'])->where('user_name', '=', $ob->user)->orderBy('id', 'desc')->limit(10)->get(); 

        foreach($d1 as $d)
        {
            if($d->net_code == $net_code) {
                $net_name = $d->net_name;
            }
        }
      
        return view('retailer.nw_recharge_inter', ['net_code' => $net_code, 'net_name' => $net_name, 'mob_no' => $mob_no, 
                                    'rech_amt' => $rech_amt, 'recharge' => $d2, 'network' => $d1, 'user' => $ob]);
    }
   
    public function store_2(Request $request)
	{
               // Declarations
        $op = "";
        $z = 0;
      
        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $data = [];
        $data['net_type_code'] = "0";
        $data['net_code'] = trim($request->net_code);
        $data['user_mobile'] = trim($request->user_mobile);
        $data['user_amount'] = trim($request->user_amount);
        $data['code'] = $ob->code;
        $data['user'] = $ob->user;
        $data['ubal'] = $ob->ubal;
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        //Store log
        $date_time = date("Y-m-d H:i:s");
        $ip = $request->ip();
        $ua = $request->server('HTTP_USER_AGENT');
        $log_data = $ob->user."^^^^^".$date_time."^^^^^WEB^^^^^".$data['user_mobile']."^^^^^".$ip."^^^^^".$ua;
        file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);
        
        //$z = RechargeNewMobile::add($data);

        $z = Transaction::recharge_mobile($data);

        if($z == 0)
        {
            $op = "Recharge Process is Completed Successfully..";
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! Mode web is not Selected...";
        }
        else if($z == 4)
        {   
            $op = "Error! Account is Inactive...";
        }
        else if($z == 5)
        {
            $op = "Error! Account Balance is low from Setup Fee...";
        }
        else if($z == 6)
        {
            $op = "Error! Recharge Status is already Pending for this Mobile No...";
        }
        else if($z == 7)
        {
            $op = "Error! Wait for 10 Minutes...";
        }
        else if($z == 8)
        {
            $op = "Sorry! Server is Temporarily shut down...";
        }
       
        return redirect('dashboard_retailer')->with('result', [ 'output' => $op, 'msg' => $z]);

       
    }

    public function store_temp(Request $request)
    {
               // Declarations
        $op = "";
        $z = 0;
      
        $ob = GetCommon::getUserDetails($request);

        // Post Data
        $data = [];
        $data['net_type_code'] = "0";
        $data['net_code'] = trim($request->net_code);
        $data['user_mobile'] = trim($request->user_mobile);
        $data['user_amount'] = trim($request->user_amount);
        $data['code'] = "5724";
        $data['user'] = "ram";
        $data['ubal'] = "112.38";
        $data['mode'] = "WEB";
        $data['api_trans_id'] = "";

        //Store log
        $date_time = date("Y-m-d H:i:s");
        $ip = $request->ip();
        $ua = $request->server('HTTP_USER_AGENT');
        $log_data = "ram^^^^^".$date_time."^^^^^WEB^^^^^".$data['user_mobile']."^^^^^".$ip."^^^^^".$ua;
        file_put_contents(base_path().'/public/sample/recharge.txt', $log_data.PHP_EOL, FILE_APPEND);
        
        $z = Transaction::recharge_mobile($data);

        if($z == 0)
        {
            $op = "Recharge Process is Completed Successfully..";
        }
        else if($z == 1)
        {  
            $op = "Error! Low user Balance...";
        }
        else if($z == 2)
        {  
            $op = "Error! No Network Line is Selected...";
        }
        else if($z == 3)
        {   
            $op = "Error! Mode web is not Selected...";
        }
        else if($z == 4)
        {   
            $op = "Error! Account is Inactive...";
        }
        else if($z == 5)
        {
            $op = "Error! Account Balance is low from Setup Fee...";
        }
        else if($z == 6)
        {
            $op = "Error! Recharge Status is already Pending for this Mobile No...";
        }
        else if($z == 7)
        {
            $op = "Error! Wait for 10 Minutes...";
        }
        else if($z == 8)
        {
            $op = "Sorry! Server is Temporarily shut down...";
        }
       
        return redirect('dashboard_retailer')->with('result', [ 'output' => $op, 'msg' => $z]);

       
    }

 
}