<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\AllTransaction;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;

class AD_Pay_ReportController extends Controller
{
    //
    public function index(Request $request)
	{
		
        $ob = GetCommon::getUserDetails($request);

        return view('admin.ad_pay_report_1', ['user' => $ob]);

    }

    
    public function view_details(Request $request)
	{
		
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        
        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $trans_type = trim($request->trans_type);

        $rem_tot = 0;
        $rem_cnt = 0;

        $fun_tot = 0;
        $fun_cnt = 0;

        $ref_tot = 0;
        $ref_cnt = 0;

        $d2 = [];

        if($trans_type == "ALL")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                ->orWhere('trans_type', '=', 'REFUND_PAYMENT')
                                ->orWhere('trans_type', '=', 'SELF_PAYMENT');
                            })->paginate(30);

            $d2 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                ->orWhere('trans_type', '=', 'REFUND_PAYMENT')
                                ->orWhere('trans_type', '=', 'SELF_PAYMENT');
                            })->get();
        }
        else if($trans_type == "REMOTE_PAYMENT")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'REMOTE_PAYMENT')->paginate(30);

            $d2 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'REMOTE_PAYMENT')->get();
        }
        else if($trans_type == "FUND_TRANSFER")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'FUND_TRANSFER')->paginate(30);

            $d2 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'FUND_TRANSFER')->get();
        }
        else if($trans_type == "REFUND_PAYMENT")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'REFUND_PAYMENT')->paginate(30);

            $d2 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'REFUND_PAYMENT')->get();
        }
        else if($trans_type == "SELF_PAYMENT")
        {
            $d1 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where('trans_type', '=', 'SELF_PAYMENT')->paginate(30);

            $d2 = AllTransaction::whereBetween('rech_date', [$f_date, $t_date])
                            ->where(function($query) {
                                return $query
                                ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                ->orWhere('trans_type', '=', 'REFUND_PAYMENT')
                                ->orWhere('trans_type', '=', 'SELF_PAYMENT');
                            })->get();
        }
        
        
        foreach($d2 as $d)
        {
            if($d->trans_type == "REMOTE_PAYMENT") {
                $rem_tot = floatval($rem_tot) + floatval($d->ret_total);
                $rem_cnt = floatval($rem_cnt) + 1;
            }

            if($d->trans_type == "FUND_TRANSFER") {
                $fun_tot = floatval($fun_tot) + floatval($d->ret_total);
                $fun_cnt = floatval($fun_cnt) + 1;
            }

            if($d->trans_type == "REFUND_PAYMENT") {
                $ref_tot = floatval($ref_tot) + floatval($d->ret_total);
                $ref_cnt = floatval($ref_cnt) + 1;
            }
        }

         $total = ['rem_tot' => $rem_tot, 'rem_cnt' => $rem_cnt, 'fun_tot' => $fun_tot, 'fun_cnt' => $fun_cnt, 
                    'ref_tot' => $ref_tot, 'ref_cnt' => $ref_cnt];

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('admin.ad_pay_report_2', ['user' => $ob, 'payment' => $d1, 'total' => $total]);

    }

    public function store_update1(Request $request, $trans_id)
	{
        $upay_1 = new UserPaymentDetails;
        $user_2 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);


        $op = 0;
        $z = "NO ACTION";

        $d1 = $upay_1->where('trans_id', '=', $trans_id)->get();

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->user_name;

            $d2 = $user_2->select('user_name')->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $user_name_1 = $d2[0]->user_name;

                // Update user name
                $upay_1->where('trans_id', '=', $trans_id)->update([ 'user_name' => $user_name_1]);
                
                $op = 1;
                $z = "Record is updated successfully...";
                
            }

            
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);;
    }

    public function store_update2(Request $request, $trans_id)
	{
        $upay_1 = new UserPaymentDetails;
        $user_2 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);


        $op = 0;
        $z = "NO ACTION";

        $d1 = $upay_1->where('trans_id', '=', $trans_id)->get();

        if($d1->count() > 0)
        {
            $user_name = $d1[0]->grant_user_name;

            $d2 = $user_2->select('user_name')->where('user_name', '=', $user_name)->get();
            if($d2->count() > 0)
            {
                $user_name_1 = $d2[0]->user_name;

                // Update user name
                $upay_1->where('trans_id', '=', $trans_id)->update([ 'grant_user_name' => $user_name_1]);
                
                $op = 1;
                $z = "Record is updated successfully...";
                
            }

            
        }

        return redirect()->back()->with('result', [ 'output' => $op, 'msg' => $z]);;
    }

}
