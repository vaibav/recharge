<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use stdClass;
use App\Libraries\GetCommon;
use App\Libraries\RechargeReportApipartner;

use App\Models\NetworkDetails;
use App\Models\NetworkTypeDetails;
use EXCEL;

class AP_RechargeReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;

        $d1 = $net_2->select('net_code','net_name')->get();

        $ob = GetCommon::getUserDetails($request);

        $pre_code = "0";
        $pos_code = "0";
        $dth_code = "0";
        $bil_code = "0";
        $mny_code = "0";

        $d1 = $net_1->select('net_type_code', 'net_type_name')->get();
        foreach($d1 as $d)
        {
            if($d->net_type_name == "PREPAID") {
                $pre_code = $d->net_type_code;
            }
            else if($d->net_type_name == "POSTPAID") {
                $pos_code = $d->net_type_code;
            }
            else if($d->net_type_name == "DTH") {
                $dth_code = $d->net_type_code;
            }
            else if($d->net_type_name == "BILL PAYMENT") {
                $bil_code = $d->net_type_code;
            }
            else if($d->net_type_name == "MONEY TRANSFER") {
                $mny_code = $d->net_type_code;
            }
            
        }

        $code = [];
        $code[0] = $pre_code;
        $code[1] = $pos_code;
        $code[2] = $dth_code;

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', '1')
                            ->where(function($query) use ($code){
                                return $query
                                ->where('net_type_code', '=', $code[0])
                                ->orWhere('net_type_code', '=', $code[1])
                                ->orWhere('net_type_code', '=', $code[2]);
                            })->get();

        $ar_web = [];
        foreach($d2 as $d)
        {
            array_push($ar_web, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_web1 = json_encode($ar_web);

        $d3 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $bil_code], ['net_status', '=', '1']])->get();

        $ar_bill = [];
        foreach($d3 as $d)
        {
            array_push($ar_bill, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_bill1 = json_encode($ar_bill);

        //-------------------------------------------

        $d4 = $net_2->select('net_code','net_name')
                            ->where([['net_type_code', '=', $mny_code], ['net_status', '=', '1']])->get();

        $ar_money = [];
        foreach($d4 as $d)
        {
            array_push($ar_money, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
        }

        $ar_money1 = json_encode($ar_money);

        return view('apipartner.rechargereport_1', ['user' => $ob, 'web' => $ar_web1, 'bill' => $ar_bill1, 'money' => $ar_money1]);
        
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
       

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $rech_type = trim($request->rech_type);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);

        $rs = [];
       
        $dc4 = RechargeReportApipartner::getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, "");

      
        
       

        $dc6 = $dc4;

        $d2 = $net_2->select('net_code','net_name')->get();

        //-----------------------------------------------------
        //Total Calculation-------------------------------------------------------
        //recharge
        $wsua_tot = 0;
        $wsut_tot = 0;
        $wfua_tot = 0;
        $wfut_tot = 0;
        $wrea_tot = 0;
        $wret_tot = 0;
        //eb
        $esua_tot = 0;
        $esut_tot = 0;
        $efua_tot = 0;
        $efut_tot = 0;
        $erea_tot = 0;
        $eret_tot = 0;
        //money
        $msua_tot = 0;
        $msut_tot = 0;
        $mfua_tot = 0;
        $mfut_tot = 0;
        $mrea_tot = 0;
        $mret_tot = 0;

        foreach($dc6 as $d)
        {
            
            if($d->trans_type == "RECHARGE")
            {
                $status = "";

                $rech_status = $d->rech_status;
                $rech_option = $d->rech_option;
                $r_tot = $d->ret_total;
                $r_amt = $d->rech_amount;

                if($rech_status == "FAILURE" && $rech_option == "2")
                {      
                    $wfua_tot = floatval($wfua_tot) + floatval($r_amt);
                    $wfut_tot = floatval($wfut_tot) + floatval($r_tot);
                    $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                    $wret_tot = floatval($wret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $wsua_tot = floatval($wsua_tot) + floatval($r_amt);
                    $wsut_tot = floatval($wsut_tot) + floatval($r_tot);
                    $wrea_tot = floatval($wrea_tot) + floatval($r_amt);
                    $wret_tot = floatval($wret_tot) + floatval($r_tot);
                }
                
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;
                $r_amt = 0;
                

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $r_amt = $d->billpayment[0]->con_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $r_amt = $d->billpayment[1]->con_amount;
                }
                
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $efua_tot = floatval($efua_tot) + floatval($r_amt);
                    $efut_tot = floatval($efut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $esua_tot = floatval($esua_tot) + floatval($r_amt);
                    $esut_tot = floatval($esut_tot) + floatval($r_tot);
                    $erea_tot = floatval($erea_tot) + floatval($r_amt);
                    $eret_tot = floatval($eret_tot) + floatval($r_tot);
                }
                
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
               
                $rech_status = "";
                $status = "";
                $r_tot = 0;
                $r_amt = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $r_amt = $d->moneytransfer[0]->mt_amount;
                    
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $r_amt = $d->moneytransfer[1]->mt_amount;
                    
                }
                if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $mfua_tot = floatval($mfua_tot) + floatval($r_amt);
                    $mfut_tot = floatval($mfut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
                else if ($rech_status == "SUCCESS")
                {
                    $msua_tot = floatval($msua_tot) + floatval($r_amt);
                    $msut_tot = floatval($msut_tot) + floatval($r_tot);
                    $mrea_tot = floatval($mrea_tot) + floatval($r_amt);
                    $mret_tot = floatval($mret_tot) + floatval($r_tot);
                }
               
            }                                        
                            
        }

        $total = ['wsua_tot' => $wsua_tot, 'wsut_tot' => $wsut_tot, 'wfua_tot' => $wfua_tot, 'wfut_tot' => $wfut_tot, 
                    'wrea_tot' => $wrea_tot, 'wret_tot' => $wret_tot,
                    'esua_tot' => $esua_tot, 'esut_tot' => $esut_tot, 'efua_tot' => $efua_tot, 'efut_tot' => $efut_tot, 
                    'erea_tot' => $erea_tot, 'eret_tot' => $eret_tot,
                    'msua_tot' => $msua_tot, 'msut_tot' => $msut_tot, 'mfua_tot' => $mfua_tot, 'mfut_tot' => $mfut_tot, 
                    'mrea_tot' => $mrea_tot, 'mret_tot' => $mret_tot];
       
       
        $rs = [];

        foreach($dc4 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('apipartner.rechargereport_2', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'd2' => $d2, 'total' => $total]); 

        
        
    }

    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $rech_type = trim($request->rech_type);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code_1 = trim($request->net_code);
       
        $dc4 = RechargeReportApipartner::getRechargeReportDetails_new($f_date, $t_date, $rech_type, $u_name, $u_status, $u_mobile, $u_amount, $u_net_code_1, "");


        $d2 = $net_2->select('net_code','net_name')->get();
       

        $headings = ['NO', 'MOBILE', 'NETWORK', 'AMOUNT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'API TRN ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL'];

        $j = 1;
         
        $content = [];

        $k = 0;

        foreach($dc4 as $d)
        {
           
            if($d->trans_type == "API_RECHARGE")
            {
                if(sizeof($d->newrecharge1) > 0)
                {
                    $net_name = "";
                    foreach($d2 as $r)
                    {
                        if($d->newrecharge1[0]->net_code == $r->net_code)
                            $net_name = $r->net_name;
                    }

                    $api_name = "";
                    $reply_id = "NA";
                    $reply_date = "";
                   

                    $reply_id = $d->newrecharge2->reply_opr_id;
                    $reply_date = $d->newrecharge2->reply_date;
                   

                    $rech_status = "";
                    $status = "";
                    $o_bal = 0;
                    $u_bal = 0;
                    $r_tot = 0;

                    if($d->trans_option == 1)
                    {
                        $rech_status = $d->newrecharge1[0]->rech_status;
                        $rech_option = $d->newrecharge1[0]->rech_option;
                        $u_bal = $d->newrecharge1[0]->user_balance;
                        $r_tot = $d->newrecharge1[0]->rech_total;
                    }
                    else if($d->trans_option == 2)
                    {
                        $rech_status = $d->newrecharge1[1]->rech_status;
                        $rech_option = $d->newrecharge1[1]->rech_option;
                        $u_bal = $d->newrecharge1[1]->user_balance;
                        $r_tot = $d->newrecharge1[1]->rech_total;
                    }
                    if($rech_status == "PENDING" && $rech_option == "0")
                    {
                        $status = "PENDING";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "PENDING" && $rech_option == "2")
                    {
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }
                    else if($rech_status == "FAILURE" && $rech_option == "2")
                    {      
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                    }
                    else if ($rech_status == "SUCCESS")
                    {
                        $status = "SUCCESS";
                        $o_bal = floatval($u_bal) + floatval($r_tot);
                    }

                    
                    if($d->trans_option == 1)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, 
                        $d->newrecharge1[0]->rech_net_per."-".$d->newrecharge1[0]->rech_net_per_amt."-".$d->newrecharge1[0]->rech_net_surp,
                        $d->newrecharge1[0]->rech_total, $d->trans_id, $d->newrecharge1[0]->api_trans_id, $reply_id,
                        $d->newrecharge1[0]->rech_date, $reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                    else if($d->trans_option == 2)
                    {
                        $content[$k++] = [$j, $d->newrecharge1[0]->rech_mobile, $net_name, 
                        $d->newrecharge1[0]->rech_amount, '',$d->newrecharge1[1]->rech_total,
                        $d->trans_id, '', '','','', $status,
                        number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                    }
                   
                }
                
               
            }
            if($d->trans_type == "BILL_PAYMENT")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->billpayment[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
               

                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

               

                if($d->trans_option == 1)
                {
                    $rech_status = $d->billpayment[0]->con_status;
                    $rech_option = $d->billpayment[0]->con_option;          
                    $r_tot = $d->billpayment[0]->con_total;
                    $u_bal = $d->billpayment[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->billpayment[1]->con_status;
                    $rech_option = $d->billpayment[1]->con_option;          
                    $r_tot = $d->billpayment[1]->con_total;
                    $u_bal = $d->billpayment[1]->user_balance;
                    
                }
                
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "PENDING";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
               
                if($d->trans_option == 1)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount, 
                        $d->billpayment[0]->con_net_per."-".$d->billpayment[0]->con_net_per_amt."-".$d->billpayment[0]->con_net_surp,
                        $d->billpayment[0]->con_total, $d->trans_id, $d->billpayment[0]->api_trans_id, $d->billpayment[0]->reply_opr_id,
                        $d->billpayment[0]->created_at, $d->billpayment[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->billpayment[0]->con_acc_no, $net_name, 
                        $d->billpayment[0]->con_amount,'',
                        $d->billpayment[0]->con_total, $d->trans_id, '', '','','',
                        $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];
                }
               
            } 
            if($d->trans_type == "BANK_TRANSFER")
            {
                $net_name = "";
                foreach($d2 as $r)
                {
                    if($d->moneytransfer[0]->net_code == $r->net_code)
                        $net_name = $r->net_name;
                }

                $api_name = "";
                $rech_status = "";
                $status = "";
                $o_bal = 0;
                $u_bal = 0;
                $r_tot = 0;

                if($d->trans_option == 1)
                {
                    $rech_status = $d->moneytransfer[0]->mt_status;
                    $rech_option = $d->moneytransfer[0]->mt_option;          
                    $r_tot = $d->moneytransfer[0]->mt_total;
                    $u_bal = $d->moneytransfer[0]->user_balance;
                   
                }
                else if($d->trans_option == 2)
                {  
                    $rech_status = $d->moneytransfer[1]->mt_status;
                    $rech_option = $d->moneytransfer[1]->mt_option;          
                    $r_tot = $d->moneytransfer[1]->mt_total;
                    $u_bal = $d->moneytransfer[1]->user_balance;
                }
                if($rech_status == "PENDING" && $rech_option == 1)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "PENDING" && $rech_option == 2)
                {
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                else if($rech_status == "FAILURE"  && $rech_option == 2)
                {      
                    $status = "FAILURE";
                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                }
                else if ($rech_status == "SUCCESS")
                {
                    $status = "SUCCESS";
                    $o_bal = floatval($u_bal) + floatval($r_tot);
                }
                
               
                
                if($d->trans_option == 1)
                {
                   
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                        $d->moneytransfer[0]->mt_amount, 
                        $d->moneytransfer[0]->mt_per."-".$d->moneytransfer[0]->mt_per_amt."-".$d->moneytransfer[0]->mt_surp,
                        $d->moneytransfer[0]->mt_total, $d->trans_id, $d->moneytransfer[0]->api_trans_id, $d->moneytransfer[0]->mt_reply_id,
                        $d->moneytransfer[0]->mt_date, $d->moneytransfer[0]->reply_date, $status, number_format($o_bal,2, ".", ""),
                        number_format($u_bal,2, ".", "")];

                }
                else if($d->trans_option == 2)
                {
                    $content[$k++] = [$j, $d->moneytransfer[0]->bk_acc_no, $net_name, 
                    $d->moneytransfer[0]->mt_amount, '',
                    $d->moneytransfer[0]->mt_total, $d->trans_id, '', '','','',
                    $status, number_format($o_bal,2, ".", ""),
                    number_format($u_bal,2, ".", "")];

                }
               
            }                                        
                  
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = $ob->user."_Recharge_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');


    }
}
