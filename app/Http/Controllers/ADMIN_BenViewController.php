<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankRemitterDetails;

class ADMIN_BenViewController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.ben_view_1', ['user' => $ob]);
        
    }

    public function view(Request $request)
    {
        $ben_1 = new UserBeneficiaryDetails;
        $rem_1 = new UserBankRemitterDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'msisdn' => 'required'
        ],
        [
            'msisdn.required' => ' Customer Mobile No is required.'
            ]);

        $msisdn = trim($request->msisdn);

        $d2 = $rem_1->where('msisdn', '=', $msisdn)->get();

        $d1 = $ben_1->where('msisdn', '=', $msisdn)->paginate(15);

        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('admin.ben_view_2', ['user' => $ob, 'bene' => $d1, 'remitter' => $d2]); 

        
    }
}
