<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserPaymentDetails;
use App\Models\TransactionAllDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\RechargeAllParentDetails; 
use App\Models\UserRechargePaymentParentDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;
use App\Models\UserAccountDetails;

class RechargeAllParentController extends Controller
{
    //
    public function getParentDetails(Request $request)
    {
        $tran_1 = new TransactionAllParentDetails;
        $rech_6 = new RechargeAllParentDetails;
        $rech_p_1 = new UserRechargePaymentParentDetails;
        $rech_p_2 = new UserRechargePaymentParentApipartnerDetails;

        $f_d = trim($request->from_date);
        $t_d = trim($request->to_date);
        
        $f_date = $f_d." 00:00:00";
        $t_date = $t_d." 23:59:59";

        $d1 = $tran_1->select('trans_id', 'user_name', 'trans_type', 'trans_option')->with(['getusertype'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->orderBy('id', 'asc')->get(); 

        $data = [];
        $j = 1;
        $z = 0;
        $date_time = date('Y-m-d H:i:s');

        /*print "<pre>";
        print_r(json_decode($d1, true));
        print "</pre>";*/

        foreach($d1 as $d)
        {
            if($d->trans_type == "WEB_RECHARGE")
            {
                if($d->getusertype->user_type == "DISTRIBUTOR")
                {
                    $d2 = $rech_p_1->where('trans_id', '=', $d->trans_id)
                                    ->where('parent_name', '=', $d->user_name)
                                    ->where('super_parent_name', '=', 'NONE')->get();
                }
                else if($d->getusertype->user_type == "SUPER DISTRIBUTOR")
                {
                    $d2 = $rech_p_1->where('trans_id', '=', $d->trans_id)
                                    ->where('super_parent_name', '=', $d->user_name)->get();
                }
            }
            else if($d->trans_type == "API_RECHARGE")
            {
                $d2 = $rech_p_2->where('trans_id', '=', $d->trans_id)
                    ->where('parent_name', '=', $d->user_name)
                    ->where('super_parent_name', '=', 'NONE')->get();
            }

            //echo response()->json($d, 200);
           // echo "<br><br><br>";

            //echo response()->json($d2, 200);
            //echo "<br><br><br>";

            $e = [];
            foreach($d2 as $r)
            {
                if($d->trans_option == 1)
                {
                    if($r->rech_status == 'PENDING' || $r->rech_status == 'SUCCESS')
                    {
                        //array_push($data, $r);
                        $e = $r;
                        break;
                    }
                }
                else if($d->trans_option == 2)
                {
                    if($r->rech_status == 'FAILURE')
                    {
                        //array_push($data, $r);
                        $e = $r;
                        break;
                    }
                }
            }

            /*echo "<br>";
            print "<pre>";
            print_r(json_decode($d2, true));
            print "</pre>";*/

            $api_trans_id = '';
            $rech_mode = '';
            if($d->trans_type == "API_RECHARGE")
            {
                if(sizeof($e) > 0)
                {
                    $api_trans_id = $e->api_trans_id;
                }
                $rech_mode = 'API';
            }
            else if($d->trans_type == "WEB_RECHARGE")
            {
                $rech_mode = 'WEB';
            }

            $rech_code = 5;
            if(sizeof($e) > 0)
            {
                if($e->rech_status == "PENDING")
                    $rech_code = 0;
                else if($e->rech_status == "SUCCESS")
                    $rech_code = 1;
                else if($e->rech_status == "FAILURE")
                    $rech_code = 2;
            }
            
            
            $cnt = $rech_6->where([['trans_id', '=', $d->trans_id], ['user_name', '=', $d->user_name], ['rech_code', '=', $rech_code]])->count();
            
            if($cnt == 0)
            {
                if(sizeof($e) > 0)
                {
                    $data1 = [ 
                        'trans_id' => $d->trans_id,
                        'api_trans_id' => $api_trans_id,
                        'user_name' => $d->user_name,
                        'net_code' => $e->net_code,
                        'rech_mobile' => $e->rech_mobile,
                        'rech_amount' => $e->rech_amount,
                        'rech_date' => $e->rech_date,
                        'con_name' => '',
                        'con_acc_no' => '',
                        'con_mobile' => '',
                        'con_amount' => '',
                        'bank_acc_no' => '',
                        'bank_acc_name' => '',
                        'bank_name' => '',
                        'bank_ifsc' => '',
                        'bank_acc_mobile' => '',
                        'bank_acc_amount' => '',
                        'rech_net_per' => $e->rech_net_per,
                        'rech_net_surp' => $e->rech_net_surp,
                        'rech_total' => $e->rech_total,
                        'user_balance' => $e->user_balance,
                        'rech_status' => $e->rech_status,
                        'rech_mode' => $rech_mode,
                        'rech_all_mode' => $d->trans_type,
                        'rech_code' => $rech_code,
                        'api_code' => '0',
                        'created_at' => $date_time,
                        'updated_at' => $date_time
                    ];
        
                    $rech_6->insert($data1);
                    $z++;
                }
                
            }

            echo $j."-".$z."......";
            $j++;
            
        }
        
 
        
    }


    public function getUserRechargeDetails(Request $request)
    {
        $rech_6 = new RechargeAllParentDetails;
        $rech_2 = new UserRechargePaymentParentDetails;
        $rech_a_2 = new UserRechargePaymentParentApipartnerDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $u_name = trim($request->user_name);
        $ux_type = trim($request->user_type);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Web Parent Data
        if($ux_type == "DISTRIBUTOR")
        {
            $d1 = $rech_2->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('parent_name', '=', $u_name)
                ->where('super_parent_name', '=', 'NONE')
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 
           
        }
        else if($ux_type == "SUPER DISTRIBUTOR")
        {
            $d1 = $rech_2->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('super_parent_name', '=', $u_name)
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 
            
        }
       

        

        $str = "<h3>NORMAL DATA</h3>";
        $str = $str. "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $re_tot = 0;
        foreach($d1 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $re_tot = floatval($re_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $re_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";


        // API Data
        $d3 = [];
        if($ux_type == "SUPER DISTRIBUTOR")
        {
            $d3 = $rech_a_2->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('parent_name', '=', $u_name)
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 
            
        }
       

     

        $str = $str . "<h3>API DATA</h3>";
        $str = $str . "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $ae_tot = 0;
        foreach($d3 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $ae_tot = floatval($ae_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $ae_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";

        
        $d2 = $rech_6->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('user_name', '=', $u_name)
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 

        $str = $str . "<h3>BACKUP DATA</h3>";
        $str = $str. "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $ce_tot = 0;
        foreach($d2 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $ce_tot = floatval($ce_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $ce_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";

        echo $str;
    }


    public function getUserRechargeParentTotal(Request $request)
    {
        $rech_6 = new RechargeAllParentDetails;
        $upay_1 = new UserPaymentDetails;
        $rech_2 = new UserRechargePaymentParentDetails;
        $rech_a_2 = new UserRechargePaymentParentApipartnerDetails;
        $user_2 = new UserAccountDetails;
        $tran_1 = new TransactionAllParentDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
        $date_time = date("Y-m-d H:i:s");

        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CATEGORY</th><th>RECHARGE_TOTAL</th></tr>";

        $dx = $user_2->select('user_name', 'user_type')
                ->where('user_type', '=', 'DISTRIBUTOR')
                ->orWhere('user_type', '=', 'SUPER DISTRIBUTOR')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $re_tot = 0;
            $ae_tot = 0;
            $ce_tot = 0;
            $ne_tot = 0;
            $d1 = [];
            $d2 = [];
            $d3 = [];
            
            // Web Parent Data
            if($ux_type == "DISTRIBUTOR")
            {
                $re_tot = $rech_2->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(parent_name) = ?',[$ux_name])
                                ->where('super_parent_name', '=', 'NONE')
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('rech_total'); 
            }
            else if($ux_type == "SUPER DISTRIBUTOR")
            {
                $re_tot = $rech_2->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(super_parent_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('rech_total'); 
            }
           

            // API Parent Data
            $ae_tot = 0;
            if($ux_type == "SUPER DISTRIBUTOR")
            {
                $ae_tot = $rech_a_2->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(parent_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('rech_total'); 
               
            }

            

            
            

            $re_tot = round($re_tot, 2);
            $ae_tot = round($ae_tot, 2);

            $ne_tot = floatval($re_tot) + floatval($ae_tot);

            $ne_tot = round($ne_tot, 2);

           /* $str = $str . "<tr><td>".$j."</td>";
            $str = $str . "<td>" . $ux_name . "</td>";
            $str = $str . "<td>" . $ux_type . "</td>";
            $str = $str . "<td>WEB TOTAL</td>";
            $str = $str . "<td>" . $re_tot . "</td></tr>";
            
            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>API TOTAL</td>";
            $str = $str . "<td>" . $ae_tot . "</td></tr>";

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>NET TOTAL</td>";
            $str = $str . "<td>" . $ne_tot . "</td></tr>";*/
            
           

            if($ne_tot != 0)
            {
                // Data insertion process
                $trans_id = rand(100000,999999);

                $arr1 = array('trans_id' => $trans_id, 'user_name' => 'admin', 'user_amount' => $this->convertNumber($ne_tot),
                            'user_req_date' => $t_date, 'grant_user_name' => $ux_name, 'grant_user_amount' => $this->convertNumber($ne_tot),
                            'grant_date' => $t_date, 'payment_mode' => 'USER_RECHARGE', 'user_remarks' => 'RECHARGED AMOUNT', 
                            'trans_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time);
                            
                $upay_1->insert($arr1);

                $arr2 = array('trans_id' => $trans_id, 'user_name' => $ux_name, 'trans_type' => 'USER_RECHARGE',
                            'trans_option' => '1', 'created_at' => $date_time, 'updated_at' => $date_time);
            
                $tran_1->insert($arr2);

                $str = $str . "<tr><td>".$j."</td>";
                $str = $str . "<td>" . $ux_name . "</td>";
                $str = $str . "<td>" . $ux_type . "</td>";
                $str = $str . "<td>BACKUP DATA</td>";
                $str = $str . "<td>" . $ne_tot . "</td></tr>";
                $j++;
            }
            

        }
        


        echo $str;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }

}
