<?php

namespace App\Libraries;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\PercentageCalculation;
use App\Libraries\BankTransferInfo;
use App\Libraries\BankRemitter;
use App\Libraries\BankReply;
use App\Libraries\TransactionBankReply;

use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserAccountDetails;

use App\Models\ServerDetails;
use App\Models\AllTransaction;
use App\Models\AllRequest;

class TransactionBank
{

    /**
     * Add BankTransfer Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add($data)
    {
        // Declarations
        $op = "";
        $z = 0;
        $per = 0;
        $peramt = 0;
        $sur = 0;
        $res_code = 0;
        $api_url = "NONE";
        $res_content = "NONE";
        $msg = "NONE";
        $otp_status ="0";
        $netamt = "0";
        $rep_trans_id = "";
        $remarks ="";
        
        // Post Data
        $r_mode = $data['r_mode'];

        
        $trans_id = $data['client_trans_id'];
        $net_code = "0";
        $bn_id = $data['ben_id'];
        $rem_id = $data['msisdn'];
        $bk_acc_no = $data['beneficiary_account_no'];
        $bk_code = $data['beneficiary_bank_code'];
        $mt_amount = $data['amount'];
        $bk_mode = $data['transfer_type'];
        $api_trans_id = $data['api_trans_id'];

        $user_code = $data['code'];
        $user_name = $data['user'];
        $user_ubal = $data['ubal'];
        $date_time = date("Y-m-d H:i:s");
        $rech_type = "BANK_TRANSFER";
        $tran_api = "TRN_API_1";
        $tran_op = "1";

        if(floatval($mt_amount) <= 5000)
        {
            $net_code = self::getNetworkCode("MONEY TRANSFER");
            $tran_api = "TRN_API_1";
            $tran_op = "1";
        }
        else if(floatval($mt_amount) > 5000)
        {
            $net_code = self::getNetworkCode("MONEY TRANSFER 1");
            $tran_api = "TRN_API_1_1";
            $tran_op = "2";
        }


        // 1.Check User Balance
        if(floatval($mt_amount) > floatval($user_ubal))
        {
            $z = 1;     //Low user Balance...
        }


        // Check Username status
        $z1 = self::checkUserModeandStatus($user_name, $user_ubal, $r_mode);
        if($z1 == 3)
        {
            $z = 2;     // Mode web is not Selected...
        }
        else if($z1 == 4)
        {
            $z = 3;     // Account is Inactivated...
        }
        else if($z1 == 5)
        {
            $z = 4;     // Account Balance is low from Setup Fee...
        }

        // Check Server Status
        $zy = self::checkServerStatus();
        if ($zy == 1)
        {
            $z = 6;     // Recharge Status is already Pending for this Account No
        }

        if($z == 0)
        {
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine($tran_api);

            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);
 
                if($api_url != "NONE")
                     $res_code = 200;

                // Insertion Process..
                list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $mt_amount, $r_mode);

                // Distributor Network Percentage Calculation
                list($parent_type, $parent_name) = self::getParent($user_name);
                list($per_d, $peramt_d, $sur_d, $netamt_d) = PercentageCalculation::getDistributorPercentage($parent_name, $net_code, $mt_amount);

                $per_s ="0";
                $peramt_s = "0";
                $sur_s = "0";
                $netamt_s = "0.00";
                $parent_name_s = "NONE";

                if($parent_type == "DISTRIBUTOR") {
                    // Super Parent Network Percentage Calculation
                     list($parent_type_s, $parent_name_s) = self::getParent($parent_name);
                     list($per_s, $peramt_s, $sur_s, $netamt_s) = PercentageCalculation::getDistributorPercentage($parent_name_s, $net_code, $mt_amount);
                }

                // Array
                $rech = [];
                $rech['trans_id'] = $trans_id;
                $rech['api_trans_id'] = $api_trans_id;

                if($parent_type == "DISTRIBUTOR") {
                    $rech['parent_name'] = $parent_name_s;
                    $rech['child_name'] = $parent_name;
                }
                else if($parent_type == "SUPER DISTRIBUTOR") {
                    $rech['parent_name'] = $parent_name;
                    $rech['child_name'] = $parent_name_s;
                }
                else {
                    $rech['parent_name'] = "-";
                    $rech['child_name'] = "-";
                }

                $rech['user_name'] = $user_name;
                $rech['net_code'] = $net_code;
                $rech['rech_mobile'] = $bk_acc_no;          // rech_mobile is bank account no
                $rech['rech_amount'] = $mt_amount;
                $rech['rech_bank'] = $bk_code."--".$bk_mode;
                $rech['ret_net_per'] = PercentageCalculation::convertNumberFormat($per);
                $rech['ret_net_surp'] = PercentageCalculation::convertNumberFormat($sur);
                $rech['ret_total'] = PercentageCalculation::convertNumberFormat($netamt);
                $rech['ret_bal'] = '0';

                if($parent_type == "DISTRIBUTOR") {
                    $rech['dis_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                    $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                    $rech['dis_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                    $rech['dis_bal'] = '0';
                    $rech['sup_net_per'] = PercentageCalculation::convertNumberFormat($per_s);
                    $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_s);
                    $rech['sup_total'] = PercentageCalculation::convertNumberFormat($netamt_s);
                    $rech['sup_bal'] = '0';
                }
                else if($parent_type == "SUPER DISTRIBUTOR") 
                {
                    $rech['dis_net_per'] = "0";
                    $rech['dis_net_surp'] = "0.00";
                    $rech['dis_total'] = "0.00";
                    $rech['dis_bal'] = '0';
                    $rech['sup_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                    $rech['sup_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                    $rech['sup_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                    $rech['sup_bal'] = '0';
                }
                else {
                    $rech['dis_net_per'] = PercentageCalculation::convertNumberFormat($per_d);
                    $rech['dis_net_surp'] = PercentageCalculation::convertNumberFormat($sur_d);
                    $rech['dis_total'] = PercentageCalculation::convertNumberFormat($netamt_d);
                    $rech['dis_bal'] = '0';
                    $rech['sup_net_per'] = "0.00";
                    $rech['sup_net_surp'] = "0.00";
                    $rech['sup_total'] = "0.00";
                    $rech['sup_bal'] = '0';
                }

                $rech['trans_type'] = "BANK_TRANSFER";
                $rech['api_code'] = $api_code;
                $rech['rech_date'] = $date_time;
                $rech['rech_status'] = "PENDING";
                $rech['rech_option'] = "0";
                $rech['reply_date'] = $date_time;
                $rech['reply_id'] = "-";
                
                $rech['created_at'] = $date_time;
                $rech['updated_at'] = $date_time;

                list($rx_bal, $zx) = self::update_balance($user_name, $netamt, $date_time);

                if($zx > 0)
                {
                    $rech['ret_bal'] = $rx_bal;

                    if($parent_type == "DISTRIBUTOR") {

                        list($dx_bal, $zy) = self::update_balance($parent_name, $netamt_d, $date_time);
                        list($sx_bal, $zz) = self::update_balance($parent_name_s, $netamt_s, $date_time);
                        $rech['dis_bal'] = $dx_bal;
                        $rech['sup_bal'] = $sx_bal;
                    }
                    else if($parent_type == "SUPER DISTRIBUTOR") 
                    {
                        list($sx_bal, $zz) = self::update_balance($parent_name, $netamt, $date_time);
                        $rech['sup_bal'] = $sx_bal;
                    }

                    $c1 = AllTransaction::insert($rech);

                    if($c1 > 0)
                    {
                        if ($res_code == 200)
                        {
                            $res_content = BankRemitter::runURL($api_url, "TRANSFER");
                            $rep_trans_id = "-";

                            if($tran_op == "1") {
                                list($status, $msg, $otp_status, $rep_trans_id, $remarks) = TransactionBankReply::storeTransferupdate_1($res_content, $trans_id);
                            }
                            else if($tran_op == "2") {
                                list($status, $msg, $otp_status, $rep_trans_id, $remarks) = TransactionBankReply::storeTransferupdate_1_1($res_content, $trans_id);
                            }

                            $req = [];
                            $req['trans_id'] = $trans_id;
                            $req['req_id'] = $res_content;
                            $req['reply_id'] = $rep_trans_id;
                            $req['created_at'] = $date_time;
                            $req['updated_at'] = $date_time;

                            $c2 = AllRequest::insert($req);

                            if($status == "FAILURE")
                            {
                                $z = 4;
                                //self::refundBalance($user_name, $netamt);
                            }
                            else  if($status == "PENDING")
                            {
                                $z = 5;
                            }

                            // All Process Over...

                           
                        }

                    }

                }

                
                
            }

            

            

        }

        return array($z, $msg, $otp_status, $rep_trans_id, $remarks, $res_content);
        
    }

    /**
     * Add Money Transfer OTP Details.
     *
     * @param  Array  $data
     * @return int  $z
     */
    public static function add_otp($data)
    {
        $z = 0;
        $message = "";
        
       
        $date_time = date("Y-m-d H:i:s");
        $r_mode = $data['r_mode'];
        $user_name = $data['user_name'];
        $trans_id = $data['u_trans_id'];
        $agent_id = $data['agent_id'];
        $msisdn = $data['msisdn'];
        $rep_trans_id = $data['trans_id'];

        file_put_contents(base_path().'/public/sample/bank_api_otp.txt', print_r($data, true), FILE_APPEND);

        if($z == 0)
        {
            // Do it.... 
            list($api_code, $v) = BankTransferInfo::getAgent1NetworkLine("TRN_API_2");
            if($api_code > 0)
            {
                $api_url = BankTransferInfo::generateBankAPI($api_code, $data);

                $res_code = 201;

                if ($api_url != "NONE")
                {
                    $res_content = BankRemitter::runURL($api_url, "TRANSFER_OTP");

                    list($status, $trans_status, $message) = BankReply::storeTransferupdate_2($res_content, $trans_id);
                    

                    if($status == "FAILURE")
                    {
                        $z = 5;
                        $mt_amt = self::getTransferAmount($trans_id);
                        self::refundBalance($user_name, $mt_amt);
                    }
                    else  if($status == "PENDING")
                    {
                        $z = 6;
                    }

                }
            }

        }
       
        return array($z, $message, $trans_status);
    }


    /**
     * Checking Functions..............................................
     *
     */
    public static function checkUserModeandStatus($user_name, $bal, $r_mode)
    {
        $user_2 = new UserAccountDetails;
        $z = 0;

        // Check User Mode (Enable -web)
        $d1 = $user_2->select('user_rec_mode', 'user_status', 'user_setup_fee')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $user_rec_mode = $d1[0]->user_rec_mode;
            $setup = $d1[0]->user_setup_fee;
            if (strpos($user_rec_mode, $r_mode) !== false) {
                // do nothing
                $x3 = 0;    // test variable
            }
            else
            {
                $z = 3;
            }

            // Check User Account Status
            if($d1[0]->user_status != 1)
            {
                $z = 4;
            }

            // Setup Fee Check..
            if(floatval($setup) >= floatval($bal))
            {
                $z = 5;     //Low user Balance...
            }
        }

        return $z;
    }
    
    public static function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }

  
    // ---- New Functions 
    public static function refundBalance($user_name, $amt)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $u_bal = 0;
        $net_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) + floatval($amt);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $uacc_1->where('user_name', '=', $user_name)->update(['user_balance' => $net_bal]);
            
        }

    }

    public static function getTransferAmount($trans_id)
    {
        $rech_3 = new AllTransaction;
    
        $mt_tot = 0;
       

        $d1 = $rech_3->select('ret_total')->where('trans_id', '=', $trans_id)->get();
        if($d1->count() > 0)
        {
            $mt_tot = $d1[0]->ret_total;
        }

        return $mt_tot;
    }


    public static function getNetworkCode($net_name)
    {
        $net_1 = new NetworkDetails;

        $net_code = "0";

        $d1 = $net_1->select('net_code')->where('net_name', '=', $net_name)->get();

        if($d1->count() > 0)
        {
            $net_code = $d1[0]->net_code;
        }

        return $net_code;
    }

    

    //---------------------new
    public static function getParent($user_name)
    {
        $user_2 = new UserAccountDetails;
        $parent_type = "NONE";
        $parent_name = "NONE";

        $d1 = $user_2->select('parent_type', 'parent_name')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $parent_type = $d1[0]->parent_type;
            $parent_name = $d1[0]->parent_name;
        }

        return array($parent_type, $parent_name);
    }

    public static function update_balance($user_name, $rech_total, $date_time)
    {
        $net_bal = 0;
        $z = 0;
        $d1 = UserAccountBalanceDetails::select('user_balance')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0)
        {
            $u_bal = $d1[0]->user_balance;

            $net_bal = floatval($u_bal) - floatval($rech_total);
            $net_bal = round($net_bal, 2);
            $net_bal = PercentageCalculation::convertNumberFormat($net_bal);

            // Update Balance
            $z = UserAccountBalanceDetails::where('user_name', $user_name)->update(['user_balance' => $net_bal, 'updated_at' => $date_time ]);
        }

        return array($net_bal, $z);
    }

}
