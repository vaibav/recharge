<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Beneficiary;
use App\Libraries\BankTransfer;
use App\Libraries\BankMobileCommon;
use App\Libraries\PercentageCalculation;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBeneficiaryDetails;
use App\Models\UserBankAgentDetails;
use App\Models\NetworkDetails;

class AndroidUserBenDeleteController extends Controller
{
    //
    public function index($msisdn, $auth_token)
	{
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result, $remitter, $beneficiary, $isVerified) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                list($data, $beni) = BankMobileCommon::fetchCustomerDetails($result);

                $cx = BankMobileCommon::add_status($user_name, $msisdn, "BENDEL");

                $d1 = $this->view_user($msisdn);

                return view('android.recharge_money_db_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 
                        'data' => $remitter, 'beni' => $beneficiary, 'beni1' => $beneficiary]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function delete(Request $request)
	{
        
        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
        $ben_acc_no = trim($request->accno);

        $z = 0;
        $z1 = 0;
        $op = "";
        $trans_id = "";
        $ben_id_1 = "0";


        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $z = $this-> checkLocalBendId($ben_acc_no, $msisdn, $ben_id);
            $data = ['user_name' => $user_name, 'msisdn' => $msisdn, 'ben_id' => $ben_id,
                        'beneficiary_id' => $ben_id, 'r_mode' => "WEB" ];
    
            file_put_contents(base_path().'/public/sample/ben_1.txt', $user_name."--".$msisdn."--".$ben_id."--".$ben_acc_no);
            list($z, $msg, $trans_id, $otp_status, $res) = Beneficiary::delete($data);
           
            if($z == 0) {
                $op = $msg;
                $z1 = 0;
            }
            else if($z == 1) {
                $op = "Mode web is not Selected...";
                $z1 = 3;
            }
            else if($z == 2) {
                $op = "Account is Inactivated...";
                $z1 = 4;
            }  
            else if($z == 3) {
                $op = "Account No is Already Registered...";
                $z1 = 5;
            }
            else if($z == 4) {
                $op = $msg;
                $z1 = 6;
            } 
            else if($z == 5) {
                $op = "PENDING...";
                $z1 = 7;
            }
           
        }
        else if($z1 == 1)
        {
            $op = "User is not Retailer";
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
        }

       
        $result = ['status' => $z1, 'message' => $op, 'trans_id' => $trans_id, 'ben_id' => $ben_id];

        return response()->json($result); 
	
    }

    public function delete_otp(Request $request)
	{
        $z1 = 0;
        $z = 0;
        $op = "";

        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
        $ben_id = trim($request->ben_id);
        $trans_id = trim($request->trans_id);
        $otp = trim($request->otp);

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            $data = ['user_name' => $user_name, 'msisdn' => $msisdn, 'trans_id' => $trans_id,
                                'otp' => $otp, 'r_mode' => "WEB" ];
    
            list($z, $msg, $res) = Beneficiary::delete_otp($data);
           
            if($z == 0)
            {
                $op = $msg;
                $z1 = 0;
                $z2 = $this->updateBenStatus($ben_id, $msisdn);
            }
            else if($z == 1)
            {
                $op = "Server is Temporariy Shutdown...";
                $z1 = 3;
            }
            else if($z == 2)
            {
                $op = "Mode web is not Selected...";
                $z1 = 4;
            }
            else if($z == 3)
            {
                $op = "Account is Inactivated...";
                $z1 = 5;
            }
            else if($z == 4)
            {
                $op = "Already Registered...";
                $z1 = 6;
            }
            else if($z == 5)
            {
                $op = $msg;
                $z1 = 7;
            }
            else if($z == 6)
            {
                $op = "Wait! Pending....";
                $z1 = 8;
            }
           
        }
        else if($z1 == 1)
        {
            $op = "User is not Retailer";
        }
        else if($z1 == 2)
        {
            $op = "Invalid API token..";
        }

       
        $result = ['status' => $z1, 'message' => $op, 'trans_id' => $trans_id, 'ben_id' => $ben_id];

        return response()->json($result); 
        
   
    }


    public function view_user($msisdn)
    {
        $ben_1 = new UserBeneficiaryDetails;

        $d1 = $ben_1->where('msisdn', '=', $msisdn)->where('ben_status', '=', 'OTP FAILED')->orderBy('id', 'desc')->get();
        
        return $d1;
    }

    public function checkLocalBendId($ben_acc_no, $msisdn, $bid)
    {
        $user_1 = new UserBeneficiaryDetails;
        $ben_id = 0;
        $id = 0;
        $z = 1;

        $d1 = $user_1->where('ben_acc_no', '=', $ben_acc_no)->where('msisdn', '=', $msisdn)->get();
          
        if($d1->count() > 0)
        {
            $ben_id = $d1[0]->ben_id;
            $id = $d1[0]->id;

            if($ben_id != $bid)
            {
                $user_1->where('id', '=', $id)->update(['ben_id' => $bid]);
            }
            
            $z = 0;
        }

        return $z;
    }

    public function updateBenStatus($ben_id, $msisdn)
    {
        $user_1 = new UserBeneficiaryDetails;
        $id = 0;
        $z = 1;

        $d1 = $user_1->where('ben_id', '=', $ben_id)->where('msisdn', '=', $msisdn)->get();
          
        if($d1->count() > 0)
        {
            $user_1->where('ben_id', '=', $ben_id)->where('msisdn', '=', $msisdn)->update(['ben_status' => 'FAILED']);
            $z = 0;
        }

        return $z;
    }

}
