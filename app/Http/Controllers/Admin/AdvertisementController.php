<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\ServerDetails;

class AdvertisementController extends Controller
{
    //
   public function index(Request $request)
    {
        
        $ob = GetCommon::getUserDetails($request);
        
        return view('admin.ad_entry', ['user' => $ob, 'title' => 'Advertisement']);
        
    }

    public function view(Request $request)
    {
        $ob = GetCommon::getUserDetails($request);
        
        $d1 = AdDetails::get();

        return view('admin.ad_view', ['add' => $d1, 'user' => $ob, 'title' => 'Advertisement']);
        
    }

    public function edit(Request $request, $trans_id)
    {
        $ob = GetCommon::getUserDetails($request);
        
        $d1 = AdDetails::where('trans_id', '=', $trans_id)->get();

        return view('admin.ad_edit', ['add' => $d1, 'user' => $ob, 'title' => 'Advertisement']);
        
    }

    public function store(Request $request)
    {
        $off_1 = new AdDetails;
        $op = 0;
        $file_name1 = "";

        $ob = GetCommon::getUserDetails($request);


        $this->validate($request, [
            'ad_data' => 'required'
        ],
        [
            'ad_data.required' => ' Advertisement Data field is required.'
            ]);


        $trans_id = rand(10000,99999);
        $ad_data = trim($request->ad_data);
        $ad_order = trim($request->ad_order);
        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        

        $d1 = $off_1->where('ad_order', '=', $ad_order)->get();

        if($d1->count() == 0)
        {
            if ($request->hasFile('ad_photo')) {
                //
                $photo = $request->file('ad_photo');
                $extension = $photo->getClientOriginalExtension();
                $file_name1 = $trans_id."_PHOTO.".$extension;
                $photo->move(public_path("/img/adv"), $file_name1);
            }

            // Insert Record... 
            $off_1->trans_id = $trans_id;
            $off_1->ad_data = $ad_data;
            $off_1->ad_order = $ad_order;
            $off_1->from_date = $date_1;
            $off_1->too_date = $date_2;
            $off_1->ad_photo = $file_name1;
            $off_1->ad_status = 1;
            

            $c = $off_1->save();

            $op = 1;
        
        }
        else {
            $op = 3;
        }
       
        

        return redirect()->back()->with('msg', $op);

    }

    public function update(Request $request)
    {
        $off_1 = new AdDetails;
        $op = 0;
        $file_name1 = "";

        $ob = GetCommon::getUserDetails($request);


        $this->validate($request, [
            'ad_data' => 'required'
        ],
        [
            'ad_data.required' => ' Advertisement Data field is required.'
            ]);


        $trans_id = trim($request->trans_id);
        $ad_data = trim($request->ad_data);
        $ad_order = trim($request->ad_order);
        $date_1 = trim($request->from_date);
        $date_2 = trim($request->to_date);
        $ad_status = trim($request->ad_status);
        
        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        

        $d1 = $off_1->where('trans_id', '=', $trans_id)->get();

        if($d1->count() > 0)
        {
            if ($request->hasFile('ad_photo')) {
                //
                $photo = $request->file('ad_photo');
                $extension = $photo->getClientOriginalExtension();
                $file_name1 = $trans_id."_PHOTO.".$extension;
                $photo->move(public_path("/img/adv"), $file_name1);
            }

            $off_1->where('trans_id', '=', $trans_id)->update(['ad_data'=> $ad_data, 
            'ad_order' => $ad_order, 'from_date' => $f_date, 'too_date' => $t_date, 'ad_status' => $ad_status]);

            $op = 1;
        
        }
        else {
            $op = 3;
        }
       
        

        return redirect()->back()->with('msg', $op);

    }

    public function delete(Request $request, $trans_id)
    {
        $off_1 = new AdDetails;
        $op = 0;

        $d1 = $off_1->where('trans_id', '=', $trans_id)->get();

        if($d1->count() > 0)
        {
            
            $off_1->where('trans_id', '=', $trans_id)->delete();

            $op = 1;
        
        }
        else {
            $op = 3;
        }
       
        return redirect()->back()->with('msg', $op);

    }
}