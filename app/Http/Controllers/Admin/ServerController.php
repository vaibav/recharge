<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\ServerDetails;

class ServerController extends Controller
{
    //
    public function index(Request $request)
    {
       
        $ob = GetCommon::getUserDetails($request);

        $d1 = ServerDetails::select('server_status')->get();
       
        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;
        }

        return view('admin.server.index', ['s_status' => $s_status, 'user' => $ob, 'title' => 'Server Status']);
        
    }

     public function store(Request $request)
    {
        $serv_1 = new ServerDetails;
        $op = 0;
        
        $ob = GetCommon::getUserDetails($request);

        $s_status = trim($request->server_status);
        
        $date_time = date("Y-m-d H:i:s");

        $d1 = $serv_1->select('server_status')->get();
       
        if($d1->count() > 0)
        {
            $serv_1->where('server_code', '=', '555')
                               ->update(['server_status' => $s_status, 'updated_at'=>$date_time]);
            $op = 1;
        }

         
        return redirect()->back()->with('msg', $op);
    }
}