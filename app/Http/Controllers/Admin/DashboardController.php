<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;
use App\Models\ApiProviderDetails;

class DashboardController extends Controller
{
    //
    public function index(Request $request)
    {
       
        $ob = GetCommon::getUserDetails($request);

        $str = $this->getData();
       
        return view('admin.ad_dashboard', ['user' => $ob, 'recharge' => $str, 'title' => 'Dashboard']);
        
    }

    public function getIndex(Request $request)
    {
    
        echo $this->getData();;
    }

    public function rech_data(Request $request)
    {

        list($rec_tot, $res_tot, $ref_tot, $trn_tot, $trs_tot, $trf_tot, $mec_tot, $mes_tot, $mef_tot) = $this->getNewRechargeList();
        
        $arr = ['res_tot' => $res_tot, 'ref_tot' => $ref_tot, 'rec_tot' => $rec_tot, 'trn_tot' => $trn_tot,
                    'trs_tot' => $trs_tot, 'trf_tot' => $trf_tot, 'mes_tot' => $mes_tot, 'mef_tot' => $mef_tot, 'mec_tot' => $mec_tot];
        
        return response()->json($arr, 200);
        
    }

    public function chart_data(Request $request)
    {

        list($ars, $arf) = $this->getRechargeGraphData();
        
        $arr = ['res' => $ars, 'ref' => $arf];
        
        return response()->json($arr, 200);
        
    }

    public function getData()
    {
        $d1 = AllTransaction::orderBy('id', 'desc')->limit(15)->get(); 
        $d2 = NetworkDetails::select('net_code','net_name')->get();
        $d3 = ApiProviderDetails::select('api_code','api_name')->get();
        
        $str = "";
        
        $j = 1;
        foreach($d1 as $d)
        {
            $net_name = "";
            $api_name = "";
            $rech_status = "";
            $status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;
            $mode = "WEB";


            foreach($d2 as $r)
            {
                if($d->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            foreach($d3 as $r)
            {
                if($d->api_code == $r->api_code)
                    $api_name = $r->api_name;
            }

            $u_bal = $d->ret_bal;
            $r_tot = $d->ret_total;
            $rech_status = $d->rech_status;
            $rech_option = $d->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) - floatval($r_tot);
                $str = $str."<tr style='background-color:#E8DAEF;'>";
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                $o_bal = floatval($u_bal) + floatval($r_tot);
                $str = $str."<tr>";
            }


            if($d->trans_id != "")
            {
                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);

                $r_l = $matches[0][0];

                $r_l = substr($r_l, -1);

                if($r_l == "R")
                    $mode = "WEB";
                else if($r_l == "A")
                    $mode = "API";
                else if($r_l == "G")
                    $mode = "GPRS";
                else if($r_l == "S")
                    $mode = "SMS";

            }

            //percentage amount
            $per = floatval($d->ret_net_per);
            $peramt1 = 0;
            $peramt2 = 0;
            if(floatval($per) != 0)
            {
                $peramt1 = floatval($per) / 100;
                $peramt1 = round($peramt1, 4);
        
                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                $peramt2 = round($peramt2, 2);
            }

            if($d->trans_type == "REMOTE_PAYMENT" || $d->trans_type == "FUND_TRANSFER" || $d->trans_type == "SELF_PAYMENT")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->parent_name."-->".$d->user_name."</td>";
            }
            else if($d->trans_type == "REFUND_PAYMENT")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."-->".$d->parent_name."</td>";
            }
            else {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
            }
            
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."<br>".$net_name."</td>";
           
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
            if($d->rech_status == "PENDING" || $d->rech_status == "SUCCESS")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                $str = $str."--".$peramt2."";
                $str = $str."--".$d->ret_net_surp."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."<br>".$mode."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>O:".$d->rech_date."<br>C:".$d->reply_date."</td>";
                
            }
            else if($d->rech_status == "FAILURE")
            {
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
               
            }
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";

            if($d->trans_type == "REMOTE_PAYMENT" || $d->trans_type == "FUND_TRANSFER" || $d->trans_type == "SELF_PAYMENT")
            {
                $o_bal = floatval($u_bal) - floatval($d->ret_total);
            }

            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>O:".number_format($o_bal,2, ".", "");
            $str = $str."<br>C:".number_format($u_bal,2, ".", "")."</td>";
           
            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td></tr>";                                     
                      
            $j++;
        }
        
        return $str;
    }

    public function getNewRechargeList()
    {
        $rech_n_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        
        $res_tot = 0;
        $ref_tot = 0;
        $rec_tot = 0;
        $trn_tot = 0;
        $trs_tot = 0;
        $trf_tot = 0;
        
        $query = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'RECHARGE')->get();
        $query1 = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])->where('trans_type', 'BANK_TRANSFER')->get();
        

        $res_tot = $query->where('rech_status', '=', 'SUCCESS')->sum('ret_total'); 

        $res_tot = round($res_tot, 2);

        $ref_tot = $query->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->sum('ret_total');

        $ref_tot = round($ref_tot, 2);

        $rec_tot = floatval($res_tot) + floatval($ref_tot);

        // Money Transfer
        $mes_tot = $query1->where('rech_status', '=', 'SUCCESS')->sum('ret_total'); 
        
        $mes_tot = round($mes_tot, 2);
        
        $mef_tot = $query1->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->sum('ret_total'); 

        $mef_tot = round($mef_tot, 2);

        $mec_tot = floatval($mes_tot) + floatval($mef_tot);

        $trs_tot = $query->where('rech_status', '=', 'SUCCESS')->count();      
                                
        $trf_tot = $query->where('rech_status', '=', 'FAILURE')->where('rech_option', '=', '2')->count();

        $trn_tot = floatval($trs_tot) + floatval($trf_tot);
        
        $res_tot = number_format($res_tot, 2, '.', '');
        
        $rec_tot = number_format($rec_tot, 2, '.', '');

                                 
        return array($rec_tot, $res_tot, $ref_tot, $trn_tot, $trs_tot, $trf_tot, $mec_tot, $mes_tot, $mef_tot);
    }

    public static function getRechargeGraphData()
    {
        $rech_n_1 = new AllTransaction;

        $f_date = date("Y-m-d")." 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        $r_date = date('Y-m-d')." 00:00:00";
        
        $ars = [];
        $arf = [];

        $h = date("H");

        $query = $rech_n_1->where('rech_date','>=', $r_date)->orderBy('rech_date', 'asc')->get();

        //echo $query->count()."<br>";
        
        for($i=0; $i<=$h; $i++)
        {
            $f_date = date("Y-m-d")." ".$i.":00:00";

            if($i == $h) {
                $t_date = date("Y-m-d H:i:s");
            }
            else {
                $t_date = date("Y-m-d")." ".$i.":59:59";
            }

            $dt_1=strtotime($f_date);
            $dt_2=strtotime($t_date);

            $res_tot = 0;
            $ref_tot = 0;
            foreach ($query as $d)
            {
                if (strtotime($d->rech_date) >= $dt_1  && strtotime($d->rech_date) <= $dt_2 )
                {
                    if($d->rech_status == "SUCCESS")
                        $res_tot++;
                    else if($d->rech_status == "FAILURE" && $d->rech_option == '2')
                        $ref_tot++;
                }
                    
            }

            $ars[$i] = $res_tot;
            $arf[$i] = $ref_tot;
            
        }
  
        return array($ars, $arf);
    }
}