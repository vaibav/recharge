<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\ApiProviderDetails;


class ApiProviderMethodController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $api_1 = new ApiProviderDetails;

        $d1 = $api_1->select('api_code','api_name', 'api_method', 'api_status')->orderBy('api_name')->get();
        
        $ob = GetCommon::getUserDetails($request);

        return view('admin.apiprovidermethod_details', ['api' => $d1, 'user' => $ob]);
        
    }

    public function change_method(Request $request, $api_code)
	{
        
        $api_1 = new ApiProviderDetails;

        $d1 = $api_1->select('api_method')->where('api_code', '=', $api_code)->get();
        
        $ob = GetCommon::getUserDetails($request);

        if($d1->count() > 0)
        {
            $api_method = $d1[0]->api_method;

            if($api_method == "GET")
            {
                $api_1->where('api_code', '=', $api_code)->update(['api_method' => "POST"]);
            }
            else if($api_method == "POST")
            {
                $api_1->where('api_code', '=', $api_code)->update(['api_method' => "GET"]);
            }

            $op = "Api Method is Updated successfully....";
        }
        else
        {
            $op = "No Record Found....";
        }

        return redirect()->back()->with('msg', $op);
        
    }

}
