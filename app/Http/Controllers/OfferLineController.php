<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\OfferLineDetails;
use App\Models\ApiProviderDetails;
use App\Models\NetworkDetails;
use App\Models\UserAccountBalanceDetails;

class OfferLineController extends Controller
{
    //
    public function index(Request $request)
	{
        $off_1 = new OfferLineDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $op = "";
        $net_code = 0;
        
        $data1 = $net_2->select('net_code','net_name')->orderBy('net_name', 'asc')->get();
        $data2 = $api_1->select('api_code','api_name')->orderBy('api_name', 'asc')->get();
        $data3 = $off_1->select('offer_line_tr_id', 'net_code', 'api_code', 'offer_type', 'offer_line_status')
                        ->where('net_code', '=', $net_code)->get();
       
        $ob = $this->getUserDetails($request);

        return view('admin.offerline', ['network' => $data1, 'api1' => $data2, 'net1' => $data3, 'net_code' => $net_code, 'user' => $ob]);
    
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function view($net_code, Request $request)
	{
        $off_1 = new OfferLineDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        
        $data1 = $net_2->select('net_code','net_name')->get();
        $data2 = $api_1->select('api_code','api_name')->get();
        $data3 = $off_1->select('offer_line_tr_id', 'net_code', 'api_code', 'offer_type', 'offer_line_status')
                        ->where('net_code', '=', $net_code)->get();
        $op = "";

        $ob = $this->getUserDetails($request);

        return view('admin.offerline', ['network' => $data1, 'api1' => $data2, 'net1' => $data3, 'net_code' => $net_code, 'user' => $ob]);
        
    }

    public function store(Request $request)
	{
        $off_1 = new OfferLineDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;
        $z = 0;
        $op = "";

        $line_tr_id = rand(1000,9999);
        $net_code = trim($request->net_code);
        $api_code = trim($request->api_code);
        $offer_type = trim($request->offer_type);

        $cnt = $off_1->where([['net_code', '=', $net_code], ['api_code', '=', $api_code], ['offer_type', '=', $offer_type]])->count();
        if($cnt > 0)
        {
            $z = 1;
            $op = "<label style='color:red;font-weight:bold;font-size:16px;' >Error! Data is Already Available..</label>";
        }
        else
        {
             // Insert Record... 
             $off_1->offer_line_tr_id = $line_tr_id;
             $off_1->net_code = $net_code;
             $off_1->api_code = $api_code;
             $off_1->offer_type = $offer_type;
             $off_1->offer_line_status = 1;
 
             $off_1->save();
             
             $op = "<label style='color:green;font-weight:bold;font-size:16px;' >Record is inserted Successfully...</label>";

        }
       

        return redirect()->back()->with('result', [ 'output' => $op]);
	
    }


    public function delete($tr_id)
	{
        $off_1 = new OfferLineDetails;
       
        $z = 0;
        $op = "";
        $net_code =  0;


        $cnt = $off_1->where('offer_line_tr_id', '=', $tr_id)->count();
        
        if($cnt > 0)
        {
            
            // Delete Record... 
            $off_1->where('offer_line_tr_id', '=', $tr_id)->delete();
            $op = "Record is deleted Successfully...";
        }
        else
        {
                            
            $op = "No Record Found...";

        }

       
        return redirect()->back()->with('result', [ 'output' => $op]);

        
    }


}
