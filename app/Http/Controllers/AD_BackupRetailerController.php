<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use App\Libraries\GetCommon;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\BackupRechargeDetails;


class AD_BackupRetailerController extends Controller
{
    //
    public function index(Request $request)
	{
        $rech_date = "";

        $ob = GetCommon::getUserDetails($request);

        $d1 = BackupRechargeDetails::select('rech_date')->orderby('id', 'desc')->limit(1)->get();

        if($d1->count() > 0) {
            $rech_date = $d1[0]->rech_date;
        }

        return view('admin.ad_backup_retailer', ['user' => $ob, 'rech_date' => $rech_date]);
        
    }

    public function store(Request $request)
	{
        $tran_1 = new TransactionAllDetails;
        $rech_1 = new UserRechargeNewDetails;
        $back_1 = new BackupRechargeDetails;
    
        $op = 0;
        $j = 0;
        $k = 0;
        $date_time = date('Y-m-d H:i:s');
        $time = "0 Seconds..";
        
        $ob = GetCommon::getUserDetails($request);

        $this->validate($request, [
            'f_date' => 'required', 
        ],
        [
            'f_date.required' => ' From Date is required.'
             ]);

        $date_1 = trim($request->f_date);
        $date_2 = $date_1;

        $f_date = $date_1." 00:00:00";
        $t_date = $date_1." 23:59:59";

        if($f_date != "" && $t_date != "")
        {
            $start = microtime(true);

            $d1 = $rech_1->with(['recharge2', 'backup'])
                                ->whereBetween('rech_date', [$f_date, $t_date])
                                ->where('rech_status', 'SUCCESS')
                                ->get(); 

            $cnt1 = $d1->count();
            $data_x = [];

            $j = 0;
            $k = 0;
            $m = 0;
            foreach($d1 as $d)
            {
                $trans_id = $d->trans_id;
                $user_name = $d->user_name;
                $net_code = $d->net_code;
                $rech_mobile = $d->rech_mobile;
                $rech_amount = $d->rech_amount;
                $rech_total = $d->rech_total;
                $u_bal = $d->user_balance;
                $rech_date = $d->rech_date;
                $rech_status = $d->rech_status;
                $rech_option = $d->rech_option;
                $reply_id = "";
                $api_code = "0";

                if($d->recharge2 != null)
                {
                    $reply_id = $d->recharge2->reply_opr_id;
                    $api_code = $d->recharge2->api_code;
                }
                
                    
                if($rech_status == "SUCCESS")
                {
                    //$cnt = $back_1->where('trans_id', '=', $trans_id)->where('user_name', '=', $user_name)->count();

                    if (strlen($reply_id) > 99) {
                        $reply_id = substr($reply_id, 0, 98);
                    }

                    if($d->backup == null)
                    {
                        $data = [ 
                            'trans_id' => $trans_id,
                            'user_name' => $user_name,
                            'net_code' => $net_code,
                            'rech_mobile' => $rech_mobile,
                            'rech_amount' => $rech_amount,
                            'rech_total' => $rech_total,
                            'user_balance' => $u_bal,
                            'rech_date' => $rech_date,
                            'rech_status' => $rech_status,
                            'reply_id' => $reply_id,
                            'api_code' => $api_code,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        ];
    
                        array_push($data_x, $data);
                        $j++;
                        $k++;

                    }
                    else {
                        $j++;
                        $m++;
                    }

                }
               
            }

            $v = $back_1->insert($data_x);

            if($v > 0)
            {
                //DELETE ENTRIES
                if($cnt1 == $j)
                {
                    $q1 = UserRechargeNewDetails::whereBetween('rech_date', [$f_date, $t_date])->delete();
                    $q2 = UserRechargeNewStatusDetails::whereBetween('created_at', [$f_date, $t_date])->delete();
                    $q3 = TransactionAllDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'WEB_RECHARGE')->delete();
                    $q4 = TransactionAllDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'API_RECHARGE')->delete();

                }
            }

            //DELETE ENTRIES (ALL ENTRIES ALREADY INSERTED IN BACKUP DETAILS)
            if($cnt1 == $m)
            {
                $q1 = UserRechargeNewDetails::whereBetween('rech_date', [$f_date, $t_date])->delete();
                $q2 = UserRechargeNewStatusDetails::whereBetween('created_at', [$f_date, $t_date])->delete();
                $q3 = TransactionAllDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'WEB_RECHARGE')->delete();
                $q4 = TransactionAllDetails::whereBetween('created_at', [$f_date, $t_date])->where('trans_type', 'API_RECHARGE')->delete();

            }

            $time = microtime(true) - $start;

        }
        
        
        $op = $j." Entries backup successfully....<br> Recharge Entries:".$cnt1."-Backup Entries :".$j."<br> Execution Time :".$time;

        return redirect()->back()->with('msg', $op);
    }

    
}
