<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\NetworkDetails;
use App\Models\NetworkTypeDetails;
use App\Models\UserAccountBalanceDetails;

class NetworkController extends Controller
{
    //
    public function index(Request $request)
	{
        $model = new NetworkDetails;
        $model1 = new NetworkTypeDetails;

        $ob = $this->getUserDetails($request);
        
        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $data1 = $model1->select('net_type_code','net_type_name')->get();
        $op = "";

        return view('admin.network', ['network' => $data, 'networktype' => $data1, 'user' => $ob]);
        
		
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function store(Request $request)
	{
        $model = new NetworkDetails;
        $model1 = new NetworkTypeDetails;
        $op = "";


        $this->validate($request, [
            'net_name' => 'required', 'net_short_code' => 'required',
        ],
        [
            'net_name.required' => ' The Network Name field is required.', 
            'net_short_code.required' => ' The Network Short Code field is required.' ]);
            
        $cx = [];
        $cx[0] = $request->net_code;
        $cx[1] = $request->net_short_code;

        $cnt = $model->where(function($query) use ($cx){
                            return $query
                            ->where('net_code', '=', $cx[0])
                            ->orWhere('net_short_code', '=', $cx[1]);
                        })->count();
        
        if($cnt > 0)
        {
            $net_code = $request->net_code;

            $cnt1 = $model->where('net_short_code', '=', $request->net_short_code)->count();

            if($cnt1 == 0)
            {
                // Update Record... 
                $model->where('net_code', '=', $net_code)->update(['net_name'=>$request->net_name, 
                'net_short_code'=>$request->net_short_code, 'net_type_code' => $request->net_type_code, 'net_status' => $request->net_status]);
            }
            else
            {
                $model->where('net_code', '=', $net_code)->update(['net_name'=>$request->net_name, 
                    'net_type_code' => $request->net_type_code, 'net_status' => $request->net_status]);
            }

            
            $op = "Record is updated Successfully...";

            // File Upload
            $file_name1 = $net_code.".jpg";

            if ($request->hasFile('net_photo')) {
                //
                $photo = $request->file('net_photo');
                $extension = $photo->getClientOriginalExtension();
                $file_name1 = $net_code.".".$extension;
                $photo->move(public_path("/networkimage"), $file_name1);
            }
        }
        else
        {
            // Insert Record... 
            $net_code = rand(100,999);
            
            $model->net_tr_id = rand(100,999);
            $model->net_code = $net_code;
            $model->net_name = $request->net_name;
            $model->net_short_code = $request->net_short_code;
            $model->net_type_code = $request->net_type_code;
            $model->net_status = $request->net_status;

            $model->save();
            // File Upload
            $file_name1 = $net_code.".jpg";

            if ($request->hasFile('net_photo')) {
                //
                $photo = $request->file('net_photo');
                $extension = $photo->getClientOriginalExtension();
                $file_name1 = $net_code.".".$extension;
                $photo->move(public_path("/networkimage"), $file_name1);
            }

            $op = "Record is inserted Successfully...";

        }

       

        return redirect()->back()->with('result', [ 'output' => $op]);

		
    }

    public function delete($net_code)
	{
        $model = new NetworkDetails;
        $model1 = new NetworkTypeDetails;
        $op = "";

        $cnt = $model->where('net_code', '=', $net_code)->count();
        
        if($cnt > 0)
        {
            // Delete Record... 
            //$model->where('net_code', '=', $net_code)->delete();
            
            $model->where('net_code', '=', $net_code)
                    ->update(['net_status' => '3']);
            $op = "Record is deleted Successfully...";
        }
        else
        {
                            
            $op = "No Record Found...";

        }

        $data = $model->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $data1 = $model1->select('net_type_code','net_type_name')->get();
        
        return redirect()->back()->with('result', [ 'output' => $op]);
    }
}
