<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use stdClass;

use App\Models\UserAccountDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllDetails;
use App\Models\ApiProviderDetails;
use App\Models\UserRechargeBillDetails;

use PDF;
use EXCEL;

class RechargeBillReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserAccountDetails;
        $net_2 = new NetworkDetails;

        $api_1 = new ApiProviderDetails;
        

        $data1 = $user_1->with(['personal'])->select('user_code','user_name')->where('user_type', '=', 'RETAILER')
                                                           ->orWhere('user_type', '=', 'API PARTNER')->get();
                                
        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $d2 = $api_1->select('api_code','api_name')->where('api_status', '=', 1)->get();
        

        $ob = GetCommon::getUserDetails($request);

        return view('admin.adminrechargebillreport', ['user' => $ob, 'user1' => $data1, 'net' => $d1, 'api' => $d2]);
        
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($request->user_name);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
        /*$d1 = $tran_1->whereBetween('created_at', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BILL_PAYMENT')
                        ->leftJoin('user_recharge_bill_details', 'TransactionAllDetails.trans_id', '=', 'user_recharge_bill_details.trans_id')
                        ->orderBy('id', 'asc')->get(); */
        /*$d1 = $tran_1->with(['billpayment'])
                        ->whereBetween('created_at', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BILL_PAYMENT')
                        ->orderBy('id', 'asc')->get(); */
                        
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        

        
        
       /* $d1 = $tran_1->with(['billpayment']);
        $d1->where('billpayment.user_name', '=', 'RRAGENCY');

        $dx2 = $d1->get();
          
        print "<pre>";
        print_r($dx2);
        print "</pre>";*/
        
       $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        return view('admin.adminrechargebillreport_view', ['user' => $ob, 'recharge' => $dc4, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'api' => $dx3, 'total' => '0']); 

        

        
    }


    public function viewdate_excel(Request $request)
    {
        $net_2 = new NetworkDetails;
        $tran_1 = new TransactionAllDetails;
        $api_1 = new ApiProviderDetails;
        $rech_1 = new UserRechargeBillDetails;
        

        $u_name = "";
        $u_status = "";
        $u_mobile = "";
        $u_amount = "";
        $u_net_code = "";

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
         // Other Requests
         $u_name = trim($request->user_name);
         $u_status = trim($request->rech_status);
         $u_mobile = trim($request->rech_mobile);
         $u_amount = trim($request->rech_amount);
         $u_api_code = trim($request->api_code);
        

        $rs = [];
        /*$d1 = $tran_1->whereBetween('created_at', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BILL_PAYMENT')
                        ->leftJoin('user_recharge_bill_details', 'TransactionAllDetails.trans_id', '=', 'user_recharge_bill_details.trans_id')
                        ->orderBy('id', 'asc')->get(); */
        /*$d1 = $tran_1->with(['billpayment'])
                        ->whereBetween('created_at', [$f_date, $t_date])
                        ->where('trans_type', '=', 'BILL_PAYMENT')
                        ->orderBy('id', 'asc')->get(); */
                        
        $dc1 = $rech_1->whereBetween('created_at', [$f_date, $t_date])->orderBy('id', 'asc')->get();

       

       
        if($u_name != "-" ) 
        {

            $dc1 = $dc1->filter(function ($d) use ($u_name){
                return $d->user_name == $u_name;
            });
        }
      
      

        if($u_status != "-" ) 
        {
            $dc2 = $dc1->filter(function ($d) use ($u_status){
                return $d->con_status == $u_status;
            });
        }
        else
        {
            $dc2 = $dc1;
        }

        if($u_mobile != "" ) 
        {
            $dc3 = $dc2->filter(function ($d) use ($u_mobile){
                return $d->con_acc_no == $u_mobile;
            });
        }
        else
        {
            $dc3 = $dc2;
        }

        if($u_amount != "" ) 
        {
            $dc4 = $dc3->filter(function ($d) use ($u_amount){
                return $d->con_amount == $u_amount;
            });
        }
        else
        {
            $dc4 = $dc3;
        }
        

        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $dx3 = $api_1->select('api_code','api_name')->get();
        
        

        $headings = ['NO', 'USER NAME', 'EB CONN NO', 'NETWORK', 'EB DUE AMT', 'NET.PER(%) / SURPLUS', 'C.AMT', 
                        'TRN ID', 'API TRN ID','REQUEST ID', 'OPR. TRN ID', 'R.DATE', 'UP.DATE', 'STATUS', 
                        'O.BAL', 'C.BAL', 'PROVIDER', 'MODE'];

        $j = 1;
        $str = "";
        $content = [];

        $k = 0;
        foreach($dc4 as $d)
        {
            
                                                        
            $net_name = "";
            foreach($d2 as $r)
            {
                if($d->net_code == $r->net_code)
                    $net_name = $r->net_name;
            }

            $api_name = "";
            foreach($dx3 as $r)
            {
                if($d->api_code == $r->api_code)
                    $api_name = $r->api_name;
            }

            $rech_status = "";
            $status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;

            $rech_option = $d->con_option;
            $rech_status = $d->con_status;
            $r_tot = $d->con_total;
            $u_bal = $d->user_balance;

            

            if($rech_status == "PENDING" && $rech_option == 1)
            {
                $status = "PENDING";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "PENDING" && $rech_option == 2)
            {
                $status = "FAILURE";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if($rech_status == "FAILURE"  && $rech_option == 2)
            {      
                $status = "FAILURE";
                //$o_bal = floatval($u_bal) - floatval($r_tot);
                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                //$u_bal = floatval($u_bal) + floatval($r_tot);
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($u_bal) + floatval($r_tot);
            }
            

            
            if($rech_option == 2 && $rech_status == "FAILURE")
            {
                $content[$k++] = [$j, $d->user_name, $d->con_acc_no, $net_name, $d->con_amount, 
                            '', $d->con_total, $d->trans_id, $d->api_trans_id, $d->con_req_status, 
                             $d->reply_opr_id, '', '', $status, 
                            $o_bal, $u_bal, $api_name, $d->con_mode];
                
            }
            else  
            {

                $content[$k++] = [$j, $d->user_name, $d->con_acc_no, $net_name, $d->con_amount, 
                                    $d->con_net_per."-".$d->con_net_per_amt."-".$d->con_net_surp, $d->con_total, $d->trans_id, 
                                    $d->api_trans_id, $d->con_req_status, $d->reply_opr_id, $d->created_at, $d->reply_date, $status, 
                                    $o_bal, $u_bal, $api_name, $d->con_mode];
                
            }
            
                                                    
            $j++;
        }
        
        $cc = [$headings, $content];

        $tit = "Recharge_bill_details_".date('Y-m-d H:i:s');
        Excel::create($tit, function($excel) use ($cc){

            $excel->sheet('Sheet 1', function($sheet) use ($cc){
    
                $sheet->fromArray($cc[1], null, 'A1', false, false);
                $sheet->prependRow(1, $cc[0]);

            });
        })->export('xls');
        
    }


}
