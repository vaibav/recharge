<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\AllTransaction;
use App\Models\NetworkDetails;

class AD_Pay_AcceptController extends Controller
{
    //
    public function index(Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);

        $d1 = AllTransaction::where('parent_name', '=', $ob->user)
                                ->where('trans_type', 'FUND_TRANSFER')
                                ->where('rech_status', 'PENDING')
                                ->orderBy('id', 'desc')->get();

        return view('admin.ad_pay_accept', ['pay1' => $d1, 'user' => $ob]);
        
    }

    public function store($trans_id, $trans_amt, Request $request)
	{
        
        $ob = GetCommon::getUserDetails($request);
       
        $data = [];
        $result = [];
        $status = 0;
        $op = "NONE";

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
        if($d1->count() > 0)
        {
            $user_name_req = $d1[0]->user_name;

            if($user_name_req != "admin")
            {
                $data['trans_id'] = $trans_id;
                $data['user_name_req'] = $user_name_req;
                $data['user_name_grnt'] = $ob->user;
                $data['user_amount'] = $trans_amt;
                $data['user_remarks'] = $d1[0]->reply_id."- done";
                $data['payment_mode'] = "FUND_TRANSFER";

                $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

                $net_code = '0';
                if($d1->count() > 0) {
                    $net_code = $d1[0]->net_code;
                }

                $data['net_code'] = $net_code;

                list($status, $op) = Stock::paymentGrant($data);
            }
            else
            {
                $status = 1;
                $op = 'Error! Request User Name is not Admin...';
            }

        }
        else
        {
            $status = 2;
            $op = 'Error! Transaction Id is not found...';
        }

        return redirect()->back()->with('result', [ 'output' => $op]);
    }

    public function delete($trans_id, Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $status = 0;
        $op = "NONE";
        $date_time = date("Y-m-d H:i:s");

        $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
        if($d1->count() > 0)
        {
            AllTransaction::where('trans_id', '=', $trans_id)
                                ->update(['rech_status'=> 'FAILURE', 'rech_option' => '2', 'reply_date' => $date_time, 'updated_at'=> $date_time ]);

            $status = 0;
            $op = 'Payment Transfer is cancelled Successfully...';

        }
        else
        {
            $status = 2;
            $op = 'Error! Transaction Id is not found...';
        }

        return redirect()->back()->with('result', [ 'output' => $op]);
    }


}