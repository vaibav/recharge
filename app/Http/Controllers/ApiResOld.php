<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserRechargeResultDetails;
use App\Models\UserRechargeDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeRequestDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentParentDetails;
use App\Models\ApiProviderResultDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\TransactionAllParentDetails;

use App\Models\UserAccountDetails;
use App\Models\UserRechargeApipartnerDetails;
use App\Models\UserRechargeRequestApipartnerDetails;
use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserRechargePaymentParentApipartnerDetails;

use App\Models\BonrixApiResultDetails;
use App\Models\ApiProviderDetails;

use App\Models\UserRechargeNewDetails;

use App\Libraries\CheckServerResult;
use App\Libraries\GetAPIResult;
use App\Libraries\SmsInterface;
use App\Libraries\RechargeNewReply;

class ResponseController extends Controller
{
    //
    public function index()
	{
        //
		
    }

    public function testurl(Request $request)
    {
        $d1 = $request->all();
        
        $str = "";
		foreach($d1 as $key => $value) 
		{
           $str = $str.$key."---".$value."--|--";
        }

        file_put_contents("testapiurl.txt", "welcome-succes-".$str."-".date("Y-m-d H:i:s").PHP_EOL, FILE_APPEND);
    }

    public function testurl1(Request $request)
    {
        $d1 = $request->all();
        
        $str = "";
		foreach($d1 as $key => $value) 
		{
           $str = $str.$key."---".$value."--|--";
        }

        file_put_contents("testapiurl.txt", "welcome-failure-".$str."-".date("Y-m-d H:i:s").PHP_EOL, FILE_APPEND);
        
    }

    public function view(Request $request)
	{
        $res_1 = new UserRechargeResultDetails;
        $bon_1 = new BonrixApiResultDetails;
        $op = "done..";

        $data = [];
        
        for($j = 0; $j<=12; $j++)
		{
			$data[$j] = "-";
		}

        $d1 = $request->all();
        
        $i = 1;
		foreach($d1 as $key => $value) 
		{
            //echo $key."---".$value."<br>";
            if ( strlen($value) > 85)
            {
                $vx = substr($value, 0, 85);
            }
            else
            {
                $vx = $value;
            }
            $data[$i] = trim($vx);
			$i++;
        }

        file_put_contents("testapiresult.txt", print_r($data, true), FILE_APPEND);

        $date_time = date("Y-m-d H:i:s");

        
        //echo "Record is inserted...";

        /*
        |--------------------------------------------------------------------------
        | Result Update Process
        |--------------------------------------------------------------------------
        |   1. Get all Pending rows in recharge_details table 
        |   2. In each row, Get API Result 
        |   3. Check User Account status
        |   4. Get Network Line
        |   5. Generate API using API code
        |   6. Get User Network Percentage Details
        */

        // Get API Code
        $trans_id = "0";
        $api_trans_id = "0";
        $api_code = "0";
        $user_name = "NONE";
        $reply_status = "";
        $reply_oprid = "NA";
        $e = 11;

        list($trans_id, $api_code, $user_name, $api_trans_id, $c, $zxx) = CheckServerResult::checkResult($data);

        

        if($zxx == 0)
        {
            $op = "No match for this result..";
        }

        $res_1->a1 = $data[1];
        $res_1->a2 = $data[2];
        $res_1->a3 = $data[3];
        $res_1->a4 = $data[4];
        $res_1->a5 = $data[5];
        $res_1->a6 = $data[6];
        $res_1->a7 = $data[7];
        $res_1->a8 = $data[8];
        $res_1->a9 = $op;
        $res_1->a10 = $trans_id;
        $res_1->a11 = $api_code;
        
        $res_1->save();

        

        $api_res_url = "NONE";
        $api_res_c = 0;


        //Check if already result is updated... 
        $e = $this->checkResultAlreadyStatus($trans_id, $c);
        if($e == 1)
        {
            // Now Pending... 
            // Get API result Data
            $s_status = "NONE";
            $s_trans_id = "0";
            $s_opr_id = "0";
            if($api_code != "0")
            {
                
                list($s_status, $s_trans_id, $s_opr_id) = GetAPIResult::getAPIResultParameters($data, $api_code);

                //file_put_contents("web_response_result.txt", $trans_id."-".$s_opr_id."-".$s_status."-".$date_time."-".PHP_EOL, FILE_APPEND);

            }

            // Web Recharge Result
            if($c == 1)
            {

                if($s_status == "SUCCESS")
                {
                    // Do updates
                    // 1.Update status in the following tables
                    //      i.User_recharge_details,   2.User_recharge_payment_details   3.parent_recharge_payment_details, 4.user_recharge_request_details
                    $reply_status = "SUCCESS";
                    $reply_oprid = $s_opr_id;
                    $zx = RechargeNewReply::updateSuccess($trans_id, $s_opr_id, $s_status, $date_time);
                    
                    //$zx = $this->updateSuccess($trans_id, $s_opr_id, $s_status, $date_time);
                    
                    if($zx > 0)
                        $op = "Response has done...";
                }
                else if($s_status == "FAILURE")
                {
                    // Do lot of Failure Updates
                    $reply_status = "FAILURE";
                    $reply_oprid = "NA";
                    $zx = RechargeNewReply::updateFailure($trans_id, $s_opr_id, $s_status, $date_time);
                    
                    //$zx = $this->updateFailure($trans_id, $s_opr_id, $s_status, $date_time, $user_name);
                    
                    if($zx > 0)
                        $op = "Response has done...";
                }
            }
            
            // API Recharge Result
            if($c == 2)
            {

                //file_put_contents("api_response_result.txt", $trans_id."-".$s_opr_id."-".$s_status."-".$date_time."-API".PHP_EOL, FILE_APPEND);

                
                if($s_status == "SUCCESS")
                {
                    $reply_status = "SUCCESS";
                    $reply_oprid = $s_opr_id;

                    $zx = RechargeNewReply::updateSuccess($trans_id, $s_opr_id, $s_status, $date_time);

                    //zx = $this->updateSuccessAPI($trans_id, $s_opr_id, $s_status, $date_time);
                    
                    //$zx = 0;
                    list($api_s_url, $api_f_url) = $this->getApipartnerUrl($user_name);

                  
                
                    if (strpos($api_s_url, '<trid>') !== false) {
                        $api_s_url = str_replace("<trid>", $api_trans_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<oprid>') !== false) {
                        $api_s_url = str_replace("<oprid>", $s_opr_id, $api_s_url);
                    }
                    if (strpos($api_s_url, '<status>') !== false) {
                        $api_s_url = str_replace("<status>", $s_status, $api_s_url);
                    }
                    

                    // Check Bonrix
                    if (strpos($api_s_url, 'bonrix.in') !== false) {
                        $bon_1->trans_id = rand(100000,999999);
                        $bon_1->result_url = $api_s_url;
                        $bon_1->result_status = "1";
                        $bon_1->save();
                    }
                    else
                    {
                        $api_res_url = $api_s_url;
                        $api_res_c = 1;
                    }
                    

                    if($zx > 0)
                        $op = "Response has done...";
                }
                else if($s_status == "FAILURE")
                {
                    $reply_status = "FAILURE";
                    $reply_oprid = "NA";

                    // Do lot of Failure Updates
                    $zx = RechargeNewReply::updateFailure($trans_id, $s_opr_id, $s_status, $date_time);

                    //$zx = $this->updateFailureAPI($trans_id, $s_opr_id, $s_status, $date_time, $user_name);
                    

                    //$zx = 0;
                    list($api_s_url, $api_f_url) = $this->getApipartnerUrl($user_name);

                                
                    if (strpos($api_f_url, '<trid>') !== false) {
                        $api_f_url = str_replace("<trid>", $api_trans_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<oprid>') !== false) {
                        $api_f_url = str_replace("<oprid>", $s_opr_id, $api_f_url);
                    }
                    if (strpos($api_f_url, '<status>') !== false) {
                        $api_f_url = str_replace("<status>", $s_status, $api_f_url);
                    }
                    
                    // Check Bonrix
                    if (strpos($api_f_url, 'bonrix.in') !== false) {
                        $bon_1->trans_id = rand(100000,999999);
                        $bon_1->result_url = $api_f_url;
                        $bon_1->result_status = "1";
                        $bon_1->save();
                    }
                    else
                    {
                        $api_res_url = $api_f_url;
                        $api_res_c = 1;
                        
                    }
                
                
                    if($zx > 0)
                        $op = "Response has done...";
                }

            }

            // TNEB Result
            if($c == 3)
            {
                $su_url = "";

                if($s_status == "SUCCESS")
                {
                    $su_url = url('/')."/pendingbill_success/".$trans_id."/".$s_opr_id;
                }
                else if($s_status == "FAILURE")
                {
                    $su_url = url('/')."/pendingbill_failure/".$trans_id."/".$s_opr_id;
                }

                if($su_url != "") {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($su_url);
                    $res_code = $res->getStatusCode();
                    $res_content_1 =$res->getBody();
                }
            }
        }
        else if($e == 2)
        {
            $op = "Error! Recharge has already done....";
        }
        else
        {
            $op = "Error! Transaction Id is Invalid...";
        }
        
       

        // SMS SENDING PROCESS...... 
        if (strpos($trans_id, 'VBS') !== false) 
        {
            list($ux_bal, $ux_mob) = $this->getBalance($user_name);
            list($r_mobile, $r_amount) = $this->getRechDetails($trans_id);
            
            if($ux_mob != "")
            {
                $reply_data = $reply_status." NO:".$r_mobile." AMT:".$r_amount." OPRID:".$reply_oprid." BAL:".$ux_bal;
                
                $zep_code = SmsInterface::callSms($user_name, $ux_mob, $reply_data);

            }
        }
        

        // Response URL for API Partner
        try
        {
            if($c == 2)
            {
                if($api_res_c == 1)
                {
                    file_put_contents("response_update_url.txt", $api_res_url.PHP_EOL, FILE_APPEND);
                    $client = new \GuzzleHttp\Client();
                    $res = $client->get($api_res_url);
                    $res_code = $res->getStatusCode();
                    $res_content_1 =$res->getBody();
                    //file_put_contents("response_update_url_1.txt", $res_content_1.PHP_EOL, FILE_APPEND);
                }
            }
        }
        catch(Exception $excp)
        {
            $op = "Response has done! but Invalid API Result URL...";
            $f_result = ['output' => $op];
            return response()->json($f_result, 200);
        }

        $f_result = ['output' => $op];
        
        return response()->json($f_result, 200);
       
    }

    public function view1(Request $request)
	{
        
        echo "new data";
        echo $request->data1;
        
        
    }

    public function checkResultAlreadyStatus($trans_id, $c)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_n_1 = new UserRechargeNewDetails;
        $rech_a_1 = new UserRechargeApipartnerDetails;
        $rech_b_1 = new UserRechargeBillDetails;

        $e = 0;

        if($c == 1) {
            $d1 = $rech_1->select('rech_status')->where('trans_id', '=', $trans_id)->get();
        }
        else if($c == 2) {
            $d1 = $rech_a_1->select('rech_status')->where('trans_id', '=', $trans_id)->get();
        }
        else if($c == 3) {
            $d1 = $rech_b_1->select('con_status')->where([['trans_id', '=', $trans_id],
                                         ['con_status', '=', 'PENDING'], ['con_option', '=', '1']])->count();
            if($d1 > 0)
                $e = 1;
        }
        
        if($e != 1)
        {
            if (!empty($d1))
            {
                if($d1->count() > 0)
                {
                    if($d1[0]->rech_status == "PENDING")
                    {
                        $e = 1;
                    }
                    else
                    {
                        $e = 2;
                    }
                }
                else
                {
                    $d2 = $rech_n_1->where('trans_id', '=', $trans_id)
                                            ->where('rech_status', '=', 'PENDING')
                                            ->where('rech_option', '=', '0')->count();
                    if($d2 > 0)
                        $e = 1;
                }
            }
            else
            {
                $d2 = $rech_n_1->where('trans_id', '=', $trans_id)
                                        ->where('rech_status', '=', 'PENDING')
                                        ->where('rech_option', '=', '0')->count();
                if($d2 > 0)
                    $e = 1;
            } 
        }
        
        

        return $e;
    }

    
  

    public function getApipartnerUrl($user_name)
    {
        $uacc_1 = new UserAccountDetails;
        $api_s_url = "";
        $api_f_url = "";

        $d1 = $uacc_1->select('user_api_url_1', 'user_api_url_2')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $api_s_url = $d->user_api_url_1;
            $api_f_url = $d->user_api_url_2;
        }

        return array($api_s_url, $api_f_url);
    }

    public function updateSuccess($trans_id, $opr_id, $status, $date_time)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_2 = new UserRechargeRequestDetails;
        $rech_3 = new UserRechargePaymentDetails;
        $rech_4 = new UserRechargePaymentParentDetails;
        $zx = 0;

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            // Update Data
            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status, 
                                        'reply_opr_id' => $opr_id, 
                                        'reply_date' => $date_time, 
                                        'updated_at' => $date_time]);
            $zx++;
        }

        $d2 = $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d2 > 0)
        {
            // Update Data
            $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status,'updated_at' => $date_time]);
            $zx++;
        }

        $d3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d3 > 0)
        {
            $d4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d4 == 0)
            {
                // Update Data
                $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }

        $d5 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d5 > 0)
        {
            $d6 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d6 == 0)
            {
                // Update Data
                $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }

        return $zx;

    }

    public function updateFailure($trans_id, $opr_id, $status, $date_time, $user_name)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_2 = new UserRechargeRequestDetails;
        
        $zx = 0;

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            // Update Data
            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status, 
                                        'reply_opr_id' => $opr_id, 
                                        'reply_date' => $date_time, 
                                        'updated_at' => $date_time]);
            $zx++;
        }

        $d2 = $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d2 > 0)
        {
            // Update Data
            $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status,'updated_at' => $date_time]);
            $zx++;
        }

        $zx = intval($zx) + $this->updateUserPaymentFailure($trans_id, $status, $date_time);

        $zx = intval($zx) + $this->updateParentPaymentFailure($trans_id, $status, $date_time);

        $zt = $this->insertTransaction($trans_id, $user_name);

        return $zx;

    }

    public function updateUserPaymentFailure($trans_id, $status, $date_time)
    {
        $rech_3 = new UserRechargePaymentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;

        $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
        if($dx3->count() > 0)
        {
            $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($dx4 == 0)
            {
                // Update Data
                $rech_total = $dx3[0]->rech_total;
                $user_name = $dx3[0]->user_name;

                // Update Failure
                $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                if($d1->count() > 0)
                {
                    $u_bal = $d1[0]->user_balance;

                    $net_bal = floatval($u_bal) + floatval($rech_total);
                    $net_bal = round($net_bal, 2);
                    $net_bal = $this->convertNumberFormat($net_bal);

                    // Update Balance
                    $uacc_1->where('user_name', '=', $user_name)
                                        ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                    
                    // Insert Payment Data
                    $pay_1 = array('trans_id' => $dx3[0]->trans_id, 
                                        'user_name' => $dx3[0]->user_name, 
                                        'net_code' => $dx3[0]->net_code,
                                        'rech_mobile' => $dx3[0]->rech_mobile, 
                                        'rech_amount' => $dx3[0]->rech_amount, 
                                        'rech_net_per' => $dx3[0]->rech_net_per, 
                                        'rech_net_per_amt' => $dx3[0]->rech_net_per_amt,
                                        'rech_net_surp' => $dx3[0]->rech_net_surp, 
                                        'rech_total' => $dx3[0]->rech_total, 
                                        'user_balance' => $net_bal,
                                        'rech_date' => $date_time,
                                        'rech_status' => $status,
                                        'rech_type' => $dx3[0]->rech_type,
                                        'created_at' => $date_time, 'updated_at' => $date_time);
                    
                    if($rech_3->insert($pay_1))
                    {
                        $zx =1;
                    }
                    
                }

                // End Failure
            }
            
        }

        return $zx;
    }

    public function updateParentPaymentFailure($trans_id, $status, $date_time)
    {
        $rech_3 = new UserRechargePaymentParentDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;

        $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
        if($dx3->count() > 0)
        {
            $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($dx4 == 0)
            {
                foreach($dx3 as $r)
                {
                    // Start Failure
                     // Update Data
                    $rech_total = $r->rech_total;
                    $user_name1 = $r->user_name;
                    $parent_name = $r->parent_name;
                    $super_parent_name = $r->super_parent_name;

                    if($super_parent_name == "NONE")
                    {
                        $user_name = $parent_name;
                    }
                    else
                    {
                        $user_name = $super_parent_name;
                    }

                    $net_bal = 0;
                    // Update Failure
                    $d5 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    if($d5->count() > 0)
                    {
                        $u_bal = $d5[0]->user_balance;

                        $net_bal = floatval($u_bal) + floatval($rech_total);
                        $net_bal = round($net_bal, 2);
                        $net_bal = $this->convertNumberFormat($net_bal);

                        // Update Balance
                        $uacc_1->where('user_name', '=', $user_name)
                                            ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                        
                        // Insert Payment Data
                        $pay_1 = array('trans_id' => $r->trans_id, 
                                            'super_parent_name' => $r->super_parent_name,
                                            'parent_name' => $r->parent_name,
                                            'user_name' => $r->user_name, 
                                            'net_code' => $r->net_code,
                                            'rech_mobile' => $r->rech_mobile, 
                                            'rech_amount' => $r->rech_amount, 
                                            'rech_net_per' => $r->rech_net_per, 
                                            'rech_net_per_amt' => $r->rech_net_per_amt,
                                            'rech_net_surp' => $r->rech_net_surp, 
                                            'rech_total' => $r->rech_total, 
                                            'user_balance' => $net_bal,
                                            'rech_date' => $date_time,
                                            'rech_status' => $status,
                                            'rech_type' => $r->rech_type,
                                            'created_at' => $date_time, 'updated_at' => $date_time);
                        
                        if($rech_3->insert($pay_1))
                        {
                            $zxt = $this->insertTransactionParent($trans_id, $user_name);
                            $zx =1;
                        }
                        
                    }
                    // End Failure For
                }
               

                // End Failure
            }
            
        }

        return $zx;
    }

    public function insertTransaction($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "WEB_RECHARGE";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertTransactionParent($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "WEB_RECHARGE";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }



    // API PARTNER UPDATIONS
    public function updateSuccessAPI($trans_id, $opr_id, $status, $date_time)
    {
        $rech_1 = new UserRechargeApipartnerDetails;
        $rech_2 = new UserRechargeRequestApipartnerDetails;
        $rech_3 = new UserRechargePaymentApipartnerDetails;
        $rech_4 = new UserRechargePaymentParentApipartnerDetails;
        $zx = 0;
        

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            // Update Data
            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status, 
                                        'reply_opr_id' => $opr_id, 
                                        'reply_date' => $date_time, 
                                        'updated_at' => $date_time]);
            $zx++;
        }

        $d2 = $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d2 > 0)
        {
            // Update Data
            $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status,'updated_at' => $date_time]);
            $zx++;
        }

        $d3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d3 > 0)
        {
            $d4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d4 == 0)
            {
                // Update Data
                $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }

        $d5 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d5 > 0)
        {
            $d6 = $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($d6 == 0)
            {
                // Update Data
                $rech_4->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                    ->update(['rech_status' => $status, 'updated_at' => $date_time]);
                $zx++;
            }
            
        }

        return $zx;

    }

    public function updateFailureAPI($trans_id, $opr_id, $status, $date_time, $user_name)
    {
        $rech_1 = new UserRechargeApipartnerDetails;
        $rech_2 = new UserRechargeRequestApipartnerDetails;
        
        $zx = 0;

        $d1 = $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d1 > 0)
        {
            // Update Data
            $rech_1->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status, 
                                        'reply_opr_id' => $opr_id, 
                                        'reply_date' => $date_time, 
                                        'updated_at' => $date_time]);
            $zx++;
        }

        $d2 = $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->count();
        if($d2 > 0)
        {
            // Update Data
            $rech_2->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])
                                ->update(['rech_status' => $status,'updated_at' => $date_time]);
            $zx++;
        }

        $zx = intval($zx) + $this->updateUserPaymentFailureAPI($trans_id, $status, $date_time);

        $zx = intval($zx) + $this->updateParentPaymentFailureAPI($trans_id, $status, $date_time);

        $zt = $this->insertTransactionAPI($trans_id, $user_name);

        return $zx;

    }

    public function updateUserPaymentFailureAPI($trans_id, $status, $date_time)
    {
        $rech_3 = new UserRechargePaymentApipartnerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;

        $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
        if($dx3->count() > 0)
        {
            $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($dx4 == 0)
            {
                // Update Data
                $rech_total = $dx3[0]->rech_total;
                $user_name = $dx3[0]->user_name;

                // Update Failure
                $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                if($d1->count() > 0)
                {
                    $u_bal = $d1[0]->user_balance;

                    $net_bal = floatval($u_bal) + floatval($rech_total);
                    $net_bal = round($net_bal, 2);
                    $net_bal = $this->convertNumberFormat($net_bal);

                    // Update Balance
                    $uacc_1->where('user_name', '=', $user_name)
                                        ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                    
                    // Insert Payment Data
                    $pay_1 = array('trans_id' => $dx3[0]->trans_id,
                                        'api_trans_id' => $dx3[0]->api_trans_id, 
                                        'user_name' => $dx3[0]->user_name, 
                                        'net_code' => $dx3[0]->net_code,
                                        'rech_mobile' => $dx3[0]->rech_mobile, 
                                        'rech_amount' => $dx3[0]->rech_amount, 
                                        'rech_net_per' => $dx3[0]->rech_net_per, 
                                        'rech_net_per_amt' => $dx3[0]->rech_net_per_amt,
                                        'rech_net_surp' => $dx3[0]->rech_net_surp, 
                                        'rech_total' => $dx3[0]->rech_total, 
                                        'user_balance' => $net_bal,
                                        'rech_date' => $date_time,
                                        'rech_status' => $status,
                                        'rech_type' => $dx3[0]->rech_type,
                                        'created_at' => $date_time, 'updated_at' => $date_time);
                    
                    if($rech_3->insert($pay_1))
                    {
                        $zx =1;
                    }
                    
                }

                // End Failure
            }
            
        }

        return $zx;
    }

    public function updateParentPaymentFailureAPI($trans_id, $status, $date_time)
    {
        $rech_3 = new UserRechargePaymentParentApipartnerDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $zx = 0;

        $dx3 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'PENDING']])->get();
        if($dx3->count() > 0)
        {
            $dx4 = $rech_3->where([['trans_id', '=', $trans_id], ['rech_status', '=', 'FAILURE']])->count();
            
            if($dx4 == 0)
            {
                foreach($dx3 as $r)
                {
                    // Start Failure
                     // Update Data
                    $rech_total = $r->rech_total;
                    $user_name1 = $r->user_name;
                    $parent_name = $r->parent_name;
                    $super_parent_name = $r->super_parent_name;

                    if($super_parent_name == "NONE")
                    {
                        $user_name = $parent_name;
                    }
                    else
                    {
                        $user_name = $super_parent_name;
                    }

                    // Update Failure
                    $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
                    if($d1->count() > 0)
                    {
                        $u_bal = $d1[0]->user_balance;

                        $net_bal = floatval($u_bal) + floatval($rech_total);
                        $net_bal = round($net_bal, 2);
                        $net_bal = $this->convertNumberFormat($net_bal);

                        // Update Balance
                        $uacc_1->where('user_name', '=', $user_name)
                                            ->update(['user_balance'=>$net_bal, 'updated_at'=>$date_time]);
                        
                        // Insert Payment Data
                        $pay_1 = array('trans_id' => $r->trans_id,
                                            'api_trans_id' => $r->api_trans_id, 
                                            'super_parent_name' => $r->super_parent_name,
                                            'parent_name' => $r->parent_name,
                                            'user_name' => $r->user_name, 
                                            'net_code' => $r->net_code,
                                            'rech_mobile' => $r->rech_mobile, 
                                            'rech_amount' => $r->rech_amount, 
                                            'rech_net_per' => $r->rech_net_per, 
                                            'rech_net_per_amt' => $r->rech_net_per_amt,
                                            'rech_net_surp' => $r->rech_net_surp, 
                                            'rech_total' => $r->rech_total, 
                                            'user_balance' => $net_bal,
                                            'rech_date' => $date_time,
                                            'rech_status' => $status,
                                            'rech_type' => $r->rech_type,
                                            'created_at' => $date_time, 'updated_at' => $date_time);
                        
                        if($rech_3->insert($pay_1))
                        {
                            $zxt = $this->insertTransactionParentAPI($trans_id, $user_name);
                            $zx =1;
                        }
                        
                    }
                    // End Failure For
                }
               

                // End Failure
            }
            
        }

        return $zx;
    }

    public function insertTransactionAPI($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "API_RECHARGE";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function insertTransactionParentAPI($trans_id, $user_name)
    {
        $tran_1 = new TransactionAllParentDetails;
        $zx = 0;

        $tran_1->trans_id = $trans_id;
        $tran_1->user_name = $user_name;
        $tran_1->trans_type = "API_RECHARGE";
        $tran_1->trans_option = '2';
    
        if($tran_1->save())
        {
            $zx =1;
        }
        
        return $zx;
    }

    public function convertNumberFormat($amt)
    {
        $ramt = 0;
        if($amt != "")
        {
            $ramt = number_format($amt,2, '.', '');
        }
        else
        {
            $ramt = number_format($ramt,2, '.', '');
        }

        return $ramt;
    }

    public function runUrl($url)
    {
        $res_status = 0;
        $res_content = "";

        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        $res_status = $res->getStatusCode();
        $res_content = $res->getBody();
        
        //file_put_contents("testapiurl1.txt", $url.PHP_EOL, FILE_APPEND);
        return array($res_status, $res_content);
    }

    public function runBonrixUrl($url)
    {
        echo "<iframe src=".$url." id='ab' style='display:none;'></iframe>";
        usleep(200000);
        file_put_contents("testapiurl1.txt", $url.PHP_EOL, FILE_APPEND);
    }

    public function convert_text($text) {

        $t = $text;
        
        $specChars = array(
            '!' => '%21',    '"' => '%22',
            '#' => '%23',    '$' => '%24',    '%' => '%25',
            '&' => '%26',    '\'' => '%27',   '(' => '%28',
            ')' => '%29',    '*' => '%2A',    '+' => '%2B',
            ',' => '%2C',    '-' => '%2D',    '.' => '%2E',
            '/' => '%2F',    ':' => '%3A',    ';' => '%3B',
            '<' => '%3C',    '=' => '%3D',    '>' => '%3E',
            '?' => '%3F',    '@' => '%40',    '[' => '%5B',
            '\\' => '%5C',   ']' => '%5D',    '^' => '%5E',
            '_' => '%5F',    '`' => '%60',    '{' => '%7B',
            '|' => '%7C',    '}' => '%7D',    '~' => '%7E',
            ',' => '%E2%80%9A',  ' ' => '%20'
        );
        
        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }
        
        return $t;
    }

    //OTP Message
    public function sendMobileSms($mobile, $message)
    {
        
        //Sms sending
        $client = new \GuzzleHttp\Client();

       $response = $client->request('GET', 'http://SMS.VAIBAVONLINE.IN/rest/services/sendSMS/sendGroupSms?', [
            'query' => ['AUTH_KEY'    =>  '519a6bdfbaca597c541d7dd8285ca24',
            'message'    =>  $message,
            'senderId'  =>  'VAIBAV',
            'routeId'   =>  '1',
            'mobileNos'  =>  $mobile,
            'smsContentType'    =>  'english',
            ]
        ]);

        
        $res_code = $response->getStatusCode();
        $res_content =$response->getBody();
        
        return $res_code;

    }

    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $uper_1 = new UserPersonalDetails;
        $ux_bal = 0;
        $ux_mob = "";

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        $d2 = $uper_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
        foreach($d2 as $d)
        {
            $ux_mob = $d->user_mobile;
        }

        return array($ux_bal, $ux_mob);
    }

    public function getRechDetails($trans_id)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_2 = new UserRechargeApipartnerDetails;

        $r_amount = 0;
        $r_mobile = 0;

        $d1 = $rech_1->select('trans_id', 'rech_mobile', 'rech_amount')->where('trans_id', '=', $trans_id)->get();
        foreach($d1 as $d)
        {
            $r_mobile = $d->rech_mobile;
            $r_amount = $d->rech_amount;
        }

        if($r_mobile == 0)
        {
            $d2 = $rech_2->select('trans_id', 'rech_mobile', 'rech_amount')->where('trans_id', '=', $trans_id)->get();
            foreach($d2 as $d)
            {
                $r_mobile = $d->rech_mobile;
                $r_amount = $d->rech_amount;
            }
        }

        return array($r_mobile, $r_amount);
    }
}
