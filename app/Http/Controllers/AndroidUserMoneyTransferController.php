<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\BankRemitter;
use App\Libraries\Beneficiary;
use App\Libraries\BankTransfer;
use App\Libraries\BankMobileCommon;
use App\Libraries\PercentageCalculation;

use App\Libraries\CyberBankTransfer;

use App\Models\UserBankRemitterDetails;
use App\Models\UserBankAgentDetails;

use App\Models\NetworkTypeDetails;
use App\Models\NetworkDetails;

use App\Models\AllTransaction;
use App\Models\ApiProviderDetails;

use App\Libraries\TransactionBank;

class AndroidUserMoneyTransferController extends Controller
{
    //
    public function index($msisdn, $auth_token)
	{
       
        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result, $remitter, $beneficiary, $isVerified) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                if($isVerified == "1") {
                     //list($data, $beni) = BankMobileCommon::fetchCustomerDetails($remitter);

                    $cx = BankMobileCommon::add_status($user_name, $msisdn, "MONEY");

                   

                    return view('android.recharge_money_tr', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                            "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 'data' => $remitter, 'beni' => $beneficiary]);
                }
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function getRetailerPer($user_name, $mt_amount, $r_mode)
    {
        $net_1 = new NetworkDetails;
        $d1 = [];

        if(floatval($mt_amount) > 5000)
        {
            $d1 = $net_1->select('net_code')->where('net_name', '=', "MONEY TRANSFER 1")->get();
        }
        else
        {
            $d1 = $net_1->select('net_code')->where('net_name', '=', "MONEY TRANSFER")->get();
        }

        $net_code = 0;

        if($d1->count() > 0)
        {
            $net_code = $d1[0]->net_code;
        }

        list($per, $peramt, $sur, $netamt) = PercentageCalculation::getRetailerPercentage($user_name, $net_code, $mt_amount, $r_mode);

        $arr = ['amt' => $mt_amount, 'per' => $per, 'per_amt' => $peramt, 'sur' => $sur, 'netamt' => $netamt];
                    
        
        return response()->json($arr, 200);
    }

    public function store(Request $request)
	{
        $z1 = 0;
        $c_msisdn   = trim($request->c_msisdn);
        $auth_token = trim($request->auth_token);
        $ben_id     = trim($request->ben_id);
        $b_mode     = trim($request->bank_mode);
        $b_amt      = trim($request->cus_amt);

        $b_accno    = trim($request->cus_acc_no);
        $b_name     = trim($request->cus_name);
        $b_code     = trim($request->bank_code);    //IFSC

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

       

        if(floatval($b_amt) <= 5000)
        {
            $trans_id = "VBMG".rand(10000000,99999999);
            $otp_status ="0";
            $rep_trans_id = "";
            $remarks ="";

            if($z1 == 0)
            {
                //Only Retailer...
                if($ben_id != "" && $c_msisdn != "" && $b_mode != "" && $b_amt != "")
                {
                   
                    $data = [   'clientTransId' => $trans_id,
                                'mobileNo' => $c_msisdn,
                                'benId' => $ben_id,
                                'bankName' => "-",
                                'bankIFSC' => $b_code,
                                'accountNo' => $b_accno,
                                'customerName' => $b_name,
                                'routingType' => $b_mode,
                                'amount' => $b_amt,
                                'userCode' => $user_code,
                                'userName' => $user_name,
                                'userBal' => $user_ubal,
                                'apiTransId' => "",
                                'rechMode' => 'WEB'
                            ];
        
                   
                    list($z, $msg, $otp_status) = CyberBankTransfer::add($data);
                }
        
        
                return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z, 
                    'trans_id' => $trans_id ]);

                //echo $z."---".$msg."----".$otp_status;
                
            }
            else if($z1 == 1)
            {
                echo "User is not Retailer";
            }
            else if($z1 == 2)
            {
                echo "Invalid API token..";
            }

        }
        else
        {
            $reminderAmount = floatval($b_amt) % 5000;
            $b_amt          = floatval($b_amt) - floatval($reminderAmount);
            $numberofTimes  = floatval($b_amt) / 5000;

            if(floatval($reminderAmount) > 0)
            {
                $trans_id = "VBMG".rand(10000000,99999999);
                $otp_status ="0";
                $rep_trans_id = "";
                $remarks ="";

                $data = [   'clientTransId' => $trans_id,
                                'mobileNo' => $c_msisdn,
                                'benId' => $ben_id,
                                'bankName' => "-",
                                'bankIFSC' => $b_code,
                                'accountNo' => $b_accno,
                                'customerName' => $b_name,
                                'routingType' => $b_mode,
                                'amount' => $reminderAmount,
                                'userCode' => $user_code,
                                'userName' => $user_name,
                                'userBal' => $user_ubal,
                                'apiTransId' => "",
                                'rechMode' => 'WEB'
                            ];
        
                 list($z, $msg, $otp_status) = CyberBankTransfer::add($data);

            }

            //Times
            for($i = 1; $i <= $numberofTimes; $i++)
            {
                $trans_id = "VBMG".rand(10000000,99999999);
                $otp_status ="0";
                $rep_trans_id = "";
                $remarks ="";

                $data = [   'clientTransId' => $trans_id,
                                'mobileNo' => $c_msisdn,
                                'benId' => $ben_id,
                                'bankName' => "-",
                                'bankIFSC' => $b_code,
                                'accountNo' => $b_accno,
                                'customerName' => $b_name,
                                'routingType' => $b_mode,
                                'amount' => "5000",
                                'userCode' => $user_code,
                                'userName' => $user_name,
                                'userBal' => $user_ubal,
                                'apiTransId' => "",
                                'rechMode' => 'WEB'
                            ];
        
                 list($z, $msg, $otp_status) = CyberBankTransfer::add($data);

            }

             return redirect()->back()->with('result', [ 'output' => $msg, 'msg' => $z, 
                    'trans_id' => $trans_id ]);
             
        
        }



       
    }

    public function getMoneyDetailsRetailer($msisdn, $auth_token)
    {
        
        $z1 = 0;
        $z = 0;
        $rs = [];
       

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                list($data, $beni) = BankMobileCommon::fetchCustomerDetails($result);

                $cx = BankMobileCommon::add_status($user_name, $msisdn, "MONEY");

                $str = $this->getMoney($user_name, "", "", "");
               
                return view('android.recharge_money_rp_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 'data' => $data, 'money' => $str]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

       
    }

    public function getMoneyFilterDetailsRetailer_1($msisdn, $auth_token)
    {
        
        $z1 = 0;
        $z = 0;
        $rs = [];
       

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                list($data, $beni) = BankMobileCommon::fetchCustomerDetails($result);

                $cx = BankMobileCommon::add_status($user_name, $msisdn, "MONEY");

                return view('android.recharge_money_rp_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 'data' => $data]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

       
    }

    public function getMoneyFilterDetailsRetailer_2(Request $request)
    {
        
        $z1 = 0;
        $z = 0;
        $rs = [];
       
        $msisdn = trim($request->msisdn);
        $auth_token = trim($request->auth_token);

        $f_date = trim($request->f_date);
        $t_date = trim($request->t_date);
        $user_msisdn = trim($request->user_mobile);

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "RETAILER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only Retailer...
            list($z, $op, $result) = BankMobileCommon::user_validation($user_name, $msisdn);
            if($z == 0)
            {
                list($data, $beni) = BankMobileCommon::fetchCustomerDetails($result);

                $cx = BankMobileCommon::add_status($user_name, $msisdn, "MONEY");

                $str = $this->getMoney($user_name, $f_date, $t_date, $user_msisdn);
               
                return view('android.recharge_money_rp_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                        "auth_token" => $auth_token, 'msisdn' => $msisdn, 'result_data' => $result, 'data' => $data, 'money' => $str]);
                        

            }
            else
            {
                $op = "Msisdn is Not Registered...";
            }
        }
        else if($z1 == 1)
        {
            echo "User is not Retailer";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }

       
    }

    public function getMoney($user_name, $f_date, $t_date, $msisdn)
    {
        $money_1 = new AllTransaction;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $d1 = [];

        $user_name = strtoupper($user_name);

        if($f_date != "" && $t_date != "")
        {
            $f_date = $f_date." 00:00:00";
            $t_date = $t_date." 23:59:59";
        }

        if($f_date == "" && $t_date == "" && $msisdn == "")             // All are Empty
        {
            $d1 = $money_1->whereRaw('upper(user_name) = ?',[$user_name])->where('trans_type', 'BANK_TRANSFER')
                                ->orderBy('id', 'desc')->limit(20)->get(); 
        }
        else if($f_date != "" && $t_date != "" && $msisdn == "")        // Msisdn is Empty
        {
            $d1 = $money_1->whereRaw('upper(user_name) = ?',[$user_name])->where('trans_type', 'BANK_TRANSFER')
                            ->whereBetween('mt_date', [$f_date, $t_date])
                            ->orderBy('id', 'desc')->get(); 
        }
        else if($f_date == "" && $t_date == "" && $msisdn != "")        // Dates are Empty
        {
            $d1 = $money_1->whereRaw('upper(user_name) = ?',[$user_name])->where('trans_type', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')->get(); 
        }
        else if($f_date != "" && $t_date != "" && $msisdn != "")        // No one is Empty
        {
            $d1 = $money_1->whereRaw('upper(user_name) = ?',[$user_name])
                            ->whereBetween('mt_date', [$f_date, $t_date])->where('trans_type', 'BANK_TRANSFER')
                            ->orderBy('id', 'desc')->get();  
        }
        
      
        $str = "";
        
        $j = 1;
        foreach($d1 as $f)
        {
            
            $reply_id = "";
            $reply_date = "";
            $rech_status = "";
            $o_bal = 0;
            $u_bal = 0;
            $r_tot = 0;
            $z = 0;

            $reply_id = $f->reply_id;
            $reply_date =$f->reply_date;

            $rech_status = $f->rech_status;
            $rech_option = $f->rech_option;

            if($rech_status == "PENDING" && $rech_option == "0")
            {
                $status = "PENDING";
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }
            else if($rech_status == "PENDING" && $rech_option == "2")
            {
                $status = "FAILURE";
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }
            else if($rech_status == "FAILURE" && $rech_option == "2")
            {      
                $status = "FAILURE";
                $o_bal = floatval($f->ret_bal) - floatval($f->ret_total);
                $z = 1;
            }
            else if ($rech_status == "SUCCESS")
            {
                $status = "SUCCESS";
                $o_bal = floatval($f->ret_bal) + floatval($f->ret_total);
            }

            $o_bal = number_format($o_bal, 2, '.', '');
            $c_bal = floatval($f->ret_bal);
            $c_bal = number_format($c_bal, 2, '.', '');

            $bank = "";
            $cd_a = explode("--", $f->rech_bank);
            if(sizeof($cd_a) > 1)
            {
                $cd_b = $cd_a[0];
            }
            else {
                $cd_b = "-";
            }
            
            if($cd_b == "UTIB") {
                $bank = "AXIS BANK";
            }
            else if($cd_b == "BARB") {
                $bank = "BANK OF BARODA";
            }
            else if($cd_b == "CNRB") {
                $bank = "CANARA BANK";
            }
            else if($cd_b == "CORP") {
                $bank = "CORPORATION BANK";
            }
            else if($cd_b == "CIUB") {
                $bank = "CITY UNION BANK";
            }
            else if($cd_b == "HDFC") {
                $bank = "HDFC BANK";
            }
            else if($cd_b == "IBKL") {
                $bank = "IDBI BANK LTD";
            }
            else if($cd_b == "IDIB") {
                $bank = "INDIAN BANK";
            }
            else if($cd_b == "IOBA") {
                $bank = "INDIAN OVERSEAS BANK";
            }
            else if($cd_b == "ICIC") {
                $bank = "ICICI BANK";
            }
            else if($cd_b == "KVBL") {
                $bank = "KARUR VYSYA BANK";
            }
            else if($cd_b == "SBIN") {
                $bank = "STATE BANK OF INDIA";
            }
            else if($cd_b == "SYNB") {
                $bank = "SYNDICATE BANK";
            }
            else if($cd_b == "TMBL") {
                $bank = "TAMILNADU MERCANTILE BANK";
            }
            else if($cd_b == "VIJB") {
                $bank = "VIJAYA BANK";
            }
            else if($cd_b == "") {
                $bank = $f->rech_bank;
            }
            else {
                $bank = $f->rech_bank;
            }

            
                               
            $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
            $str = $str."<div class='row' style='margin:0px 0px;'>"; 
            if($status == "SUCCESS") {
                $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
            }
            else if($status == "FAILURE") {
                $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
            }
            else if($status == "PENDING") {
                $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
            }
           
            $str = $str."<p style='font-size: 16px;margin: 0;'>".$f->rech_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
            $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->rech_mobile."</p>"; 
            $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$bank."</p>"; 
            
            if($z== 0) {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$reply_id."</p>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 11px;'>".$f->rech_date."</p>"; 
            }
            else  {
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'></p>";  
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 11px;'>".$f->reply_date."</p>"; 
            }

            
            
            $str = $str."</div><div class='col-3 col-sm-3 col-md-2 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
            $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".$o_bal."</p>";
            $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".$c_bal."</p>";
            $str = $str."</div></div></div>";
       
        }
        
        return $str; 

    }

    
}
