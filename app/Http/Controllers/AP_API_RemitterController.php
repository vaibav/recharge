<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetAPICommon;
use App\Libraries\BankRemitter;

class AP_API_RemitterController extends Controller
{
    //
    public function remitter_check(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $msisdn = trim($request->msisdn);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if(GetAPICommon::checkNumber($msisdn))
            {
                $data = ['user_name_1' => $user_name,
                    'msisdn' => $msisdn,
                    'r_mode' => "API"
                ];

                list($z, $op, $result) = BankRemitter::userCheck($data);

                $result = (array)json_decode($result);
            }
            else
            {
                $result = ['status' => '2', 'message' => 'Invalid MSISDN...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function remitter_entry(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $name = trim($request->user_name);
        $address = trim($request->user_address);
        $city = trim($request->user_city);
        $state_code = trim($request->user_state_code);
        $pincode = trim($request->user_pincode);
        $msisdn = trim($request->msisdn);
        $rem_id = trim($request->rem_id);
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            $data = ['user_name_1' => $user_name,
                    'user_name' => $name,
                    'user_address' => $address,
                    'user_city' => $city,
                    'user_state_code' => $state_code,
                    'user_pincode' => $pincode,
                    'msisdn' => $msisdn,
                    'api_trans_id' => $rem_id,
                    'r_mode' => "API"
                ];
            
            $v = [];
            $v = $this->checkValues($data);

            if($v[0] == 0)
            {
                list($z, $msg, $res_content) = BankRemitter::add($data);

                $result = (array)json_decode($res_content);
            }
            else
            {
                $result = ['status' => '2', 'message' => $v[1]];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function checkValues($data)
    {
        $v = [];
        $v[0] = 0;
        $v[1] = "success";

        if(!GetAPICommon::checkNumber($data['msisdn']))
        {
            $v[0] = 1;
            $v[1] = "Invalid MSISDN";
        }
        else if(!GetAPICommon::checkNumber($data['user_state_code']))
        {
            $v[0] = 2;
            $v[1] = "Invalid State Code";
        }
        else if(!GetAPICommon::checkNumber($data['user_pincode']))
        {
            $v[0] = 3;
            $v[1] = "Invalid Pincode";
        }
        else if($data['user_name'] == "")
        {
            $v[0] = 4;
            $v[1] = "Empty Name";
        }
        else if($data['user_city'] == "")
        {
            $v[0] = 5;
            $v[1] = "Empty City";
        }
        else if($data['api_trans_id'] == "")
        {
            $v[0] = 6;
            $v[1] = "Empty Remitter ID";
        }

        return $v;
    }
}
