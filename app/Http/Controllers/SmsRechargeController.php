<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Libraries\RechargeMobile;
use App\Libraries\SmsInterface;
use App\Libraries\RechargeNewMobile;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;

// Network
use App\Models\NetworkDetails;

// Recharge
use App\Models\UserRechargeDetails;
use App\Models\UserRechargePaymentDetails;


class SmsRechargeController extends Controller
{
    /**
     * Sms Recharge (Forwarded from Android Application)
     */
    public function sms_recharge(Request $request)
	{
        $uacc_1 = new UserPersonalDetails;
       

        // Declarations
        $z = 0;
        $user_name = "";
        $user_ubal = 0;
        $m_z = 0;
        $net_type_code = "32";

        // Post Data
        $mob_no = trim($request->mob_no);
        $mob_da = trim($request->mob_data);
        $date_time = date("Y-m-d H:i:s");

        file_put_contents("testsmsresult.txt", $mob_no."---".$mob_da."---".$date_time." ".PHP_EOL, FILE_APPEND);
        
        if (strpos($mob_no, '+91') !== false) {
            $mob_no = str_replace("+91", "", $mob_no);
        }
        
           
        $dc1 = $uacc_1->with(['account'])->where('user_mobile', '=', $mob_no)->get();

        if($dc1->count() > 0)
        {
            if($dc1[0]->account->user_type == 'RETAILER')
            {
                $user_name = $dc1[0]->user_name;
                $user_code = $dc1[0]->user_code;
                $user_ubal = $this->getBalance($user_name);
            }
            else
            {
                $z = 2;         // User is not Retailer
            }
        }
        else
        {
            $z = 1;             // Invalid user
        }


        if($z == 0)
        {
            if($mob_da == "CB")
            {
                $z = 11;                // Current Balance
                $op = "Your Account Balance is ".$user_ubal;
                $m_z = 616;

                SmsInterface::callSms($user_name, $mob_no, $op);

            }
            else if($mob_da == "CR")
            {
                $z = 12;                // Last Three Recharge Results
                $op = $this->LastThreeTransaction($user_name);
                $m_z = 617;

                SmsInterface::callSms($user_name, $mob_no, $op);
            }
            else
            {
                // Normal Recharge
                if($z == 0)
                {
                    list($net_code, $user_mobile, $user_amount) = $this->getSmsDetails($mob_da);

                    if($net_code == "" || $user_mobile == "" || $user_amount == "")
                    {
                        $z = 3;              // Invalid Message
                    }
                }

            }
        }

        if($z == 0)
        {
            if (!is_numeric($user_amount)) 
            {
                $z = 4;         //Amount is not a valid no...
            }
        }

        if($z == 0)
        {
            if (!is_numeric($user_mobile)) 
            {
                $z = 5;         //Amount is not a valid no...
            }
        }
        

        if($z == 0)
        {
            // Get Network code
            $net_code = $this->getNetCode($net_code);
            if($net_code == 0)
            {
                $z = 6;         //Invalid Network...
            }
            else  if($net_code == 1)
            {
                $z = 7;         //Inactive Network...
            }
        }

        


        if ($z == 0)
        {
            $data = [];
            $data['net_type_code'] = $net_type_code;
            $data['net_code'] = $net_code;
            $data['user_mobile'] = trim($user_mobile);
            $data['user_amount'] = trim($user_amount);
            $data['code'] = $user_code;
            $data['user'] = $user_name;
            $data['ubal'] = $user_ubal;
            $data['mode'] = "SMS";
            $data['api_trans_id'] = "";

            //list($z1, $trans_id) = RechargeMobile::add($data);

            $z1 = RechargeNewMobile::add($data);

            if($z1 == 0)
            {
                $op = "Recharge Process is Completed Successfully..";
                $m_z = 200;
            }
            else if($z1 == 1)
            {  
                $op = "Error! Low user Balance...";
                $m_z = 604;
            }
            else if($z1 == 2)
            {  
                $op = "Error! No Network Line is Selected...";
                $m_z = 605;
            }
            else if($z1 == 3)
            {   
                $op = "Error! Mode web is not Selected...";
                $m_z = 606;
            }
            else if($z1 == 4)
            {   
                $op = "Error! Account is Inactive...";
                $m_z = 607;
            }
            else if($z1 == 5)
            {
                $op = "Error! Account Balance is low from Setup Fee...";
                $m_z = 608;
            }
            else if($z1 == 6)
            {
                $op = "Error! Recharge Status is already Pending for this Mobile No...";
                $m_z = 609;
            }
            else if($z1 == 7)
            {
                $op = "Error! Wait for 10 Minutes...";
                $m_z = 610;
            }
            else if($z1 == 8)
            {
                $op = "Sorry! Server is Temporarily shut down...";
                $m_z = 611;
            }
        }
        else if($z == 1)
        {  
            $op = "Error! Invalid User...";
            $m_z = 612;
        }
        else if($z == 2)
        {  
            $op = "Error! User is not Retailer...";
            $m_z = 613;
        }
        else if($z == 3)
        {   
            $op = "Error! Invalid Message...";
            $m_z = 614;
        }
        else if($z == 4)
        {   
            $op = "Error! Amount is not a valid no...";
            $m_z = 615;
        }
        else if($z == 5)
        {
            $op = "Error! Mobile no is not a valid no...";
            $m_z = 616;
        }
        else if($z == 6)
        {
            $op = "Error! Invalid Network Code...";
            $m_z = 617;
        }
        else if($z == 7)
        {
            $op = "Error! Inactive Network Code...";
            $m_z = 617;
        }
        else if($z == 11)
        {
            $op = "Success! Balance has been sent...";
            $m_z = 200;
        }
        else if($z == 12)
        {
            $op = "Success! Recharge details has been sent...";
            $m_z = 200;
        }
        
          

        //$axx = $this->sendSms($mob_no, $op);

        $result = ["status" => $m_z, 'output' => $op];

        return response()->json($result, 200);

    }


    /**
     * Get User Balance
     */

    public function getBalance($user_name)
    {
        $uacc_1 = new UserAccountBalanceDetails;
        $ux_bal = 0;

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $user_name)->get();
        foreach($d1 as $d)
        {
            $ux_bal = $d->user_balance;
        }

        return $ux_bal;
    }



    /**
     * Split and Get SMS Data
     */
    function getSmsDetails($str)
    {

        $aa = str_split($str);

        $net = "";
        $stx = "";
        $f = 0;
        $mob = "";
        $amt = "";
        
        foreach($aa as $a)
        {
            if($f == 0)
            {
                if(!is_numeric($a))
                {
                    $net = $net.$a;
                }
                else
                {
                    $f = 1;
                }
            }
            if($f == 1)
            {
                $stx = $stx.$a;
            }  
        }

        $ab = explode("A", $stx);
        if(sizeof($ab) > 1)
        {
            $mob = $ab[0];
            $amt = $ab[1];
        }

        return array($net, $mob, $amt);

    }


     /**
     * Get Net Code
     */
    public function getNetCode($net_short)
    {
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $net_status = 0;

        
        $d1 = $net_1->select('net_code', 'net_status')->where('net_short_code', '=', $net_short)->get();
        if($d1->count() > 0)
        {
           $net_code = $d1[0]->net_code;
           $net_status = $d1[0]->net_status;

           if($net_status != 1)
                $net_code = 1;
        }

        return $net_code;
    }

    /**
     * Last three recharge details
     */
    public function LastThreeTransaction($user_name)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $op = "";

        
        $d1 = $rech_1->select('trans_id','net_code','rech_mobile','rech_amount','rech_total', 'rech_status')
                    ->where('user_name', '=', $user_name)
                    ->orderBy('id', 'desc')->limit(3)->get();
        
        $user_bal = 0;
        foreach($d1 as $d)
        {
            $trans_id = $d->trans_id;
            $status = $d->rech_status;
            $net_code = $d->net_code;

            $d2 = $rech_2->select('user_balance')
                    ->where('trans_id', '=', $trans_id)
                    ->where('user_name', '=', $user_name)
                    ->where('rech_status', '=', $status)
                    ->get();
            
            $user_bal = 0;
            if($d2->count() > 0)
            {
                $user_bal = $d2[0]->user_balance;
            }

            $net_name = "";
            $d3 = $net_1->select('net_name')->where('net_code', '=', $net_code)->get();
            if($d3->count() > 0)
            {
                $net_name = $d3[0]->net_name;
            }

            $op = $op."TR:".$trans_id."-OP:".$net_name."-NO:".$d->rech_moble."-AM:".$d->rech_amount."-TT:".$d->rech_total;
            $op = $op."-ST:".$status."-BL:".$user_bal.",";
        }

        if($op == "")
            $op = "No Transaction...";

        return $op;
    }

    

}
