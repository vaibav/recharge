<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;

use App\Models\NetworkDetails;
use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;

use App\Models\NetworkPackageDetails;

class SD_UserController extends Controller
{
    //
    public function index(Request $request)
	{

        $net_o1 = new NetworkDetails;
        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();
        $d1 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('sdistributor.user_1', ['network' => $data, 'package' => $d1, 'user' => $ob]);
        
    }

    public function store(Request $request)
	{
        $net_o1 = new NetworkDetails;
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;

        $data = $net_o1->select('net_code','net_name','net_short_code','net_type_code','net_status')->get();

        // Declaration
        $file_name1 = "";
        $file_name2 = "";

        $ob = GetCommon::getUserDetails($request);

        $parent_code = $ob->code;
        $parent_name = $ob->user;
        $parent_type = $ob->mode;

        // Validation
        $this->validate($request, [
            'user_per_name' => 'required',
            'user_city' => 'required',
            'user_mobile' => 'required',
            'user_name' => 'required|without_spaces',
            'user_pwd' => 'required'
        ],
        [
            'user_per_name.required' => ' The Name is required.',
            'user_city.required' => ' The user city is required.',
            'user_mobile.required' => ' The user mobile is required.',
            'user_name.required' => ' The user name is required.',
            'user_name.without_spaces' => ' The user name dot not have any white spaces.',
            'user_pwd.required' => ' The password is required.'
            ]);

        $user_code = rand(1000,9999);
        $user_per_name = trim($request->user_per_name);
        $user_city = trim($request->user_city);
        $user_state = trim($request->user_state);
        $user_phone = trim($request->user_phone);
        $user_mobile = trim($request->user_mobile);
        $user_mail = trim($request->user_mail);
        $user_kyc = trim($request->user_kyc);
        $pack_id = trim($request->pack_id);

        $user_name = trim($request->user_name);
        $user_pwd = trim($request->user_pwd);
        $user_acc_type = trim($request->user_acc_type);
        $user_setup_fee = trim($request->user_setup_fee);
        
        //recharge type
        $user_rech = $request->user_rec_mode;
        $user_rec_mode = "";
        foreach ($user_rech as $c)
        { 
            $user_rec_mode = $user_rec_mode . $c. "@";
        }
        
        $user_api_url_1 = trim($request->user_api_url_1);
        $user_api_url_2 = trim($request->user_api_url_2);

        // Temporary File Name Storage
        $file_name1 = $user_code."_PHOTO.jpg";
        $file_name2 = $user_code."_KYC.jpg";

        if ($request->hasFile('user_photo')) {
            //
            $photo = $request->file('user_photo');
            $extension = $photo->getClientOriginalExtension();
            $file_name1 = $user_code."_PHOTO.".$extension;
            $photo->move(public_path("/uploadphoto"), $file_name1);
        }
       

        if ($request->hasFile('user_photo')) {
            //
            $kyc_proof = $request->file('user_kyc_proof');
            $extension = $kyc_proof->getClientOriginalExtension();
            $file_name2 = $user_code."_KYC.".$extension;
            $kyc_proof->move(public_path("/uploadkyc"), $file_name2);
        }
       
        
        // Insert Data
        $cnt = $user_1->where('user_code', '=', $user_code)->count();
        if($cnt == 0)
        {
            $cnt1 = $user_1->where('user_name', '=', $user_name)->count();
            if($cnt1 == 0)
            {
                // Personal Details....
                $user_1->user_code = $user_code;
                $user_1->user_name = $user_name;
                $user_1->user_per_name = $user_per_name;
                $user_1->user_city = $user_city;
                $user_1->user_state = $user_state;
                $user_1->user_phone = $user_phone;
                $user_1->user_mobile = $user_mobile;
                $user_1->user_mail = $user_mail;
                $user_1->user_kyc = $user_kyc;
                $user_1->user_photo = $file_name1;
                $user_1->user_kyc_proof = $file_name2;

                $user_1->save();

                // Account Details.....
                $user_2->user_code = $user_code;
                $user_2->user_name = $user_name;
                $user_2->user_pwd = $user_pwd;
                $user_2->user_type = $user_acc_type;
                $user_2->user_setup_fee = $user_setup_fee;
                $user_2->parent_type = $parent_type;
                $user_2->parent_code = $parent_code;
                $user_2->parent_name = $parent_name;
                $user_2->pack_id = $pack_id;
                $user_2->user_rec_mode = $user_rec_mode;
                $user_2->user_api_url_1 = $user_api_url_1;
                $user_2->user_api_url_2 = $user_api_url_2;
                $user_2->user_status = 4;

                $user_2->save();

               

                // User Chain Details
                $d1 = $user_4->select('user_level', 'user_chain')->where('user_code', '=', $parent_code)->get();
                if($d1->count() > 0)
                {
                    $user_level = intval($d1[0]->user_level) + 1;
                    $user_chain = $d1[0]->user_chain."-".$user_code;
                }
                else
                {
                    $user_level = 1;
                    $user_chain = "1-".$user_code;
                }

                $user_4->user_code = $user_code;
                $user_4->user_name = $user_name;
                $user_4->parent_code = $parent_code;
                $user_4->parent_name = $parent_name;
                $user_4->user_level = $user_level;
                $user_4->user_chain = $user_chain;
                
                $user_4->save();

                // Account Balance Details.....
                $user_5->user_code = $user_code;
                $user_5->user_name = $user_name;
                $user_5->user_balance = "0.00";
                
                $user_5->save();

                $op = "User Entry is created successfully....";
              
            }
            else
            {
                $op = "User Name Already Exists...";
            }
        }
        else
        {
            $op = "User Code Already Exists....";
        }
        
       
        return redirect()->back()->with('msg', $op);
        
    }

    public function viewall(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $user_2 = new UserAccountDetails;
        
        $user_4 = new UserChainDetails;
        $user_5 = new UserAccountBalanceDetails;

        $rs = [];

        $ob = GetCommon::getUserDetails($request);
        $user_code = $ob->code;
       
        
        //$d1 = $user_4->select('user_code', 'user_level')->where('user_chain', 'like', '%-'.$user_code.'-%')->get();
        $d1 = $user_4->select('user_code', 'user_level')->where('parent_code', '=', $user_code)->get();

        $j = 0;
        foreach($d1 as $d)
        {
            $data = $user_1->with(['account', 'balance'])->where('user_code', '=', $d->user_code)->get();
            $rs[$j][0] = $data;
            $rs[$j][1] = $d->user_level;
            $j++;
        }

        //var_dump($rs);

        return view('sdistributor.user_view_1', ['user1' => $rs, 'user' => $ob]);

       

    }

    public function view_one($user_code, Request $request)
	{
        $user_1 = new UserPersonalDetails;
        
        $data = $user_1->with(['account'])->where('user_code', '=', $user_code)->get();
        $d1 = NetworkPackageDetails::groupBy('pack_name', 'pack_id')->selectRaw('pack_id, pack_name')->orderby('pack_name','asc')->get();

        $ob = GetCommon::getUserDetails($request);
        
        return view('sdistributor.user_view_2', ['user1' => $data, 'package' => $d1, 'user' => $ob]);
    }

    public function view_network($user_code, Request $request)
	{

        $userName = "-";
        $packId   = "-";

        $userDetails = UserAccountDetails::select('user_name', 'pack_id')->where('user_code', $user_code)->first();

        if($userDetails){
            $userName = $userDetails->user_name;
            $packId   = $userDetails->pack_id;
        }

        $packDetails    = NetworkPackageDetails::where('pack_id', $packId)->get();
        $networkDetails = NetworkDetails::select('net_code', 'net_name')->get();

        $ob = GetCommon::getUserDetails($request);

        return view('sdistributor.user_view_3', ['user' => $ob, 'packDetails' => $packDetails, 'networkDetails' => $networkDetails]);
    }

    public function checkUserName($user_name, Request $request)
    {
        $user_2 = new UserAccountDetails;
        $cnt = 0;

        $cnt = $user_2->where('user_name', '=', $user_name)->count();

        echo $cnt;
    }

    public function checkUserMobile($user_mobile, Request $request)
    {
        $user_1 = new UserPersonalDetails;
        $cnt = 0;

        $cnt = $user_1->where('user_mobile', '=', $user_mobile)->count();

        echo $cnt;
    }

}
