<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\UserAccountDetails;
use App\Models\UserPersonalDetails;

use App\Models\ServerDetails;

class LoginController extends Controller
{
    //
    public function index()
	{
		
        return view('login.login',  ['output' => '']);
		
    }

    public function check(Request $request)
	{
        
        $user_2 = new UserAccountDetails;
        $user_1 = new UserPersonalDetails;
        $op = "";
        // Validation
        $this->validate($request, [
            'username' => 'required|without_spaces',
            'password' => 'required'
        ],
        [
            'username.required' => ' Username is required.',
            'password.required' => ' Password is required.'
           
            ]);

        $user_name = trim($request->username);
        $user_pwd = trim($request->password);
        
        
        $d1 = $user_2->select('user_code', 'user_name', 'user_pwd', 'user_type','user_status')->where('user_name', '=', $user_name)->get();
        $d2 = $user_1->select('user_mobile')->where('user_name', '=', $user_name)->get();
        
        if($d1->count() > 0)
        {
            $pwd = $d1[0]->user_pwd;
            $status = $d1[0]->user_status;
            $s_status = $this->checkServerStatus();

            $k = 0;

            if($pwd == $user_pwd) {
                $k = 1;
            }
            else {
                if (Hash::check($user_pwd, $pwd)) {
                    // The passwords match...
                    $k = 1;
                }
            }

            if($k == 1)
            {
                if($status == "1")
                {
                    $op = "Success ! Valid User...";

                    $request->session()->put('code', $d1[0]->user_code);
                    $request->session()->put('user', $d1[0]->user_name);
                    $request->session()->put('mode', $d1[0]->user_type);
                    $request->session()->put('valid', 1);
                    if($d1[0]->user_type == "ADMIN")
                    {
                        if($d2->count() > 0)
                        {
                            //$this->sendSms($d2[0]->user_mobile, $user_name);
                        }
                        return redirect('dashboard');
                    }

                    
                    if($s_status == 0)
                    {
                        if($d1[0]->user_type == "SUPER DISTRIBUTOR" )
                        {
                            if($d2->count() > 0)
                            {
                                //$this->sendSms($d2[0]->user_mobile, $user_name);
                            }
                            return redirect('dashboard_user');
                        }
                        else if($d1[0]->user_type == "DISTRIBUTOR" )
                        {
                            if($d2->count() > 0)
                            {
                                //$this->sendSms($d2[0]->user_mobile, $user_name);
                            }
                            return redirect('dashboard_distributor');
                        }
                        else if($d1[0]->user_type == "RETAILER" )
                        {
                            if($d2->count() > 0)
                            {
                                //$this->sendSms($d2[0]->user_mobile, $user_name);
                            }
                            return redirect('dashboard_retailer');
                        }
                        else if($d1[0]->user_type == "API PARTNER" )
                        {
                            if($d2->count() > 0)
                            {
                                //$this->sendSms($d2[0]->user_mobile, $user_name);
                            }
                            return redirect('dashboard_apipartner');
                        }
                        else if($d1[0]->user_type == "AGENT" )
                        {
                            if($d2->count() > 0)
                            {
                                //$this->sendSms($d2[0]->user_mobile, $user_name);
                            }
                            return redirect('dashboard_agent');
                        }
                    }
                    else
                    {
                        $op = "Server is Temporarily Shut down...";
                        return redirect()->back()->with('data', [ 'output' => $op]);
                    }
                    
                }
                else if($status == "2")
                {
                    $op = "Account is Locked...";
                    return redirect()->back()->with('data', [ 'output' => $op]);
                }
                else if($status == "4")
                {
                    $op = "Waiting For Admin Approval...";
                    return redirect()->back()->with('data', [ 'output' => $op]);
                }
            }
            else 
            {
                $op = "Invalid User...";
                return redirect()->back()->with('data', [ 'output' => $op]);
            }
            

        }
        else
        {
            $op = "Invalid User...";
            return redirect()->back()->with('data', [ 'output' => $op]);
        }
        
    }

    public function logout(Request $request)
	{
        
        if($request->session()->has('code'))
        {
            $request->session()->forget('code');
        }
        if($request->session()->has('user'))
        {
            $request->session()->forget('user');
        }
        if($request->session()->has('mode'))
        {
            $request->session()->forget('mode');
        }
		if($request->session()->has('valid'))
        {
            $request->session()->forget('valid');
        }

        return redirect('login')->with('data', [ 'output' => ""]);
    }

    public function sendSms($mobile, $user_name)
    {
        //Sms sending
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'http://bhashsms.com/api/sendmsg.php?', [
            'query' => ['user'    =>  'vaibav1',
            'pass'    =>  '123456',
            'sender'  =>  'VAIBAV',
            'phone'   =>  $mobile,
            'sender'  =>  'VAIBAV',
            'text'    =>  'Welcome '.$user_name.'! You have login successfully at '.date('d/m/Y H:i:s'),
            'priority'    =>  'ndnd',
            'stype'  =>  'normal']
        ]);
    }

    public function checkServerStatus()
    {
        $serv_1 = new ServerDetails;
        
        $z = 0;

        $d1 = $serv_1->select('server_status')->get();

        if($d1->count() > 0)
        {
            $s_status = $d1[0]->server_status;

            if($s_status == 2)
                $z = 1;
        }

        return $z;
    }


}
