<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Libraries\GetCommon;
use App\Libraries\Stock;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;
use App\Models\UserPaymentDetails;

class SD_StockController extends Controller
{
    //
    public function index(Request $request)
	{

        $ob = GetCommon::getUserDetails($request);

        return view('sdistributor.stock_1', ['user' => $ob]);
        
    }

    public function viewdate(Request $request)
    {
        
        $upay_1 = new UserPaymentDetails;
        $net_2 = new NetworkDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Opening Balanace
        $o_balance1 = Stock::stockOpeningBalance("2018-10-01 00:00:00", $date_1." 00:00:00", $ob->user);
        $o_balance = Stock::stockCustomOpeningBalance($ob->user);
        $o_balance = floatval($o_balance) + floatval($o_balance1);
        
        $o_sales_re = Stock::getDistributorSales($ob->user, "2018-10-01 00:00:00",$f_date, "RECHARGE");
        $o_sales_eb = Stock::getDistributorSales($ob->user, "2018-10-01 00:00:00", $f_date, "BILL_PAYMENT");
        $o_sales_mo = Stock::getDistributorSales($ob->user, "2018-10-01 00:00:00",$f_date, "BANK_TRANSFER");
        $o_sales_mv = 0;
        $o_balance = floatval($o_balance) - (floatval($o_sales_re) + floatval($o_sales_eb) + floatval($o_sales_mo) + floatval($o_sales_mv));

        $old_re = Stock::getDistributorOldRecharge($ob->user);

        // current sales...
        $c_sales_re = Stock::getDistributorSales($ob->user, $f_date, $t_date, "RECHARGE");
        $c_sales_eb = Stock::getDistributorSales($ob->user, $f_date, $t_date, "BILL_PAYMENT");
        $c_sales_mo = Stock::getDistributorSales($ob->user, $f_date, $t_date, "BANK_TRANSFER");
        $c_sales_mv = 0;

        $data = Stock::stockData($ob->user, $f_date, $t_date);
        $d1 = Stock::getDistributorRecharge($ob->user, $f_date, $t_date);
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        // Manual Pagination
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('sdistributor.stock_2', ['pay1' => $data, 'obalance' => $o_balance, 'recharge' => $d1, 
        'network' => $d2, 'bill' => $c_sales_eb, 'ce_tot' => $c_sales_re, 
        'money' => $c_sales_mo, 'mverify' => $c_sales_mv, 'old_re' => $old_re, 'user' => $ob]); 

        
    }
}
