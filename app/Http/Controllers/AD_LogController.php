<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetCommon;
use DB;

use App\Models\UserAccountDetails;
use App\Models\NetworkPackageDetails;

class AD_LogController extends Controller
{
    //
    public function index(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        $d1 = UserAccountDetails::select('user_name')->orderby('user_name', 'asc')->get();
       
        return view('admin.ad_log', ['user' => $ob, 'user_details' => $d1, 'u_date' => '', 'u_name' => '', 'u_method' => '', 'data' => '']);
       
    }

    public function view(Request $request)
	{
        $ob  = GetCommon::getUserDetails($request);
        $z   = 0;
        $str = "";

        $u_date   = trim($request->u_date);
        $u_method = trim($request->u_method);
        $u_name   = trim($request->u_name);

        $d1 = UserAccountDetails::select('user_name')->orderby('user_name', 'asc')->get();

        if($u_date != "") {
           
            $txt_file    = file_get_contents(base_path().'/public/sample/recharge.txt');
            $rows        = explode("\n", $txt_file);

            $j = 1;

            foreach($rows as $row => $data)
            {
                //get row data
                if($data != "" && $data != null)
                {
                    $z1 = 0;
                    $z2 = 0;

                    $row_data = explode('^^^^^', $data);

                    $u_nam1    = trim($row_data[0]);
                    $u_dat1    = $row_data[1];
                    $u_mode    = trim($row_data[2]);
                    $u_mobi    = $row_data[3];
                    $u_ipad    = $row_data[4];
                    $u_agen    = $row_data[5];

                   
                    if($u_name == "ALL" || strtoupper($u_name) == strtoupper($u_nam1)) {
                        $z1 = 1;
                    }

                    if($u_method == "ALL" || $u_method == $u_mode) {
                        $z2 = 1;
                    }

                    if ((strpos($u_dat1, $u_date) !== false) && ($z1 == 1) && ($z2 == 1))
                    {
                        $str = $str."<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_nam1."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_dat1."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_mode."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_mobi."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_ipad."</td>";
                        $str = $str."<td style='font-size:12px;padding:7px 8px;'>".$u_agen."</td></tr>";
                        $j++;
                    }

                    
                }
            
            
            }
        }
        else {
            $str = "Please Enter Date";
        }


        return view('admin.ad_log', ['user' => $ob, 'user_details' => $d1, 'u_date' => $u_date, 'u_name' => $u_name, 'u_method' => $u_method, 'data' => $str]);
       
    }
  
}
