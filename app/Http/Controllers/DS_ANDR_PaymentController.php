<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;
use App\Libraries\Stock;

use App\Models\UserPersonalDetails;
use App\Models\TransactionAllDetails;
use App\Models\UserPaymentDetails;
use App\Models\UserAccountDetails;
use App\Models\UserAccountBalanceDetails;
use App\Models\UserChainDetails;
use App\Models\NetworkDetails;

use App\Models\PaymentLedgerDetails;
use App\Models\AllTransaction;

class DS_ANDR_PaymentController extends Controller
{
    //
    public function remote_index(Request $request)
	{
        $user_1 = new UserAccountDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
                
            }
        }


        if($z1 == 0)
        {
            //Only Distributor...
            $d1 = $user_1->with(['personal'])->where('parent_name', '=', $user_name)->orderBy('user_name', 'asc')->get();

            return view('android.remote_payment_ds', ['users' => $d1, 'user_name' => $user_name, 
                    'user_type' => $user_type, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);

           
        }
        else if($z1 == 1)
        {
            echo "User is not Distributor";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store(Request $request)
	{
        $user_1 = new UserAccountDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $data = [];
        $result = [];
        $status = 0;
        $op = "NONE";

        list($q, $user_name_grnt, $user_type, $user_code_grnt, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }


        if($z1 == 0)
        {
            //Only Distributor...
            $user_name_req = trim($request->user_name_rs);
    
            if($user_name_req != "admin")
            {
                $data['trans_id'] = rand(100000,999999);
                $data['user_name_req'] = $user_name_req;
                $data['user_name_grnt'] = $user_name_grnt;
                $data['user_amount'] = trim($request->user_amount);
                $data['user_remarks'] = trim($request->user_remarks);
                $data['payment_mode'] = "REMOTE_PAYMENT";

                $d1 = NetworkDetails::select('net_code')->where('net_name', 'REMOTE_PAYMENT')->get();

                $net_code = '0';
                if($d1->count() > 0) {
                    $net_code = $d1[0]->net_code;
                }

                $data['net_code'] = $net_code;
    
                list($status, $op) = Stock::PaymentGrant($data);
            }
            else
            {
                $status = 1;
                $op = 'Error! Request User Name is not Admin...';
            }
    
            

           
        }
        else if($z1 == 1)
        {
            $status = 2;
            $op = 'Error! User is not Distributor...';
        }
        else if($z1 == 2)
        {
            $status = 2;
            $op = 'Error! Invalid API token..';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);
    }

    public function remote_view(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }


        if($z1 == 0)
        {
            //Only Distributor...
            $d1 = $upay_1->select('trans_id','user_name', 'grant_user_amount', 'grant_date', 'payment_mode', 'trans_status', 'user_remarks')
            ->where('grant_user_name', '=', $user_name)->orderBy('id', 'desc')->limit(15)->get();

            return view('android.remote_payment_ds_rp', ['pay' => $d1, 'user_name' => $user_name, 
                    'user_type' => $user_type, 'user_bal' => $user_ubal, "auth_token" => $auth_token]);

           
        }
        else if($z1 == 1)
        {
            echo "User is not Distributor";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function request_index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            return view('android.payment_request_ds_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store_request(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $user_amount = trim($request->user_amt);
        $user_remarks = trim($request->user_remarks);

        $z1 = 0;
        $z = 0;
       

        list($q, $user_name_req, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            // Get Parent User Name
            $user_name_grnt = "None";
            $d1 = UserAccountDetails::select('parent_name')->where('user_name', '=', $user_name_req)->get();
            if($d1->count() > 0)
            {
                $user_name_grnt = $d1[0]->parent_name;
            }

            //Only DISTRIBUTOR...
            $data = [];
            $data['trans_id'] = rand(100000,999999);
            $data['user_name_req'] = $user_name_req;
            $data['user_name_grnt'] = $user_name_grnt;
            $data['user_amount'] = $user_amount;
            $data['user_remarks'] = $user_remarks;
            $data['payment_mode'] = "FUND_TRANSFER";

            $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

            $net_code = '0';
            if($d1->count() > 0) {
                $net_code = $d1[0]->net_code;
            }

            $data['net_code'] = $net_code;

            list($status, $op) = Stock::PaymentRequest($data);

            return redirect()->back()->with('msg', $op);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view_request(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $user_name = strtoupper($user_name);
            $d1 = $upay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('payment_mode', '!=', 'OPENING_BALANCE')->orderBy('id', 'desc')->get(); 

            return view('android.payment_request_ds_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'payment' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function accept_index(Request $request)
	{
        $upay_1 = new UserPaymentDetails;

        $auth_token = trim($request->auth_token);

        $z1 = 0;

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = AllTransaction::where('parent_name', '=', $user_name)
                                ->where(function($query) {
                                    return $query
                                    ->where('trans_type', '=', 'REMOTE_PAYMENT')
                                    ->orWhere('trans_type', '=', 'FUND_TRANSFER')
                                    ->orWhere('trans_type', '=', 'REFUND_PAYMENT');
                                })
                                ->where('rech_status', '=', 'PENDING')->orderBy('id', 'desc')->get();

            return view('android.payment_accept_ds_1', ['pay' => $d1, 'user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not DISTRIBUTOR";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function store_accept(Request $request)
	{
        $upay_1 = new UserPaymentDetails;
        
        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);
        $user_amount = trim($request->amt);

        $z1 = 0;
        $z = 0;
        $status = 0;
        $op = 'None...';
       

        list($q, $user_name_grnt, $user_type, $user_code_grnt, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
            if($d1->count() > 0)
            {
                $user_name_req = $d1[0]->user_name;
    
                if($user_name_req != "admin")
                {
                    $data['trans_id'] = $trans_id;
                    $data['user_name_req'] = $user_name_req;
                    $data['user_name_grnt'] = $user_name_grnt;
                    $data['user_amount'] = $user_amount;
                    $data['user_remarks'] = $d1[0]->reply_id."- done";
                    $data['payment_mode'] = "FUND_TRANSFER";
    
                    $d1 = NetworkDetails::select('net_code')->where('net_name', 'FUND_TRANSFER')->get();

                    $net_code = '0';
                    if($d1->count() > 0) {
                        $net_code = $d1[0]->net_code;
                    }

                    $data['net_code'] = $net_code;

                    list($status, $op) = Stock::paymentGrant($data);
                }
                else
                {
                    $status = 1;
                    $op = 'Error! Request User Name is not Admin...';
                }
    
            }
            else
            {
                $status = 2;
                $op = 'Error! Transaction Id is not found...';
            }
            
        }
        else if($z1 == 1)
        {
            $status = 2;
            $op = 'Error! User is not Distributor...';
        }
        else if($z1 == 2)
        {
            $status = 2;
            $op = 'Error! Invalid API token..';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);
    }

    public function store_delete(Request $request)
	{

        $auth_token = trim($request->auth_token);
        $trans_id = trim($request->trans_id);

        $z1 = 0;
        $z = 0;
        $status = 0;
        $op = 'None...';
        $date_time = date("Y-m-d H:i:s");
       

        list($q, $user_name_grnt, $user_type, $user_code_grnt, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "DISTRIBUTOR") {
                if($user_type != "SUPER DISTRIBUTOR") {
                    $z1 = 1;
                }
            }
        }

        if($z1 == 0)
        {
            //Only DISTRIBUTOR...
            $d1 = AllTransaction::where([['trans_id', '=', $trans_id], ['rech_status', 'PENDING']])->get();
            if($d1->count() > 0)
            {
                AllTransaction::where('trans_id', '=', $trans_id)
                                ->update(['rech_status'=> 'FAILURE', 'rech_option' => '2', 'reply_date' => $date_time, 'updated_at'=> $date_time ]);

                $status = 0;
                $op = 'Payment Transfer is cancelled Successfully...';
    
            }
            else
            {
                $status = 2;
                $op = 'Error! Transaction Id is not found...';
            }
            
        }
        else if($z1 == 1)
        {
            $status = 2;
            $op = 'Error! User is not Distributor...';
        }
        else if($z1 == 2)
        {
            $status = 2;
            $op = 'Error! Invalid API token..';
        }

        return redirect()->back()->with('result', [ 'msg' => $status, 'output' => $op]);
    }

    public function getCode($user_name)
    {
        $user_1 = new UserPersonalDetails;
        $code = 0;

        $d1 = $user_1->select('user_code')->where('user_name', '=', $user_name)->get();
        if($d1->count() > 0) {
           $bal = $d1[0]->user_code;
        }
        
        return $code;
    }
}
