<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

use App\Libraries\GetCommon;
use App\Libraries\RechargeReportSuperDistributor;

use App\Models\NetworkDetails;
use App\Models\UserAccountDetails;

class SD_RechargeReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $net_2 = new NetworkDetails;
        $user_1 = new UserAccountDetails;

        $ob = GetCommon::getUserDetails($request);

        $user_name = strtoupper($ob->user);

        $d1 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->orderBy('net_name', 'asc')->get();

        $d2 = $user_1->with(['personal'])->whereRaw('upper(parent_name) = ?',[$user_name])
                                ->orderBy('user_name', 'asc')->get();

        return view('sdistributor.rechargereport_1', ['user' => $ob, 'network' => $d1, 'user1' => $d2]);
        
    }

    public function viewdate(Request $request)
    {
        $net_2 = new NetworkDetails;
        
        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        // Other Requests
        $u_name = $ob->user;
        $u_name1 = trim($request->user_name);
        $u_status = trim($request->rech_status);
        $u_mobile = trim($request->rech_mobile);
        $u_amount = trim($request->rech_amount);
        $u_net_code = trim($request->net_code);
        $u_rech_type = trim($request->rech_type);

        $rs = [];
       
        $d1 = RechargeReportSuperDistributor::getRechargeReportDetails_new($f_date, $t_date, $u_rech_type, $u_name, $u_name1, $u_status, $u_mobile, $u_amount, $u_net_code);
                                
        $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', 1)->get();

        $re_tot = 0;
        $ce_tot = 0;
        $su_tot = 0;
        $fa_tot = 0;
        $pe_tot = 0;
        $su = 0;
        $fa = 0;
        $pe = 0;
        $j = 0;
        foreach($d1 as $r)
        {
            if($r->rech_status == "SUCCESS" )
            {
                $su_tot = floatval($su_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $su++;
            }
            else if($r->rech_status == "FAILURE" && $r->rech_option == "2")
            {
                $fa_tot = floatval($fa_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $fa++;
            }
            else if($r->rech_status == "PENDING" && $r->rech_option == "0")
            {
                $pe_tot = floatval($pe_tot) + floatval($r->rech_total);
                $re_tot = floatval($re_tot) + floatval($r->rech_amount);
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
                $pe++;
            }

            
            
        }
        
        $total = ['re_tot' => $re_tot, 'ce_tot' => $ce_tot, 'su_tot' => $su_tot, 'fa_tot' => $fa_tot, 'pe_tot' => $pe_tot, 'no_su' => $su, 'no_fa' => $fa, 'no_pe' => $pe];
       
       
        $rs = [];

        foreach($d1 as $d)
        {
            array_push($rs, $d);
        }
        
        //current page for pagination
        $page = $request->page;

        // manually slice array of product to display on page
        $perPage = 30;
        if($page != "")
        {
            $offset = ($page-1) * $perPage;
        }
        else
        {
            $offset = 0;
        }

        $rec = array_slice($rs, $offset, $perPage);

        $rec = new Paginator($rec, count($rs), $perPage, $page, ['path'  => $request->url(),'query' => $request->query(),]);

        return view('sdistributor.rechargereport_2', ['user' => $ob, 'recharge' => $rec, 'from_date' => $date_1, 'to_date' => $date_2, 'network' => $d2, 'total' => $total]); 

        
    }
}
