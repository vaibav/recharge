<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Libraries\GetCommon;

use App\Models\UserPersonalDetails;
use App\Models\UserAccountDetails;
use App\Models\AgentAreaDetails;
use App\Models\PaymentLedgerDetails;

class AgentRetailerCollectionController extends Controller
{
    //
    public function index(Request $request)
	{
        $user_1 = new UserPersonalDetails;
        $area_1 = new AgentAreaDetails;
        $pay_1 = new PaymentLedgerDetails;

        $ob = GetCommon::getUserDetails($request);

        $user_name = $ob->user;
        $user_name = strtoupper($user_name);

        $de_tot = 0;
        $ce_tot = 0;
        $pe_tot = 0;

        // debit (taken)
        $de_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                    ->where('pay_type', '=', 'C')
                    ->where('pay_status', '=', '1')
                    ->sum('pay_amount'); 

        // credit (paid)
        $ce_tot = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                    ->where('pay_type', '=', 'D')
                    ->where('pay_status', '=', '1')
                    ->sum('pay_amount'); 

        // round two digits fraction
        $de_tot = round($de_tot, 2);
        $ce_tot = round($ce_tot, 2);

        $pe_tot = floatval($de_tot) - floatval($ce_tot);

        $de_tot = $this->convertNumber($de_tot);
        $ce_tot = $this->convertNumber($ce_tot);
        $pe_tot = $this->convertNumber($pe_tot);

        $d3 = $pay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where(function($query) {
                            return $query
                            ->where('pay_type', '=', 'C')
                            ->orWhere('pay_type', '=', 'D');
                        })
                        ->get();
        
        $user_details = ['user_name' => $user_name, 'c_details' => $d3, 
                            'de_tot' => $de_tot, 'ce_tot' => $ce_tot, 'pe_tot' => $pe_tot];


        return view('retailer.collection_details', ['user' => $ob, 'user_details' => $user_details]);
        
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
