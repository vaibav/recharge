<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

use App\Models\UserAccountBalanceDetails;
use App\Models\UserPaymentDetails;
use App\Models\TransactionAllDetails;
use App\Models\NetworkDetails;
use App\Models\TransactionAllParentDetails;
use App\Models\RechargeAllDetails; 
use App\Models\UserRechargeDetails;
use App\Models\UserRechargeBillDetails;
use App\Models\UserRechargeApipartnerDetails;
use App\Models\UserRechargePaymentDetails;
use App\Models\UserRechargePaymentApipartnerDetails;
use App\Models\UserAccountDetails;

use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewParentDetails;

use App\Models\UserBankTransferDetails;
use App\Models\UserBeneficiaryAccVerifyDetails;

use App\Models\UserOldAccountDetails;
use App\Models\BackupRechargeDetails;
use App\Models\BackupRechargeParentDetails;

class RechargeAllController extends Controller
{
    //
    public function getDetails(Request $request)
    {
        $tran_1 = new TransactionAllDetails;
        $rech_6 = new RechargeAllDetails;

        $f_d = trim($request->from_date);
        $t_d = trim($request->to_date);
        
        $f_date = $f_d." 00:00:00";
        $t_date = $t_d." 23:59:59";

        $d1 = $tran_1->select('trans_id','trans_type', 'trans_option', 'user_name')
                ->with(['webrecharge1', 'webrecharge2', 'webrecharge3', 'apirecharge1', 'apirecharge2', 'apirecharge3'])
                ->whereBetween('created_at', [$f_date, $t_date])
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        $date_time = date('Y-m-d H:i:s');
        echo "<table border=1 cellspacing=0 cellpadding=2>";
        foreach($d1 as $d)
        {
            $v = 0;
            $trans_id = "";
            $trans_id = $d->trans_id;
            
           
            if($d->trans_type == 'WEB_RECHARGE')
            {
                
                $api_trans_id = '';
                $user_name = $d->user_name;
                $net_code = $d->webrecharge2->net_code;
                $rech_mobile = $d->webrecharge2->rech_mobile;
                $rech_amount = $d->webrecharge2->rech_amount;
                $rech_date = $d->webrecharge2->rech_date;

                if($d->trans_option == 1)
                {
                    $rech_net_per = $d->webrecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->webrecharge1[0]->rech_net_surp;
                    $rech_total = $d->webrecharge1[0]->rech_total;
                    $user_balance = $d->webrecharge1[0]->user_balance;
                    // PENDING OR SUCCESS
                    if ($d->webrecharge2->rech_status == 'SUCCESS')
                    {
                        // SUCCESS
                        $rech_status = "SUCCESS";
                        $rech_code = 1;
                    }
                    else if ($d->webrecharge2->rech_status == 'FAILURE')
                    {
                        // PENDING FAILURE
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }
                    else
                    {
                        // PENDING 
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }

                }
                else if($d->trans_option == 2)
                {
                    $rech_net_per = $d->webrecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->webrecharge1[0]->rech_net_surp;
                    $rech_total = $d->webrecharge1[0]->rech_total;
                    $user_balance = $d->webrecharge1[1]->user_balance;

                     // DEFINITELY FAILURE
                     $rech_status = "FAILURE";
                     $rech_code = 2;
                }

                $rech_mode = 'WEB';
                if (strpos($d->trans_id, 'VBG') !== false) {
                    $rech_mode = "GPRS";
                }

                $api_code = $d->webrecharge2->api_code;
                $rech_req_status = $d->webrecharge3->rech_req_status;
                $rech_all_mode = 'WEB_RECHARGE';
                $reply_opr_id = $d->webrecharge2->reply_opr_id;
                $reply_opr_date = $d->webrecharge2->reply_date;
                $v = 1;
            }
            else if($d->trans_type == 'API_RECHARGE')
            {
                $api_trans_id = $d->apirecharge2->api_trans_id;
                $user_name = $d->user_name;
                $net_code = $d->apirecharge2->net_code;
                $rech_mobile = $d->apirecharge2->rech_mobile;
                $rech_amount = $d->apirecharge2->rech_amount;
                $rech_date = $d->apirecharge2->rech_date;

                if($d->trans_option == 1)
                {
                    $rech_net_per = $d->apirecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->apirecharge1[0]->rech_net_surp;
                    $rech_total = $d->apirecharge1[0]->rech_total;
                    $user_balance = $d->apirecharge1[0]->user_balance;
                    // PENDING OR SUCCESS
                    if ($d->apirecharge2->rech_status == 'SUCCESS')
                    {
                        // SUCCESS
                        $rech_status = "SUCCESS";
                        $rech_code = 1;
                    }
                    else if ($d->apirecharge2->rech_status == 'FAILURE')
                    {
                        // PENDING FAILURE
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }
                    else
                    {
                        // PENDING 
                        $rech_status = "PENDING";
                        $rech_code = 0;
                    }

                   

                }
                else if($d->trans_option == 2)
                {
                    $rech_net_per = $d->apirecharge1[0]->rech_net_per;
                    $rech_net_surp = $d->apirecharge1[0]->rech_net_surp;
                    $rech_total = $d->apirecharge1[0]->rech_total;
                    $user_balance = $d->apirecharge1[1]->user_balance;

                     // DEFINITELY FAILURE
                     $rech_status = "FAILURE";
                     $rech_code = 2;
                }

                $rech_mode = 'API';

                $api_code = $d->apirecharge2->api_code;
                $rech_req_status = $d->apirecharge3->rech_req_status;
                $rech_all_mode = 'API_RECHARGE';
                $reply_opr_id = $d->apirecharge2->reply_opr_id;
                $reply_opr_date = $d->apirecharge2->reply_date;
                $v = 1;
            }

           
            

            if($v == 1)
            {
                $cnt = $rech_6->where([['trans_id', '=', $trans_id], ['rech_code', '=', $rech_code]])->count();

                
                if($cnt == 0)
                {
                    if($rech_req_status != strip_tags($rech_req_status)) {
                        // contains HTML
                        $rech_req_status = strip_tags($rech_req_status);
                    }

                    $data = [ 
                        'trans_id' => $trans_id,
                        'api_trans_id' => $api_trans_id,
                        'user_name' => $user_name,
                        'net_code' => $net_code,
                        'rech_mobile' => $rech_mobile,
                        'rech_amount' => $rech_amount,
                        'rech_date' => $rech_date,
                        'con_name' => '',
                        'con_acc_no' => '',
                        'con_mobile' => '',
                        'con_amount' => '',
                        'bank_acc_no' => '',
                        'bank_acc_name' => '',
                        'bank_name' => '',
                        'bank_ifsc' => '',
                        'bank_acc_mobile' => '',
                        'bank_acc_amount' => '',
                        'rech_net_per' => $rech_net_per,
                        'rech_net_surp' => $rech_net_surp,
                        'rech_total' => $rech_total,
                        'user_balance' => $user_balance,
                        'rech_req_status' => $rech_req_status,
                        'rech_status' => $rech_status,
                        'rech_mode' => $rech_mode,
                        'rech_all_mode' => $rech_all_mode,
                        'rech_code' => $rech_code,
                        'api_code' => $api_code,
                        'reply_opr_id' => $reply_opr_id,
                        'reply_date' => $reply_opr_date,
                        'created_at' => $date_time,
                        'updated_at' => $date_time
                    ];

                    $rech_6->insert($data);
                }

            }

            /*echo "<tr><td>".$j."</td><td>".$trans_id."</td>";
            echo "<td>".$api_trans_id."</td><td>".$user_name."</td>";

            echo "<td>".$net_code."</td><td>".$rech_mobile."</td>";
            echo "<td>".$rech_amount."</td><td>".$rech_date."</td>";
            

            echo "<td>".$rech_net_per."</td><td>".$rech_net_surp."</td>";
            echo "<td>".$rech_total."</td><td>".$user_balance."</td>";
            
            echo "<td>".$rech_req_status."</td><td>".$rech_status."</td>";
            echo "<td>".$rech_mode."</td><td>".$rech_all_mode."</td><td>".$rech_code."</td>";

            echo "<td>".$reply_opr_id."</td><td>".$reply_opr_date."</td></tr>";*/
            echo $j."--";

            $j++;
        }
        
        
    }

    public function getUserDetails($request)
    {
        
        $ob = new stdClass();
        $ob->user = "NONE";
        $ob->code = "NONE";
        $ob->mode = "NONE";
        $ob->ubal = 0;

        $uacc_1 = new UserAccountBalanceDetails;

		if($request->session()->has('user'))
        {
            $ob->user = $request->session()->get('user');
        }
        if($request->session()->has('code'))
        {
            $ob->code = $request->session()->get('code');
        }
        if($request->session()->has('mode'))
        {
            $ob->mode = $request->session()->get('mode');
        }

        $d1 = $uacc_1->select('user_balance')->where('user_name', '=', $ob->user)->get();
        foreach($d1 as $d)
        {
            $ob->ubal = $d->user_balance;
        }

        return $ob;
    }

    public function getUserRechargeDetails(Request $request)
    {
        $rech_6 = new RechargeAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        $u_name = trim($request->user_name);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        $d1 = $rech_2->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('user_name', '=', $u_name)
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 

        $str = "<h3>NORMAL DATA</h3>";
        $str = $str. "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $re_tot = 0;
        foreach($d1 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $re_tot = floatval($re_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $re_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";


        // API Data
        $d3 = $rech_a_2->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
        ->whereBetween('rech_date', [$f_date, $t_date])
        ->where('user_name', '=', $u_name)
        ->where('rech_status', '=', 'SUCCESS')
        ->orderBy('id', 'asc')->get(); 

     

        $str = $str . "<h3>API DATA</h3>";
        $str = $str . "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $ae_tot = 0;
        foreach($d3 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $ae_tot = floatval($ae_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $ae_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";

        
        $d2 = $rech_6->select('trans_id','user_name', 'net_code', 'rech_mobile', 'rech_amount', 'rech_total', 'rech_status', 'rech_date')
                ->whereBetween('rech_date', [$f_date, $t_date])
                ->where('user_name', '=', $u_name)
                ->where('rech_status', '=', 'SUCCESS')
                ->orderBy('id', 'asc')->get(); 

        $str = $str . "<h3>BACKUP DATA</h3>";
        $str = $str. "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>TR.ID</th><th>NET CODE</th><th>MOBILE</th><th>AMOUNT</th><th>TOTAL</th><th>STATUS</th><th>DATE</th></tr>";

        $j = 1;
        $ce_tot = 0;
        foreach($d2 as $d)
        {
            $str = $str . "<tr><td>" . $j . "</td>";
            $str = $str . "<td>" . $d->trans_id . "</td>";
            $str = $str . "<td>" . $d->net_code . "</td>";
            $str = $str . "<td>" . $d->rech_mobile . "</td>";
            $str = $str . "<td>" . $d->rech_amount . "</td>";
            $str = $str . "<td>" . $d->rech_total . "</td>";
            $str = $str . "<td>" . $d->rech_status . "</td>";
            $str = $str . "<td>" . $d->rech_date . "</td></tr>";

            $ce_tot = floatval($ce_tot) + floatval($d->rech_total);
            $j++;
        }

            $str = $str . "<tr><td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></td>";
            $str = $str . "<td>TOTAL</td>";
            $str = $str . "<td>" . $ce_tot . "</td>";
            $str = $str . "<td></td>";
            $str = $str . "<td></tr>";

        $str = $str."</table><br>";

        echo $str;
    }

    public function getUserRechargeTotal(Request $request)
    {
        $rech_6 = new RechargeAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $upay_1 = new UserPaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $user_2 = new UserAccountDetails;
        $tran_1 = new TransactionAllDetails;

        $re_1 = new UserRechargeDetails;
        $re_2 = new UserRechargeApipartnerDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
        $date_time = date("Y-m-d H:i:s");

        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CATEGORY</th><th>RECHARGE_TOTAL</th></tr>";

        $dx = $user_2->select('user_name', 'user_type')
                ->where('user_type', '=', 'RETAILER')
                ->orWhere('user_type', '=', 'API PARTNER')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $re_tot = 0;
            $ae_tot = 0;
            $ce_tot = 0;
            

            if($ux_type == "RETAILER")
            {
                $ce_tot = $re_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('rech_total'); 
            }
            else if($ux_type == "API PARTNER")
            {
                $ce_tot = $re_2->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'SUCCESS')
                                ->sum('rech_total'); 
            }
           
            //$ce_tot = number_format($ce_tot, 2, ".", "");
            
            $str = $str . "<tr><td></td>";
            $str = $str . "<td>" . $ux_name . "</td>";
            $str = $str . "<td>" . $ux_type . "</td>";
            $str = $str . "<td>BACKUP DATA</td>";
            $str = $str . "<td>" . $ce_tot . "</td></tr>";
            $j++;

            if($ce_tot != 0)
            {
                // Data insertion process
                $trans_id = rand(100000,999999);

                $arr1 = array('trans_id' => $trans_id, 'user_name' => 'admin', 'user_amount' => $this->convertNumber($ce_tot),
                            'user_req_date' => $t_date, 'grant_user_name' => $ux_name, 'grant_user_amount' => $this->convertNumber($ce_tot),
                            'grant_date' => $t_date, 'payment_mode' => 'USER_RECHARGE', 'user_remarks' => 'RECHARGED AMOUNT', 
                            'trans_status' => '1', 'created_at' => $date_time, 'updated_at' => $date_time);
                            
                $upay_1->insert($arr1);

                $arr2 = array('trans_id' => $trans_id, 'user_name' => $ux_name, 'trans_type' => 'USER_RECHARGE',
                            'trans_option' => '1', 'created_at' => $date_time, 'updated_at' => $date_time);
            
                $tran_1->insert($arr2);
            }
            

        }
        


        echo $str;
    }

    public function getUserRechargeAllTotal(Request $request)
    {
        $rech_6 = new RechargeAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_1 = new UserRechargeDetails;
        $upay_1 = new UserPaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $user_2 = new UserAccountDetails;
        $tran_1 = new TransactionAllDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
        $date_time = date("Y-m-d H:i:s");

        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CATEGORY</th><th>RECHARGE_TOTAL</th></tr>";

        $dx = $user_2->select('user_name', 'user_type')
                ->where('user_type', '=', 'RETAILER')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $re_tot = 0;
            $ae_tot = 0;
            $ce_tot = 0;
            

            //Backup Data
            $d3 = $rech_1->select('trans_id','rech_total')
                    ->whereBetween('rech_date', [$f_date, $t_date])
                    ->whereRaw('upper(user_name) = ?',[$ux_name])
                    ->where('rech_status', '=', 'SUCCESS')
                    ->orderBy('id', 'asc')->get(); 
                
            foreach($d3 as $r)
            {
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
            }

            
            $str = $str . "<tr><td></td>";
            $str = $str . "<td>" . $ux_name . "</td>";
            $str = $str . "<td>" . $ux_type . "</td>";
            $str = $str . "<td>BACKUP DATA</td>";
            $str = $str . "<td>" . $ce_tot . "</td></tr>";
            $j++;

            

        }
        


        echo $str;
    }

    public function getUserRechargeAllTotal1(Request $request)
    {
        $rech_6 = new RechargeAllDetails;
        $rech_2 = new UserRechargePaymentDetails;
        $rech_1 = new UserRechargeApipartnerDetails;
        $upay_1 = new UserPaymentDetails;
        $rech_a_2 = new UserRechargePaymentApipartnerDetails;
        $user_2 = new UserAccountDetails;
        $tran_1 = new TransactionAllDetails;

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);
        

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";
        $date_time = date("Y-m-d H:i:s");

        $str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        $str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CATEGORY</th><th>RECHARGE_TOTAL</th></tr>";

        $dx = $user_2->select('user_name', 'user_type')
                ->Where('user_type', '=', 'API PARTNER')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $re_tot = 0;
            $ae_tot = 0;
            $ce_tot = 0;
            

            //Backup Data
            $d3 = $rech_1->select('trans_id','rech_total')
                    ->whereBetween('rech_date', [$f_date, $t_date])
                    ->whereRaw('upper(user_name) = ?',[$ux_name])
                    ->where('rech_status', '=', 'SUCCESS')
                    ->orderBy('id', 'asc')->get(); 
                
            foreach($d3 as $r)
            {
                $ce_tot = floatval($ce_tot) + floatval($r->rech_total);
            }

            
            $str = $str . "<tr><td></td>";
            $str = $str . "<td>" . $ux_name . "</td>";
            $str = $str . "<td>" . $ux_type . "</td>";
            $str = $str . "<td>BACKUP DATA</td>";
            $str = $str . "<td>" . $ce_tot . "</td></tr>";
            $j++;

            

        }
        


        echo $str;
    }

    public function getUserCurrentRechargeTotal(Request $request)
    {
        $rech_1 = new UserRechargeDetails;
        $rech_n_1 = new UserRechargeNewDetails;
        $rech_2 = new UserRechargeApipartnerDetails;
        $rech_3 = new UserRechargeBillDetails;
        $upay_1 = new UserPaymentDetails;
        $user_2 = new UserAccountDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $bank_1 = new UserBankTransferDetails;
        $bank_2 = new UserBeneficiaryAccVerifyDetails;
        $back_1 = new UserOldAccountDetails;

        $ob = $this->getUserDetails($request);

        $f_date = "2018-10-01 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        $date_time = date("Y-m-d H:i:s");

        //$str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        //$str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CURRENT BALANCE</th><th>FUND TRANSFER</th><th>SUCCESS RECHARGE</th><th> PENDING RECHARGE</th><th>LEDGER BALANCE</th><th>DEVIATION</th></tr>";

        $str = "";
        $dx = $user_2->select('user_name', 'user_type')
                ->where('user_type', '=', 'RETAILER')
                ->orWhere('user_type', '=', 'API PARTNER')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $cu_bal = 0;
            $st_bal = 0;
            $nt_bal = 0;
            $ce_tot = 0;
            $pe_tot = 0;
            $eb_tot = 0;


            // Current Account Balance
            $d1 = $uacc_1->select('user_balance')
                    ->where('user_name', '=', $ux_name)->get();
                    
            if ($d1->count() > 0)
            {
                $cu_bal = $d1[0]->user_balance;
            }
           
            
            // User Payment Details
            $st_bal = $this->stockBalance($f_date, $t_date, $ux_name, $upay_1);

            $ux_name =strtoupper($ux_name);

            // User Recharge Details
            $ce_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(user_name) = ?',[$ux_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 

            
            $oce_tot = $back_1->whereRaw('upper(user_name) = ?',[$ux_name])
                            ->sum('user_amount'); 
            //$oce_tot = 0;


            // EB Success Data
            $eb_tot = $rech_3->whereBetween('created_at', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->where('con_status', '=', 'SUCCESS')
                                ->sum('con_total');

            // Money Transfer Data
            $bk_tot = $bank_1->whereBetween('mt_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->where('mt_status', '=', 'SUCCESS')
                                ->sum('mt_total');

             // Money verify Data
             $bv_tot = $bank_2->whereBetween('ben_date', [$f_date, $t_date])
                                    ->whereRaw('upper(user_name) = ?',[$ux_name])
                                    ->where('ben_status', '=', 'SUCCESS')
                                    ->sum('ben_surplus');                   

            // Pending Total
            $pe_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'PENDING')
                                ->where('rech_option', '=', '0')
                                ->sum('rech_total');
            
            $st_bal = round($st_bal, 2);
            $ce_tot = round($ce_tot, 2);
            $oce_tot = round($oce_tot, 2);
            $pe_tot = round($pe_tot, 2);
            $eb_tot = round($eb_tot, 2);
            $bk_tot = round($bk_tot, 2);

            $nt_bal = floatval($st_bal) - floatval($ce_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($oce_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($pe_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($eb_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($bk_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($bv_tot);

            $nt_bal = abs($nt_bal);

            $dev = floatval($cu_bal) - floatval($nt_bal);

            $dev = round($dev, 2);

            $dev = abs($dev);

            if(floatval($dev) > 0)
            {
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $j . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_name . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_type . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . $cu_bal . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($st_bal, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($oce_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($ce_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($eb_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($bk_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($bv_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($pe_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . number_format($nt_bal, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . number_format($dev, 2, ".", "") . "</td></tr>";
            }
            
           
            
            
            $j++;
            
            

            

        }
        


        return view('admin.adminstock_user_all', ['user' => $ob, 'data' => $str]);
    }

    public function getUserCurrentDistributorTotal(Request $request)
    {
        
        $rech_n_1 = new UserRechargeNewParentDetails;
        $upay_1 = new UserPaymentDetails;
        $user_2 = new UserAccountDetails;
        $uacc_1 = new UserAccountBalanceDetails;
        $back_1 = new UserOldAccountDetails;

        $ob = $this->getUserDetails($request);

        $f_date = "2019-08-01 00:00:00";
        $t_date = date("Y-m-d H:i:s");
        $date_time = date("Y-m-d H:i:s");

        //$str = "<table border = '1' cellspacing = '0' cellpadding ='4'>";
        //$str = $str . "<tr><th>NO</th><th>USER_NAME</th><th>USER_TYPE</th><th>CURRENT BALANCE</th><th>FUND TRANSFER</th><th>SUCCESS RECHARGE</th><th> PENDING RECHARGE</th><th>LEDGER BALANCE</th><th>DEVIATION</th></tr>";

        $str = "";
        $dx = $user_2->select('user_name', 'user_type')
                ->where('user_type', '=', 'DISTRIBUTOR')
                ->orWhere('user_type', '=', 'SUPER DISTRIBUTOR')
                ->orderBy('id', 'asc')->get(); 
        
        $j = 1;
        foreach($dx as $d)
        {
            $ux_name = $d->user_name;
            $ux_type = $d->user_type;

            $cu_bal = 0;
            $st_bal = 0;
            $nt_bal = 0;
            $ce_tot = 0;
            $pe_tot = 0;
            $eb_tot = 0;


            // Current Account Balance
            $d1 = $uacc_1->select('user_balance')
                    ->where('user_name', '=', $ux_name)->get();
                    
            if ($d1->count() > 0)
            {
                $cu_bal = $d1[0]->user_balance;
            }
           
            
            // User Payment Details
            $st_bal = $this->stockBalance($f_date, $t_date, $ux_name, $upay_1);

            $ux_name =strtoupper($ux_name);

            // User Recharge Details
            $ce_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                            ->whereRaw('upper(parent_name) = ?',[$ux_name])
                            ->where('rech_status', '=', 'SUCCESS')
                            ->sum('rech_total'); 

            $oce_tot = $back_1->whereRaw('upper(user_name) = ?',[$ux_name])
                                ->sum('user_amount');
           
            // Pending Total
            $pe_tot = $rech_n_1->whereBetween('rech_date', [$f_date, $t_date])
                                ->whereRaw('upper(parent_name) = ?',[$ux_name])
                                ->where('rech_status', '=', 'PENDING')
                                ->where('rech_option', '=', '0')
                                ->sum('rech_total');
            
            $st_bal = round($st_bal, 2);
            $ce_tot = round($ce_tot, 2);
            $oce_tot = round($oce_tot, 2);
            $pe_tot = round($pe_tot, 2);
            $eb_tot = round($eb_tot, 2);

            $ce_tot = floatval($ce_tot) + floatval($oce_tot);

            $nt_bal = floatval($st_bal) - floatval($ce_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($pe_tot);

            $nt_bal = abs($nt_bal);

            $nt_bal = floatval($nt_bal) - floatval($eb_tot);

            $nt_bal = abs($nt_bal);

            $dev = floatval($cu_bal) - floatval($nt_bal);

            $dev = abs($dev);

            if(floatval($dev) != 0)
            {
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $j . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_name . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;'>" . $ux_type . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . $cu_bal . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($st_bal, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($ce_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($pe_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;'>" . number_format($oce_tot, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . number_format($nt_bal, 2, ".", "") . "</td>";
                $str = $str . "<td style='font-size:12px;padding:7px 6px;text-align:right;font-weight:bold;'>" . number_format($dev, 2, ".", "") . "</td></tr>";
            }
            
           
            
            
            $j++;

            

        }
        


        return view('admin.adminstock_distributor_all', ['user' => $ob, 'data' => $str]);
    }

    public function stockBalance($f_date, $t_date, $user_name, $upay_1)
    {
        $user_name =strtoupper($user_name);

        $debit = $upay_1->whereRaw('upper(user_name) = ?',[$user_name])
                        ->where('trans_status', '=', 1)
                        ->whereBetween('grant_date', [$f_date, $t_date])
                        ->sum('grant_user_amount');
        
        $credit = $upay_1->whereRaw('upper(grant_user_name) = ?',[$user_name])
                            ->where('trans_status', '=', 1)
                            ->whereBetween('grant_date', [$f_date, $t_date])
                            ->sum('grant_user_amount');
        
        $balance = 0;

        //file_put_contents("acc.txt", $user_name."---".$debit."---".$credit."--".PHP_EOL, FILE_APPEND);

        $balance = floatval($debit) - floatval($credit);
        
        return $balance;
    }

    public function convertNumber($no)
    {
        $no = round($no, 2);
        $no = number_format($no, 2, '.', '');

        return $no;
    }
}
