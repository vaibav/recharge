<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetAPICommon;
use App\Libraries\BankRemitter;
use App\Libraries\RechargeInfo;
use App\Libraries\RechargeOffers;

use App\Models\RechargeInfoDetails;
use App\Models\NetworkDetails;

class AP_API_OfferController extends Controller
{
    //
    public function operator_check(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $auth_token = trim($request->auth_token);
        $mobile = trim($request->mobile);

        $net_type_code = "0";
        $net_code = "388";
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if(GetAPICommon::checkNumber($mobile))
            {
                list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OPERATOR_CHECK");
                if($api_code != 0)
                {
                    $res_content = RechargeOffers::checkOperator($api_code, $net_code, $mobile, $user_name, "A".rand(10000,99999));

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'No plans Available in this Network...'];
                }
 
            }
            else
            {
                $result = ['status' => '2', 'message' => 'Invalid Mobile No...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function plan_check(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $auth_token = trim($request->auth_token);
        $net_short = trim($request->operator);

        $net_type_code = "0";
        $net_code = "388";
        $mobile = "0";
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if($net_short != "")
            {
                list($net_code, $net_type_code, $net_status) = $this->getNetworkCode($net_short);

                list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "MOBILE_PLAN");
                if($api_code != 0)
                {
                    $res_content = RechargeOffers::getPlan($api_code, $net_code, $mobile, $user_name, "A".rand(10000,99999), $net_short);

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'No plans Available in this Network...'];
                }
 
            }
            else
            {
                $result = ['status' => '2', 'message' => 'Invalid Mobile No...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function eb_check(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $auth_token = trim($request->auth_token);
        $net_short = trim($request->operator);
        $mobile = trim($request->cons_no);

        $net_type_code = "0";
        $net_code = "388";
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if($net_short != "")
            {
                list($net_code, $net_type_code, $net_status) = $this->getNetworkCode($net_short);


                list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "BILL_CHECK");
                if($api_code != 0)
                {
                    $res_content = RechargeOffers::checkEB($api_code, $net_code, $mobile, $user_name, "A".rand(10000,99999), $net_short);

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'No plans Available in this Network...'];
                }
 
            }
            else
            {
                $result = ['status' => '2', 'message' => 'Invalid Mobile No...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function offer_check(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $auth_token = trim($request->auth_token);
        $net_short = trim($request->operator);
        $mobile = trim($request->mobile);

        $net_type_code = "0";
        $net_code = "388";
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if($net_short != "" && GetAPICommon::checkNumber($mobile))
            {
                list($net_code, $net_type_code, $net_status) = $this->getNetworkCode($net_short);

                list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "OFFER_121");
                if($api_code != 0)
                {

                    $res_content = RechargeOffers::getOffer121($api_code, $net_code, $mobile, $user_name, "A".rand(10000,99999));

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'No plans Available in this Network...'];
                }
 
            }
            else if($net_short == "")
            {
                $result = ['status' => '2', 'message' => 'Invalid Operator Code...'];
            }
            else if(!GetAPICommon::checkNumber($mobile))
            {
                $result = ['status' => '2', 'message' => 'Invalid Mobile No...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    public function dth_check(Request $request)
	{
        $ri = new RechargeInfoDetails;

        $auth_token = trim($request->auth_token);
        $net_short = trim($request->operator);
        $mobile = trim($request->mobile);

        $net_type_code = "0";
        $net_code = "388";
              
        list($q, $user_name, $user_code, $user_ubal) = GetAPICommon::getUserDetails($auth_token);

        $result = ['status' => '2', 'message' => 'none...'];

        if($q == 1)
        {
            // Valid API Partner
            if($net_short != "" && GetAPICommon::checkNumber($mobile))
            {
                list($net_code, $net_type_code, $net_status) = $this->getNetworkCode($net_short);

                list($api_code, $v) = RechargeInfo::getInfoNetworkLine($net_code, "DTH_INFO");
                if($api_code != 0)
                {

                    $res_content = RechargeOffers::getDth($api_code, $net_code, $mobile, $user_name, "A".rand(10000,99999));

                    $result = (array)json_decode($res_content);
                }
                else
                {
                    $result = ['status' => '2', 'message' => 'No details Available in this Network...'];
                }
 
            }
            else if($net_short == "")
            {
                $result = ['status' => '2', 'message' => 'Invalid Operator Code...'];
            }
            else if(!GetAPICommon::checkNumber($mobile))
            {
                $result = ['status' => '2', 'message' => 'Invalid Mobile No...'];
            }

        }
        else
        {
            $result = ['status' => '2', 'message' => 'Invalid API Key....'];
        }

    
        return response()->json($result, 200);
	
    }

    
    public function getNetworkCode($net_short)
    {
        $net_1 = new NetworkDetails;
        $net_code = 0;
        $net_type_code = 0;
        $net_status = 0;


        $d1 = $net_1->select('net_code', 'net_type_code', 'net_status')->where('net_short_code', '=', $net_short)->get();
        foreach($d1 as $d)
        {
            $net_code = $d->net_code;
            $net_type_code = $d->net_type_code;
            $net_status = $d->net_status;
        }

        return array($net_code, $net_type_code, $net_status);
    }
    
}
