<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;
use DateTime;

use App\Models\UserBankTransferDetails;

use App\Libraries\GetCommon;

class MoneyTransferReportController extends Controller
{
    //
    public function index_retailer(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('user.moneyreport_retailer', ['user' => $ob]);
        
    }

    public function index_admin(Request $request)
	{
        $ob = GetCommon::getUserDetails($request);

        return view('admin.moneyreport', ['user' => $ob]);
        
    }

    public function viewdate_retailer(Request $request)
    {
        $money_1 = new UserBankTransferDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        
        $d1 = $money_1->where('user_name', '=', $ob->user)
                        ->whereBetween('mt_date', [$f_date, $t_date])
                    ->orderBy('id', 'desc')->paginate(15); 

        
        
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('user.moneyreport_retailer_view', ['user' => $ob, 'money' => $d1, 'from_date' => $date_1, 'to_date' => $date_2]); 

        
    }

    public function viewdate_admin(Request $request)
    {
        $money_1 = new UserBankTransferDetails;

        $ob = GetCommon::getUserDetails($request);

        // Validation
        $this->validate($request, [
            'f_date' => 'required',
            't_date' => 'required'
        ],
        [
            'f_date.required' => ' The From Date is required.',
            't_date.required' => ' The To Date is required.'
            ]);

        $date_1 = trim($request->f_date);
        $date_2 = trim($request->t_date);

        $f_date = $date_1." 00:00:00";
        $t_date = $date_2." 23:59:59";

        
        $d1 = $money_1->whereBetween('mt_date', [$f_date, $t_date])
                    ->orderBy('id', 'desc')->paginate(15); 

        
        
        $cus_url = $request->url()."?".$request->getQueryString();
        
        $d1->setPath($cus_url);

        return view('admin.moneyreport_view', ['user' => $ob, 'money' => $d1, 'from_date' => $date_1, 'to_date' => $date_2]); 

        
    }
}
