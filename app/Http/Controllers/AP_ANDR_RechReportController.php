<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GetMobileCommon;

use App\Models\TransactionAllDetails;
use App\Models\UserRechargeNewDetails;
use App\Models\UserRechargeNewStatusDetails;
use App\Models\UserBankTransferDetails;
use App\Models\UserRechargeBillDetails;

use App\Models\NetworkDetails;
use App\Models\NetworkTypeDetails;
use App\Models\ApiProviderDetails;

class AP_ANDR_RechReportController extends Controller
{
    //
    public function index(Request $request)
	{
        $auth_token = trim($request->auth_token);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API Partner...
            $d1 = $this->getRechargeDetailsApipartner($user_name);

            return view('android.ap_recharge_report_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'recharge' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not ApiPartner";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view($auth_token)
	{
        $net_1 = new NetworkTypeDetails;
        $net_2 = new NetworkDetails;

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only ApiPartner...
            $pre_code = "0";
            $pos_code = "0";
            $dth_code = "0";
            $bil_code = "0";
            $mny_code = "0";

            $d1 = $net_1->select('net_type_code', 'net_type_name')->get();
            foreach($d1 as $d)
            {
                if($d->net_type_name == "PREPAID") {
                    $pre_code = $d->net_type_code;
                }
                else if($d->net_type_name == "POSTPAID") {
                    $pos_code = $d->net_type_code;
                }
                else if($d->net_type_name == "DTH") {
                    $dth_code = $d->net_type_code;
                }
                else if($d->net_type_name == "BILL PAYMENT") {
                    $bil_code = $d->net_type_code;
                }
                else if($d->net_type_name == "MONEY TRANSFER") {
                    $mny_code = $d->net_type_code;
                }
                
            }

            $code = [];
            $code[0] = $pre_code;
            $code[1] = $pos_code;
            $code[2] = $dth_code;

            $d2 = $net_2->select('net_code','net_name')->where('net_status', '=', '1')
                                ->where(function($query) use ($code){
                                    return $query
                                    ->where('net_type_code', '=', $code[0])
                                    ->orWhere('net_type_code', '=', $code[1])
                                    ->orWhere('net_type_code', '=', $code[2]);
                                })->get();

            $ar_web = [];
            foreach($d2 as $d)
            {
                array_push($ar_web, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }

            $ar_web1 = json_encode($ar_web);

            $d3 = $net_2->select('net_code','net_name')
                                ->where([['net_type_code', '=', $bil_code], ['net_status', '=', '1']])->get();

            $ar_bill = [];
            foreach($d3 as $d)
            {
                array_push($ar_bill, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }

            $ar_bill1 = json_encode($ar_bill);

            //-------------------------------------------

            $d4 = $net_2->select('net_code','net_name')
                                ->where([['net_type_code', '=', $mny_code], ['net_status', '=', '1']])->get();

            $ar_money = [];
            foreach($d4 as $d)
            {
                array_push($ar_money, ['net_code' => $d->net_code, 'net_name' => $d->net_name]);
            }

            $ar_money1 = json_encode($ar_money);
                   

            return view('android.ap_recharge_report_2', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'web' => $ar_web1, 'bill' => $ar_bill1, 'money' => $ar_money1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Apipartner";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function view_date(Request $request)
	{
        $auth_token = trim($request->auth_token);
        $f_date = trim($request->f_date);
        $t_date = trim($request->t_date);
        $rech_type = trim($request->rech_type);
        $net_code = trim($request->net_code);
        $user_mobile = trim($request->user_mobile);

        $z1 = 0;
        $user_name = "";
        $user_type = "";
        $user_ubal = "";
        $user_code = "";
        $d2 = [];

        list($q, $user_name, $user_type, $user_code, $user_ubal) = GetMobileCommon::getUserDetails($auth_token);

        if($q == 0) {
            $z1 = 2;
        }

        if($z1 == 0) {
            if($user_type != "API PARTNER") {
                $z1 = 1;
            }
        }

        if($z1 == 0)
        {
            //Only API Partner...
            $d1 = $this->getRechargeDetailsApipartner_1($user_name, $f_date, $t_date, $rech_type, $net_code, $user_mobile);

            return view('android.ap_recharge_report_1', ['user_name' => $user_name, 'user_bal' => $user_ubal, 
                                "auth_token" => $auth_token, 'recharge' => $d1]);
            
        }
        else if($z1 == 1)
        {
            echo "User is not Apipartner";
        }
        else if($z1 == 2)
        {
            echo "Invalid API token..";
        }
    }

    public function getRechargeDetailsApipartner($user_name)
    {
        $tran_1 = new TransactionAllDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $rs = [];
        $d1 = $tran_1->with(['newrecharge1', 'newrecharge2', 'billpayment', 'moneytransfer'])
                    ->where('user_name', '=', $user_name)
                    ->where(function($query){
                        return $query
                        ->where('trans_type', '=', 'API_RECHARGE')
                        ->orWhere('trans_type', '=', 'BILL_PAYMENT')
                        ->orWhere('trans_type', '=', 'BANK_TRANSFER');
                    })
                    ->orderBy('id', 'desc')
                    ->limit(20)->get(); 

        
        
      // return response()->json($d1, 200);
        
      $d2 = $net_2->select('net_code','net_name')->get();
      $d3 = $api_1->select('api_code','api_name')->get();
      
      $str = "";
      
      $j = 1;
      foreach($d1 as $d)
      {
         
          if($d->trans_type == "API_RECHARGE")
          {
              if(sizeof($d->newrecharge1) > 0)
              {
                  $net_name = "";
                  foreach($d2 as $r)
                  {
                      if($d->newrecharge1[0]->net_code == $r->net_code)
                          $net_name = $r->net_name;
                  }
  
                  $api_name = "";
                  foreach($d3 as $r)
                  {
                      if($d->newrecharge2->api_code == $r->api_code)
                          $api_name = $r->api_name;
                  }
  
                  $rech_status = "";
                  $status = "";
                  $o_bal = 0;
                  $u_bal = 0;
                  $r_tot = 0;
  
                  if($d->trans_option == 1)
                  {
                      $rech_status = $d->newrecharge1[0]->rech_status;
                      $rech_option = $d->newrecharge1[0]->rech_option;
                      $u_bal = $d->newrecharge1[0]->user_balance;
                      $r_tot = $d->newrecharge1[0]->rech_total;
                  }
                  else if($d->trans_option == 2)
                  {
                      $rech_status = $d->newrecharge1[1]->rech_status;
                      $rech_option = $d->newrecharge1[1]->rech_option;
                      $u_bal = $d->newrecharge1[1]->user_balance;
                      $r_tot = $d->newrecharge1[1]->rech_total;
                  }
                  if($rech_status == "PENDING" && $rech_option == "0")
                  {
                      $status = "PENDING";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  else if($rech_status == "PENDING" && $rech_option == "2")
                  {
                      $status = "FAILURE";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  else if($rech_status == "FAILURE" && $rech_option == "2")
                  {      
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                  }
                  else if ($rech_status == "SUCCESS")
                  {
                      $status = "SUCCESS";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  
                 
                $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                if($status == "SUCCESS") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "FAILURE") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "PENDING") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
            
                $str = $str."<p style='font-size: 20px;margin: 0;'>".$d->newrecharge1[0]->rech_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
                $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->newrecharge1[0]->rech_mobile."</p>"; 
                
                if($d->trans_option == 1) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->newrecharge2->reply_opr_id."</p>"; 
                }
                else if($d->trans_option == 2) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
                }

                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->newrecharge1[0]->rech_date."</p>"; 
                
                $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
                $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
                $str = $str."</div></div></div>";

        
              }
             
              
          }
          if($d->trans_type == "BILL_PAYMENT")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->billpayment[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $api_name = "";
              foreach($d3 as $r)
              {
                  if($d->billpayment[0]->api_code == $r->api_code)
                      $api_name = $r->api_name;
              }

              $rech_status = "";
              $status = "";
              $o_bal = 0;
              $u_bal = 0;
              $r_tot = 0;

             

              if($d->trans_option == 1)
              {
                  $rech_status = $d->billpayment[0]->con_status;
                  $rech_option = $d->billpayment[0]->con_option;          
                  $r_tot = $d->billpayment[0]->con_total;
                  $u_bal = $d->billpayment[0]->user_balance;
                 
              }
              else if($d->trans_option == 2)
              {  
                  $rech_status = $d->billpayment[1]->con_status;
                  $rech_option = $d->billpayment[1]->con_option;          
                  $r_tot = $d->billpayment[1]->con_total;
                  $u_bal = $d->billpayment[1]->user_balance;
                  
              }
              if($rech_status == "PENDING" && $rech_option == 1)
              {
                  $status = "PENDING";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "PENDING" && $rech_option == 2)
              {
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "FAILURE"  && $rech_option == 2)
              {      
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) - floatval($r_tot) ;
              }
              else if ($rech_status == "SUCCESS")
              {
                  $status = "SUCCESS";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              
             
                $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                if($status == "SUCCESS") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "FAILURE") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "PENDING") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
            
                $str = $str."<p style='font-size: 20px;margin: 0;'>".$d->billpayment[0]->con_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
                $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->billpayment[0]->con_acc_no."</p>"; 
                
                if($d->trans_option == 1) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->billpayment[0]->reply_opr_id."</p>"; 
                }
                else if($d->trans_option == 2) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
                }

                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->billpayment[0]->created_at."</p>"; 
                
                $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
                $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
                $str = $str."</div></div></div>";
              

             
              
              
          } 
          if($d->trans_type == "BANK_TRANSFER")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->moneytransfer[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $api_name = "";
              foreach($d3 as $r)
              {
                  if($d->moneytransfer[0]->api_code == $r->api_code)
                      $api_name = $r->api_name;
              }

              $rech_status = "";
              $status = "";
              $o_bal = 0;
              $u_bal = 0;
              $r_tot = 0;
              $b_code = "";

              if($d->trans_option == 1)
              {
                  $b_code = $d->moneytransfer[0]->bk_code;
                  $rech_status = $d->moneytransfer[0]->mt_status;
                  $rech_option = $d->moneytransfer[0]->mt_option;          
                  $r_tot = $d->moneytransfer[0]->mt_total;
                  $u_bal = $d->moneytransfer[0]->user_balance;
              }
              else if($d->trans_option == 2)
              {  
                  $b_code = $d->moneytransfer[1]->bk_code;
                  $rech_status = $d->moneytransfer[1]->mt_status;
                  $rech_option = $d->moneytransfer[1]->mt_option;          
                  $r_tot = $d->moneytransfer[1]->mt_total;
                  $u_bal = $d->moneytransfer[1]->user_balance;
                  
              }
              if($rech_status == "PENDING" && $rech_option == 1)
              {
                  $status = "PENDING";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "PENDING" && $rech_option == 2)
              {
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "FAILURE"  && $rech_option == 2)
              {      
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) - floatval($r_tot) ;
                  
              }
              else if ($rech_status == "SUCCESS")
              {
                  $status = "SUCCESS";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }

                $bank = "";
                if($b_code == "UTIB") {
                    $bank = "AXIS BANK";
                }
                else if($b_code == "BARB") {
                    $bank = "BANK OF BARODA";
                }
                else if($b_code == "CNRB") {
                    $bank = "CANARA BANK";
                }
                else if($b_code == "CORP") {
                    $bank = "CORPORATION BANK";
                }
                else if($b_code == "CIUB") {
                    $bank = "CITY UNION BANK";
                }
                else if($b_code == "HDFC") {
                    $bank = "HDFC BANK";
                }
                else if($b_code == "IBKL") {
                    $bank = "IDBI BANK LTD";
                }
                else if($b_code == "IDIB") {
                    $bank = "INDIAN BANK";
                }
                else if($b_code == "IOBA") {
                    $bank = "INDIAN OVERSEAS BANK";
                }
                else if($b_code == "ICIC") {
                    $bank = "ICICI BANK";
                }
                else if($b_code == "KVBL") {
                    $bank = "KARUR VYSYA BANK";
                }
                else if($b_code == "SBIN") {
                    $bank = "STATE BANK OF INDIA";
                }
                else if($b_code == "SYNB") {
                    $bank = "SYNDICATE BANK";
                }
                else if($b_code == "TMBL") {
                    $bank = "TAMILNADU MERCANTILE BANK";
                }
                else if($b_code == "VIJB") {
                    $bank = "VIJAYA BANK";
                }
                else if($b_code == "") {
                    $bank = $b_code;
                }
                else {
                    $bank = $b_code;
                }
              
             
              $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
              $str = $str."<div class='row' style='margin:0px 0px;'>"; 
              if($status == "SUCCESS") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
              else if($status == "FAILURE") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
              else if($status == "PENDING") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
          
              $str = $str."<p style='font-size: 16px;margin: 0;'>".$d->moneytransfer[0]->mt_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
              $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
              $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$bank."</p>"; 
              $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->moneytransfer[0]->bk_acc_no."</p>"; 
              
              if($d->trans_option == 1) {
                  $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->moneytransfer[0]->mt_reply_id."</p>"; 
              }
              else if($d->trans_option == 2) {
                  $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
              }
              $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->moneytransfer[0]->mt_date."</p>"; 
              
              $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
              $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
              $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
              $str = $str."</div></div></div>";
              

             
          }                                        
         
                       
          $j++;
      }
        
      
        return $str; 
    }

    public function getRechargeDetailsApipartner_1($user_name, $f_date, $t_date, $rech_type, $net_code, $user_mobile)
    {
        $tran_1 = new TransactionAllDetails;
        $net_2 = new NetworkDetails;
        $api_1 = new ApiProviderDetails;

        $user_name = strtoupper($user_name);

        $d1 = [];

        if($f_date != "" && $t_date != "")
        {
            $f_date = $f_date." 00:00:00";
            $t_date = $t_date." 23:59:59";
        }

        if($f_date != "" && $t_date != "" && $rech_type == "WEB_RECHARGE")
        {
            $d1 = $tran_1->with(['newrecharge1', 'newrecharge2'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'API_RECHARGE')
                    ->orderBy('id', 'desc')->get(); 
            
            if($net_code != "" && $net_code != "-")
            {
                $d1 = $d1->filter(function ($d) use ($net_code){
                    return $d->newrecharge1[0]->net_code == $net_code;
                });
            }
            else  if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->newrecharge1[0]->rech_mobile == $user_mobile;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BILL_PAYMENT")
        {
            $d1 = $tran_1->with(['billpayment'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BILL_PAYMENT')
                    ->orderBy('id', 'desc')->get(); 

            if($net_code != "" && $net_code != "-")
            {
                $d1 = $d1->filter(function ($d) use ($net_code){
                    return $d->billpayment[0]->net_code == $net_code;
                });
            }
            else  if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->billpayment[0]->con_acc_no == $user_mobile;
                });
            }
        }
        else if($f_date != "" && $t_date != "" && $rech_type == "BANK_TRANSFER")
        {
            $d1 = $tran_1->with(['moneytransfer'])
                    ->whereRaw('upper(user_name) = ?',[$user_name])
                    ->whereBetween('created_at', [$f_date, $t_date])
                    ->where('trans_type', '=', 'BANK_TRANSFER')
                    ->orderBy('id', 'desc')->get(); 

            if($net_code != "" && $net_code != "-")
            {
                $d1 = $d1->filter(function ($d) use ($net_code){
                    return $d->moneytransfer[0]->net_code == $net_code;
                });
            }
            else  if($user_mobile != "")
            {
                $d1 = $d1->filter(function ($d) use ($user_mobile){
                    return $d->moneytransfer[0]->bk_acc_no == $user_mobile;
                });
            }
        }
        
       
        
        
      // return response()->json($d1, 200);
        
      $d2 = $net_2->select('net_code','net_name')->get();
      $d3 = $api_1->select('api_code','api_name')->get();
      
      $str = "";
      
      $j = 1;
      foreach($d1 as $d)
      {
         
          if($d->trans_type == "API_RECHARGE")
          {
              if(sizeof($d->newrecharge1) > 0)
              {
                  $net_name = "";
                  foreach($d2 as $r)
                  {
                      if($d->newrecharge1[0]->net_code == $r->net_code)
                          $net_name = $r->net_name;
                  }
  
                  $api_name = "";
                  foreach($d3 as $r)
                  {
                      if($d->newrecharge2->api_code == $r->api_code)
                          $api_name = $r->api_name;
                  }
  
                  $rech_status = "";
                  $status = "";
                  $o_bal = 0;
                  $u_bal = 0;
                  $r_tot = 0;
  
                  if($d->trans_option == 1)
                  {
                      $rech_status = $d->newrecharge1[0]->rech_status;
                      $rech_option = $d->newrecharge1[0]->rech_option;
                      $u_bal = $d->newrecharge1[0]->user_balance;
                      $r_tot = $d->newrecharge1[0]->rech_total;
                  }
                  else if($d->trans_option == 2)
                  {
                      $rech_status = $d->newrecharge1[1]->rech_status;
                      $rech_option = $d->newrecharge1[1]->rech_option;
                      $u_bal = $d->newrecharge1[1]->user_balance;
                      $r_tot = $d->newrecharge1[1]->rech_total;
                  }
                  if($rech_status == "PENDING" && $rech_option == "0")
                  {
                      $status = "PENDING";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  else if($rech_status == "PENDING" && $rech_option == "2")
                  {
                      $status = "FAILURE";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  else if($rech_status == "FAILURE" && $rech_option == "2")
                  {      
                        $status = "FAILURE";
                        $o_bal = floatval($u_bal) - floatval($r_tot);
                  }
                  else if ($rech_status == "SUCCESS")
                  {
                      $status = "SUCCESS";
                      $o_bal = floatval($u_bal) + floatval($r_tot);
                  }
                  
                 
                $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                if($status == "SUCCESS") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "FAILURE") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "PENDING") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
            
                $str = $str."<p style='font-size: 20px;margin: 0;'>".$d->newrecharge1[0]->rech_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
                $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->newrecharge1[0]->rech_mobile."</p>"; 
                
                if($d->trans_option == 1) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->newrecharge2->reply_opr_id."</p>"; 
                }
                else if($d->trans_option == 2) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
                }

                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->newrecharge1[0]->rech_date."</p>"; 
                
                $str = $str."</div><div class='col-3 col-sm-3 col-md-2 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
                $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
                $str = $str."</div></div></div>";

        
              }
             
              
          }
          if($d->trans_type == "BILL_PAYMENT")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->billpayment[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $api_name = "";
              foreach($d3 as $r)
              {
                  if($d->billpayment[0]->api_code == $r->api_code)
                      $api_name = $r->api_name;
              }

              $rech_status = "";
              $status = "";
              $o_bal = 0;
              $u_bal = 0;
              $r_tot = 0;

             

              if($d->trans_option == 1)
              {
                  $rech_status = $d->billpayment[0]->con_status;
                  $rech_option = $d->billpayment[0]->con_option;          
                  $r_tot = $d->billpayment[0]->con_total;
                  $u_bal = $d->billpayment[0]->user_balance;
                 
              }
              else if($d->trans_option == 2)
              {  
                  $rech_status = $d->billpayment[1]->con_status;
                  $rech_option = $d->billpayment[1]->con_option;          
                  $r_tot = $d->billpayment[1]->con_total;
                  $u_bal = $d->billpayment[1]->user_balance;
                  
              }
              if($rech_status == "PENDING" && $rech_option == 1)
              {
                  $status = "PENDING";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "PENDING" && $rech_option == 2)
              {
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "FAILURE"  && $rech_option == 2)
              {      
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) - floatval($r_tot) ;
              }
              else if ($rech_status == "SUCCESS")
              {
                  $status = "SUCCESS";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              
             
                $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
                $str = $str."<div class='row' style='margin:0px 0px;'>"; 
                if($status == "SUCCESS") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "FAILURE") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
                else if($status == "PENDING") {
                    $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
                }
            
                $str = $str."<p style='font-size: 20px;margin: 0;'>".$d->billpayment[0]->con_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
                $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$net_name."</p>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->billpayment[0]->con_acc_no."</p>"; 
                
                if($d->trans_option == 1) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->billpayment[0]->reply_opr_id."</p>"; 
                }
                else if($d->trans_option == 2) {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
                }

                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->billpayment[0]->created_at."</p>"; 
                
                $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
                $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
                $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
                $str = $str."</div></div></div>";
              

             
              
              
          } 
          if($d->trans_type == "BANK_TRANSFER")
          {
              $net_name = "";
              foreach($d2 as $r)
              {
                  if($d->moneytransfer[0]->net_code == $r->net_code)
                      $net_name = $r->net_name;
              }

              $api_name = "";
              foreach($d3 as $r)
              {
                  if($d->moneytransfer[0]->api_code == $r->api_code)
                      $api_name = $r->api_name;
              }

              $rech_status = "";
              $status = "";
              $o_bal = 0;
              $u_bal = 0;
              $r_tot = 0;
              $b_code = "";

              if($d->trans_option == 1)
              {
                  $b_code = $d->moneytransfer[0]->bk_code;
                  $rech_status = $d->moneytransfer[0]->mt_status;
                  $rech_option = $d->moneytransfer[0]->mt_option;          
                  $r_tot = $d->moneytransfer[0]->mt_total;
                  $u_bal = $d->moneytransfer[0]->user_balance;
              }
              else if($d->trans_option == 2)
              {  
                  $b_code = $d->moneytransfer[1]->bk_code;
                  $rech_status = $d->moneytransfer[1]->mt_status;
                  $rech_option = $d->moneytransfer[1]->mt_option;          
                  $r_tot = $d->moneytransfer[1]->mt_total;
                  $u_bal = $d->moneytransfer[1]->user_balance;
                  
              }
              if($rech_status == "PENDING" && $rech_option == 1)
              {
                  $status = "PENDING";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "PENDING" && $rech_option == 2)
              {
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }
              else if($rech_status == "FAILURE"  && $rech_option == 2)
              {      
                  $status = "FAILURE";
                  $o_bal = floatval($u_bal) - floatval($r_tot) ;
                  
              }
              else if ($rech_status == "SUCCESS")
              {
                  $status = "SUCCESS";
                  $o_bal = floatval($u_bal) + floatval($r_tot);
              }

                $bank = "";
                if($b_code == "UTIB") {
                    $bank = "AXIS BANK";
                }
                else if($b_code == "BARB") {
                    $bank = "BANK OF BARODA";
                }
                else if($b_code == "CNRB") {
                    $bank = "CANARA BANK";
                }
                else if($b_code == "CORP") {
                    $bank = "CORPORATION BANK";
                }
                else if($b_code == "CIUB") {
                    $bank = "CITY UNION BANK";
                }
                else if($b_code == "HDFC") {
                    $bank = "HDFC BANK";
                }
                else if($b_code == "IBKL") {
                    $bank = "IDBI BANK LTD";
                }
                else if($b_code == "IDIB") {
                    $bank = "INDIAN BANK";
                }
                else if($b_code == "IOBA") {
                    $bank = "INDIAN OVERSEAS BANK";
                }
                else if($b_code == "ICIC") {
                    $bank = "ICICI BANK";
                }
                else if($b_code == "KVBL") {
                    $bank = "KARUR VYSYA BANK";
                }
                else if($b_code == "SBIN") {
                    $bank = "STATE BANK OF INDIA";
                }
                else if($b_code == "SYNB") {
                    $bank = "SYNDICATE BANK";
                }
                else if($b_code == "TMBL") {
                    $bank = "TAMILNADU MERCANTILE BANK";
                }
                else if($b_code == "VIJB") {
                    $bank = "VIJAYA BANK";
                }
                else if($b_code == "") {
                    $bank = $b_code;
                }
                else {
                    $bank = $b_code;
                }
              
             
              $str = $str."<div style='margin:10px 10px;' class='card z-depth-1' >"; 
              $str = $str."<div class='row' style='margin:0px 0px;'>"; 
              if($status == "SUCCESS") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #81c784 green lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
              else if($status == "FAILURE") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center #e57373 red lighten-2 white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
              else if($status == "PENDING") {
                  $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center primary-color white-text' style='margin:0px 0px;padding: 0px 4px;'>"; 
              }
          
              $str = $str."<p style='font-size: 16px;margin: 0;'>".$d->moneytransfer[0]->mt_amount."</p><label style='font-size: 10px'>".$status."</label> "; 
              $str = $str."</div><div class='col-7 col-sm-7 col-md-8 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
              $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$bank."</p>"; 
              $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>".$d->moneytransfer[0]->bk_acc_no."</p>"; 
              
              if($d->trans_option == 1) {
                  $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->moneytransfer[0]->mt_reply_id."</p>"; 
              }
              else if($d->trans_option == 2) {
                  $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'></p>";  
              }

              $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 10px;'>".$d->moneytransfer[0]->mt_date."</p>"; 
              
              $str = $str."</div><div class='col-3 col-sm-3 col-md-3 text-left' style='margin:0px 0px;padding: 0px 4px;'>";
              $str = $str."<p class = 'text-muted' style='margin:12px 2px;font-size: 11px;'>O:".number_format($o_bal,2, ".", "")."</p>";
              $str = $str."<p class = 'text-muted' style='margin:4px 2px;font-size: 11px;'>C:".number_format($u_bal,2, ".", "")."</p>";
              $str = $str."</div></div></div>";
              

             
          }                                        
         
                       
          $j++;
      }
        
      
        return $str; 
    }

}
