<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>VaibavOnline</title>
    <link rel='stylesheet' href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style type="text/css">
         @import url(https://fonts.googleapis.com/css?family=Gudea:400,700);
        body
        {
            -webkit-perspective: 800px;
            perspective: 800px;
            height: 100vh;
            margin: 0;
            overflow: hidden;
            font-family: 'Gudea', sans-serif;
            background: #EA5C54;
            background: linear-gradient(135deg, #EA5C54 0%, #bb6dec 100%);
        }

       
        .login_title 
        {
            color: #afb1be;
            height: 100px;
            text-align: left;
            font-size: 16px;
        }
        body .login_fields {
            height: 238px;
            
            left: 0;
        }
        body .login .validation {
            position: absolute;
            z-index: 1;
            right: 10px;
            top: 6px;
            opacity: 0;
        }
        body .login_fields .icon {
            position: absolute;
            z-index: 1;
            left: 8px;
            top: 14px;
            opacity: .5;
        }
        body .login_fields input[type='password'] {
            color: #DC6180 !important;
        }
        
        body .login_fields__user, body .login_fields__password 
        {
            position: relative;
        }
        body .login_fields__submit {
            position: relative;
            top: 30px;
            left: 0;
            width: 80%;
            right: 0;
            margin: auto;
        }
        body .login_fields__submit .forgot 
        {
            float: right;
            font-size: 10px;
            margin-top: 11px;
            text-decoration: underline;
        }
        body .login_fields__submit .forgot a {
            color: #606479;
        }
        body .login_fields__submit input {
            border-radius: 50px;
            background: transparent;
            padding: 10px 40px;
            border: 2px solid #DC6180;
            color: #DC6180;
            text-transform: uppercase;
            font-size: 11px;
            transition-property: background,color;
            transition-duration: .2s;
        }
        body .login_fields__submit input:focus 
        {
            box-shadow: none;
            outline: none;
        }
        body .login_fields__submit input:hover 
        {
            color: white;
            background: #DC6180;
            cursor: pointer;
            transition-property: background,color;
            transition-duration: .2s;
        }

        body .disclaimer 
        {
            bottom: 20px;
            left: 35px;
            color: #afb1be;
            font-size: 12px;
            text-align: justify;
        }

        @media (min-width:320px)  
        {
            /* smartphones, portrait iPhone, portrait 480x320 phones (Android) */ 
            .img_logo
            {
                height: 37px; 
                width: 110px;
            }
            body .login_fields input[type='text'], body .login_fields input[type='password'] 
            {
                color: #afb1be;
                position: inherit;
                width: 90%;
                overflow-x: hidden;
                font-size: 14px;
                height: 26px;
                margin-top: -2px;
                margin-left:-26px;
                margin-bottom: 0px;
                background: #32364a;
                left: 0;
                padding: 10px 65px;
                border-top: 2px solid #393d52;
                border-bottom: 2px solid #393d52;
                border-right: none;
                border-left: none;
                outline: none;
                font-family: 'Gudea', sans-serif;
                box-shadow: none;
            }
        }
        @media (min-width:480px)  
        { 	
            /* smartphones, Android phones, landscape iPhone */ 
            .img_logo
            {
                height: 37px; 
                width: 110px;
            }
        }
        @media (min-width:600px)  
        { 	
            /* portrait tablets, portrait iPad, e-readers (Nook/Kindle), landscape 800x480 phones (Android) */ 
            .img_logo
            {
                height: 39px; 
                width: 130px;
            }
        }
        @media (min-width:768px)  
        { 	
            /* tablet, landscape iPad, lo-res laptops ands desktops */ 
            .login
            {
                opacity: 1;
                top: 20px;
                -webkit-transition-timing-function: cubic-bezier(0.68, -0.25, 0.265, 0.85);
                transition-property: opacity,box-shadow,top,left,-webkit-transform;
                transition-property: transform,opacity,box-shadow,top,left;
                transition-property: transform,opacity,box-shadow,top,left,-webkit-transform;
                transition-duration: .5s;
                border-top: 2px solid #D8312A;
                background: #35394a;
                margin: 15px 55px;
                background: linear-gradient(45deg, #35394a 0%, #1f222e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#35394a', endColorstr='#1f222e',GradientType=1 );
            }
            body .login_fields input[type='text'], body .login_fields input[type='password'] 
            {
                color: #afb1be;
                position: inherit;
                width: 90%;
                overflow-x: hidden;
                font-size: 14px;
                height: 21px;
                margin-top: -2px;
                margin-left:-26px;
                margin-bottom: 0px;
                background: #32364a;
                left: 0;
                padding: 10px 65px;
                border-top: 2px solid #393d52;
                border-bottom: 2px solid #393d52;
                border-right: none;
                border-left: none;
                outline: none;
                font-family: 'Gudea', sans-serif;
                box-shadow: none;
            }
        }
        @media (min-width:1024px) 
        { 
            /* big landscape tablets, laptops, and desktops */ 
            .login
            {
                opacity: 1;
                top: 20px;
                -webkit-transition-timing-function: cubic-bezier(0.68, -0.25, 0.265, 0.85);
                transition-property: opacity,box-shadow,top,left,-webkit-transform;
                transition-property: transform,opacity,box-shadow,top,left;
                transition-property: transform,opacity,box-shadow,top,left,-webkit-transform;
                transition-duration: .5s;
                border-top: 2px solid #D8312A;
                background: #35394a;
                margin: 15px 15px;
                background: linear-gradient(45deg, #35394a 0%, #1f222e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#35394a', endColorstr='#1f222e',GradientType=1 );
            }

            body .login_fields input[type='text'], body .login_fields input[type='password'] 
            {
                color: #afb1be;
                position: inherit;
                width: 71%;
                overflow-x: hidden;
                font-size: 14px;
                height: 21px;
                margin-top: -2px;
                margin-left:-29px;
                margin-bottom: 0px;
                background: #32364a;
                left: 0;
                padding: 10px 65px;
                border-top: 2px solid #393d52;
                border-bottom: 2px solid #393d52;
                border-right: none;
                border-left: none;
                outline: none;
                font-family: 'Gudea', sans-serif;
                box-shadow: none;
            }
        }
        @media (min-width:1281px) 
        { 
            /* hi-res laptops and desktops */ 
            .login
            {
                opacity: 1;
                top: 0px;
                -webkit-transition-timing-function: cubic-bezier(0.68, -0.25, 0.265, 0.85);
                transition-property: opacity,box-shadow,top,left,-webkit-transform;
                transition-property: transform,opacity,box-shadow,top,left;
                transition-property: transform,opacity,box-shadow,top,left,-webkit-transform;
                transition-duration: .5s;
                border-top: 2px solid #D8312A;
                background: #35394a;
                margin: 15px 55px;
                background: linear-gradient(45deg, #35394a 0%, #1f222e 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#35394a', endColorstr='#1f222e',GradientType=1 );
            }

            body .login_fields input[type='text'], body .login_fields input[type='password'] 
            {
                color: #afb1be;
                position: inherit;
                width: 70%;
                overflow-x: hidden;
                font-size: 14px;
                height: 21px;
                margin-top: -2px;
                margin-left:-24px;
                margin-bottom: 0px;
                background: #32364a;
                left: 0;
                padding: 10px 65px;
                border-top: 2px solid #393d52;
                border-bottom: 2px solid #393d52;
                border-right: none;
                border-left: none;
                outline: none;
                font-family: 'Gudea', sans-serif;
                box-shadow: none;
            }

        }

    </style>
</head>
<body>

    <div class="row">
        <div class="col s12 m12 l4 xl4"></div>
        <div class="col s12 m12 l4 xl4" style="margin-top: 50px;">
            <div class="card login" >
                <div class="card-content white-text">
                    <div class='login_title'>
                        <span style="font-size:22px;">VaibavOnline Login</span>
                    </div>

                    <form id = "id_login_form" action="{{url('login_check')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class='login_fields'>
                        <div class='login_fields__user'>
                            <div class='icon'>
                                <img src="{{ asset('img/small/user_icon_copy.png') }}">
                            </div>
                            <input placeholder='Username' type='text' name="username" id='username' />
                        </div>

                        <div class='login_fields__password'>
                            <div class='icon'>
                            <img src="{{ asset('img/small/lock_icon_copy.png') }}">
                            </div>
                            <input placeholder='Password' type='password' name='password' id='password' />
                                
                        </div>

                        <div class='login_fields__submit'>
                            <input type='submit' value='Log In' name='btn_login' id = "btn_login" />
                            <div class='forgot'>
                            <a href='#'>Forgotten password?</a>
                            </div>
                        </div>

                        <br>
                        <br>
                        <br>
                        <?php
                            if(session()->has('data'))
                            {
                                $op = session('data');
                                echo "<p id = 'id_err'>".$op['output']."</p>";
                            }
                            else
                            {
                                echo "<p id = 'id_err'></p>";
                            }
                        ?>
                        

                    </div>

                    </form>
                    
                    <div class='disclaimer'>
                    <p>This is VaibavOnline Multi Recharge Portal Exclusively for Vaibav Customers. Customers can recharge their mobile numbers and get report in this portal.</p>
                    </div>
                </div>
               
            </div>
        </div>
        <div class="col s12 m12 l4 xl4"></div>
    </div>


    <script src='https:////cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http:////ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $('#btn_login').on('click',function(e)
            {
                e.preventDefault();

                var a = $('#username').val();
                var b = $('#username').val();

                if(a != "" && b != "")
                {
                    $('#id_login_form').attr('action', "{{url('login_check')}}");
                    $('#id_login_form').submit();
                }
                else if(a == "")
                {
                    $('#id_err').val("User Name is Empty...");
                }
                else if(b == "")
                {
                    $('#id_err').val("Password is Empty...");
                }
            });
        });
    </script>
</body>
</html>