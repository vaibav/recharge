<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('sdistributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_user')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">User Network Details : <?php echo $user->user; ?></span>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                  <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Code</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                  <th style='font-size:12px;padding:7px 8px;'>From Amount</th>
                                  <th style='font-size:12px;padding:7px 8px;'>To Amount</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Percentage </th>
                                  <th style='font-size:12px;padding:7px 8px;'>Surplus Charge </th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                              <?php 
                                        $j = 1;
                                        foreach($packDetails as $f)
                                        {
                                            $netName = "";
                                            $user_kyc = "";
                                            foreach($networkDetails as $r)
                                            {
                                                if($f->net_code == $r->net_code)
                                                {
                                                    $netName = $r->net_name;
                                                    break;
                                                }
                                            }

                                           

                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_code."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$netName."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->from_amt."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->to_amt."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_per."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_surp."</td>";
                                         

                                        
                                        
                                          
                                          echo "</tr>";

                                          
                                          $j++;
                                        }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('common.bottom')

    
   

    <script>
     $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
   
      });
    </script>
    </body>
</html>