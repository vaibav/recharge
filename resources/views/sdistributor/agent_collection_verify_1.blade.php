<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('sdistributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_user')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                <span class="card-title" style = "padding:12px;">Agent Collection Verify</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <form id= "rech_form" class="form-horizontal" action="{{url('sd_collection_verify_view')}}" method="get" accept-charset="UTF-8">
                           

                             <div class="row" style = "margin-bottom:8px;">
                                <div class="input-field col s12 m12 l5 xl5" style="margin: 3px 0px;">
                                    <input type="text" id="id_from_date" name="from_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                    <label for = "id_from_date">From Date</label>
                                </div>
                            </div>
                            
                            <div class="row" style = "margin-bottom:8px;">
                                <div class="input-field col s12 m12 l5 xl5" style="margin: 3px 0px;">
                                    <input type="text" id="id_to_date" name="to_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                    <label for = "id_to_date">To Date</label>
                                </div>
                            </div>
                                
                            <div class="row" style = "margin-bottom:8px;">
                                <div class="input-field col s12 m12 l5 xl5" style="margin: 3px 0px;">
                                        <select id="id_agent_name"  name = "agent_name" >
                                            <option value = '-'>----Select----</option>
                                        <?php
                                            $str = "";
                                            foreach($agent_details as $f)
                                            {
                                                $str = $str."<option value='".$f->user_name."'>".$f->user_name."</option>";
                                            }
                                            echo $str;
                                        ?>
                                        </select>
                                        <label>Agent Name</label>
                                </div>
                                <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                    <p>&nbsp;</p>
                                </div>
                                <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                
                                </div>
                            </div>
                                
                                
                                

                               
                                                    
                            <div class="row">
                                <div class="col s12 m12 l2 xl2">
                                        <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                <i class="material-icons right">send</i>
                                        </button>
                                </div>

                                <div class="col s12 m12 l2 xl2">
                                    <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                <i class="material-icons right">cloud</i>
                                        </button>
                                </div>
                                <div class="col s12 m12 l2 xl2">
                                        
                                </div>
                                    
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Body --> 

               
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

    

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

            
            

             function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

      });
    </script>
    </body>
</html>
