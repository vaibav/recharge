<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles Icons -->
        <link href="{{ asset('css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/imple-line-icons.css') }}" rel="stylesheet">

        <!-- Main Styles  -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">

        <!-- Styles -->
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="img/brand/logo.png" width="100" height="40" alt="VaibavOnline" >
        <img class="navbar-brand-minimized" src="img/brand/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show" id="side">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <ul class="nav navbar-nav ml-auto">
        
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-bell-o"></i> Balance
              <span class="badge badge-info">500.00</span>
            </a>
            
            <div class="dropdown-header text-center">
              <strong>Settings</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-user"></i> Change Password</a>
            
            
            <div class="divider"></div>
            
            <a class="dropdown-item" href="#">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
     
    </header>


    <div class="app-body">
      <div class="sidebar" data-toggle="sidebar-hide">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="index.html">
                <i class="nav-icon icon-speedometer"></i> Admin
                <span class="badge badge-primary">NEW</span>
              </a>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i> Network</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_provider_type_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Network Type Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Network Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_provider_type_details.php">
                            <i class="nav-icon icon-puzzle"></i> Network Type List</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_details.php">
                            <i class="nav-icon icon-puzzle"></i> Network List</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_line_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Network Line Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_api_line_entry.php">
                            <i class="nav-icon icon-puzzle"></i> API Partner Line Entry</a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i> API Master</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_provider_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Network Type Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_provider_details.php">
                            <i class="nav-icon icon-puzzle"></i> Network Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_api_request_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Provider Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_api_result_entry.php">
                            <i class="nav-icon icon-puzzle"></i> View Provider</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_api_request_details.php">
                            <i class="nav-icon icon-puzzle"></i> API Request Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_bonrix_entry.php">
                            <i class="nav-icon icon-puzzle"></i> API Result Entry</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_api_request_details.php">
                              <i class="nav-icon icon-puzzle"></i> API Request Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_bonrix_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Bondrix Network</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_bondrixurl_entry.php">
                              <i class="nav-icon icon-puzzle"></i> Bondrix URL</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_apilink_code_details.php">
                            <i class="nav-icon icon-puzzle"></i> API Link Code Details</a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_creation.php">
                            <i class="nav-icon icon-puzzle"></i> User Creation Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_creation_edit.php">
                            <i class="nav-icon icon-puzzle"></i> Edit User</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_photoupdate.php">
                            <i class="nav-icon icon-puzzle"></i> Edit Photo</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_details.php">
                            <i class="nav-icon icon-puzzle"></i> User Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_accounts.php">
                            <i class="nav-icon icon-puzzle"></i> User Accounts</a>
                        </li>
                        
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_fund.php">
                            <i class="nav-icon icon-puzzle"></i> Self Payment Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_fund_details.php">
                            <i class="nav-icon icon-puzzle"></i> Payment Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_remote_fund.php">
                            <i class="nav-icon icon-puzzle"></i> User Remote Payment</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_amount_deduct.php">
                            <i class="nav-icon icon-puzzle"></i> User Refund Payment</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_payment.php">
                            <i class="nav-icon icon-puzzle"></i> User Payment</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_bill_payment.php">
                              <i class="nav-icon icon-puzzle"></i> Bill Payment</a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link" href="admin_money_payment.php">
                              <i class="nav-icon icon-puzzle"></i> Money Transfer</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="admin_user_payment_details.php">
                                <i class="nav-icon icon-puzzle"></i> User Payment Details</a>
                            </li>
                            
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge Details</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_recharge_details.php">
                            <i class="nav-icon icon-puzzle"></i> User Recharge Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_recharge_details_request.php">
                            <i class="nav-icon icon-puzzle"></i> User Recharge Request</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_pending_details.php">
                            <i class="nav-icon icon-puzzle"></i> Pending Report</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_recharge_details_one.php">
                            <i class="nav-icon icon-puzzle"></i> Date Recharge Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_dist_recharge_details.php">
                            <i class="nav-icon icon-puzzle"></i> Distributor Recharge</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_dist_recharge_details_date.php">
                              <i class="nav-icon icon-puzzle"></i> Date Distributor Details</a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link" href="admin_provider_recharge_details.php">
                              <i class="nav-icon icon-puzzle"></i> Provider Details</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="admin_server_reply.php">
                                <i class="nav-icon icon-puzzle"></i> Server Reply</a>
                            </li>
                    </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Others</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_offer_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Offer Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_offer_details.php">
                            <i class="nav-icon icon-puzzle"></i> Offer Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_complaint_details.php">
                            <i class="nav-icon icon-puzzle"></i> Complaint Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bank_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Bank Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bank_details.php">
                            <i class="nav-icon icon-puzzle"></i> Bank View</a>
                        </li>
                        
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Bulk SMS</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bulksms.php">
                            <i class="nav-icon icon-puzzle"></i> Entry</a>
                        </li>   
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Downloads </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_download.php">
                            <i class="nav-icon icon-puzzle"></i>Downloads </a>
                        </li>   
                    </ul>
            </li>

          </ul>
        </nav>
        
      </div>
      <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Home</li>
          <li class="breadcrumb-item">
            <a href="#">Admin</a>
          </li>
          <li class="breadcrumb-item active">Dashboard</li>
          <!-- Breadcrumb Menu-->
          <li class="breadcrumb-menu d-md-down-none">
            <div class="btn-group" role="group" aria-label="Button group">
              <a class="btn" href="#">
                <i class="icon-speech"></i>
              </a>
              <a class="btn" href="./">
                <i class="icon-graph"></i>  Dashboard</a>
              <a class="btn" href="#">
                <i class="fa fa-bell-o"></i> Balance
                <span class="badge badge-info">500.00</span>
              </a>
            </div>
          </li>
        </ol>
        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Network Type Entry</div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                          <div class ="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                              <form class="form-horizontal" action="" method="post">
                                  <div class="form-group row">
                                      <label class="col-sm-5 col-form-label" for="input-normal">Provider Id</label>
                                      <div class="col-sm-6">
                                        <input class="form-control" id="input-normal" type="text" name="input-normal" placeholder="Normal">
                                      </div>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Provider Name</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="input-normal" type="text" name="input-normal" placeholder="Normal">
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row" style="padding:6px 15px;">
                                      <button class="btn btn-sm btn-primary" type="submit">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>
                                    </div>
                                </form>
                          </div>
                          <div class ="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                              <table class="table table-responsive-sm table-bordered">
                                  <thead>
                                    <tr>
                                      <th>Username</th>
                                      <th>Date registered</th>
                                      <th>Role</th>
                                      <th>Status</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>Pompeius René</td>
                                      <td>2012/01/01</td>
                                      <td>Member</td>
                                      <td>
                                        <span class="badge badge-success">Active</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Paĉjo Jadon</td>
                                      <td>2012/02/01</td>
                                      <td>Staff</td>
                                      <td>
                                        <span class="badge badge-danger">Banned</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Micheal Mercurius</td>
                                      <td>2012/02/01</td>
                                      <td>Admin</td>
                                      <td>
                                        <span class="badge badge-secondary">Inactive</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Ganesha Dubhghall</td>
                                      <td>2012/03/01</td>
                                      <td>Member</td>
                                      <td>
                                        <span class="badge badge-warning">Pending</span>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td>Hiroto Šimun</td>
                                      <td>2012/01/21</td>
                                      <td>Staff</td>
                                      <td>
                                        <span class="badge badge-success">Active</span>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                          </div>
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    <footer class="app-footer">
      <!-- <div>
        <a href="http://www.vaibavonline.com">VaibavOnline</a>
        <span>&copy; 2018 VaibavOnline</span>
      </div> -->
      <div class="ml-auto">
        <span>Powered by</span>
        <a href="http://www.vaibavonline.com">VaibavOnline</a>
      </div>
    </footer>





        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/pace.min.js') }}"></script>
        <script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('js/coreui.min.js') }}"></script>
    </body>
</html>
