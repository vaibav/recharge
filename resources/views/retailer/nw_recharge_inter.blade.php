@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <form id="rech_form" class="form-horizontal" action="{{url('recharge_nw_store')}}" method = "POST" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_mobile" value="{{ $mob_no }}">
            <input type="hidden" name="net_code" value="{{ $net_code }}">
            <input type="hidden" name="user_amount" value="{{ $rech_amt }}">

            <?php $img_path = asset('/networkpng/'.$net_code.'.png'); ?>

            <div class="text-center mt-5">
              <div class="card-profile-image">
                <a href="javascript:;">
                  <img src="{{$img_path}}" class="rounded-circle">
                </a>
              </div>
              <br>
              <h4>{{$net_name}}</h4>
              <div class="h6 font-weight-300"></i></div>
              <div class="h6 mt-4"><i class="ni business_briefcase-24 mr-2"></i>Mobile : <b>{{$mob_no}}</b></div>
              <div><i class="ni education_hat mr-2"></i>Amount : <b>{{$rech_amt}}</b></div>
              
            </div>

            <div class="mt-5 py-5 border-top text-center">
              <div class="row justify-content-center">
                <div class="col-lg-9">
                  <button class="btn btn-icon btn-primary" type="button" id="btn_submit" name="btn_submit">
                    <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                      <span class="btn-inner--text">Recharge</span>
                  </button>
                  <button class="btn btn-icon btn-danger" type="button" id="btn_back" name="btn_back">
                    <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                      <span class="btn-inner--text">Back</span>
                  </button>
                </div>
              </div>
            </div>
        
           
           
            <br>
          </form>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-2">

       <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
          <!-- Table -->
          <div class="table-responsive">

           
                <table class="table table-striped align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Mobile</th>
                            <th scope="col" class="sort" data-sort="budget">Network</th>
                            <th scope="col" class="sort" data-sort="status">Amount</th>
                            <th scope="col" class="sort" data-sort="status">Opr Id</th>
                            <th scope="col" class="sort" data-sort="status">Up date</th>
                            <th scope="col" class="sort" data-sort="status">down date</th>
                            <th scope="col">Status</th>
                            <th scope="col" class="sort" data-sort="completion">O.Bal</th>
                            <th scope="col">C.Bal</th>
                        </tr>
                    </thead>
                    <tbody class="list">

                      <?php 
                          $str = "";

                          foreach($recharge as $f)
                          {
                              $net_name = "";
                              foreach($network as $r)
                              {
                                  if($f->net_code == $r->net_code)
                                      $net_name = $r->net_name;
                              }

                              $reply_id = "";
                              $reply_date = "";
                              $rech_status = "";
                              $o_bal = 0;
                              $z = 0;

                              $rech_status = $f->rech_status;
                              $rech_option = $f->rech_option;

                              if($f->recharge2 != null)
                              {
                                $reply_id = $f->recharge2->reply_opr_id;
                                $reply_date =$f->recharge2->reply_date;

                                
                              }

                              if($rech_status == "PENDING" && $rech_option == "0")
                              {
                                  $status = "<button class='btn btn-icon btn-primary btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-user-run'></i></span>
                                            </button>";
                                  
                                  $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                              }
                              else if($rech_status == "FAILURE" && $rech_option == "2")
                              {
                                  $status = "<button class='btn btn-icon btn-warning btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-bold-left'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                              }
                              else if($rech_status == "PENDING" && $rech_option == "2")
                              {      
                                  $status = "<button class='btn btn-icon btn-danger btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-scissors'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                                  $z = 1;
                              }
                              else if ($rech_status == "SUCCESS")
                              {
                                  $status = "<button class='btn btn-icon btn-success btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-check-bold'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                              }

                              $o_bal = number_format($o_bal, 2, '.', '');
                              $c_bal = floatval($f->user_balance);
                              $c_bal = number_format($c_bal, 2, '.', '');

                              $str = $str . "<tr><td>".$f->rech_mobile."</td>";
                              $str = $str . "<td>".$net_name."</td>";
                              $str = $str . "<td>".$f->rech_amount."---";
                              $str = $str . $f->rech_net_per. "---".number_format($f->rech_net_per_amt,2, ".", "")."---";
                              $str = $str . $f->rech_total."</td>";
                              $str = $str . "<td>".$reply_id."</td>";
                              $str = $str . "<td>".$f->rech_date."</td>";
                              $str = $str . "<td>".$reply_date."</td>";
                              $str = $str . "<td>".$status."</td>";
                              $str = $str . "<td>".$o_bal."</td>";
                              $str = $str . "<td>".$c_bal."</td></tr>";
                          }

                          

                          echo $str;

                      ?>
                        
                       

                    </tbody>
                </table>
            

          </div>

          <!-- End -->
      </div>
      <div class="col-lg-2">
         <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<?php
    if(session()->has('msg'))
    {
        $op = session('msg');
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op."', 'success'); 
            });
            </script>";
        session()->forget('msg');
    }

    
?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $('#btn_back').on('click',function(e)
    {
      e.preventDefault();
        
      window.open("<?php echo url('/'); ?>/dashboard_retailer",'_self');
        
    }); 

    $('#btn_submit').on('click',function(e)
    {
      e.preventDefault();
      startloader();
        
      $('#rech_form').attr('action', "{{url('recharge_nw_store')}}");
      $('#rech_form').submit();
        
    }); 

    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
  
   
});
</script>
@stop
@stop