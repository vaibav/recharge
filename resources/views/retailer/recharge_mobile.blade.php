<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobile Recharge Portal">
    <title>VaibavOnline</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
    <link href="{{asset('css/r_style.css')}}" rel="stylesheet">
    <link href="{{asset('css/v_tab.css')}}" rel="stylesheet">
    <link href="{{asset('css/loader.css')}}" rel="stylesheet">
 

</head>

<body>
<div class="container-fluid" id = "loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;" >
    <div class="row">
        <div class="col-md-12">
            <div class="loader" style="position: absolute; top: 50%; left: 50%;">
                <p>Loading...</p>
                <div class="loader-inner"></div>
                <div class="loader-inner"></div>
                <div class="loader-inner"></div>
            </div>
        </div>
    </div>
</div>
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <img src = "{{ asset('img/brand/logo.png') }}" style = "height:40px; width:90px;" class="responsive-img">
        <div id="close-sidebar">
          <i class="fas fa-bars"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
          <img class="img-responsive img-rounded" src="{{ asset('uploadphoto/user.jpg') }}"
            alt="User picture">
        </div>
        <div class="user-info">
          <span class="user-name">{{$user->user}}
            
          </span>
          <span class="user-role">{{$user->mode}}</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Bal : {{$user->ubal}}</span>
          </span>
        </div>
      </div>
      
      
      <div class="sidebar-menu">
          <ul>
            <li class="sidebar">
                <a href="{{url('dashboard_retailer')}}"><i class="fa fa-tachometer-alt"></i><span>Dashboard</span></a>
            </li>
      
            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-tachometer-alt"></i><span>User</span></a>
                <div class="sidebar-submenu">
                  <ul>
                      <li><a href="{{url('user_one')}}">User Details</a></li>
                      <li><a href="{{url('surplus_retailer')}}">Network Surplus Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-shopping-cart"></i><span>Payment</span></a>
                <div class="sidebar-submenu">
                  <ul>
                      <li><a href="{{url('paymentrequest_retailer')}}">Request</a></li>
                      <li><a href="{{url('stockdetails_retailer')}}">Stock Details</a></li>
                      <li><a href="{{url('report_money_verify')}}">Benificiary Verify Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="far fa-gem"></i><span>Money Transfer</span></a>
                <div class="sidebar-submenu">
                  <ul>
                    <li><a href="{{url('money_user_check')}}">Go</a></li>
                    <li><a href="{{url('ben_view')}}">Beneficiary</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-chart-line"></i><span>Report</span></a>
                <div class="sidebar-submenu">
                  <ul>
                  <li><a href="{{url('rechargedetails_retailer')}}">Recharge Details</a></li>
                    <li><a href="{{url('recharge_bill_retailer')}}">EBBill Details</a></li>
                    <li><a href="{{url('recharge_money_retailer')}}">Money Transfer Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-globe"></i><span>Collection</span></a>
                <div class="sidebar-submenu">
                  <ul>
                        <li><a href="{{url('collection_retailer')}}">Collection Details</a></li>
                  </ul>
                </div>
            </li>

            <li class="sidebar-dropdown">
                <a href="#"><i class="fa fa-bell"></i><span>Complaint</span></a>
                <div class="sidebar-submenu">
                  <ul>
                    <li><a href="{{url('complaint_retailer')}}">Complaint Entry</a></li>
                    <li><a href="{{url('complaint_view_retailer')}}">Complaint Details</a></li>
                  </ul>
                </div>
            </li>
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>
      <a href="{{url('changepassword_retailer')}}">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="{{url('logout')}}">
        <i class="fa fa-power-off"></i>
      </a>
    </div>
  </nav>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
        <div class="card ">
            
<div class="card-body " style="padding: 5px 10px;" id = "rech_all">

    <div class="row">
        <div class="col-md-4 px-2">
            <button type="button" class="btn btnx" id="a1">Prepaid</button>
            <button type="button" class="btn btnx" id="a2">Dth</button>
            <button type="button" class="btn btnx" id="a3">Postpaid</button>
            <button type="button" class="btn btnx" id="a4">EBBill</button>
        </div>
        <div class="col-md-4 px-2 pt-4">
                <?php 
                    $stx = "";
                    foreach($offer as $r) {
                        $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                    }
                ?>
            <marquee direction="left"><?php echo $stx; ?> </marquee>
        </div>
        <div class="col-md-4 px-2 text-right">
            <div class = 'row m-0'>
                <div class = class="col-md-12 m-0 pl-2 ">
                    <p class = "m-0 p-0" style = "font-size:26px;font-weight:bold;color:#6BCCF3" id = "r_mob"></p>
                </div>
            </div>
            <div class = 'row m-0'>
                <div class = class="col-md-12 m-0 pl-3">
                    <p class = "text-right m-0 p-0" style = "font-size:22px;font-weight:bold;color:pink;" id = "r_amt"></p>
                </div>
            </div>
        </div>
        
    </div>

    <br>
    <div class="card" style="border-radius: 15px;" id="rech_1">
    <form id="rech_form1" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        
        <div class="col-md-3  px-2">
            <div class="form-group">
                <label>Mobile No</label>
                <input type="text" class="form-control" placeholder="" id="id_user_mobile_1" name="user_mobile_1">
            </div>
        </div>
    
        <div class="col-md-3 px-2">
            <div class="form-group">
                <label>Network</label> 
                <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                        style="text-decoration: none;"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img id = "nt1_x" src = "{{asset('networkpng/1.png')}}" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                    <input type = "hidden" id = "net_code_1" name = "net_code_1" value="0">
                </a>
                <div class="dropdown-menu" aria-labelledby="purchase" >
                    <?php
                        foreach($network1 as $f)
                        {
                            $path = asset('networkpng/'.$f->net_code.'.png');

                            echo "<a class='dropdown-item' id = 'nt1_".$f->net_code."' href='#'>
                                    <img src = '".$path."' style = 'height:25px; width:90px;'/></a>";
                        }   
                    ?> 
                   
                  
                </div>
            </div>
        </div>
    
        <div class="col-md-2 px-2">
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control" placeholder="" id="id_user_amount_1" name="user_amount_1">
            </div>
        </div>
    
        <div class="col-md-4 px-2">

            <div class="row" style="margin-top:20px;">
                <div class="col-md-5">
                        <button type="button" class="btn btny" id="btn_submit_1">Recharge</button>
                </div>
                <div class="col-md-6 text-right" style="padding:2px 5px;">
                    <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_121">121</a>
                    <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_all">plan</a>
                </div>
                
            </div>
        </div>
      
    </div>
    </form> 
    </div>

    <!-- DTH-->
    <div class="card" style="border-radius: 15px;display:none;" id="rech_2">
            <form id="rech_form2" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                
                <div class="col-md-3  px-2">
                    <div class="form-group">
                        <label>Dth No</label>
                        <input type="text" class="form-control" placeholder="" id="id_user_mobile_2" name="user_mobile_2">
                    </div>
                </div>
            
                <div class="col-md-3 px-2">
                    <div class="form-group">
                        <label>Network</label> 
                        <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                                style="text-decoration: none;"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img id = "nt1_x" src = "{{asset('networkpng/1.png')}}" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                            <input type = "hidden" id = "net_code_2" name = "net_code_2" value="0">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="purchase" >
                        <?php
                            foreach($network3 as $f)
                            {
                                $path = asset('networkpng/'.$f->net_code.'.png');

                                echo "<a class='dropdown-item' id = 'nt2_".$f->net_code."' href='#'>
                                        <img src = '".$path."' style = 'height:25px; width:90px;'/></a>";
                            }   
                        ?> 
                        </div>
                    </div>
                </div>
            
                <div class="col-md-2 px-2">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control" placeholder="" id="id_user_amount_2" name="user_amount_2">
                    </div>
                </div>
            
                <div class="col-md-4 px-2">
        
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-5">
                                <button type="button" class="btn btny" id="btn_submit_2">Recharge</button>
                        </div>
                        <div class="col-md-6 text-right" style="padding:2px 5px;">
                            <a class="btn btn-circle" style="background: #BB8FCE;color:white;font-size:14px;" id="plan_info">info</a>
                           
                        </div>
                        
                    </div>
                </div>
              
            </div>
            </form> 
    </div>
    
    <!-- Postpaid-->
    <div class="card" style="border-radius: 15px;display: none;" id="rech_3">
    <form id="rech_form3" class="form-horizontal" action="#" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row">
        
        <div class="col-md-3  px-2">
            <div class="form-group">
                <label>Mobile No</label>
                <input type="text" class="form-control" placeholder="" id="id_user_mobile_3" name="user_mobile_3">
            </div>
        </div>
    
        <div class="col-md-3 px-2">
            <div class="form-group">
                <label>Network</label> 
                <a class="nav-link form-control dropdown-toggle" href="#" id="purchase" 
                        style="text-decoration: none;"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img id = "nt1_x" src = "{{asset('networkpng/1.png')}}" alt = "SELECT NETWORK" style = "height:25px; width:90px;"/>
                    <input type = "hidden" id = "net_code_3" name = "net_code_3" value="0">
                </a>
                <div class="dropdown-menu" aria-labelledby="purchase" >
                    <?php
                            foreach($network2 as $f)
                            {
                                if($f->net_code == "824") {
                                    $path = asset('networkpng/857.png');
                                }
                                else if($f->net_code == "259") {
                                    $path = asset('networkpng/636.png');
                                }
                                else if($f->net_code == "973") {
                                    $path = asset('networkpng/336.png');
                                }
                                else if($f->net_code == "686") {
                                    $path = asset('networkpng/781.png');
                                }
                                else if($f->net_code == "590") {
                                    $path = asset('networkpng/372.png');
                                }
                                else if($f->net_code == "972") {
                                    $path = asset('networkpng/510.png');
                                }
                                else {
                                    $path = asset('networkpng/'.$f->net_code.'.png');
                                }
                                

                                echo "<a class='dropdown-item' id = 'nt3_".$f->net_code."' href='#'>
                                        <img src = '".$path."' style = 'height:25px; width:90px;'/></a>";
                            }   
                    ?> 
                </div>
            </div>
        </div>
    
        <div class="col-md-2 px-2">
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control" placeholder="" id="id_user_amount_3" name="user_amount_3">
            </div>
        </div>
    
        <div class="col-md-4 px-2">

            <div class="row" style="margin-top:20px;">
                <div class="col-md-5">
                        <button type="button" class="btn btny" id="btn_submit_3">Recharge</button>
                </div>
                <div class="col-md-6 text-right" style="padding:2px 5px;">
                  
                </div>
                
            </div>
        </div>
        
    </div>
    </form> 
    </div>
    
   
    <!-- Reports-->
    <div class="row" style="height:410px;overflow-y: scroll;">
        <div class="col-md-4 px-2">
            <!--Report starts-->
            <?php 
                echo $recharge;
            ?>
            <!-- end-->
        </div>

        <!-- Offers 121 - Mobile Plans -->
        <div class="col-md-8 px-3" id = "mob_plan">
           
            
        </div>
    </div>
        
</form>
</div>

        </div>
    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.js"></script>
<script src="{{asset('js/style.js')}}" ></script>

<?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else  
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

<script type="text/javascript">
$(document).ready(function () {

    $('#a1').on('click',function(e) {
        e.preventDefault();
        $('#rech_1').show("slow"); $('#rech_2').hide("slow"); $('#rech_3').hide("slow"); 
    });

    $('#a2').on('click',function(e) {
        e.preventDefault();
        $('#rech_1').hide("slow"); $('#rech_2').show("slow"); $('#rech_3').hide("slow"); 
    });

    $('#a3').on('click',function(e) {
        e.preventDefault();
        $('#rech_1').hide("slow"); $('#rech_2').hide("slow"); $('#rech_3').show("slow"); 
    });

    $('#a4').on('click',function(e) {
        e.preventDefault();
        window.open("<?php echo url('/'); ?>/recharge_bill_final","_self");
    });

    $('#loader').hide();

    $("#id_user_mobile_1, #id_user_mobile_2, #id_user_mobile_3").keydown(function (e) 
    {
        numbersExactly(e);
    });
    $("#id_user_amount_1, #id_user_amount_2, #id_user_amount_3").keydown(function (e) 
    {
        numbersOnly(e);
    });

    $("#rech_all").on('click', "a",function (e) 
    {
        e.preventDefault();
        var gid = this.id;
        
        if(gid.indexOf("nt1_") !== -1)
        {
            var at = gid.split("nt1_");
            $("#net_code_1").val(at[1]);
            var url = "./networkpng/" + at[1] + ".png";
            $("#nt1_x").attr("src", url);
        }
        if(gid.indexOf("nt2_") !== -1)
        {
            var at = gid.split("nt2_");
            $("#net_code_2").val(at[1]);
            var url = "./networkpng/" + at[1] + ".jpg";
            $("#nt2_x").attr("src", url);
        }
        if(gid.indexOf("nt3_") !== -1)
        {
            var at = gid.split("nt3_");
            $("#net_code_3").val(at[1]);
            var url = "./networkpng/" + at[1] + ".png";
            $("#nt3_x").attr("src", url);
        }

        if(gid.indexOf("plan_121") !== -1)
        {
            var net_code = $('#net_code_1').val(); 
            var usr_mobe = $('#id_user_mobile_1').val();

            if(net_code != "" && usr_mobe != "")
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'recharge_offer_new/' + net_code + "/" + usr_mobe,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        $('#mob_plan').html(opr);
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }
        }
        if(gid.indexOf("plan_all") !== -1)
        {
            var net_code = $('#net_code_1').val(); 

            if(net_code != "")
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'recharge_plan_new/' + net_code,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        $('#mob_plan').html(opr);
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }
        }
        if(gid.indexOf("plan_info") !== -1)
        {
            var net_code = $('#net_code_2').val(); 
            var usr_mobe = $('#id_user_mobile_2').val();

            if(net_code != "" && usr_mobe != "")
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'recharge_dthinfo_new/' + net_code + "/" + usr_mobe,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        $('#mob_plan').html(opr);
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }
        }
        
    
    });

    $("#id_user_mobile_1").keyup(function (e) 
    {
        var mob = $("#id_user_mobile_1").val();
        if(mob.length == 10){
            //getOperatorCheck(mob);
        }
        $("#r_mob").text(mob);
    });
    $("#id_user_mobile_2").keyup(function (e) 
    {
        var mob = $("#id_user_mobile_2").val();
        $("#r_mob").text(mob);
    });
    $("#id_user_mobile_3").keyup(function (e) 
    {
        var mob = $("#id_user_mobile_3").val();
        $("#r_mob").text(mob);
    });

    $("#id_user_amount_1, #id_user_amount_2, #id_user_amount_3").keyup(function (e) 
    {
        var mob = $(this).val();
        $("#r_amt").text(mob);
    });

    function getOperatorCheck(mob)
    {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        $('#loader').show(); 

        $.ajax({
            url:'recharge_operator_new/' + mob,
            type: 'GET',
            success: function( response ){
                var opr = response;
                $('#loader').hide(); 
                if(opr != "1") {
                    $("#net_code_1").val(opr);
                    var url = "./networkpng/" + opr + ".png";
                    $("#nt1_x").attr("src", url);
                }
            },
            error: function (xhr, b, c) {
                console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
            }
        });
    }
   

    $('#btn_submit_1').on('click',function(e)
    {
        e.preventDefault();
        $('#btn_submit_1').prop('disabled', true);

        var un = $('#net_code_1').val();
        var um = $('#id_user_mobile_1').val();
        var ua = $('#id_user_amount_1').val();
        
        //alert(ax);
        if(um != "" && ua != "" && un != "" && un != null)
        {

            var url = "./networkpng/" + un + ".png";

            swal({
                title: 'Are you sure?',
                type: 'warning',
                html:  '<table class ="table table-striped">' + '<tr><th  style="padding:4px 8px;">NETWORK</th><td style="padding:4px 8px;"><img src = ' + url +' style = "height:25px; width:90px;"/></td></tr>'
                                            + '<tr><th style="padding:4px 8px;">Mobile No</th><td style="padding:4px 8px;">' + um + '</td></tr>'  
                                            + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + ua + '</td></tr></table>',
                                            
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Recharge!'
                }).then((result) => {
                if (result.value) {
                    $('#loader').show();
                    $('#rech_form1').attr('action', "{{url('recharge_store_1')}}");
                    $('#rech_form1').submit();
                }
                else
                {
                    swal("Cancelled", "No Recharge...", "error");
                    $('#btn_submit_1').prop('disabled', false);
                }
            });
            
        }
        else if(um == "")
        {
            swal("Cancelled", "Please Enter Mobile No!", "error");
            $('#btn_submit_1').prop('disabled', false);
        }
        else if(ua == "")
        {
            swal("Cancelled", "Please Enter Amount!", "error");
            $('#btn_submit_1').prop('disabled', false);
        }
        else if(un == "" || un == null)
        {
            swal("Cancelled", "Please Select Network!", "error");
            $('#btn_submit_1').prop('disabled', false);
        }
        
        
    }); 

     $('#btn_submit_2').on('click',function(e)
        {
            e.preventDefault();
            $('#btn_submit_2').prop('disabled', true);

            var un = $('#net_code_2').val();
            var um = $('#id_user_mobile_2').val();
            var ua = $('#id_user_amount_2').val();
            
            //alert(ax);
            if(um != "" && ua != "" && un != "" && un != null)
            {

                 var url = "./networkimage/" + un + ".jpg";

                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    html:  '<table class ="table table-striped">' + '<tr><th  style="padding:4px 8px;">NETWORK</th><td style="padding:4px 8px;"><img src = ' + url +' style = "height:25px; width:90px;"/></td></tr>'
                                                + '<tr><th style="padding:4px 8px;">Mobile No</th><td style="padding:4px 8px;">' + um + '</td></tr>'  
                                                + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + ua + '</td></tr></table>',
                                                
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Recharge!'
                    }).then((result) => {
                    if (result.value) {
                        $('#loader').show();
                        $('#rech_form2').attr('action', "{{url('recharge_store_2')}}");
                        $('#rech_form2').submit();
                    }
                    else
                    {
                        swal("Cancelled", "No Recharge...", "error");
                        $('#btn_submit_2').prop('disabled', false);
                    }
                });
                
            }
            else if(um == "")
            {
                swal("Cancelled", "Please Enter Mobile No!", "error");
                $('#btn_submit_2').prop('disabled', false);
            }
            else if(ua == "")
            {
                swal("Cancelled", "Please Enter Amount!", "error");
                $('#btn_submit_2').prop('disabled', false);
            }
            else if(un == "" || un == null)
            {
                swal("Cancelled", "Please Select Network!", "error");
                $('#btn_submit_2').prop('disabled', false);
            }
            
            
        }); 


        $('#btn_submit_3').on('click',function(e)
        {
            e.preventDefault();
            $('#btn_submit_3').prop('disabled', true);

            var un = $('#net_code_3').val();
            var um = $('#id_user_mobile_3').val();
            var ua = $('#id_user_amount_3').val();
            
            //alert(ax);
            if(um != "" && ua != "" && un != "" && un != null)
            {

                 var url = "./networkimage/" + un + ".jpg";

                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    html:  '<table class ="table table-striped">' + '<tr><th  style="padding:4px 8px;">NETWORK</th><td style="padding:4px 8px;"><img src = ' + url +' style = "height:25px; width:90px;"/></td></tr>'
                                                + '<tr><th style="padding:4px 8px;">Mobile No</th><td style="padding:4px 8px;">' + um + '</td></tr>'  
                                                + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + ua + '</td></tr></table>',
                                                
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Recharge!'
                    }).then((result) => {
                    if (result.value) {
                        $('#loader').show();
                        $('#rech_form3').attr('action', "{{url('recharge_store_3')}}");
                        $('#rech_form3').submit();
                    }
                    else
                    {
                        swal("Cancelled", "No Recharge...", "error");
                        $('#btn_submit_3').prop('disabled', false);
                    }
                });
                
            }
            else if(um == "")
            {
                swal("Cancelled", "Please Enter Mobile No!", "error");
                $('#btn_submit_3').prop('disabled', false);
            }
            else if(ua == "")
            {
                swal("Cancelled", "Please Enter Amount!", "error");
                $('#btn_submit_3').prop('disabled', false);
            }
            else if(un == "" || un == null)
            {
                swal("Cancelled", "Please Select Network!", "error");
                $('#btn_submit_3').prop('disabled', false);
            }
            
            
        }); 

        function numbersOnly(e)
        {
            
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
            // let it happen, don't do anything
            return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            }
        }

        function numbersExactly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
                    // let it happen, don't do anything
                    return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
});
</script>

</body>
</html>