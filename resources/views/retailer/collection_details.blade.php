<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('retailer.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Collection Details</span>
                    
                </div>

                <?php      
                    $de_amt = $user_details['de_tot'];
                    $ce_amt = $user_details['ce_tot'];
                    $pe_amt = $user_details['pe_tot'];
                    $u_name = $user_details['user_name'];
                    $c_details = $user_details['c_details'];
                ?>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l12 x12">
                            <table class="bordered striped responsive-table ">
                                <tr>
                                    <td style='font-size:12px;padding:7px 8px;'>USER NAME</td>
                                    <th style='font-size:12px;padding:7px 8px;'>{{$u_name}}</th>
                                    <td style='font-size:12px;padding:7px 8px;'></td>
                                    <th style='font-size:12px;padding:7px 8px;'></th>
                                    <td style='font-size:12px;padding:7px 8px;'></td>
                                    <th style='font-size:12px;padding:7px 8px;'></th>
                                </tr>
                                <tr>
                                    <td style='font-size:12px;padding:7px 8px;'>DEBIT(taken)</td>
                                    <th style='font-size:12px;padding:7px 8px;'>{{$de_amt}}</th>
                                    <td style='font-size:12px;padding:7px 8px;'>CREDIT(paid)</td>
                                    <th style='font-size:12px;padding:7px 8px;'>{{$ce_amt}}</th>
                                    <td style='font-size:12px;padding:7px 8px;'>BALANCE</td>
                                    <th style='font-size:12px;padding:7px 8px;'>{{$pe_amt}}</th>
                                </tr>
                                
                            </table>
                        </div>
                            
                    </div>


                     <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l12 x12">
                            <table class="bordered striped responsive-table ">
                                <thead>
                                    <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Bill No</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                        <th style='font-size:12px;padding:7px 8px;'>DD/Cheque No.</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Image</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Debit (taken)</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Credit (Paid)</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Balance</th>   
                                </tr>
                                </thead>
                                <tbody id="tbl_body" >
                                <?php 
                                    $j = 1;

                                    $debit = 0;
                                    $credit = 0;
                                    $balance = 0;
                                    $status = "";

                                    foreach($c_details as $f)
                                    {
                                                                                
                                        if($f->pay_type == "C" )
                                        {
                                            // Debit -
                                            if ($f->pay_status == "1") {
                                                $debit = floatval($debit) + floatval($f->pay_amount);
                                                $balance = floatval($balance) - floatval($f->pay_amount);
                                                $status = "SUCCESS";
                                            }
                                            else if ($f->pay_status == "2") {
                                                $status = "FAILURE";
                                            }
                                            else if ($f->pay_status == "0") {
                                                $status = "PENDING";
                                            }

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->bill_no."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_option."</td>";

                                            if($f->pay_image != "-") {
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadbankreceipt/".$f->pay_image."' style='height:120px;width:120px;' alt='NO PHOTO'/>";
                                                echo "</td>";
                                            }
                                            else
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                            }

                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$status."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".abs($balance)."</td>";
                                            echo "</tr>";
                                            
                                        }
                                        else if($f->pay_type == "D" )
                                        {
                                            // Credit +
                                            if ($f->pay_status == "1") {
                                                $credit = floatval($credit) + floatval($f->pay_amount);
                                                $balance = floatval($balance) + floatval($f->pay_amount);
                                                $status = "SUCCESS";
                                            }
                                            else if ($f->pay_status == "2") {
                                                $status = "FAILURE";
                                            }
                                            else if ($f->pay_status == "0") {
                                                $status = "PENDING";
                                            }
                                            
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->bill_no."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_option."</td>";

                                            if($f->pay_image != "-") {
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadbankreceipt/".$f->pay_image."' style='height:120px;width:120px;' alt='NO PHOTO'/>";
                                                echo "</td>";
                                            }
                                            else
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                            }

                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$status."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".abs($balance)."</td>";
                                            echo "</tr>";
                                        }
                                    
                                        $j++;
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                            
                    </div>
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
  
      });
    </script>
    </body>
</html>
