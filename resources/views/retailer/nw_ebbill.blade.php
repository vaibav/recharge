@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <form id="rech_form" class="form-horizontal" action="{{url('ebbill_nw_1')}}" method = "POST" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="nav-wrapper px-3">
                  <ul class="nav nav-pills nav-pills-circle" id="tabs_2" role="tablist">
                    <li class="nav-item text-center">
                      <a class="nav-link rounded-circle active" id="eb-tab" >
                        <span class="nav-link-icon d-block"><i class="ni ni-sound-wave"></i></span>
                      </a>
                      <span style="font-size:13px; color: orange;font-weight: 600">EBBill</span>
                    </li>
                    <li class="nav-item text-center">
                      <a class="nav-link" id="rech-tab" >
                        <span class="nav-link-icon d-block"><i class="ni ni-mobile-button"></i></span>
                      </a>
                      <span style="font-size:13px; color: orange;font-weight: 600">Recharge</span>
                    </li>
                    
                  </ul>
            </div>
            <div class="tab-content px-3" >
                <div class="tab-pane fade show active" id="tabs_2_1"role="tabpanel" aria-labelledby="tabs-icons-text-1-tab" >
                    <br>
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <select class="form-control form-control-alternative selectpicker" data-style="btn btn-link" id="id_net_code" name="net_code" style="padding-top: 0px;padding-bottom: 0px;">
                            </select>
                        </div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control form-control-alternative" placeholder="Consumer No" id="id_eb_cons_no" name="eb_cons_no">
                        </div>
                      </div>

                      <div class="col-12 col-sm-12 col-md-4">
                        <div class="form-group">
                            <button class="btn btn-warning btn-round" id = "btn_info" style="font-size: 12px;padding:2px 2px !important;">info</button>
                        </div>
                      </div>

                    </div>
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-4">
                         <div class="form-group">
                            <input type="text" class="form-control form-control-alternative" placeholder="Amount" id="id_eb_cons_amount" name="eb_cons_amount">
        
                        </div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-4">
                         <div class="form-group">
                            <input type="text" class="form-control form-control-alternative" placeholder="Name" id="id_eb_cons_name" name="eb_cons_name">
        
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12 col-sm-12 col-md-4">
                         <div class="form-group">
                            <input type="text" class="form-control form-control-alternative" placeholder="Mobile No" id="id_eb_cons_mobile" name="eb_cons_mobile">
        
                        </div>
                      </div>
                      <div class="col-12 col-sm-12 col-md-4">
                         <div class="form-group">
                            <input type="text" class="form-control form-control-alternative" placeholder="Due Date" id="id_eb_cons_duedate" name="eb_cons_duedate">
        
                        </div>
                      </div>
                    </div>
                </div>
               
            </div>
        
            <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-3" style="padding-left: 35px;">
                  <button class="btn btn-icon btn-primary" type="button" id="btn_submit" name="btn_submit">
                    <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                      <span class="btn-inner--text">Next</span>
                  </button>
                </div>
            </div>
            <br>
          </form>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-2">

       <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
          <!-- Table -->
          <div class="table-responsive">

           
                <table class="table table-striped align-items-center">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="sort" data-sort="name">Mobile</th>
                            <th scope="col" class="sort" data-sort="budget">Network</th>
                            <th scope="col" class="sort" data-sort="status">Amount</th>
                            <th scope="col" class="sort" data-sort="status">Opr Id</th>
                            <th scope="col" class="sort" data-sort="status">Up date</th>
                            <th scope="col" class="sort" data-sort="status">down date</th>
                            <th scope="col">Status</th>
                            <th scope="col" class="sort" data-sort="completion">O.Bal</th>
                            <th scope="col">C.Bal</th>
                        </tr>
                    </thead>
                    <tbody class="list">

                      <?php 
                          $str = "";
                          $j = 1;
                          foreach($ebdetail as $f)
                          {
                              $net_name = "";
                              foreach($network as $r)
                              {
                                  if($f->net_code == $r->net_code)
                                      $net_name = $r->net_name;
                              }

                              $rech_status = "";
                              $o_bal = 0;
                              $z = 0;

                              $rech_status = $f->con_status;
                              $rech_option = $f->con_option;
                              $reply_id = $f->reply_opr_id;
                              $reply_date =$f->reply_date;
                              $status = "";

                              if($rech_status == "PENDING" && $rech_option == "1")
                              {
                                  $status = "<button class='btn btn-icon btn-primary btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-user-run'></i></span>
                                            </button>";
                                  
                                  $o_bal = floatval($f->user_balance) + floatval($f->con_total);
                              }
                              else if($rech_status == "FAILURE" && $rech_option == "2")
                              {
                                  $status = "<button class='btn btn-icon btn-warning btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-bold-left'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) + floatval($f->con_total);
                              }
                              else if($rech_status == "PENDING" && $rech_option == "2")
                              {      
                                  $status = "<button class='btn btn-icon btn-danger btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-scissors'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) - floatval($f->con_total);
                                  $z = 1;
                              }
                              else if ($rech_status == "SUCCESS")
                              {
                                  $status = "<button class='btn btn-icon btn-success btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-check-bold'></i></span>
                                            </button>";
                                 
                                  $o_bal = floatval($f->user_balance) + floatval($f->con_total);
                              }

                              $o_bal = number_format($o_bal, 2, '.', '');
                              $c_bal = floatval($f->user_balance);
                              $c_bal = number_format($c_bal, 2, '.', '');

                              $str = $str . "<tr><td>".$f->con_acc_no."</td>";
                              $str = $str . "<td>".$net_name."</td>";
                              $str = $str . "<td>".$f->con_amount."---";
                              $str = $str . $f->con_net_per. "(%)---".number_format($f->con_net_per_amt,2, ".", "")."---";
                              $str = $str . number_format($f->con_net_surp,2, ".", "")."---";
                              $str = $str . $f->con_total."</td>";
                              $str = $str . "<td>".$reply_id."</td>";
                              $str = $str . "<td>".$f->created_at."</td>";
                              $str = $str . "<td>".$reply_date."</td>";
                              $str = $str . "<td>".$status."</td>";
                              $str = $str . "<td>".$o_bal."</td>";
                              $str = $str . "<td>".$c_bal."</td></tr>";

                              
                          }

                          

                          echo $str;

                      ?>
                        
                       

                    </tbody>
                </table>
            

          </div>

          <!-- End -->
      </div>
      <div class="col-lg-2">
         <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
    <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content bg-gradient-danger">
          
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-notification"></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="modal-body" id = "model-body">
              
               
                
            </div>
            
            <div class="modal-footer">
               
                <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
            
          

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    getEB();

    function getEB()
    {
      $('#id_net_code').empty();

      var passedArray =  <?php echo json_encode($eb); ?>; 
             
      for(var i = 0; i < passedArray.length; i++){ 
          var d = passedArray[i];
          var newOpt = $('<option>').attr('value',d['net_code']).text(d['net_name']);
          $('#id_net_code').append(newOpt);
      } 
      
    }

    $('#eb-tab').on('click',function(e)
    {
      $(this).addClass('active');
      $('#rech-tab').removeClass('active');
       getEB();
    });

    $('#rech-tab').on('click',function(e)
    {
      window.open("<?php echo url('/'); ?>/dashboard_retailer",'_self');
    });

    


    $('#btn_submit').on('click',function(e)
    {
      e.preventDefault();

      z= checkData();

      if(z[0] == 0)
      {
        $('#rech_form').attr('action', "{{url('ebbill_nw_1')}}");
        $('#rech_form').submit();
      }
      else {
          swal("Cancelled", z[1], "error");
      }
        
        
    }); 

    $('#btn_info').on('click',function(e)
    {
      e.preventDefault();

      var net_code1 = $("#id_net_code").val();
      var usr_mobe  = $("#id_eb_cons_no").val();
      var ath = $('#auth_token').val();

      if(net_code1 == null || net_code1 =="")
      {
        swal("Cancelled", 'Please Select Network!', "error");
      }
      else if(usr_mobe == "")
      {
        swal("Cancelled", 'Please Enter Mobile No!', "error");
      }
      else
      {
          startloader();
          $.ajax({
              url: '<?php echo url('/'); ?>/rech_eb',
              type: 'GET',
              data: {net_code:net_code1, con_no:usr_mobe},
              success: function (data) { 
                  var z = data.split("*");
                  $('#model-body').html(z[3]);
                  $('#id_eb_cons_name').val(z[1]);
                  $('#id_eb_cons_amount').val(z[0]);
                  $('#modal-notification').modal('show');
                  stoploader();
                  
              },
              error: function(data, mXg) { 
                  $('#loader').hide();
                  stoploader();
              }
          }); 
      }

      
        
    }); 

  $("#id_eb_cons_no, #id_eb_cons_amount, #id_eb_cons_mobile").keydown(function (e) 
  {
      numbersOnly(e);
  });

  function checkData()
  {
      var z = [];
      z[0] = 0;
      z[1] = "Success";

    
      if($("#id_eb_cons_no").val() == "" ) {
          z[0] = 1;
          z[1] = "Consumer No is Empty";
      }

      if($("#id_eb_cons_amount").val() == "" ) {
          z[0] = 1;
          z[1] = "Consumer Amount is Empty";
      }

      if($("#id_eb_cons_mobile").val() == "" ) {
          z[0] = 1;
          z[1] = "Consumer Mobile is Empty";
      }

      if($("#id_net_code").val() == "" ) {
          z[0] = 1;
          z[1] = "Network is Empty";
      }

      return z;
  }


    function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
        // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
        // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
          // let it happen, don't do anything
          return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }

    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
   
});
</script>
@stop
@stop


