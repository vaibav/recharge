<!-- BEGIN: Header-->
<header class="page-topbar " id="header">
    <div class="navbar navbar-fixed"> 
    <nav class="navbar-main title-pink nav-collapsible sideNav-lock navbar-dark  ">
        <div class="nav-wrapper">
        
        <ul class="navbar-list right">
             <li><a  href="#">RETAILER</a></li>
            <li><a  href="#">{{$bal}}</a></li>
            <li><a  href="#">Hi {{$uname}}!</a></li>
            <li><a class="dropdown-trigger profile-button" href="javascript:void(0);" 
                data-target="profile-dropdown"><i class="material-icons">more_vert</i></a></li>
        </ul>
        
        <!-- profile-dropdown-->
        <ul class="dropdown-content" id="profile-dropdown" style="width:350px;">
            <li><a class="grey-text text-darken-1" href="#">{{$uname}}</a></li>
            <li><a class="grey-text text-darken-1" href="#">{{$bal}}</a></li>
            <li class="divider"></li>
            <li><a class="grey-text text-darken-1" href="{{url('changepassword_retailer')}}"><i class="material-icons">lock_outline</i> Change Pwd</a></li>
            <li><a class="grey-text text-darken-1" href="{{url('logout')}}"><i class="material-icons">keyboard_tab</i> Logout</a></li>
        </ul>
        </div>
        
    </nav>
    </div>
</header>
<!-- END: Header-->

<!-- BEGIN: SideNav-->
 <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
    <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="#"><img src="{{ asset('img/brand/logo.png') }}" alt="materialize logo"/></a>
        <a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
    <li class="{{$act1[0]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
        <i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">Dashboard</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li class="active"><a class="collapsible-body {{$act[0]}}" href="{{url('dashboard_retailer')}}" data-i18n="">
            <i class="material-icons">radio_button_unchecked</i><span>Recharge</span></a>
            </li>
            <li><a class="collapsible-body {{$act[1]}}" href="{{url('recharge_bill_final')}}" data-i18n="">
            <i class="material-icons">radio_button_unchecked</i><span>EBBill</span></a>
            </li>
            <li><a class="collapsible-body {{$act[2]}}" href="{{url('money_user_check')}}" data-i18n="">
            <i class="material-icons">radio_button_unchecked</i><span>Money Transfer</span></a>
            </li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[1]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">cloud</i><span class="menu-title" data-i18n="">User</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('user_one')}}" class="waves-effect waves-cyan {{$act[3]}}"  data-i18n="">User Details</a></li>
            <li><a href="{{url('surplus_retailer')}}" class="waves-effect waves-cyan {{$act[4]}}"  data-i18n="">Network Surplus Details</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[2]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">account_balance</i><span class="menu-title" data-i18n="">Payment</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('paymentrequest_retailer')}}" class="waves-effect waves-cyan {{$act[5]}}"  data-i18n="">Request</a></li>
            <li><a href="{{url('stockdetails_retailer')}}" class="waves-effect waves-cyan {{$act[6]}}"  data-i18n="">Stock Details</a></li>
            <li><a href="{{url('report_money_verify')}}" class="waves-effect waves-cyan {{$act[7]}}"  data-i18n="">Benificiary Verify Details</a></li>
        </ul>
        </div>
    </li>

    <li class=" {{$act1[3]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">money</i><span class="menu-title" data-i18n="">Money Transfer</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('money_user_check')}}" class="waves-effect waves-cyan {{$act[8]}}"  data-i18n="">Go</a></li>
            <li><a href="{{url('ben_view')}}" class="waves-effect waves-cyan {{$act[9]}}"  data-i18n="">Beneficiary</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[4]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">timeline</i><span class="menu-title" data-i18n="">Report</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('rechargedetails_retailer')}}" class="waves-effect waves-cyan {{$act[10]}}"  data-i18n="">Recharge Details</a></li>
            <li><a href="{{url('recharge_bill_retailer')}}" class="waves-effect waves-cyan {{$act[11]}}"  data-i18n="">EBBill Details</a></li>
            <li><a href="{{url('recharge_money_retailer')}}" class="waves-effect waves-cyan {{$act[12]}}"  data-i18n="">Money Transfer Details</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[5]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">add_alert</i><span class="menu-title" data-i18n="">Complaint</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('complaint_retailer')}}" class="waves-effect waves-cyan {{$act[13]}}"  data-i18n="">Complaint Entry</a></li>
            <li><a href="{{url('complaint_view_retailer')}}" class="waves-effect waves-cyan {{$act[14]}}"  data-i18n="">Complaint Details</a></li>
        </ul>
        </div>
    </li>

    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->