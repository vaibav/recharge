<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <title>VaibavOnline</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/material-kit/2.0.6/css/material-kit.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="{{asset('css/chat.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

</head>

<body>
<!-- loader-->
<div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
    <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
            <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar  navbar-expand-lg bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="#" style="padding:5px 10px;padding-left: 0px;">
            <img src="{{ asset('img/brand/logo.png') }}" class="img-responsive" style="height:45px;width:130px;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse " id="navbarNavDropdown">
        <ul class="navbar-nav justify-content-end">
            <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="m_user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    User
                </a>
                <div class="dropdown-menu" aria-labelledby="m_user">
                    <a class="dropdown-item" href="{{url('user_one')}}">User Details</a>
                    <a class="dropdown-item" href="{{url('surplus_retailer')}}">Network Surplus Details</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="m_pay" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Payment
                </a>
                <div class="dropdown-menu" aria-labelledby="m_pay">
                    <a class="dropdown-item" href="{{url('paymentrequest_retailer')}}">Payment Request</a>
                    <a class="dropdown-item" href="{{url('stockdetails_retailer')}}">Stock Details</a>
                    <a class="dropdown-item" href="{{url('report_money_verify')}}">Benificiary Verify Details</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="m_money" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Money Transfer
                </a>
                <div class="dropdown-menu" aria-labelledby="m_money">
                    <a class="dropdown-item" href="{{url('money_user_check')}}">Go</a>
                    <a class="dropdown-item" href="{{url('ben_view')}}">Beneficiary</a>
                    <a class="dropdown-item" href="{{url('recharge_money_retailer')}}">Money Transfer Details</a>
                </div>
            </li>  
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="m_report" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Report
                </a>
                <div class="dropdown-menu" aria-labelledby="m_report">
                    <a class="dropdown-item" href="{{url('rechargedetails_retailer')}}">Recharge Details</a>
                    <a class="dropdown-item" href="{{url('recharge_bill_retailer')}}">EBBill Details</a>
                </div>
            </li>  
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="m_comp" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Complaint
                </a>
                <div class="dropdown-menu" aria-labelledby="m_comp">
                    <a class="dropdown-item" href="{{url('complaint_retailer')}}">Complaint Entry</a>
                    <a class="dropdown-item" href="{{url('complaint_view_retailer')}}">Complaint Details</a>
                    
                </div>
            </li>
        </ul>

        <form class="form-inline ml-auto">
            <div class="form-group no-border">
                <label style="color:white;">Hi {{$user->user}} |</label><br>
                <label style="color:white;">&nbsp;Bal:{{$user->ubal}} &nbsp;</label>
            </div>
            <a  class="btn btn-sm btn-white btn-just-icon btn-round" href = "{{url('logout')}}">
                    <i class="tiny material-icons">logout</i>
            </a>
        </form>
        </div>
    </div>
</nav>
<div style="height:5px;"></div>   

<div class="container-fluid">
        <div class="card text-center" style="margin:10px 5px;margin-top: 30px">
            <div class="card-header card-header-rose" style="padding:10px 10px;">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4">
                        <ul class="nav nav-tabs">
                            <li class="nav-item"><a class="nav-link " href="#0" id="re_pre">Prepaid</a></li>
                            <li class="nav-item"><a class="nav-link" href="#0" id="re_dth">Dth</a></li>
                            <li class="nav-item"><a class="nav-link" href="#0" id="re_pos">Postpaid</a></li>
                            <li class="nav-item"><a class="nav-link" href="#0" id="re_eb">EB Bill</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3">
                        <?php 
                            $stx = "";
                            foreach($offer as $r)
                            {
                                $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                            }
                        ?>
                        <marquee direction = "left"><?php echo $stx; ?></marquee>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 text-right">
                        <label style="color:white;font-size:25px;" id = "r_mob"></label>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 text-right">
                        <label style="color:white;font-size:25px;" id = "r_amt"></label>
                    </div>
                </div>
               
            </div>
            <div class="card-body">
                <form id="rech_form" action="{{url('recharge_store')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">
                    <input type = "hidden" id = "id_ebb" value ='0'>
            
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <label for="id_user_mobile">Enter Mobile No</label>
                                <input type="text" class="form-control" id="id_user_mobile" name="user_mobile" aria-describedby="emailHelp" placeholder="">
                            </div>

                              <select id="id_net_type_code" name="net_type_code" style = "display:none;">
                                <?php
                                    foreach($networktype as $f)
                                    {
                                        echo "<option value='".$f->net_type_code."'>".$f->net_type_name."</option>";
                                    }
                                ?>
                            </select>

                        </div>
                      

                        <div class="col-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <label for="id_net_code">Select Network</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" id="id_net_code" name="net_code" style="padding-top: 0px;padding-bottom: 0px;">
                                    <?php
                                        foreach($network as $f)
                                        {
                                            echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3">
                            <div class="form-group">
                                <label for="id_user_mobile">Enter Amount</label>
                                <input type="text" class="form-control" id="id_user_amount" name="user_amount" aria-describedby="emailHelp" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3">
                            <button type="submit" class="btn btn-primary" id="btn_submit" name="btn_submit">Submit</button>
                            <button class="btn btn-primary btn-fab  btn-round" style = "font-size: 12px;" id="plan_121">
                                121
                            </button>
                            <button class="btn btn-primary btn-fab  btn-round" style = "font-size: 12px;" id="plan_mob">
                                Plan
                            </button>
                        </div>
                    </div>

                        
                        
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-sm-12 col-md-4" id="style-1">
                <!-- Report-->
                <div id="tbl_body">
                </div>

                <!-- End -->
            </div>
            <div class="col-12 col-sm-12 col-md-8" id="rech_offer_body">
               <!-- Plans -->
               <!-- Nav tabs -->
               
               <!-- End Plan-->
            </div>
        </div>
   </div>




</div>
<!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
            
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>

<!-- Chat box--> 
<div class="fabs">
      <div class="chat">
          <div class="chat_header">
            <div class="chat_option">
                <span id="chat_head" style="padding-left:10px;">VaibavOnline</span> <br> <span class="agent" style="padding-left:10px;">Chat Box</span> 
            </div>
          </div>
         
          <div id="chat_converse" class="chat_conversion chat_converse">
                  
            <span class="chat_msg_item chat_msg_item_admin">
                  <div class="chat_avatar">
                     <img src="{{asset('img/avatars/1.jpg')}}"/>
                  </div>Hey there! Any question?</span>
            <span class="chat_msg_item chat_msg_item_user">
                  Hello!</span>
                  <span class="status">20m ago</span>
            <span class="chat_msg_item chat_msg_item_admin">
                  <div class="chat_avatar">
                     <img src="{{asset('img/avatars/1.jpg')}}"/>
                  </div>Hey! Would you like to talk sales, support, or anyone?</span>
            <span class="chat_msg_item chat_msg_item_user">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
                   <span class="status2">Just now. Not seen yet</span>
          </div>
          
         
          <div class="fab_field">
            <a id="fab_send" class="fab"><i class="zmdi zmdi-mail-send"></i></a>
            <textarea id="chatSend" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>
          </div>
        </div>
        <a id="prime" class="fab"><i class="prime zmdi zmdi-comment-outline"></i></a>
    </div>

<!-- End chatbox --> 

<!-- page-wrapper -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/material-kit/2.0.6/js/material-kit.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js"></script>

<?php
    $net_code = 0;
    $mob = 0;
    $net_type = 0;
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 11)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 30)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 31)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 20)
        {
            $net_code = $op1['net_code'];
            $net_type = $op1['net_type'];
            $mob = $op1['mob'];

            echo "<script>
            $(document).ready(function() 
            {
                $('#rech_offer_body').html('".$res."');
                $('.tabs').tabs();
                //$('.modal').modal();
                //$('.modal').modal('open');

                $('#id_ebb').val('2');
                
                $('#id_net_type_code').val(".$net_type.");
                $('#id_net_code').val(".$net_code.");
                $('#id_user_mobile').val(".$mob.");
                //$('#id_user_mobile').addClass('active');
                $('#r_mob').text('".$mob."');

                $(document).on('click', 'a', function(e){
                    
                    var amt = $(this).text();
                    var id = this.id;
                    if(id == 'id_off')
                    {
                        $('#id_user_amount').val(amt);
                        $('#r_amt').text(amt);
                    }
                });

                $('input[name=group1]').change(function (e) {
                    var amt = $(this).val();
                    $('#id_user_amount').val(amt);
                    //$('#id_user_amount').addClass('active');
                    $('#id_amt').text(amt);
                    M.updateTextFields();
                });

                M.updateTextFields();

            });
            </script>";
        }
        else if($op == 22)
        {
            

            echo "<script>
            $(document).ready(function() 
            {
                $('#rech_offer_body').html('".$res."');
                
                $(document).on('click', 'a', function(e){
                    
                    var amt = $(this).text();
                    var id = this.id;
                    if(id == 'id_off')
                    {
                        $('#id_user_amount').val(amt);
                    }
                });
                
                M.updateTextFields();

            });
            </script>";
        }
        else if($op == 23)
        {
            $net_code = $op1['net_code'];
            $net_type = $op1['net_type'];
            $mob = $op1['mob'];

            if($res != "")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    //alert('".$res."');
                    var d = 1;
                    $('#id_user_mobile').val(".$mob.");
                    $('#r_mob').text('".$mob."');
                    

                    $('#id_net_code option').each(function() {
                        if($(this).text() == '" . $res . "') {
                            $(this).attr('selected', 'selected');  
                            d = 2;          
                        }                        
                    });

                    if(d == 1)
                    {
                        $('#id_net_code option:contains(" . $res . ")').attr('selected', 'selected');
                    }
                    
                    M.updateTextFields();

                    

                });
                </script>";
            }
            
        }
        
        session()->forget('result');
    }
?>


<script>
    $(document).ready(function() 
    {
        // chat box
        $('#chat_converse').css('display', 'block');

        $('#prime').click(function() {
        toggleFab();
        });


        //Toggle chat and links
        function toggleFab() {
        $('.prime').toggleClass('zmdi-comment-outline');
        $('.prime').toggleClass('zmdi-close');
        $('.prime').toggleClass('is-active');
        $('.prime').toggleClass('is-visible');
        $('#prime').toggleClass('is-float');
        $('.chat').toggleClass('is-visible');
        $('.fab').toggleClass('is-visible');
        
        }
        

        $('#id_net_type_code, #id_net_code').on('contentChanged', function() {
            $(this).material_select();
        });

        var c = 1;
        var opt = 1;
        $('#loader').hide();
        load_data();
        loadPrepaid();
        
        window.setInterval(function(){
            /// call your function here
            //load_data();
        }, 19000);

        

        function load_data()
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            jQuery.ajax({
                url:'userresult_data',
                type: 'GET',
                success: function( data ){

                    $('#tbl_body').html(data);
                    var d = new Date();
                    $('#tim').html(d.toLocaleString());
                    if(c == 1)
                    {
                        $('#tim').css("background-color", "#EC7063");
                        c = 2;
                    }
                    else if(c == 2)
                    {
                        $('#tim').css("background-color", "#27AE60");
                        c = 1;
                    }
                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function loadPrepaid()
        {
            var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
            getNetworkData(ty);
        }

        $('#re_pre').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
            getNetworkData(ty);
            $("#plan_121").html('121');
            $("#plan_mob").show();
            opt = 1;
        });

        $('#re_dth').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('DTH')").attr('selected', true).val();
            getNetworkData(ty);
            $("#plan_121").html('Info');
            $("#plan_mob").hide();
            opt = 2;
        });

        $('#re_pos').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('POSTPAID')").attr('selected', true).val();
            getNetworkData(ty);
            opt = 3;
        });

            $('#re_eb').on('click',function(e)
        {
            e.preventDefault();
            window.open("<?php echo url('/'); ?>/recharge_bill_final","_self");
        });

        $("#id_net_type_code").change(function(e)
        {
            var ty = $('option:selected', this).val();
            var ax = $('option:selected', this).text();
            $('#loader').show();
            if(ty != "-")
            {
                getNetworkData(ty);
                if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
                {
                    $('#rc1').show();
                    $('#eb1').hide();
                    $('#eb2').hide();
                    $('#eb3').hide();

                    if(ax == "PREPAID")
                        $("#plan_121").html('121');
                    else if(ax == "DTH")
                        $("#plan_121").html('Info');
                }
                
                

            }
                
        });

        function getNetworkData(net_type)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $.ajax({
                url:'network_data/' + net_type,
                type: 'GET',
                dataType: "json",
                success: function( response ){
                    var zz=response['data'];
                    $('#id_net_code').empty();
                    for(var i=0;i<zz.length;i++)
                    {
                        if(zz[i][0]!='')
                        {
                            var newOpt = $('<option>').attr('value',zz[i][0]).text(zz[i][1]);
                            $('#id_net_code').append(newOpt);
                        }
                    }
                   
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                }
            });
        }

        $("#id_user_mobile, #id_eb_cons_mobile").keydown(function (e) 
        {
            numbersExactly(e);
        });
        $("#id_user_amount, #id_eb_cons_amount").keydown(function (e) 
        {
            numbersOnly(e);
        });

        $("#id_user_mobile").keyup(function (e) 
        {
            var mob = $("#id_user_mobile").val();
            if(opt == 1)
            {
                if(mob.length == 10)
                {
                    //alert("valid mobile no");
                    /*$('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_mobile_check')}}");
                    $('#rech_form').submit();*/
                    getOperatorCheck(mob)
                }
            }
            $("#r_mob").text(mob);
        });

        $("#id_user_amount").keyup(function (e) 
        {
            var mob = $("#id_user_amount").val();
            
            $("#r_amt").text(mob);
        });

                    
        function getOperatorCheck(mob)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $('#loader').show(); 

            $.ajax({
                url:'rech_operator?user_mobile=' + mob,
                type: 'GET',
                success: function( response ){
                    var opr = response;
                    //alert(opr);
                    var d = 1;
                    if(opr != "" && opr != "0")
                    {
                        
                        $('#id_net_code option').each(function() {
                            if($(this).text() == opr) {
                                $(this).attr('selected', 'selected');  
                                d = 2;          
                            }                        
                        });

                        if(d == 1)
                        {
                            if(hasWhiteSpace(opr))
                            {
                                var opr1 = opr.split(' ');
                                $('#id_net_code option').each(function() {
                                    if($(this).text().indexOf(opr1[0]) !== -1) {
                                        $(this).attr('selected', 'selected');  
                                        d = 2;  
                                            
                                    }                        
                                });
                            }
                        }
                        
                        
                    }
                
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                    $('#loader').hide(); 
                }
            });
        }


        
            

        function hasWhiteSpace(s) {
            return s.indexOf(' ') >= 0;
        }


        $('#btn_submit').on('click',function(e)
        {
            e.preventDefault();
            
            $('#btn_submit').prop('disabled', true);
            var net = $('option:selected', '#id_net_code').val();
            var net1 = $('option:selected', '#id_net_code').text();
            var mob = $('#id_user_mobile').val();
            var amt = $('#id_user_amount').val();
            
            if(net != "" && mob != "" && amt != "")
            {
                var form = $(this).parents('form');
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">NETWORK</th><td style="padding:4px 8px;">' + net1 + '</td></tr>'
                                                + '<tr><th style="padding:4px 8px;">Mobile No</th><td style="padding:4px 8px;">' + mob + '</td></tr>'  
                                                + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + amt + '</td></tr></table>',
                                                
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Recharge!'
                    }).then((result) => {
                    if (result.value) {
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_store')}}");
                        $('#rech_form').submit();
                    }
                    else
                    {
                        swal("Cancelled", "No Recharge...", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                });
                
            }
            else if(mob == "")
            {
                swal("Cancelled", "Mobile No is Empty...", "error");
                $('#btn_submit').prop('disabled', false);
            }
            else if(amt == "")
            {
                swal("Cancelled", "Amount is Empty...", "error");
                $('#btn_submit').prop('disabled', false);
            }

            
            
            
        }); 

        $('#plan_121').on('click',function(e)
        {
            e.preventDefault();
            var ax = $('option:selected', '#id_net_type_code').text();
            if(ax == "PREPAID")
            {
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_user_mobile').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    $('#plan_121').prop('disabled', true);
                   /* $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers1')}}");
                    $('#rech_form').submit();*/

                    $('#loader').show();
                    $.ajax({
                        url:'rech_offer/?net_code=' + net_code +"&user_mobile=" +usr_mobe,
                        type: 'GET',
                        success: function( response ){
                           
                            $('#rech_offer_body').html(response);
                            $('#loader').hide(); 
                            $('#plan_121').prop('disabled', false);
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            $('#loader').hide();
                            $('#plan_121').prop('disabled', false);
                        }
                    });
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }
            }
            else if(ax == "DTH")
            {
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_user_mobile').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    /*$('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('dth_offers')}}");
                    $('#rech_form').submit();*/
                    getDthInfo(usr_mobe, net_code);
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }
            }
            
        });

        $('#plan_mob').on('click',function(e)
        {
            e.preventDefault();
            var ax = $('option:selected', '#id_net_type_code').text();
            if(ax == "PREPAID")
            {
                var net_code = $('#id_net_code').val();
                //alert(net_code);
                if(net_code != "" )
                {
                    $('#loader').show();
                    $.ajax({
                        url:'rech_plan?net_code=' + net_code,
                        type: 'GET',
                        success: function( response ){
                           
                            $('#rech_offer_body').html(response);
                            $('#loader').hide(); 
                            $('.tabs').tabs();
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            $('#loader').hide();
                        }
                    });
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
            }
            
            
        });

        function getDthInfo(mob, network)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $('#loader').show(); 

            $.ajax({
                url:'rech_dth/?net_code=' + network +"&user_mobile=" +mob,
                type: 'GET',
                success: function( response ){
                    $('#rech_offer_body').html(response);
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                    $('#loader').hide(); 
                }
            });
        }

        
        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("bill1_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Get Bill!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                
                }

                var nid1=gid.split("bill2_");
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Get Bill!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                
                }

        });
            
        function numbersOnly(e)
        {
            
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
            // let it happen, don't do anything
            return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            }
        }

        function numbersExactly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
                    // let it happen, don't do anything
                    return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
    });
</script>
</body>
</html>