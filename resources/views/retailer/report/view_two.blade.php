@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <div class="container">
              <div class="row" >
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-danger" style="color:white;">
                    <h5>Recharge</h5>
                    <div class="row" >
                      
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;">Success Amount </label><br>
                          <label class="title-con" style="font-size:14px;">Success Credit </label><br>
                          <label class="title-con" style="font-size:14px;">Failure Amount </label><br>
                          <label class="title-con" style="font-size:14px;">Failure Credit </label><br>
                          <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Total Credit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                       
                      </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-primary" style="color:white;">
                    <h5>Bill Payment</h5>
                    <div class="row" >
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;">Success Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Success Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                          <label class="title-con" style="font-size:14px;">TotalCredit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                        
                      </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-warning" style="color:white;">
                    <h5>Money Transfer</h5>
                    <div class="row" >
                      <div class="col-12 col-sm-12 col-md-6">
                            <label class="title-con" style="font-size:14px;">Success Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Success Credit</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Credit</label><br>
                            <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Total Credit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                        
                      </div>
                    </div>
                </div>
              </div>
          </div>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 90" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 90 0 90"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      
      <div class="col-lg-12">
          <!-- Table -->
          <div class="table-responsive">

           
                <table class="table table-striped align-items-center">
                    <thead class="thead-light">
                        <tr>
                          <th style="font-size:12px;padding:7px 8px;">NO</th>
                          <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                          <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                          <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                          <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                          <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL 1</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL 2</th>
                        </tr>
                    </thead>
                    <tbody class="list" id = "tbl_body">

                      <?php 
                                         
                        $i = 1;  
                        $str = ""; 
                        foreach($rechargeDetails as $item)
                        {
                            
                            //network
                            $netName = "";
                            foreach($networkDetails as $net)
                            {
                                if($item->net_code == $net->net_code){
                                    $netName = $net->net_name;
                                    break;
                                }
                            }

                            //percentage amount
                            $netPer    = floatval($item->ret_net_per);
                            $perAmtOne = 0;
                            $perAmtTwo = 0;
                            if(floatval($netPer) != 0)
                            {
                                $perAmtOne = floatval($netPer) / 100;
                                $perAmtOne = round($perAmtOne, 4);
                                $perAmtTwo = floatval($item->rech_amount) * floatval($perAmtOne);
                                $perAmtTwo = round($perAmtTwo, 2);
                            }

                            echo "<tr><td>".$i."</td>";
                            echo "<td>".$item->rech_mobile."</td>";
                            echo "<td>".$netName."</td>";
                            echo "<td>".$item->rech_amount."</td>";

                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->ret_net_per;
                            echo "<i class='icon-control-play ' style='color:blue;'></i> ".$perAmtTwo."";
                            echo "<i class='icon-control-play ' style='color:blue;'></i>".$item->ret_net_surp."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$item->ret_total."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->trans_id."</td>";

                            if($item->rech_status == "SUCCESS")
                            {
                                
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->reply_id."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->rech_date."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->reply_date."</td>";
                            }
                            else if($item->rech_status == "PENDING" )
                            {
                                echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->rech_date."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                            }
                            else if($item->rech_status == "FAILURE" )
                            {
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->reply_id."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->reply_date."</td>";
                            }

                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$item->rech_status."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'></td>";

                            echo "</tr>";

                            $i++;
                        }           
                         
                      

                      
                      ?>
                        
                       

                    </tbody>
                </table>
              

          </div>

          <!-- End -->
      </div>
      
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $('#tbl_body').delegate('button', 'click', function(e) {
            e.preventDefault();
            var gid=this.id;
            var nid=gid.split("bill1_");
            if(nid.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid[1];
                        window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nidx=gid.split("bill11_");
            if(nidx.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nidx[1];
                        window.open("<?php echo url('/'); ?>/rechargedetails_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid1=gid.split("bill2_");
            if(nid1.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid1[1];
                        window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid12=gid.split("bill21_");
            if(nid12.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid12[1];
                        window.open ( "<?php echo url('/'); ?>/rechargebill_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid2=gid.split("bill3_");
            if(nid2.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid2[1];
                        window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

             var nid21=gid.split("bill31_");
            if(nid21.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid21[1];
                        window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

    });

   
});
</script>
@stop
@stop