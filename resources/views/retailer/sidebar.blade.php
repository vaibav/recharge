<!-- Navbar goes here -->
<ul id="dropdown1" class="dropdown-content">
            <li><a href="#!">Bal : {{$bal}}</a></li>
            <li><a href="{{url('changepassword_retailer')}}">Change Password</a></li>
            <li class="divider"></li>
            <li><a href="{{url('logout')}}">logout</a></li>
        </ul>
        <div class="navbar-fixed">
            <nav class="#5e35b1 deep-purple darken-1">
                <div class="row">
                    <div class="col s3 m4 l3 xl3">
                        <a href="#" data-target="slide-out" class="sidenav-trigger" style="display: block;margin: 0 10px;"><i class="material-icons">menu</i></a>
                        <img class="responsive-img" src="{{ asset('img/brand/logo.png') }}" style="height: 46px; width: 85px;padding-top: 15px;">
                    </div>
                    <div class="col s9 m8 l9 xl9 right-align">
                        <ul class="right ">
                            <li><a href="#" style="padding: 0 6px;">Hai {{$uname}}!&nbsp;&nbsp;&nbsp;&#x20B9;{{$bal}}</a></li>
                            <!-- Dropdown Trigger -->
                            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1" style="padding: 0 6px;padding-top: 2px;"><i class="material-icons">more_vert</i></a></li>
                        </ul>
                    </div>
                </div>
                
            </nav>
        </div>
        
        <ul id="slide-out" class="sidenav">
            <li>
                <div class="user-view">
                <div class="background">
                    <img src="{{ asset('img/brand/back.jpg') }}">
                </div>
                <a href="#user"><img class="circle" src="{{ asset('uploadphoto/user.jpg') }}"></a>
                <a href="#name"><span class="white-text name">{{$uname}}</span></a>
                <a href="{{url('dashboard_retailer')}}"><span class="white-text name" style = "margin-top:4px;">{{$user->mode}}</span></a>
                </div>
            </li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown2"><i class="material-icons">cloud</i> User</a>
                <ul id="dropdown2" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('user_one')}}">User Details</a></li>
                    <li><a href="{{url('surplus_retailer')}}">Network Surplus Details</a></li>
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown3"><i class="material-icons">cloud</i> Payment Details</a>
                <ul id="dropdown3" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('paymentrequest_retailer')}}">Payment Request</a></li>
                    <li><a href="{{url('stockdetails_retailer')}}">Stock Details</a></li>
                    <li><a href="{{url('report_money_verify')}}">Benificiary Verify Details</a></li>
                
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown4"><i class="material-icons">cloud</i> Money Transfer</a>
                <ul id="dropdown4" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('money_user_check')}}">Go</a></li>
                    <li><a href="{{url('ben_view')}}">Beneficiary</a></li>
                    
                
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown5"><i class="material-icons">cloud</i> Reports</a>
                <ul id="dropdown5" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('rechargedetails_retailer')}}">Recharge Details</a></li>
                    <li><a href="{{url('recharge_bill_retailer')}}">EBBill Details</a></li>
                    <li><a href="{{url('recharge_money_retailer')}}">Money Transfer Details</a></li>
                   
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown7"><i class="material-icons">cloud</i> Collection</a>
                <ul id="dropdown7" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('collection_retailer')}}">Collection Details</a></li>
                   
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown6"><i class="material-icons">cloud</i>Complaint</a>
                <ul id="dropdown6" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('complaint_retailer')}}">Complaint Entry</a></li>
                    <li><a href="{{url('complaint_view_retailer')}}">Complaint Details</a></li>
                </ul>
            </li>
            
            
           
        </ul>