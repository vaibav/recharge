@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <form id="rech_form" class="form-horizontal" action="{{url('rechargedetails_retailer_view')}}" method = "GET" accept-charset="UTF-8">
            
            <input type="hidden" id ="web_code" name="web_code" value="{{ $web }}">
            <input type="hidden" id ="bill_code" name="bill_code" value="{{ $bill }}">
            <input type="hidden" id ="money_code" name="money_code" value="{{ $money }}">
            
            <br>
            <div class="container">
              <div class="row" >
                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                    <label for="id_f_date">From Date</label>
                    <input type="text" class="form-control form-control-alternative datepicker" id="id_f_date" name="f_date" value = "<?php echo date('Y-m-d'); ?>">
                      
                  </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                    <label for="id_t_date">To Date</label>
                    <input type="text" class="form-control form-control-alternative datepicker" id="id_t_date" name="t_date" value = "<?php echo date('Y-m-d'); ?>">
                  </div>
                </div>
              </div>
              <br>
              <div class="row" >
                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                      <label for="id_rech_type">Recharge Type</label>
                      <select class="form-control form-control-alternative selectpicker" data-style="btn btn-link" id="id_rech_type"  name = "rech_type" style="padding-top: 0px;padding-bottom: 0px;">
                       <option value="ALL" selected>ALL</option>
                       <option value="RECHARGE" >RECHARGE</option>
                       <option value="BILL_PAYMENT" >BILL_PAYMENT</option>
                       <option value="BANK_TRANSFER" >BANK_TRANSFER</option>
                      </select>
                      
                  </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4" id = "c1" style = "display:none;">
                  <div class="form-group">
                      <label for="id_rech_type">Recharge Status</label>
                      <select class="form-control form-control-alternative selectpicker" data-style="btn btn-link" id="id_rech_status" name="rech_status" style="padding-top: 0px;padding-bottom: 0px;">
                        <option value="-" >---Select--</option>
                        <option value="SUCCESS">SUCCESS</option>
                        <option value="FAILURE">FAILURE</option>
                        <option value="PENDING">PENDING</option>
                      </select>
                  </div>
                </div>
              </div>
              <br>
              <div class="row" id = "c2" style = "display:none;">
                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                    <label for="id_rech_mobile">Mobile No</label>
                    <input type="text" class="form-control form-control-alternative" id="id_rech_mobile" name="rech_mobile" value = "">
                      
                  </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                    <label for="id_rech_amount">Amount</label>
                    <input type="text" class="form-control form-control-alternative" id="id_rech_amount" name="rech_amount" >
                  </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4">
                  <div class="form-group">
                      <label for="id_net_code">Network</label>
                      <select class="form-control form-control-alternative selectpicker" data-style="btn btn-link" id="id_net_code" name="net_code" style="padding-top: 0px;padding-bottom: 0px;">
                        <option value="-" >---Select--</option>
                      </select>
                  </div>
                </div>
              </div>


          
              <br>
              <div class="row">
                  <div class="col-12 col-sm-12 col-md-3" style="padding-left: 35px;">
                    <button class="btn btn-icon btn-primary" type="button" id="btn_submit" name="btn_submit">
                      <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                        <span class="btn-inner--text">Submit</span>
                    </button>
                  </div>
              </div>
              <br>
            </div>
          </form>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-2">

       
      </div>
      <div class="col-lg-8">
          <!-- Table -->
          

          <!-- End -->
      </div>
      <div class="col-lg-2">
         
      </div>
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" ></script>

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
   

    

    $('#btn_submit').on('click',function(e)
    {
      e.preventDefault();

      z= checkData();

      if(z[0] == 0)
      {
        $('#web_code').val("-");
        $('#bill_code').val("-");
        $('#money_code').val("-");
                
        $('#rech_form').attr('action', "{{url('rechargedetails_retailer_view')}}");
        $('#rech_form').submit();
      }
      else {
          swal("Cancelled", z[1], "error");
      }
        
        
    }); 

  $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
  {
      numbersOnly(e);
  });

  $("#id_rech_type").change(function(e)
    {
        var ty = $('option:selected', this).val();
       
        if(ty == "ALL")
        {
            
            $('#c2').hide("slow");
            $('#c1').hide("slow");
        }
        else if(ty == "RECHARGE")
        {
            var data = $("#web_code").val();
            loadNetwork(data);
            
            $('#c2').show("slow");
            $('#c1').show("slow");
        
        }
        else if(ty == "BILL_PAYMENT")
        {
            var data = $("#bill_code").val();
            loadNetwork(data);
           
            $('#c2').show("slow");
            $('#c1').show("slow");
        
        }
        else if(ty == "BANK_TRANSFER")
        {
            var data = $("#money_code").val();
            loadNetwork(data);
            
            $('#c2').show("slow");
            $('#c1').show("slow");
        }
            
    });

   function loadNetwork(data)
  {
      var net = JSON.parse(data);
      
      var net_code = $("#id_net_code");
      $(net_code).empty();
      var option1 = $("<option />");
          option1.html("select Network");
          option1.val("-");
          net_code.append(option1);

      $(net).each(function () {
          var option = $("<option />");
          option.html(this.net_name);
          option.val(this.net_code);
          net_code.append(option);
      });

      
  }

  function checkData()
  {
      var z = [];
      z[0] = 0;
      z[1] = "Success";

    
      if($("#id_f_date").val() == "" ) {
          z[0] = 1;
          z[1] = "From Date is Empty";
      }

      if($("#id_t_date").val() == "" ) {
          z[0] = 1;
          z[1] = "To Date is Empty";
      }

      if($("#id_net_code").val() == "" ) {
          z[0] = 1;
          z[1] = "Network is Empty";
      }

      return z;
  }


    function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
        // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
        // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
          // let it happen, don't do anything
          return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
   
});
</script>
@stop
@stop