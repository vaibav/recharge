<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        <meta name="google" content="notranslate">
        
    </head>
    <body style = "background-color: #34495e;">
        @include('retailer.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Complaint Recharge</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('complaint_retailer')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>


                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                      <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                      
                        <!-- Main Content-->
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                            <!-- Form Starts-->
                            <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                        <th style="font-size:12px;padding:7px 8px;">NO</th>
                                        <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                        <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                        <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                        <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                        <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                        <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                        <th style="font-size:12px;padding:7px 8px;">COMPLAINT</th>
                                        <th style="font-size:12px;padding:7px 8px;">SEND</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                        <?php 
                                            
                                                
                                                $str = "";
                                                
                                                $j = 1;
                                                foreach($recharge as $d)
                                                {
                                                
                                                    if($d->trans_type == "WEB_RECHARGE")
                                                    {
                                                        if(sizeof($d->newrecharge1) > 0)
                                                        {
                                                            $net_name = "";
                                                            foreach($d2 as $r)
                                                            {
                                                                if($d->newrecharge1[0]->net_code == $r->net_code)
                                                                    $net_name = $r->net_name;
                                                            }
                                        
                                                            $reply_id = $d->newrecharge2->reply_opr_id;
                                                            $reply_date = $d->newrecharge2->reply_date;
                                                        
                                        
                                                            $rech_status = "";
                                                            $status = "";
                                                            $r_tot = 0;
                                        
                                                            if($d->trans_option == 1)
                                                            {
                                                                $rech_status = $d->newrecharge1[0]->rech_status;
                                                                $rech_option = $d->newrecharge1[0]->rech_option;
                                                                
                                                                $str = $str."<tr>";
                                                            }
                                                            else if($d->trans_option == 2)
                                                            {
                                                                $rech_status = $d->newrecharge1[1]->rech_status;
                                                                $rech_option = $d->newrecharge1[1]->rech_option;
                                                              
                                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                            }
                                                            if($rech_status == "PENDING" && $rech_option == "0")
                                                            {
                                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                            }
                                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                                            {
                                                                $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                            }
                                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                                            {      
                                                                $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                            }
                                                            else if ($rech_status == "SUCCESS")
                                                            {
                                                                $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                                
                                                            }
                                        
                                                           
                                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;' id = 'mob_".$d->trans_id."'>".$d->newrecharge1[0]->rech_mobile."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;' id = 'amt_".$d->trans_id."'>".$d->newrecharge1[0]->rech_amount."</td>";
                                                            if($d->trans_option == 1)
                                                            {
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_net_per;
                                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->newrecharge1[0]->rech_net_per_amt."";
                                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->newrecharge1[0]->rech_net_surp."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->rech_total."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_date."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>";
                                                                    $str = $str."<input type='text' id='cmp_".$d->trans_id."' class='form-control' /></td>";
                                                                $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-small ' id='sbt_".$d->trans_id."'>
                                                                    Send</button></td></tr>";
                                                            }
                                                            else if($d->trans_option == 2)
                                                            {
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[1]->rech_total."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'></td>";
                                                                $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'></td></tr>";
                                                            }
                                                          
                                                        }
                                                        
                                                    
                                                    }
                                                                                        
                                                
                                                                       
                                                    $j++;
                                                }
                                                
                                                echo $str; 
                                            

                                            
                                            ?>

                                    </tbody>
                                </table>
                            
                                {{ $recharge->links('vendor.pagination.materializecss') }}
                                        
                                    

                                <!-- End Form-->
                            </div>
                        </div>

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                


                     
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Complaint is registered Successfully...', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#tbl_body').delegate('button', 'click', function(e) {
                    e.preventDefault();
                    var gid = this.id;
                    var nid=gid.split("sbt_");
                    if(nid.length>1)
                    {
                        var tid = nid[1];
                        var mob = $('#mob_' + tid).text();
                        var amt = $('#amt_' + tid).text();
                        var cmp = $('#cmp_' + tid).val();

                        //alert(tid + "---" + mob + "---" + amt + "---" + cmp);

                        if(tid != "" && mob != "" && amt != "" && cmp != "")
                        {
                            swal({
                                title: 'Are you sure?',
                                text: "Confirmation Alert",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, Send Complaint!'
                                }).then((result) => {
                                if (result.value) {
                                    
                                    window.open("<?php echo url('/'); ?>/complaint_retailer_recharge_store?trid=" + tid + "&mob=" + mob + "&amt=" + amt + "&cmp=" +cmp, "_self" );
                                }
                                else
                                {
                                    swal("Cancelled", "Sorry...", "error");
                                }
                            });
                        }
                        else
                        {
                            swal("Cancelled", "Complaint Entry May be Empty...", "error");
                        }
                        
                    
                    }

                    

            });
  
      });
    </script>
    </body>
</html>
