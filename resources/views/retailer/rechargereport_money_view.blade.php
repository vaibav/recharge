<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('retailer.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Money Transfer Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('recharge_money_retailer')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <div class="row" >
                    <div class="col s12 m12 l12 xl12 " >
                        <div class = "card-content white darken-1" >
                            <div class="col s9 m9 l9 xl9 left-align">
                                <label class="title-con" style="font-size:14px;">Success Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Success Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">Failure Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Failure Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Credit Total</label><br>
                            </div>
                            <div class="col s2 m2 l2 xl2 right-align" >
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['msua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['msut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mrea_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mret_tot'] }}</label><br>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style="font-size:12px;padding:7px 8px;">NO</th>
                                <th style="font-size:12px;padding:7px 8px;">ACC.NO</th>
                                <th style="font-size:12px;padding:7px 8px;">BANK</th>
                                <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                    $str = "";
                                    $j = 1;
                                    foreach($recharge as $d)
                                    {
                                        if($d->trans_type == "BANK_TRANSFER")
                                        {
                                           
                            
                                            $api_name = "";
                            
                                            $rech_status = "";
                                            $status = "";
                                            $o_bal = 0;
                                            $u_bal = 0;
                                            $r_tot = 0;
                            
                                        
                            
                                            if($d->trans_option == 1)
                                            {
                                                $str = $str."<tr>";
                                                $rech_status = $d->moneytransfer[0]->mt_status;
                                                $rech_option = $d->moneytransfer[0]->mt_option;          
                                                $r_tot = $d->moneytransfer[0]->mt_total;
                                                $u_bal = $d->moneytransfer[0]->user_balance;
                                            
                                            }
                                            else if($d->trans_option == 2)
                                            {  
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                $rech_status = $d->moneytransfer[1]->mt_status;
                                                $rech_option = $d->moneytransfer[1]->mt_option;          
                                                $r_tot = $d->moneytransfer[1]->mt_total;
                                                $u_bal = $d->moneytransfer[1]->user_balance;
                                                
                                            }
                                            if($rech_status == "PENDING" && $rech_option == 1)
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == 2)
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if($rech_status == "FAILURE"  && $rech_option == 2)
                                            {      
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                                //$u_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            
                                            $mode = "WEB";
                                            if($d->trans_id != "")
                                            {
                                                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                            
                                                $r_l = $matches[0][0];
                            
                                                $r_l = substr($r_l, -1);
                            
                                                if($r_l == "R")
                                                    $mode = "WEB";
                                                else if($r_l == "A")
                                                    $mode = "API";
                                                else if($r_l == "G")
                                                    $mode = "GPRS";
                                                else if($r_l == "S")
                                                    $mode = "SMS";
                            
                                            }

                                            $bank = "";
                                            if($d->moneytransfer[0]->bk_code == "UTIB") {
                                                $bank = "AXIS BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "BARB") {
                                                $bank = "BANK OF BARODA";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "CNRB") {
                                                $bank = "CANARA BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "CORP") {
                                                $bank = "CORPORATION BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "CIUB") {
                                                $bank = "CITY UNION BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "HDFC") {
                                                $bank = "HDFC BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "IBKL") {
                                                $bank = "IDBI BANK LTD";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "IDIB") {
                                                $bank = "INDIAN BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "IOBA") {
                                                $bank = "INDIAN OVERSEAS BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "ICIC") {
                                                $bank = "ICICI BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "KVBL") {
                                                $bank = "KARUR VYSYA BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "SBIN") {
                                                $bank = "STATE BANK OF INDIA";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "SYNB") {
                                                $bank = "SYNDICATE BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "TMBL") {
                                                $bank = "TAMILNADU MERCANTILE BANK";
                                            }
                                            else if($d->moneytransfer[0]->bk_code == "VIJB") {
                                                $bank = "VIJAYA BANK";
                                            }
                                            else {
                                                $bank = $d->moneytransfer[0]->bk_code;
                                            }

                                            
                            
                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->bk_acc_no."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$bank."</td>";
                                            
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_amount."</td>";
                                            if($d->trans_option == 1)
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->moneytransfer[0]->mt_per_amt."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->moneytransfer[0]->mt_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_date."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->reply_date."</td>";
                                            }
                                            else if($d->trans_option == 2)
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 250px;word-break: break-word;'>".$d->moneytransfer[1]->mt_reply_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td>";
                                            $str = $str. "<td style='font-size:11px;padding:7px 8px;'><button class='btn-floating btn-small' id='bill3_".$d->trans_id."'>
                                            <i class='material-icons right'>show_chart</i></button></td></tr>";
                                        }                                        
                                                            
                                        $j++;
                                    }
                                    
                                    echo $str; 
                                    
                                ?>

                            </tbody>
                        </table>

                        {{ $recharge->links('vendor.pagination.materializecss') }}
              
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#tbl_body').delegate('button', 'click', function(e) {
                      e.preventDefault();
                      var gid=this.id;
                      var nid=gid.split("bill3_");
                      if(nid.length>1)
                      {
                          swal({
                              title: 'Are you sure?',
                              text: "Confirmation Alert",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Yes, Get Bill!'
                              }).then((result) => {
                              if (result.value) {
                                  var code = nid[1];
                                  window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice/" + code);
                              }
                              else
                              {
                                  swal("Cancelled", "Sorry...", "error");
                              }
                          });
                        
                      }

                });
            
           
      });
    </script>
    </body>
</html>
