@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <div class="container">
              <div class="row" >
               
                <div class="col-12 col-sm-12 col-md-12 bg-gradient-danger" style="color:white;">
                    <div class="row" >
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;">Success Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Success Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Amount Total</label><br>
                          <label class="title-con" style="font-size:14px;">Credit Total</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['erea_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['eret_tot'] }}</label><br>
                      </div>
                    </div>
                </div>
                
              </div>
          </div>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      
      <div class="col-lg-12">
          <!-- Table -->
          <div class="table-responsive">

           
                <table class="table table-striped align-items-center">
                    <thead class="thead-light">
                        <tr>
                          <th style="font-size:12px;padding:7px 8px;">NO</th>
                          <th style="font-size:12px;padding:7px 8px;">CONN.NO</th>
                          <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                          <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                          <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                          <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL</th>
                        </tr>
                    </thead>
                    <tbody class="list" id = "tbl_body">

                      <?php 
                          $str = "";
                          $j = 1;
                          foreach($recharge as $d)
                          {
                              if($d->trans_type == "BILL_PAYMENT")
                              {
                                  $net_name = "";
                                  foreach($d2 as $r)
                                  {
                                      if($d->billpayment[0]->net_code == $r->net_code)
                                          $net_name = $r->net_name;
                                  }
                  
                                  $api_name = "";
                                  $rech_status = "";
                                  $status = "";
                                  $o_bal = 0;
                                  $u_bal = 0;
                                  $r_tot = 0;
                  
                                  if($d->trans_option == 1)
                                  {
                                      $str = $str."<tr>";
                                      $rech_status = $d->billpayment[0]->con_status;
                                      $rech_option = $d->billpayment[0]->con_option;          
                                      $r_tot = $d->billpayment[0]->con_total;
                                      $u_bal = $d->billpayment[0]->user_balance;
                                  
                                  }
                                  else if($d->trans_option == 2)
                                  {  
                                      $str = $str."<tr style='background-color:#E8DAEF;'>";
                                      $rech_status = $d->billpayment[1]->con_status;
                                      $rech_option = $d->billpayment[1]->con_option;          
                                      $r_tot = $d->billpayment[1]->con_total;
                                      $u_bal = $d->billpayment[1]->user_balance;
                                      
                                  }
                                  
                                  if($rech_status == "PENDING" && $rech_option == 1)
                                  {
                                      $status = "<button class='btn btn-icon btn-primary btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-user-run'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if($rech_status == "PENDING" && $rech_option == 2)
                                  {
                                      $status = "<button class='btn btn-icon btn-danger btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-scissors'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if($rech_status == "FAILURE"  && $rech_option == 2)
                                  {      
                                      $status = "<button class='btn btn-icon btn-warning btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-bold-left'></i></span>
                                            </button>";
                                      //$o_bal = floatval($u_bal) - floatval($r_tot);
                                      $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                      //$u_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if ($rech_status == "SUCCESS")
                                  {
                                      $status = "<button class='btn btn-icon btn-success btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-check-bold'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  
                                  $mode = "WEB";
                                  if($d->trans_id != "")
                                  {
                                      preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                  
                                      $r_l = $matches[0][0];
                  
                                      $r_l = substr($r_l, -1);
                  
                                      if($r_l == "R")
                                          $mode = "WEB";
                                      else if($r_l == "A")
                                          $mode = "API";
                                      else if($r_l == "G")
                                          $mode = "GPRS";
                                      else if($r_l == "S")
                                          $mode = "SMS";
                  
                                  }
                                  
                  
                                  $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                                  if($d->trans_option == 1)
                                  {
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                                      $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                                      $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                                  }
                                  else if($d->trans_option == 2)
                                  {
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                  }
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td>";
                                   $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill2_".$d->trans_id."'>
                                                <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                            </button></td></tr>";
                              } 
                                                                  
                          
                                                  
                              $j++;
                          }
                          
                          echo $str; 
                          
                      ?>

                        
                       

                    </tbody>
                </table>
                {{ $recharge->links('vendor.pagination.argon') }}

          </div>

          <!-- End -->
      </div>
      
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
  
  $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("bill2_");
        if(nid.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Get Bill!'
                }).then((result) => {
                if (result.value) {
                    var code = nid[1];
                    window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
          
        }

  });

});
</script>
@stop
@stop