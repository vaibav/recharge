@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <form id="rech_form" class="form-horizontal" action="{{url('rechargedetails_bill_retailer_view')}}" method = "GET" accept-charset="UTF-8">
            
           <h6 style="margin:15px;">Bill Payment Report </h6>
            
            <br>
            <div class="row" style="margin: 15px;">
              <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                  <label for="id_f_date">From Date</label>
                  <input type="text" class="form-control form-control-alternative datepicker" id="id_f_date" name="f_date" value = "<?php echo date('Y-m-d'); ?>">
                    
                </div>
              </div>

              <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                  <label for="id_t_date">To Date</label>
                  <input type="text" class="form-control form-control-alternative datepicker" id="id_t_date" name="t_date" value = "<?php echo date('Y-m-d'); ?>">
                </div>
              </div>
            </div>
        
            <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-3" style="padding-left: 35px;">
                  <button class="btn btn-icon btn-primary" type="button" id="btn_submit" name="btn_submit">
                    <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                      <span class="btn-inner--text">Next</span>
                  </button>
                </div>
            </div>
            <br>
          </form>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-2">

       
      </div>
      <div class="col-lg-8">
          <!-- Table -->
          

          <!-- End -->
      </div>
      <div class="col-lg-2">
         
      </div>
      
    </div>
  </div>
</div>

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" ></script>

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    
  $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
   
    $('#btn_submit').on('click',function(e)
    {
      e.preventDefault();

      z= checkData();

      if(z[0] == 0)
      {
        $('#rech_form').attr('action', "{{url('rechargedetails_bill_retailer_view')}}");
        $('#rech_form').submit();
      }
      else {
          swal("Cancelled", z[1], "error");
      }
        
        
    }); 

 
  function checkData()
  {
      var z = [];
      z[0] = 0;
      z[1] = "Success";

    
      if($("#id_f_date").val() == "" ) {
          z[0] = 1;
          z[1] = "From Date is Empty";
      }

      if($("#id_t_date").val() == "" ) {
          z[0] = 1;
          z[1] = "To Date is Empty";
      }

      return z;
  }


    
   
});
</script>
@stop
@stop