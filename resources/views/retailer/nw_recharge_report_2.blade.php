@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <div class="container">
              <div class="row" >
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-danger" style="color:white;">
                    <h5>Recharge</h5>
                    <div class="row" >
                      
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;">Success Amount </label><br>
                          <label class="title-con" style="font-size:14px;">Success Credit </label><br>
                          <label class="title-con" style="font-size:14px;">Failure Amount </label><br>
                          <label class="title-con" style="font-size:14px;">Failure Credit </label><br>
                          <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Total Credit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;{{ $total['wsua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;{{ $total['wsut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" id = "re_3">&#x20B9;{{ $total['wfua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" id = "tr_1">&#x20B9;{{ $total['wfut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" id = "tr_2">&#x20B9;{{ $total['wrea_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" id = "tr_3">&#x20B9;{{ $total['wret_tot'] }}</label><br>
                      </div>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-primary" style="color:white;">
                    <h5>Bill Payment</h5>
                    <div class="row" >
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;">Success Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Success Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Amount</label><br>
                          <label class="title-con" style="font-size:14px;">Failure Credit</label><br>
                          <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                          <label class="title-con" style="font-size:14px;">TotalCredit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['erea_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['eret_tot'] }}</label><br>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 bg-gradient-warning" style="color:white;">
                    <h5>Money Transfer</h5>
                    <div class="row" >
                      <div class="col-12 col-sm-12 col-md-6">
                            <label class="title-con" style="font-size:14px;">Success Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Success Credit</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Credit</label><br>
                            <label class="title-con" style="font-size:14px;">Total Amount</label><br>
                            <label class="title-con" style="font-size:14px;">Total Credit</label><br>
                      </div>
                      <div class="col-12 col-sm-12 col-md-6">
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['msua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;">&#x20B9;{{ $total['msut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfua_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfut_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mrea_tot'] }}</label><br>
                          <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mret_tot'] }}</label><br>
                      </div>
                    </div>
                </div>
              </div>
          </div>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 90" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 90 0 90"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      
      <div class="col-lg-12">
          <!-- Table -->
          <div class="table-responsive">

           
                <table class="table table-striped align-items-center">
                    <thead class="thead-light">
                        <tr>
                          <th style="font-size:12px;padding:7px 8px;">NO</th>
                          <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                          <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                          <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                          <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                          <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                          <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                          <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL 1</th>
                          <th style="font-size:12px;padding:7px 8px;text-align:right;">BILL 2</th>
                        </tr>
                    </thead>
                    <tbody class="list" id = "tbl_body">

                      <?php 
                                            
                                                
                          $str = "";
                          
                          $j = 1;
                          foreach($recharge as $d)
                          {
                          
                              if($d->trans_type == "RECHARGE" || $d->trans_type == "BANK_TRANSFER" || $d->trans_type == "BILL_PAYMENT")
                              {

                                $net_name = "";
                                foreach($d2 as $r)
                                {
                                    if($d->net_code == $r->net_code)
                                        $net_name = $r->net_name;
                                }

                                $reply_id = $d->reply_id;
                                $reply_date = $d->reply_date;

                                $rech_status = $d->rech_status;
                                $rech_option = $d->rech_option;
                                $u_bal = $d->ret_bal;
                                $r_tot = $d->ret_total;
                                $o_bal = 0;

                                if($rech_status == "PENDING" && $rech_option == "0")
                                {
                                    $status = "<button class='btn btn-icon btn-primary btn-round' type='button'>
                                          <span class='btn-inner--icon'><i class='ni ni-user-run'></i></span>
                                      </button>";
                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                    $str = $str."<tr>";
                                }
                                else if($rech_status == "PENDING" && $rech_option == "2")
                                {
                                    $status = "<button class='btn btn-icon btn-danger btn-round' type='button'>
                                          <span class='btn-inner--icon'><i class='ni ni-scissors'></i></span>
                                      </button>";
                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                    $str = $str."<tr>";
                                }
                                else if($rech_status == "FAILURE" && $rech_option == "2")
                                {      
                                    $status = "<button class='btn btn-icon btn-warning btn-round' type='button'>
                                          <span class='btn-inner--icon'><i class='ni ni-bold-left'></i></span>
                                      </button>";
                                    $o_bal = floatval($u_bal) - floatval($r_tot);
                                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                                }
                                else if ($rech_status == "SUCCESS")
                                {
                                    $status = "<button class='btn btn-icon btn-success btn-round' type='button'>
                                          <span class='btn-inner--icon'><i class='ni ni-check-bold'></i></span>
                                      </button>";
                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                    $str = $str."<tr>";
                                }

                                $mode = "WEB";
                                if($d->trans_id != "")
                                {
                                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
            
                                    $r_l = $matches[0][0];
            
                                    $r_l = substr($r_l, -1);
            
                                    if($r_l == "R")
                                        $mode = "WEB";
                                    else if($r_l == "A")
                                        $mode = "API";
                                    else if($r_l == "G")
                                        $mode = "GPRS";
                                    else if($r_l == "S")
                                        $mode = "SMS";
            
                                }

                                //percentage amount
                                $per = floatval($d->ret_net_per);
                                $peramt1 = 0;
                                $peramt2 = 0;
                                if(floatval($per) != 0)
                                {
                                    $peramt1 = floatval($per) / 100;
                                    $peramt1 = round($peramt1, 4);
                            
                                    $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                    $peramt2 = round($peramt2, 2);
                                }

                                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                     
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";

                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$peramt2."";
                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->ret_net_surp."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                if($d->rech_status == "SUCCESS")
                                {
                                    
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                }
                                else if($d->rech_status == "PENDING" )
                                {
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                }
                                else if($d->rech_status == "FAILURE" )
                                {
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                }
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td>";

                                if($d->trans_type == "RECHARGE")
                                {
                                  $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill1_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button> </td>";

                                  $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill11_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button></td></tr>";
                                }
                                else if($d->trans_type == "BANK_TRANSFER")
                                {
                                   $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill3_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button> </td>";

                                  $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill31_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button></td></tr>";
                                }
                                else if($d->trans_type == "BILL_PAYMENT")
                                {
                                   $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill2_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button> </td>";

                                  $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill21_".$d->trans_id."'>
                                          <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                      </button></td></tr>";
                                }
                                
                              
                              }
                              if($d->trans_type == "BILL_PAYMENT1")
                              {
                                  $net_name = "";
                                  foreach($d2 as $r)
                                  {
                                      if($d->billpayment[0]->net_code == $r->net_code)
                                          $net_name = $r->net_name;
                                  }
                  
                                  $api_name = "";
                                 
                  
                                  $rech_status = "";
                                  $status = "";
                                  $o_bal = 0;
                                  $u_bal = 0;
                                  $r_tot = 0;
                  
                              
                  
                                  if($d->trans_option == 1)
                                  {
                                      $str = $str."<tr>";
                                      $rech_status = $d->billpayment[0]->con_status;
                                      $rech_option = $d->billpayment[0]->con_option;          
                                      $r_tot = $d->billpayment[0]->con_total;
                                      $u_bal = $d->billpayment[0]->user_balance;
                                  
                                  }
                                  else if($d->trans_option == 2)
                                  {  
                                      $str = $str."<tr style='background-color:#E8DAEF;'>";
                                      $rech_status = $d->billpayment[1]->con_status;
                                      $rech_option = $d->billpayment[1]->con_option;          
                                      $r_tot = $d->billpayment[1]->con_total;
                                      $u_bal = $d->billpayment[1]->user_balance;
                                      
                                  }
                                  
                                  if($rech_status == "PENDING" && $rech_option == 1)
                                  {
                                      $status = "<button class='btn btn-icon btn-primary btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-user-run'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if($rech_status == "PENDING" && $rech_option == 2)
                                  {
                                      $status = "<button class='btn btn-icon btn-danger btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-scissors'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if($rech_status == "FAILURE"  && $rech_option == 2)
                                  {      
                                      $status = "<button class='btn btn-icon btn-warning btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-bold-left'></i></span>
                                            </button>";
                                      //$o_bal = floatval($u_bal) - floatval($r_tot);
                                      $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                      //$u_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  else if ($rech_status == "SUCCESS")
                                  {
                                      $status = "<button class='btn btn-icon btn-success btn-round' type='button'>
                                                <span class='btn-inner--icon'><i class='ni ni-check-bold'></i></span>
                                            </button>";
                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                  }
                                  
                                  $mode = "WEB";
                                  if($d->trans_id != "")
                                  {
                                      preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                  
                                      $r_l = $matches[0][0];
                  
                                      $r_l = substr($r_l, -1);
                  
                                      if($r_l == "R")
                                          $mode = "WEB";
                                      else if($r_l == "A")
                                          $mode = "API";
                                      else if($r_l == "G")
                                          $mode = "GPRS";
                                      else if($r_l == "S")
                                          $mode = "SMS";
                  
                                  }
                                  
                  
                                  $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                                  if($d->trans_option == 1)
                                  {
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                                      $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                                      $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                                  }
                                  else if($d->trans_option == 2)
                                  {
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                  }
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                  $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td>";

                                  $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill2_".$d->trans_id."'>
                                                <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                            </button> </td>";
                                      $str = $str. "<td style='font-size:11px;padding:7px 8px;text-align:center;'><button class='btn btn-icon btn-primary btn-round' type='button' id='bill21_".$d->trans_id."'>
                                                <span class='btn-inner--icon'><i class='ni ni-diamond'></i></span>
                                            </button></td></tr>";
                                 
                              } 
                                                                  
                          
                                                 
                              $j++;
                          }
                          
                          echo $str; 
                      

                      
                      ?>
                        
                       

                    </tbody>
                </table>
                {{ $recharge->links('vendor.pagination.argon') }}

          </div>

          <!-- End -->
      </div>
      
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $('#tbl_body').delegate('button', 'click', function(e) {
            e.preventDefault();
            var gid=this.id;
            var nid=gid.split("bill1_");
            if(nid.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid[1];
                        window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nidx=gid.split("bill11_");
            if(nidx.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nidx[1];
                        window.open("<?php echo url('/'); ?>/rechargedetails_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid1=gid.split("bill2_");
            if(nid1.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid1[1];
                        window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid12=gid.split("bill21_");
            if(nid12.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid12[1];
                        window.open ( "<?php echo url('/'); ?>/rechargebill_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

            var nid2=gid.split("bill3_");
            if(nid2.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid2[1];
                        window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

             var nid21=gid.split("bill31_");
            if(nid21.length>1)
            {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Get Bill!'
                    }).then((result) => {
                    if (result.value) {
                        var code = nid21[1];
                        window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice_t/" + code);
                    }
                    else
                    {
                        swal("Cancelled", "Sorry...", "error");
                    }
                });
            
            }

    });

   
});
</script>
@stop
@stop