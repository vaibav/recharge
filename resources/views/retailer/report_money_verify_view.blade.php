<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('retailer.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Beneficiary Verification Report</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('report_money_verify')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>


                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                      <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       
                        <!-- Main Content-->
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                            <!-- Form Starts-->
                            <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                        <th style="font-size:12px;padding:7px 8px;">NO</th>
                                        <th style="font-size:12px;padding:7px 8px;">Account No</th>
                                        <th style="font-size:12px;padding:7px 8px;">Bank</th>
                                        <th style="font-size:12px;padding:7px 8px;">Surplus</th>
                                        <th style="font-size:12px;padding:7px 8px;">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                        <?php  
                                            $str = "";
                                            
                                            $j = 1;

                                            foreach($money as $d)
                                            {
                                            
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                if($d->ben_status == "FAILURE" )
                                                {      
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                }
                                                else if ($d->ben_status == "SUCCESS")
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                }

                                                $bank = "";
                                                if($d->ben_code == "UTIB") {
                                                    $bank = "AXIS BANK";
                                                }
                                                else if($d->ben_code == "BARB") {
                                                    $bank = "BANK OF BARODA";
                                                }
                                                else if($d->ben_code == "CNRB") {
                                                    $bank = "CANARA BANK";
                                                }
                                                else if($d->ben_code == "CORP") {
                                                    $bank = "CORPORATION BANK";
                                                }
                                                else if($d->ben_code == "CIUB") {
                                                    $bank = "CITY UNION BANK";
                                                }
                                                else if($d->ben_code == "HDFC") {
                                                    $bank = "HDFC BANK";
                                                }
                                                else if($d->ben_code == "IBKL") {
                                                    $bank = "IDBI BANK LTD";
                                                }
                                                else if($d->ben_code == "IDIB") {
                                                    $bank = "INDIAN BANK";
                                                }
                                                else if($d->ben_code == "IOBA") {
                                                    $bank = "INDIAN OVERSEAS BANK";
                                                }
                                                else if($d->ben_code == "ICIC") {
                                                    $bank = "ICICI BANK";
                                                }
                                                else if($d->ben_code == "KVBL") {
                                                    $bank = "KARUR VYSYA BANK";
                                                }
                                                else if($d->ben_code == "SBIN") {
                                                    $bank = "STATE BANK OF INDIA";
                                                }
                                                else if($d->ben_code == "SYNB") {
                                                    $bank = "SYNDICATE BANK";
                                                }
                                                else if($d->ben_code == "TMBL") {
                                                    $bank = "TAMILNADU MERCANTILE BANK";
                                                }
                                                else if($d->ben_code == "VIJB") {
                                                    $bank = "VIJAYA BANK";
                                                }
                                                else {
                                                    $bank = $d->ben_code;
                                                }

                                                $str = $str."<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ben_acc_no."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$bank."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ben_surplus."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td></tr>";

                                                $j++;
                                            }

                                            $str = $str."<tr><td style='font-size:11px;padding:7px 8px;'></td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>Total</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$total."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td></tr>";
                                            
                                            echo $str; 
                                            

                                            
                                            ?>

                                    </tbody>
                                </table>
                            
                                {{ $money->links('vendor.pagination.materializecss') }}
                                        
                                    

                                <!-- End Form-->
                            </div>
                        </div>

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                


                     
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#tbl_body').delegate('button', 'click', function(e) {
                    e.preventDefault();
                    var gid=this.id;
                    var nid=gid.split("bill1_");
                    if(nid.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid[1];
                                window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

                    var nid1=gid.split("bill2_");
                    if(nid1.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid1[1];
                                window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

                    var nid2=gid.split("bill3_");
                    if(nid2.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid2[1];
                                window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

            });
  
      });
    </script>
    </body>
</html>
