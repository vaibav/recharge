@extends('layouts.retailer_new')
@section('styles')
    <style type = "text/css">
      table th {font-size:13px !important;}
      table td {font-size:12px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
 <div class="section section-hero section-shaped">
  <div class="shape shape-style-1 bg-gradient-default">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
      
  <div class="page-header">
    <div class="container shape-container d-flex align-items-center py-lg" style="padding-top: 26px !important;">
      <div class="col px-0">
         <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12" style="margin-top: 5px;">
                  <marquee><label style="color:orange;">Welcome to VaibavOnline Recharge Portal</label></marquee>
                </div>
            </div>
            <br>
        <div class="card card-profile shadow ">
          <form id="rech_form" class="form-horizontal" action="{{url('paymentrequest_store_retailer')}}" method = "POST" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <h6 style="margin:25px">Payment Request</h6>
            
            <br>
            <div class="row" style="margin: 15px;">
              <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                  <label for="id_user_amount">Request Amount</label>
                  <input type="text" class="form-control form-control-alternative" id="id_user_amount" name="user_amount" value = "">
                    
                </div>
              </div>

            </div>

            <div class="row" style="margin: 15px;">
              <div class="col-12 col-sm-12 col-md-4">
                <div class="form-group">
                  <label for="id_user_remarks">Remarks</label>
                  <input type="text" class="form-control form-control-alternative" id="id_user_remarks" name="user_remarks" >
                </div>
              </div>
            </div>
        
            <br>
            <div class="row">
                <div class="col-12 col-sm-12 col-md-3" style="padding-left: 35px;">
                  <button class="btn btn-icon btn-primary" type="button" id="btn_submit" name="btn_submit">
                    <span class="btn-inner--icon"><i class="ni ni-bag-17"></i></span>
                      <span class="btn-inner--text">Request</span>
                  </button>
                </div>
            </div>
            <br>
          </form>
           


        </div>
        
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>


<div class="section features-6">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-lg-2">

       <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
          <!-- Table -->
          <div class="table-responsive">
              <table class="table table-striped align-items-center">
                  <thead class="thead-light">
                      <tr>
                          <th style='font-size:12px;padding:7px 8px;'>NO</th>
                          <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                          <th style='font-size:12px;padding:7px 8px;'>Req Amt</th>
                          <th style='font-size:12px;padding:7px 8px;'>Req Date</th>
                          <th style='font-size:12px;padding:7px 8px;'>Trans Amt</th>
                          <th style='font-size:12px;padding:7px 8px;'>Date</th>
                          <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                          <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                          <th style='font-size:12px;padding:7px 8px;'>Status</th>
                      </tr>
                  </thead>
                  <tbody class="list">

                    <?php 
                        $j = 1;
                        foreach($pay as $f)
                        {
                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_amount."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_date."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ret_total."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_date."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_id."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_type."</td>";
                          if($f->rech_status == "PENDING")
                              echo "<td style='font-size:12px;padding:7px 8px;'>PENDING</td>";
                          else if($f->rech_status == "SUCCESS")
                              echo "<td style='font-size:12px;padding:7px 8px;'>SUCCESS</td>";
                          else if($f->rech_status == "FAILURE")
                              echo "<td style='font-size:12px;padding:7px 8px;'>FAILURE</td>";
                          echo "</tr>";
                          $j++;
                          
                        }

                    ?>
                  </tbody>
              </table>
          </div>
          <!-- End -->
      </div>
      <div class="col-lg-2">
         <div class="overlay-ribbon">
          <div class="ribbon-content">
            <span id="close" class="close-x"><a href="#">x</a></span>
            <div class="img-container"><img src="{{ asset('img/brand/logo.png') }}" width="171" height="135" alt="" /></div>
            <h3>Advertisement</h3>
            <p>This is Advertisement banner.. you can add content in this banner.</p>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

            
          

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else
        {
          echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>"; 
        }
        session()->forget('result');
    }

?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $("#id_user_amount").keydown(function (e) 
    {
        numbersOnly(e);
    });

    $('#btn_submit').on('click',function(e)
    {
      e.preventDefault();

      z= checkData();

      if(z[0] == 0)
      {
        $('#rech_form').attr('action', "{{url('paymentrequest_store_retailer')}}");
        $('#rech_form').submit();
      }
      else {
          swal("Cancelled", z[1], "error");
      }
        
        
    }); 

 
  function checkData()
  {
      var z = [];
      z[0] = 0;
      z[1] = "Success";

    
      if($("#id_user_amount").val() == "" ) {
          z[0] = 1;
          z[1] = "Amount is Empty";
      }

      if($("#id_user_remarks").val() == "" ) {
          z[0] = 1;
          z[1] = "Remarks is Empty";
      }

      return z;
  }

  function numbersOnly(e)
  {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
    // Allow: Ctrl+A, Command+A
    (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
    // Allow: home, end, left, right, down, up
    (e.keyCode >= 35 && e.keyCode <= 40)) 
    {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  }
    
   
});
</script>
@stop
@stop