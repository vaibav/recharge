<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('retailer.sidebar', array('bal' => $user->ubal, 'uname' => $user->user));
       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Network Surplus Details</span>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                    <th style='font-size:12px;padding:7px 8px;'>From Amount </th>
                                    <th style='font-size:12px;padding:7px 8px;'>To Amount</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Surplus Charge</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                  <?php 
                                        
                                        $j = 1;
                                        foreach($surplus as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->net_tr_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;display:none;'>".$f->net_code."</td>";

                                          foreach($network as $r)
                                          {
                                              if($r->net_code == $f->net_code)
                                                  echo "<td style='font-size:12px;padding:7px 8px;'>".$r->net_name."</td>";
                                          }
                                          
                                         
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->from_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->to_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->surplus_charge."</td>";
                                        
                                          echo "</tr>";
                                        
                                          $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('common.bottom')

    

    <script>
     $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

          
      });
    </script>
    </body>
</html>
