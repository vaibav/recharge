<!DOCTYPE html>
<html lang="en">

<head>
    @include('common.top_1')
    <style>
        .input-field input[type=text] {
            margin-top: .4rem;
                padding-left: 1rem;
                border: none;
                border-radius: .3rem;
                background-color: #eceff1;
            }
            
        .input-field input:focus + label { color: #5D6D7E; }
        .row .input-field input:focus { border: 0px solid #5D6D7E; box-shadow: 1px 1px 1px #5D6D7E; }
        .input-field input[type=text].valid { border: 1px solid #e3e6f0; box-shadow: 0 1px 0 0 #e3e6f0; }
        .input-field>label:not(.label-icon).active {
            -webkit-transform: translateY(-24px) scale(0.8);
            transform: translateY(-24px) scale(1);
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
        } 
        .sm-box {width:100%;height:65px;padding:4px 5px;margin-top:-50px;}
      .shadow {
        border: 1px solid #e3e6f0;
        box-shadow: 0 0.15rem 1.75rem 0 rgba(58,59,69,.15);
        -webkit-box-shadow: 0 0.15rem 1.75rem 0 rgba(58,59,69,.15);
      }
      .c-pink { background-image: linear-gradient(to right top, #845ec2, #9c5dc0, #b15dbc, #c55cb7, #d65db1); }
      .m-0 {margin:0px 0px;} .m-1 {margin:5px 8px;} .p-1 {padding:2px 5px;} .f-1 {font-size:12px;} .mt-1 {margin-top:5px;}
      .pb-1 {padding-bottom:5px;} .br-3 {border-radius:15px;} .br-2 {border-radius:10px;}
    </style>

</head>

<body>
<!-- loader-->
<div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
    <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
        <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
            <div class="circle"></div>
            </div><div class="gap-patch">
            <div class="circle"></div>
            </div><div class="circle-clipper right">
            <div class="circle"></div>
            </div>
        </div>
    </div>
</div>

<?php $ax = ['active', '', '', '', '','','','','','','','','','','',''];  $bx = ['active', '', '', '', '','']; ?>

@include('retailer.menu', array('bal' => $user->ubal, 'uname' => $user->user, 'act' => $ax, 'act1' => $bx))

<div id = "main">
<!-- Page Layout here -->
<div class="row">
<div class="content-wrapper-before title-pink"></div>
<div class="col s12">
<div class="container-fluid">

    <div class="row " style="margin-bottom: 5px;">
        <div class="col s12 m12 l7 xl7" >
                <div class="col s4 m4 l2 xl2  white-text text-darken-4" style="font-size: 16px; font-weight:600;padding: 3px; ">
                    News :
                </div>
                <div class="col s8 m8 l10 xl10 white-text text-darken-4" style="font-size: 16px; font-weight:500; padding: 3px; ">
                    <?php 
                        $stx = "";
                        foreach($offer as $r)
                        {
                            $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                        }
                    ?>
                        <marquee direction="left"><?php echo $stx; ?> </marquee>
                </div>
        </div>
        
        <div class="col s12 m12 l5 xl5">
            <div class="col s9 m9 l5 xl5 white-text text-darken-4" style="font-size: 16px; font-weight:600; padding: 3px; ">
                Inactive Networks :
            </div>
            <div class="col s3 m3 l7 xl7 white-text text-darken-4" style="font-size: 16px; font-weight:500; padding: 3px; ">
                <?php 
                    $sty = "";
                    //print_r($in_active);
                    foreach($in_active as $r)
                    {
                        $sty = $sty . $r->net_name. " &nbsp;&nbsp;";
                    }
                ?>
                    <marquee direction="left"><?php echo $sty; ?> </marquee>
            </div>
        </div>
    </div>

    <div style = "margin:10px 10px;">      
        <div   style = "border-radius:4px;padding-top:8px;background:white;">
            
            <div class="row" style="margin-bottom: 10px;">
                <div class="col s12 m12 l12 x12">
                    <!-- Form Starts-->
                    <form id="rech_form" class="form-horizontal" action="{{url('recharge_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">
                        <input type = "hidden" id = "id_ebb" value ='0'>
                        
                        <div class="row" style = "margin-bottom:10px;">
                            <div class="col s12 m12 l12 xl12" style="margin: 3px 0px;">

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="col s12 m12 l12 xl12" style="margin: 3px 0px;">
                                        <!--
                                        <ul class="tabs">
                                            <li class="tab col s3 m3 l3 xl3 #1e88e5 #1a237e indigo darken-4"><a href="#" id="re_pre" class="active white-text">PREPAID</a></li>
                                            <li class="tab col s3 m3 l3 xl3 #1e88e5 #1a237e indigo darken-4"><a href="#" id="re_dth" class="white-text">DTH</a></li>
                                            <li class="tab col s3 m3 l3 xl3 #1e88e5 #1a237e indigo darken-4"><a href="#" id="re_pos" class="white-text">POSTPAID</a></li>
                                            <li class="tab col s3 m3 l3 xl3 #1e88e5 #1a237e indigo darken-4"><a href="#" id="re_eb" class="white-text">EBBILL</a></li>
                                        </ul>

                                        -->
                                        <div class="row" style = "margin-bottom:5px;">
                                            <div class="col s12 m12 l6 xl6 left-align" style="margin: 3px 0px;">
                                            <a href="#" id="re_pre" class="chip title-pink white-text" >PREPAID</a></li>
                                            <a href="#" id="re_dth" class="chip title-pink white-text" style = "font-size:10px;">DTH</a></li>
                                            <a href="#" id="re_pos" class="chip title-pink white-text" style = "font-size:10px;">POSTPAID</a></li>
                                            <a href="#" id="re_eb" class="chip title-pink white-text" style = "font-size:10px;">EBBILL</a></li>
                                            </div>
                                            <div class="col s12 m12 l4 xl4">
                                                <p class = "black-text" style = "font-size:30px;font-weight:bold;margin:5px 5px;" id = "r_mob"></p>
                                            </div>
                                            <div class="col s12 m12 l2 xl2">
                                                <p class = "pink-text" style = "font-size:32px;font-weight:bold;margin:5px 5px;" id = "r_amt"></p>
                                            </div>
                                        </div>

                                        <!-- Form Objects -->
                                        <div class="row" style = "margin-bottom:5px;">
                                            <div class="input-field col s12 m12 l12 xl12" style="margin: 10px 0px;display:none;">
                                                <select id="id_net_type_code" name="net_type_code" >
                                                    <?php
                                                        foreach($networktype as $f)
                                                        {
                                                            echo "<option value='".$f->net_type_code."'>".$f->net_type_name."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="input-field col s10 m10 l3 xl3" style="margin: 10px 0px;">
                                                <input id="id_user_mobile" type="text" name="user_mobile" placeholder = "Mobile No">
                                                <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
                                            </div>

                                            <div class="input-field col s10 m10 l3 xl3" style="margin: 10px 0px;">
                                                <select id="id_net_code" name="net_code" >
                                                    <?php
                                                        foreach($network as $f)
                                                        {
                                                            echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                                        }
                                                    ?>
                                                </select>
                                            
                                            </div>

                                            <div class="input-field col s12 m12 l2 xl2" style="margin: 10px 0px;">
                                                <input id="id_user_amount" type="text" name="user_amount" placeholder = "Amount">
                                                <span class="text-danger">{{ $errors->first('user_amount') }}</span>
                                            </div>

                                            <div class="input-field col s2 m2 l4 xl4" style="margin: 10px 0px;">
                                                <div class="input-field col s12 m12 l7 xl7" style="margin: 10px 0px;">
                                                    <button class="btn waves-effect waves-light #ff6f00 blue darken-4 " type="submit"  id="btn_submit" name="btn_submit">Recharge
                                                        <i class="material-icons right">send</i>
                                                    </button>
                                                </div>
                                                <div class="input-field col s12 m12 l5 xl5 right-align" style="margin: 10px 0px;">
                                                    <button class="btn-floating text-center #7b1fa2 purple darken-2" id="plan_121">121</button>
                                                    <button class="btn-floating text-center #7b1fa2 purple darken-2" id="plan_mob">plan</button>
                                                </div>
                                                
                                            </div>


                                        </div>

                        
                                    </div>
                                </div>

                   
                                <!-- End Form Objects -->
                            </div>

                            
                        </div>

                        <div class ="row">
                            <div class="col s12 m12 l5 xl5 "  id="style-1" >

                                <div id="tbl_body">
                                </div>

                            </div>
                            <div class="col s12 m12 l7 xl7 "  id="rech_offer_body">

                              

                            </div>
                        </div>

                    </form>

                    <!-- End Form-->
                </div>
                    
            </div>
            
            
            
        </div>
    </div>
</div>
</div>
</div>
    <!-- End Page Layout  -->

</div>
<!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
            
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
</div>

<!-- page-wrapper -->
@include('common.bottom_1')

<?php
    $net_code = 0;
    $mob = 0;
    $net_type = 0;
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 11)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 30)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 31)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        else if($op == 20)
        {
            $net_code = $op1['net_code'];
            $net_type = $op1['net_type'];
            $mob = $op1['mob'];

            echo "<script>
            $(document).ready(function() 
            {
                $('#rech_offer_body').html('".$res."');
                $('.tabs').tabs();
                //$('.modal').modal();
                //$('.modal').modal('open');

                $('#id_ebb').val('2');
                
                $('#id_net_type_code').val(".$net_type.");
                $('#id_net_code').val(".$net_code.");
                $('#id_user_mobile').val(".$mob.");
                //$('#id_user_mobile').addClass('active');
                $('#r_mob').text('".$mob."');

                $(document).on('click', 'a', function(e){
                    
                    var amt = $(this).text();
                    var id = this.id;
                    if(id == 'id_off')
                    {
                        $('#id_user_amount').val(amt);
                        $('#r_amt').text(amt);
                    }
                });

                $('input[name=group1]').change(function (e) {
                    var amt = $(this).val();
                    $('#id_user_amount').val(amt);
                    //$('#id_user_amount').addClass('active');
                    $('#id_amt').text(amt);
                    M.updateTextFields();
                });

                M.updateTextFields();

            });
            </script>";
        }
        else if($op == 22)
        {
            

            echo "<script>
            $(document).ready(function() 
            {
                $('#rech_offer_body').html('".$res."');
                
                $(document).on('click', 'a', function(e){
                    
                    var amt = $(this).text();
                    var id = this.id;
                    if(id == 'id_off')
                    {
                        $('#id_user_amount').val(amt);
                    }
                });
                
                M.updateTextFields();

            });
            </script>";
        }
        else if($op == 23)
        {
            $net_code = $op1['net_code'];
            $net_type = $op1['net_type'];
            $mob = $op1['mob'];

            if($res != "")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    //alert('".$res."');
                    var d = 1;
                    $('#id_user_mobile').val(".$mob.");
                    $('#r_mob').text('".$mob."');
                    

                    $('#id_net_code option').each(function() {
                        if($(this).text() == '" . $res . "') {
                            $(this).attr('selected', 'selected');  
                            d = 2;          
                        }                        
                    });

                    if(d == 1)
                    {
                        $('#id_net_code option:contains(" . $res . ")').attr('selected', 'selected');
                    }
                    
                    M.updateTextFields();

                    

                });
                </script>";
            }
            
        }
        
        session()->forget('result');
    }
?>


<script>
    $(document).ready(function() 
    {
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();
        $('.tabs').tabs();
        $('#style-1').perfectScrollbar();

        $('#id_net_type_code, #id_net_code').on('contentChanged', function() {
            $(this).material_select();
        });

        var c = 1;
        var opt = 1;
        $('#loader').hide();
        load_data();
        loadPrepaid();
        
        window.setInterval(function(){
            /// call your function here
            //load_data();
        }, 19000);

        

        function load_data()
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            jQuery.ajax({
                url:'userresult_data',
                type: 'GET',
                success: function( data ){

                    $('#tbl_body').html(data);
                    var d = new Date();
                    $('#tim').html(d.toLocaleString());
                    if(c == 1)
                    {
                        $('#tim').css("background-color", "#EC7063");
                        c = 2;
                    }
                    else if(c == 2)
                    {
                        $('#tim').css("background-color", "#27AE60");
                        c = 1;
                    }
                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function loadPrepaid()
        {
            var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
            getNetworkData(ty);
        }

        $('#re_pre').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
            getNetworkData(ty);
            $("#plan_121").html('121');
            $("#plan_mob").show();
            opt = 1;
        });

        $('#re_dth').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('DTH')").attr('selected', true).val();
            getNetworkData(ty);
            $("#plan_121").html('Info');
            $("#plan_mob").hide();
            opt = 2;
        });

        $('#re_pos').on('click',function(e)
        {
            e.preventDefault();
            var ty = $("#id_net_type_code option:contains('POSTPAID')").attr('selected', true).val();
            getNetworkData(ty);
            opt = 3;
        });

            $('#re_eb').on('click',function(e)
        {
            e.preventDefault();
            window.open("<?php echo url('/'); ?>/recharge_bill_final","_self");
        });

        $("#id_net_type_code").change(function(e)
        {
            var ty = $('option:selected', this).val();
            var ax = $('option:selected', this).text();
            $('#loader').show();
            if(ty != "-")
            {
                getNetworkData(ty);
                if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
                {
                    $('#rc1').show();
                    $('#eb1').hide();
                    $('#eb2').hide();
                    $('#eb3').hide();

                    if(ax == "PREPAID")
                        $("#plan_121").html('121');
                    else if(ax == "DTH")
                        $("#plan_121").html('Info');
                }
                
                

            }
                
        });

        function getNetworkData(net_type)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $.ajax({
                url:'network_data/' + net_type,
                type: 'GET',
                dataType: "json",
                success: function( response ){
                    var zz=response['data'];
                    $('#id_net_code').empty();
                    for(var i=0;i<zz.length;i++)
                    {
                        if(zz[i][0]!='')
                        {
                            var newOpt = $('<option>').attr('value',zz[i][0]).text(zz[i][1]);
                            $('#id_net_code').append(newOpt);
                        }
                    }
                    $('#id_net_code').formSelect();
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                }
            });
        }

        $("#id_user_mobile, #id_eb_cons_mobile").keydown(function (e) 
        {
            numbersExactly(e);
        });
        $("#id_user_amount, #id_eb_cons_amount").keydown(function (e) 
        {
            numbersOnly(e);
        });

        $("#id_user_mobile").keyup(function (e) 
        {
            var mob = $("#id_user_mobile").val();
            if(opt == 1)
            {
                if(mob.length == 10)
                {
                    //alert("valid mobile no");
                    /*$('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_mobile_check')}}");
                    $('#rech_form').submit();*/
                    getOperatorCheck(mob)
                }
            }
            $("#r_mob").text(mob);
        });

        $("#id_user_amount").keyup(function (e) 
        {
            var mob = $("#id_user_amount").val();
            
            $("#r_amt").text(mob);
        });

                    
        function getOperatorCheck(mob)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $('#loader').show(); 

            $.ajax({
                url:'rech_operator?user_mobile=' + mob,
                type: 'GET',
                success: function( response ){
                    var opr = response;
                    //alert(opr);
                    var d = 1;
                    if(opr != "" && opr != "0")
                    {
                        
                        $('#id_net_code option').each(function() {
                            if($(this).text() == opr) {
                                $(this).attr('selected', 'selected');  
                                d = 2;          
                            }                        
                        });

                        if(d == 1)
                        {
                            if(hasWhiteSpace(opr))
                            {
                                var opr1 = opr.split(' ');
                                $('#id_net_code option').each(function() {
                                    if($(this).text().indexOf(opr1[0]) !== -1) {
                                        $(this).attr('selected', 'selected');  
                                        d = 2;  
                                            
                                    }                        
                                });
                            }
                        }
                        
                        
                    }
                    $('#id_net_code').formSelect();
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                }
            });
        }


        
            

        function hasWhiteSpace(s) {
            return s.indexOf(' ') >= 0;
        }


        $('#btn_submit').on('click',function(e)
        {
            e.preventDefault();
            
            $('#btn_submit').prop('disabled', true);
            var net = $('option:selected', '#id_net_code').val();
            var net1 = $('option:selected', '#id_net_code').text();
            var mob = $('#id_user_mobile').val();
            var amt = $('#id_user_amount').val();
            
            if(net != "" && mob != "" && amt != "")
            {
                var form = $(this).parents('form');
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">NETWORK</th><td style="padding:4px 8px;">' + net1 + '</td></tr>'
                                                + '<tr><th style="padding:4px 8px;">Mobile No</th><td style="padding:4px 8px;">' + mob + '</td></tr>'  
                                                + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + amt + '</td></tr></table>',
                                                
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Recharge!'
                    }).then((result) => {
                    if (result.value) {
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_store')}}");
                        $('#rech_form').submit();
                    }
                    else
                    {
                        swal("Cancelled", "No Recharge...", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                });
                
            }
            else if(mob == "")
            {
                swal("Cancelled", "Mobile No is Empty...", "error");
                $('#btn_submit').prop('disabled', false);
            }
            else if(amt == "")
            {
                swal("Cancelled", "Amount is Empty...", "error");
                $('#btn_submit').prop('disabled', false);
            }

            
            
            
        }); 

        $('#plan_121').on('click',function(e)
        {
            e.preventDefault();
            var ax = $('option:selected', '#id_net_type_code').text();
            if(ax == "PREPAID")
            {
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_user_mobile').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    $('#plan_121').prop('disabled', true);
                   /* $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers1')}}");
                    $('#rech_form').submit();*/

                    $('#loader').show();
                    $.ajax({
                        url:'rech_offer/?net_code=' + net_code +"&user_mobile=" +usr_mobe,
                        type: 'GET',
                        success: function( response ){
                           
                            $('#rech_offer_body').html(response);
                            $('#loader').hide(); 
                            $('#plan_121').prop('disabled', false);
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            $('#loader').hide();
                            $('#plan_121').prop('disabled', false);
                        }
                    });
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }
            }
            else if(ax == "DTH")
            {
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_user_mobile').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    /*$('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('dth_offers')}}");
                    $('#rech_form').submit();*/
                    getDthInfo(usr_mobe, net_code);
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }
            }
            
        });

        $('#plan_mob').on('click',function(e)
        {
            e.preventDefault();
            var ax = $('option:selected', '#id_net_type_code').text();
            if(ax == "PREPAID")
            {
                var net_code = $('#id_net_code').val();
                //alert(net_code);
                if(net_code != "" )
                {
                    $('#loader').show();
                    $.ajax({
                        url:'rech_plan?net_code=' + net_code,
                        type: 'GET',
                        success: function( response ){
                           
                            $('#rech_offer_body').html(response);
                            $('#loader').hide(); 
                            $('.tabs').tabs();
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            $('#loader').hide();
                        }
                    });
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
            }
            
            
        });

        function getDthInfo(mob, network)
        {
            
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $('#loader').show(); 

            $.ajax({
                url:'rech_dth/?net_code=' + network +"&user_mobile=" +mob,
                type: 'GET',
                success: function( response ){
                    $('#rech_offer_body').html(response);
                    $('#loader').hide(); 
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                    $('#loader').hide(); 
                }
            });
        }

        
        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("bill1_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Get Bill!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                
                }

                var nid1=gid.split("bill2_");
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Get Bill!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                
                }

        });
            
        function numbersOnly(e)
        {
            
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
            // let it happen, don't do anything
            return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            }
        }

        function numbersExactly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
                    // let it happen, don't do anything
                    return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
    });
</script>
</body>
</html>