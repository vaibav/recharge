<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

    @include('user.sidebar_x_retailer')

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('money-dashboard')}}">
            <i class="large material-icons">arrow_back</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
        
        <br>
    
        <div class="row " >
            <div class="col s12 m12 l12 xl12 " >
                <div class = "card1 shadow m-36">
                    <form id = "rech_form" class="user" action="{{url('beneficiary-store')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" id="csrftk" name="_token" value="{{ csrf_token() }}">
                
                    <input type="hidden" id = "mobileNo"  name="mobileNo" value="<?php echo $mobileNo; ?>">
                    <input type="hidden" id = "firstName" name="firstName" value="<?php echo $remitter['name']; ?>">
                    <input type="hidden" id = "remId"     name="remId" value="<?php echo $remitter['id']; ?>">
                    <br>

                    <div class="row" style="margin:2px 2px;">
                        <div class="col s6 m6 l6 xl6" style="margin:2px 10px;padding:1px 1px;">
                            <label class="title-con" style="margin-top:20px;">Beneficiary Entry</label>
                        </div>
                        <div class="col s5 m5 l5 xl5 right-align" style="margin:2px 2px;padding:1px 1px;">
                            
                        </div>
                    </div>

                    <div class="row" style="margin:2px 2px;">
                        <div class="col s12 m12 l12 xl12 center-align" style="margin:2px 10px;padding:1px 1px;">
                             <a class="btn-floating btn-small waves-effect waves-light blue">A</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">B</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">C</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">D</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">E</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">F</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">G</a>

                             <a class="btn-floating btn-small waves-effect waves-light green">H</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">I</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">J</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">K</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">L</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">M</a>
                             <a class="btn-floating btn-small waves-effect waves-light green">N</a>

                             <a class="btn-floating btn-small waves-effect waves-light pink">O</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">P</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">Q</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">R</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">S</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">T</a>
                             <a class="btn-floating btn-small waves-effect waves-light pink">U</a>

                             <a class="btn-floating btn-small waves-effect waves-light blue">V</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">W</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">X</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">Y</a>
                             <a class="btn-floating btn-small waves-effect waves-light blue">Z</a>
                        </div>
                      
                    </div>
                    

                    
                    <div class="row" style="margin-top:20px;margin:5px 2px;">
                        <div class="input-field col s12 m12 l6 xl6" >
                            <select id="bank_code" name = "bank_code" style = "padding:6px 6px;">
                                    <option value="">Select Bank</option>

                                    <?php 
                                        foreach($bank as $d){
                                            echo "<option value =".$d['bankCode'].">".$d['bankName']."</option>";
                                        }
                                    ?>
                                    <!--
                                    <option value="ANDB">Andhra Bank</option>
                                    <option value="UTIB">Axis Bank</option>
                                    <option value="BARB">Bank of Baroda</option>
                                    <option value="BKID">Bank of India</option>
                                    <option value="CNRB">Canara Bank</option>
                                    <option value="CSBK">Catholic Syrian Bank</option>
                                    <option value="CBIN">Central Bank of India</option>
                                    <option value="CITI">Citi Bank</option>
                                    <option value="CCBN">Citizen Co-Op Bank</option>
                                    <option value="CIUB">City Union Bank</option>
                                    <option value="CORP">Corporation Bank</option>
                                    <option value="DLXB">Dhanlaxmi Bank</option>

                                    <option value="FDRL">Federal Bank</option>
                                    <option value="FINO">FINO Payment Bank</option>
                                    <option value="HDFC">HDFC Bank</option>
                                    <option value="HSBC">HSBC Bank</option>

                                    <option value="ICIC">ICICI Bank</option>
                                    <option value="IBKL">IDBI Bank</option>
                                    <option value="IDFB">IDFC Bank</option>
                                    <option value="IDIB">Indian Bank</option>
                                    <option value="IOBA">Indian Overseas Bank</option>
                                    <option value="INDB">IndusInd Bank</option>
    
                                    <option value="KVBL">Karur Vysya Bank</option>
                                    <option value="KKBK">Kotak Mahindra Bank</option>
                                    <option value="KKBN">Lakshmi Vilas Bank</option>
                                    <option value="BKID">National Co-Operative Bank</option>
                                    <option value="ORBC">Oriental Bank of Commerce</option>
                                    <option value="PYTM">PAYTM PAYMENT BANK</option>
                                    <option value="PUNB">Punjab National Bank</option>

                                    <option value="SIBL">South Indian Bank</option>
                                    <option value="SCBL">Standard Chartered Bank</option>
                                    <option value="SBIN">State Bank of India</option>
                                    <option value="SYNB">Syndicate Bank</option>
                                    <option value="TMBL">Tamilnad Mercantile Bank</option>
                                    <option value="TNSC">The Tamilnadu State Apex Cooperative Bank</option>

                                    <option value="UCBA">UCO Bank</option>
                                    <option value="UBIN">Union Bank of India</option>
                                    <option value="UTBI">United Bank of India</option>
                                    <option value="VIJB">Vijaya Bank</option>
                                    <option value="YESB">Yes Bank</option>
                                -->
                                </select>  
                                    
                        </div>
        
                        <div class="input-field col s12 m12 l4 xl4" >
                          
                        </div>
                        <div class="input-field col s12 m12 l2 xl2" >
                            
                        </div>
                    </div>

                    <div class="row" style="margin:5px 2px;">
                           
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Account No" id="ben_acc_no" name = "ben_acc_no" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('ben_acc_no') }}</span>
                            </div>

                             <div class="input-field col s12 m12 l6 xl6" >
                                 
                            </div>
                    </div>

                    <div class="row" style="margin:5px 2px;">
                           
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="IFSC Code (It is Must)" id="bank_ifsc"  name = "bank_ifsc" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('bank_ifsc') }}</span>   
                            </div>

                             <div class="input-field col s12 m12 l6 xl6" >
                                 <button class="btn waves-effect waves-light orange"  id="btn_verify">Verify
                                    <i class="material-icons right">send</i>
                                </button>
                            </div>
                    </div>

                    <div class="row" style="margin:5px 2px;">
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Beneficiary Name" id="ben_acc_name"  name = "ben_acc_name" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('ben_acc_name') }}</span>    
                            </div>
            
                           
                    </div>
        
                    
        
                    <div class="row" style="margin:5px 2px;">
                        <div class="input-field col s12 m12 l12 xl12 right-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit">submit</button>
                        </div>
                    </div>
            
                    </form>
                </div>
               
            </div>
    
    
            
        </div>
    </div>
    

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP</h4>
            <p id = "tr_id"></p>
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="OTP" id="cus_otp" name="cus_otp" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_otp">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
   @include('user.bottom_x')

    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
       
        //$ben_acc_no = $op1['ben_acc_no'];
        if($op == 0)
        {
           echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success');
                window.location.href = '".url('/'). "/money-dashboard';
               
            });
            </script>";
            
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success');
                window.location.href = '".url('/'). "/money-dashboard';
               
            });
            </script>";
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error');
               
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var x = 0;

            $("#ben_acc_no").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_view').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('ben_view')}}", "_self");
            });

            $("#bank_code").change(function(e)
            {
                var ty = $('option:selected', this).val();
                var ax = $('option:selected', this).text();
                if(ty != "-")
                {
                    $.ajax({
                        url:'get-bank-ifsc/' + ty,
                        type: 'GET',
                        success: function( response ){
                           if(response != "-"){
                                $("#bank_ifsc").val(response);
                           }
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        }
                    });
                }
                    
            });

            $('.btn-floating ').on("click",function(){
              var btnText =  $(this).text();
              if(btnText.length == 1)
              {
                 $.ajax({
                        url:'get-bank-name/' + btnText,
                        type: 'GET',
                        success: function( response ){
                           if(response != "-"){
                                //alert(response);
                                $('#bank_code').empty();
                                $('#bank_code').append(response);
                                $('select').formSelect();
                           }
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        }
                    });
              }
            });

            $('#btn_verify').on('click',function(e)
            {
                e.preventDefault();

                var acc_no = $("#ben_acc_no").val();
                var b_ifsc = $("#bank_ifsc").val();
                var b_code = $('option:selected', "#bank_code").val();
                var age_id = $("#agent_id").val();
                var msisdn = $("#mobileNo").val();
                var csrftk = $("#csrftk").val();

                
                if(acc_no == "")
                {
                    swal("Cancelled", "Error! Account No is Empty....", "error");
                }
                else if(b_code == "-")
                {
                    swal("Cancelled", "Error! Please Select Bank....", "error");
                }
                else if(msisdn == "-")
                {
                    swal("Cancelled", "Error! Please Select Customer....", "error");
                }
                else
                {
                    //alert(acc_no + "--" + b_code + "---" + age_id + "---"+ msisdn);
                    $('#btn_verify').prop('disabled', true);
                    swal({
                            title: 'Are you sure? (Rs.5 deducted from your Account)',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Verify!'
                            }).then((result) => {
                            if (result.value) {
                                $('#loader').show();
                                $.ajax({
                                    url: '<?php echo url('/'); ?>/beneficiary-check',
                                    type: 'get',
                                    data: {ben_acc_no:acc_no, bank_code:b_code, agent_msisdn:msisdn, bank_ifsc:b_ifsc },
                                    dataType: 'JSON',
                                    success: function (data) { 
                                        if(data.status == 1)
                                        {
                                            swal('Alert!', data.message, 'success');
                                            $("#ben_acc_name").val(data.ben_name);
                                            M.updateTextFields();
                                        }
                                        else if(data.status == 2)
                                        {
                                            swal("Cancelled", data.message, "error");
                                        }
                                        $('#loader').hide();
                                        $('#btn_verify').prop('disabled', false);
                                    },
                                    error: function(data, mXg) { 
                                        $('#loader').hide();
                                        console.log(data);
                                    }
                                }); 
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                $('#btn_verify').prop('disabled', false);
                            }
                        });
                   
                }

                

            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var bank = $('option:selected', '#bank_code').val();
                var ifsc = $('#bank_ifsc').val();
                var z = checkBankIfsc(bank, ifsc);

                if(z == 1)
                {
                    $('#btn_submit').prop('disabled', true);
                    swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Add!'
                            }).then((result) => {
                            if (result.value) {
                                $('#loader').show();
                                $('#rech_form').attr('action', "{{url('beneficiary-store')}}");
                                $('#rech_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                $('#btn_submit').prop('disabled', false);
                            }
                        });
                }
                else if(z == 2)
                {
                    swal("Cancelled", "You Must Enter Any one... Either Bank or IFSC code", "error");
                }
                else
                {
                    swal("Cancelled", "Dont Enter Both Bank and IFSC code! \n Select Bank Or Enter Ifsc Code", "error");
                }
                   
            }); 

             $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                var otp = $('#cus_otp').val();
                var bid = $('#ben_id').val();
                var tid = $('#trans_id').val();
                var msi = $('#c_msisdn').val();
                $('#btn_otp').prop('disabled', true);
                
                //alert(otp + "---" + bid + "----" + tid + "----" + msi);
                if(otp == "") {
                    swal("Cancelled", "Please Enter OTP", "error");
                    $('#btn_otp').prop('disabled', false);
                }
                else if(otp != "" && bid != "" && tid != "") 
                {
                    $('#loader').show();
                    window.location.href = "<?php echo url('/'); ?>/ben_otp_entry_store/" + bid + "/" + msi + "/" + tid + "/" + otp;
                    
                }
                   
            }); 

            function checkBankIfsc(bank, ifsc)
            {
                var z = 1;

                if(ifsc == "")
                {
                    z = 0;
                }

                return z;
            }

            
            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
        
        });
    </script>
</body>
</html>