<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
                <div class="card " style = "margin-top:60px">
                    <div class="card-image">
                        <span class="card-title" style = "padding:12px;">Collection Report</span>
                        <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('collection_report')}}" ><i class="Small material-icons">arrow_back</i></a>
                    </div>
                    
                    <div class="card-content white darken-1" style = "border-radius:4px;">
                    
                        <!-- Page Body --> 
                        <div class = "row">
                            <div class ="col s12 m12 l11 xl11">
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                        <tr>
                                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                            <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Paid Amount</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Paid Date</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                            <th style='font-size:12px;padding:7px 8px;'>DD/Cheque No</th> 
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                        <?php 
                                            $j = 1;
                                            $pay_tot = 0;
                                            foreach($collection as $f)
                                            {
                                                echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_option."</td>";
                                                echo "</tr>";
                                                         
                                                $pay_tot = floatval($pay_tot) + floatval($f->pay_amount);
                                                $j++;
                                            }

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>TOTAL</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$pay_tot."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "</tr>";
                                        ?>
                                    </tbody>
                                </table>
                            
                            </div>
                        </div>
                        <!-- End Body --> 
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

     

    <script>
     $(document).ready(function() 
	 {
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        
       
      });
    </script>
    </body>
</html>
