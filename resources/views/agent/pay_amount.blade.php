<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
                <div class="card " style = "margin-top:60px">
                    <div class="card-image">
                        <span class="card-title" style = "padding:12px;">Collection Payment</span>
                        
                    </div>
                    
                    <div class="card-content white darken-1" style = "border-radius:4px;">
                    
                        <?php 
                                
                            $de_amt = $user_details['de_tot'];
                            $ce_amt = $user_details['ce_tot'];
                            $pe_amt = $user_details['pe_tot'];
                            $u_name = $user_details['user_name'];
                            $c_details = $user_details['c_details'];
                        ?>

                        <!-- Page Body --> 
                        <div class = "row">
                            <div class ="col s12 m12 l5 xl5">
                            <form id= "rech_form" class="form-horizontal" action="{{url('pay_agent_store')}}" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                          <select id="id_user_name"  name = "user_name" >
                                              <option value = '-'>----Select----</option>
                                            <?php
                                                $str = "";
                                                foreach($user1 as $f)
                                                {
                                                    if(strtolower($f[0]) == strtolower($u_name))
                                                    {
                                                        $str = $str."<option value='".$f[0]."' selected>".$f[0]."--".$f[1]."</option>";
                                                    }
                                                    else
                                                    {
                                                        $str = $str."<option value='".$f[0]."'>".$f[0]."--".$f[1]."</option>";
                                                    }
                                                    
                                                }
                                                echo $str;
                                            ?>
                                            </select>
                                          <label>User Name</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <input type="text" id="id_pay_amt" name="pay_amt" class="validate" placeholder = "Paid Amount">
                                        
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <input type="text" id="id_pay_date" name="pay_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <select id="id_pay_mode"  name = "pay_mode" >
                                              <option value = '-'>----Select Pay Mode----</option>
                                              <option value = 'CASH'>CASH</option>
                                              <option value = 'CHEQUE'>CHEQUE</option>
                                              <option value = 'D.D'>D.D</option>
                                              <option value = 'IMPS-RDGS-NEFT'>IMPS/RDGS/NEFT</option>
                                        </select>
                                       
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <input type="text" id="id_pay_option" name="pay_option" class="validate" placeholder= "DD/CHEQUE NO/IMPS/NEFT TRAN ID">
                                        
                                    </div>
                                </div>

                                <div class="file-field input-field col s12 m12 l11 xl11" style="margin: 3px 0px;padding:5px 5px;">
                                    <div class="btn">
                                        <span>Image Upload</span>
                                        <input type="file" id="id_pay_image" name="pay_image"> 
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder = "Image(<200KB)">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s4 m4 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s4 m4 l2 xl2">
                                        <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            
                                    </div>
                                        
                                </div>
                            </form>
                            </div>

                            

                            <div class ="col s12 m12 l7 xl7">
                         
                                <div class = "row" style = "margin-bottom:2px">
                                    <div class ="col s12 m12 l10 xl10" >
                                        <table class="bordered striped responsive-table ">
                                            <tr>
                                                <td style='font-size:12px;padding:7px 8px;'>USER NAME</td>
                                                <th style='font-size:12px;padding:7px 8px;'>{{$u_name}}</th>
                                                <td style='font-size:12px;padding:7px 8px;'></td>
                                                <th style='font-size:12px;padding:7px 8px;'></th>
                                                <td style='font-size:12px;padding:7px 8px;'></td>
                                                <th style='font-size:12px;padding:7px 8px;'></th>
                                            </tr>
                                            <tr>
                                                <td style='font-size:12px;padding:7px 8px;'>DEBIT(taken)</td>
                                                <th style='font-size:12px;padding:7px 8px;'>{{$de_amt}}</th>
                                                <td style='font-size:12px;padding:7px 8px;'>CREDIT(paid)</td>
                                                <th style='font-size:12px;padding:7px 8px;'>{{$ce_amt}}</th>
                                                <td style='font-size:12px;padding:7px 8px;'>BALANCE</td>
                                                <th style='font-size:12px;padding:7px 8px;'>{{$pe_amt}}</th>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                                <br>

                                <!-- Page Body --> 
                                <div class = "row" style = "margin-bottom:2px">
                                    <div class ="col s12 m12 l12 xl12" style = "height:300px;overflow-y:scroll;">
                                        <table class="bordered striped responsive-table ">
                                            <thead>
                                                <tr>
                                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Bill No</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Debit (taken)</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Credit (Paid)</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Balance</th>
                                                   
                                                    
                                            </tr>
                                            </thead>
                                            <tbody id="tbl_body" >
                                            <?php 
                                                $j = 1;

                                                $debit = 0;
                                                $credit = 0;
                                                $balance = 0;
                                                $status = "";

                                                foreach($c_details as $f)
                                                {
                                                    
                                                    if($f->pay_type == "C" )
                                                    {
                                                        // Debit -
                                                        if ($f->pay_status == "1") {
                                                            $debit = floatval($debit) + floatval($f->pay_amount);
                                                            $balance = floatval($balance) - floatval($f->pay_amount);
                                                            $status = "SUCCESS";
                                                        }
                                                        else if ($f->pay_status == "2") {
                                                            $status = "FAILURE";
                                                        }
                                                        else if ($f->pay_status == "0") {
                                                            $status = "PENDING";
                                                        }

                                                        echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->bill_no."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$status."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".abs($balance)."</td>";
                                                        echo "</tr>";
                                                        
                                                    }
                                                    else if($f->pay_type == "D" )
                                                    {
                                                        // Credit +
                                                        if ($f->pay_status == "1") {
                                                            $credit = floatval($credit) + floatval($f->pay_amount);
                                                            $balance = floatval($balance) + floatval($f->pay_amount);
                                                            $status = "SUCCESS";
                                                        }
                                                        else if ($f->pay_status == "2") {
                                                            $status = "FAILURE";
                                                        }
                                                        else if ($f->pay_status == "0") {
                                                            $status = "PENDING";
                                                        }
                                                        
                                                        echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->bill_no."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$status."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".abs($balance)."</td>";
                                                        echo "</tr>";
                                                    }
                                                
                                                    $j++;
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End Body --> 
                            </div>
                        </div>
                        <!-- End Body --> 
                    
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

     <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

        $("#id_pay_amt").keydown(function (e) 
        {
            numbersOnly(e);
        });

        $("#id_user_name").change(function(e)
        {
            var ty = $('option:selected', this).val();
            var tx = $('option:selected', this).text();
            //$('#loader').show();
            if(ty != "-")
            {
                //getNetworkData(ty);
                //alert(tx + "-----" + ty);
                window.location.href = "<?php echo url('/'); ?>/pay_agent/" + ty;

            }
                
        }); 

        $('#btn_submit').on('click',function(e)
        {
            e.preventDefault();
            var u_nme = $('option:selected', '#id_user_name').val(); 
            var u_amt = $('#id_pay_amt').val();
            var u_dat = $('#id_pay_date').val();
            var u_mod = $('option:selected', '#id_pay_mode').val();
            
            if(u_nme != "-" && u_amt != "" && u_dat != "" && u_mod != "-")
            {
                swal({
                    title: 'Are you sure?',
                    type: 'warning',
                    html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">USER NAME</th><td style="padding:4px 8px;">' + u_nme + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">PAID AMOUNT</th><td style="padding:4px 8px;">' + u_amt + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">DATE</th><td style="padding:4px 8px;">' + u_dat + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">MODE</th><td style="padding:4px 8px;">' + u_mod + '</td></tr></tabel>',
                                                 
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Pay!'
                    }).then((result) => {
                    if (result.value) {
                        $('#btn_submit').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('pay_agent_store')}}");
                        $('#rech_form').submit();
                    }
                    else
                    {
                        swal("Cancelled", "No Allocate...", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                });

                
            }
            else if(u_nme == "-")
            {
                swal("Cancelled", "Please User Name...", "error");
            }
            else if(u_amt == "")
            {
                swal("Cancelled", "Please Enter Amount...", "error");
            }
            else if(u_dat == "")
            {
                swal("Cancelled", "Please Enter Date...", "error");
            }
            else if(u_mod == "-")
            {
                swal("Cancelled", "Please Select Payment Mode...", "error");
            }
            
        });


        function numbersOnly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
            // let it happen, don't do anything
            return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
            }
        }

       
      });
    </script>
    </body>
</html>
