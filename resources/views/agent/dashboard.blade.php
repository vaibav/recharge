<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

       

        <!-- Page Layout here -->
        <div class="row " >
            <div class="col s12 m12 l12 xl12">
                <div class="row" style="margin-bottom: 5px;">
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s4 m4 l2 xl2  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                News :
                            </div>
                            <div class="col s8 m8 l10 xl10 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $stx = "";
                                    foreach($offer as $r)
                                    {
                                        $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $stx; ?> </marquee>
                            </div>
                    </div>
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s9 m9 l4 xl4  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                Inactive Networks :
                            </div>
                            <div class="col s3 m3 l8 xl8 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $sty = "";
                                    //print_r($in_active);
                                    foreach($in_active as $r)
                                    {
                                        $sty = $sty . $r->net_name. " &nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $sty; ?> </marquee>
                            </div>
                    </div>
                </div>

            <div class="card " >
                
               

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <h3>Welcome Agent....</h3>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
       
      });
    </script>
    </body>
</html>
