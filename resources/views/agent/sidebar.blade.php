<!-- Navbar goes here -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="#!">Bal : {{$bal}}</a></li>
    <li><a href="{{url('changepassword')}}">Change Password</a></li>
    <li class="divider"></li>
    <li><a href="{{url('logout')}}">logout</a></li>
</ul>
<div class="navbar-fixed">
    <nav class="#1e88e5 pink darken-1">
        <div class="row">
            <div class="col s6 m6 l3 xl3">
                <a href="#" data-target="slide-out" class="sidenav-trigger" style="display: block;margin: 0 10px;"><i class="material-icons">menu</i></a>
                <img class="responsive-img" src="{{ asset('img/brand/logo.png') }}" style="height: 46px; width: 85px;padding-top: 15px;">
            </div>
            <div class="col s6 m6 l9 xl9 right-align">
                <ul class="right ">
                    <li><a href="badges.html" style="padding: 0 6px;">&#x20B9;{{$bal}}</a></li>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger" href="#!" data-target="dropdown1" style="padding: 0 6px;padding-top: 2px;"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>   
<ul id="slide-out" class="sidenav #1a237e indigo darken-4" >
    <li>
        <div class="user-view">
        <div class="background">
            <img src="{{ asset('img/brand/back.jpg') }}">
        </div>
        <a href="#user"><img class="circle" src="{{ asset('uploadphoto/user.jpg') }}"></a>
        <a href="#name"><span class="white-text name">{{$uname}}</span></a>
        <a href="{{url('dashboard_agent')}}"><span class="white-text name" style = "margin-top:4px;">{{$user->mode}}</span></a>
        </div>
    </li>
    
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">User <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li ><a href="{{url('user_one_agent')}}" class = "white-text">User Details</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Collection <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
              <li><a href="{{url('pay_agent')}}" class = "white-text">collection</a></li>
            <li><a href="{{url('collection_details')}}" class = "white-text">collection Details</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    
    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Report <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('collection_report')}}" class = "white-text">collection Report</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>
    
</ul>