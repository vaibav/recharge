<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
                <div class="card " style = "margin-top:60px">
                    <div class="card-image">
                        <span class="card-title" style = "padding:12px;">Collection Report</span>
                        
                    </div>
                    
                    <div class="card-content white darken-1" style = "border-radius:4px;">
                    
                       

                        <!-- Page Body --> 
                        <div class = "row">
                            <div class ="col s12 m12 l5 xl5">
                            <form id= "rech_form" class="form-horizontal" action="{{url('collection_report_view')}}" method="get">
                                
                                
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <input type="text" id="id_from_date" name="from_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <label for = "id_from_date">From Date</label>
                                    </div>
                                </div>
                               
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l11 xl11" style="margin: 3px 0px;">
                                        <input type="text" id="id_to_date" name="to_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <label for = "id_to_date">To Date</label>
                                    </div>
                                </div>

                                

                               

                                
                                <div class="row">
                                    <div class="col s4 m4 l4 xl4">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s4 m4 l4 xl4">
                                        <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            
                                    </div>
                                        
                                </div>
                            </form>
                            </div>

                            

                            
                        </div>
                        <!-- End Body --> 
                    
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

     

    <script>
     $(document).ready(function() 
	 {
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

        
       
        
       
      });
    </script>
    </body>
</html>
