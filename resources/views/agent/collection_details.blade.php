<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
                <div class="card " style = "margin-top:60px">
                    <div class="card-image">
                        <span class="card-title" style = "padding:12px;">Pending Collection Details</span>
                        
                    </div>
                    
                    <div class="card-content white darken-1" style = "border-radius:4px;">
                    <p>Current Area : <?php echo $area_name; ?></p>
                        <!-- Page Body --> 
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                        <tr>
                                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                            <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Full Name</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Taken</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Paid</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Pending</th>
                                            <th style='font-size:12px;padding:7px 8px;display:none;'>Pay</th>
                                            
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                    <?php 
                                                $j = 1;

                                                foreach($c_details as $f)
                                                {
                                               
                                                
                                                echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_name']."</td>";
                                               echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['per_name']."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_mobile']."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['de_tot']."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['ce_tot']."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['pe_tot']."</td>";
                                                
                                               

                                                echo "<td style='font-size:12px;padding:7px 8px;display:none;'>";
                                                echo "<button class='btn-floating btn-sm ' id='view_".$f['user_name']."'>
                                                Pay</td>";

                                               

                                               
                                                
                                                echo "</tr>";
                                                                                        
                                                $j++;
                                                }
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Body --> 
                    
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

              $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("view_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure to View?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            //window.location.href = "<?php echo url('/'); ?>/user_one_agent_1/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "No Recharge...", "error");
                        }
                    });

                    
                    
                }
                
            });
       
      });
    </script>
    </body>
</html>
