<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('agent.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('agent.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_agent')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
                <div class="card " style = "margin-top:60px">
                    <div class="card-image">
                        <span class="card-title" style = "padding:12px;">User Details</span>
                    </div>
                    
                    <div class="card-content white darken-1" style = "border-radius:4px;">
       
                        <!-- Page Body --> 
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                    
                                    </thead>
                                    <tbody id="tbl_body">
                                    <?php 
                                                
                                                foreach($user1 as $f)
                                                {
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>USER CODE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_code."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>NAME</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_per_name."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>CITY</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_city."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>STATE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_state."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>LANDLINE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_phone."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>MOBILE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_mobile."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>KYC</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_kyc."</td></tr>";
                                                    
                                                
                                                    
                                                
                                        
                                                }
                                            ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class ="col s12 m12 l12 xl12">
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                    
                                    </thead>
                                    <tbody id="tbl_body1">
                                    <?php 
                                                $photo = "";
                                                $kyc = "";
                                                foreach($user1 as $f)
                                                {
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>USER NAME</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_name."</td></tr>";
                                                    
                                                
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>SET UP FEE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_setup_fee."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>USER TYPE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_type."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>PARENT NAME</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->parent_name."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>PARENT TYPE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->parent_type."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>RECHARGE MODE</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_rec_mode."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>API SUCCESS URL</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_api_url_1."</td></tr>";
                                                    
                                                    echo "<tr><th style='font-size:12px;padding:7px 8px;'>API FAILURE URL</th>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_api_url_2."</td></tr>";
                                                    
                                                    $photo = $f->user_photo;
                                                    $kyc = $f->user_kyc_proof;
                                        
                                                }
                                            ?>
                                    </tbody>
                                </table>
                            </div>

                            <div class ="col s12 m12 l12 xl12">
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                    
                                    </thead>
                                    <tbody id="tbl_body2">
                                    <?php 
                                                
                                                echo "<tr><th style='font-size:12px;padding:7px 8px;'>PHOTO</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadphoto/".$photo."' style='height:150px;width:150px;' alt='NO PHOTO'/>";
                                                echo "</td></tr>";
                                            
                                                echo "<tr><th style='font-size:12px;padding:7px 8px;'>KYC PROOF</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadkyc/".$kyc."' style='height:150px;width:150px;' alt='NO PHOTO'/>";
                                                echo "</td></tr>";
                                            
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End Body --> 
                    
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('agent.bottom')

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            
       
      });
    </script>
    </body>
</html>
