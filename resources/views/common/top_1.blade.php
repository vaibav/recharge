<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<title>Lakshmi Mobiles</title>
<link rel="icon" href="{{ asset('img/brand/logo.png') }}" type="image/x-icon"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/css/perfect-scrollbar.min.css">
<link href="{{asset('css/r_style.css')}}" rel="stylesheet">


<style>
   .pge {
    margin-left: 242px;
  }

@media only screen and (max-width : 992px) {
  .pge {
    margin-left: 0px;
  }
}

.title-pink
{
    background: -webkit-linear-gradient(45deg,#303f9f,#7b1fa2);
    background: -moz- oldlinear-gradient(45deg,#303f9f,#7b1fa2);
    background: -o-linear-gradient(45deg,#303f9f,#7b1fa2);
    background: linear-gradient(45deg,#303f9f,#7b1fa2);
    -webkit-box-shadow: 3px 3px 20px 0 rgba(123,31,162,.5);
    box-shadow: 3px 3px 20px 0 rgba(123,31,162,.5);
}
</style>