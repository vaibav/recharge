<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " style="padding-top:10px">
<div class="container-fluid">
    <div class="navbar-wrapper">
        <a class="navbar-brand" href="javascript:;" style="color:black;font-weight:bold">{{$title}}</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
    <span class="sr-only">Toggle navigation</span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
    <span class="navbar-toggler-icon icon-bar"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end">
    <form class="navbar-form">
        <div class="input-group no-border" style="padding-right: 10px;padding-top:3px;">
        <?php 
            $stx = "";
            foreach($user->offer as $r)
            {
                $stx = $stx . $r->offer_details. " <i class='fa fa-bell-o' aria-hidden='true' class=''></i>";
            }

            $sty = "";
            foreach($user->inactive as $r)
            {
                $sty = $sty . $r->net_name. " <i class='fa fa-bell-o ' aria-hidden='true' class=''></i>";
            }
            
        ?>

        <marquee>
            <p style="color:purple;font-weight: bold;margin-bottom:2px;">Welcome! {{$stx}}</p>
            <p style="color:red;font-weight: bold;margin-bottom:2px;">Inactive Networks! {{$sty}}</p>
        </marquee>
        </div>
    </form>
    <ul class="navbar-nav">
        <li class="nav-item " >
        <p style="color:black;font-weight:bold;font-size: 16px;padding-top: 12px;padding-left:10px;">Hi {{$user->user}}! &nbsp;&nbsp; {{$user->ubal}} </p> 
        </li>
        <li class="nav-item dropdown">
        <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons" style="color:black;font-weight:bold">person</i>
            <p class="d-lg-none d-md-block">
            Account
            </p>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="{{url('changepassword')}}">Change Pin</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{url('logout')}}">Log out</a>
        </div>
        </li>
    </ul>
    </div>
</div>
</nav>
<!-- End Navbar -->