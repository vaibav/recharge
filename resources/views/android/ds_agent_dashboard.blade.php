<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <style>
        .floating-2 {
            box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            position: relative; z-index: 1; transition: all .2s ease-in-out; border-radius: 50%; 
            padding: 0; cursor: pointer; width: 32px; height: 32px; font-size:12px; padding:6px 2px;
    }
        </style>
    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   
    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-2 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-rupee-sign fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Collection</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                
                
            </div>
        </div>
        
    </div>

    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style = "height:420px;overflow-y:scroll;" id = "rech_1">
     
        <div class=" card z-depth-1 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c1">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                        id = "a_ic"><i class='far fa-user fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Area Allocation</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-1 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c2">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-rupee-sign fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Verification</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-1 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c3">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-pause fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Pending Report</p>
                </div>
            </div>
        </div>

    
    </div>
 
   
    @include('android.common_bottom')

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 250);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                }
         
            });

            $('#c1').on('click',function(e)
            {
                e.preventDefault();
            
                var at = $('#auth_token').val();
                window.open("<?php echo url('/'); ?>/mobile_user_agent_allocation_ds_1?auth_token=" + at,'_self');

            }); 

            $('#c2').on('click',function(e)
            {
                e.preventDefault();
            
                var at = $('#auth_token').val();
                window.open("<?php echo url('/'); ?>/mobile_user_collection_verify_ds_1?auth_token=" + at,'_self');

            }); 

            $('#c3').on('click',function(e)
            {
                e.preventDefault();
            
                var at = $('#auth_token').val();
                window.open("<?php echo url('/'); ?>/mobile_user_collection_pending_ds_1?auth_token=" + at,'_self');

            }); 

           
        });
    </script>
</body>
</html>