<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                    <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['mobile']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo floatval($data['consumedlimit']) + floatval($data['remaininglimit']); ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consumedlimit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaininglimit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h6 amber-text text-darken-3 " style="margin:7px 5px;">Beneficiary Delete</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_back">
                                <i class="fas fa-arrow-circle-left fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>
    <form id = "rech_form" class="user" action="#" method="post" accept-charset="UTF-8">
        <input type="hidden" id = "csrftk" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id = "u_name" name="u_name" value="{{ $user_name }}">
        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
        <input type="hidden" id = "ben_id" name="ben_id" value="">
        <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $msisdn; ?>">
        <input type="hidden" id = "trans_id" name="trans_id" value="">
       

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($beni as $r)
            {
              
                $str = $str . " <div style='margin:10px 10px;border-left: 5px solid #F0B27A;' class='card z-depth-1' >
                    <div class='row' style='margin:0px 0px;'>
                        <div class='col-2 col-sm-2 col-md-2 text-center' style='margin:0px 0px;padding: 20px 4px;'>
                            <a class='floating btn-small waves-effect waves-light  #e57373 red lighten-2 white-text text-center'
                                id='delete_".$r['id']."'><i class='far fa-trash-alt'></i></a>
                        </div>

                        <div class='col-6 col-sm-6 col-md-6 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <p class = 'h6' style='margin:10px 2px;font-size: 14px;color: #52BE80;'  id='accno_".$r['id']."'>".$r['account']."</p>
                            <p class = 'text-muted' style='margin:10px 2px;font-size: 14px;'  id='name_".$r['id']."'>".$r['name']."</p>
                            <p class = 'text-muted' style='margin:10px 2px;font-size: 14px;' id='bnk_".$r['id']."'>".$r['bank']."</p>
                             <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;' id='bnkifsc_".$r['id']."'>".$r['ifsc']."</p>
                            
                            
                        </div>
                       
                        <div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>";

                            // Already Pending Delete OTPs Check
                            $dx = 0;
                            $trans_id = "0";
                           /* foreach($beni1 as $f)
                            {
                                if($f->ben_id == $r['b_id']) {
                                    $dx =1;
                                    $trans_id = $f->trans_id;
                                    break;
                                }
                            }*/

                            if($dx == 1) {
                                $str = $str."<div class='row' style='margin:0px 5px;' id='otps_".$r['id']."'>";
                            }
                            else {
                                $str = $str."<div class='row' style='margin:0px 5px;display:none;' id='otps_".$r['id']."'>";
                            }
                            
                            $str = $str."<div class='col-12 col-sm-12 col-md-12' style='padding:2px 9px;padding-top: 10px;padding-bottom: 6px;'>
                                            <input class='form-control curve-border-1' type='number' 
                                            placeholder='OTP' id='otp_".$r['id']."' name='otp_".$r['id']."'>
                                    </div>
                                    <p class = 'text-muted' style='margin:10px 2px;font-size: 12px;display:none;' id='trid_".$r['id']."'>".$trans_id."</p>
                                </div>";
                
                            if($dx == 1) {
                                $str = $str."<div class='row' style='margin:0px 5px;' id='otpr_".$r['id']."'>";
                            }
                            else {
                                $str = $str."<div class='row' style='margin:0px 5px;display:none;' id='otpr_".$r['id']."'>";
                            }
                          
                            $str = $str."<div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                    <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id='send_".$r['id']."'><i class='fas fa-paper-plane'></i></a>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                    <a class='floating btn-sm waves-effect waves-light #00acc1 cyan darken-1 white-text text-center' 
                                    id='resend_".$r['id']."'><i class='fas fa-share-square'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
                
                $j++;
            }

            echo $str;
        ?>


       
    </div>

    </form>

    @include('android.common_bottom')
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + ms + "/" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
                
                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var bid = nid[1];
                    var ath = $('#auth_token').val();
                    var msi = $('#c_msisdn').val();
                    var tid = $('#trid_' + bid).text();
                    var cotp = $('#otp_' + bid).val();

                    //alert(cotp + "---" + bid + "----" + tid + "----" + msi + "---" + ath);
                    if(cotp == "") {
                        $.alert("Enter OTP");
                        $('#btn_otp').prop('disabled', false);
                    }
                    else if(cotp != "" && bid != "" && tid != "" && msi != "" && ath != "") 
                    {
                        $('#loader').show();
                        $.ajax({
                            url: '<?php echo url('/'); ?>/mobile_user_money_db_delete_otp',
                            type: 'GET',
                            data: {ben_id:bid, msisdn:msi, trans_id:tid, otp:cotp, auth_token:ath},
                            dataType: 'JSON',
                            success: function (data) { 
                                if(data.status == 0)
                                {
                                    $.alert(data.message);
                                    window.open("<?php echo url('/'); ?>/mobile_user_money_db_1/" + msi + "/" + ath,'_self');
                                }
                                else 
                                {
                                    $.alert(data.message);
                                }
                                $('#loader').hide();
                                $('#btn_otp').prop('disabled', false);
                            },
                            error: function(data, mXg) { 
                                $('#loader').hide();
                                console.log(data);
                            }
                        }); 
                    
                    }
                   
                }

                if(gid.indexOf("delete_") !== -1)
                {
                    var nid = gid.split("delete_");
                    var bid = nid[1];
                    var ath = $('#auth_token').val();
                    var msi = $('#c_msisdn').val();
                    var accno = $('#accno_' + bid).text();
                   

                    //alert(cotp + "---" + bid + "----" + tid + "----" + msi + "---" + ath);
                    if(bid != "" && accno != "" && msi != "" && ath != "") 
                    {
                        $.confirm({
                            title: 'Beneficiary Delete',
                            content: '<table class ="bordered striped responsive-table">'
                                    + '<tr><th style="padding:4px 8px;">Account No</th><td style="padding:4px 8px;">' + accno + '</td></tr>'  
                                    + '<tr><th style="padding:4px 8px;">Name</th><td style="padding:4px 8px;">' + $('#name_' + bid).text() + '</td></tr>'
                                    + '<tr><th style="padding:4px 8px;">Bank</th><td style="padding:4px 8px;">' + $('#bnk_' + bid).text() + '</td></tr></table>',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'OK',
                                    btnClass: 'btn-green',
                                    action: function(){
                                        $('#loader').show();
                                        $.ajax({
                                            url: '<?php echo url('/'); ?>/mobile_user_money_db_delete',
                                            type: 'GET',
                                            data: {ben_id:bid, msisdn:msi, accno:accno, auth_token:ath},
                                            dataType: 'JSON',
                                            success: function (data) { 
                                                if(data.status == 0)
                                                {
                                                    $.alert(data.message);
                                                    $('#otps_' + bid).show();
                                                    $('#otpr_' + bid).show();
                                                    $('#trid_' + bid).text(data.trans_id);
                                                }
                                                else 
                                                {
                                                    $.alert(data.message);
                                                }
                                                $('#loader').hide();
                                                
                                                
                                            },
                                            error: function(data, mXg) { 
                                                $('#loader').hide();
                                                console.log(data);
                                                
                                            }
                                        }); 
                                       
                                    }
                                },
                                close: function () {
                                    $.alert('Sorry! No Delete...');
                                }
                            }
                        });
                       
                       
                    
                    }
                   
                }

            
            });

           
          
        });
    </script>
</body>
</html>