<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                   <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['mobile']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo floatval($data['consumedlimit']) + floatval($data['remaininglimit']); ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consumedlimit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaininglimit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Beneficiaries</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_add">
                                <i class="fas fa-plus fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_back">
                                <i class="fas fa-arrow-circle-left fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>

    <form id = "rech_form" class="user" action="#" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id = "u_name" name="u_name" value="{{ $user_name }}">
        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
        <input type="hidden" id = "ben_id" name="ben_id" value="">
        <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $msisdn; ?>">

        <input type="hidden" id = "bene_id" name = "bane_id" value="">
        <input type="hidden" id = "cus_acc_no" name = "cus_acc_no" value="">
        <input type="hidden" id = "cus_name" name = "cus_name" value="">
        <input type="hidden" id = "bank_code" name = "bank_code" value="">
        <input type="hidden" id = "bank_mode" name = "bank_mode" value="">
        <input type="hidden" id = "cus_amt" name = "cus_amt" value="">

        <input type="hidden" id = "trans_id" name="trans_id" value="">
        <input type="hidden" id = "otp" name="otp" value="">
        <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($beni1 as $r)
            {
                $bank = "";
               /* if($r->bank_code == "UTIB") {
                    $bank = "AXIS BANK";
                }
                else if($r->bank_code == "BARB") {
                    $bank = "BANK OF BARODA";
                }
                else if($r->bank_code == "CNRB") {
                    $bank = "CANARA BANK";
                }
                else if($r->bank_code == "CORP") {
                    $bank = "CORPORATION BANK";
                }
                else if($r->bank_code == "CIUB") {
                    $bank = "CITY UNION BANK";
                }
                else if($r->bank_code == "HDFC") {
                    $bank = "HDFC BANK";
                }
                else if($r->bank_code == "IBKL") {
                    $bank = "IDBI BANK LTD";
                }
                else if($r->bank_code == "IDIB") {
                    $bank = "INDIAN BANK";
                }
                else if($r->bank_code == "IOBA") {
                    $bank = "INDIAN OVERSEAS BANK";
                }
                else if($r->bank_code == "ICIC") {
                    $bank = "ICICI BANK";
                }
                else if($r->bank_code == "KVBL") {
                    $bank = "KARUR VYSYA BANK";
                }
                else if($r->bank_code == "SBIN") {
                    $bank = "STATE BANK OF INDIA";
                }
                else if($r->bank_code == "SYNB") {
                    $bank = "SYNDICATE BANK";
                }
                else if($r->bank_code == "TMBL") {
                    $bank = "TAMILNADU MERCANTILE BANK";
                }
                else if($r->bank_code == "VIJB") {
                    $bank = "VIJAYA BANK";
                }
                else if($r->bank_code == "") {
                    $bank = $r->bank_ifsc;
                }
                else {
                    $bank = $r->bank_code;
                }*/

               
                $str1 = "";
              /*  if($r->ben_status == "OTP SUCCESS") {
                   $str1 = $str1."<div class='row' style='margin:0px 5px;'>
                                    <div class='col-12 col-sm-12 col-md-12' style='padding:2px 9px;padding-top: 10px;padding-bottom: 6px;'>
                                            <input class='form-control curve-border-1' type='number' 
                                            placeholder='OTP' id='otp_".$r->ben_id."' name='otp_".$r->ben_id."'>
                                    </div>
                                </div>
                                <div class='row' style='margin:0px 5px;'>
                                    <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                        <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                        id='send_".$r->ben_id."'><i class='fas fa-paper-plane'></i></a>
                                    </div>
                                    <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                        <a class='floating btn-small waves-effect waves-light #00acc1 cyan darken-1 white-text text-center' 
                                        id='resend_".$r->ben_id."'><i class='fas fa-share-square'></i></a>
                                    </div>
                                </div>";
                }*/
                


                $str = $str . " <div style='margin:10px 10px;border-left: 5px solid #F0B27A;' class='card z-depth-1' >
                    <div class='row' style='margin:0px 0px;'>
                        <div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <p class = 'h6' style='margin:10px 2px;font-size: 14px;color: #52BE80;'  id='accno_".$r['id']."'>".$r['account']."</p>
                            <p class = 'text-muted' style='margin:10px 2px;font-size: 14px;'  id='name_".$r['id']."'>".$r['name']."</p>
                            <p class = 'text-muted' style='margin:10px 2px;font-size: 14px;' id='bnk_".$r['id']."'>".$bank."</p>
                            <p class = 'text-muted' style='margin:10px 2px;font-size: 14px;' id='bnkifsc_".$r['id']."'>".$r['ifsc']."</p>
                            
                        </div>
                       
                        <div class='col-5 col-sm-5 col-md-5 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            ".$str1."
                        </div>
                    </div>
                </div>";
                
                $j++;
            }

            echo $str;
        ?>


       
    </div>

    </form>

    @include('android.common_bottom')
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();
                    //alert(ms);

                    window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + ms + "/" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
                if(gid.indexOf("a_add") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_ab_2/" + ms + "/" + at,'_self');
                   
                }

                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var bid = nid[1];
                    var ath = $('#auth_token').val();
                    var msi = $('#c_msisdn').val();
                    var tid = $('#trid_' + bid).text();
                    var cotp = $('#otp_' + bid).val();

                    //alert(cotp + "---" + bid + "----" + tid + "----" + msi + "---" + ath);
                    if(cotp == "") {
                        $.alert("Enter OTP");
                        $('#btn_otp').prop('disabled', false);
                    }
                    else if(cotp != "" && bid != "" && tid != "" && msi != "" && ath != "") 
                    {
                        $('#loader').show();
                        $.ajax({
                            url: '<?php echo url('/'); ?>/mobile_user_money_ab_store_otp',
                            type: 'GET',
                            data: {ben_id:bid, msisdn:msi, trans_id:tid, otp:cotp, auth_token:ath},
                            dataType: 'JSON',
                            success: function (data) { 
                                if(data.status == 0)
                                {
                                    $.alert(data.message);
                                    window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + msi + "/" + ath,'_self');
                                }
                                else 
                                {
                                    $.alert(data.message);
                                }
                                $('#loader').hide();
                                $('#btn_otp').prop('disabled', false);
                            },
                            error: function(data, mXg) { 
                                $('#loader').hide();
                                console.log(data);
                            }
                        }); 
                    
                    }
                   
                }

                if(gid.indexOf("resend_") !== -1)
                {
                    var nid = gid.split("resend_");
                    var bid = nid[1];
                    var ath = $('#auth_token').val();
                    var msi = $('#c_msisdn').val();
                    var tid = $('#trid_' + bid).text();
                   

                    //alert(cotp + "---" + bid + "----" + tid + "----" + msi + "---" + ath);
                    if(bid != "" && tid != "" && msi != "" && ath != "") 
                    {
                        $.confirm({
                            title: 'Are You Sure?',
                            content: 'Resend OTP',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'OK',
                                    btnClass: 'btn-green',
                                    action: function(){
                                        $('#loader').show();
                                        $.ajax({
                                            url: '<?php echo url('/'); ?>/mobile_user_money_ab_resend_otp',
                                            type: 'GET',
                                            data: {ben_id:bid, msisdn:msi, trans_id:tid, auth_token:ath},
                                            dataType: 'JSON',
                                            success: function (data) { 
                                                
                                                window.open("<?php echo url('/'); ?>/mobile_user_money_ab_1/" + msi + "/" + ath,'_self');
                                            },
                                            error: function(data, mXg) { 
                                                $('#loader').hide();
                                                console.log(data);
                                                $('#btn_otp').prop('disabled', false);
                                            }
                                        }); 
                                       
                                    }
                                },
                                close: function () {
                                    $.alert('Sorry! No Transfer...');
                                }
                            }
                        });
                       
                       
                    
                    }
                   
                }

            
            });

           
          
        });
    </script>
</body>
</html>