<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
    </head>
<body style="background-color: #ffffff;overflow:hidden;height: 90vh;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 70px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
               
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;border-radius:15px;" class="card z-depth-2 " id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Collection</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_filter"><i class='fas fa-filter fa-2x white-text'></i></a>
                
            </div>
        </div>

        <div class="row" style="margin:3px 0px;">
            <div class="col-12 col-sm-12 col-md-12 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <table class ="table table-striped" style = "margin-bottom:2px">
                    <tr><th  style="padding:2px 8px;font-size:12px;text-align:left;width:40%;">USER NAME</th>
                        <td style="padding:2px 8px;font-size:12px;font-size:12px;text-align:left" id = "u_n">-</td></tr>
                    <tr><th style="padding:2px 8px;font-size:12px;text-align:left;width:40%;">TAKEN</th>
                        <td style="padding:2px 8px;font-size:12px;font-size:12px;text-align:left" id = "u_t">-</td></tr> 
                    <tr><th style="padding:2px 8px;font-size:12px;text-align:left;width:40%;">PAID</th>
                        <td style="padding:2px 8px;font-size:12px;font-size:12px;text-align:left" id = "u_p">-</td></tr>
                    <tr><th style="padding:2px 8px;font-size:12px;text-align:left;width:40%;">BALANCE</th>
                        <td style="padding:2px 8px;font-size:12px;font-size:12px;text-align:left" id = "u_b">-</td></tr></tabel>
                </table>
            </div>
           
        </div>
        
    </div>

   
    
   <div style="overflow:auto; position:absolute; top:315px; left:10px; right:10px; bottom:2px;border-radius:15px;" id = "rech_1" class="card z-depth-2"> 

        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form" class="form-horizontal" action="{{url('mobile_user_collection_ag_2')}}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                       
                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                <select class="form-control curve-border" id="id_user_name"  name = "user_name" >
                                    <option value = '-'>----Select----</option>
                                    <?php
                                        $str = "";
                                        foreach($user1 as $f)
                                        {
                                            $str = $str."<option value='".$f[0]."'>".$f[0]."--".$f[1]."</option>";
                                        }
                                        echo $str;
                                    ?>                    
                                </select>
                            </div>
                            
                        </div>

                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="number" 
                                placeholder="Paid Amount" id="id_pay_amt" name="pay_amt">
                            </div>
                            
                        </div>

                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="text" 
                                placeholder="Paid Amount" i id="id_pay_date" name="pay_date" value = "<?php echo date('Y-m-d'); ?>">
                            </div>
                            
                        </div>
                            
                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <select class="form-control curve-border" id="id_pay_mode"  name = "pay_mode" >
                                    <option value = '-'>----Select Pay Mode----</option>
                                    <option value = 'CASH'>CASH</option>
                                    <option value = 'CHEQUE'>CHEQUE</option>
                                    <option value = 'D.D'>D.D</option>
                                    <option value = 'IMPS-RDGS-NEFT'>IMPS/RDGS/NEFT</option>                   
                                </select>
                            </div>
                            
                        </div>

                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="number" 
                                placeholder="DD/CHEQUE NO" id="id_pay_option" name="pay_option">
                            </div>
                            
                        </div>

                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="id_pay_image" name="pay_image"
                                        aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="id_pay_image">Choose Image</label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        
                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12 text-right">
                                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                            </div>
                        </div>
                        
                    </form>
            </div>
        </div>
    </div>

   

    @include('android.common_bottom')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

   <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            echo "<script>
            $(document).ready(function() 
            {
                $.alert('".$res."');
            });
            </script>";
            session()->forget('result');
        }
    ?>
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

           

            $('#id_pay_date').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });


            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
               
                if(gid.indexOf("a_filter") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_collection_ag_4?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                }
         
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var u_nme = $('option:selected', '#id_user_name').val(); 
                var u_amt = $('#id_pay_amt').val();
                var u_dat = $('#id_pay_date').val();
                var u_mod = $('option:selected', '#id_pay_mode').val();
                
                //alert(ax);
                if(u_nme != "-" && u_amt != "" && u_dat != "" && u_mod != "-")
                {

                    $.confirm({
                        title: 'Are You Sure?',
                        content: '<table class ="table table-striped">' + '<tr><th  style="padding:4px 8px;">USER NAME</th><td style="padding:4px 8px;">' + u_nme + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">PAID AMOUNT</th><td style="padding:4px 8px;">' + u_amt + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">DATE</th><td style="padding:4px 8px;">' + u_dat + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">MODE</th><td style="padding:4px 8px;">' + u_mod + '</td></tr></tabel> ',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('mobile_user_collection_ag_2')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                    
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(u_nme == "-")
                {
                    $.alert('Please Select User Name!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(u_amt == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(u_dat == "")
                {
                    $.alert('Please Enter Date!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(u_mod == "-")
                {
                    $.alert('Please Select Payment Mode!');
                    $('#btn_submit').prop('disabled', false);
                }
                
                
            }); 

            $("#id_user_name").change(function(e)
            {
                var ty = $('option:selected', this).val();
                var ath = $('#auth_token').val();

                $('#loader').show();
                if(ty != "-")
                {
                    $('#loader').show();
                    $.ajax({
                        url: '<?php echo url('/'); ?>/mobile_user_collection_ag_3',
                        type: 'GET',
                        data: {auth_token:ath, user_name:ty},
                        dataType: 'JSON',
                        success: function (data) { 
                            $('#u_n').text(data.user_name);
                            $('#u_t').text(data.de_tot);
                            $('#u_p').text(data.ce_tot);
                            $('#u_b').text(data.pe_tot);
                            $('#loader').hide();
                            
                        },
                        error: function(data, mXg) { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    }); 
                   
                    

                }
                    
            });
          
        });
    </script>
</body>
</html>