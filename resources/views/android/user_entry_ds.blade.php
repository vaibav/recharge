<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>
</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='far fa-user fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">User Creation</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
               
            </div>
        </div>
        
    </div>
   
    <form id="rech_form" class="form-horizontal" action="{{url('mobile_distributor_store')}}" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style="margin:10px 10px;height:410px;border-radius:15px;overflow-y:scroll;"class="card z-depth-2"  id = "rech_1"> 
        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="ccol-12 col-sm-12 col-md-12" style="margin:2px 2px;padding: 0px 0px;">
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="full name" id="id_user_per_name" name="user_per_name" type="text" class="form-control curve-border">
                </div>
                
                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="Area" id="id_user_state" name="user_state" type="text" class="form-control curve-border">
                </div>
                
                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="city" id="id_user_city" name="user_city" type="text" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="Mobile No" id="id_user_mobile" name="user_mobile" type="number" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="Whatsapp No" id="id_user_phone" name="user_phone" type="number" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="mail id" id="id_user_mail" name="user_mail"  type="text" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12 text-center" >
                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_1">next</button>
                </div>
                    
            </div>
        </div>
    </div>

    <div style="margin:10px 10px;height:410px;border-radius:15px;overflow-y:scroll;display:none;" class="card z-depth-2" id = "rech_2"> 
        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col-12 col-sm-12 col-md-12" style="margin:2px 2px;padding: 0px 0px;">
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="User Name" id="id_user_name" name="user_name" type="text" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="password" id="id_user_pwd" name="user_pwd" type="password" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <select class="form-control curve-border" id="id_user_acc_type" name="user_acc_type">
                        <option value='-'>User Account Type</option>
                        <?php
                            if($user_type == "DISTRIBUTOR")
                            {
                                echo "<option value='RETAILER'>RETAILER</option>";
                                echo "<option value='AGENT'>AGENT</option>";
                            }
                            else if($user_type == "SUPER DISTRIBUTOR")
                            {
                                echo "<option value='DISTRIBUTOR'>DISTRIBUTOR</option>";
                                echo "<option value='API PARTNER'>API PARTNER</option>";
                                echo "<option value='AGENT'>AGENT</option>";
                            }
                        ?>
                        
                        
                    </select>
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <select class="form-control curve-border" id="id_pack_id" name="pack_id">
                        <option value='-'>Select Package</option>
                        <?php
                            foreach($package as $f) {
                                echo "<option value='".$f->pack_id."' >".$f->pack_name."</option>";
                            }
                        ?>
                        
                        
                    </select>
                </div>

                <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12">
                    <input placeholder="Setup Fee" id="id_user_setup_fee"  name="user_setup_fee" type="number" class="form-control curve-border">
                </div>

                <div style="height:17px"></div>
                <div class="row" style = "margin-bottom:5px;margin-left: 5px;">
                    <div class="col-3 col-sm-3 col-md-3" style="margin: 3px 0px;">
                        <label style = "font-size:14px;"><input type="checkbox" id="id_user_rec_mode1" name="user_rec_mode[]" value="WEB"/><span>WEB &nbsp;</span></label>
                    </div>
                    <div class="col-3 col-sm-3 col-md-3 " style="margin: 3px 0px;">
                        <label style = "font-size:14px;"><input type="checkbox" id="id_user_rec_mode2" name="user_rec_mode[]" value="GPRS" /><span>GPRS&nbsp;</span></label>
                    </div>
                    <div class="col-3 col-sm-3 col-md-3 " style="margin: 3px 0px;">
                        <label style = "font-size:14px;"><input type="checkbox" id="id_user_rec_mode3" name="user_rec_mode[]" value="API" /><span>API&nbsp;</span></label>
                    </div>
                    <div class="col-3 col-sm-3 col-md-3 " style="margin: 3px 0px;">
                        <label style = "font-size:14px;"><input type="checkbox" id="id_user_rec_mode4" name="user_rec_mode[]" value="SMS" /><span>SMS&nbsp;</span></label>
                    </div>
                </div>

                <div style="height:14px"></div>
                <div class="col-12 col-sm-12 col-md-12" id = "id_f1">
                    <input placeholder="Success URL" iid="id_user_api_url_1" name="user_api_url_1" type="text" class="form-control curve-border">
                </div>

                 <div style="height:17px"></div>
                <div class="col-12 col-sm-12 col-md-12" id = "id_f2">
                    <input placeholder="FAILURE URL" iid="id_user_api_url_2" name="user_api_url_2" type="text" class="form-control curve-border">
                </div>
                
                <div style="height:17px"></div>
                <div class="row" style = "margin-bottom:5px;margin-left: 5px;">
                    <div class="col-6 col-sm-6 col-md-6 text-center" >
                        <button class="btn btn-small waves-effect waves-light waves-light #8e24aa orange darken-1" id="btn_2">Back</button>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 text-center" >
                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_3">next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin:10px 10px;height:410px;border-radius:15px;overflow-y:scroll;display:none;" class="card z-depth-2" id = "rech_3"> 
        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col-12 col-sm-12 col-md-12" style="margin:2px 2px;padding: 0px 0px;">
                <p class="title amber-text" style="padding: 0px 10px;">Network Percentage</p>

                <?php 
                    $j = 1;
                    $str = "";
                    foreach($network as $f)
                    {
                        if($j == 0) {
                            $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        else if(($j % 2) != 0) {
                            $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                        }
                        else if(($j % 2) == 0) {
                            $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        

                        $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:1px 0px;padding: 6px 10px;'>
                                        <input type='hidden' id='id_net_code_".$j."' name='net_code_".$j."' value='".$f->net_code."' />
                                        <p class = 'text-muted' style='margin:1px 2px;font-size: 12px;'>".$f->net_name."</p>
                                    </div>";
                        
                        $str = $str . "<div class='col-5 col-sm-5 col-md-5' style='margin:2px 0px;padding: 2px 3px;'>
                                            <input class='form-control curve-border-1' type='number' id='id_net_per_".$j."' name='net_per_".$j."' value='0'/>
                                            <input type='hidden' id='id_net_surp_".$j."' name='net_surp_".$j."' value='0' />
                                        </div>";
                        $str = $str."</div>";
                    
                        $j++;
                    }

                    echo $str;
                ?>
          
               
                <div class = "row" style="margin:0px 0px;padding: 0px 0px;background: white  ;">
                    <div class="col-6 col-sm-6 col-md-6 text-center" >
                        <button class="btn btn-small waves-effect waves-light #8e24aa orange darken-1" id="btn_4">Back</button>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 text-center" >
                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">Submit</button>
                    </div>
                </div>
                    
            </div>
        </div>
    </div>

    </form>

    @include('android.common_bottom')
    
    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$op."');
                });
                </script>";
            session()->forget('msg');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
                $('#rech_2').height($(window).height() - 240);
                $('#rech_3').height($(window).height() - 240);
            });

            $(window).trigger('resize');
            
            $("#id_user_acc_type").change(function(e)
            {
                var ty = $('option:selected', this).val();

                if(ty != "-")
                {
                    if(ty == "API PARTNER")
                    {
                        $("#id_user_api_url_1").val("");
                        $("#id_user_api_url_2").val("");
                        $("#id_f1").show();
                        $("#id_f2").show();
                    }
                    else
                    {
                        $("#id_user_api_url_1").val("*");
                        $("#id_user_api_url_2").val("*");
                        $("#id_f1").hide();
                        $("#id_f2").hide();
                    }
                }
            });

            $('#btn_1').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').show("slow");
                $('#rech_3').hide("slow");
            });

            $('#btn_2').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').show("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').hide("slow");
                
            });

            $('#btn_3').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').show("slow");
            
            });

            $('#btn_4').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').show("slow");
                $('#rech_3').hide("slow");
            });
           
            $("#id_user_name").blur(function(e) 
            {
                e.preventDefault();
                var user_name = $("#id_user_name").val();
                $('#loader').show();

                if (user_name != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_user/" + user_name,
                        success: function (data) {
                            if(data == 1)
                            {
                                $('#loader').hide();
                                $.alert('User Name Already Exists....!');
                                
                                $("#id_user_name").val("");
                                $("#id_user_name").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_name").focus();
                }
                   
            });

            $("#id_user_mobile").blur(function(e) 
            {
                e.preventDefault();
                var user_mobile = $("#id_user_mobile").val();
                $('#loader').show();

                if (user_mobile != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_mobile/" + user_mobile,
                        success: function (data) {
                            if(data > 0)
                            {
                                $('#loader').hide();
                                $.alert('Mobile No Already Exists....!');
                                $("#id_user_mobile").val("");
                                $("#id_user_mobile").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_mobile").focus();
                }
                   
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                var z = check_data();
                $('#btn_submit').prop('disabled', true);

                if(z[0] == 0)
                {
                    $.confirm({
                        title: 'User Creation!',
                        content: 'Are You Sure?',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('mobile_distributor_store')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                }
                else
                {
                    $.alert(z[1]);
                    $('#btn_submit').prop('disabled', false);
                }
                
            }); 

            function check_data()
            {
                var z = [];
                z[0] = 0;
                z[1] = "done";

                if($('#id_user_per_name').val() == "") {
                    z[0] = 1;
                    z[1] = "Person Name is Empty..";
                }
                else  if($('#id_user_city').val() == "") {
                    z[0] = 1;
                    z[1] = "City is Empty..";
                }
                else  if($('#id_user_mobile').val() == "") {
                    z[0] = 1;
                    z[1] = "Mobile No is Empty..";
                }
                else  if($('#id_user_mail').val() == "") {
                    z[0] = 1;
                    z[1] = "mail is Empty..";
                }
                else  if($('#id_user_name').val() == "") {
                    z[0] = 1;
                    z[1] = "user name is Empty..";
                }
                else  if($('#id_user_pwd').val() == "") {
                    z[0] = 1;
                    z[1] = "Password is Empty..";
                }
                else  if($('#id_pack_id').val() == "-") {
                    z[0] = 1;
                    z[1] = "Package is Empty..";
                }
                
                return z;
            }


        });
    </script>
</body>
</html>