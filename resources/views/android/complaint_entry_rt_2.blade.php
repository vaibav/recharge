<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   
    <!-- Title-->
     <div style="margin:10px 10px;" class="card z-depth-2 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Complaint Entry</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_back"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>
    
    
    <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            echo $recharge;
        ?>
    </div>

   

    @include('android.common_bottom')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('Complaint is registered Successfully...!');
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('Sorry! Complaint is not registered!');
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');


            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
               
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_complaint_rt_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }

                if(gid.indexOf("rech_") !== -1)
                {
                    var gid = this.id;
                    var nid = gid.split("rech_");
                    var td = nid[1];

                    var at = $('#auth_token').val();
                    var mb = $('#mob_' + td).text();
                    var am = $('#amt_' + td).text();
                    var cm = $('#cmp_' + td).val();

                    if(td != "" && mb != "" && am != "" && cm != "")
                    {
                        $.confirm({
                            title: 'Complaint!',
                            content: 'Are You Sure?',
                            type: 'purple',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'OK',
                                    btnClass: 'btn-purple',
                                    action: function(){
                                        $('#loader').show();
                                        var str = "<?php echo url('/'); ?>/mobile_user_complaint_rt_3?auth_token=" +at;
                                        str = str + "&trid=" + td + "&mob=" + mb + "&amt=" + am + "&cmp=" +cm;
                                        window.open(str , "_self" );
                                    }
                                },
                                close: function () {
                                    $.alert('Canceled!');
                                   
                                }
                            }
                        });

                        
                    }
                    else if(cm == "")
                    {
                        $.alert('Complaint Entry May be Empty...');
                    }
                    else
                    {
                        $.alert('Any one value May be Empty...');
                    }
                   
                }

         
            });

           
          
        });
    </script>
</body>
</html>