<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        
    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">My Account</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_back"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>

    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

     <div style="margin:10px 0px;height:410px;overflow-y:scroll;"   id = "rech_1"> 
     <?php 
        $j = 1;
        
        $debit = 0;
        $credit = 0;
        $balance = 0;
        $str = "";
       /* if($obalance != 0)
        {
            $balance = floatval($balance) + floatval($obalance);
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2 ' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            
            $str = $str. "<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='material-icons'>trending_up</i></a><br>
                        </div>
                        <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                            <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>OPENING BALANCE</p>
                            <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                        </div>
                        <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                            <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>+ ".Number_format($balance,2,'.','')."</p>
                        </div>
                    </div></div>";
           
        }*/
        foreach($pay1 as $f)
        {
            if($f->user_name == $user_name && $f->rech_status == "SUCCESS")
            {
                // Debit +
                $debit = floatval($debit) + floatval($f->ret_total);
                $balance = floatval($balance) + floatval($f->ret_total);

                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2 ' >"; 
                $str = $str."<div class = 'row rounded-pill' style='background: white;'>";
                
                $str = $str."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='material-icons'>trending_up</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->parent_name."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->trans_type." </p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->reply_date."</p>
                                
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>+ ".$f->ret_total."</p>
                            </div>
                        </div></div>";
                
            }
            else if($f->parent_name == $user_name && $f->rech_status == "SUCCESS")
            {
                // Credit -
                $credit = floatval($credit) + floatval($f->ret_total);
                $balance = floatval($balance) - floatval($f->ret_total);

                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
                $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

                $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #ec407a pink lighten-1 white-text'>
                                    <i class='material-icons'>trending_down</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->user_name."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->trans_type." </p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->reply_date."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$f->ret_total."</p>
                            </div>
                        </div></div>";

               
            }
            $j++;
        
        }

       /* if($ce_tot != '0')
        {
            $credit = floatval($credit) + floatval($ce_tot);
            $balance = floatval($balance) - floatval($ce_tot);

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'>
                                <i class='material-icons'>remove</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>RECHARGE AMT</p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".number_format($ce_tot,2,'.','')."</p>
                            </div>
                        </div></div>";
           
            $j++;
        
        }
        
        if($old_re != '0')
        {
            $credit = floatval($credit) + floatval($old_re);
            $balance = floatval($balance) - floatval($old_re);

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'>
                                <i class='material-icons'>remove</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>OLD RECHARGE AMT</p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".number_format($old_re,2,'.','')."</p>
                            </div>
                        </div></div>";
           
            $j++;
        
        }

        if($bill != '0')
        {
            $credit = floatval($credit) + floatval($bill);
            $balance = floatval($balance) - floatval($bill);

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'>
                                <i class='material-icons'>remove</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>EB BILL AMT</p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".number_format($bill,2,'.','')."</p>
                            </div>
                        </div></div>";

            $j++;
        
        }

        if($money != '0')
        {
            $credit = floatval($credit) + floatval($money);
            $balance = floatval($balance) - floatval($money);

            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'>
                                <i class='material-icons'>remove</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>MONEY TRANSFER</p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".number_format($money,2,'.','')."</p>
                            </div>
                        </div></div>";

          
            $j++;
        
        }

        if($mverify != '0')
        {
            $credit = floatval($credit) + floatval($mverify);
            $balance = floatval($balance) - floatval($mverify);
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'>
                                <i class='material-icons'>remove</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>BENEFICIARY VERIFY</p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".number_format($mverify,2,'.','')."</p>
                            </div>
                        </div></div>";

            $j++;
        
        }*/

        echo $str;
        
    ?>
    </div>
   
    



@include('android.common_bottom')




<script type="text/javascript">
$(document).ready(function() 
{
    
    $('#loader').hide();

    $(window).resize(function() {
        $('#rech_1').height($(window).height() - 250);
    });

    $(window).trigger('resize');

   


    $(document).on('click', "a",function (e) 
    {
        e.preventDefault();
        var gid = this.id;

        if(gid.indexOf("a_back") !== -1)
        {
            var at = $('#auth_token').val();

            window.open("<?php echo url('/'); ?>/mobile_user_stock_ds_1?auth_token=" + at,'_self');
            
        }
        
        if(gid.indexOf("a_log") !== -1)
        {
            window.open("logout:" ,'_self');
        }
 
    });

   

   
  
});
</script>
</body>
</html>