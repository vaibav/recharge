<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Payment Request</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                    id = "a_back"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
            </div>
        </div>
        
    </div>

    <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style="margin:0px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($payment as $d)
            {
                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
                $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>"; 
                $str = $str."<div class='col-3 col-sm-3 col-md-3 text-center' style='margin:4px 0px;padding: 5px 3px;'>"; 

                if($d->trans_status == 1) {
                    $str = $str."<a class='floating  btn-small waves-effect waves-light default-color white-text'>
                    <i class='small material-icons '>check</i></a><br>"; 
                }
                else if($d->trans_status == 2) {
                    $str = $str."<a class='floating  btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text'>
                                    <i class='small material-icons '>clear</i></a><br>"; 
                }
                else if($d->trans_status == 0) {
                    $str = $str."<a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'>
                    <i class='small material-icons '>directions_run</i></a><br>"; 
                }
            
                
                $str = $str."<p style='font-size: 12px;margin: 0;'>".$d->user_amount."</p> ";
                
                $str = $str."</div><div class='col-9 col-sm-9 col-md-9 text-left' style='margin:0px 0px;padding: 0px 4px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$d->payment_mode."</p>";
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>Request Date : ".$d->user_req_date."</p>"; 
                if($d->trans_status == 1)
                {
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>Trasnfer Date : ".$d->grant_date."</p>";
                    $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>Trasnfer Amount : ".$d->grant_user_amount."</p>";
                    
                }
                
                $str = $str."</div></div></div>";
               
                $j++;
            }

            echo $str;
        ?>
    </div>

   

    @include('android.common_bottom')

   
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');


            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
               
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_payment_request_ds_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
               

         
            });

           
          
        });
    </script>
</body>
</html>