<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }

        
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;" class="card z-depth-1">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-rupee-sign fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:9px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:500;font-size:20px;">Payment Accept</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
               
                
            </div>
        </div>
        
    </div>

    <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
        
    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($pay as $r)
            {
                $str = $str . " <div style='margin:10px 10px;border-left: 5px solid #33b5e5;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-6 col-sm-6 col-md-6 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <p class = 'h6' style='margin:9px 2px;font-size: 14px;color: #52BE80;' id='uname_".$r->trans_id."'>".$r->user_name."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'>".$r->rech_amount."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'>".$r->rech_date."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'>".$r->reply_id."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;display:none;' id='trid_".$r->trans_id."'>".$r->trans_id."</p>
                        </div>
                        <div class='col-5 col-sm-5 col-md-5 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-12 col-sm-12 col-md-12' style='padding:2px 9px;padding-top: 10px;padding-bottom: 6px;'>
                                        <input class='form-control curve-border-1' type='number' 
                                        placeholder='0.00' id='amt_".$r->trans_id."' name='amt_".$r->trans_id."'>
                                </div>
                            </div>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                    <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id='send_".$r->trans_id."'><i class='fas fa-check'></i></a>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                    <a class='floating btn-small waves-effect waves-light #00acc1 red lighten-2 white-text text-center' 
                                    id='delete_".$r->trans_id."'><i class='fas fa-times'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
                
                $j++;
            }

            echo $str;
        ?>


       
    </div>

    </form>

    @include('android.common_bottom')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
    
   

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

           

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var tid = nid[1];
                    var ath = $('#auth_token').val();
                    var amt = $('#amt_' + tid).val();
                    var unm = $('#uname_' + tid).text();

                    if(amt != "")
                    {
                        $.confirm({
                            title: 'Are You Sure?',
                            content: '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">User</th><td style="padding:4px 8px;">' + unm + '</td></tr>'
                                            + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + amt + '</td></tr></tabel>',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'OK',
                                    btnClass: 'btn-green',
                                    action: function(){
                                        $('#loader').show();
                                        window.location.href = "<?php echo url('/'); ?>/mobile_user_payment_accept_ds_2?trans_id=" + tid + "&amt=" + amt +"&auth_token=" +ath;
                                    }
                                },
                                close: function () {
                                    $.alert('Sorry! No Transfer...');
                                }
                            }
                        });
                    }
                    else
                    {
                        $.alert('Error! Please Enter Amount...');
                    }
                 
                }

                if(gid.indexOf("delete_") !== -1)
                {
                    var nid = gid.split("delete_");
                    var tid = nid[1];
                    var ath = $('#auth_token').val();
                   
                    $.confirm({
                        title: 'Payment Cancel',
                        content: 'Are You Sure?',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                    $('#loader').show();
                                    window.location.href = "<?php echo url('/'); ?>/mobile_user_payment_accept_ds_3?trans_id=" + tid + "&auth_token=" +ath;
                                }
                            },
                            close: function () {
                                $.alert('Sorry! No Transfer...');
                            }
                        }
                    });
                 
                }

            });

        });
    </script>
</body>
</html>