<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
    </head>
<body style="background-color: #ffffff;overflow:hidden;height: 90vh;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 70px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
               
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-2 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Report</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                
                
            </div>
        </div>

       
        
    </div>

   
    
   <div style="overflow:auto; position:absolute; top:210px; left:10px; right:10px; bottom:2px;border-radius:15px;" id = "rech_1" class="card z-depth-2"> 

        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form" class="form-horizontal" action="{{url('mobile_user_collectio_ag_rp_2')}}" method="get">
                       
                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                       
                        
                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="text" 
                                placeholder="From Date" i id="id_f_date" name="f_date" value = "<?php echo date('Y-m-d'); ?>">
                            </div>
                            
                        </div>

                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="text" 
                                placeholder="From Date" i id="id_t_date" name="t_date" value = "<?php echo date('Y-m-d'); ?>">
                            </div>
                            
                        </div>
                            
                        <div style="height:16px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12 text-right">
                                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                            </div>
                        </div>
                        
                    </form>
            </div>
        </div>
    </div>

   

    @include('android.common_bottom')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $('#id_f_date').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });

            $('#id_t_date').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });


            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                }
         
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var u_fd = $('#id_f_date').val();
                var u_td = $('#id_t_dte').val();
                
                //alert(ax);
                if(u_fd != "" && u_td != "")
                {

                   $('#rech_form').attr('action', "{{url('mobile_user_collection_ag_rp_2')}}");
                    $('#rech_form').submit();
                    $('#loader').show();
                    
                }
                else if(u_fd == "")
                {
                    $.alert('Please Select From Date!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(u_td == "")
                {
                    $.alert('Please Select To Date!');
                    $('#btn_submit').prop('disabled', false);
                }
               
                
                
            }); 

           
          
        });
    </script>
</body>
</html>