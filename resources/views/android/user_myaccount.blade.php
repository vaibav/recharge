<!DOCTYPE html>
<html lang="en">
<head>
    @include('android.top')
   
</head>
<body style="background-color: #FFFFFF;">

    <div style="height:120px;background-image: linear-gradient(to bottom, #4F52B1, #6D6FBE);">
        <div class="row">
           
            <div class="col s12 m12 center-align" style="margin-top: 15px;padding: 0px 3px;">
                <img src = "{{ asset('img/brand/logo.png') }}" style=" height: 49px; width: 98px;"/>
            </div>
            
        </div>
    </div>

    <div style="margin-top:-30px;margin-left: 10px;margin-right: 10px;height: 65px;" class="card1 shadow">
            <div class="row" style="margin:0px 0px;">
                <div class="col s7 m7 left-align" style="margin:5px 0px;padding: 0px 10px;">
                    <p class="title" >My Account</p>
                    <p class="sub-title" >Welcome <?php echo $user_name; ?>!</p>
                </div>
                <div class="col s5 m5 right-align" style="margin:10px 0px;padding: 0px 4px;">
                        <p class="title" style="margin: 5px 2px"><?php echo $user_ubal; ?></p>
                    </div>
            </div>
    </div>

    

  

    

    <div style="margin:10px 10px;overflow-y:scroll;height:530px;" class="" id = "rech_4">
           <div class = "card1 shadow">

                <?php 

                    $j = 0;
                    $str = "";
                    $debit = 0;
                    $credit = 0;
                    $balance = 0;

                    
                    foreach($pay as $f)
                    {
                        if($f->user_name == $user_name && $f->trans_status == 1)
                        {
                            // Debit +
                            $debit = floatval($debit) + floatval($f->grant_user_amount);
                            $balance = floatval($balance) + floatval($f->grant_user_amount);

                            if($j == 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                            }
                            else if(($j % 2) != 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                            }
                            else if(($j % 2) == 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                            }
                            

                            $str = $str. "
                                        <div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                            <a class='btn-floating  btn-small waves-effect waves-light #26c6da cyan lighten-1'><i class='material-icons'>trending_up</i></a><br>
                                        </div>
                                        <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                            <p class='user'>".$f->grant_user_name."</p>
                                            <p class='dat'>".$f->grant_date."&nbsp; &nbsp;".$f->payment_mode." </p>
                                            <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                        </div>
                                        <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>+ ".$f->grant_user_amount."</p>
                                        </div>
                                    </div>";
                            
                           
                           
                            
                        }
                        else if($f->grant_user_name == $user_name && $f->trans_status == 1)
                        {
                            // Credit -
                            $credit = floatval($credit) + floatval($f->grant_user_amount);
                            $balance = floatval($balance) - floatval($f->grant_user_amount);

                            if($j == 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                            }
                            else if(($j % 2) != 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                            }
                            else if(($j % 2) == 0)
                            {
                                $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                            }

                            $str = $str ."<div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                            <a class='btn-floating  btn-small waves-effect waves-light #ec407a pink lighten-1'><i class='material-icons'>trending_down</i></a><br>
                                        </div>
                                        <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                            <p class='user'>".$f->user_name."</p>
                                            <p class='dat'>".$f->grant_date."&nbsp; &nbsp;".$f->payment_mode." </p>
                                            <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                        </div>
                                        <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>- ".$f->grant_user_amount."</p>
                                        </div>
                                    </div>";

                            

                        }
                        $j++;

                    }

                    $stx = "";


                    // RECHARGE AMOUNT
                    if($res != "0")
                    {
                        if($j == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        else if(($j % 2) != 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                        }
                        else if(($j % 2) == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
    
                        $credit = floatval($credit) + floatval($res);
                        $balance = floatval($balance) - floatval($res);
                        
                        $res = round($res, 2);
                        
    
                        $str = $str ."<div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                                <a class='btn-floating  btn-small waves-effect waves-light core'><i class='material-icons'>remove</i></a><br>
                                            </div>
                                            <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                                <p class='user'>RECHARGE AMT</p>
                                                <p class='dat'></p>
                                                <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                            </div>
                                            <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>- ".$res."</p>
                                            </div>
                                        </div>";
    
                        $j++;
                    }
                    

                    // PENDING RECHARGE AMOUNT
                    if($rep != "0")
                    {
                        if($j == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        else if(($j % 2) != 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                        }
                        else if(($j % 2) == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
    
                        $credit = floatval($credit) + floatval($rep);
                        $balance = floatval($balance) - floatval($rep);

                        $rep = round($rep, 2);
    
                        $str = $str ."<div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                                <a class='btn-floating  btn-small waves-effect waves-light core'><i class='material-icons'>remove</i></a><br>
                                            </div>
                                            <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                                <p class='user'>PENDING RECHARGE AMT</p>
                                                <p class='dat'></p>
                                                <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                            </div>
                                            <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>- ".$rep."</p>
                                            </div>
                                        </div>";
    
                        $j++;
                    }
                   


                    // EBBILL AMOUNT
                    if($ebs != "0")
                    {
                        if($j == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        else if(($j % 2) != 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                        }
                        else if(($j % 2) == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
    
                        $credit = floatval($credit) + floatval($ebs);
                        $balance = floatval($balance) - floatval($ebs);

                        $ebs = round($ebs, 2);
    
                        $str = $str ."<div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                                <a class='btn-floating  btn-small waves-effect waves-light core'><i class='material-icons'>remove</i></a><br>
                                            </div>
                                            <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                                <p class='user'>EBBILL AMT</p>
                                                <p class='dat'></p>
                                                <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                            </div>
                                            <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>- ".$ebs."</p>
                                            </div>
                                        </div>";
    
                        $j++;
                    }
                   

                    // PENDING EBBILL AMOUNT
                    if($ebp != "0")
                    {
                        if($j == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
                        else if(($j % 2) != 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                        }
                        else if(($j % 2) == 0) {
                            $str = $str . "<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                        }
    
                        $credit = floatval($credit) + floatval($ebp);
                        $balance = floatval($balance) - floatval($ebp);
    
                        $str = $str ."<div class='col s2 m2 center-align' style='margin:4px 0px;padding: 12px 3px;'>
                                                <a class='btn-floating  btn-small waves-effect waves-light core'><i class='material-icons'>remove</i></a><br>
                                            </div>
                                            <div class='col s6 m6' style='margin:2px 0px;padding: 2px 3px;'>
                                                <p class='user'>PENDING EBBILL AMT</p>
                                                <p class='dat'></p>
                                                <p class='dat'>BAL: ".number_format($balance,2,'.','')."</p>
                                            </div>
                                            <div class='col s4 m4 right-align' style='margin:4px 0px;padding: 2px 5px;'>
                                                <p class='amt'>- ".$ebp."</p>
                                            </div>
                                        </div>";
    
                        $j++;
                    }
                    

                    echo $str;

                ?>
               
              
                
                  

                      
                          
                              
                                  
                                      
           </div>
            
           
    </div>

    
    @include('android.bottom')
    <script type="text/javascript">
        $(document).ready(function() 
	    {
           
            var $browser = $(window).height();
            var $content = $('#rech_4');
            var $window = $(window).on('resize', function(){
                var height = $browser - 160;
                height = height - 30;
                $content.height(height);
            }).trigger('resize'); //on page load
           
           
        });
    </script>
</body>
</html>