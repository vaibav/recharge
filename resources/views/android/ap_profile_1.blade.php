<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

   

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='far fa-user fa-2x white-text'></i></a>
            </div>
            <div class="col-5 col-sm-5 col-md-5 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Profile</p>
            </div>
            <div class="col-5 col-sm-5 col-md-5 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_per"><i class='fas fa-percent fa-2x white-text'></i></a>
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_sur"><i class='fas fa-dollar-sign fa-2x white-text'></i></a>
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_pas"><i class='fas fa-key fa-2x white-text'></i></a>
            </div>
        </div>
        
    </div>
    
    
    

    <div style="margin:10px 10px;height:410px;overflow-y:scroll;" id = "rech_1" > 

         
                   
                <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                        
                <?php 

                    $x = [];
                    foreach($profile as $d)
                    {
                        $x = $d;
                    }

                ?>
                       
                 <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Code</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_code'] }}</p> 
                        </div>
                    </div>
                </div>
                    
                <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Name</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_per_name'] }}</p> 
                        </div>
                    </div>
                </div>
                
                <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>City</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_city'] }}</p> 
                        </div>
                    </div>
                </div>

                <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light#ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Mobile No</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_mobile'] }}</p> 
                        </div>
                    </div>
                </div>

               
               <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Setup Fee</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_setup_fee'] }}</p> 
                        </div>
                    </div>
                </div>

                <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>User Type</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_type'] }}</p> 
                        </div>
                    </div>
                </div>
                
                <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Parent</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['parent_name'] }}</p> 
                        </div>
                    </div>
                </div>

                 <div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                        <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                            <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                            <i class='small material-icons '>favorite_border</i></a>
                        </div>
                        <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>Mode</p> 
                            <p class = "text-muted" style='font-size: 14px;margin: 0;'>{{ $x['user_rec_mode'] }}</p> 
                        </div>
                    </div>
                </div>

    </div>

   

    @include('android.common_bottom')

    
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');

            
            

            
            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_per") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_profile_ap_2?auth_token=" + at,'_self');
                   
                }
                else if(gid.indexOf("a_sur") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_profile_ap_3?auth_token=" + at,'_self');
                   
                }
                else if(gid.indexOf("a_pas") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_password_ap_1?auth_token=" + at,'_self');
                   
                }
                else if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                   
                }

         
            });
           

            

           
          
        });
    </script>
</body>
</html>