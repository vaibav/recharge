<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
<meta name="theme-color" content="#E67E22" />
<title>Lakshmi Mobiles</title>
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Roboto+Condensed|PT+Sans&display=swap" rel="stylesheet">

<style type="text/css">
    .floating {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15); position: relative; z-index: 1;
        transition: all .2s ease-in-out; border-radius: 50%; padding: 0; cursor: pointer;
        width: 42px; height: 42px; font-size:14px; padding:10px 5px;
    }
    .floating-1 {
            box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            position: relative; z-index: 1; transition: all .2s ease-in-out; border-radius: 50%; 
            padding: 0; cursor: pointer; width: 37px; height: 37px; font-size:12px; padding:8px 2px;
    }
    .curve-border {border:1px solid #F0B27A;border-radius: 25px;}
    .row { margin: 0; padding: 0 }
</style>