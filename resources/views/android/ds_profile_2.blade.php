<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   
    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='far fa-user fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Percentage</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_back"><i class='fas fa-arrow-left fa-1x white-text'></i></a>
                
            </div>
        </div>
        
    </div>
    

    <div style="margin:0px 0px;height:410px;overflow-y:scroll;" id = "rech_1" > 
   
        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

        <?php 

            $str = "";
            foreach($profile as $d)
            {
                $net_name = "";
                foreach($network as $r)
                {
                    if($d->net_code == $r->net_code)
                    {
                        $net_name = $r->net_name;
                        break;
                    }
                }

                if($d->user_net_per != "0")
                {
                    $str = $str ."div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >
                    <div class='row rounded-pill' style='margin:0px 0px;'>
                            <div class='col-3 col-sm-3 col-md-3 text-center' style='margin:0px 0px;padding: 5px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text'>
                                <i class='small material-icons '>star_border</i></a>
                            </div>
                            <div class='col-8 col-sm-8 col-md-8 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                                <p class = 'text-muted' style='font-size: 14px;margin: 0;'>".$net_name."</p> 
                                <p class = 'text-muted' style='font-size: 14px;margin: 0;'>".$d->user_net_per."</p> 
                            </div>
                        </div>
                    </div>";
                }
            
            }

            echo $str;

        ?>
                        
               
    </div>

    @include('android.common_bottom')

    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_profile_ds_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                   
                }

            });
           
        });
    </script>
</body>
</html>