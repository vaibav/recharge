<!DOCTYPE html>
<html lang="en">
<head>
    @include('android.top')
   
</head>
<body style="background-color: #FFFFFF;">

    <div style="height:120px;background-image: linear-gradient(to bottom, #4F52B1, #6D6FBE);">
        <div class="row">
           
            <div class="col s12 m12 center-align" style="margin-top: 15px;padding: 0px 3px;">
                <img src = "{{ asset('img/brand/logo.png') }}" style=" height: 49px; width: 98px;"/>
            </div>
            
        </div>
    </div>

    <div style="margin-top:-30px;margin-left: 10px;margin-right: 10px;height: 65px;" class="card1 shadow">
            <div class="row" style="margin:0px 0px;">
                <div class="col s7 m7 left-align" style="margin:5px 0px;padding: 0px 10px;">
                    <p class="title" >User Creation</p>
                    <p class="sub-title" >Welcome <?php echo $user_name; ?>!</p>
                </div>
                <div class="col s5 m5 right-align" style="margin:10px 0px;padding: 0px 4px;">
                        <p class="title" style="margin: 5px 2px"><?php echo $user_ubal; ?></p>
                    </div>
            </div>
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(0,0,0,0.5); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 40%;">
            <div class="spinner-layer spinner-red-only">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"> <div class="circle"></div></div>
                <div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    
    <form id="rech_form" class="form-horizontal" action="{{url('mobile_user_store')}}" method="post" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="auth_token" value="{{ $auth_token }}">
    <div style="margin:10px 10px;overflow-y:scroll;height:530px;" class="card1 shadow"  id = "rech_1">
        
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                        <div class="input-field col s12 m12">
                            <input placeholder="name" id="id_user_per_name" name="user_per_name" type="text" class="validate">
                            <label for="id_user_per_name">Full Name</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="city" id="id_user_state" name="user_state" type="text" class="validate">
                            <label for="id_user_state">Area</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="city" id="id_user_city" name="user_city" type="text" class="validate">
                            <label for="id_user_city">City</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="number" id="id_user_mobile" name="user_mobile" type="number" class="validate">
                            <label for="id_user_mobile">Mobile</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="number" id="id_user_phone" name="user_phone" type="number" class="validate">
                            <label for="id_user_phone">WhatsApp</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="ab@abc.com" id="id_user_mail" name="user_mail"  type="text" class="validate">
                            <label for="id_user_mail">Email</label>
                        </div>

                        
                        
                        <div class="input-field col s12 m12 center-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_1">next</button>
                        </div>
                    
            </div>
        </div>
    </div>

    <div style="margin:10px 10px;display: none;overflow-y:scroll;height:530px;" class="card1 shadow"  id = "rech_2">
        
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                        <div class="input-field col s12 m12">
                            <input placeholder="" id="id_user_name" name="user_name" type="text" class="validate">
                            <label for="id_user_name">User Name</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="" id="id_user_pwd" name="user_pwd" type="password" class="validate">
                            <label for="id_user_pwd">Password</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <select id="id_user_acc_type" name="user_acc_type">
                                <?php
                                    if($user_type == "SUPER DISTRIBUTOR")
                                    {
                                        echo "<option value='DISTRIBUTOR'>DISTRIBUTOR</option>";
                                        echo "<option value='API PARTNER'>API PARTNER</option>";
                                    }
                                    else if($user_type == "DISTRIBUTOR")
                                    {
                                        echo "<option value='RETAILER'>RETAILER</option>";
                                    }
                                ?>
                            </select>
                            <label>Account Type</label>
                        </div>

                        <div class="input-field col s12 m12">
                            <input placeholder="" id="id_user_setup_fee"  name="user_setup_fee" type="number" class="validate">
                            <label for="id_user_setup_fee">Setup Fee</label>
                        </div>

                        <div class="row" style = "margin-bottom:45px;margin-left: 5px;">
                            <div class="input-field col s3 m3" style="margin: 3px 0px;">
                                <label><input type="checkbox" id="id_user_rec_mode1" name="user_rec_mode[]" value="WEB"/><span>WEB &nbsp;</span></label>
                            </div>
                            <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                <label><input type="checkbox" id="id_user_rec_mode2" name="user_rec_mode[]" value="GPRS" /><span>GPRS&nbsp;</span></label>
                            </div>
                            <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                <label><input type="checkbox" id="id_user_rec_mode3" name="user_rec_mode[]" value="API" /><span>API&nbsp;</span></label>
                            </div>
                            <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                <label><input type="checkbox" id="id_user_rec_mode4" name="user_rec_mode[]" value="SMS" /><span>SMS&nbsp;</span></label>
                            </div>
                        </div>
                        <div class="input-field col s12 m12" id = "id_f1">
                            <input placeholder="number" id="id_user_api_url_1" name="user_api_url_1" type="text" class="validate">
                            <label for="id_user_api_url_1">API Success URL</label>
                        </div>

                        <div class="input-field col s12 m12" id = "id_f2">
                            <input placeholder="number" id="id_user_api_url_2" name="user_api_url_2" type="text" class="validate">
                            <label for="id_user_api_url_2">API Failure URL</label>
                        </div>
                        
                        <div class="input-field col s6 m6 right-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa orange darken-1" id="btn_2">Back</button>
                        </div>
                        <div class="input-field col s6 m6 left-align" >
                            <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_3">next</button>
                        </div>
                    
            </div>
        </div>
    </div>

    <div style="margin:10px 10px;display: none;overflow-y:scroll;height:530px;" class="card1 shadow" id = "rech_3">
            <p class="title amber-text" style="padding: 0px 10px;">Network Percentage</p>

            <?php 
                $j = 1;
                $str = "";
                foreach($network as $f)
                {
                    if($j == 0) {
                        $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                    }
                    else if(($j % 2) != 0) {
                        $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: white;'>";
                    }
                    else if(($j % 2) == 0) {
                        $str = $str."<div class = 'row' style='margin:0px 0px;padding: 0px 0px;background: #F5EEF8;'>";
                    }
                    

                    $str = $str."<div class='col s7 m7 left-align' style='margin:4px 0px;padding: 12px 10px;'>
                                    <input type='hidden' id='id_net_code_".$j."' name='net_code_".$j."' value='".$f->net_code."' />
                                    <p class='user'>".$f->net_name."</p>
                                </div>";
                    
                    $str = $str . "<div class='col s5 m5' style='margin:2px 0px;padding: 2px 3px;'>
                                        <input type='text' id='id_net_per_".$j."' name='net_per_".$j."' value='0'/>
                                        <input type='hidden' id='id_net_surp_".$j."' name='net_surp_".$j."' value='0' />
                                    </div>";
                    $str = $str."</div>";
                   
                    $j++;
                }

                echo $str;
            ?>
          
              

            <div class = "row" style="margin:0px 0px;padding: 0px 0px;background: white  ;">
                <div class="input-field col s6 m6 right-align" >
                        <button class="btn btn-small waves-effect waves-light #8e24aa orange darken-1" id="btn_4">Back</button>
                </div>
                <div class="input-field col s6 m6 left-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit">Submit</button>
                </div>
            </div>
              
        
        </div>
  
  
    </form>
  

     

    
    
    @include('android.bottom')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$op."');
                });
                </script>";
            session()->forget('msg');
        }
    ?>
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $('select').formSelect();
            $("#id_f1").hide();
            $("#id_f2").hide();
           
            var $browser = $(window).height();
            var $content = $('#rech_2');
            var $content1 = $('#rech_3');
            var $content2 = $('#rech_1');
            var $window = $(window).on('resize', function(){
                var height = $browser - 160;
                height = height - 30;
                $content.height(height);
                $content1.height(height);
                $content2.height(height);
            }).trigger('resize'); //on page load

            $("#id_user_acc_type").change(function(e)
            {
                var ty = $('option:selected', this).val();

                if(ty != "-")
                {
                    if(ty == "API PARTNER")
                    {
                        $("#id_user_api_url_1").val("");
                        $("#id_user_api_url_2").val("");
                        $("#id_f1").show();
                        $("#id_f2").show();
                    }
                    else
                    {
                        $("#id_user_api_url_1").val("*");
                        $("#id_user_api_url_2").val("*");
                        $("#id_f1").hide();
                        $("#id_f2").hide();
                    }
                }
            });

            $('#btn_1').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').show("slow");
                $('#rech_3').hide("slow");
            });

            $('#btn_2').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').show("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').hide("slow");
                
            });

            $('#btn_3').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').show("slow");
            
            });

            $('#btn_4').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').show("slow");
                $('#rech_3').hide("slow");
            });
           
            $("#id_user_name").blur(function(e) 
            {
                e.preventDefault();
                var user_name = $("#id_user_name").val();
                $('#loader').show();

                if (user_name != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_user/" + user_name,
                        success: function (data) {
                            if(data == 1)
                            {
                                $('#loader').hide();
                                $.alert('User Name Already Exists....!');
                                
                                $("#id_user_name").val("");
                                $("#id_user_name").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_name").focus();
                }
                   
            });

            $("#id_user_mobile").blur(function(e) 
            {
                e.preventDefault();
                var user_mobile = $("#id_user_mobile").val();
                $('#loader').show();

                if (user_mobile != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_mobile/" + user_mobile,
                        success: function (data) {
                            if(data > 0)
                            {
                                $('#loader').hide();
                                $.alert('Mobile No Already Exists....!');
                                $("#id_user_mobile").val("");
                                $("#id_user_mobile").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_mobile").focus();
                }
                   
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                var z = check_data();
                $('#btn_submit').prop('disabled', true);

                if(z[0] == 0)
                {
                    $.confirm({
                        title: 'User Creation!',
                        content: 'Are You Sure?',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('mobile_user_store')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                }
                else
                {
                    $.alert(z[1]);
                    $('#btn_submit').prop('disabled', false);
                }
                
            }); 

            function check_data()
            {
                var z = [];
                z[0] = 0;
                z[1] = "done";

                if($('#id_user_per_name').val() == "") {
                    z[0] = 1;
                    z[1] = "Person Name is Empty..";
                }
                else  if($('#id_user_city').val() == "") {
                    z[0] = 1;
                    z[1] = "City is Empty..";
                }
                else  if($('#id_user_mobile').val() == "") {
                    z[0] = 1;
                    z[1] = "Mobile No is Empty..";
                }
                else  if($('#id_user_mail').val() == "") {
                    z[0] = 1;
                    z[1] = "mail is Empty..";
                }
                else  if($('#id_user_name').val() == "") {
                    z[0] = 1;
                    z[1] = "user name is Empty..";
                }
                else  if($('#id_user_pwd').val() == "") {
                    z[0] = 1;
                    z[1] = "Password is Empty..";
                }
                
                return z;
            }


        });
    </script>
</body>
</html>