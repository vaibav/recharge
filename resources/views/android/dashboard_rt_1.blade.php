<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <style>
        .floating-2 {
            box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
            position: relative; z-index: 1; transition: all .2s ease-in-out; border-radius: 50%; 
            padding: 0; cursor: pointer; width: 32px; height: 32px; font-size:12px; padding:6px 2px;
    }
        </style>
    </head>
<body style="background-color: #ffffff;overflow:hidden;height: 90vh;">
    
    @include('android.common_loader')

    <div class = "waves-effect waves-light" style="height:200px;background:#8E44AD;">
        <div class="row">
            
            <div class="col-8 col-sm-8 col-md-8 text-left" style="margin-top: 25px; margin-left: 1px;padding:2px 5px;">
                <h5 class="card-title white-text text-darken-4" style = "margin-left:65px;"><a>Lakshmi Mobiles</a></h5>
                <p class="h5  " ></p>  
            </div>
            <div class="col-3 col-sm-3 col-md-3 text-right" style="margin-top: 25px;">
                <p><a href = "logout:" id = "a_log"><i class="fas fa-sign-out-alt fa-1x white-text pr-1" aria-hidden="true"></i></a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-8 col-sm-8 col-md-8 text-left" style="margin-top: 4px; margin-left: 15px;padding:2px 5px;">
                <p class="h5 white-text" 
                        style="margin:0px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:16px;">
                        Hello <?php echo $user_name ?>!</p>
                
            </div>
            <div class="col-3 col-sm-3 col-md-3 text-right" style="margin-top: 4px;padding:2px 5px;">
                <p class="h5 white-text" 
                        style="margin:0px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:16px;">
                        {{ $user_bal }}</p>
            </div>
        </div>

        <div class="row" style="margin:2px 15px;">
            <div class="col-12 col-sm-12 col-md-12 text-left" style="margin:8px 2px; padding:2px 5px;">
                <marquee><p class = "text-muted white-text" style="margin-bottom: 4px;">Welcome to Lakshmi Mobiles Recharge Portal...</p></marquee>
            </div>
            
        </div>
    </div>
    

    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

    <div class="row text-center">
        <div class="col-11 col-sm-11 col-md-11 text-left card z-depth-2"
                     style="margin-top:-55px;margin-left: 15px;margin-right: 1px;height: 150px;padding: 2px 2px;border-radius: 15px;">

            <canvas id="myChart" style = "height:150px"></canvas>

            
                
        </div> 
        
    </div>
    

    <div style = "overflow:auto; position:absolute; top:305px; left:2px; right:2px; bottom:2px;" id = "rech_1">
     
        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c1">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-mobile-alt fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Recharge</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c2">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fab fa-medapps fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Bill Payment</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c3">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-rupee-sign fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Money Transfer</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c4">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-carrot fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Payment Request</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c5">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-coins fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">My Account</p>
                </div>
            </div>
        </div>

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c6">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='far fa-chart-bar fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Recharge Reports</p>
                </div>
            </div>
        </div>

        
        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c7">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='fas fa-fighter-jet fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Complaints</p>
                </div>
            </div>
        </div>
        

        <div class=" card z-depth-2 rounded-pill" style="margin:10px 15px;padding:0px 2px;" id = "c8">
            <div class = "row ">
                <div class="col-3 col-sm-3 col-md-3 text-center " style="margin:0px 0px;padding: 3px 1px;padding-top:4px;padding-bottom:2px;">
                    <a class='floating-2 btn-small waves-effect waves-light #00acc1 secondary-color white-text text-center' 
                        id = "a_ic"><i class='far fa-user fa-1x white-text'></i> </a>
                </div>
                <div class="col-9 col-sm-9 col-md-9 text-left " style="margin:0px 0px;padding: 0px 5px;">
                    <p class=" #0288d1 light-blue-text text-darken-3 " 
                    style="margin:5px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Profile</p>
                </div>
            </div>
        </div>

        
        
    </div>
    

   
    @include('android.common_bottom')

    <script src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                   
                }

         
            });


            $('#c1').on('click',function(e)
            {
                
                window.open("va_rech:" ,'_self');

            }); 

            $('#c2').on('click',function(e)
            {
                window.open("va_bill:" ,'_self');

            }); 

            $('#c3').on('click',function(e)
            {
                window.open("va_money:" ,'_self');

            }); 

             $('#c4').on('click',function(e)
            {
                
                window.open("va_request:" ,'_self');

            }); 

            $('#c5').on('click',function(e)
            {
                window.open("va_account:" ,'_self');

            }); 

            $('#c6').on('click',function(e)
            {
                window.open("va_report:" ,'_self');

            }); 
            $('#c7').on('click',function(e)
            {
                window.open("va_complaint:" ,'_self');

            }); 

            $('#c8').on('click',function(e)
            {
                window.open("va_profile:" ,'_self');

            }); 


            load_data2();


            function load_data2()
            {
                var at = $('#auth_token').val();

                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'mobile_chart_data_rt',
                    type: 'GET',
                    data : {auth_token:at},
                    dataType: "json",
                    success: function( data ){
                        var res = data['res'];
                        var ref = data['ref'];
                        var red = data['dat']
                        chart_data(res, ref, red);
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function chart_data(res, ref, red)
            {
                new Chart(document.getElementById("myChart"), {
                    type: 'line',
                    data: {
                        labels: red,
                        datasets: [{ 
                            data: res,
                            label : "Succes",
                            borderColor: "#52BE80",
                            fill: true
                        }, { 
                            data: ref,
                            label : "Failure",
                            borderColor: "#CD6155",
                            fill: true
                        }
                        ]
                    },
                    options: {
                       
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }

          
        });
    </script>
</body>
</html>