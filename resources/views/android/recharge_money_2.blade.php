<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;" class="card z-depth-1">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-7 col-sm-7 col-md-7 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Money Transfer</p>
            </div>
            <div class="col-3 col-sm-3 col-md-3 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                
            </div>
        </div>
        
    </div>

    

    <div style="margin:10px 10px;border-left: 5px solid #F0B27A;" class="card z-depth-1"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
            
                        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
                        <input type="hidden" id = "msisdn" name="msisdn" value="{{ $msisdn }}">
                            <div style="height:24px"></div>
                           
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-center">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" 
                                        id="btn_1" style = 'width:220px;'><i class="fas fa-rupee-sign"></i> Money Transfer</button>
                                </div>
                            </div>

                            <div style="height:24px"></div>
                        
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-center">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" 
                                        id="btn_2" style = 'width:220px;'><i class="fas fa-plus-circle"></i> Add Beneficiary</button>
                                </div>
                            </div>

                            <div style="height:24px"></div>
                        
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-center">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" 
                                        id="btn_3" style = 'width:220px;'><i class="far fa-trash-alt"></i> delete Beneficiary</button>
                                </div>
                            </div>

                            <div style="height:24px"></div>
                    
            </div>
        </div>
    </div>

    @include('android.common_bottom')
    
   

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();
            
            $('#btn_1').on('click',function(e)
            {
                e.preventDefault();
                var at = $('#auth_token').val();
                var ms = $('#msisdn').val();

                window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + ms + "/" + at, '_self');

            }); 

            $('#btn_2').on('click',function(e)
            {
                e.preventDefault();
                var at = $('#auth_token').val();
                var ms = $('#msisdn').val();

                window.open("<?php echo url('/'); ?>/mobile_user_money_ab_1/" + ms + "/" + at, '_self');

            }); 

            $('#btn_3').on('click',function(e)
            {
                e.preventDefault();
                var at = $('#auth_token').val();
                var ms = $('#msisdn').val();

                window.open("<?php echo url('/'); ?>/mobile_user_money_db_1/" + ms + "/" + at,'_self');

            }); 


        });
    </script>
</body>
</html>