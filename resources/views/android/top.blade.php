<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>VaibavOnline</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Roboto+Condensed|PT+Sans&display=swap" rel="stylesheet">

<style type="text/css">
    .shadow { box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); }
    .card1 { word-wrap: break-word; background-color: #fff; background-clip: border-box; border: 1px solid #e3e6f0; border-radius: .35rem; }
    .title { font-family: 'Roboto Condensed', sans-serif; font-size: 19px;font-weight: 550;color: #6D6FBE;margin: 2px 2px; }
    .sub-title { font-family: 'Roboto ', sans-serif;font-size: 12px;font-weight: 450;color: rgb(73, 74, 100);margin: 2px 2px;}
    .core { background-color: #6D6FBE;}
    .user {margin:2px 0px;font-size: 14px;font-weight: 550;color: #5D6D7E;font-family: 'Roboto', sans-serif;}
    .dat {margin:2px 0px;font-size: 14px;font-weight: 460;color: #99A3A4  ;font-family: 'Roboto Condensed', sans-serif; }
    .amt { margin:18px 0px;font-size: 15px;font-weight: 550;color: #6D6FBE  ;font-family: 'Roboto Condensed', sans-serif; }

</style>