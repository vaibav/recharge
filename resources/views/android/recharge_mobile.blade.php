<!DOCTYPE html>
<html lang="en">
<head>
    @include('android.common_top')
    <style>
.ad-box {margin:5px; border-radius: 10px;position: absolute; bottom: 20px; z-index:1000;width: 92%;}
#header { position: relative; min-height: 150px; text-align: center; padding: 10px; }
#header img { max-width: 155px; position: relative; margin-top: -10px; }
#ribbon { background-color: #8E44AD; width: 235px; padding: 5px 10px; position: relative; margin-top: -78px; margin-left: auto; margin-right: auto; font-family: 'Shadows Into Light Two', cursive;
  font-size: 10px; letter-spacing: 2px; color: #ffffff; text-align: center; box-shadow: 0 2px 6px #999999; }

#ribbon:before, #ribbon:after { content: ""; width: 3px; bottom: 18px; position: absolute; display: block; border: 15px solid #8E44AD; box-shadow: 0 8px 6px -6px #999999; z-index: -2; }
#ribbon:before { left: -30px; border-right-width: 35px; border-left-color: transparent; }
#ribbon:after { right: -30px; border-left-width: 35px; border-right-color: transparent; }

.popupCloseButton {
    background-color: #E74C3C; border: 3px solid #45B39D; color:white; border-radius: 50px; cursor: pointer; 
    display: inline-block; font-family: arial; font-weight: bold; position: absolute; top: -20px; right: 65px;
    font-size: 25px; line-height: 30px; width: 35px; height: 35px; text-align: center;
}
.popupCloseButton:hover { background-color: #ccc; }

.wraper { position: relative; overflow: hidden; height: 25px; width: 218px; }
.wraper p { position: absolute; margin-bottom:5px;color:white;font-weight: bold; line-height: 25px; white-space: nowrap; animation: marquee 9s linear infinite; }

@keyframes marquee {
  0% { transform: translateX(100%); }
  100% { transform: translateX(-100%); }
}
    </style>
</head>
<body style="background-color: #ffffff;overflow:hidden;height: 90vh;">

<?php
    $i = 1;
    if($adver != null)
    {
        $str = "";

        $path = "";
        
        foreach($adver as $c)
        {
            if($c->ad_photo != "")
            {
                $path = asset('img/adv/'.$c->ad_photo);
                
            }
?>
    <div class="ad-box" id ="a_{{$i}}" style="display: none;">
        <div id="header">
          <img src="{{$path}}" style='height:60px;width:140px;' class='image-fluid' />
          <div class="popupCloseButton">&times;</div>
        </div>
        <div id="ribbon">
          <div class="wraper" id = "b_1" ><p>{{$c->ad_data}}</p></div>
        </div>
    </div>

<?php
            $str = $str. "#a_".$i.",";
            $i++;
        }
       
        
    }

    if($i != 0) {
        $str = substr($str, 0, -1);
        $i = $i-1;
    }

    echo "<input type ='hidden' id = 'ad_div' value = '".$str."'>";
    echo "<input type ='hidden' id = 'div_count' value = '".$i."'>";
   
?>

    
    @include('android.common_loader')
    @include('android.common_title')
    
    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 85px;border-radius:15px" class="card z-depth-2">
        <div class="card-body" style = "padding:0.7rem">
            <div class="row" style="margin:0px 0px;">
                <div class="col-4 col-sm-4 col-md-4 text-left" style="margin:2px 0px;padding: 0px 4px;">
                    <p class="card-text" style="margin-bottom: 4px;"><a><?php echo $user_name ?>!</a></p>
                    <p class="card-text" style="margin-bottom: 4px;"> {{ $user_bal }}</p>
                </div>
                <div class="col-8 col-sm-8 col-md-8 text-right" style="margin:2px 0px;padding: 0px 4px;">
                    <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a1" style = "height:42px;width:42px;"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>

                    <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a2" style = "height:42px;width:42px;"><i class='fas fa-satellite-dish fa-2x white-text'></i></a>

                    <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a3" style = "height:42px;width:42px;"><i class='fas fa-phone fa-2x white-text'></i></a>

                    <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a4" style = "height:42px;width:42px;"><i class='fas fa-filter fa-2x white-text'></i></a>

                   
                </div>
            </div>
            
        </div>
    </div>

     <!-- Prepaid Recharge-->
     <div style="margin:6px 10px;" class="card z-depth-2 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Prepaid</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
            </div>
        </div>
        
    </div>
   
    <div style="overflow:auto; position:absolute; top:220px; left:10px; right:10px; bottom:15px;border-radius:15px;" class="card z-depth-2"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                <form id="rech_form1" class="form-horizontal" action="{{url('recharge_mobile_prepaid_store1')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                    <br>

                    <div class="row pl-1 pr-1">
                        <div class="col-8 col-sm-8 col-md-8 pl-1 pr-1">
                            <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;" 
                                        placeholder="Mobile No" id="id_user_mobile_1" name="user_mobile_1">

        
                            <div style="height:15px;"></div>
                            <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;" 
                                        placeholder="Amount" id="id_user_amount_1" name="user_amount_1">

                            <div style="height:15px;"></div>
                            <div class="row pl-1 pr-1">
                                <div class="col-12 col-sm-12 col-md-12 text-left pl-0 pr-0">
                                        <a class="floating btn-small waves-effect waves-light#ff8f00 amber darken-3 white-text" id="plan_121" >121</a>
                                        <a class="floating btn-small waves-effect waves-light #ff6d00 orange accent-4 white-text" id="plan_all">Plan</a>
                                        <a class="floating btn-small waves-effect waves-light #ff6d00 orange accent-4 white-text" id="plan_con" >
                                            <i class="far fa-address-book fa-2x" style = "padding-left:5px"></i></a>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 text-right pl-0 pr-0">
                                        <button class="btn  waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit_1">submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-4 col-sm-4 col-md-4 text-center pl-1 pr-1">
                            <!-- Add Netwokd to this Area-->
                            <?php
                                foreach($network1 as $f)
                                {
                                    $path = asset('networkpng/'.$f->net_code.'.png');

                                    
                                    echo "<div class='form-check' style='margin-bottom: 6px;'>
                                            <label class='form-check-label text-muted' style='font-size: 14px;'>
                                                <input type='radio' class='form-check-input' name='net_code_1' 
                                                        id='id_net_code_".$f->net_code."' value ='".$f->net_code."'>
                                                <img src='".$path."' style='height: 30px;width: 80px' />
                                            </label>
                                        </div>";
                                    
                                }   
                            ?> 

                            
                            
                            <!-- End Network-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Dth Recharge-->
    <div style="margin:10px 10px;display:none;" class="card z-depth-2 rounded-pill" id = "rech_22">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-satellite-dish fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Dth</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
            </div>
        </div>
        
    </div>
    
    
    <div style="overflow:auto; position:absolute; top:220px; left:10px; right:10px; bottom:15px;border-radius:15px;display: none;" class="card z-depth-2"  id = "rech_2">
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                <form id="rech_form2" class="form-horizontal" action="{{url('recharge_mobile_dth_store1')}}" method = "POST" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                <br>

                <div class="row pl-1 pr-1">
                    <div class="col-8 col-sm-8 col-md-8 pl-1 pr-1">
                        <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;"
                                placeholder="Dth No" id="id_user_mobile_2" name = "user_mobile_2">
            
                        <div style="height:15px;"></div>
                        <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;" 
                                    placeholder="Amount" id="id_user_amount_2" name = "user_amount_2">

                        <div style="height:15px;"></div>
                        <div class="row pl-1 pr-1">
                            <div class="col-12 col-sm-12 col-md-12 text-left pl-0 pr-0">
                                <a class="floating btn-small waves-effect waves-light#ff8f00 amber darken-3 white-text" id="plan_dth" >info</a>
                                <a class="floating btn-small waves-effect waves-light#ff8f00 amber darken-3 white-text" id="plan_dth_plan" >plan</a>
                                <a class="floating btn-small waves-effect waves-light #ff6d00 orange accent-4 white-text" id="plan_con_2" >
                                        <i class="far fa-address-book fa-2x" style = "padding-left:5px"></i></a>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 text-right pl-0 pr-0">
                                <button class="btn  waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit_2">submit</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-4 col-sm-4 col-md-4 text-center pl-1 pr-1">
                        <!-- Add Netwokd to this Area-->
                        <?php
                            foreach($network3 as $f)
                            {
                                $path = asset('networkpng/'.$f->net_code.'.png');
                                echo "<div class='form-check' style='margin-bottom: 6px;'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='net_code_2' 
                                                    id='id_net_code_".$f->net_code."' value ='".$f->net_code."'>
                                            <img src='".$path."' style='height: 30px;width: 80px' />
                                        </label>
                                    </div>";
                                
                            }   
                        ?> 
                        <!-- End Network-->
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <!-- Postpaid Recharge-->
    <div style="margin:10px 10px;display:none;" class="card z-depth-2 rounded-pill" id = "rech_33">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-phone fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Postpaid</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
            </div>
        </div>
        
    </div>
    
        
    <div style="overflow:auto; position:absolute; top:220px; left:10px; right:10px; bottom:15px;border-radius:15px;display: none;" class="card z-depth-2"  id = "rech_3">
            
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                <form id="rech_form3" class="form-horizontal" action="{{url('recharge_mobile_postpaid_store1')}}" method="post" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                <br>

                <div class="row pl-1 pr-1">
                    <div class="col-8 col-sm-8 col-md-8 pl-1 pr-1">
                        <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;"
                                placeholder="Mobile No" id="id_user_mobile_3" name = "user_mobile_3">
            
                        <div style="height:15px;"></div>
                        <input class="form-control curve-border" type="number" style="padding:3px 10px; font-size:1.3rem;" 
                                placeholder="Amount" id="id_user_amount_3" name = "user_amount_3">

                        <div style="height:15px;"></div>
                        <div class="row pl-1 pr-1">
                            <div class="col-4 col-sm-4 col-md-4 text-left pl-0 pr-0">
                                <a class="floating btn-small waves-effect waves-light #ff6d00 orange accent-4 white-text" id="plan_con_3" >
                                        <i class="far fa-address-book fa-2x" style = "padding-left:5px"></i></a>
                            </div>
                            <div class="col-8 col-sm-8 col-md-8 text-right pl-0 pr-0">
                                <button class="btn  waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit_3">submit</button>
                            </div>
                        </div>
                    </div>

                    <div class="col-4 col-sm-4 col-md-4 text-center pl-1 pr-1">
                        <!-- Add Netwokd to this Area-->
                        <?php
                            foreach($network2 as $f)
                            {
                                if($f->net_code == "824") {
                                    $path = asset('networkpng/857.png');
                                }
                                else if($f->net_code == "259") {
                                    $path = asset('networkpng/636.png');
                                }
                                else if($f->net_code == "973") {
                                    $path = asset('networkpng/336.png');
                                }
                                else if($f->net_code == "686") {
                                    $path = asset('networkpng/781.png');
                                }
                                else if($f->net_code == "590") {
                                    $path = asset('networkpng/372.png');
                                }
                                else if($f->net_code == "972") {
                                    $path = asset('networkpng/510.png');
                                }
                                else {
                                    $path = asset('networkpng/'.$f->net_code.'.png');
                                }
                                echo "<div class='form-check' style='margin-bottom: 6px;'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='net_code_3' 
                                                    id='id_net_code_".$f->net_code."' value ='".$f->net_code."'>
                                            <img src='".$path."' style='height: 30px;width: 80px' />
                                        </label>
                                    </div>";
                                
                            }   
                        ?> 
                        <!-- End Network-->
                    </div>
                </div>

            </form>
            </div>
        </div>
    </div>

    <!-- Report-->
    <div style="margin:10px 10px;display:none;" class="card z-depth-2 rounded-pill" id = "rech_44">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-arrows-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Report</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
            </div>
        </div>
    </div>
    

    <div style="overflow:auto; position:absolute; top:220px; left:0px; right:0px; bottom:15px;display: none;" id = "rech_4">
        <?php echo $recharge; ?>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
        <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
        <div class="modal-dialog modal-dialog-centered modal-sm mw-100" role="document" style = "width:90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style = "max-height: calc(100vh - 143px);overflow-y: auto;">
                    <table class="table table-striped table-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    @include('android.common_bottom')
    
    

      <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Congratulations!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                    
                });
                </script>";
            }
            else  
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();
            
            $('#a1').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').show("slow"); $('#rech_2').hide("slow"); $('#rech_3').hide("slow"); $('#rech_4').hide("slow");
                $('#rech_11').show("slow"); $('#rech_22').hide("slow"); $('#rech_33').hide("slow"); $('#rech_44').hide("slow");
            });

            $('#a2').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_2').show("slow"); $('#rech_3').hide("slow"); $('#rech_4').hide("slow");
                $('#rech_11').hide("slow"); $('#rech_22').show("slow"); $('#rech_33').hide("slow"); $('#rech_44').hide("slow");
            });

            $('#a3').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_2').hide("slow"); $('#rech_3').show("slow"); $('#rech_4').hide("slow");
                $('#rech_11').hide("slow"); $('#rech_22').hide("slow"); $('#rech_33').show("slow"); $('#rech_44').hide("slow");
            });

            $('#a4').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_2').hide("slow"); $('#rech_3').hide("slow"); $('#rech_4').show("slow");
                $('#rech_11').hide("slow"); $('#rech_22').hide("slow"); $('#rech_33').hide("slow"); $('#rech_44').show("slow");
            });


            //Advertisement
            var counter = 0;
            var ad_value = $('#ad_div').val();
            var d_count = $('#div_count').val();
            var divs = $(ad_value);
            showDiv(); // show first div  

            function showDiv () 
            {
                if(d_count > 0)
                {
                    
                        if(counter != 0)
                        {
                            var inx = counter % d_count;
                            if(inx == 0) {
                                counter = 0;
                            }
                        }
                    
                        divs.hide() // hide all divs
                            .filter(function (index) { return index == counter % d_count; }) 
                            .show('slow'); // and show it
                        counter++;
                }
                
            }

             var myVar = setInterval(function () {
            showDiv(); // show next div
        }, 10 * 1000); // do this every 10 seconds   

        

          $(".popupCloseButton").click(function(){
            clearInterval(myVar);
            divs.hide();
          });

            $("#id_user_mobile_1").keyup(function (e) {
                var mob = $("#id_user_mobile_1").val();
                if(mob.length == 10)
                {
                    getOperatorCheck(mob);
                }
            });

            $("#id_user_mobile_2").blur(function (e) {
                var mob = $("#id_user_mobile_2").val();
                getDthOperatorCheck(mob);
            });

            $('#id_add_b').on('click',function(e)
            {
                $('#id_add').hide();
            });

                     
            function getOperatorCheck(mob)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'rech_operator?user_mobile=' + mob,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        //alert(opr);
                        var d = 1;
                        if(opr != "" && opr != "0")
                        {
                            //alert(opr);
                            if(opr == "AIRTEL")    
                                $('#id_net_code_857').prop("checked", true);
                            else if(opr == "VODAFONE")    
                                $('#id_net_code_636').prop("checked", true);
                            else if(opr == "BSNL")    
                                $('#id_net_code_510').prop("checked", true);
                            else if(opr == "BSNL GSM")    
                                $('#id_net_code_510').prop("checked", true);
                            else if(opr == "DOCOMO")    
                                $('#id_net_code_781').prop("checked", true);
                            else if(opr == "JIO")    
                                $('#id_net_code_372').prop("checked", true);
                            else if(opr == "Reliance Jio Infocomm Limited")    
                                $('#id_net_code_372').prop("checked", true);
                            else if(opr == "IDEA")    
                                $('#id_net_code_336').prop("checked", true);
                        }
                        
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }

            function getDthOperatorCheck(mob)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'api/plan/web/dth/operator?mobile=' + mob,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        //alert(opr);
                        var d = 1;
                        if(opr != "" && opr != "0")
                        {
                            if(opr == "VIDEOCON")    
                                $('#id_net_code_719').prop("checked", true);
                            else if(opr == "AIRTELDTH")    
                                $('#id_net_code_786').prop("checked", true);
                            else if(opr == "SUNDIRECT")    
                                $('#id_net_code_547').prop("checked", true);
                            else if(opr == "TATASKY")    
                                $('#id_net_code_227').prop("checked", true);
                            else if(opr == "DISHTV")    
                                $('#id_net_code_202').prop("checked", true);
                            else if(opr == "BIGTV")    
                                $('#id_net_code_536').prop("checked", true);
                        }
                        
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }

            function hasWhiteSpace(s) {
                return s.indexOf(' ') >= 0;
            }

            $('#btn_submit_1').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_1').prop('disabled', true);

                var un = $("input[name='net_code_1']:checked").val(); 
                var um = $('#id_user_mobile_1').val();
                var ua = $('#id_user_amount_1').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "" && un != null)
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_1').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form1').attr('action', "{{url('recharge_mobile_prepaid_store1')}}");
                                    $('#rech_form1').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                else if(un == "" || un == null)
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                
                
            }); 


            $('#btn_submit_2').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_2').prop('disabled', true);

                var un = $("input[name='net_code_2']:checked").val(); 
                var um = $('#id_user_mobile_2').val();
                var ua = $('#id_user_amount_2').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "" && un != null)
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_2').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form2').attr('action', "{{url('recharge_mobile_dth_store1')}}");
                                    $('#rech_form2').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                else if(un == "" || un == null)
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                
                
            }); 

             $('#btn_submit_3').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_3').prop('disabled', true);

                var un = $("input[name='net_code_3']:checked").val(); 
                var um = $('#id_user_mobile_3').val();
                var ua = $('#id_user_amount_3').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "" && un != null)
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_3').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form3').attr('action', "{{url('recharge_mobile_postpaid_store1')}}");
                                    $('#rech_form3').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                else if(un == "" || un == null)
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                
                
            }); 

            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code =  $("input[name='net_code_1']:checked").val(); 
                var usr_mobe = $('#id_user_mobile_1').val();
                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    getMobileOffer(net_code, usr_mobe);
                }
        
            });

            function getMobileOffer(net, mob)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'m_offer?net_code=' + net + "&user_mobile=" + mob,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        $('#rech_offer_body').html(opr);
                        $('#modal1').modal('show');

                        $('#loader').hide(); 

                        
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }

            $('#plan_all').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code =  $("input[name='net_code_1']:checked").val(); 
               
                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    getMobilePlanAll(net_code);
                }
        
                
            });
            
            $('#plan_con').on('click',function(e)
            {
                e.preventDefault();
               
               
        
                
            });

            function getMobilePlanAll(net)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'m_plan?net_code=' + net,
                    type: 'GET',
                    success: function( response ){
                        var opr = response;
                        $('#rech_offer_body').html(opr);
                        $('#modal1').modal('show');

                        $('#loader').hide(); 

                        
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }

            $(document).on('click', 'a', function(e){ 
                var amt = $(this).text();
                var id = this.id;
                if(id == 'id_off')
                {
                    $('#id_user_amount_1').val(amt);
                    $('#modal1').modal('hide');
                }
            });

            $('#plan_dth').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code = $("input[name='net_code_2']:checked").val(); 
                var usr_mobe = $('#id_user_mobile_2').val();
                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    getDthInfo(usr_mobe, net_code);
                }
        
                
            });

            function getDthInfo(mob, network)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                     url:'api/plan/web/dth/customercheck?operator=' + network + "&mobile=" +mob,
                    type: 'GET',
                    success: function( response ){
                        $('#rech_offer_body').html(response);
                        $('#modal1').modal('show');
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }

             $('#plan_dth_plan').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code = $("input[name='net_code_2']:checked").val(); 
                
                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#loader').show(); 

                    $.ajax({
                        url:'api/plan/web/dth/plans?operator=' + net_code,
                        type: 'GET',
                        success: function( response ){
                            $('#rech_offer_body').html(response);
                            $('#modal1').modal('show');
                            $('#loader').hide(); 
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            $('#loader').hide(); 
                        }
                    });
                }
        
                
            });
    
        });
    </script>
</body>
</html>