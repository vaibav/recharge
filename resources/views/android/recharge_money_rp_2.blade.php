<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                    <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['user_name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['msisdn']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo $data['total_limit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consume_limit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaining_limit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Report</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
              
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_back">
                                <i class="fas fa-arrow-circle-left fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>
    

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form1" class="form-horizontal" action="{{url('mobile_user_money_rp_3')}}" method="get" accept-charset="UTF-8">

                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                        <input type="hidden" id = "msisdn" name="msisdn" value="<?php echo $data['msisdn']; ?>">
                        

                        <div style="height:17px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                <input class="form-control curve-border" type="text"
                                placeholder="From Date" id="f_date" name = "f_date">
                            </div>
                            
                        </div>
                            

                        <div style="height:17px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="text" 
                                placeholder="To Date" id="t_date" name = "t_date">
                            </div>
                            
                        </div>
                        <div style="height:17px"></div>

                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="number"
                                placeholder="Customer Mobile No" id="user_mobile"  name = "user_mobile">
                            </div>
                        </div>
                        <div style="height:17px"></div>

                        
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12 text-right">
                                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                            </div>
                        </div>
                        

                        
                    </form>
            </div>
        </div>
       


        </div>

         

       

        
       

   

    @include('android.common_bottom')
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
                
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $('#f_date').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });

            $('#t_date').datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                autoclose: true
            });

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + ms + "/" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
               
            });

           
        });
    </script>
</body>
</html>