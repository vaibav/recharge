<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">My Account</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
               
                
            </div>
        </div>
        
    </div>

   
    <div style="margin:10px 10px;height:410px;overflow-y:scroll;border-radius:15px;" id = "rech_1" class="card z-depth-2"> 

        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                <form id="rech_form" class="form-horizontal" action="{{url('mobile_user_stock_ds_2')}}" method="get" accept-charset="UTF-8">

                    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                    

                    <div style="height:20px"></div>
                    <div class="row pl-3 pr-3">
                        <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                            <input class="form-control curve-border" type="text"
                            placeholder="From Date" id="f_date" name = "f_date">
                        </div>
                        
                    </div>
                        
                    <div style="height:20px"></div>
                    <div class="row pl-3 pr-3">
                        <div class="col-12 col-sm-12 col-md-12">
                            <input class="form-control curve-border" type="text" 
                            placeholder="To Date" id="t_date" name = "t_date">
                        </div>
                        
                    </div>

                    <div style="height:20px"></div>
                    <div class="row pl-3 pr-3">
                        <div class="col-12 col-sm-12 col-md-12 text-right">
                                <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>



@include('android.common_bottom')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">
$(document).ready(function() 
{
    
    $('#loader').hide();

    $(window).resize(function() {
        $('#rech_1').height($(window).height() - 250);
    });

    $(window).trigger('resize');

    $('#f_date').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        autoclose: true
    });

    $('#t_date').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        autoclose: true
    });


    $(document).on('click', "a",function (e) 
    {
        e.preventDefault();
        var gid = this.id;
        
        if(gid.indexOf("a_log") !== -1)
        {
            window.open("logout:" ,'_self');
        }
 
    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        $('#btn_submit').prop('disabled', true);

        var rt = $('option:selected', "#rech_type").val();
        var fd = $('#f_date').val();
        var td = $('#t_date').val();
        var at = $('#auth_token').val();
      
        
        //alert(ax);
        if(rt != "" && fd != "" && td != "")
        {

            var z =  date_check(fd, td);

            if(z == 0)
            {
                $.confirm({
                    title: 'Stock Report!',
                    content: 'Are You Sure? ',
                    type: 'purple',
                    typeAnimated: true,
                    buttons: {
                        close: function () {
                            $.alert('Canceled!');
                            $('#btn_submit').prop('disabled', false);
                        },
                        tryAgain: {
                            text: 'OK',
                            btnClass: 'btn-purple',
                            action: function(){
                                $('#rech_form').attr('action', "{{url('mobile_user_stock_ds_2')}}");
                                $('#rech_form').submit();
                                $('#loader').show();
                                
                            }
                        }
                        
                    }
                });
            }
            else if(z == 1)
            {
                $.alert('Maximum 10 days range! Please change dates!');
                $('#btn_submit').prop('disabled', false);
            }
            else if(z == 2)
            {
                $.alert('Invalid days range! Please change dates!');
                $('#btn_submit').prop('disabled', false);
            }
            
        }
        else if(rt == "")
        {
            $.alert('Please Select Recharge Type!');
            $('#btn_submit').prop('disabled', false);
        }
        else if(fd == "")
        {
            $.alert('Please Select From Date!');
            $('#btn_submit').prop('disabled', false);
        }
        else if(td == "")
        {
            $.alert('Please Select From Date!');
            $('#btn_submit').prop('disabled', false);
        }
        
        
    }); 

    function date_check(d1, d2)
    {
        var date1 = new Date(d1); 
        var date2 = new Date(d2); 
        var z = 0;
        
        var Difference_In_Time = date2.getTime() - date1.getTime(); 
        var t_days = Difference_In_Time / (1000 * 3600 * 24); 
        
        if(t_days > 10)
        {
            z = 1;
        }
        else if(t_days < 0)
        {
            z = 2;
        }

        return z; 
    }

   
  
});
</script>
</body>
</html>