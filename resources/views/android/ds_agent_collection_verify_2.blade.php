<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
        
    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-2 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Verification</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                    id = "a_back"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>

    <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style="margin:0px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($collection as $f)
            {
                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
                $str = $str."<div class='row rounded-pill' style='margin:0px 0px;'>"; 
                
                $str = $str."<div class='col-7 col-sm-7 col-md-7 text-left' style='margin:0px 0px;padding: 5px 8px;'>"; 
                $str = $str."<p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->pay_amount."</p>";
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->user_name."</p>"; 
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->bill_no."</p>"; 
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->pay_date."</p>"; 
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->pay_mode."</p>"; 
                $str = $str."<p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->pay_option."</p>"; 

                $str = $str."</div><div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 5px 4px;'>
                        <div class='row' style='margin:0px 5px;'>
                            <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                <a class='floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2 white-text text-center' 
                                id='failure_".$f->trans_id."'><i class='small material-icons '>clear</i></a>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                <a class='floating btn-small waves-effect waves-light cyan white-text text-center'
                                id='success_".$f->trans_id."'><i class='small material-icons '>check</i></a>
                            </div>
                        </div>
                    </div>"; 

                $str = $str."</div></div>";
            
                $j++;
            }

            echo $str;
        ?>
    </div>
   
    @include('android.common_bottom')

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $res = $op1['output'];

            echo "<script>
            $(document).ready(function() 
            {
                $.alert('".$res."');
            });
            </script>";
            
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 250);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
               
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_collection_verify_ds_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                }

                if(gid.indexOf("success_") !== -1)
                {
                    var nid = gid.split("success_");
                    var t_id = nid[1];
                    var a_th = $('#auth_token').val();
                   
                    $.confirm({
                        title: 'Confirmation',
                        content: 'Are You Sure?',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                    $('#loader').show();

                                    var str = "<?php echo url('/'); ?>/mobile_user_collection_verify_ds_3?auth_token=";
                                    str = str + a_th + "&trans_id=" + t_id;
                        
                                    window.location.href = str;
                                }
                            },
                            close: function () {
                                $.alert('Sorry! No Success...');
                            }
                        }
                    });
                   
                }

                if(gid.indexOf("failure_") !== -1)
                {
                    var nid = gid.split("failure_");
                    var t_id = nid[1];
                    var a_th = $('#auth_token').val();
                   
                    $.confirm({
                        title: 'Confirmation',
                        content: 'Are You Sure?',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                    $('#loader').show();

                                    var str = "<?php echo url('/'); ?>/mobile_user_collection_verify_ds_4?auth_token=";
                                    str = str + a_th + "&trans_id=" + t_id;
                        
                                    window.location.href = str;
                                }
                            },
                            close: function () {
                                $.alert('Sorry! No Failure...');
                            }
                        }
                    });
                   
                }
         
            });

           

           
        });
    </script>
</body>
</html>