<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

   

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-dollar-sign fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Surplus Charge</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a_back"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>
    
    
    

    <div style="margin:10px 10px;height:410px;overflow-y:scroll;border-radius:15px;" id = "rech_1" class="card z-depth-2"> 

         <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                   
                <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                        
                <?php 

                    $str = "";
                    $str = $str ."<div style='height:5px'></div>
                            <div class='row pl-3 pr-3'>
                                <div class='col-5 col-sm-5 col-md-5' style = 'padding-right:2px;'>
                                    <p class = 'text-body' style='margin:2px 2px;font-size: 11px;'>Nework</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:2px;'>
                                    <p class = 'text-muted' style='margin:2px 1px;font-size: 10px;'>From</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:1px;'>
                                    <p class = 'text-muted' style='margin:2px 1px;font-size: 10px;'>To</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:2px;'>
                                    <p class = 'text-muted' style='margin:1px 1px;font-size: 10px;'>Amt</p>
                                </div>
                            </div>";

                    foreach($surplus as $d)
                    {
                        $net_name = "";
                        foreach($network as $r)
                        {
                            if($d->net_code == $r->net_code)
                            {
                                $net_name = $r->net_name;
                                break;
                            }
                        }

                        $str = $str ."<div style='height:5px'></div>
                            <div class='row pl-3 pr-3'>
                                <div class='col-5 col-sm-5 col-md-5' style = 'padding-right:4px;'>
                                    <p class = 'text-body' style='margin:2px 2px;font-size: 11px;'>".$net_name."</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:2px;'>
                                    <p class = 'text-muted' style='margin:2px 1px;font-size: 10px;'>".$d->from_amount."</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:1px;'>
                                    <p class = 'text-muted' style='margin:2px 1px;font-size: 10px;'>".$d->to_amount."</p>
                                </div>
                                <div class='col-2 col-sm-2 col-md-2' style = 'padding-right:2px;'>
                                    <p class = 'text-muted' style='margin:2px 1px;font-size: 11px;'>".$d->surplus_charge."</p>
                                </div>
                            </div>";
                       
                    }

                    echo $str;

                ?>
            
            </div>
        </div>
    </div>

   

    @include('android.common_bottom')

    
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');

            
            

            
            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_profile_rt_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("logout:" ,'_self');
                   
                }

         
            });
           

            

           
          
        });
    </script>
</body>
</html>