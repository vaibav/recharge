<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;" class="card z-depth-1">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Add Customer</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
               
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_back">
                                <i class="fas fa-arrow-circle-left fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>

    <div style="margin:10px 10px;border-left: 5px solid #F0B27A;height:410px;overflow-y:scroll;" class="card z-depth-1"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form1" class="form-horizontal" action="{{url('mobile_user_money_rm_store')}}" method="post" accept-charset="UTF-8">

                        <input type="hidden" id = "csrftk" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
                        
                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Msisdn(Mobile No)" id="msisdn" name = "msisdn">
                                </div>
                            </div>
                            

                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text" 
                                    placeholder="First Name" id="user_rem_name" name = "user_rem_name">
                                </div>
                                
                            </div>

                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text" 
                                    placeholder="Last Name" id="user_rem_lname" name = "user_rem_lname">
                                </div>
                                
                            </div>

                            <div style="height:17px"></div>

                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text"
                                    placeholder="Address" id="user_address" name = "user_address">
                                </div>
                            </div>
                            <div style="height:17px"></div>

                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text"
                                    placeholder="City" id="user_city"  name = "user_city">
                                </div>
                            </div>
                            <div style="height:17px"></div>

                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12" >
                                    <select class="form-control curve-border" id="user_state_code" name = "user_state_code" >
                                        <option value="6">Tamil Nadu</option>
                                        <option value="1">New Delhi</option>
                                        <option value="5">Kerala</option>
                                    </select>
                                </div>
                            </div>

                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Pincode" id="user_pincode"  name = "user_pincode">
                                </div>
                            </div>
                            <div style="height:17px"></div>
                            
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-right">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                                </div>
                            </div>
                        

                        
                    </form>
            </div>
        </div>
    </div>

    
    @include('android.common_bottom')
    
    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        $msi = $op1['msisdn'];
        $uri = url('/')."/mobile_user_money_tr_1/".$msi."/";
      
        if($op == 0)
        {
            echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                    var at = $('#auth_token').val();
                                    window.open('".$uri."' + at,'_self');
                                }
                            }
                            
                        }
                    });
                });
                </script>";
        }
        else 
        {
            echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
        }
        session()->forget('result');
    }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                
                    window.open("<?php echo url('/'); ?>/mobile_user_money_1?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var z = checkData();

                if(z[0] == 0)
                {
                   
                    $('#btn_submit').prop('disabled', true);

                    $.confirm({
                        title: 'Add Beneficiary',
                        content: 'Are you sure?',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Add',
                                btnClass: 'btn-green',
                                action: function(){
                                    $('#loader').show();
                                    $('#rech_form1').attr('action', "{{url('mobile_user_money_rm_store')}}");
                                    $('#rech_form1').submit();
                                }
                            },
                            close: function () {
                                $.alert("Sorry..");
                                $('#btn_submit').prop('disabled', false);
                            }
                        }
                    });

                   
                    
                }
                else 
                {
                    $.alert(z[1]);
                }   
            }); 

            

            function checkData()
            {
                var z = [];
                z[0] = 0;
                z[1] = "";
                
                if($('#msisdn').val() == "")
                {
                    z[0] = 1;
                    z[1] = "Mobile is Empty";
                }
                else if($('#user_rem_name').val() == "")
                {
                    z[0] = 1;
                    z[1] = "First Name is Empty";
                }
                else if($('#user_rem_lname').val() == "")
                {
                    z[0] = 1;
                    z[1] = "Last Name is Empty";
                }
                else if($('#user_address').val() == "")
                {
                    z[0] = 1;
                    z[1] = "Address is Empty";
                }
                else if($('#user_city').val() == "")
                {
                    z[0] = 1;
                    z[1] = "City is Empty";
                }
                else if($('#user_pincode').val() == "")
                {
                    z[0] = 1;
                    z[1] = "Pincode is Empty";
                }
                

                return z;
            }


           
          
        });
    </script>
</body>
</html>