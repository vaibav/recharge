<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')
</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2 ">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-rupee-sign fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">My Account</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                
                
            </div>
        </div>
        
    </div>


    

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;"   id = "rech_1"> 
    <?php 

        $j = 0;
        $str = "";
        $debit = 0;
        $credit = 0;
        $balance = 0;


        foreach($pay as $f)
        {
            if($f->user_name == $user_name && $f->trans_status == 1)
            {
                // Debit +
                $debit = floatval($debit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) + floatval($f->grant_user_amount);

                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2 ' >"; 
                $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
                
                $str = $str. "<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #26c6da cyan lighten-1 white-text'><i class='material-icons'>trending_up</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->grant_user_name."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->payment_mode." </p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->grant_date."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>+ ".$f->grant_user_amount."</p>
                            </div>
                        </div></div>";
                
            
            
                
            }
            else if($f->grant_user_name == $user_name && $f->trans_status == 1)
            {
                // Credit -
                $credit = floatval($credit) + floatval($f->grant_user_amount);
                $balance = floatval($balance) - floatval($f->grant_user_amount);

                $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
                $str = $str . "<div class = 'row' style='background: white;'>";

                $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                <a class='floating  btn-small waves-effect waves-light #ec407a pink lighten-1'><i class='material-icons'>trending_down</i></a><br>
                            </div>
                            <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>".$f->user_name."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>".$f->payment_mode." </p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 11px;'>".$f->grant_date."</p>
                                <p class = 'text-muted' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                            </div>
                            <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$f->grant_user_amount."</p>
                            </div>
                        </div></div>";

                

            }
            $j++;

        }

        $stx = "";


        // RECHARGE AMOUNT
        if($res != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

            $credit = floatval($credit) + floatval($res);
            $balance = floatval($balance) - floatval($res);
            
            $res = round($res, 2);
            

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>RECHARGE AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$res."</p>
                                </div>
                            </div></div>";

            $j++;
        }


        // PENDING RECHARGE AMOUNT
        if($rep != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";
            $credit = floatval($credit) + floatval($rep);
            $balance = floatval($balance) - floatval($rep);

            $rep = round($rep, 2);

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>PENDING RECHARGE AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$rep."</p>
                                </div>
                            </div></div>";

            $j++;
        }



        // EBBILL AMOUNT
        if($ebs != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

            $credit = floatval($credit) + floatval($ebs);
            $balance = floatval($balance) - floatval($ebs);

            $ebs = round($ebs, 2);

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>EBBILL AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$ebs."</p>
                                </div>
                            </div></div>";

            $j++;
        }


        // PENDING EBBILL AMOUNT
        if($ebp != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:5px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

            $credit = floatval($credit) + floatval($ebp);
            $balance = floatval($balance) - floatval($ebp);

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                   <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>PENDING EBBILL AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$ebp."</p>
                                </div>
                            </div></div>";

            $j++;
        }


        // MONEY AMOUNT
        if($mns != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

            $credit = floatval($credit) + floatval($mns);
            $balance = floatval($balance) - floatval($mns);

            $mns = round($mns, 2);

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                   <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>MONEY TRANSFER AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$mns."</p>
                                </div>
                            </div></div>";

            $j++;
        }

        // MONEY BENEFICIARY VERIFY AMOUNT
        if($mvs != "0")
        {
            $str = $str."<div style='margin:10px 10px;border-radius:15px;' class='card z-depth-2' >"; 
            $str = $str . "<div class = 'row rounded-pill' style='background: white;'>";

            $credit = floatval($credit) + floatval($mvs);
            $balance = floatval($balance) - floatval($mvs);

            $mvs = round($mvs, 2);

            $str = $str ."<div class='col-2 col-sm-2 col-md-2 text-center' style='margin:4px 0px;padding: 12px 3px;'>
                                    <a class='floating  btn-small waves-effect waves-light #e57373 red lighten-2 white-text'><i class='material-icons'>remove</i></a><br>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6' style='margin:2px 0px;padding: 2px 3px;'>
                                   <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>BENEFICIARY VERIFY AMT</p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'></p>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 12px;'>BAL: ".number_format($balance,2,'.','')."</p>
                                </div>
                                <div class='col-4 col-sm-4 col-md-4 text-right' style='margin:4px 0px;padding: 2px 5px;'>
                                    <p class = 'text-body' style='margin:2px 5px;font-size: 14px;'>- ".$mvs."</p>
                                </div>
                            </div></div>";

            $j++;
        }



        echo $str;

        ?>
    </div>

    @include('android.common_bottom')
    
    

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');
            
            


        });
    </script>
</body>
</html>