<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                    <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['user_name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['msisdn']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo $data['total_limit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consume_limit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaining_limit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h6 amber-text text-darken-3 " style="margin:7px 6px;">Money Transfer</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id = "a1"><i class='fas fa-plus'></i></a>&nbsp;
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a4">
                                <i class="fas fa-chart-line fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>

    <input type="hidden" id = "u_name" name="u_name" value="{{ $user_name }}">
    <input type="hidden" id = "ben_id" name="ben_id" value="">
    <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['msisdn']; ?>">
    <input type="hidden" id = "trans_id" name="trans_id" value="">
    <input type="hidden" id = "otp" name="otp" value="">
    <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($beni as $r)
            {
                $str = $str . " <div style='margin:10px 10px;border-left: 5px solid #F0B27A;' class='card z-depth-1' >
                    <div class='row' style='margin:0px 0px;'>
                        <div class='col-6 col-sm-6 col-md-6 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <p class = 'h6' style='margin:9px 2px;font-size: 14px;color: #52BE80;'>".$r['b_accno']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'>".$r['b_name']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'>".$r['b_bank']."</p>
                            
                        </div>
                        <div class='col-2 col-sm-2 col-md-2 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <div class='row'>
                                <div class='col-12 col-sm-12 col-md-12' style='padding:10px 2px;'>
                                    <div class='form-check'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['b_id']."'>IMPS
                                        </label>
                                    </div>
                                    <div class='form-check'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['b_id']."'>NEFT
                                        </label>
                                    </div>
                                    <div class='form-check' >
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['b_id']."'>RDGS
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-12 col-sm-12 col-md-12' style='padding:2px 9px;padding-top: 10px;padding-bottom: 6px;'>
                                        <input class='form-control curve-border-1' type='number' 
                                        placeholder='0.00' id='amt_".$r['b_id']."' name='amt_".$r['b_id']."'>
                                </div>
                            </div>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                    <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id='calc_".$r['b_id']."'><i class='fas fa-percentage'></i></a>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                    <a class='floating btn-small waves-effect waves-light #00acc1 cyan darken-1 white-text text-center' 
                                    id='send_".$r['b_id']."'><i class='fas fa-paper-plane'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
                
                $j++;
            }

            echo $str;
        ?>


       
    </div>

    @include('android.common_bottom')
    
    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op != 0)  
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var b_id = nid[1];
                    var b_amnt = $('#amt_' + b_id).val();
                    var b_mode = $('option:selected', '#mode_' + b_id).val();

                    //alert(b_id + "---" + b_amnt + "---" + b_mode + "---"+ b_bcode +"----"+b_accno+"---"+b_name);

                    if(b_id == ben_id)
                    {
                        if(b_mode == "-") {
                            swal("Cancelled", "Please Select Transfer Mode", "error");
                        }
                        else if(b_amnt == "") {
                            swal("Cancelled", "Please Enter Amount", "error");
                        }
                        else
                        {
                            $('#ben_id').val(b_id);
                            $('#bene_id').val(b_id);
                            $('#bank_code').val(b_bcode);
                            $('#cus_acc_no').val(b_accno);
                            $('#cus_name').val(b_name);
                            $('#bank_mode').val(b_mode);
                            $('#cus_amt').val(b_amnt);

                            swal({
                                title: 'Are you sure?',
                                type: 'warning',
                                html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Bank</th><td style="padding:4px 8px;">' + b_bank + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Account No</th><td style="padding:4px 8px;">' + $('#cus_acc_no').val() + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Name</th><td style="padding:4px 8px;">' + $('#cus_name').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Mode</th><td style="padding:4px 8px;">' + $('#bank_mode').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + $('#cus_amt').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + ben_per + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + ben_pamt + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + ben_surp + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + ben_net + '</td></tr></tabel>',
        
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, Transfer!'
                                }).then((result) => {
                                if (result.value) {
                                    $('#loader').show();
                                    
                                    $('#rech_form').attr('action', "{{url('recharge_money')}}");
                                    $('#rech_form').submit();
                                }
                                else
                                {
                                    swal("Cancelled", "No Recharge...", "error");
                                    
                                }
                            });
                        }
                    }
                    else
                    {
                        swal("Cancelled", "Please Click calculate Button...", "error");
                    }
                   
                   
                   
                    
                }

                
                
                if(gid.indexOf("btn_otp") !== -1)
                {
                    $('#btn_submit_1').prop('disabled', true);

                    
                    var otp = $('#cus_otp').val();
                    

                    if(otp == "") {
                        swal("Cancelled", "Please Enter OTP", "error");
                        $('#btn_submit_1').prop('disabled', false);
                    }
                    else if(otp != "") 
                    {
                        $('#otp').val(otp);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_money_otp')}}");
                        $('#rech_form').submit();
                    }
                   
                }

                if(gid.indexOf("btn_add") !== -1)
                {
                    window.open("{{url('ben_entry')}}", "_self");
                   
                }

                if(gid.indexOf("calc_") !== -1)
                {
                    var nid = gid.split("calc_");
                    var b_id = nid[1];
                    var b_amnt = $('#amt_' + b_id).val();
                    var u_name = $('#u_name').val();
                  
                    alert(b_id + "--" + b_amnt + "--" + u_name);
                    if(b_amnt != "")
                    {
                        $('#loader').show(); 
                        $.ajax({
                            type: 'GET', //THIS NEEDS TO BE GET
                            url: "<?php echo url('/'); ?>/money_percentage_mobile/" + u_name + "/" + b_amnt + "/WEB",
                            success: function (data) {
                                ben_id = b_id;
                                ben_amt = b_amnt;
                                ben_per = data['per'];
                                ben_pamt = data['per_amt'];
                                ben_surp = data['sur'];
                                ben_net = data['netamt'];
                                $('#loader').hide(); 

                                $.confirm({
                                    title: 'Calculation Percentage',
                                    content: '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + data['amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + data['per'] + '</td></tr>'  
                                                            + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + data['per_amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + data['sur'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + data['netamt'] + '</td></tr></tabel>',
                                    type: 'green',
                                    typeAnimated: true,
                                    buttons: {
                                        tryAgain: {
                                            text: 'OK',
                                            btnClass: 'btn-green',
                                            action: function(){
                                            }
                                        },
                                        close: function () {
                                        }
                                    }
                                });

                                
                            },
                            error: function() { 
                                
                                console.log(data);
                                $('#loader').hide(); 
                            }
                        });
                        
                    }
                    else
                    {
                        swal("Cancelled", "Please Enter Transfer Amount...", "error");
                    }
                   
                   
                    
                }

                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
            });
            
          
        });
    </script>
</body>
</html>