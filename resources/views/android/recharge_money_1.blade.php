<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;" class="card z-depth-1">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-7 col-sm-7 col-md-7 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Money Transfer</p>
            </div>
            <div class="col-3 col-sm-3 col-md-3 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                
            </div>
        </div>
        
    </div>

    

    <div style="margin:10px 10px;height:410px;overflow-y:scroll;border-left: 5px solid #F0B27A;" class="card z-depth-1"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form1" class="form-horizontal" action="{{url('mobile_user_money_2')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                            <div style="height:17px"></div>
                           
                         
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Mobile No" id="msisdn" name="msisdn">
                                </div>
                            </div>
                            <div style="height:17px"></div>
                            
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-right">
                                        <button class="btn btn-sm waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                                </div>
                            </div>

                            <div style="height:30px"></div>
                            
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-center">
                                        <button class="btn btn-small waves-effect waves-light #f4511e green darken-1 white-text" 
                                            id="btn_remiter" style = "border-radius:20px;">Add Customer</button>
                                </div>
                            </div>
                        

                        
                    </form>
            </div>
        </div>
    </div>

    @include('android.common_bottom')
    
    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op != 0)  
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var um = $('#msisdn').val();
                
                //alert(ax);
                if(um != "" )
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure?',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form1').attr('action', "{{url('mobile_user_money_2')}}");
                                    $('#rech_form1').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit').prop('disabled', false);
                }
               
            }); 

            $('#btn_remiter').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_remiter').prop('disabled', true);

                var at = $('#auth_token').val();
                
                //alert(ax);
                if(at != "")
                {
                    window.open("<?php echo url('/'); ?>/mobile_user_money_rm_1/" + at,'_self');
                }
               
               
            }); 


        });
    </script>
</body>
</html>