<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
    
    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fab fa-medapps fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Bill Payment</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a4"><i class='fas fa-filter fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>
    
    

    

    <div style="margin:10px 10px;border-radius:15px;height:310px;overflow-y:scroll;" class="card z-depth-2"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form1" class="form-horizontal" action="{{url('recharge_mobile_ebbill_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="auth_token" name="auth_token" value="{{ $auth_token }}">
                        <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12" >
                                    <select class="form-control curve-border" name="net_code" id = "id_net_code" >
                                        <?php
                                            foreach($network as $f)
                                            {
                                                echo "<option value = '".$f->net_code."'>".$f->net_name."</option>";
                                                
                                            }   
                                        ?>                                  
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-9 col-sm-9 col-md-9">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Consumer No" id="id_eb_cons_no" name="eb_cons_no">
                                </div>
                                <div class="col-3 col-sm-3 col-md-3 text-center" >
                                    <a class="floating btn-small waves-effect waves-light#ff8f00 amber darken-3 white-text" id="eb_info">info</a>
                                </div>
                            </div>
                            

                            <div style="height:14px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text" 
                                    placeholder="Name" id="id_eb_cons_name" name="eb_cons_name">
                                </div>
                                
                            </div>
                            <div style="height:17px"></div>

                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Amount" id="id_eb_cons_amount" name="eb_cons_amount">
                                </div>
                            </div>
                            <div style="height:17px"></div>

                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Mobile No" id="id_eb_cons_mobile" name="eb_cons_mobile">
                                </div>
                            </div>
                            <div style="height:17px"></div>
                            
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-right">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                                </div>
                            </div>
                        

                        
                    </form>
            </div>
        </div>
    </div>

   
    <!-- Report-->
    <!-- Title-->
    <div style="margin:10px 10px;display:none;" class="card z-depth-1 rounded-pill" id = "rech_44">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fab fa-medapps fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Report</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                        id = "a1"><i class='fas fa-arrow-left fa-2x white-text'></i></a>
                
            </div>
        </div>
        
    </div>
    

    <div style="margin:10px 0px;height:350px;display: none;overflow-y:scroll" id = "rech_4">
            
            <?php echo $recharge; ?>
    </div>






    

    <!-- Modal -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
        <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
        <div class="modal-dialog modal-dialog-centered modal-sm mw-100" role="document" style = "width:90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style = "max-height: calc(100vh - 143px);overflow-y: auto;">
                    <table class="table table-striped table-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    @include('android.common_bottom')
    
    

      <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Congratulations!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                    
                });
                </script>";
            }
            else  
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();
            
            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 250);
                $('#rech_4').height($(window).height() - 250);
            });

            $(window).trigger('resize');

            $('#a1').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').show("slow"); $('#rech_4').hide("slow");
                $('#rech_11').show("slow"); $('#rech_44').hide("slow");
            });

            $('#a4').on('click',function(e) {
                e.preventDefault();
                $('#rech_1').hide("slow"); $('#rech_4').show("slow");
                $('#rech_11').hide("slow"); $('#rech_44').show("slow");
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var un = $('option:selected', "#id_net_code").val();
                var uc = $('#id_eb_cons_no').val();
                var ua = $('#id_eb_cons_amount').val();
                var um = $('#id_eb_cons_mobile').val();
                
                //alert(ax);
                if(un != "" && uc != "" && ua != "" && um != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> Con No : ' +uc+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form1').attr('action', "{{url('recharge_mobile_ebbill_store')}}");
                                    $('#rech_form1').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(uc == "")
                {
                    $.alert('Please Enter Connection No!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(un == "")
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit').prop('disabled', false);
                }
                
                
            }); 


            $('#eb_info').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code1 =  $('option:selected', '#id_net_code').val();
                var usr_mobe = $('#id_eb_cons_no').val();
                var ath = $('#auth_token').val();

               

                if(net_code1 == null || net_code1 =="")
                {
                    $.confirm({
                        title: 'EB Info',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'EB Info!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#loader').show();
                    $.ajax({
                        url: '<?php echo url('/'); ?>/rech_eb',
                        type: 'GET',
                        data: {net_code:net_code1, con_no:usr_mobe},
                        success: function (data) { 
                            var z = data.split("*");
                            $('#rech_offer_body').html(z[3]);
                            $('#id_eb_cons_name').val(z[1]);
                            $('#id_eb_cons_amount').val(z[0]);
                            $('#modal1').modal('show');
                            $('#loader').hide();
                            
                        },
                        error: function(data, mXg) { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    }); 
                }
        
                
            });

           

           
    
        });
    </script>
</body>
</html>