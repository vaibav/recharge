<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }

        .btn-circle {
          display:block;
          height: 30px;
          width: 30px;
          border-radius: 50%;
          background: #5DADE2;
          border: 1px solid #5DADE2;
          text-align: center;
          color: white !important;
          
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                    <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['mobile']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo floatval($data['consumedlimit']) + floatval($data['remaininglimit']); ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consumedlimit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaininglimit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h5 amber-text text-darken-3 " style="margin:7px 10px;">Add Beneficiary</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
               
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_back">
                                <i class="fas fa-arrow-circle-left fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        
    </div>

    <div style="margin:10px 10px;border-left: 5px solid #F0B27A;height:410px;overflow-y:scroll;" class="card z-depth-1"  id = "rech_1"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form1" class="form-horizontal" action="{{url('mobile_user_money_ab_store')}}" method="post" accept-charset="UTF-8">

                        <input type="hidden" id = "csrftk" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
                        <input type="hidden" id = "ben_id" name="ben_id" value="">
                        <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['mobile']; ?>">
                        <input type="hidden" id = "trans_id" name="trans_id" value="">
                        <input type="hidden" id = "otp" name="otp" value="">
                        <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">
                        <input type="hidden" id = "firstName" name="firstName" value="<?php echo $data['name']; ?>">
                        <input type="hidden" id = "remId"     name="remId" value="<?php echo $data['id']; ?>">

                        <div class="row" style="margin:2px 2px;">
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">A</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">B</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">C</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">D</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">E</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">F</button>
                            </div>
                            <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">G</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">H</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">I</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">J</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">K</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">L</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">M</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">N</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">O</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">P</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">Q</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">R</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">S</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">T</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">U</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">V</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">W</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">X</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">Y</button>
                            </div>
                             <div class="col-1 col-sm-1 col-md-1 align-center" style="margin:2px 10px;padding:1px 1px;">
                                 <button class="btn-circle">Z</button>
                            </div>

                                
                            
                          
                        </div>

                        <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12" >
                                    <select class="form-control curve-border" id="bank_code" name = "bank_code" >
                                    <option value="">Select Bank</option>
                                    <?php 
                                        foreach($bank as $d){
                                            echo "<option value =".$d['bankCode'].">".$d['bankName']."</option>";
                                        }
                                    ?>

                                         
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                    <input class="form-control curve-border" type="number"
                                    placeholder="Account No" id="ben_acc_no" name = "ben_acc_no">
                                </div>
                                
                            </div>

                            <div style="height:17px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-9 col-sm-9 col-md-9">
                                    <input class="form-control curve-border" type="text"
                                    placeholder="IFSC Code (Must)" id="bank_ifsc"  name = "bank_ifsc">
                                </div>
                                <div class="col-3 col-sm-3 col-md-3 text-center" style = "padding-left:3px;">
                                    <a class="btn btn-sm waves-effect waves-light#ff8f00 amber darken-3 white-text" 
                                    style = "padding:8px 10px;" id="btn_verify">verify</a>
                                </div>
                            </div>
                            

                            <div style="height:14px"></div>
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12">
                                    <input class="form-control curve-border" type="text" 
                                    placeholder="Beneficiary Name" id="ben_acc_name"  name = "ben_acc_name">
                                </div>
                                
                            </div>
                            <div style="height:17px"></div>

                            
                            <div class="row pl-3 pr-3">
                                <div class="col-12 col-sm-12 col-md-12 text-right">
                                        <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                                </div>
                            </div>
                        

                        
                    </form>
            </div>
        </div>
    </div>

    <div style="margin:10px 10px;border-left: 5px solid #F0B27A;height:410px;overflow-y:scroll;display:none;" class="card z-depth-1"  id = "rech_2"> 
        <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col-12 col-sm-12 col-md-12" style="margin:2px 2px;padding: 0px 0px;">
                <div style="height:14px"></div>
                <div class="row pl-3 pr-3">
                    <div class="col-12 col-sm-12 col-md-12">
                        <p id = "bn_id">&nbsp;</p>
                        <p id = "tr_id">&nbsp;</p>
                    </div>
                </div>

                <div style="height:14px"></div>
                <div class="row pl-3 pr-3">
                    <div class="col-12 col-sm-12 col-md-12">
                        <input class="form-control curve-border" type="text" 
                        placeholder="OTP" id="cus_otp" name="cus_otp">
                    </div>
                </div>

                <div style="height:17px"></div>
                    
                <div class="row pl-3 pr-3">
                    <div class="col-12 col-sm-12 col-md-12 text-right">
                            <button class="btn btn-small waves-effect waves-light 
                                #f4511e deep-orange darken-1 white-text" id="btn_otp">submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    @include('android.common_bottom')
    
    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
       /* $trans_id = $op1['trans_id'];
        $ben_acc_no = $op1['ben_acc_no'];
        $ben_id = $op1['ben_id'];
        $msisdn = $op1['msisdn'];
        //$ben_acc_no = $op1['ben_acc_no'];*/

        //file_put_contents(base_path().'/public/sample/ben_delete.txt', $op."---".$res."---".$trans_id."---".$otp_status."----".$remarks, FILE_APPEND);

        if($op == 0)
        {
            /*echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#ben_id').val('".$ben_id."');
                    $('#c_msisdn').val('".$msisdn."');
                    $('#tr_id').text('".$trans_id."');
                    $('#bn_id').text('".$ben_id."');

                    $('#rech_1').hide(); 
                    $('#rech_2').show();
                });
                </script>";*/

                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: 'Beneficiary is added Successfully..',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                });
                </script>";
        }
        else 
        {
            echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                });
                </script>";
        }
        session()->forget('result');
    }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
                $('#rech_2').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_ab_1/" + ms + "/" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
                if(gid.indexOf("btn_verify") !== -1)
                {
                    e.preventDefault();

                    var ath = $("#auth_token").val();
                    var acc_no = $("#ben_acc_no").val();
                    var b_ifsc = $("#bank_ifsc").val();
                    var b_code = $('option:selected', "#bank_code").val();
                    var msisdn = $("#c_msisdn").val();
                    var csrftk = $("#csrftk").val();


                    if(acc_no == "")
                    {
                        $.alert("Error! Account No is Empty....");
                    }
                    else if(b_code == "-")
                    {
                        $.alert("Error! Please Select Bank....");
                    }
                    else if(msisdn == "-")
                    {
                        $.alert("Error! Please Select Customer....");
                    }
                    else
                    {
                        //alert(acc_no + "--" + b_code + "---" + age_id + "---"+ msisdn);
                        $('#btn_verify').prop('disabled', true);

                        $.confirm({
                            title: 'Account Verification',
                            content: 'Are you sure? (Rs.5 deducted from your Account)',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'OK',
                                    btnClass: 'btn-green',
                                    action: function(){
                                        $('#loader').show();
                                        $.ajax({
                                            url: '<?php echo url('/'); ?>/mobile_user_money_ab_verify',
                                            type: 'POST',
                                            data: {_token: csrftk, ben_acc_no:acc_no, bank_code:b_code, agent_msisdn:msisdn, bank_ifsc:b_ifsc, auth_token:ath },
                                            dataType: 'JSON',
                                            success: function (data) { 
                                                console.log(data);
                                                if(data.status == 1)
                                                {
                                                    //$.alert(data.message);
                                                    $("#ben_acc_name").val(data.ben_name);
                                                }
                                                else if(data.status == 2)
                                                {
                                                    $.alert(data.message);
                                                }
                                                $('#loader').hide();
                                                $('#btn_verify').prop('disabled', false);
                                            },
                                            error: function(data, mXg) { 
                                                $('#loader').hide();
                                                console.log(data);
                                                $('#btn_verify').prop('disabled', false);
                                            }
                                        }); 
                                    }
                                },
                                close: function () {
                                    $.alert("Sorry..");
                                }
                            }
                        });

                        
                    }
                   
                }

            
            });

            $("#bank_code").change(function(e)
            {
                var ty = $('option:selected', this).val();
                var ax = $('option:selected', this).text();
                if(ty != "-")
                {
                    $.ajax({
                        url:"{{url('/get-bank-ifsc/')}}" + "/" + ty,
                        type: 'GET',
                        success: function( response ){
                           if(response != "-"){
                                $("#bank_ifsc").val(response);
                           }
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        }
                    });
                }
                    
            });

            $('.btn-circle ').on("click",function(e){
                e.preventDefault();

                var btnText =  $(this).text();
                if(btnText.length == 1)
                {
                 $.ajax({
                        url:"{{url('/get-bank-name/')}}" + "/" + btnText,
                        type: 'GET',
                        success: function( response ){
                           if(response != "-"){
                                //alert(response);
                                $('#bank_code').empty();
                                $('#bank_code').append(response);
                           }
                        },
                        error: function (xhr, b, c) {
                            console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        }
                    });
                }
            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var bank = $('option:selected', '#bank_code').val();
                var ifsc = $('#bank_ifsc').val();
                var z = checkBankIfsc(bank, ifsc);
                z = 1;

                if(z == 1)
                {
                    var b_acc = $('#ben_acc_no').val();
                    var b_nam = $('#ben_acc_name').val();

                    if(b_acc != "" && ifsc != "")
                    {
                        $('#btn_submit').prop('disabled', true);

                        $.confirm({
                            title: 'Add Beneficiary',
                            content: 'Are you sure?',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'Add',
                                    btnClass: 'btn-green',
                                    action: function(){
                                        $('#loader').show();
                                        $('#rech_form1').attr('action', "{{url('mobile_user_money_ab_store')}}");
                                        $('#rech_form1').submit();
                                    }
                                },
                                close: function () {
                                    $.alert("Sorry..");
                                    $('#btn_submit').prop('disabled', false);
                                }
                            }
                        });
                    }
                    else if(b_acc == "")
                    {
                        $.alert("Please Enter Account No..");
                    }
                    else if(b_nam == "")
                    {
                        $.alert("Please Enter Account Holder Name..");
                    }
                    
                }
                else if(z == 2)
                {
                    $.alert("You Must Enter Any one... Either Bank or IFSC code");
                }
                else
                {
                    $.alert("Dont Enter Both Bank and IFSC code! \n Select Bank Or Enter Ifsc Code");
                }
                   
            }); 

            $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                var otp = $('#cus_otp').val();
                var bid = $('#ben_id').val();
                var tid = $('#trans_id').val();
                var msi = $('#c_msisdn').val();
                var ath = $('#auth_token').val()
                var csrftk = $("#csrftk").val();
                var cotp = $("#cus_otp").val();
                $('#btn_otp').prop('disabled', true);
                
                //alert(otp + "---" + bid + "----" + tid + "----" + msi + "---" + ath);
                if(cotp == "") {
                    $.alert("Enter OTP");
                    $('#btn_otp').prop('disabled', false);
                }
                else if(cotp != "" && bid != "" && tid != "" && msi != "" && ath != "") 
                {
                    $('#loader').show();
                    $.ajax({
                        url: '<?php echo url('/'); ?>/mobile_user_money_ab_store_otp',
                        type: 'GET',
                        data: {ben_id:bid, msisdn:msi, trans_id:tid, otp:cotp, auth_token:ath},
                        dataType: 'JSON',
                        success: function (data) { 
                            if(data.status == 0)
                            {
                                $.alert(data.message);
                                window.open("<?php echo url('/'); ?>/mobile_user_money_tr_1/" + msi + "/" + ath,'_self');
                            }
                            else 
                            {
                                $.alert(data.message);
                            }
                            $('#loader').hide();
                            $('#btn_otp').prop('disabled', false);
                        },
                        error: function(data, mXg) { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    }); 
                
                }
                   
            }); 

            function checkBankIfsc(bank, ifsc)
            {
                var z = 0;

                if(bank == "" && ifsc != "")
                {
                    z = 1;
                }
                else if(bank != "" && ifsc == "")
                {
                    z = 1;
                }
                else if(bank == "" && ifsc == "")
                {
                    z = 2;
                }

                return z;
            }


           
          
        });
    </script>
</body>
</html>