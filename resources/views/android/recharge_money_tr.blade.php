<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 110px;" class="card z-depth-1">
        <div class="card-body" style="padding: 10px 10px;">
            <div class="row" >
                <div class="col-6 col-sm-6 col-md-6 text-left" style="padding: 3px 10px;">
                    <h6 class="card-title text-muted" style="margin-bottom: 4px;"><a>Hai {{ $user_name }}!</a></h6>
                    <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
                </div>
                <div class="col-6 col-sm-6 col-md-7 text-right "style="padding: 3px 10px;">
                    <p class = 'h6' style='margin:2px 2px;font-size: 14px;color: #52BE80;'><?php echo $data['name']; ?></p>
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'><?php echo $data['mobile']; ?></p>
                </div>
                
            </div>
            <div class="row" >
                <div class="col-4 col-sm-4 col-md-4 text-left" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-donate"></i> <?php echo floatval($data['consumedlimit']) + floatval($data['remaininglimit']); ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-center" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="far fa-handshake"></i> <?php echo $data['consumedlimit']; ?></p>
                </div>
                <div class="col-4 col-sm-4 col-md-4 text-right" style="padding: 3px 10px;">
                    <p class = 'text-muted' style='margin:2px 2px;font-size: 14px;'>
                        <i class="fas fa-handshake"></i> <?php echo $data['remaininglimit']; ?></p>
                </div>
                    
            </div>
            
            
        </div>
    </div>

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:50px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:50px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-rupee-sign fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-left " style="height:50px;margin:0px 0px;padding: 0px 4px;">
                <p class="h6 amber-text text-darken-3 " style="margin:7px 5px;">Money Transfer</p>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-right " style="height:50px;margin:0px 0px;padding: 3px 3px;">
                <a class='floating btn-small waves-effect waves-light #42a5f5 blue lighten-1 white-text text-center'
                                    id = "a_rep" style = "width:38px;height:38px;"><i class="fas fa-filter"></i></a>
                <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id = "a_add" style = "width:38px;height:38px;"><i class='fas fa-plus'></i></a>
                <a class='floating btn-small waves-effect waves-light #e57373 red lighten-2 white-text text-center'
                                    id = "a_del" style = "width:38px;height:38px;"><i class="far fa-trash-alt"></i></a>
                <a class='floating btn-small waves-effect waves-light #ffb74d orange lighten-2 white-text text-center'
                                    id = "a_back" style = "width:38px;height:38px;"><i class='fas fa-arrow-circle-left white-text'></i></a>
               
                </a>
            </div>
        </div>
        
    </div>
    <form id = "rech_form" class="user" action="#" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" id = "u_name" name="u_name" value="{{ $user_name }}">
        <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">
        <input type="hidden" id = "ben_id" name="ben_id" value="">
        <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['mobile']; ?>">
        <input type="hidden" id = "rem_id" name="rem_id" value="<?php echo $data['id']; ?>">

        <input type="hidden" id = "bene_id" name = "bane_id" value="">
        <input type="hidden" id = "cus_acc_no" name = "cus_acc_no" value="">
        <input type="hidden" id = "cus_name" name = "cus_name" value="">
        <input type="hidden" id = "bank_code" name = "bank_code" value="">
        <input type="hidden" id = "bank_mode" name = "bank_mode" value="">
        <input type="hidden" id = "cus_amt" name = "cus_amt" value="">

        <input type="hidden" id = "trans_id" name="trans_id" value="">
        <input type="hidden" id = "otp" name="otp" value="">
        <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            $j = 1;
            $str = "";
            foreach($beni as $r)
            {
                $str = $str . " <div style='margin:10px 10px;border-left: 5px solid #F0B27A;' class='card z-depth-1' >
                    <div class='row' style='margin:0px 0px;'>
                        <div class='col-6 col-sm-6 col-md-6 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <p class = 'h6' style='margin:9px 2px;font-size: 14px;color: #52BE80;'  id='accno_".$r['id']."'>".$r['account']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;'  id='name_".$r['id']."'>".$r['name']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;' id='bnk_".$r['id']."'>".$r['bank']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;' id='bnkifsc_".$r['id']."'>".$r['ifsc']."</p>
                            <p class = 'text-muted' style='margin:9px 2px;font-size: 14px;display:none;' id='bcode_".$r['id']."'>".$r['ifsc']."</p>
                            
                        </div>
                        <div class='col-2 col-sm-2 col-md-2 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <div class='row'>
                                <div class='col-12 col-sm-12 col-md-12' style='padding:10px 2px;'>
                                    <div class='form-check'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['id']."' value ='IMPS'>IMPS
                                        </label>
                                    </div>
                                    <div class='form-check'>
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['id']."' value ='NEFT'>NEFT
                                        </label>
                                    </div>
                                    <div class='form-check' >
                                        <label class='form-check-label text-muted' style='font-size: 14px;'>
                                            <input type='radio' class='form-check-input' name='mode_".$r['id']."' value ='RDGS'>RDGS
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-4 col-sm-4 col-md-4 text-left' style='margin:0px 0px;padding: 0px 4px;'>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-12 col-sm-12 col-md-12' style='padding:2px 9px;padding-top: 10px;padding-bottom: 6px;'>
                                        <input class='form-control curve-border-1' type='number' 
                                        placeholder='0.00' id='amt_".$r['id']."' name='amt_".$r['id']."'>
                                </div>
                            </div>
                            <div class='row' style='margin:0px 5px;'>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;padding-left:10px;'>
                                    <a class='floating btn-small waves-effect waves-light #81c784 green lighten-2 white-text text-center'
                                    id='calc_".$r['id']."'><i class='fas fa-percentage'></i></a>
                                </div>
                                <div class='col-6 col-sm-6 col-md-6 ' style='padding:2px 4px;'>
                                    <a class='floating btn-small waves-effect waves-light #00acc1 cyan darken-1 white-text text-center' 
                                    id='send_".$r['id']."'><i class='fas fa-paper-plane'></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
                
                $j++;
            }

            echo $str;
        ?>


       
    </div>

    </form>

    @include('android.common_bottom')
    
    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
      
        //$ben_acc_no = $op1['ben_acc_no'];

        //file_put_contents(base_path().'/public/sample/ben_delete.txt', $op."---".$res."---".$trans_id."---".$otp_status."----".$remarks, FILE_APPEND);

        if($op == 0)
        {
           /* if($otp_status == "1")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#rep_trans_id').val('".$rep_trans_id."');

                    $('#tr_id').text('".$trans_id."');
                    $('#rep_tr_id').text('".$rep_trans_id."');

                   
                });
                </script>";
            }
            else if($otp_status == "0")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                });
                </script>";
            }*/

            echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Information!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                });
                </script>";
            
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                $.alert('".$res."');
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var b_id = nid[1];
                    var b_bank = $('#bnk_' + b_id).text();
                    var b_accno = $('#accno_' + b_id).text();
                    var b_name = $('#name_' + b_id).text();
                    var b_bcode = $('#bcode_' + b_id).text();
                    var b_amnt = $('#amt_' + b_id).val();
                    var b_mode = $("input[name='mode_" + b_id + "']:checked").val();

                    //alert(b_id + "---" + b_amnt + "---" + b_mode);

                    //alert(b_id + "---" + b_amnt + "---" + b_mode + "---"+ b_bcode +"----"+b_accno+"---"+b_name);

                    if(b_id == ben_id)
                    {
                        if(b_mode == "") {
                            $.alert('Please Select Transfer Mode');
                        }
                        else if(b_mode == null) {
                            $.alert('Please Select Transfer Mode');
                        }
                        else if(check_mode(b_mode) == 0) {
                            $.alert('Please Select Transfer Mode');
                        }
                        else if(b_amnt == "") {
                            $.alert('Please Select Transfer Mode');
                        }
                        else
                        {
                            $('#ben_id').val(b_id);
                            $('#bene_id').val(b_id);
                            $('#cus_acc_no').val(b_accno);
                            $('#cus_name').val(b_name);
                            $('#bank_code').val(b_bcode);
                            $('#bank_mode').val(b_mode);
                            $('#cus_amt').val(b_amnt);

                             $.confirm({
                                    title: 'Are You Sure?',
                                    content: '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Bank</th><td style="padding:4px 8px;">' + b_bank + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Account No</th><td style="padding:4px 8px;">' + $('#cus_acc_no').val() + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Name</th><td style="padding:4px 8px;">' + $('#cus_name').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">IFSC</th><td style="padding:4px 8px;">' + $('#bank_code').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + $('#cus_amt').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + ben_per + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + ben_pamt + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + ben_surp + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + ben_net + '</td></tr></tabel>',
                                    type: 'green',
                                    typeAnimated: true,
                                    buttons: {
                                        tryAgain: {
                                            text: 'OK',
                                            btnClass: 'btn-green',
                                            action: function(){
                                                 $('#loader').show();
                                    
                                                $('#rech_form').attr('action', "{{url('mobile_user_money_tr_2')}}");
                                                $('#rech_form').submit();
                                            }
                                        },
                                        close: function () {
                                            $.alert('Sorry! No Transfer...');
                                        }
                                    }
                                });

                        }
                    }
                    else
                    {
                        $.alert('Please Click calculate Button...');
                    }
                   
                   
                   
                    
                }

                
                
                if(gid.indexOf("btn_otp") !== -1)
                {
                    $('#btn_submit_1').prop('disabled', true);

                    
                    var otp = $('#cus_otp').val();
                    

                    if(otp == "") {
                        swal("Cancelled", "Please Enter OTP", "error");
                        $('#btn_submit_1').prop('disabled', false);
                    }
                    else if(otp != "") 
                    {
                        $('#otp').val(otp);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_money_otp')}}");
                        $('#rech_form').submit();
                    }
                   
                }

                if(gid.indexOf("a_back") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();
                    //alert(ms);

                    window.open("<?php echo url('/'); ?>/mobile_user_money_out?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }

                if(gid.indexOf("a_add") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_ab_1/" + ms + "/" + at,'_self');
                   
                }

                if(gid.indexOf("a_del") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_money_db_1/" + ms + "/" + at,'_self');
                   
                }

                 if(gid.indexOf("a_rep") !== -1)
                {
                    var at = $('#auth_token').val();
                    var ms = $('#c_msisdn').val();

                   window.open("<?php echo url('/'); ?>/mobile_user_money_rp_1/" + ms + "/" + at + "///",'_self');
                   
                }

                if(gid.indexOf("calc_") !== -1)
                {
                    var nid = gid.split("calc_");
                    var b_id = nid[1];
                    var b_amnt = $('#amt_' + b_id).val();
                    var u_name = $('#u_name').val();
                  
                    //alert(b_id + "--" + b_amnt + "--" + u_name);
                    if(b_amnt != "")
                    {
                        $('#loader').show(); 
                        $.ajax({
                            type: 'GET', //THIS NEEDS TO BE GET
                            url: "<?php echo url('/'); ?>/money_percentage_mobile/" + u_name + "/" + b_amnt + "/WEB",
                            success: function (data) {
                                ben_id = b_id;
                                ben_amt = b_amnt;
                                ben_per = data['per'];
                                ben_pamt = data['per_amt'];
                                ben_surp = data['sur'];
                                ben_net = data['netamt'];
                                $('#loader').hide(); 

                                $.confirm({
                                    title: 'Calculation Percentage',
                                    content: '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + data['amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + data['per'] + '</td></tr>'  
                                                            + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + data['per_amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + data['sur'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + data['netamt'] + '</td></tr></tabel>',
                                    type: 'green',
                                    typeAnimated: true,
                                    buttons: {
                                        tryAgain: {
                                            text: 'OK',
                                            btnClass: 'btn-green',
                                            action: function(){
                                            }
                                        },
                                        close: function () {
                                        }
                                    }
                                });

                                
                            },
                            error: function() { 
                                
                                console.log(data);
                                $('#loader').hide(); 
                            }
                        });
                        
                    }
                    else
                    {
                        $.alert('Please Enter Transfer Amount!');
                    }
                   
                   
                    
                }

                
            });

            function check_mode(mode)
            {
                var z = 0;
                if(mode == "IMPS"){
                    z = 1;
                }
                else if(mode == "NEFT"){
                    z = 1;
                }
                else if(mode == "RDGS"){
                    z = 1;
                }

                return z;
            }
            
          
        });
    </script>
</body>
</html>