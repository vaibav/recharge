<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='fas fa-mobile-alt fa-2x white-text'></i></a>
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Remote Payment</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="margin:0px 0px;padding: 0px 6px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #ba68c8 purple lighten-2 white-text text-center' 
                    id = "a_filter"><i class='fas fa-filter fa-2x white-text'></i></a>
            </div>
        </div>
        
    </div>
    
    
    

    <div style="margin:10px 10px;height:410px;border-radius:15px;overflow-y:scroll;" id = "rech_1" class="card z-depth-2"> 

         <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form" class="form-horizontal" action="{{url('mobile_remote_payment_ds_store')}}" method="POST" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                        
                        <div style="height:17px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                <select id="id_user_name"  name = "user_name_rs" class="form-control curve-border">
                                    <option value = '-'>----Select----</option>
                                    <?php
                                        $str = "";
                                        foreach($users as $f)
                                        {
                                            $str = $str."<option value='".$f->user_name."'>".$f->user_name."--".$f->personal->user_per_name."</option>";   
                                        }
                                        echo $str;
                                    ?>
                                </select>
                            </div>
                        </div>
                       
                        <div style="height:17px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                <input class="form-control curve-border" type="number"
                                placeholder="Amount" id="user_amount" name = "user_amount">
                            </div>
                            
                        </div>
                            

                        <div style="height:17px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <input class="form-control curve-border" type="text" 
                                placeholder="Remarks" id="user_remarks" name = "user_remarks">
                            </div>
                            
                        </div>

                        <div style="height:12px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12">
                                <table class="bordered striped responsive-table ">
                                    <tr>
                                        <td style='font-size:12px;padding:7px 8px;'>USER NAME</td>
                                        <th style='font-size:12px;padding:7px 8px;' id="u_n"></th>
                                    </tr>
                                    <tr>
                                        <td style='font-size:12px;padding:7px 8px;'>BALANCE</td>
                                        <th style='font-size:12px;padding:7px 8px;' id="u_b"></th>
                                    </tr>
                                </table>
                            </div> 
                        </div>

                        <div style="height:10px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12 text-right">
                                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                            </div>
                        </div>
                        

                        
                    </form>
            </div>
        </div>
    </div>

   

    @include('android.common_bottom')

     <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    s$.alert('".$res."');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
 
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 240);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_filter") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_remote_payment_ds_rp?auth_token=" + at,'_self');
                   
                }
                if(gid.indexOf("a_log") !== -1)
                {
                   
                    window.open("logout:" ,'_self');
                   
                }
            });

            $("#id_user_name").change(function(e)
            {
                var un = $('option:selected', this).val();
            
                if(un != "-")
                {
                   getUserBalance(un);
                }
                    
            });

            function getUserBalance(u_n)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $('#loader').show(); 

                $.ajax({
                    url:'get_ub/' + u_n,
                    type: 'GET',
                    success: function( response ){
                        $('#u_b').text(response);
                        $('#u_n').text(u_n);
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                        $('#loader').hide(); 
                    }
                });
            }


            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                
                var un = $('option:selected', '#id_user_name').val();
                var uamt = $('#user_amount').val();
                var urmk = $('#user_remarks').val();
                
                //alert(ax);
                if(uamt != "" && urmk != "" && un != "-")
                {

                    $.confirm({
                        title: 'Remote Payment',
                        content: 'Are You Sure?',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('mobile_remote_payment_ds_store')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(un == "-")
                {
                    $.alert('Please Select User Name!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(uamt == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(urmk == "")
                {
                    $.alert('Please Enter Remarks!');
                    $('#btn_submit').prop('disabled', false);
                }
                
            }); 
           
        });
    </script>
</body>
</html>