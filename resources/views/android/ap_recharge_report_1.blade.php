<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

        <style type="text/css">
        
        .curve-border-1 {
            border:1px solid #F0B27A;border-radius: 25px;
            padding: 4px 8px;line-height: 1.3;height: calc(1.2em + .55rem + 2px);font-size: 0.9rem;
        }
       
    </style>

</head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;" class="card z-depth-1">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   

    

    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1" id = "rech_11">
        <div class="row" style="margin:0px 0px;height:40px;">
            <div class="col-2 col-sm-2 col-md-2 text-center warm-flame-gradient" style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <i class="fas fa-mobile-alt fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 text-left " style="height:40px;margin:0px 0px;padding: 0px 4px;">
                <p class="h6 amber-text text-darken-3 " style="margin:7px 4px;">Recharge Report</p>
            </div>
            <div class="col-4 col-sm-4 col-md-4 text-right " style="height:40px;margin:0px 0px;padding: 0px 6px;">
                <a  class="btn btn-sm sunny-morning-gradient" 
                                style="margin:4px 2px;padding: 3px 5px;border-radius: 5px;" id = "a_filter">
                                <i class="fas fa-filter fa-2x white-text pr-1 pt-1 pl-1" aria-hidden="true"></i>
                
                </a>
            </div>
        </div>
        
    </div>
    
    <input type="hidden" id = "auth_token" name="auth_token" value="{{ $auth_token }}">

    <div style="margin:10px 0px;height:410px;overflow-y:scroll;" id = "rech_1"> 

        <?php 
            echo $recharge;
        ?>
    </div>

   

    @include('android.common_bottom')
    
    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 270);
            });

            $(window).trigger('resize');

           

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
               
                if(gid.indexOf("a_filter") !== -1)
                {
                    var at = $('#auth_token').val();

                    window.open("<?php echo url('/'); ?>/mobile_user_recharge_ap_2/" + at,'_self');
                   
                }

         
            });

           
          
        });
    </script>
</body>
</html>