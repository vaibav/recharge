<!DOCTYPE html>
<html lang="en">
    <head>
        @include('android.common_top')

    </head>
<body style="background-color: #ffffff;overflow-x:hidden;height: 90vh;overflow-y:scroll;">
    
    @include('android.common_loader')

    @include('android.common_title')

   <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 90px;border-radius:15px;" class="card z-depth-2">
            <div class="card-body">
                <h5 class="card-title" style="margin-bottom: 4px;"><a>Hello <?php echo $user_name ?>!</a></h5>
                <p class="card-text" style="margin-bottom: 4px;">{{ $user_bal }}</p>
               
            </div>
    </div>
   
    <!-- Title-->
    <div style="margin:10px 10px;" class="card z-depth-1 rounded-pill" id = "rech_11">
        <div class="row" style="margin:0px 0px;">
            <div class="col-2 col-sm-2 col-md-2 text-center " style="margin:0px 0px;padding:0px 2px;padding-top:5px;">
                <a class='floating-1 btn-small waves-effect waves-light #00acc1 info-color white-text text-center' 
                            id = "a_ic"><i class='far fa-user fa-2x white-text'></i></a>
            </div>
            <div class="col-10 col-sm-10 col-md-10 text-left " style="margin:0px 0px;padding: 0px 4px;">
                <p class="h5 #0288d1 light-blue-text text-darken-3 " 
                        style="margin:10px 4px;font-family: 'Roboto Condensed', sans-serif;font-weight:520;font-size:20px;">Password Change</p>
            </div>
           
        </div>
        
    </div>
    

    <div style="margin:10px 10px;height:410px;overflow-y:scroll;border-radius:15px;" id = "rech_1" class="card z-depth-2"> 

        <div class = "row rounded-pill" style="margin:4px 2px;padding: 0px 0px;">
            <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                    <form id="rech_form" class="form-horizontal" action="{{url('mobile_user_password_rt_2')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id ="auth_token" name="auth_token" value="{{ $auth_token }}">
                    
                        <div style="height:20px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12" style = "padding-right:8px;">
                                <input class="form-control curve-border" type="password"
                                placeholder="new password" id="id_new_pwd" name="new_pwd">
                            </div>
                            
                        </div>
                            
                      
                        <div style="height:20px"></div>
                        <div class="row pl-3 pr-3">
                            <div class="col-12 col-sm-12 col-md-12 text-right">
                                    <button class="btn btn-small waves-effect waves-light #f4511e deep-orange darken-1 white-text" id="btn_submit">submit</button>
                            </div>
                        </div>
                        
                    </form>
            </div>
        </div>
    </div>

    @include('android.common_bottom')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('Password has been changed Successfully...!');
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('Error!', 'Sorry...!');
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>

    
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            
            $('#loader').hide();

            $(window).resize(function() {
                $('#rech_1').height($(window).height() - 250);
            });

            $(window).trigger('resize');

            $(document).on('click', "a",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("a_log") !== -1)
                {
                    window.open("logout:" ,'_self');
                   
                }

            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var rm = $('id_new_pwd').val();
                
                //alert(ax);
                if(rm != "")
                {

                    $.confirm({
                        title: 'Change Password!',
                        content: 'Are You Sure? ',
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('mobile_user_password_rt_2')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                    
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(rm == "")
                {
                    $.alert('Please Enter Password!');
                    $('#btn_submit').prop('disabled', false);
                }
                
                
            }); 
           
        });
    </script>
</body>
</html>