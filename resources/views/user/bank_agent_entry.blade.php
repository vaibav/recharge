<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Bank Agent Entry</span>
                    <a class='btn-floating halfway-fab waves-effect waves-light red' id='btn_back'><i class='material-icons'>arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                            <form  id = "rech_form" class="form-horizontal" action="{{url('bank_agent_entry_store')}}" method="post" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">

                               
                                <div class="row" style="margin-bottom: 2px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="agent_msisdn" name = "agent_msisdn" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_msisdn') }}</span>
                                        <label for="agent_msisdn">Mobile No</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                       
                                    </div>
                                </div>

                            
                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="agent_name" name = "agent_name" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_name') }}</span>
                                        <label for="agent_name">Agent Name</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="agent_cname"  name = "agent_cname" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_cname') }}</span>
                                        <label for="agent_cname">Company Name</label>
                                    </div>
                                    
                                </div>

                            
                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="agent_address" name = "agent_address" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_address') }}</span>
                                        <label for="agent_address">Address</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="agent_city"  name = "agent_city" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_city') }}</span>
                                        <label for="agent_city">city</label>
                                    </div>
                                    
                                </div>

                            
                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:5px;margin-bottom: 3px;">
                                        <select id="agent_state_code" name = "agent_state_code" style="color:#9e9e9e;">
                                            <option value="" disabled selected>Select State</option>
                                            <option value="6">Tamil Nadu</option>
                                            <option value="1">New Delhi</option>
                                            <option value="5">Kerala</option>
                                        </select>
                                        
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:5px;margin-bottom: 3px;">
                                        <input id="agent_pincode"  name = "agent_pincode" type="text" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('agent_pincode') }}</span>
                                        <label for="agent_pincode">Pincode</label>
                                    </div>
                                    
                                </div>

                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="col s12 m12 l12 xl12 left-align" style="margin-top:4px;margin-bottom: 4px;margin-left:60px;">
                                        <button class="btn waves-effect waves-light " type="submit" name="action" id="btn_submit">Submit
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>
                        </form>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    window.location.href = '".url('/'). "/bank_agent_otp_entry';
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                    //window.location.href = '".url('/'). "/bank_agent_otp_entry';
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#btn_back').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('bank_agent_view')}}", "_self");
            });


            $("#agent_msisdn, #agent_pincode").keydown(function (e) 
            {
                numbersExactly(e);
            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Add!'
                        }).then((result) => {
                        if (result.value) {
                            //$('#loader').show();
                            $('#rech_form').attr('action', "{{url('bank_agent_entry_store')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                   
            }); 

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
            
           
      });
    </script>
    </body>
</html>
