<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VaibavOnline</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|PT+Sans&display=swap" >

    <style type="text/css">
        .select-wrapper input.select-dropdown{
            color:#99A3A4;
            
        }

        input[type=text]:not(.browser-default)
        {
            font-size:20px;
            border-bottom: 1.4px solid #D5DBDB;
        }

        .p_amt {font-size: 16px;font-weight: 760;color: #444;font-family: 'Open Sans', sans-serif;}
        .p_net {font-size: 16px;font-weight: 760;color: #444;font-family: 'Open Sans', sans-serif;}
        .p_mob {font-size: 16px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        .p_opr {font-size: 14px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        .p_upa {font-size: 16px;font-weight: 560;font-family: 'PT Sans', sans-serif;color: #99A3A4;}
        .p_dna {font-size: 16px;font-weight: 560;font-family: 'PT Sans', sans-serif;color: #99A3A4;}
        


        
    </style>
   
</head>
<body style="background-color: #ffffff;">

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(0,0,0,0.5); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 30%;">
            <div class="spinner-layer spinner-red-only">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"> <div class="circle"></div></div>
                <div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

   <div class="row">
        <div class="col s12 m12" style="padding: 0px 0px; margin: 0px 0px;">
          <div class="card" style="padding: 0px 0px; margin: 0px 0px;box-shadow: 0px 0px 0px 0px #888888; ">
            <div class="card-image" style="height: 120px;background-color: #673AB7;">
                <span class="card-title" style="padding-bottom: 44px; padding-top: 34px;"><b>Prepaid</b></span>
                <a class="btn-floating halfway-fab waves-effect waves-light red" id = "btn_det"></a>
            </div>

            <div class="card-content section scrollspy" style="background-color: #ffffff;padding: 20px 12px;" id = "rech_1">
              <!-- New Content-->
              <div class="row">
                <div class="col s12 m12" style="padding: 0px 15px;">
                    <form id="rech_form" class="form-horizontal" action="{{url('recharge_mobile_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                        
                        <div class="row" >
                            <div class="col s4 m4" style="margin: 4px 0px;">
                                <span class="card-title grey-text " style="padding-bottom: 5px; padding-top: 5px;"><b>Entry</b></span>
                            </div>
                             <div class="col s8 m8 right-align" style="margin: 4px 0px;">
                                <span class="p_mob grey-text " style="padding-bottom: 5px; padding-top: 8px;">Hi <?php echo $user_name ?>! &nbsp;&nbsp; <?php echo $user_bal ?></span>
                            </div>
                        </div>

                        

                        <div class="row" >
                                <div class="input-field col s12 m12" style="margin: 0px 0px;">
                                    <input placeholder="Mobile No" id="id_user_mobile" name  = "user_mobile" type="text" class="validate">
                                    <label style="color:#949898;font-weight: 550;" for="id_user_mobile">Mobile No</label>
                                </div>
                        </div>
                        <div class="row" >
                                <div class="input-field col s10 m10" style="padding: 3px 10px;margin: 8px 0px;">
                                    <select class="grey-text text-darken-2" name="net_code" id = "id_net_code">
                                    <?php
                               
                                        foreach($network as $f)
                                        {
                                            echo "<option value = '".$f->net_code."'>".$f->net_name."</option>";
                                            
                                        }   
                                    ?>
                                
                                    </select>
                                    <label style="color:#949898;font-weight: 550;">Network</label>
                                </div>

                                <div class="input-field col s2 m2 center-align" style="padding: 2px 4px;margin: 8px 0px;">
                                    <button class="btn-floating btn-small waves-effect waves-light #880e4f pink darken-4" id="plan_121">121</button>
                                </div>
                        </div>
                        <div class="row">

                                <div class="input-field col s12 m12" style="margin: 8px 0px;">
                                        <input placeholder="Amount" id="id_user_amount" name = "user_amount" type="text" class="validate">
                                        <label style="color:#949898;font-weight: 550;" for="id_user_amount">Amount</label>
                                </div>
                        </div>

                        <div class="row">    
                                <div class="input-field col s12 m12 right-align" style="margin: 4px 0px;">
                                        <button class="btn btn-small waves-effect waves-light #6a1b9a purple darken-3" id="btn_submit" name="btn_submit">submit</button>
                                </div>
                            
                        </div>

                    </form>
                </div>
              </div>

              <!-- End -->
            </div>
            

            <div class="card-content section scrollspy" style="background-color: #ffffff;padding: 15px 12px;height:400px;overflow-y:scroll;display: none;" id = "rech_2">
                
                <span class="card-title grey-text " style="padding-bottom: 5px; padding-top: 5px;"><b>Report</b></span><br>
                <!-- New Content-->
                <?php echo $recharge; ?>
              

                
                      


                 
                <!-- End -->
              </div>
          </div>
        </div>
      </div>


       <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content" style = "padding:5px 5px;">
                <h5>Info</h5>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>
    

    
    <script src='https:////cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

      <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Congratulations!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                    
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 30)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 31)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('.modal').modal();
                    $('.modal').modal('open');

                    $('#id_ebb').val('2');
                   
                    //$('#id_net_type_code').val(".$net_type.");
                    $('#id_net_code').val(".$net_code.");
                    //$('#".$net_code."').prop('checked', true);
                    $('#id_user_mobile').val(".$mob.");
                    //$('#id_user_mobile').addClass('active');
                    $('#id_mob').text(".$mob.");

                    $('input[name=group1]').change(function (e) {
                        var amt = $(this).val();
                        $('#id_user_amount').val(amt);
                        //$('#id_user_amount').addClass('active');
                        //$('#id_amt').text(amt);
                        M.updateTextFields();
                    });

                    M.updateTextFields();
                    $('select').formSelect();

                });
                </script>";
            }
            else if($op == 21)
            {
                
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            //M.toast({html: 'I am a toast!', classes: 'rounded'});

            $('#loader').hide();
            $('select').formSelect();
            //$('#loader').show();

            $('#btn_det').html("<i class='material-icons'>multiline_chart</i>");

            var $header = $('.card-image');
            var $browser = $(window).height();
            var $content = $('#rech_2');
            var $window = $(window).on('resize', function(){
                var height = $browser - $header.height();
                height = height - 30;
                $content.height(height);
            }).trigger('resize'); //on page load



            $('#btn_det').on('click',function(e)
            {
                if($('#rech_2:visible').length == 0)
                {
                    $('#rech_2').show("slow");
                    $('#rech_1').hide("slow");
                    $('#btn_det').html("<i class='material-icons'>reply</i>");

                }
                else
                {
                    $('#rech_1').show("slow");
                    $('#rech_2').hide("slow");
                    $('#btn_det').html("<i class='material-icons'>multiline_chart</i>");
                }
            });

            $("#id_user_mobile").keydown(function (e) 
            {
                numbersExactly(e);
            });
            $("#id_user_amount").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                
                var um = $('#id_user_mobile').val();
                var ua = $('#id_user_amount').val();
                
                //alert(ax);
                if(um != "" && ua != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('recharge_mobile_store')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit').prop('disabled', false);
                }
                
                
                
            }); 

            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code =  $('option:selected', '#id_net_code').text();
                var usr_mobe = $('#id_user_mobile').val();

               

                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                    $('#rech_form').submit();
                }
                    
                /*if(net_code != "" && usr_mobe != "")
                {
                    $('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                    $('#rech_form').submit();
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }*/
               
                    
                
                
            });

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }


        });
    </script>
</body>
</html>