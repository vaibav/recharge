<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        <style>
          
          body 
          {
            background-color: #6D4C41;
          }

            .content {
            position: inherit;
            top: 20%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
                    transform: translate(-50%, -50%);
            height: auto;
            overflow: hidden;
            font-family: 'Ubuntu', sans-serif;
            font-size: 18px;
            line-height: 40px;
            color: #ecf0f1;
            }
            .content__container {
            font-weight: 100;
            overflow: hidden;
            height: 40px;
            padding: 0 40px;
            }


            .content__container__text {
            display: inline;
            color : #F5B7B1;
            font-size: 20px;
            float: left;
            margin: 0;
            }
            .content__container__list {
            margin-top: 0;
            padding-left: 90px;
            text-align: left;
            list-style: none;
            
            }

            .content__container__list1 {
           
                padding-left: 200px;
            
            text-align: left;
            list-style: none;
            
            }

            .blinking{
                animation:blinkingText 0.8s infinite;
            }
            @keyframes blinkingText{
                0%{     color: #F1C40F;    }
                49%{    color: transparent; }
                50%{    color: transparent; }
                99%{    color:transparent;  }
                100%{   color: #F1C40F;    }
            }

        </style>
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide">
        @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Dashboard', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
          <div class="row">
                <div class="col-lg-6" style="padding:5px 15px;">
                    <div class="content">
                        <div class="content__container">
                            <p class="content__container__text">
                            News
                            </p>
                            <?php 
                                $stx = "";
                                foreach($offer as $r)
                                {
                                    $stx = $stx . $r->offer_details. " <i class='fa fa-bell-o blinking' aria-hidden='true' class=''></i>";
                                }
                            ?>
                            <p class="content__container__list">
                            <marquee direction='left'><?php echo $stx; ?></marquee>
                            </p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6" style="padding:5px 5px;">
                    <div class="content">
                        <div class="content__container">
                            <p class="content__container__text">
                            Inactive Networks
                            </p>
                            <?php 
                                $sty = "";
                                //print_r($in_active);
                                foreach($in_active as $r)
                                {
                                    $sty = $sty . $r->net_name. " <i class='fa fa-exclamation-triangle blinking' aria-hidden='true' class=''></i>";
                                }
                            ?>
                            <p class="content__container__list1">
                            <marquee direction='right'><?php echo $sty; ?></marquee>
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>


            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                        Dashboard
                         <span class="badge badge-pill badge-danger float-right" id="tim" style='font-size:14px;'></span>
                    </div>
                    <div class="card-body" style="padding:6px 10px;">
                        

                        <div class = "row">
                            <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-responsive-sm table-bordered">
                                  <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET.PER(%) / SURPLUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR. TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                                      
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body">
                                   
                                    
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- End -->

                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	  {
            var c = 1;
            load_data();
            window.setInterval(function(){
                /// call your function here
                load_data();
			}, 22000);
         
            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'apipartnerresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d.toLocaleString());
						if(c == 1)
						{
							$('#tim').css("background-color", "#EC7063");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("background-color", "#27AE60");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

      });
    </script>
    </body>
</html>
