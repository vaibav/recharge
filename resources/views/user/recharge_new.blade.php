<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')

        <style>
            .input-field input[type=text] {
                    background-color: transparent; border: 1px solid #e3e6f0; border-radius: 25px; outline: none; height: 2.5rem; 
                    width: 100%; font-size: 15px; margin: 0 0 8px 0; padding: 0; padding-left: 15px; line-height: 1.05;
                    -webkit-box-shadow: none; box-shadow: none; -webkit-box-sizing: inherit; box-sizing: inherit;
                    -webkit-transition: border .3s, -webkit-box-shadow .3s; transition: none;
                }
                
                .input-field input:focus + label { color: #5D6D7E; }
                .row .input-field input:focus { border: 0.5px solid #5D6D7E; box-shadow: 1px 1px 1px #5D6D7E; }
                .input-field input[type=text].valid { border: 1px solid #e3e6f0; box-shadow: 0 1px 0 0 #e3e6f0; }
                .input-field>label:not(.label-icon).active {
                    -webkit-transform: translateY(-24px) scale(0.8);
                    transform: translateY(-24px) scale(1);
                    -webkit-transform-origin: 0 0;
                    transform-origin: 0 0;
                }
        </style>
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                <div class="row" style="margin-bottom: 5px;">
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s4 m4 l2 xl2  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                News :
                            </div>
                            <div class="col s8 m8 l10 xl10 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $stx = "";
                                    foreach($offer as $r)
                                    {
                                        $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $stx; ?> </marquee>
                            </div>
                    </div>
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s9 m9 l4 xl4  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                Inactive Networks :
                            </div>
                            <div class="col s3 m3 l8 xl8 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $sty = "";
                                    //print_r($in_active);
                                    foreach($in_active as $r)
                                    {
                                        $sty = $sty . $r->net_name. " &nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $sty; ?> </marquee>
                            </div>
                    </div>
                </div>

            <div class="card " >
                
               
                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l12 x12">
                            <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('recharge_store')}}" method="post" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">
                                <input type = "hidden" id = "id_ebb" value ='0'>
                                
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="col s12 m12 l4 xl4" style="margin: 3px 0px;">

                                        <div class="input-field col s12 m12 l12 xl12 center-align" style="margin: 10px 0px;">
                                            <button class="btn-floating text-center #7b1fa2 blue darken-2" id="re_pre"><i class="large material-icons">phone_android</i></button>
                                            <button class="btn-floating text-center #7b1fa2 blue darken-2" id="re_dth"><i class="medium material-icons">desktop_windows</i></button>
                                            <button class="btn-floating text-center #7b1fa2 blue darken-2" id="re_pos"><i class="material-icons">speaker_phone</i></button>
                                        </div>

                                        

                                        <!-- Form Objects -->
                                        <div class="input-field col s12 m12 l12 xl12" style="margin: 10px 0px;display:none;">
                                            <select id="id_net_type_code" name="net_type_code" >
                                                <?php
                                                    foreach($networktype as $f)
                                                    {
                                                        echo "<option value='".$f->net_type_code."'>".$f->net_type_name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="input-field col s10 m10 l10 xl10" style="margin: 10px 0px;">
                                            <input id="id_user_mobile" type="text" name="user_mobile" placeholder = "Mobile No">
                                            <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
                                        </div>

                                        <div class="input-field col s2 m2 l2 xl2" style="margin: 10px 0px;">
                                            <button class="btn-floating text-center #7b1fa2 purple darken-2" id="plan_121">121</button>
                                        </div>

                                        <div class="input-field col s10 m10 l10 xl10" style="margin: 10px 0px;">
                                            <select id="id_net_code" name="net_code" >
                                                <?php
                                                    foreach($network as $f)
                                                    {
                                                        echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        
                                        </div>

                                        <div class="input-field col s2 m2 l2 xl2" style="margin: 10px 0px;">
                                            <button class="btn-floating text-center #7b1fa2 purple darken-2" id="plan_mob">plan</button>
                                        </div>

                                       

                                        <div class="input-field col s12 m12 l12 xl12" style="margin: 10px 0px;">
                                            <input id="id_user_amount" type="text" name="user_amount" placeholder = "Amount">
                                            <span class="text-danger">{{ $errors->first('user_amount') }}</span>
                                        </div>

                                        <div class="input-field col s6 m6 l6 xl6 left-align" style="margin: 10px 0px;">
                                            <button class="btn waves-effect waves-light #ff6f00 blue darken-4 " type="submit"  id="btn_submit" name="btn_submit">Recharge
                                                    <i class="material-icons right">send</i>
                                            </button>
                                        </div>

                                        <div class="input-field col s6 m6 l6 xl6 left-align" style="margin: 10px 0px;">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  id="btn_submit" name="btn_submit">Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                        </div>
                                        
                                        <!-- End Form Objects -->
                                    </div>

                                    <div class="col s12 m12 l8 xl8" style="margin: 3px 0px; height:300px;overflow-y:scroll;">
                                        <!-- plan details -->
                                        <table class="bordered striped responsive-table" id="rech_offer_body">
                                        </table>
                                        
                                         <!-- End plan -->
                                    </div>
                                </div>

                            </form>

                            <!-- End Form-->
                        </div>
                        
                    </div>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style="font-size:12px;padding:7px 8px;">NO</th>
                                <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR.ID</th>
                                <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;">BILL</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
                
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    @include('user.bottom1')

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 30)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 31)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('.tabs').tabs();
                    //$('.modal').modal();
                    //$('.modal').modal('open');

                    $('#id_ebb').val('2');
                   
                    $('#id_net_type_code').val(".$net_type.");
                    $('#id_net_code').val(".$net_code.");
                    $('#id_user_mobile').val(".$mob.");
                    //$('#id_user_mobile').addClass('active');
                    $('#id_mob').text(".$mob.");

                    $(document).on('click', 'a', function(e){
                       
                        var amt = $(this).text();
                        var id = this.id;
                        if(id == 'id_off')
                        {
                            $('#id_user_amount').val(amt);
                        }
                    });

                    $('input[name=group1]').change(function (e) {
                        var amt = $(this).val();
                        $('#id_user_amount').val(amt);
                        //$('#id_user_amount').addClass('active');
                        $('#id_amt').text(amt);
                        M.updateTextFields();
                    });

                    M.updateTextFields();

                });
                </script>";
            }
            else if($op == 22)
            {
                

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    
                    $(document).on('click', 'a', function(e){
                       
                        var amt = $(this).text();
                        var id = this.id;
                        if(id == 'id_off')
                        {
                            $('#id_user_amount').val(amt);
                        }
                    });
                    
                    M.updateTextFields();

                });
                </script>";
            }
            else if($op == 21)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $con_no = $op1['mob'];
                $con_na = $op1['name'];
                $con_am = $op1['amount'];
                $con_du = $op1['due'];

                file_put_contents(base_path().'/public/sample/eb_data.txt', $net_code."---".$net_type."---".$con_no."---".$con_na."---".$con_am."---".$con_du);

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('.modal').modal();
                    $('.modal').modal('open');

                    $('#rc1').hide();
                    $('#eb1').show();
                    $('#eb2').show();
                    $('#eb3').show();

                    getNetworkData(".$net_type.");

                    var con_na = '".$con_na."';

                    $('#id_net_type_code').val(".$net_type.");
                    $('#id_net_code').val(".$net_code.");
                    $('#id_eb_cons_no').val('".$con_no."');
                    $('#id_eb_cons_amount').val(".$con_am.");
                    M.updateTextFields();
                    
                    if (con_na != 'NONE')
                    {
                        $('#eb1').show();
                        $('#eb2').show();
                        $('#eb3').show();
                        $('#id_eb_cons_name').val('".$con_na."');
                        $('#id_eb_cons_duedate').val('".$con_du."');
                        M.updateTextFields();
                    }
                    else
                    {
                        $('#eb_1').hide();
                        $('#eb_2').hide();
                    }
                    $('#id_ebb').val('1');

                    function getNetworkData(net_type)
                    {
                        
                        $.ajaxSetup({
                            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                        });
        
                        $.ajax({
                            url:'network_data/' + net_type,
                            type: 'GET',
                            dataType: 'json',
                            success: function( response ){
                                var zz=response['data'];
                                $('#id_net_code').empty();
                                for(var i=0;i<zz.length;i++)
                                {
                                   
                                    if(zz[i][0]!='')
                                        $('#id_net_code').append('<option value='+zz[i][0]+'>'+zz[i][1]+'</option>');
                                }
                                $('#loader').hide(); 
                            },
                            error: function (xhr, b, c) {
                                console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            }
                        });
                    }

                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            $('.tabs').tabs();
            

            $('#id_net_type_code, #id_net_code').on('contentChanged', function() {
                $(this).material_select();
            });

           var c = 1;
            $('#loader').hide();
            load_data();
            
            window.setInterval(function(){
                /// call your function here
                load_data();
            }, 19000);

            

            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'userresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
                        $('#tim').html(d.toLocaleString());
                        if(c == 1)
                        {
                            $('#tim').css("background-color", "#EC7063");
                            c = 2;
                        }
                        else if(c == 2)
                        {
                            $('#tim').css("background-color", "#27AE60");
                            c = 1;
                        }
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function loadPrepaid()
            {
                var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
                getNetworkData(ty);
            }

            $('#re_pre').on('click',function(e)
            {
                e.preventDefault();
                var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
                getNetworkData(ty);
            });

            $('#re_dth').on('click',function(e)
            {
                e.preventDefault();
                var ty = $("#id_net_type_code option:contains('DTH')").attr('selected', true).val();
                getNetworkData(ty);
            });

            $('#re_pos').on('click',function(e)
            {
                e.preventDefault();
                var ty = $("#id_net_type_code option:contains('POSTPAID')").attr('selected', true).val();
                getNetworkData(ty);
            });

            $("#id_net_type_code").change(function(e)
            {
                var ty = $('option:selected', this).val();
                var ax = $('option:selected', this).text();
                $('#loader').show();
                if(ty != "-")
                {
                    getNetworkData(ty);
                    if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
                    {
                        $('#rc1').show();
                        $('#eb1').hide();
                        $('#eb2').hide();
                        $('#eb3').hide();

                        if(ax == "PREPAID")
                            $("#plan_121").html('121');
                        else if(ax == "DTH")
                            $("#plan_121").html('Info');
                    }
                    else if(ax == "BILL" || ax == "BILL PAYMENT")
                    {
                        $('#rc1').hide();
                        $('#eb1').show();
                        $('#eb2').show();
                        $('#eb3').show();
                    }
                    

                }
                    
            });

            function getNetworkData(net_type)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $.ajax({
                    url:'network_data/' + net_type,
                    type: 'GET',
                    dataType: "json",
                    success: function( response ){
                        var zz=response['data'];
                        $('#id_net_code').empty();
                        for(var i=0;i<zz.length;i++)
                        {
                            //alert(zz[i][0]+"----"+zz[i][1]);
                            if(zz[i][0]!="")
                            {
                                var $newOpt = $("<option>").attr("value",zz[i][0]).text(zz[i][1]);
                                $('#id_net_code').append($newOpt);
                            }
                                //$('#id_net_code').append('<option value='+zz[i][0]+'>'+zz[i][1]+'</option>');
                        }
                        $('#id_net_code').formSelect();
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            $("#id_user_mobile, #id_eb_cons_mobile").keydown(function (e) 
            {
                numbersExactly(e);
            });
            $("#id_user_amount, #id_eb_cons_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $("#id_user_mobile").keyup(function (e) 
            {
                var mob = $("#id_user_mobile").val();
                $("#id_mob").text(mob);
            });

            $("#id_user_amount").keyup(function (e) 
            {
                var mob = $("#id_user_amount").val();
                $("#id_amt").text(mob);
            });

            
            
                

           /* $("#modal1").on('hidden.bs.modal', function(e){
                
                e.preventDefault();
                alert("hello...");
                var amt = $('input[name=group1]:checked').val();
                alert(amt);
               /* $('#id_net_code').val('0');
                $('#id_net_type_code').val('0');
                $('#id_user_mobile').val('0');
                $("#id_mob").text(0);*/
                
               /* if(amt != "")
                {
                    var amt1 = amt.split(".");
                    if(amt1.length >= 2)
                    {
                        $('#id_user_amount').val(amt1[0]);
                        //$("#id_amt").text(amt1[0]);
                    }
                    else
                    {
                        $('#id_user_amount').val(amt);
                        //$("#id_amt").text(amt);
                    }
                }
                

            });*/


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#btn_submit').prop('disabled', true);
                var ax = $('option:selected', '#id_net_type_code').text();
                //alert(ax);
                if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
                {
                    var form = $(this).parents('form');
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Recharge!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('recharge_store')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No Recharge...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                    
                }
                else if(ax == "BILL" || ax == "BILL PAYMENT")
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Recharge!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('recharge_bill')}}");
                            $('#rech_form').submit();
                            //swal("Cancelled", "Under Progress...", "error");
                            //$('#btn_submit').prop('disabled', false);
                        }
                        else
                        {
                            swal("Cancelled", "No Recharge...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                }
                
                
            }); 

            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
                var ax = $('option:selected', '#id_net_type_code').text();
                if(ax == "PREPAID")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_offers1')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                    else if(usr_mobe == "")
                    {
                        swal("Cancelled", "Please Enter Mobile No...", "error");
                    }
                }
                else if(ax == "DTH")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('dth_offers')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                    else if(usr_mobe == "")
                    {
                        swal("Cancelled", "Please Enter Mobile No...", "error");
                    }
                }
                
            });

            $('#plan_mob').on('click',function(e)
            {
                e.preventDefault();
                var ax = $('option:selected', '#id_net_type_code').text();
                if(ax == "PREPAID")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" )
                    {
                        $('#plan_mob').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('mobile_plans')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                }
                
                
            });

            $('#eb_info').on('click',function(e)
            {
                e.preventDefault();
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_eb_cons_no').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    $('#eb_info').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('eb_info')}}");
                    $('#rech_form').submit();
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Consumber No...", "error");
                }
                
            });

            $('#tbl_body').delegate('button', 'click', function(e) {
                    e.preventDefault();
                    var gid=this.id;
                    var nid=gid.split("bill1_");
                    if(nid.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid[1];
                                window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

                    var nid1=gid.split("bill2_");
                    if(nid1.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid1[1];
                                window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

            });
                
            function numbersOnly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                // let it happen, don't do anything
                return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
                }
            }

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
      });
    </script>
    </body>
</html>
