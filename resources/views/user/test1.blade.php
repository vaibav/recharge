<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide">
        @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Recharge', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card">
                    <div class="card-header">
                        <div class = "row">
                            <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <i class="fa fa-align-justify"></i> Please Wait...
                            </div>
                            <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">

                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="padding:6px 10px;">
                      
                      <div class = "row">
                          
                          </div>
                      </div>
                      

                     
                     

                        

                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	   {
            

           $.ajax({
            method: 'GET', // Type of response and matches what we said in the route
            url: 'test4', // This is the url we gave in the route
            data: {'id' : 1}, // a JSON object to send back
            success: function(response){ // What to do if we succeed
                document.writeln(response); 
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
                //document.writeln('AJAX error: ' + textStatus + ' : ' + errorThrown);
            }
        });
            
            window.setTimeout(function(){

                    window.location = "{{url('recharge')}}";

            }, 4000);
            
            
            

            
               
     });
    </script>
    </body>
</html>
