<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        <?php 
            
            if($user->mode == "SUPER DISTRIBUTOR") 
            {
            ?>
                @include('user.sidebar_sdistributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_user')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            else if($user->mode == "DISTRIBUTOR" ) {
                ?>
                @include('user.sidebar_distributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_distributor')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            
        ?>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Payment Request</span>
                    
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l5 xl5">
                            <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('paymentrequest_store_user')}}" method="post" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l7 xl7" style="margin: 3px 0px;">
                                        <input type="text" id="id_user_amount" name="user_amount" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('user_amount') }}</span>
                                        <label for="id_user_amount">Amount</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l7 xl7" style="margin: 3px 0px;">
                                        <input type="text" id="id_user_remarks" name="user_remarks" class="validate">
                                        <span class="text-danger red-text">{{ $errors->first('user_remarks') }}</span>
                                        <label for="id_user_remarks">Remarks</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                        </div>
                        
                        <!-- Table --> 
                        <div class="col s12 m12 l7 xl7">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Req Amt</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Req Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Trans Amt</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        
                                        $j = 1;
                                        foreach($pay1 as $f)
                                        {
                                          if($f->payment_mode == "FUND_TRANSFER")
                                          {
                                              echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_amount."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_req_date."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->grant_user_amount."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->grant_date."</td>";
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_remarks."</td>";
                                              if($f->trans_status == 0)
                                                  echo "<td style='font-size:12px;padding:7px 8px;'>PENDING</td>";
                                              else if($f->trans_status == 1)
                                                  echo "<td style='font-size:12px;padding:7px 8px;'>SUCCESS</td>";
                                              else if($f->trans_status == 2)
                                                  echo "<td style='font-size:12px;padding:7px 8px;'>FAILURE</td>";
                                              echo "</tr>";
                                              $j++;
                                          }
                                          
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>

                        </div>
                    </div>
                
               
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('user.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
            session()->forget('result');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

            $("#id_user_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
  
      });
    </script>
    </body>
</html>
