<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style ='background-color: #0E6655;'>
        @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Recharge Details', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i>Recharge Details</div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                          <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <form class="form-horizontal" action="{{url('user_rechargedetails_sdistributor_view')}}" method="get" accept-charset="UTF-8">
                                  
                                  
                                  
                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">From Date</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="id_f_date" type="text" name="f_date" placeholder="YYYY-MM-DD" value = "<?php echo date('Y-m-d'); ?>">
                                      <span class="text-danger">{{ $errors->first('f_date') }}</span>
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">To Date</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="id_t_date" type="text" name="t_date" placeholder="YYYY-MM-DD" value = "<?php echo date('Y-m-d'); ?>">
                                      <span class="text-danger">{{ $errors->first('t_date') }}</span>
                                    </div>
                                  </div>

                                  <hr>
                                  <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal">User Name</label>
                                    <div class="col-sm-8">
                                      <input class="form-control" id="id_user_name" type="text" name="user_name" placeholder="">
                                    
                                        <div>
                                                <select id="id_user_list" size="10" class="form-control" style=" display:none;" >
                                                <?php
                                                    $str = "";
                                                    foreach($user1 as $f)
                                                    {
                                                        $per_name = "";
                                                        foreach($user2 as $r)
                                                        {
                                                            if($f->user_code == $r->user_code)
                                                            {
                                                                $per_name = $r->user_per_name;
                                                                break;
                                                            }
                                                        }
                                                        $str = $str."<option value='".$f->user_code."'>".$f->user_name."-".$f->user_code."-".$per_name."</option>";
                                                    }
                                                    echo $str;
                                                ?>
                                                </select>
                                         </div>
                                    </div>
                                  </div>
                                 
                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Status</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="id_rech_status" name="rech_status" >
                                            <option value="-">---Select---</option>
                                            <option value="SUCCESS">SUCCESS</option>
                                            <option value="FAILURE">FAILURE</option>
                                            <option value="PENDING">PENDING</option>
                                          
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group row" id = "id_f1">
                                    <label class="col-sm-5 col-form-label" for="input-normal" id="id_lab_min">Mobile No</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="id_rech_mobile" name="rech_mobile" >
                                          
                                    </div>
                                  </div>

                                  <div class="form-group row" id = "id_f2">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Amount</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" id="id_rech_amount" name="rech_amount" >
                                         
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Network</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="id_net_code" name="net_code" >
                                          <option value = "-">---Select---</option>
                                          <?php
                                                foreach($network as $f)
                                                {
                                                    echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                                }
                                          ?>
                                          
                                      </select>
                                    </div>
                                  </div>  
                                  
                                  <div class="form-group row" style="padding:6px 15px;">
                                      <button class="btn btn-sm btn-primary" type="submit">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>
                                  </div>

                                  <div class="form-group row" style="padding:6px 15px;">
                                        
                                         
                                  </div>

                                </form>
                          </div>
                         
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	   {
            
          $('#id_f_date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
            $('#id_t_date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
            
            $('#id_user_name').keyup(function(e) 
            {
                //$('#myModal').modal();
                filter();
            });
            $('#id_user_name').blur(function(e) 
            {
                //$('#myModal').modal();
                $('#id_user_list').hide("slow");
            });

            function filter() {
                var keyword = document.getElementById("id_user_name").value;
                var select = document.getElementById("id_user_list");
                for (var i = 0; i < select.length; i++) {
                    var txt = select.options[i].text;
                    if (txt.substring(0, keyword.length).toLowerCase() !== keyword.toLowerCase() && keyword.trim() !== "") {
                        select.options[i].style.display = 'none';
                    } else {
                        select.options[i].style.display = 'list-item';
                    }
                }
                $('#id_user_list').show("slow");
            }
            $('#id_user_list').click(function(e) 
            {
                //$('#myModal').modal();
                
                var a =$('#id_user_list :selected').text();
                var b = a.split("-");
                //alert(b[0]);
                $('#id_user_name').val(b[0]);
               
                //$('#pur_a3').val(b[1]);
                $('#id_user_list').hide("slow");
            });
           
            $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
            {
                numbersOnly(e);
            });

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
  
     });
    </script>
    </body>
</html>
