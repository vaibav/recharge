<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VaibavOnline</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|PT+Sans&display=swap" >

    <style type="text/css">
        .shadow { box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); }
        .card1
        {
            position: relative; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical;
            -webkit-box-direction: normal;  -ms-flex-direction: column;  flex-direction: column;  min-width: 0;
            word-wrap: break-word; background-color: #fff; background-clip: border-box; border: 1px solid #e3e6f0; border-radius: .35rem;
        }

        .p_amt {margin:2px 0px;font-size: 15px;font-weight: 760;color: #444;font-family: 'Open Sans', sans-serif;}
        .p_net {margin:2px 0px;font-size: 15px;font-weight: 760;color: #444;font-family: 'Open Sans', sans-serif;}
        .p_mob {margin:2px 0px;font-size: 14px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        .p_opr {margin:2px 0px;font-size: 12px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        .p_upa {margin:2px 0px;font-size: 14px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        .p_dna {margin:2px 0px;font-size: 14px;font-weight: 560;color: #99A3A4;font-family: 'PT Sans', sans-serif;}
        


        
    </style>
   
</head>
<body style="background-color: #ffffff;">

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(0,0,0,0.5); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 40%;">
            <div class="spinner-layer spinner-red-only">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"> <div class="circle"></div></div>
                <div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div style="height:180px;background-image: linear-gradient(to bottom, #CC00CC, #CC66FF);border-bottom-left-radius: 45px;border-bottom-right-radius: 45px;">
        <div class="row">
            <div class="col s2 m2 center-align" style="margin-top: 30px;padding: 0px 3px;">
                <img src = "{{ asset('img/brand/ic_launcher.png') }}" style=" height: 55px; width: 55px;"/>
            </div>
            <div class="col s7 m7 left-align" style="margin-top: 30px; margin-left: 2px;">
                    <p class="white-text" style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 660;padding: 2px 0px;margin: 0px 0px;">
                        Recharge</p>
                    <p style="font-size: 14px;font-weight: 560;color: #ffffff  ;font-family: 'PT Sans', sans-serif;padding: 1px 0px;margin: 0px 0px;">
                        Hi <?php echo $user_name ?>!</p>
            </div>
            <div class="col s2 m2 right-align" style="margin-top: 30px; margin-left: 10px;">
                    <p class="white-text" style="font-family: 'Open Sans', sans-serif;font-size: 16px;font-weight: 660;padding: 2px 0px;">
                    <?php echo $user_bal ?></p>
                   
            </div>
        </div>
    </div>

    <div style="margin-top:-50px;margin-left: 10px;margin-right: 10px;height: 80px;" class="card1 shadow">
            <div class="row" style="margin:0px 0px;">
                <div class="col s12 m12 center-align" style="margin:10px 2px;padding: 0px 4px;">
                    <a class="btn-floating btn-large waves-effect waves-light #7b1fa2 purple darken-2" style="margin: 0px 8px;" id = "a1"><i class="large material-icons">phone_android</i></a>   
                    <a class="btn-floating btn-large waves-effect waves-light #7b1fa2 purple darken-2" style="margin: 0px 8px;" id = "a2"><i class="medium material-icons">desktop_windows</i></a>   
                    <a class="btn-floating btn-large waves-effect waves-light #7b1fa2 purple darken-2" style="margin: 0px 8px;" id = "a3"><i class="material-icons">speaker_phone</i></a>   
                    <a class="btn-floating btn-large waves-effect waves-light #7b1fa2 purple darken-2" style="margin: 0px 8px;" id = "a4"><i class="material-icons">multiline_chart</i></a>   
                </div>
               
            </div>
    </div>

    <div style="margin:10px 10px;" class="card1 shadow"  id = "rech_1">
            <h5 style="margin: 10px 10px;" class="orange-text">Prepaid</h5>
            <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
                <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                        <form id="rech_form1" class="form-horizontal" action="{{url('recharge_mobile_prepaid_store')}}" method="post" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                            <div class="input-field col s12 m12">
                                <input placeholder="number" id="id_user_mobile_1" name  = "user_mobile_1" type="number" class="validate">
                                <label for="id_user_mobile_1">Mobile No</label>
                            </div>

                            <div class="input-field col s9 m9" >
                                <select name="net_code_1" id = "id_net_code_1">
                                    <option value="" disabled selected>Select Network</option>
                                    <?php
                               
                                        foreach($network1 as $f)
                                        {
                                            echo "<option value = '".$f->net_code."'>".$f->net_name."</option>";
                                            
                                        }   
                                    ?>
                                </select>
                                <label>Network</label>
                            </div>
                            <div class="input-field col s3 m3 center-align" >
                                    <a class="btn-floating btn-small waves-effect waves-light #ff6f00 amber darken-4" id="plan_121">121</a>
                            </div>

                            <div class="input-field col s12 m12">
                                    <input placeholder="0" id="id_user_amount_1" name = "user_amount_1" type="number" class="validate">
                                    <label for="id_user_amount_1">Amount</label>
                            </div>

                            <div class="input-field col s12 m12 right-align" >
                                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit_1">submit</button>
                            </div>
                        </form>
                </div>
            </div>
    </div>

    <div style="margin:10px 10px;display: none;" class="card1 shadow"  id = "rech_2">
            <h5 style="margin: 10px 10px;" class="orange-text">Dth</h5>
            <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
                <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                        <form id="rech_form2" class="form-horizontal" action="{{url('recharge_mobile_dth_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                            <div class="input-field col s12 m12">
                                <input placeholder="number" id="id_user_mobile_2" name  = "user_mobile_2" type="number" class="validate">
                                <label for="id_user_mobile_2">Dth No</label>
                            </div>

                            <div class="input-field col s9 m9" >
                                <select name="net_code_2" id = "id_net_code_2">
                                    <option value="" disabled selected>Select Network</option>
                                    <?php
                               
                                        foreach($network3 as $f)
                                        {
                                            echo "<option value = '".$f->net_code."'>".$f->net_name."</option>";
                                            
                                        }   
                                    ?>
                                </select>
                                <label>Network</label>
                            </div>

                            <div class="input-field col s3 m3 center-align" >
                                    <a class="btn-floating btn-small waves-effect waves-light #ff6f00 amber darken-4" id="plan_dth">info</a>
                            </div>
                            

                            <div class="input-field col s12 m12">
                                    <input placeholder="0" id="id_user_amount_2" name = "user_amount_2" type="number" class="validate">
                                    <label for="id_user_amount_2">Amount</label>
                            </div>

                            <div class="input-field col s12 m12 right-align" >
                                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit_2">submit</button>
                            </div>
                        </form>
                </div>
            </div>
    </div>

    <div style="margin:10px 10px;display: none;" class="card1 shadow"  id = "rech_3">
            <h5 style="margin: 10px 10px;" class="orange-text">Postpaid</h5>
            <div class = "row" style="margin:4px 2px;padding: 0px 0px;">
                <div class="col s12 m12" style="margin:2px 2px;padding: 0px 0px;">
                        <form id="rech_form3" class="form-horizontal" action="{{url('recharge_mobile_postpaid_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="auth_token" value="{{ $auth_token }}">
                            <div class="input-field col s12 m12">
                                <input placeholder="number" id="id_user_mobile_3" name  = "user_mobile_3" type="number" class="validate">
                                <label for="id_user_mobile_3">Mobile No</label>
                            </div>

                            <div class="input-field col s12 m12" >
                                <select name="net_code_3" id = "id_net_code_3">
                                    <option value="" disabled selected>Select Network</option>
                                    <?php
                               
                                        foreach($network2 as $f)
                                        {
                                            echo "<option value = '".$f->net_code."'>".$f->net_name."</option>";
                                            
                                        }   
                                    ?>
                                </select>
                                <label>Network</label>
                            </div>

                           

                            <div class="input-field col s12 m12">
                                    <input placeholder="0" id="id_user_amount_3" name = "user_amount_3" type="number" class="validate">
                                    <label for="id_user_amount_3">Amount</label>
                            </div>

                            <div class="input-field col s12 m12 right-align" >
                                    <button  class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit_3">submit</button>
                            </div>
                        </form>
                </div>
            </div>
    </div>

    <div style="margin:10px 10px;height:410px;display: none;overflow-y:scroll" class="card1 shadow"  id = "rech_4">
            <h5 style="margin: 10px 10px;" class="orange-text">Report</h5>
            <?php echo $recharge; ?>
    </div>






    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content" style = "padding:5px 5px;">
                <h5>Info</h5>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>
    

    
    <script src='https:////cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

      <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Congratulations!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                    
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 30)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 31)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('.modal').modal();
                    $('.modal').modal('open');

                 
                    $('#id_net_code_1').val(".$net_code.");
                    $('#id_user_mobile_1').val(".$mob.");
                    
                    
                    $('input[name=group1]').change(function (e) {
                        var amt = $(this).val();
                        $('#id_user_amount_1').val(amt);
                        M.updateTextFields();
                    });

                    M.updateTextFields();
                    $('select').formSelect();

                });
                </script>";
            }
            
            else if($op == 21)
            {
                
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            //M.toast({html: 'I am a toast!', classes: 'rounded'});

            $('#loader').hide();
            $('select').formSelect();
            //$('#loader').show();

            

            //var $header = $('.card-image');
            var $browser = $(window).height();
            var $content = $('#rech_4');
            var $window = $(window).on('resize', function(){
                var height = $browser - 270;
                height = height - 30;
                $content.height(height);
            }).trigger('resize'); //on page load



            $('#a1').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').show("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').hide("slow");
                $('#rech_4').hide("slow");
            });

            $('#a2').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').show("slow");
                $('#rech_3').hide("slow");
                $('#rech_4').hide("slow");
            });

            $('#a3').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').show("slow");
                $('#rech_4').hide("slow");
            });

            $('#a4').on('click',function(e)
            {
                e.preventDefault();
                $('#rech_1').hide("slow");
                $('#rech_2').hide("slow");
                $('#rech_3').hide("slow");
                $('#rech_4').show("slow");
            });

            $("#id_user_mobile_1").keydown(function (e) 
            {
                numbersExactly(e);
            });
            $("#id_user_amount_1").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_submit_1').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_1').prop('disabled', true);

                var un = $('option:selected', "#id_net_code_1").val();
                var um = $('#id_user_mobile_1').val();
                var ua = $('#id_user_amount_1').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_1').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form1').attr('action', "{{url('recharge_mobile_prepaid_store')}}");
                                    $('#rech_form1').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                else if(un == "")
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_1').prop('disabled', false);
                }
                
                
            }); 


            $('#btn_submit_2').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_2').prop('disabled', true);

                var un = $('option:selected', "#id_net_code_2").val();
                var um = $('#id_user_mobile_2').val();
                var ua = $('#id_user_amount_2').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_2').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form2').attr('action', "{{url('recharge_mobile_dth_store')}}");
                                    $('#rech_form2').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                else if(un == "")
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_2').prop('disabled', false);
                }
                
                
            }); 

             $('#btn_submit_3').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit_3').prop('disabled', true);

                var un = $('option:selected', "#id_net_code_3").val();
                var um = $('#id_user_mobile_3').val();
                var ua = $('#id_user_amount_3').val();
                
                //alert(ax);
                if(um != "" && ua != "" && un != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit_3').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form3').attr('action', "{{url('recharge_mobile_postpaid_store')}}");
                                    $('#rech_form3').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                else if(un == "")
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit_3').prop('disabled', false);
                }
                
                
            }); 

            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code =  $('option:selected', '#id_net_code_1').val();
                var usr_mobe = $('#id_user_mobile_1').val();

               

                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form1').attr('action', "{{url('recharge_offers')}}");
                    $('#rech_form1').submit();
                }
        
                
            });

            $('#plan_dth').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code =  $('option:selected', '#id_net_code_2').val();
                var usr_mobe = $('#id_user_mobile_2').val();

               

                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#plan_dth').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form2').attr('action', "{{url('dth_offers')}}");
                    $('#rech_form2').submit();
                }
        
                
            });
            
            

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }


        });
    </script>
</body>
</html>