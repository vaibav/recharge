<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar_apipartner', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                <div class="row" style="margin-bottom: 5px;">
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s4 m4 l2 xl2  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                News :
                            </div>
                            <div class="col s8 m8 l10 xl10 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $stx = "";
                                    foreach($offer as $r)
                                    {
                                        $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $stx; ?> </marquee>
                            </div>
                    </div>
                    <div class="col s12 m12 l6 xl6">
                            <div class="col s9 m9 l4 xl4  amber-text text-darken-1" style="font-size: 22px; padding: 10px; ">
                                Inactive Networks :
                            </div>
                            <div class="col s3 m3 l8 xl8 " style="font-size: 22px; padding: 10px; color: white;">
                                <?php 
                                    $sty = "";
                                    //print_r($in_active);
                                    foreach($in_active as $r)
                                    {
                                        $sty = $sty . $r->net_name. " &nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $sty; ?> </marquee>
                            </div>
                    </div>
                </div>

            <div class="card " >
                
                <div class="card-image">
                    <a class="btn-floating halfway-fab waves-effect waves-light  #8e24aa purple darken-1 center-align pulse" id="tim" style="font-size:14px;width: 170px;height: 40px;border-radius: 2%;margin-right: 30px; ">5/26/2019, 10:39:39 PM </a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style="font-size:12px;padding:7px 8px;">NO</th>
                                <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                <th style="font-size:12px;padding:7px 8px;">RECH.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET.PER(%) / SURPLUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">API TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('user.bottom1')

  
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            var c = 1;
            $('#loader').hide();
            load_data();
            
            window.setInterval(function(){
                /// call your function here
                load_data();
            }, 25000);

           

           function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'apipartnerresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d.toLocaleString());
						if(c == 1)
						{
							$('#tim').css("background-color", "#EC7063");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("background-color", "#27AE60");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

           

            
      });
    </script>
    </body>
</html>
