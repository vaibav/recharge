 <!-- Navbar goes here -->
    <ul id="dropdown1" class="dropdown-content">
        <li><a href="#!">Bal : {{$user->ubal}}</a></li>
        <li><a href="{{url('changepassword')}}">Change Password</a></li>
        <li class="divider"></li>
        <li><a href="{{url('logout')}}">logout</a></li>
    </ul>
    <div class="navbar-fixed">
        <nav class="#1e88e5 white darken-1 shadow">
            <div class="row">
                <div class="col s4 m4 l3 xl3" style = "padding:0px 2px;">
                    <a href="#" data-target="slide-out" class="sidenav-trigger" style="display: block;margin: 0 10px;"><i class="material-icons purple-text">menu</i></a>
                    <img class="responsive-img" src="{{ asset('img/brand/logo.png') }}" style="height: 39px; width: 60px;padding-top: 18px;">
                </div>
                <div class="col s8 m8 l9 xl9 right-align" style = "padding:0px 2px;">
                    <ul class="right">
                    <li><a href="#" class = "title-con" style="padding: 0 6px;font-size:14px;">Hai {{$user->user}}!&nbsp;&nbsp;&nbsp;&#x20B9;{{$user->ubal}}</a></li>
                     <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger purple-text" href="#!" data-target="dropdown1" style="padding: 0 2px;padding-top: 2px;"><i class="material-icons">more_vert</i></a></li>
                    </ul>
                </div>
            </div>
            
        </nav>
    </div>
    
    <ul id="slide-out" class="sidenav #616161 grey darken-2 ">
        <li>
            <div class="user-view">
            <div class="background">
                <img src="{{ asset('img/brand/back1.jpg') }}">
            </div>
            <a href="#user"><img class="circle" src="{{ asset('uploadphoto/user.jpg') }}"></a>
            <a href="#name"><span class="white-text name">VaibavOnline</span></a>
            <a href="{{url('dashboard_retailer')}}"><span class="white-text name" style = "margin-top:4px;">{{$user->user}}</span></a>
            </div>
        </li>
        <li><a href="#!" class="dropdown-trigger" data-target="dropdown2"><i class="material-icons">cloud</i> User</a>
            <ul id="dropdown2" class="dropdown-content #fafafa grey lighten-5">
                <li><a href="{{url('user_one')}}">User Details</a></li>
                <li><a href="{{url('surplus_retailer')}}">Network Surplus Details</a></li>
            </ul>
        </li>
        <li><div class="divider #616161 grey darken-2"></div></li>
        <li><a href="#!" class="dropdown-trigger" data-target="dropdown3"><i class="material-icons">cloud</i> Payment Details</a>
            <ul id="dropdown3" class="dropdown-content #fafafa grey lighten-5">
                <li><a href="{{url('paymentrequest_retailer')}}">Payment Request</a></li>
                <li><a href="{{url('stockdetails_retailer')}}">Stock Details</a></li>
            </ul>
        </li>
        <li><div class="divider #616161 grey darken-2"></div></li>
        <li><a href="#!" class="dropdown-trigger" data-target="dropdown4"><i class="material-icons">cloud</i> Money Transfer</a>
            <ul id="dropdown4" class="dropdown-content #fafafa grey lighten-5">
                <li><a href="{{url('money_user_check')}}">Go</a></li>
            </ul>
        </li>
        <li><div class="divider #616161 grey darken-2"></div></li>
        <li><a href="#!" class="dropdown-trigger" data-target="dropdown5"><i class="material-icons">cloud</i> Recharge</a>
            <ul id="dropdown5" class="dropdown-content #fafafa grey lighten-5">
                <li><a href="{{url('rechargedetails_retailer')}}">Recharge Details</a></li>
                <li><a href="{{url('recharge_bill_retailer')}}">EBBill Details</a></li>
            
            </ul>
        </li>
        <li><div class="divider #616161 grey darken-2"></div></li>
        <li><a href="#!" class="dropdown-trigger" data-target="dropdown6"><i class="material-icons">cloud</i>Complaint</a>
            <ul id="dropdown6" class="dropdown-content #fafafa grey lighten-5">
                <li><a href="{{url('complaint_retailer')}}">Complaint Entry</a></li>
                <li><a href="{{url('complaint_view_retailer')}}">Complaint Details</a></li>
            </ul>
        </li>
    </ul>