<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar_distributor', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_distributor')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('user_rechargedetails_distributor')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <div class = "row">
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">call</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title">&#x20B9;<?php echo number_format($total['re_tot'], 2, ".", ""); ?></span>
                            <p class = "amber-text">Recharge Total</p>
                          </div>
                          
                        </div>
                    </div>
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light amber"><i class="material-icons">call_end</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title">&#x20B9;<?php echo number_format($total['ce_tot'], 2, ".", ""); ?></span>
                            <p class = "amber-text">Credit Total</p>
                          </div>
                          
                        </div>
                    </div>
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light green"><i class="material-icons">check</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title">&#x20B9;<?php echo number_format($total['su_tot'], 2, ".", ""); ?></span>
                            <p class = "amber-text">Success Total</p>
                          </div>
                          
                        </div>
                    </div>
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light red"><i class="material-icons">close</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title">&#x20B9;<?php echo number_format($total['fa_tot'], 2, ".", ""); ?></span>
                            <p class = "amber-text">Failure Total</p>
                          </div>
                          
                        </div>
                    </div>
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light violet"><i class="material-icons">show_chart</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title"><?php echo (floatval($total['no_su'] + floatval($total['no_fa'])));  ?></span>
                            <p class = "amber-text">Total Transactions</p>
                          </div>
                          
                        </div>
                    </div>
                    <div class="col s12 m12 l2 xl2">
                        <div class="card blue-grey darken-1">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light green"><i class="material-icons">show_chart</i></a>
                          </div>
                          <div class="card-content white-text">
                            <span class="card-title"><?php echo $total['no_su'];  ?></span>
                            <p class = "amber-text">Success Transactions</p>
                          </div>
                          
                        </div>
                    </div>
                </div>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET(%)/SURP</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TYPE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                                      
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                      
                                       foreach($recharge as $f)
                                       {
                                          
                                                                                       
                                           $net_name = "";
                                           foreach($network as $r)
                                           {
                                               if($f->net_code == $r->net_code)
                                                   $net_name = $r->net_name;
                                           }

                                           $reply_id = "";
                                           $reply_date = "";
                                           $rech_status = "";
                                           $o_bal = 0;
                                           $z = 0;

                                            if(sizeof($f->newparentrecharge2) > 0)
                                            {
                                                $reply_id = $f->newparentrecharge2->reply_opr_id;
                                                $reply_date =$f->newparentrecharge2->reply_date;
                                            }

                                            $rech_status = $f->rech_status;
                                            $rech_option = $f->rech_option;

                                           if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                $status = "<button class='btn-floating btn-sm waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                            {
                                                $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                            {      
                                                $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                                                $z = 1;
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn-floating btn-sm waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
               
                                           $o_bal = number_format($o_bal, 2, '.', '');
                                           $c_bal = floatval($f->user_balance);
                                           $c_bal = number_format($c_bal, 2, '.', '');

                                           
                                           if($z == 0)
                                           {
                                               echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_net_per."<i class='icon-control-play ' style='color:blue;'></i> ".$f->rech_net_per_amt."";

                                               echo "<i class='icon-control-play ' style='color:blue;'></i>".$f->rech_net_surp."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";

                                               echo "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 130px;word-break: break-word;'>".$reply_id."</div></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_type."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                               
                                           }
                                           else
                                           {
                                               echo "<tr style='background-color:#E8DAEF;'><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";

                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";

                                               
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                           }

                                          
                                           
                                            echo "</tr>";
                                                                                   
                                           $j++;
                                       }
                                       

                                      
                                    ?>

                            </tbody>
                        </table>
                        
                        {{ $recharge->links('vendor.pagination.materializecss') }}


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
                <h4>Info</h4>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    @include('user.bottom1')

   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

            
           
      });
    </script>
    </body>
</html>
