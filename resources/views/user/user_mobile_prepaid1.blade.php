<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VaibavOnline</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">

    <style type="text/css">
        .title_caption
        {
            padding: 20px 3px;
            font-size: 26px;
            font-family: 'Muli', sans-serif;
            font-weight: bold;
            
        }

        .title_subcaption
        {
            padding: 20px 10px;
            font-size: 20px;
            font-weight: 300;
            font-family: 'Muli', sans-serif;
            
        }

        .p_title
        {
            font-size: 16px;
            font-weight: 500;
            font-family: 'Muli', sans-serif;
        }

        .xfont
        {
            font-family: 'Muli', sans-serif;
            color:#4A148C;
        }

        .xsubfont
        {
            font-family: 'Muli', sans-serif;
            color:#ad1457;
        }

        .xxfont
        {
            font-family: 'Muli', sans-serif;
            
        }

       /* label focus color */
        .input-field input:focus + label 
        {
            color: #4a148c !important;
        }

        /* label underline focus color */
        .row .input-field input:focus 
        {
            border-bottom: 1px solid #4a148c !important;
            box-shadow: 0 1px 0 0 #4a148c !important
        }

        .input-field input + label 
        {
            color:#AEB6BF !important;
            
        }

        .input-field input 
        {
            border-bottom: 1px solid #AEB6BF !important;
            box-shadow: 0 1px 0 0 #fff !important
        }


       [type="radio"]:checked+span:after {
            background-color: #4a148c !important;
            border-color: #4a148c;
        }

        .card-1
        {
            position: relative; display: -webkit-box; display: -ms-flexbox; display: flex; -webkit-box-orient: vertical;
            -webkit-box-direction: normal;  -ms-flex-direction: column;  flex-direction: column;  min-width: 0;margin:10px 5px;
            word-wrap: break-word; background-color: #fff; background-clip: border-box; border: 1px solid #e3e6f0; border-radius: .35rem;
        }

        .shadow { box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); }

        

        


        
    </style>
   
</head>
<body style = "overflow-x:hidden;">

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(0,0,0,0.5); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-red-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

    <div class="row" >
        <div class="col s12 m12" style="padding:0px 0px;">
          <div class="card" style="margin : 0px 0px;">
            <div class="card-image #01579b light-blue darken-4" style="height: 140px;">
                <div class="row">
                    <div class="col s9 m9">
                            <label class="title_subcaption white-text"><?php echo $user_name ?></label><br>
                            <label class="title_subcaption white-text">&#x20B9;<?php echo $user_bal ?></label>
                    </div>
                    <div class="col s3 m3 " style="padding: 2px 16px;">
                            <img src = '{{ asset("img/brand/mob_logo.png")}}' style="height:48px; width: 48px;float: right;"/>
                    </div>
                </div>
                

                
              
            </div>
            <div class="card-content #fafafa white lighten-2">
              
                <!-- Small card-->
                <div class="row" style="margin-top: -85px;">
                    <div class="col s12 m12">
                      <div class="card-1 #fcfcfc #fcfcfc darken-1 shadow" style = "border-radius: 5px;">
                        <div class="card-content white-text" >
                          <span class="title_caption orange-text" ><b>Prepaid</b></span><br>
                          <span class="xsubfont blue-text" style="padding-left: 5px;"><b>Prepaid Mobile Recharge</b></span>
                        </div>
                        
                      </div>
                    </div>
                  </div>

                <!-- End-->

                <form id="rech_form" class="form-horizontal" action="{{url('recharge_mobile_store')}}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="auth_token" value="{{ $auth_token }}">

                <!-- Small card - 2 -->
                <div class="row" style="margin-top: -80px;">
                    <div class="col s12 m12">
                      <div class="card-1 #fcfcfc #fcfcfc darken-1 shadow" style = "border-radius: 5px;">
                        <div class="card-content white-text" style="padding: 15px 18px;">
                         
                            <p class="p_title xfont blue-text"><b>Select Network</b></p>

                            <div style="height:10px"></div>

                            <div class="row" style="margin-bottom: 5px;">

                            <?php
                               
                                foreach($network as $f)
                                {
                                    
                                   
                                 
                            ?>
                            
                            
                                <div class="col s6 m6">
                                    <label>
                                        <input name="net_code" id = "<?php echo $f->net_code; ?>" type="radio"  value = "<?php echo $f->net_code; ?>"/>
                                        <span class="xxfont"><?php echo $f->net_name; ?></span>
                                    </label>
                                </div>
                            
                            <?php
                                    
                                }
                            ?>

                            </div>


                           

                          
                        </div>
                        
                      </div>
                    </div>
                </div>

                <!-- End-->


                <!-- Small card - 3 -->
                <div class="row" style="margin-top: -80px;">
                    <div class="col s12 m12">
                        <div class="card-1 #fcfcfc #fcfcfc darken-1 shadow" style = "border-radius: 5px; ">
                            <div class="card-content white-text" style="padding: 15px 18px;">
                                
                                <div class="row" style="margin-bottom: 6px;">
                                    <div class="input-field col s9 m9" style="margin: 2px 2px;padding: 2px 2px;">
                                        <input id="id_user_mobile" name = "user_mobile" type="text" class="validate" style = "font-size:20px;">
                                        <label for="id_user_mobile">Mobile No</label>
                                    </div>

                                    <div class="input-field col s2 m2 center-align" style="margin: 2px 2px;padding: 2px 2px;">
                                            <button class="btn-floating btn waves-effect waves-light #0277bd light-blue darken-3 white-text" id="plan_121">121
                                            </button>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 5px;">
                                    <div class="input-field col s12 m12" style="margin: 2px 2px;padding: 2px 2px;">
                                        <input id="id_user_amount" name = "user_amount" type="text" class="validate" style = "font-size:20px;">
                                        <label for="id_user_amount">Amount</label>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 5px;">
                                        <div class="col s12 m12 right-align" style="margin: 2px 2px;">
                                                <button class="btn waves-effect waves-light#0277bd light-blue darken-5 white-text" type="submit" id="btn_submit" name="btn_submit">Submit
                                                </button>
                                        </div>
                                 </div>

                            </div>
                        </div>
                    </div>
                </div>

                </form>
    
                <!-- End-->

                <!-- Small card - 3 -->
                <?php echo $recharge; ?>
        
                    <!-- End-->

            </div>
          </div>
        </div>
      </div>

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content" style = "padding:5px 5px;">
                <h5>Info</h5>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    
    <script src='https:////cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

      <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.confirm({
                        title: 'Congratulations!',
                        content: '".$res."',
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function(){
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                    
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 30)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $.alert('".$res."');
                });
                </script>";
            }
            else if($op == 31)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('.modal').modal();
                    $('.modal').modal('open');

                    $('#id_ebb').val('2');
                   
                    //$('#id_net_type_code').val(".$net_type.");
                    //$('#id_net_code').val(".$net_code.");
                    $('#".$net_code."').prop('checked', true);
                    $('#id_user_mobile').val(".$mob.");
                    //$('#id_user_mobile').addClass('active');
                    $('#id_mob').text(".$mob.");

                    $('input[name=group1]').change(function (e) {
                        var amt = $(this).val();
                        $('#id_user_amount').val(amt);
                        //$('#id_user_amount').addClass('active');
                        //$('#id_amt').text(amt);
                        M.updateTextFields();
                    });

                    M.updateTextFields();

                });
                </script>";
            }
            else if($op == 21)
            {
                
            }
            session()->forget('result');
        }
    ?>

    <script type="text/javascript">
        $(document).ready(function() 
	    {
            //M.toast({html: 'I am a toast!', classes: 'rounded'});

            $('#loader').hide();
            //$('#loader').show();

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);

                var nc = $('input[name=net_code]:checked').val();;
                var um = $('#id_user_mobile').val();
                var ua = $('#id_user_amount').val();
                
                //alert(ax);
                if(nc != "" && um != "" && ua != "")
                {

                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Are You Sure? <br> MOB : ' +um+ '<br> AMT : ' +ua,
                        type: 'purple',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                                $.alert('Canceled!');
                                $('#btn_submit').prop('disabled', false);
                            },
                            tryAgain: {
                                text: 'OK',
                                btnClass: 'btn-purple',
                                action: function(){
                                    $('#rech_form').attr('action', "{{url('recharge_mobile_store')}}");
                                    $('#rech_form').submit();
                                    $('#loader').show();
                                }
                            }
                            
                        }
                    });
                    
                }
                else if(nc == "")
                {
                    $.alert('Please Select Network!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(um == "")
                {
                    $.alert('Please Enter Mobile No!');
                    $('#btn_submit').prop('disabled', false);
                }
                else if(ua == "")
                {
                    $.alert('Please Enter Amount!');
                    $('#btn_submit').prop('disabled', false);
                }
                
                
                
            }); 

            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
               
                var net_code = $('input[name=net_code]:checked').val();
                var usr_mobe = $('#id_user_mobile').val();

               

                if(net_code == null || net_code =="")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Select Network!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else if(usr_mobe == "")
                {
                    $.confirm({
                        title: 'VaibavOnline!',
                        content: 'Please Enter Mobile No!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: function () {
                            }
                        }
                    });
                }
                else
                {
                    $('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                    $('#rech_form').submit();
                }
                    
                /*if(net_code != "" && usr_mobe != "")
                {
                    $('#plan_121').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                    $('#rech_form').submit();
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Mobile No...", "error");
                }*/
               
                    
                
                
            });


        });
    </script>
</body>
</html>