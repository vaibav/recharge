<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
       
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide">
    @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => '', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card">
                  
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                          <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                <h3>
                                <?php
                                          if(session()->has('result'))
                                          {
                                            $op = session('result');
                                            echo $op['output'];
                                          }
                                        ?>
                                </h3>
                          </div>
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	 {
          
      });
    </script>
    </body>
</html>