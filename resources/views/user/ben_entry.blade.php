<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')

   
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

    @include('user.sidebar_x_retailer')

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('bank_remitter_direct_check')}}">
            <i class="large material-icons">arrow_back</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
        
        <br>
    
        <div class="row " >
            <div class="col s12 m12 l12 xl12 " >
                <div class = "card1 shadow m-36">
                    <form id = "rech_form" class="user" action="{{url('ben_entry_store')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" id="csrftk" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id = "ben_id" name="ben_id" value="">
                    <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['msisdn']; ?>">
                    <input type="hidden" id = "agent_id" name="agent_id" value="<?php echo $agent_id; ?>">
                    <input type="hidden" id = "id_user_code" name="user_code"  value="{{ $user->code }}">
                    <input type="hidden" id = "trans_id" name="trans_id" value="">
                    <input type="hidden" name="ben_id" id="ben_id" value="">
                    <input type="hidden" id = "otp" name="otp" value="">
                    <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">
                    <br>

                    <div class="row" style="margin:2px 2px;">
                        <div class="col s6 m6 l6 xl6" style="margin:2px 10px;padding:1px 1px;">
                            <label class="title-con" style="margin-top:20px;">Beneficiary Entry</label>
                        </div>
                        <div class="col s5 m5 l5 xl5 right-align" style="margin:2px 2px;padding:1px 1px;">
                            <button class="btn  btn-small waves-effect waves-light #00897b teal darken-1" id="btn_view">view</button>
                        </div>
                    </div>
                    

                    
                    <div class="row" style="margin-top:20px;margin:5px 2px;">
                        <div class="input-field col s12 m12 l6 xl6" >
                            <select id="bank_code" name = "bank_code" style = "padding:6px 6px;">
                                    <option value="">Select Bank</option>
                                    <option value="ANDB">Andhra Bank</option>
                                    <option value="UTIB">Axis Bank</option>
                                    <option value="BARB">Bank of Baroda</option>
                                    <option value="BKID">Bank of India</option>
                                    <option value="CNRB">Canara Bank</option>
                                    <option value="CSBK">Catholic Syrian Bank</option>
                                    <option value="CBIN">Central Bank of India</option>
                                    <option value="CITI">Citi Bank</option>
                                    <option value="CCBN">Citizen Co-Op Bank</option>
                                    <option value="CIUB">City Union Bank</option>
                                    <option value="CORP">Corporation Bank</option>
                                    <option value="DLXB">Dhanlaxmi Bank</option>

                                    <option value="FDRL">Federal Bank</option>
                                    <option value="FINO">FINO Payment Bank</option>
                                    <option value="HDFC">HDFC Bank</option>
                                    <option value="HSBC">HSBC Bank</option>

                                    <option value="ICIC">ICICI Bank</option>
                                    <option value="IBKL">IDBI Bank</option>
                                    <option value="IDFB">IDFC Bank</option>
                                    <option value="IDIB">Indian Bank</option>
                                    <option value="IOBA">Indian Overseas Bank</option>
                                    <option value="INDB">IndusInd Bank</option>
    
                                    <option value="KVBL">Karur Vysya Bank</option>
                                    <option value="KKBK">Kotak Mahindra Bank</option>
                                    <option value="KKBN">Lakshmi Vilas Bank</option>
                                    <option value="BKID">National Co-Operative Bank</option>
                                    <option value="ORBC">Oriental Bank of Commerce</option>
                                    <option value="PYTM">PAYTM PAYMENT BANK</option>
                                    <option value="PUNB">Punjab National Bank</option>

                                    <option value="SIBL">South Indian Bank</option>
                                    <option value="SCBL">Standard Chartered Bank</option>
                                    <option value="SBIN">State Bank of India</option>
                                    <option value="SYNB">Syndicate Bank</option>
                                    <option value="TMBL">Tamilnad Mercantile Bank</option>
                                    <option value="TNSC">The Tamilnadu State Apex Cooperative Bank</option>

                                    <option value="UCBA">UCO Bank</option>
                                    <option value="UBIN">Union Bank of India</option>
                                    <option value="UTBI">United Bank of India</option>
                                    <option value="VIJB">Vijaya Bank</option>
                                    <option value="YESB">Yes Bank</option>
                                </select>  
                                    
                        </div>
        
                        <div class="input-field col s12 m12 l4 xl4" >
                            <input placeholder="Account No" id="ben_acc_no" name = "ben_acc_no" type="text" class="validate">
                            <span class="text-danger">{{ $errors->first('ben_acc_no') }}</span>
                        </div>
                        <div class="input-field col s12 m12 l2 xl2" >
                            <button class="btn waves-effect waves-light orange"  id="btn_verify">Verify
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>

                     <div class="row" style="margin:5px 2px;">
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Beneficiary Name" id="ben_acc_name"  name = "ben_acc_name" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('ben_acc_name') }}</span>    
                            </div>
            
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="IFSC Code (Dont Enter if select Bank)" id="bank_ifsc"  name = "bank_ifsc" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('bank_ifsc') }}</span>   
                            </div>
                    </div>
        
                    <div class="row" style="margin:5px 2px;">
                            <div class="input-field col s12 m12 l6 xl6" >
                                <select id="agent_state_code" name = "agent_state_code" style = "padding:6px 6px;">
                                    <option value="">Select State</option>
                                        <option value="6">Tamil Nadu</option>
                                        <option value="1">New Delhi</option>
                                        <option value="5">Kerala</option>
                                </select>  
                                        
                            </div>
            
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Pincode" id="agent_pincode"  name = "agent_pincode" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('agent_pincode') }}</span>
                            </div>
                    </div>
        
                    <div class="row" style="margin:5px 2px;">
                        <div class="input-field col s12 m12 l12 xl12 right-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit">submit</button>
                        </div>
                    </div>
            
                    </form>
                </div>
               
            </div>
    
    
            
        </div>
    </div>
    

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP</h4>
            <p id = "tr_id"></p>
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="OTP" id="cus_otp" name="cus_otp" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_otp">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
   @include('user.bottom_x')

    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        $trans_id = $op1['trans_id'];
        $ben_acc_no = $op1['ben_acc_no'];
        $ben_id = $op1['ben_id'];
        //$ben_acc_no = $op1['ben_acc_no'];
        if($op == 0)
        {
            echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#ben_id').val('".$ben_id."');
                    $('#tr_id').text('".$trans_id."');
                   

                    $('.modal').modal();
                    $('.modal').modal('open');
                });
                </script>";
            
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success');
                window.location.href = '".url('/'). "/bank_remitter_direct_check';
               
            });
            </script>";
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error');
               
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var x = 0;

            $("#ben_acc_no").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_view').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('ben_view')}}", "_self");
            });

            $('#btn_verify').on('click',function(e)
            {
                e.preventDefault();

                var acc_no = $("#ben_acc_no").val();
                var b_ifsc = $("#bank_ifsc").val();
                var b_code = $('option:selected', "#bank_code").val();
                var age_id = $("#agent_id").val();
                var msisdn = $("#c_msisdn").val();
                var csrftk = $("#csrftk").val();

                
                if(acc_no == "")
                {
                    swal("Cancelled", "Error! Account No is Empty....", "error");
                }
                else if(b_code == "-")
                {
                    swal("Cancelled", "Error! Please Select Bank....", "error");
                }
                else if(msisdn == "-")
                {
                    swal("Cancelled", "Error! Please Select Customer....", "error");
                }
                else
                {
                    //alert(acc_no + "--" + b_code + "---" + age_id + "---"+ msisdn);
                    $('#btn_verify').prop('disabled', true);
                    swal({
                            title: 'Are you sure? (Rs.3 deducted from your Account)',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Verify!'
                            }).then((result) => {
                            if (result.value) {
                                $('#loader').show();
                                $.ajax({
                                    url: '<?php echo url('/'); ?>/check_ben_account',
                                    type: 'POST',
                                    data: {_token: csrftk, ben_acc_no:acc_no, bank_code:b_code, agent_id:age_id, agent_msisdn:msisdn, bank_ifsc:b_ifsc },
                                    dataType: 'JSON',
                                    success: function (data) { 
                                        if(data.status == 1)
                                        {
                                            swal('Alert!', data.message, 'success');
                                            $("#ben_acc_name").val(data.ben_name);
                                            M.updateTextFields();
                                        }
                                        else if(data.status == 2)
                                        {
                                            swal("Cancelled", data.message, "error");
                                        }
                                        $('#loader').hide();
                                        $('#btn_verify').prop('disabled', false);
                                    },
                                    error: function(data, mXg) { 
                                        $('#loader').hide();
                                        console.log(data);
                                    }
                                }); 
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                $('#btn_verify').prop('disabled', false);
                            }
                        });
                   
                }

                

            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var bank = $('option:selected', '#bank_code').val();
                var ifsc = $('#bank_ifsc').val();
                var z = checkBankIfsc(bank, ifsc);

                if(z == 1)
                {
                    $('#btn_submit').prop('disabled', true);
                    swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Add!'
                            }).then((result) => {
                            if (result.value) {
                                $('#loader').show();
                                $('#rech_form').attr('action', "{{url('ben_entry_store')}}");
                                $('#rech_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                $('#btn_submit').prop('disabled', false);
                            }
                        });
                }
                else if(z == 2)
                {
                    swal("Cancelled", "You Must Enter Any one... Either Bank or IFSC code", "error");
                }
                else
                {
                    swal("Cancelled", "Dont Enter Both Bank and IFSC code! \n Select Bank Or Enter Ifsc Code", "error");
                }
                   
            }); 

             $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                var otp = $('#cus_otp').val();
                var bid = $('#ben_id').val();
                var tid = $('#trans_id').val();
                var msi = $('#c_msisdn').val();
                $('#btn_otp').prop('disabled', true);
                
                //alert(otp + "---" + bid + "----" + tid + "----" + msi);
                if(otp == "") {
                    swal("Cancelled", "Please Enter OTP", "error");
                    $('#btn_otp').prop('disabled', false);
                }
                else if(otp != "" && bid != "" && tid != "") 
                {
                    $('#loader').show();
                    window.location.href = "<?php echo url('/'); ?>/ben_otp_entry_store/" + bid + "/" + msi + "/" + tid + "/" + otp;
                    
                }
                   
            }); 

            function checkBankIfsc(bank, ifsc)
            {
                var z = 0;

                if(bank == "" && ifsc != "")
                {
                    z = 1;
                }
                else if(bank != "" && ifsc == "")
                {
                    z = 1;
                }
                else if(bank == "" && ifsc == "")
                {
                    z = 2;
                }

                return z;
            }

            
            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
        
        });
    </script>
</body>
</html>