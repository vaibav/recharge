<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')

   
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

   @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('bank_remitter_direct_check')}}">
            <i class="large material-icons">arrow_back</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
        
        <br>
    
        <div class="row " >
            <div class="col s12 m12 l12 xl12 " >
                <div class = "card1 shadow m-36">
                    <br>

                    <div class="row" style="margin:2px 2px;">
                        <div class="col s6 m6 l6 xl6" style="margin:2px 10px;padding:1px 1px;">
                            <label class="title-con" style="margin-top:20px;">Beneficiary Details</label>
                        </div>
                        <div class="col s5 m5 l5 xl5 right-align" style="margin:2px 2px;padding:1px 1px;">
                            <button class="btn  btn-small waves-effect waves-light #00897b teal darken-1" id="btn_back">Back</button>
                        </div>
                    </div>
                    

                    
                    <div class="row" style="margin-top:20px;margin:5px 2px;">
                        <div class="col s11 m11 l11 xl11" style="margin:2px 10px;padding:1px 1px;">
                            
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Beneficiary Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Remitter</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Account No</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Acc Hol Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Bank Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>IFSC Code</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Trans ID</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Reply ID</th>
                                    <th style='font-size:12px;padding:7px 8px;'>RESEND OTP</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        
                                        $j = 1;
                                        foreach($data as $f)
                                        {
                                            $bank = "";
                                            if($f->bank_code == "UTIB") {
                                                $bank = "AXIS BANK";
                                            }
                                            else if($f->bank_code == "BARB") {
                                                $bank = "BANK OF BARODA";
                                            }
                                            else if($f->bank_code == "CNRB") {
                                                $bank = "CANARA BANK";
                                            }
                                            else if($f->bank_code == "CORP") {
                                                $bank = "CORPORATION BANK";
                                            }
                                            else if($f->bank_code == "CIUB") {
                                                $bank = "CITY UNION BANK";
                                            }
                                            else if($f->bank_code == "HDFC") {
                                                $bank = "HDFC BANK";
                                            }
                                            else if($f->bank_code == "IBKL") {
                                                $bank = "IDBI BANK LTD";
                                            }
                                            else if($f->bank_code == "IDIB") {
                                                $bank = "INDIAN BANK";
                                            }
                                            else if($f->bank_code == "IOBA") {
                                                $bank = "INDIAN OVERSEAS BANK";
                                            }
                                            else if($f->bank_code == "ICIC") {
                                                $bank = "ICICI BANK";
                                            }
                                            else if($f->bank_code == "KVBL") {
                                                $bank = "KARUR VYSYA BANK";
                                            }
                                            else if($f->bank_code == "SBIN") {
                                                $bank = "STATE BANK OF INDIA";
                                            }
                                            else if($f->bank_code == "SYNB") {
                                                $bank = "SYNDICATE BANK";
                                            }
                                            else if($f->bank_code == "TMBL") {
                                                $bank = "TAMILNADU MERCANTILE BANK";
                                            }
                                            else if($f->bank_code == "VIJB") {
                                                $bank = "VIJAYA BANK";
                                            }
                                            else {
                                                $bank = $f->bank_code;
                                            }

                                            $rem_name = "";
                                            $rem_mobile = "";
                                            foreach($remitter as $r)
                                            {
                                                if($f->msisdn == $r->msisdn)
                                                {
                                                    $rem_name = $r->user_rem_name;
                                                    $rem_mobile = $r->msisdn;
                                                }
                                            }

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$rem_name."-".$rem_mobile."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_acc_no."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_acc_name."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$bank."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->bank_ifsc."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_status."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_rep_opr_id."</td>";

                                            if($f->ben_status == "OTP SUCCESS")
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;text-align:center'>";
                                                echo "<button class='btn-floating btn-sm blue' id='resend_".$f->ben_id."_".$rem_mobile."_".$f->trans_id."'>
                                                <i class='small material-icons'>arrow_forward</i></td>";
                                            }
                                            else
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                            }

                                            echo "</tr>";
                                          
                                            $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>


                        </div>
                    </div>

                     
                    
                   
                    
                </div>
               
            </div>
    
    
            
        </div>
    </div>
    

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP</h4>
            <p id = "tr_id"></p>
            <input type = "hidden" id = "b_id" value = "">
            <input type = "hidden" id = "b_td" value = "">
            <input type = "hidden" id = "b_md" value = "">
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="OTP" id="cus_otp" name="cus_otp" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_otp">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
   @include('user.bottom_x')

    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        
       
        //$ben_acc_no = $op1['ben_acc_no'];
        if($op == 20)
        {
            $trans_id_r = $op1['trans_id_r'];
            $msi = $op1['msisdn'];
            $ben_id = $op1['ben_id'];

            echo "<script>
                $(document).ready(function() 
                {
                    $('#b_id').val('".$ben_id."');
                    $('#b_md').val('".$msi."');
                    $('#b_td').val('".$trans_id_r."');
                    $('#tr_id').text('".$trans_id_r."');
                   

                    $('.modal').modal();
                    $('.modal').modal('open');
                });
                </script>";
            
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success');
                window.location.href = '".url('/'). "/bank_remitter_direct_check';
               
            });
            </script>";
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error');
               
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            

            $('#btn_back').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('ben_entry')}}", "_self");
            });

           $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid = gid.split("resend_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure to View?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Resend OTP!'
                        }).then((result) => {
                        if (result.value) {

                            var rid = nid[1].split("_");
                            var b_id = rid[0];
                            var b_ms = rid[1];
                            var b_tr = rid[2];

                            if(b_id != "" && b_ms !="" && b_tr != "")
                            {
                                window.location.href = "<?php echo url('/'); ?>/ben_otp_resend/" + b_id + "/" + b_ms + "/" + b_tr;
                            }

                            //alert(b_id + "---" + b_ms + "---" + b_tr);
                            //alert(nid[1]);
                            //window.location.href = "<?php echo url('/'); ?>/ben_otp_entry/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });

                   
                }
                
            });

            $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('ben_entry')}}", "_self");

                $('#btn_otp').prop('disabled', true);
                var otp = $('#cus_otp').val();
                var tid = $('#b_td').val();
                var msisdn = $('#b_md').val();
                var benid = $('#b_id').val();
                
                alert(otp + "--" + tid + "--" + msisdn);
                if(otp == "") {
                    swal("Cancelled", "Please Enter OTP", "error");
                    $('#btn_otp').prop('disabled', false);
                }
                else if(otp != "" && tid != "" && msisdn != "") 
                {
                    window.location.href = "<?php echo url('/'); ?>/ben_otp_entry_store/" + benid + "/" + msisdn + "/" + tid + "/" + otp;
                }
            });

            
            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
        
        });
    </script>
</body>
</html>