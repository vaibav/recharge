<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        <?php 
            
            if($user->mode == "SUPER DISTRIBUTOR") 
            {
            ?>
                @include('user.sidebar_sdistributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_user')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            else if($user->mode == "DISTRIBUTOR" ) {
                ?>
                @include('user.sidebar_distributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_distributor')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            
        ?>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                <span class="card-title" style = "padding:12px;">Payment Accept</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                          <table class="bordered striped responsive-table">
                              <thead>
                                <tr>
                                <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Request Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Request Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Payment Mode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Grant Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Action</th>
                                </tr>
                              </thead>
                              <tbody id="tbl_body">
                              <?php 
                                        
                                        $j = 1;
                                        foreach($pay1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_req_date."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->payment_mode."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_remarks."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' id='Amt_".$f->trans_id."' /></td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>";
                                          echo "<button class='btn-floating btn-sm waves-effect waves-light green' id='Save_".$f->trans_id."'>
                                          <i class='small material-icons '>check</i></button>&nbsp;&nbsp;";

                                          echo "<button class='btn-floating btn-sm waves-effect waves-light red' id='Delete_".$f->trans_id."'>
                                          <i class='small material-icons '>clear</i></button>";
                                    
                                          echo "</tr>";
                                          $j++;
                                        }
                                        
                                    ?>
                                    
                                
                              </tbody>
                          </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('user.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op['output']."', 'success'); 
                });
                </script>";
            
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $("input[type=text]").keydown(function (e) 
            {
                numbersOnly(e);
            });

             $('#tbl_body').delegate('input', 'keyup', function(e) 
            {
                e.preventDefault();
                var bal = $("#id_user_bal").val();
                var amt = $("#" + this.id).val();
                //alert(bal+"---"+this.id);
                if(parseFloat(amt) > parseFloat(bal))
                {
                    swal("Cancelled", "Error! High value than Balance!", "error");
                    $("#" +this.id).val("");
                }
            });
            

            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("Save_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Transfer!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            var amt = $("#Amt_" + code).val();

                            if(amt != "") {
                                window.location.href = "<?php echo url('/'); ?>/paymentaccept_store_user/" + code + "/" + amt;
                            }
                            else {
                                swal("Cancelled", "Amount is Empty...", "error");
                            }
                            //alert(code + "---" + amt);
                            
                        }
                        else
                        {
                            swal("Cancelled", "No Transfer...", "error");
                
                        }
                    });
                    
                }
                var nid1=gid.split("Delete_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Cancel!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.location.href = "<?php echo url('/'); ?>/paymentaccept_delete_user/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "No Transfer...", "error");
                            
                        }
                    });
                    
                }
            });

             function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

      });
    </script>
    </body>
</html>
