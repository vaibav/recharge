<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')

        <!-- Background CSS  -->
        <link href="{{ asset('css/retailer.css') }}" rel="stylesheet">
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide">
        @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Dashboard / Recharge', 'uname' => $user->user, 'bal' => $user->ubal))
        <div id ="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url({{ asset('img/loader/3.gif') }}) 50% 50% no-repeat rgba(249,249,249, 0.5) ;display:none;" ></div>
        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
          <div class="row">
                <div class="col-lg-6" style="padding:5px 15px;">
                    <div class="content">
                        <div class="content__container">
                            <p class="content__container__text">
                            News
                            </p>
                            <?php 
                                $stx = "";
                                foreach($offer as $r)
                                {
                                    $stx = $stx . $r->offer_details. " <i class='fa fa-bell-o blinking' aria-hidden='true' class=''></i>";
                                }
                            ?>
                            <p class="content__container__list">
                            <marquee direction='left'><?php echo $stx; ?></marquee>
                            </p>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6" style="padding:5px 5px;">
                    <div class="content">
                        <div class="content__container">
                            <p class="content__container__text">
                            Inactive Networks
                            </p>
                            <?php 
                                $sty = "";
                                //print_r($in_active);
                                foreach($in_active as $r)
                                {
                                    $sty = $sty . $r->net_name. " <i class='fa fa-exclamation-triangle blinking' aria-hidden='true' class=''></i>";
                                }
                            ?>
                            <p class="content__container__list1">
                            <marquee direction='left'><?php echo $sty; ?></marquee>
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>


            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                    <div class="card-header">
                        Recharge
                         <span class="badge badge-pill badge-danger float-right" id="tim" style='font-size:14px;'></span>
                    </div>
                    <div class="card-body" style="padding:6px 10px;">
                      <form  id = "rech_form" class="form-horizontal" action="{{url('recharge_store')}}" method="post" accept-charset="UTF-8">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="hidden" name="user_code" id="id_user_code" value="{{ $user->code }}">
                      <div class = "row">
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="input-normal">Network Type</label>
                                <div class="col-sm-6">
                                  <select class="form-control" id="id_net_type_code" name="net_type_code" >
                                        <option value="-">---Select---</option>
                                      <?php
                                            foreach($networktype as $f)
                                            {
                                                echo "<option value='".$f->net_type_code."'>".$f->net_type_name."</option>";
                                            }
                                      ?>
                                      
                                  </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="input-normal">Network</label>
                                <div class="col-sm-6">
                                  <select class="form-control" id="id_net_code" name="net_code" >
                                      <?php
                                            foreach($network as $f)
                                            {
                                                echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                            }
                                      ?>
                                      
                                  </select>
                                </div>
                              </div>  
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div class = "row">
                                    <div class ="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <label style ="font-size:40px;font-weight:bold;color:purple;" id="id_mob"></label>
                                    </div>
                                    <div class ="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                        <label style ="font-size:60px;font-weight:bold;color:green;" id="id_amt"></label>
                                    </div>
                                </div>
                               
                          </div>
                      </div>
                      
                        <div class = "row" id = "rc1">
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="input-normal">Mobile No</label>
                                <div class="col-sm-5">
                                  <input class="form-control" id="id_user_mobile" type="text" name="user_mobile" style="font-size:14px;" placeholder="">
                                  
                                  <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
                                </div>
                                <div class="col-sm-3" style = "padding-left:2px; padding-right:5px;">
                                    <button class = "btn btn-brand btn-yahoo" id = "plan_121">121 Offers</button>
                                </div>
                              </div>  
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="input-normal">Recharge Amount</label>
                                <div class="col-sm-6">
                                  <input class="form-control" id="id_user_amount" type="text" name="user_amount" style="font-size:14px;" placeholder="">
                                  <span class="text-danger">{{ $errors->first('user_amount') }}</span>
                                </div>
                              </div>
                          </div>
                      </div>
                    
                      <!-- EB Recharge -->
                      <div class = "row" id = "eb1">
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="input-normal" style="color:#21618C">Consumer No</label>
                                <div class="col-sm-5">
                                    <input class="form-control" id="id_eb_cons_no" type="text" name="eb_cons_no" placeholder="">
                                    <span class="text-danger">{{ $errors->first('eb_cons_no') }}</span>
                                </div>
                                <div class="col-sm-3" style = "padding-left:2px; padding-right:5px;">
                                    <button class = "btn btn-brand btn-yahoo" id = "eb_info">EB Info</button>
                                </div>
                              </div>  
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12" >
                            <div class="form-group row" id = "eb_1">
                                <label class="col-sm-4 col-form-label" for="input-normal" style="color:#21618C">Consumer Name</label>
                                <div class="col-sm-5">
                                    <input class="form-control" id="id_eb_cons_name" type="text" name="eb_cons_name" placeholder="">
                                    <span class="text-danger">{{ $errors->first('eb_cons_name') }}</span>
                                </div>
                              </div>  
                          </div>

                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal" style="color:#21618C"> Amount</label>
                                    <div class="col-sm-5">
                                        <input class="form-control" id="id_eb_cons_amount" type="text" name="eb_cons_amount" placeholder="">
                                        <span class="text-danger">{{ $errors->first('eb_cons_amount') }}</span>
                                    </div>
                                </div>
                              
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group row" id = "eb_2">
                                    <label class="col-sm-4 col-form-label" for="input-normal" style="color:#21618C"> Due Date</label>
                                    <div class="col-sm-5">
                                    <input class="form-control" id="id_eb_cons_duedate" type="text" name="eb_cons_duedate" placeholder="">
                                    <span class="text-danger">{{ $errors->first('eb_cons_duedate') }}</span>
                                    </div>
                                </div>
                                 
                          </div>

                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal" style="color:#21618C">Mobile No</label>
                                    <div class="col-sm-5">
                                        <input class="form-control" id="id_eb_cons_mobile" type="text" name="eb_cons_mobile" placeholder="">
                                        <span class="text-danger">{{ $errors->first('eb_cons_mobile') }}</span>
                                    </div>
                              </div> 
                          </div>
                      </div>
                      <!-- End EB --> 

                       <!-- Money Transfer -->
                       <div class = "row" id = "bn1" >
                         
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#4A235A">Accnt No</label>
                                <div class="col-sm-6">
                                    <select class="form-control" id="id_bn_id" name="bn_id" >
                                            <option value="-">---Select---</option>
                                            <?php
                                                    foreach($ben as $f)
                                                    {
                                                        echo "<option value='".$f->benn_id."'>".$f->ben_acc_no."-".$f->ben_acc_name."</option>";
                                                    }
                                            ?>
                                        
                                    </select>
                                </div>
                              </div>  
                          </div>

               
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#4A235A"> Amount</label>
                                <div class="col-sm-6">
                                  <input class="form-control" id="id_mt_amount" type="text" name="mt_amount" placeholder="">
                                  <span class="text-danger">{{ $errors->first('mt_amount') }}</span>
                                </div>
                              </div>
                          </div>
                      </div>
                      <!-- End Money --> 
                    
                      <!-- Payment Gateway -->
                      <div class = "row" id = "pg1">
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#21618C">Name</label>
                                <div class="col-sm-6">
                                    <input class="form-control" id="id_pg_user_name" type="text" name="pg_user_name" placeholder="">
                                    <span class="text-danger">{{ $errors->first('pg_user_name') }}</span>
                                </div>
                              </div>  
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                            <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#21618C">Email</label>
                                <div class="col-sm-6">
                                    <input class="form-control" id="id_pg_user_email" type="text" name="pg_user_email" placeholder="">
                                    <span class="text-danger">{{ $errors->first('pg_user_email') }}</span>
                                </div>
                              </div>  
                          </div>

                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#21618C">Mobile No</label>
                                <div class="col-sm-6">
                                  <input class="form-control" id="id_pg_user_mobile" type="text" name="pg_user_mobile" placeholder="">
                                  <span class="text-danger">{{ $errors->first('pg_user_mobile') }}</span>
                                </div>
                              </div>  
                          </div>
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group row">
                                <label class="col-sm-5 col-form-label" for="input-normal" style="color:#21618C"> Amount</label>
                                <div class="col-sm-6">
                                  <input class="form-control" id="id_pg_user_amount" type="text" name="pg_user_amount" placeholder="">
                                  <span class="text-danger">{{ $errors->first('pg_user_amount') }}</span>
                                </div>
                              </div>
                          </div>
                      </div>
                      <!-- End Payment Gateway --> 
                    
                      <div class = "row">
                          <div class ="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                <div class="form-group row" style="padding:6px 15px;margin-bottom:5px;">
                                      <button class="btn btn-sm btn-primary" type="submit" id="btn_submit">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>

                                          <input type = "hidden" id = "id_ebb" value ='0'>
                                </div>
                          </div>
                      </div>
                      </form>
                        
                        <div class = "row">
                            <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <table class="table table-responsive-sm table-bordered">
                                  <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Recharge Amt</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET.PER(%) / SURPLUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR. TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;'>BILL</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body">
                                    <?php 
                                        
                                        
                                        //echo $recharge;

                                       
                                    ?>
                                    
                                  </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>

    <div class="modal fade" id="rech_offer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Info</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content-->
        </div>
        <!-- /.modal-dialog-->
    </div>
    <!-- /.modal-->
    
    @include('user.footer')
    @include('user.bottom')

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 30)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 31)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    
                    $('#rech_offer_body').html('".$res."');
                    $('#rech_offer').modal();
                });
                </script>";
            }
            else if($op == 21)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $con_no = $op1['mob'];
                $con_na = $op1['name'];
                $con_am = $op1['amount'];
                $con_du = $op1['due'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('#rech_offer_body').html('".$res."');
                    $('#rech_offer').modal();

                    $('#rc1').hide();
                    $('#eb1').show();
                    $('#bn1').hide();
                    $('#pg1').hide();

                    getNetworkData(".$net_type.");

                    var con_na = '".$con_na."';

                    $('#id_net_type_code').val(".$net_type.");
                    $('#id_net_code').val(".$net_code.");
                    $('#id_eb_cons_no').val(".$con_no.");
                    $('#id_eb_cons_amount').val(".$con_am.");
                    
                    if (con_na != 'NONE')
                    {
                        $('#eb_1').show();
                        $('#eb_2').show();
                        $('#id_eb_cons_name').val('".$con_na."');
                        $('#id_eb_cons_duedate').val('".$con_du."');
                    }
                    else
                    {
                        $('#eb_1').hide();
                        $('#eb_2').hide();
                    }
                    $('#id_ebb').val('1');

                    function getNetworkData(net_type)
                    {
                        
                        $.ajaxSetup({
                            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                        });
        
                        $.ajax({
                            url:'network_data/' + net_type,
                            type: 'GET',
                            dataType: 'json',
                            success: function( response ){
                                var zz=response['data'];
                                $('#id_net_code').empty();
                                for(var i=0;i<zz.length;i++)
                                {
                                   
                                    if(zz[i][0]!='')
                                        $('#id_net_code').append('<option value='+zz[i][0]+'>'+zz[i][1]+'</option>');
                                }
                                $('#loader').hide(); 
                            },
                            error: function (xhr, b, c) {
                                console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                            }
                        });
                    }

                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
    <script>
     $(document).ready(function() 
	   {
            var c = 1;
            $('#loader').hide();
            load_data();
            
            window.setInterval(function(){
                /// call your function here
                load_data();
			}, 19000);

            var ex = $('#id_ebb').val();

            if(ex == 0)
            {
                loadPrepaid();
                $('#eb_1').hide();
                $('#eb_2').hide();
                $('#rc1').show();
                $('#eb1').hide();
                $('#bn1').hide();
                $('#pg1').hide();
            }

            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'userresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d.toLocaleString());
						if(c == 1)
						{
							$('#tim').css("background-color", "#EC7063");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("background-color", "#27AE60");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function loadPrepaid()
            {
                var ty = $("#id_net_type_code option:contains('PREPAID')").attr('selected', true).val();
                getNetworkData(ty);
            }

            $("#id_net_type_code").change(function(e)
            {
                var ty = $('option:selected', this).val();
                var ax = $('option:selected', this).text();
                $('#loader').show();
                if(ty != "-")
                {
                    getNetworkData(ty);
                    if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
					{
                        $('#rc1').show();
                        $('#eb1').hide();
                        $('#bn1').hide();
                        $('#pg1').hide();

                        if(ax == "PREPAID")
                            $("#plan_121").html('121 Offers');
                        else if(ax == "DTH")
                            $("#plan_121").html('DTH Info');
                    }
                    else if(ax == "BILL" || ax == "BILL PAYMENT")
                    {
                        $('#rc1').hide();
                        $('#eb1').show();
                        $('#bn1').hide();
                        $('#pg1').hide();
                    }
                    else if(ax == "MONEY" || ax == "MONEY TRANSFER")
                    {
                        $('#rc1').hide();
                        $('#eb1').hide();
                        $('#bn1').show();
                        $('#pg1').hide();
                    }
                    else if(ax == "PAYMENT GATEWAY")
                    {
                        $('#rc1').hide();
                        $('#eb1').hide();
                        $('#bn1').hide();
                        $('#pg1').show();
                    }

                }
                    
            });

            function getNetworkData(net_type)
            {
                
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });

                $.ajax({
                    url:'network_data/' + net_type,
                    type: 'GET',
                    dataType: "json",
                    success: function( response ){
                        var zz=response['data'];
                        $('#id_net_code').empty();
                        for(var i=0;i<zz.length;i++)
                        {
                            //alert(zz[i][0]+"----"+zz[i][1]);
                            if(zz[i][0]!="")
                                $('#id_net_code').append('<option value='+zz[i][0]+'>'+zz[i][1]+'</option>');
                        }
                        $('#loader').hide(); 
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            $("#id_user_mobile, #id_eb_cons_mobile, #id_bn_acc_no, #id_bn_acc_mobile").keydown(function (e) 
            {
                numbersExactly(e);
            });
            $("#id_user_amount, #id_eb_cons_amount, #id_bn_acc_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $("#id_user_mobile").keyup(function (e) 
            {
                var mob = $("#id_user_mobile").val();
                $("#id_mob").text(mob);
            });

            $("#id_user_amount").keyup(function (e) 
            {
                var mob = $("#id_user_amount").val();
                $("#id_amt").text(mob);
            });

            $("#id_bk_amount").keyup(function (e) 
            {
                var mob = $("#id_bk_amount").val();
                $("#id_amt").text(mob);
            });

            $("#id_eb_cons_no").blur(function(e)
            {
                e.preventDefault();
                /*var con_no = $("#id_eb_cons_no").val();
                if(con_no != "")
                {
                    $('#loader').show();
                    $.ajaxSetup({
                        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                    });
                    $.ajax({
                        url:'geteb_data/' + con_no,
                        type: 'GET',
                        success: function( data ){

                            $('#loader').hide();                            
                            data = data.replace("{", "");
                            data = data.replace("}", "");
                            var data1 = data.split(",");
                            for(var i=0;i<data1.length;i++)
                            {
                                var data2 = data1[i].split(":")
                                
                                data2[0] = data2[0].replace(/"/g, '');
                                data2[1] = data2[1].replace(/"/g, '');
                                if(data2[0] == "Customer Name")
                                {
                                    $("#id_eb_cons_name").val(data2[1]);
                                }
                                else if(data2[0] == "Due Amount")
                                {
                                    $("#id_eb_cons_amount").val(data2[1]);
                                }
                                else if(data2[0] == "Due Date")
                                {
                                    $("#id_eb_cons_duedate").val(data2[1]);
                                }
                                
                            }
                            
                           
                        },
                        error: function (xhr, b, c) {
                            console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                        }
                    });
                }*/
                
            });


            $("#rech_offer").on('hidden.bs.modal', function(e){
                
                e.preventDefault();
                var amt = $('input[name=amt]:checked').val();
                $('#id_net_code').val('<?php echo $net_code; ?>');
                $('#id_net_type_code').val('<?php echo $net_type; ?>');
                $('#id_user_mobile').val('<?php echo $mob; ?>');
                $("#id_mob").text(<?php echo $mob; ?>);
                
                if(amt != "")
                {
                    var amt1 = amt.split(".");
                    if(amt1.length >= 2)
                    {
                        $('#id_user_amount').val(amt1[0]);
                        $("#id_amt").text(amt1[0]);
                    }
                    else
                    {
                        $('#id_user_amount').val(amt);
                        $("#id_amt").text(amt);
                    }
                }
                

            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#btn_submit').prop('disabled', true);
                var ax = $('option:selected', '#id_net_type_code').text();
                //alert(ax);
                if(ax == "PREPAID" || ax == "POSTPAID" || ax == "DTH")
                {
                    var form = $(this).parents('form');
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Recharge!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('recharge_store')}}");
							$('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No Recharge...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                    
                }
                else if(ax == "BILL" || ax == "BILL PAYMENT")
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Recharge!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('recharge_bill')}}");
							$('#rech_form').submit();
                            //swal("Cancelled", "Under Progress...", "error");
                            //$('#btn_submit').prop('disabled', false);
                        }
                        else
                        {
                            swal("Cancelled", "No Recharge...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                }
                else if(ax == "MONEY" || ax == "MONEY TRANSFER")
                {
                    var bn_id = $('option:selected', '#id_bn_id').val();
                    if(bn_id != "-")
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Money Transfer!'
                            }).then((result) => {
                            if (result.value) {
                               $('#loader').show();
                                $('#rech_form').attr('action', "{{url('recharge_money')}}");
                                $('#rech_form').submit();
                                //swal("Cancelled", "Under Progress...", "error");
                                //$('#btn_submit').prop('disabled', false);
                            }
                            else
                            {
                                swal("Cancelled", "No Transfer...", "error");
                                $('#btn_submit').prop('disabled', false);
                            }
                        });
                    }
                    else
                    {
                        swal("Error", "Please Select Account No...", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                    
                }
                else if(ax == "TEST" || ax == "PAYMENT GATEWAY")
                {
                    swal("Cancelled", "Under Progress...", "error");
                    $('#btn_submit').prop('disabled', false);
                }
                
            }); 
            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
                var ax = $('option:selected', '#id_net_type_code').text();
                if(ax == "PREPAID")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                    else if(usr_mobe == "")
                    {
                        swal("Cancelled", "Please Enter Mobile No...", "error");
                    }
                }
                else if(ax == "DTH")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('dth_offers')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                    else if(usr_mobe == "")
                    {
                        swal("Cancelled", "Please Enter Mobile No...", "error");
                    }
                }
               
            });

            $('#eb_info').on('click',function(e)
            {
                e.preventDefault();
                var net_code = $('#id_net_code').val();
                var usr_mobe = $('#id_eb_cons_no').val();
                
                if(net_code != "" && usr_mobe != "")
                {
                    $('#eb_info').prop('disabled', true);
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('eb_info')}}");
                    $('#rech_form').submit();
                }
                else if(net_code == "")
                {
                    swal("Cancelled", "Please Select Network...", "error");
                }
                else if(usr_mobe == "")
                {
                    swal("Cancelled", "Please Enter Consumber No...", "error");
                }
               
            });

            $('#tbl_body').delegate('button', 'click', function(e) {
                      e.preventDefault();
                      var gid=this.id;
                      var nid=gid.split("bill_");
                      if(nid.length>1)
                      {
                          swal({
                              title: 'Are you sure?',
                              text: "Confirmation Alert",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Yes, Get Bill!'
                              }).then((result) => {
                              if (result.value) {
                                  var code = nid[1];
                                  window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                              }
                              else
                              {
                                  swal("Cancelled", "Sorry...", "error");
                              }
                          });
                        
                      }

                      var nid1=gid.split("bill1_");
                      if(nid1.length>1)
                      {
                          swal({
                              title: 'Are you sure?',
                              text: "Confirmation Alert",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Yes, Get Bill!'
                              }).then((result) => {
                              if (result.value) {
                                  var code = nid1[1];
                                  window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                              }
                              else
                              {
                                  swal("Cancelled", "Sorry...", "error");
                              }
                          });
                        
                      }

                });
            
            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
               
     });
    </script>
    </body>
</html>
