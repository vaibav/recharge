<header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="{{ asset('img/brand/logo.png') }}" width="100" height="40" alt="VaibavOnline" >
        <img class="navbar-brand-minimized" src="{{ asset('img/brand/sygnet.svg') }}" width="30" height="30" alt="CoreUI Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show" id="side">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <ul class="nav navbar-nav ml-auto">
        
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <?php 
                    $path = asset('uploadphoto/'.$user->code.'_PHOTO.jpg');

                    if (@getimagesize($path)) 
                    {
                        $cx = 1;
                    }
                    else
                    {
                      $path = asset('uploadphoto/user.jpg');
                    }
                    
              ?>
            <img class="img-avatar" src="{{ $path }}" alt="VaibavOnline">
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
              <i class="fa fa-bell-o"></i> Balance
              <span class="badge badge-info">{{$bal}}</span>
            </a>
            
            <div class="dropdown-header text-center">
              <strong>Settings</strong>
            </div>

             <?php
                if($user->mode == "SUPER DISTRIBUTOR" || $user->mode == "DISTRIBUTOR")
                {
                    echo "<a class='dropdown-item' href='".url('changepassword_distributor')."'><i class='fa fa-user'></i> Change Password</a>";
                }
                else if($user->mode == "RETAILER")
                {
                    echo "<a class='dropdown-item' href='".url('changepassword_retailer')."'><i class='fa fa-user'></i> Change Password</a>";
                }
                else if($user->mode == "API PARTNER")
                {
                    echo "<a class='dropdown-item' href='".url('changepassword_apipartner')."'><i class='fa fa-user'></i> Change Password</a>";
                }
            ?>
            
            
            
            <div class="divider"></div>
            
            <a class="dropdown-item" href="{{url('logout')}}">
              <i class="fa fa-lock"></i> Logout</a>
          </div>
        </li>
      </ul>
     
    </header>