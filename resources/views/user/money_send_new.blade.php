<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')

   
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

    @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('dashboard_retailer')}}">
            <i class="large material-icons">home</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
        <div class="row ">
            <div class="col s12 m12 l3 xl3 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Name</label>
                        <br>
                        <label class="title-subcon"><?php echo $data['user_name']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">accessibility</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l3 xl3 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Msisdn</label><br>
                        <label class="title-subcon"><?php echo $data['msisdn']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">phone_android</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Total Limit</label><br>
                        <label class="title-subcon"><?php echo $data['total_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_done</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Consumed</label><br>
                        <label class="title-subcon"><?php echo $data['consume_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_download</i>
                    </div>
                </div>
            </div>
    
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Balance</label><br>
                        <label class="title-subcon"><?php echo $data['remaining_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_upload</i>
                    </div>
                </div>
            </div>
            
            
        </div>
        

        <!-- Page Body --> 
        <div class = "row card shadow" style="margin-bottom: 5px;margin-left:8px;margin-right:8px;">
            <div class ="col s12 m12 l12 xl12" style = "border: 1px solid #e3e6f0;border-radius:5px;padding:0px 0px;">
                <!-- Form Starts-->
                <div class="row" style="margin:2px 2px;">
                    <div class="col s6 m6 l6 xl6" style="margin:2px 2px;padding:1px 1px;">
                        <label class="title-con" style="margin-top:20px;">Beneficieries</label>
                    </div>
                    <div class="col s5 m5 l5 xl5 right-align" style="margin:2px 2px;padding:1px 1px;">
                        <button class="btn-floating  btn-small waves-effect waves-light #00897b teal darken-1" id="btn_add"><i class="small material-icons ">add</i></button>
                    </div>
                </div>

                <form id = "rech_form" class="user" action="#" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id = "ben_id" name="ben_id" value="">
                    <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['msisdn']; ?>">
                    <input type="hidden" id = "agent_id" name="agent_id" value="<?php echo $agent_id; ?>">
                    <input type="hidden" id = "bank_code" name = "bank_code" value="">

                    <input type="hidden" id = "nene_id" name = "bane_id" value="">
                    <input type="hidden" id = "cus_acc_no" name = "cus_acc_no" value="">
                    <input type="hidden" id = "cus_name" name = "cus_name" value="">
                    <input type="hidden" id = "cus_name" name = "cus_name" value="">
                    <input type="hidden" id = "bank_mode" name = "bank_mode" value="">
                    <input type="hidden" id = "cus_amt" name = "cus_amt" value="">

                    <input type="hidden" id = "trans_id" name="trans_id" value="">
                    <input type="hidden" id = "otp" name="otp" value="">
                    <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">


                <table class="bordered striped responsive-table ">
                    <thead>
                    <tr>
                        <th style='padding:4px 8px;' class = 'p-type PT'>NO</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'></th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Account No</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Name</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Bank</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Bank Code</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Mode</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Amount</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Calculate Per(%)</th>
                        <th style='padding:4px 8px;' class = 'p-type PT'>Action</th>
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                        <?php 
                            $j = 1;
                            $str = "";
                            foreach($beni as $r)
                            {
                                $str = $str."<tr><td style='padding:4px 8px;' class = 'p-type PT'>".$j."</td>";
                                $str = $str. "<td  style='font-size:11px;padding:4px 8px;text-align:left;'>";
                                $str = $str. "<button class='btn-floating btn-small waves-effect red darken-1' id='del_".$r['b_id']."'>
                                <i class='small material-icons '>delete_forever</i></button></td>";
                                $str = $str."<td style='padding:4px 8px;display:none;' class = 'p-type PT'>".$r['b_id']."</td>";
                                $str = $str."<td style='padding:4px 8px;' class = 'p-type PT' id='accno_".$r['b_id']."'>".$r['b_accno']."</td>";
                                $str = $str."<td style='padding:4px 8px;' class = 'p-type PT' id='name_".$r['b_id']."'>".$r['b_name']."</td>";
                                $str = $str."<td style='padding:4px 8px;' class = 'p-type PT' id='bnk_".$r['b_id']."'>".$r['b_bank']."</td>";
                                $str = $str."<td style='padding:4px 8px;' class = 'p-type PT' id='bcode_".$r['b_id']."'>".$r['b_bcode']."</td>";
                                
                                $str = $str."<td style='padding:4px 8px;' class = 'p-type PT'>";

                                $str = $str."<select id='mode_".$r['b_id']."' style = 'padding:6px 6px;'>
                                        <option value='-'>Select Mode</option>
                                        <option value='ANY'>ANY</option>
                                        <option value='IMPS'>IMPS</option>
                                        <option value='IMPS'>NEFT</option>
                                        <option value='RDGS'>RDGS</option>
                                        </select>";
                                $str = $str."<td  style='font-size:11px;padding:4px 8px;text-align:right;'><input type='text' id='amt_".$r['b_id']."' class='form-control' /></td>";
                                
                                $str = $str. "<td  style='font-size:11px;padding:4px 8px;text-align:left;'>";
                                $str = $str. "<button class='btn btn-small waves-effect purple darken-1' id='calc_".$r['b_id']."'>
                                Calculate</button></td>";
                                $str = $str. "<td  style='font-size:11px;padding:4px 8px;text-align:left;'>";
                                $str = $str. "<button class='btn btn-small waves-effect #c2185b pink darken-2' id='send_".$r['b_id']."'>
                                send</button></td>";
                                $str = $str."</tr>";
                    
                                $j++;
                            }

                            echo $str;
                        ?>

                    </tbody>
                </table>
                
                        
                </form>  

                <!-- End Form-->
            </div>
        </div>
        <!-- End Body --> 
    
        
    </div>
    

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP</h4>
            <p id = "tr_id"></p>
            <p id = "rep_tr_id"></p>
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="otp" id="cus_otp" name="cus_otp" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_otp">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    <!-- Delete Modal Structure -->
    <div id="modal2" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP for Delete Beneficiary</h4>
            <p id = "tr_id_1"></p>
            <p id = "rep_tr_id_1"></p>
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="otp" id="cus_otp_1" name="cus_otp_1" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_ttp_1">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
   @include('user.bottom_x')

    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        $trans_id = $op1['trans_id'];
        $rep_trans_id = $op1['rep_trans_id'];
        $otp_status = $op1['otp_status'];
        $remarks = $op1['remarks'];
        //$ben_acc_no = $op1['ben_acc_no'];

        file_put_contents(base_path().'/public/sample/ben_delete.txt', $op."---".$res."---".$trans_id."---".$otp_status."----".$remarks, FILE_APPEND);

        if($op == 0)
        {
            if($otp_status == "1")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#rep_trans_id').val('".$rep_trans_id."');

                    $('#tr_id').text('".$trans_id."');
                    $('#rep_tr_id').text('".$rep_trans_id."');

                    $('.modal').modal();
                    $('.modal').modal('open');
                });
                </script>";
            }
            else if($otp_status == "0")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                });
                </script>";
            }
            
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$remarks.". Transaction is ".$otp_status."', 'success');
               
            });
            </script>";
        }
        else if($op == 40)
        {
            if($otp_status == "1")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#rep_trans_id').val('".$rep_trans_id."');
                    $('#c_msisdn').val('".$op1['msisdn']."');

                    $('#tr_id_1').text('".$trans_id."');
                    $('#rep_tr_id_1').text('".$rep_trans_id."');

                    $('.modal').modal();
                    $('#modal2').modal('open');
                });
                </script>";
            }
            else if($otp_status == "0")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    //swal('Alert!', '".$res."', 'success');
                    swal({title: 'Great!', text: '".$res."', type: 'success'}).then(function()
                    { 
                         window.location.href = '".url('/'). "/bank_remitter_direct_check';
                    });
                });
                </script>";
            }
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error');
               
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var x = 0;
            var ben_id = 0;
            var ben_amt = 0;
            var ben_per = 0;
            var ben_pamt = 0;
            var ben_surp = 0;
            var ben_net = 0;

            $(document).on('click', "button",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("send_") !== -1)
                {
                    var nid = gid.split("send_");
                    var b_id = nid[1];
                    var b_bcode = $('#bcode_' + b_id).text();
                    var b_bank = $('#bnk_' + b_id).text();
                    var b_accno = $('#accno_' + b_id).text();
                    var b_name = $('#name_' + b_id).text();
                    var b_amnt = $('#amt_' + b_id).val();
                    var b_mode = $('option:selected', '#mode_' + b_id).val();

                    //alert(b_id + "---" + b_amnt + "---" + b_mode + "---"+ b_bcode +"----"+b_accno+"---"+b_name);

                    if(b_id == ben_id)
                    {
                        if(b_mode == "-") {
                            swal("Cancelled", "Please Select Transfer Mode", "error");
                        }
                        else if(b_amnt == "") {
                            swal("Cancelled", "Please Enter Amount", "error");
                        }
                        else
                        {
                            $('#ben_id').val(b_id);
                            $('#bene_id').val(b_id);
                            $('#bank_code').val(b_bcode);
                            $('#cus_acc_no').val(b_accno);
                            $('#cus_name').val(b_name);
                            $('#bank_mode').val(b_mode);
                            $('#cus_amt').val(b_amnt);

                            swal({
                                title: 'Are you sure?',
                                type: 'warning',
                                html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Bank</th><td style="padding:4px 8px;">' + b_bank + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Account No</th><td style="padding:4px 8px;">' + $('#cus_acc_no').val() + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Name</th><td style="padding:4px 8px;">' + $('#cus_name').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Mode</th><td style="padding:4px 8px;">' + $('#bank_mode').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + $('#cus_amt').val() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + ben_per + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + ben_pamt + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + ben_surp + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + ben_net + '</td></tr></tabel>',
        
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, Transfer!'
                                }).then((result) => {
                                if (result.value) {
                                    $('#loader').show();
                                    
                                    $('#rech_form').attr('action', "{{url('recharge_money')}}");
                                    $('#rech_form').submit();
                                }
                                else
                                {
                                    swal("Cancelled", "No Recharge...", "error");
                                    
                                }
                            });
                        }
                    }
                    else
                    {
                        swal("Cancelled", "Please Click calculate Button...", "error");
                    }
                   
                   
                   
                    
                }

                
                
                if(gid.indexOf("btn_otp") !== -1)
                {
                    $('#btn_submit_1').prop('disabled', true);

                    
                    var otp = $('#cus_otp').val();
                    

                    if(otp == "") {
                        swal("Cancelled", "Please Enter OTP", "error");
                        $('#btn_submit_1').prop('disabled', false);
                    }
                    else if(otp != "") 
                    {
                        $('#otp').val(otp);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_money_otp')}}");
                        $('#rech_form').submit();
                    }
                   
                }

                if(gid.indexOf("btn_add") !== -1)
                {
                    window.open("{{url('ben_entry')}}", "_self");
                   
                }

                if(gid.indexOf("calc_") !== -1)
                {
                    var nid = gid.split("calc_");
                    var b_id = nid[1];
                    var b_amnt = $('#amt_' + b_id).val();
                  
                    if(b_amnt != "")
                    {
                        $.ajax({
                            type: 'GET', //THIS NEEDS TO BE GET
                            url: "<?php echo url('/'); ?>/money_percentage?cus_amt=" + b_amnt + "&r_mode=WEB",
                            success: function (data) {
                                ben_id = b_id;
                                ben_amt = b_amnt;
                                ben_per = data['per'];
                                ben_pamt = data['per_amt'];
                                ben_surp = data['sur'];
                                ben_net = data['netamt'];

                                swal({
                                        title: 'Calculation Percentage',
                                        type: 'warning',
                                        html:  '<table class ="bordered striped responsive-table">' + '<tr><th  style="padding:4px 8px;">Amount</th><td style="padding:4px 8px;">' + data['amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Percentage(%)</th><td style="padding:4px 8px;">' + data['per'] + '</td></tr>'  
                                                            + '<tr><th style="padding:4px 8px;">Percent.Amount</th><td style="padding:4px 8px;">' + data['per_amt'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Surplus Charge</th><td style="padding:4px 8px;">' + data['sur'] + '</td></tr>'
                                                            + '<tr><th style="padding:4px 8px;">Net Amount</th><td style="padding:4px 8px;">' + data['netamt'] + '</td></tr></tabel>',
                
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'OK!'
                                        }).then((result) => {
                                        if (result.value) {
                                            
                                        }
                                        else
                                        {
                                            
                                            
                                        }
                                    });
                            },
                            error: function() { 
                                
                                console.log(data);
                            }
                        });
                        
                    }
                    else
                    {
                        swal("Cancelled", "Please Enter Transfer Amount...", "error");
                    }
                   
                   
                    
                }

                if(gid.indexOf("del_") !== -1)
                {
                    var nid = gid.split("del_");
                    var b_id = nid[1];
                    var b_accno = $('#accno_' + b_id).text();
                    var msisdn = $('#c_msisdn').val();
                    swal({
                                title: 'Are you sure?',
                                type: 'warning',
                                html:  '<table class ="bordered striped responsive-table">'
                                                    + '<tr><th style="padding:4px 8px;">Account No</th><td style="padding:4px 8px;">' + $('#accno_' + b_id).text() + '</td></tr>'  
                                                    + '<tr><th style="padding:4px 8px;">Name</th><td style="padding:4px 8px;">' + $('#name_' + b_id).text() + '</td></tr>'
                                                    + '<tr><th style="padding:4px 8px;">Bank</th><td style="padding:4px 8px;">' + $('#bnk_' + b_id).text() + '</td></tr></table>',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes, delete!'
                                }).then((result) => {
                                if (result.value) {
                                    //$('#loader').show();
                                    
                                    window.location.href = "<?php echo url('/'); ?>/ben_delete/" + b_accno + "/" + msisdn + "/" + b_id;
                                }
                                else
                                {
                                    swal("Cancelled", "No delete...", "error");
                                    
                                }
                            });
                   
                }

                if(gid.indexOf("btn_ttp_1") !== -1)
                {
                    $('#btn_otp_1').prop('disabled', true);
                    var otp = $('#cus_otp_1').val();
                    var tid = $('#trans_id').val();
                    var msisdn = $('#c_msisdn').val();
                    
                    alert(otp + "--" + tid + "--" + msisdn);
                    if(otp == "") {
                        swal("Cancelled", "Please Enter OTP", "error");
                        $('#btn_otp_1').prop('disabled', false);
                    }
                    else if(otp != "" && tid != "" && msisdn != "") 
                    {
                        window.location.href = "<?php echo url('/'); ?>/ben_delete_otp/" + tid + "/" + msisdn + "/" + otp;
                    }
                   
                }

                
            });

            
        
        });
    </script>
</body>
</html>