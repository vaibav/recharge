<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
    </head>
     
    <body style = "background-color: #34495e;">

       
        
       
        
        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row" >
                <div class="col s12 m12 l12 x12 center-align" >
                    <h2 class="white-text">Vaibav Online</h2>
                    <h4 class="white-text">Welcome Retailer</h4>
                </div>
        </div>
        <br><br><br><br><br>
        <div class="row" >
            <div class="col s12 m12 l2 x2 " ></div>
            <div class="col s12 m12 l4 xl4 " style="margin-bottom: 50px;">
              
                    <div class="card">
                        <div class="card-image">
                            
                            <span class="card-title">Recharge</span>
                            <a class="btn-floating halfway-fab waves-effect waves-light red" id='btn_rech'><i class="material-icons">arrow_forward</i></a>
                        </div>
                        <div class="card-content">
                            <p> This is Mobile Recharge portal exclusively for VAIBAV Retailers. Retailer recharge any network and get report through this portal.</p>
                        </div>
                    </div>
                
           
            </div>

            <div class="col s12 m12 l4 xl4 ">
               
               
                    <div class="card">
                            <div class="card-image">
                                
                                <span class="card-title">Money Transfer</span>
                                <a class="btn-floating halfway-fab waves-effect waves-light red" id='btn_new'><i class="material-icons">add</i></a>
                            </div>
                            <div class="card-content">
                                
                                <form  id = "rech_form" class="form-horizontal" action="{{url('bank_remitter_check')}}" method="post" accept-charset="UTF-8">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="row" style="margin-bottom: 2px;">
                                            <div class="input-field col s12 m12 l6 xl6" style="margin-top:4px;margin-bottom: 4px;">
                                                <input id="msisdn" name = "msisdn" type="text" class="validate">
                                                <span class="text-danger">{{ $errors->first('msisdn') }}</span>
                                                <label for="msisdn">Customer Mobile No</label>
                                            </div>
                                            
                                            <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                                <button class="btn waves-effect waves-light green darken-2" type="submit" name="action" id="btn_submit">GO
                                                    
                                                </button>
                                            </div>
                                        </div>

                                </form>

                            </div>
                        </div>
                
           
            </div>
            <div class="col s12 m12 l2 x2 " ></div>
        </div>
        
        <!-- End Page Layout  -->

       

    <!--JavaScript at end of body for optimized loading-->
    @include('user.bottom1')

     <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <!-- script -->
    <script>
    $(document).ready(function() 
    {
        $(".dropdown-trigger").dropdown();
        $('.sidenav').sidenav();
        $('select').formSelect();

        $('#loader').hide();
    
        
        $("#msisdn").keydown(function (e) 
        {
            numbersExactly(e);
        });
      

        $('#btn_new').on('click',function(e)
        {
            e.preventDefault();
            window.open("{{url('bank_remitter_entry')}}", "_self");
        });

        $('#btn_rech').on('click',function(e)
        {
            e.preventDefault();
            window.open("{{url('dashboard_retailer')}}", "_self");
        });

        $('#btn_submit').on('click',function(e)
        {
            e.preventDefault();
            var mob = $("#msisdn").val();
            if(mob != "")
            {
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Go!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('bank_remitter_check')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
            }
            else
            {
                swal("Cancelled", "Error...Mobile No is Empty!", "error");
            }
            
        }); 

       
        function numbersExactly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
                    // let it happen, don't do anything
                    return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }
        
    });
    </script>

    </body>
  </html>