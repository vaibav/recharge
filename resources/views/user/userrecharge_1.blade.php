<!DOCTYPE html>
<!-- html -->
<html lang="{{ app()->getLocale() }}">
<!-- head -->
<head>
    <title>VaibavOnline</title>

    <!--meta data-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="VaibavOnline is a Mobile Multirecharge web portal" />
    <!--//meta data-->

    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"  media="all" /><!-- Fontawesome-CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/materialize.min.css') }} " />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/easy-responsive-tabs.css') }} " />
    
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type='text/javascript' src="{{ asset('js/jquery.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/materialize.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/easyResponsiveTabs.js') }}"></script>

    <!-- online fonts -->
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    
       
</head>
<!-- //head -->
<!-- body -->
<body  style="background: #F4F6F6 ;">

        <div class="fixed-action-btn toolbar">
                <a class="btn-floating btn-large purple">
                  <i class="large material-icons">mode_edit</i>
                </a>
                <ul>
                  <li><a class="btn-floating ">User</a></li>
                  <li><a class="btn-floating ">Payment</a></li>
                  <li><a class="btn-floating ">Recharge</a></li>
                  <li><a class="btn-floating ">logout</a></li>
                </ul>
              </div>
    <div class="container-fluid">
           
        <!-- Page Content goes here -->
        <div class="row">
            

            <div class="col s12 m8 l8 xl8">
                <div style="height: 80px;">
                    <div class="row">
                        
                        <div class="col s12 m9 l9 xl9" style="padding-top: 15px;padding-bottom: 15px;">
                            <label class="badge " style="padding: 8px 5px;border-radius: 2px;">
                                    <label class="col s3 m3 l3 xl3" style="font-size: 20px;">News :</label>    
                                <marquee class="col s9 m8 l8 xl8 " direction = "left" style="font-size: 20px;color:#52BE80;font-weight: bold;">
                                    <?php 
                                        $stx = "";
                                        foreach($offer as $r)
                                        {
                                            $stx = $stx . $r->offer_details. " <i class='fa fa-bell-o blinking' aria-hidden='true' class=''></i>";
                                        }
                                        echo $stx;
                                    ?>
                                </marquee>
                                
                            </label>
                        </div>
                        <div class="col s12 m3 l3 xl3 right-align" style="padding-top: 15px;padding-bottom: 15px;">
                                <label  style="font-size: 20px;">Bal : <?php echo $user->ubal; ?></label>
                        </div>
                    </div>
                    
                </div>
                <div id="parentVerticalTab">
					<div class="agileits-tab_nav">
                        <ul class="resp-tabs-list hor_1">
                            <li style="font-family: 'Oswald', sans-serif;"><i class="icon fa fa-mobile" aria-hidden="true"></i>PREPAID</li>
                            <li style="font-family: 'Oswald', sans-serif;"><i class="icon fa fa-mobile" aria-hidden="true"></i>POSTPAID</li>
                            <li style="font-family: 'Oswald', sans-serif;"><i class="icon fa fa-television" aria-hidden="true"></i>DTH</li>
                            <li style="font-family: 'Oswald', sans-serif;"><i class="icon fa fa-lightbulb-o" aria-hidden="true"></i>BILL</li>
                            <li style="font-family: 'Oswald', sans-serif;"><i class="icon fa fa-credit-card" aria-hidden="true"></i>MONEY</li>
                        </ul>
					</div>
					<div class="resp-tabs-container hor_1">
                        <!-- tab1  Prepaid-->
						<div>
                            <div class="tabs-box">
                                
                                <img src="{{ asset('img/small/mobile_96.png') }}" class="w3ls-mobile" alt="" data-pin-nopin="true">
        
                                <div class="clearfix"> </div>
                                
                                <div class="login-form">	
                                    <form id = "rech_form" action="pay.html" method="post" method="post" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="id_user_mobile" type="text" name="user_mobile" class="validate">
                                                <label for="id_user_mobile">Mobile No</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;margin-right: 2px;">
                                            <select class="icons col s12" id="id_net_code" name="net_code" >
                                                <option value="" disabled selected>Select Network</option>
                                                <?php
                                                    foreach($network as $f)
                                                    {
                                                        if ($f->net_type_code == '20')
                                                            echo "<option value='".$f->net_code."' data-icon='". asset("img/prepaid/".$f->net_code.".jpg") ."'>".$f->net_name."</option>";
                                                    }
                                                ?>
                                               
                                            </select>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="id_user_amount" type="text" name="user_amount" class="validate">
                                                <label for="id_user_amount">Amount</label>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 col s12 m4 l4 xl4" type="submit" id="btn_submit">Recharge
                                                <i class="material-icons right">send</i>
                                            </button>
                                            <div class="col s12 m4 l4 xl4 center-align">
                                                    <a class="btn-floating pulse text-center #7b1fa2 purple darken-2" style="text-align: center"  href = "#" id = "plan_121">121 </a>
                                            </div>
                                            <div class="col s12 m4 l4 xl4 left-align">
                                                    <a class="btn-floating pulse text-center #00796b teal darken-2" style="text-align: center" href="#">plans </a>
                                            </div>
                                           
                                           
                                        </div>


                                      
                                    </form>													
                                </div>	
		
                                <div class="clearfix"> </div>
                                
		                    </div>
	
			            </div>
                        <!-- /tab1 -->
                        
                        
                         <!-- tab2   Postpaid-->
						<div>
                            <div class="tabs-box">
                                
                                <img src="{{ asset('img/small/postpaid_96.png') }}" class="w3ls-mobile" alt="" data-pin-nopin="true">
        
                                <div class="clearfix"> </div>
                                
                                <div class="login-form">	
                                    <form action="pay.html" method="post" id="signup">

                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="email" type="text" class="validate">
                                                <label for="email">Mobile No</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;margin-right: 2px;">
                                            <select class="icons col s12" >
                                                <option value="" disabled selected>Select Network</option>
                                                <?php
                                                    foreach($network as $f)
                                                    {
                                                        if ($f->net_type_code == '21')
                                                            echo "<option value='".$f->net_code."' data-icon='". asset("img/prepaid/".$f->net_code.".jpg") ."'>".$f->net_name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="amt" type="text" class="validate">
                                                <label for="amt">Amount</label>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 col s12 m4 l4 xl4" type="submit" name="action">Recharge
                                                <i class="material-icons right">send</i>
                                            </button>
                                          
                                           
                                        </div>


                                      
                                    </form>													
                                </div>	
		
                                <div class="clearfix"> </div>
                                
		                    </div>
	
			             </div>
                        <!-- /tab2 -->
                        
                  
                        <!-- tab3   DTH-->
						<div>
                            <div class="tabs-box">
                                
                                <img src="{{ asset('img/small/dth_96.png') }}" class="w3ls-mobile" alt="" data-pin-nopin="true">
        
                                <div class="clearfix"> </div>
                                
                                <div class="login-form">	
                                    <form action="pay.html" method="post" id="signup">

                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="email" type="text" class="validate">
                                                <label for="email">Mobile No</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;margin-right: 2px;">
                                            <select class="icons col s12" >
                                                <option value="" disabled selected>Select Network</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/857.jpg') }}">Airtel</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/636.jpg') }}">Vodafone</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/510.jpg') }}">BSNL Topup</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/552.jpg') }}">BSNL Recharge</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/336.jpg') }}">Idea</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/781.jpg') }}">Docomo</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/372.jpg') }}">Jio</option>
                                            </select>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="amt" type="text" class="validate">
                                                <label for="amt">Amount</label>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 col s12 m4 l4 xl4" type="submit" name="action">Recharge
                                                <i class="material-icons right">send</i>
                                            </button>
                                            <div class="col s12 m4 l4 xl4 center-align">
                                                    <a class="btn-floating pulse text-center #7b1fa2 purple darken-2" style="text-align: center" href="two.html">info </a>
                                            </div>
                                           
                                           
                                        </div>

                                    </form>													
                                </div>	
		
                                <div class="clearfix"> </div>
                                
		                    </div>
	
			             </div>
                        <!-- /tab3 -->


                        <!-- tab4 ---- Bill -->
						<div>
                            <div class="tabs-box">
                                
                                <img src="{{ asset('img/small/light_96.png') }}" class="w3ls-mobile" alt="" data-pin-nopin="true">
        
                                <div class="clearfix"> </div>
                                
                                <div class="login-form">	
                                    <form action="pay.html" method="post" id="signup">

                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="eb_bill" type="text" class="validate">
                                                <label for="eb_bill">Consumer No</label>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;margin-right: 2px;">
                                            <select class="icons col s12" >
                                            <option value="" disabled selected>Select Network</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/857.jpg') }}">Airtel</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/636.jpg') }}">Vodafone</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/510.jpg') }}">BSNL Topup</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/552.jpg') }}">BSNL Recharge</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/336.jpg') }}">Idea</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/781.jpg') }}">Docomo</option>
                                                <option value="" data-icon="{{ asset('img/prepaid/372.jpg') }}">Jio</option>
                                            </select>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="input-field col s12">
                                                <input id="amt" type="text" class="validate">
                                                <label for="amt">Amount</label>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-bottom: 5px;margin-left: 2px;">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 col s12 m4 l4 xl4" type="submit" name="action">Pay
                                                <i class="material-icons right">send</i>
                                            </button>
                                            <div class="col s12 m4 l4 xl4 center-align">
                                                    <a class="btn-floating pulse text-center #7b1fa2 purple darken-2" style="text-align: center" href="two.html">info </a>
                                            </div>

                                        </div>


                                      
                                    </form>													
                                </div>	
		
                                <div class="clearfix"> </div>
                                
		                    </div>
	
			             </div>
                        <!-- /tab3 -->


                        <!-- tab5 ---- Money -->
						<div>
                            <div class="tabs-box">
                                
                                <img src="{{ asset('img/small/money_96.png') }}" class="w3ls-mobile" alt="" data-pin-nopin="true">
        
                                <div class="clearfix"> </div>
                                
                                <div class="login-form">	
                                    <form action="pay.html" method="post" id="signup">

                                        <label> Under Construction....</label>
                                       
                                    </form>													
                                </div>	
		
                                <div class="clearfix"> </div>
                                
		                    </div>
	
			             </div>
                        <!-- /tab3 -->
        
                      
					</div>
					<div class="clearfix"></div>
				</div>

                <!-- footer -->
                <div class = "row" style="margin-bottom:2px;">
                    <div class="col s112 m4 l4 xl4" style="margin-top:2px;">
                        <img src="{{ asset('img/brand/logo.png') }}" style = "width:170px; height:60px;" />
                    </div>

                     <div class="col s112 m6 l6 xl6 center-align" style="margin-top:2px;">
                        <div class="preloader-wrapper small active" id = "pre_loader">
                            <div class="spinner-layer spinner-green-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!--Recharge Report-->
            <div class="col s12 m4 l4 xl4" style="margin-top:10px;" id = "tbl_body">
                <?php echo $recharge; ?>
                
                    
            </div>
            
            
        </div>


        
    </div>

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
                <h4>Info</h4>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
    
   
	 

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 11)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            else if($op == 20)
            {
                $net_code = $op1['net_code'];
                $net_type = $op1['net_type'];
                $mob = $op1['mob'];

                echo "<script>
                $(document).ready(function() 
                {
                    $('select').formSelect();
                    $('.fixed-action-btn').floatingActionButton({
                        toolbarEnabled: true
                    });
                    $('#rech_offer_body').html('".$res."');
                    $('.modal').modal();
                    $('.modal').modal('open'); 
                });
                </script>";
            }
            
            session()->forget('result');
        }
    ?>
    

    

    <!--Plug-in Initialisation-->
    <!-- script -->
    <script>
        $(document).ready(function() {

            $('select').formSelect();
            $('.fixed-action-btn').floatingActionButton({
                toolbarEnabled: true
            });

            //$('.modal').modal();
            
            $('#pre_loader').hide();

            window.setInterval(function(){
                /// call your function here
                $('#pre_loader').show();
                load_data();
			}, 19000);

            $("#tab2").hide();
            $("#tab3").hide();
            $("#tab4").hide();
            $(".tabs-menu a").click(function(event){
                event.preventDefault();
                var tab=$(this).attr("href");
                $(".tab-grid").not(tab).css("display","none");
                $(tab).fadeIn("slow");
            });

            //Vertical Tab
            $('#parentVerticalTab').easyResponsiveTabs({
                type: 'vertical', //Types: default, vertical, accordion
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                tabidentify: 'hor_1', // The tab groups identifier
                activate: function(event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#nested-tabInfo2');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });


            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'userresult_data1',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
                        $('#pre_loader').hide();
						/*$('#tim').html(d.toLocaleString());
						if(c == 1)
						{
							$('#tim').css("background-color", "#EC7063");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("background-color", "#27AE60");
							c = 1;
						}*/
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }


            $('#plan_121').on('click',function(e)
            {
                e.preventDefault();
                var ax = "PREPAID";
                if(ax == "PREPAID")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        //$('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_offers')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        M.toast({html: 'Please Select Network...', classes: 'rounded'})
                        
                    }
                    else if(usr_mobe == "")
                    {
                        M.toast({html: 'Please Enter Mobile No...', classes: 'rounded'})
                        
                    }
                }
                else if(ax == "DTH")
                {
                    var net_code = $('#id_net_code').val();
                    var usr_mobe = $('#id_user_mobile').val();
                    
                    if(net_code != "" && usr_mobe != "")
                    {
                        $('#plan_121').prop('disabled', true);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('dth_offers')}}");
                        $('#rech_form').submit();
                    }
                    else if(net_code == "")
                    {
                        swal("Cancelled", "Please Select Network...", "error");
                    }
                    else if(usr_mobe == "")
                    {
                        swal("Cancelled", "Please Enter Mobile No...", "error");
                    }
                }
               
            });


        });
    </script>
	
	
    

    
</body>
<!-- //body -->
</html>
<!-- //html -->