<ol class="breadcrumb">
    <li class="breadcrumb-item black-text">Home</li>
    <li class="breadcrumb-item">
    <?php 
        if($user->mode == "RETAILER")
            echo "<a href='".url('dashboard_retailer')."'>".$user->mode."</a>";
        else if($user->mode == "SUPER DISTRIBUTOR")
            echo "<a href='".url('dashboard_user')."'>".$user->mode."</a>";
        else if($user->mode == "DISTRIBUTOR")
            echo "<a href='".url('dashboard_distributor')."'>".$user->mode."</a>";
        else if($user->mode == "API PARTNER")
            echo "<a href='".url('dashboard_apipartner')."'>".$user->mode."</a>";
    ?>
    
    </li>
    <li class="breadcrumb-item active">{{$title}}</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
        <span class="badge"> <i class="fa fa-bell-o"></i> {{$bal}}</span>
        <span class="badge"> <i class="icon-graph"></i> &nbsp; Welcome {{$uname}}</span>
    
    </li>
</ol>