<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

    @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('dashboard_retailer')}}">
            <i class="large material-icons">home</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
       
        <br>
        <br>
        
        <div class="row " >
            <div class="col s12 m12 l12 xl12 " >
                <div class = "card1 shadow m-36">
                    <form id = "rech_form" class="user" action="{{url('bank_remitter_check')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <br>
                    <label class="title-con" style="margin-top:20px;padding-left:10px;">Customer Login</label>
                    <div class="row" style="margin-top:20px;margin:5px 2px;">
                        <div class="input-field col s12 m12 l6 xl6" >
                            <input placeholder="Mobile No" id="msisdn" name = "msisdn" type="text" class="validate">
                            <span class="text-danger">{{ $errors->first('msisdn') }}</span>
                        </div>
                        <div class="input-field col s12 m12 l6 xl6 left-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit">submit</button>
                        </div>
                    </div>
        
                   
                    </form>


                      <div class="row" style="margin-top:20px;margin:5px 2px;">
                            <div class="col s11 m11 l11 xl11" style="margin:2px 10px;padding:1px 1px;">
                            
                                <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                            <th style='font-size:12px;padding:7px 8px;'>No</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Id </th>
                                            <th style='font-size:12px;padding:7px 8px;'>Msisdn</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Address</th>
                                            <th style='font-size:12px;padding:7px 8px;'>City</th>
                                            <th style='font-size:12px;padding:7px 8px;'>State</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Pincode</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                            <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                    <?php 
                                                
                                                $j = 1;
                                                foreach($remitter as $f)
                                                {
                                                    $state = "";
                                                    if($f->user_state_code == "6") {
                                                        $state = "TAMIL NADU";
                                                    }
                                                   

                                                    echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_id."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->msisdn."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_rem_name."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_address."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_city."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$state."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_pincode."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_status."</td>";
                                                    echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_date."</td>";
                                                   

                                                    echo "</tr>";
                                                
                                                    $j++;
                                                }
                                                
                                            ?>
                                    </tbody>
                                </table>


                        </div>
                    </div>

                </div>
               
            </div>
    
    
            
        </div>
    </div>
    

   
   @include('user.bottom_x')
    
   <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    
                });
                </script>";
            }
            else 
            {
                $msisdn = $op1['msisdn'];
                $url_1 = 'bank_remitter_entry?msisdn='.$msisdn;
                
                echo "<script>
                $(document).ready(function() 
                {
                    swal({
                        title: 'Do You Create New Customer?',
                        text: '".$res."',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Create!'
                        }).then((result) => {
                        if (result.value) {
                            window.open('".url($url_1)."', '_self');
                        }
                        else
                        {
                            swal('Cancelled', 'Sorry...', 'error');
                            
                        }
                    });
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
           
            $("#msisdn").keydown(function (e) 
            {
                numbersExactly(e);
            });


            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var mob = $("#msisdn").val();
                if(mob != "")
                {
                    $('#btn_submit').prop('disabled', true);
                    swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Go!'
                            }).then((result) => {
                            if (result.value) {
                                
                                $('#rech_form').attr('action', "{{url('bank_remitter_check')}}");
                                $('#rech_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                $('#btn_submit').prop('disabled', false);
                            }
                        });
                }
                else
                {
                    swal("Cancelled", "Error...Mobile No is Empty!", "error");
                }
                
            }); 


            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }


        });
    </script>
</body>
</html>