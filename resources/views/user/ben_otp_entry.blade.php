<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <div class="row" >
                <div class="col s12 m12 l12 x12 center-align" >
                    <h4 class="white-text">Vaibav Online</h4>
                    <h5 class="white-text">Hello {{$rem_name}}! Welcome To Money Transfer!</h5>
                </div>
        </div>

      <!-- loader-->
      <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:40px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Beneficiary OTP Entry</span>
                    
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <?php
                    if($beneficiary->count() > 0)
                    {
                        $ben_id = $beneficiary[0]->ben_id;
                        $agent_id = $beneficiary[0]->agent_id;
                        $msisdn = $beneficiary[0]->msisdn;
                        $trans_id = $beneficiary[0]->trans_id;
                    }
                    else
                    {
                        $ben_id = '0';
                        $agent_id = "1234";
                        $msisdn = "9999999999";
                        $trans_id = "122324";
                    }
                ?>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <form  id = "rech_form" class="form-horizontal" action="{{url('ben_otp_entry_store')}}" method="post" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="ben_id" id="ben_id" value="{{ $ben_id }}">
                                <input type="hidden" name="agent_id" id="agent_id" value="{{ $agent_id }}">
                                <input type="hidden" name="msisdn" id="msisdn" value="{{ $msisdn }}">
                                <input type="hidden" name="trans_id" id="trans_id" value="{{ $trans_id }}">
                               
                                <div class="row" style="margin-bottom: 2px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="otp" name = "otp" type="text" class="validate">
                                        <span class="text-danger">{{ $errors->first('otp') }}</span>
                                        <label for="otp">OTP</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <button class="btn waves-effect waves-light green darken-2" type="submit" name="action" id="btn_otp">Resend OTP
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>

                            
                                
                            
                               
                            
                                

                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="col s12 m12 l12 xl12 left-align" style="margin-top:4px;margin-bottom: 4px;margin-left:50px;">
                                        <button class="btn waves-effect waves-light blue darken-2" type="submit" name="action" id="btn_submit">Submit
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>
                        </form>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 

                </div>
                
            </div>


            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

    <?php

       
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    window.location.href = '".url('/'). "/dashboard_money_new';
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                });
                </script>";
            }
            else if($op > 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                    //window.location.href = '".url('/'). "/dashboard_money_new';
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#btn_back').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('ben_view')}}", "_self");
            });
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Add!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('ben_otp_entry_store')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                   
            }); 

            $("#otp").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Resend!'
                        }).then((result) => {
                        if (result.value) {
                            //$('#loader').show();
                            $('#rech_form').attr('action', "{{url('ben_otp_resend')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                   
            }); 

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
            
           
      });
    </script>
    </body>
</html>
