<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Bank Remitter</span>
                    <a class='btn-floating halfway-fab waves-effect waves-light red' id='btn_add'><i class='material-icons'>add</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Remitter Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Address</th>
                                      <th style='font-size:12px;padding:7px 8px;'>city</th>
                                      <th style='font-size:12px;padding:7px 8px;'>State</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Pincode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Reply ID</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                        
                                        $j = 1;
                                        foreach($data as $f)
                                        {
                                            $state = "OTHER STATE";
                                            if($f->user_state_code == 6)
                                                $state = "TAMIL NADU";

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->msisdn."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_rem_name."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_address."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_city."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$state."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_pincode."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_status."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_rep_opr_id."</td>";
                                            echo "</tr>";
                                          
                                            $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
           
            
           
      });
    </script>
    </body>
</html>
