<!DOCTYPE html>
<html lang="en">
<head>
    @include('user.top_x')

   
</head>
<body style="background-color: #FFFFFF;overflow-x: hidden;">

    @include('user.sidebar_x_retailer')

    <div class="fixed-action-btn">
        <a class="btn-floating btn-large #01579b light-blue darken-4" href = "{{url('dashboard_retailer')}}">
            <i class="large material-icons">home</i>
        </a>
            
    </div>

    <!-- loader-->
    <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
        <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
                </div><div class="circle-clipper right">
                <div class="circle"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap-con">
        <div class="row ">
            <div class="col s12 m12 l3 xl3 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Name</label>
                        <br>
                        <label class="title-subcon"><?php echo $data['user_name']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">accessibility</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l3 xl3 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Msisdn</label><br>
                        <label class="title-subcon"><?php echo $data['msisdn']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">phone_android</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Total Limit</label><br>
                        <label class="title-subcon"><?php echo $data['total_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_done</i>
                    </div>
                </div>
            </div>
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Consumed</label><br>
                        <label class="title-subcon"><?php echo $data['consume_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_download</i>
                    </div>
                </div>
            </div>
    
    
            <div class="col s12 m12 l2 xl2 ">
                <div class = "card1 h-80 shadow">
                    <div class="col s9 m9 l9 xl9 left-align" style="margin: 10px 2px;padding: 1px 2px;">
                        <label class="title-con">Balance</label><br>
                        <label class="title-subcon"><?php echo $data['remaining_limit']; ?></label>
                    </div>
                    <div class="col s2 m2 l2 xl2 center-align" style="margin: 20px 2px;padding: 1px 2px;">
                        <i class="small material-icons amber-text">cloud_upload</i>
                    </div>
                </div>
            </div>
            
            
        </div>
        <br>
    
        <div class="row " >
            <div class="col s12 m12 l8 xl8 " >
                <div class = "card1 shadow m-36">
                    <form id = "rech_form" class="user" action="#" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id = "ben_id" name="ben_id" value="">
                    <input type="hidden" id = "c_msisdn" name="c_msisdn" value="<?php echo $data['msisdn']; ?>">
                    <input type="hidden" id = "agent_id" name="agent_id" value="<?php echo $agent_id; ?>">
                    <input type="hidden" id = "trans_id" name="trans_id" value="">
                    <input type="hidden" id = "otp" name="otp" value="">
                    <input type="hidden" id = "rep_trans_id" name="rep_trans_id" value="">
                    <br>
                    <label class="title-con" style="margin-top:20px;padding-left:10px;">Transfer</label>
                    <div class="row" style="margin-top:20px;margin:5px 2px;">
                        <div class="input-field col s12 m12 l6 xl6" >
                            <select id="bank_code" name = "bank_code" style = "padding:6px 6px;">
                                    <option value="">Select Bank</option>
                                    <option value="ANDB">Andhra Bank</option>
                                    <option value="UTIB">Axis Bank</option>
                                    <option value="BARB">Bank of Baroda</option>
                                    <option value="BKID">Bank of India</option>
                                    <option value="CNRB">Canara Bank</option>
                                    <option value="CSBK">Catholic Syrian Bank</option>
                                    <option value="CBIN">Central Bank of India</option>
                                    <option value="CITI">Citi Bank</option>
                                    <option value="CCBN">Citizen Co-Op Bank</option>
                                    <option value="CIUB">City Union Bank</option>
                                    <option value="CORP">Corporation Bank</option>
                                    <option value="DLXB">Dhanlaxmi Bank</option>

                                    <option value="FDRL">Federal Bank</option>
                                    <option value="FINO">FINO Payment Bank</option>
                                    <option value="HDFC">HDFC Bank</option>
                                    <option value="HSBC">HSBC Bank</option>

                                    <option value="ICIC">ICICI Bank</option>
                                    <option value="IBKL">IDBI Bank</option>
                                    <option value="IDFB">IDFC Bank</option>
                                    <option value="IDIB">Indian Bank</option>
                                    <option value="IOBA">Indian Overseas Bank</option>
                                    <option value="INDB">IndusInd Bank</option>
    
                                    <option value="KVBL">Karur Vysya Bank</option>
                                    <option value="KKBK">Kotak Mahindra Bank</option>
                                    <option value="KKBN">Lakshmi Vilas Bank</option>
                                    <option value="BKID">National Co-Operative Bank</option>
                                    <option value="ORBC">Oriental Bank of Commerce</option>
                                    <option value="PYTM">PAYTM PAYMENT BANK</option>
                                    <option value="PUNB">Punjab National Bank</option>

                                    <option value="SIBL">South Indian Bank</option>
                                    <option value="SCBL">Standard Chartered Bank</option>
                                    <option value="SBIN">State Bank of India</option>
                                    <option value="SYNB">Syndicate Bank</option>
                                    <option value="TMBL">Tamilnad Mercantile Bank</option>
                                    <option value="TNSC">The Tamilnadu State Apex Cooperative Bank</option>

                                    <option value="UCBA">UCO Bank</option>
                                    <option value="UBIN">Union Bank of India</option>
                                    <option value="UTBI">United Bank of India</option>
                                    <option value="VIJB">Vijaya Bank</option>
                                    <option value="YESB">Yes Bank</option>
                                </select>  
                                    
                        </div>
        
                        <div class="input-field col s12 m12 l6 xl6" >
                            <input placeholder="Account No" id="cus_acc_no" name = "cus_acc_no" type="text" class="validate">
                            <span class="text-danger">{{ $errors->first('cus_acc_no') }}</span>
                        </div>
                    </div>

                     <div class="row" style="margin:5px 2px;">
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Beneficiary Name" id="cus_name" name="cus_name" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('cus_name') }}</span>
                                        
                            </div>
            
                            <div class="input-field col s12 m12 l6 xl6" >
                                
                            </div>
                    </div>
        
                    <div class="row" style="margin:5px 2px;">
                            <div class="input-field col s12 m12 l6 xl6" >
                                <select id="bank_mode" name = "bank_mode" style = "padding:6px 6px;">
                                    <option value="">Select Mode</option>
                                    <option value="ANY">ANY</option>
                                    <option value="IMPS">IMPS</option>
                                    <option value="IMPS">NEFT</option>
                                    <option value="RDGS">RDGS</option>
                                </select>  
                                        
                            </div>
            
                            <div class="input-field col s12 m12 l6 xl6" >
                                <input placeholder="Amount" id="cus_amt" name="cus_amt" type="text" class="validate">
                                <span class="text-danger">{{ $errors->first('cus_amt') }}</span>
                            </div>
                    </div>
        
                    <div class="row" style="margin:5px 2px;">
                        <div class="input-field col s12 m12 l12 xl12 right-align" >
                                <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_submit">submit</button>
                        </div>
                    </div>
            
                    </form>
                </div>
               
            </div>
    
    
            <div class="col s12 m12 l4 xl4 " >
                <div class = "card1 shadow m-36 scrollspy" style="padding: 2px 5px;overflow-y: scroll;height:300px;">
                       
                        <div class="row" style="margin:2px 2px;">
                            <div class="col s6 m6 l6 xl6" style="margin:2px 2px;padding:1px 1px;">
                                <label class="title-con" style="margin-top:20px;">Beneficieries</label>
                            </div>
                            <div class="col s5 m5 l5 xl5 right-align" style="margin:2px 2px;padding:1px 1px;">
                                <button class="btn-floating  btn-small waves-effect waves-light #00897b teal darken-1" id="btn_add"><i class="small material-icons ">add</i></button>
                            </div>
                        </div>
                        <?php 
                            $j = 1;
                            foreach($beni as $r)
                            {
                        ?>
                       
                        <div class="row card1 shadow" style="margin:2px 2px;">
                            <div class="input-field col s9 m9 9 xl9" style="margin:6px 0px;">
                                    <p class="p-type PT" style = "display:none;" id = "<?php echo 'b_id_'.$j; ?>"><?php echo $r['b_id']; ?></p>
                                    <p class="p-type PT" id = "<?php echo 'b_name_'.$j; ?>"><?php echo $r['b_name']; ?></p>
                                    <p class="p-type PT" id = "<?php echo 'b_accno_'.$j; ?>"><?php echo $r['b_accno']; ?></p>
                                    <p class="p-type PT" id = "<?php echo 'b_bank_'.$j; ?>"><?php echo $r['b_bank']; ?></p>
                                    <p class="p-type PT" style = "display:none;" id = "<?php echo 'b_bcode_'.$j; ?>"><?php echo $r['b_bcode']; ?></p>
                            </div>
                
                            <div class="col s2 m2 l2 xl2 center-align" style="margin:18px 0px;">
                                    <button class="btn-floating  waves-effect waves-light #8e24aa purple darken-1" id = "<?php echo 'btn_ben_'.$j; ?>">
                                            <i class="small material-icons" id = "<?php echo 'ico_'.$j; ?>">check</i></button>
                            </div>
                        </div>
            
                        <?php
                                $j++;
                            }
                        ?>
                </div>
                
            </div>
        </div>
    </div>
    

    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Enter OTP</h4>
            <p id = "tr_id"></p>
            <p id = "rep_tr_id"></p>
            <div class="row" style="margin:5px 2px;">
                <div class="input-field col s12 m12 l6 xl6" >
                    <input placeholder="otp" id="cus_otp" name="cus_otp" type="text" class="validate"> 
                </div>

                <div class="input-field col s12 m12 l6 xl6 center-align" >
                    <button class="btn btn-small waves-effect waves-light #8e24aa purple darken-1" id="btn_otp">submit</button>
                </div>
            </div>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

   
   @include('user.bottom_x')

    <?php

   
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        $trans_id = $op1['trans_id'];
        $rep_trans_id = $op1['rep_trans_id'];
        $otp_status = $op1['otp_status'];
        $remarks = $op1['remarks'];
        //$ben_acc_no = $op1['ben_acc_no'];
        if($op == 0)
        {
            if($otp_status == "1")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    $('#trans_id').val('".$trans_id."');
                    $('#rep_trans_id').val('".$rep_trans_id."');

                    $('#tr_id').text('".$trans_id."');
                    $('#rep_tr_id').text('".$rep_trans_id."');

                    $('.modal').modal();
                    $('.modal').modal('open');
                });
                </script>";
            }
            else if($otp_status == "0")
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    window.location.href = '".url('/'). "/money_user_check';
                });
                </script>";
            }
            
        }
        else if($op == 10)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$remarks.". Transaction is ".$otp_status."', 'success');
                window.location.href = '".url('/'). "/money_user_check';
               
            });
            </script>";
        }
        else 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error');
               
            });
            </script>";
        }
        session()->forget('result');
    }
    ?>

    
   
    <script type="text/javascript">
        $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var x = 0;

            $(document).on('click', "button",function (e) 
            {
                e.preventDefault();
                var gid = this.id;
                
                if(gid.indexOf("btn_ben_") !== -1)
                {
                    var nid = gid.split("btn_ben_");
                    var i = nid[1];
                    //alert(i);

                    var b_id = $('#b_id_' + i).text();
                    var b_name = $('#b_name_' + i).text();
                    var b_accno = $('#b_accno_' + i).text();
                    var b_bank = $('#b_bank_' + i).text();
                    var b_bcode = $('#b_bcode_' + i).text();

                    //alert(b_id + "---" + b_name + "---" + b_accno + "---" + b_bank + "---" + b_bcode);

                    $('#ben_id').val(b_id);
                    $('#bank_code').val(b_bcode);
                    $('#cus_acc_no').val(b_accno);
                    $('#cus_name').val(b_name);

                    $('#bank_code').prop('disabled', true);
                    $('#cus_acc_no').prop('disabled', true);
                    $('#cus_name').prop('disabled', true);
                        

                    if((x == 0) && (x != i))
                    {
                        // First time
                        $('#ico_' + i).html("done_all");
                        $("#btn_ben_" + i).removeClass("btn-floating  waves-effect waves-light #8e24aa purple darken-1").addClass("btn-floating  waves-effect waves-light #00b0ff light-blue accent-3");
                        
                        x = i;
                    }
                    else if((x != 0) && (x != i))
                    {
                        $('#ico_' + i).html("done_all");
                        $("#btn_ben_" + i).removeClass("btn-floating  waves-effect waves-light #8e24aa purple darken-1").addClass("btn-floating  waves-effect waves-light #00b0ff light-blue accent-3");

                        // Remove old one
                        $('#ico_' + x).html("check");
                        $("#btn_ben_" + x).removeClass("btn-floating  waves-effect waves-light #00b0ff light-blue accent-3").addClass("btn-floating  waves-effect waves-light #8e24aa purple darken-1");

                        x = i;
                    }
                    else if((x != 0) && (x == i))
                    {
                        // Remove Everything
                        $('#ico_' + x).html("check");
                        $("#btn_ben_" + x).removeClass("btn-floating  waves-effect waves-light #00b0ff light-blue accent-3").addClass("btn-floating  waves-effect waves-light #8e24aa purple darken-1");
                        
                        $('#ben_id').val("");
                        $('#bank_code').prop('disabled', false);
                        $('#cus_acc_no').prop('disabled', false);
                        $('#cus_name').prop('disabled', false);
                        $('#bank_code').val("");
                        $('#cus_acc_no').val("");
                        $('#cus_name').val("");
                        x = 0;
                    }
                    

                    M.updateTextFields();
                    $('select').formSelect();

                }

                if(gid.indexOf("btn_submit") !== -1)
                {
                    $('#btn_submit').prop('disabled', true);

                    var b_bank = $('option:selected', '#bank_code').text();
                    var b_bcode = $('option:selected', '#bank_code').val();
                    var b_accno = $('#cus_acc_no').val();
                    var b_name = $('#cus_name').val();
                    var b_mode = $('option:selected', '#bank_mode').val();
                    var b_mode1 = $('option:selected', '#bank_mode').text();
                    var b_amnt = $('#cus_amt').val();

                    if(b_bcode == "") {
                        swal("Cancelled", "Please Select Bank", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                    else if(b_accno == "") {
                        swal("Cancelled", "Please Enter Account No", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                    else if(b_mode == "") {
                        swal("Cancelled", "Please Select Transfer Mode", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                    else if(b_amnt == "") {
                        swal("Cancelled", "Please Enter Amount", "error");
                        $('#btn_submit').prop('disabled', false);
                    }
                    else if(b_bcode != ""  && b_accno != "" && b_mode != "" && b_amnt != "" && b_name != "") 
                    {
                        swal({
                            title: 'Are you sure? <br>Bank : ' +b_bank + "<br>Acc.No : "+b_accno + "<br>Mode : " + b_mode1 + "<br> Amt : "+b_amnt,
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Transfer!'
                            }).then((result) => {
                            if (result.value) {
                                $('#loader').show();
                                $('#bank_code').prop('disabled', false);
                                $('#cus_acc_no').prop('disabled', false);
                                $('#cus_name').prop('disabled', false);
                                $('#rech_form').attr('action', "{{url('recharge_money')}}");
                                $('#rech_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "No Recharge...", "error");
                                $('#btn_submit').prop('disabled', false);
                            }
                        });
                    }
                   
                        

                   

                }
                
                if(gid.indexOf("btn_otp") !== -1)
                {
                    $('#btn_submit_1').prop('disabled', true);

                    
                    var otp = $('#cus_otp').val();
                    

                    if(otp == "") {
                        swal("Cancelled", "Please Enter OTP", "error");
                        $('#btn_submit_1').prop('disabled', false);
                    }
                    else if(otp != "") 
                    {
                        $('#otp').val(otp);
                        $('#loader').show();
                        $('#rech_form').attr('action', "{{url('recharge_money_otp')}}");
                        $('#rech_form').submit();
                    }
                   
                }

                if(gid.indexOf("btn_add") !== -1)
                {
                    window.open("{{url('ben_entry')}}", "_self");
                   
                }
                
            });
        
        });
    </script>
</body>
</html>