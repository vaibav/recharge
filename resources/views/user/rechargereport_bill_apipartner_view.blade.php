<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar_apipartner', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge EBBill Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargedetails_bill_apipartner')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:4px 4px;'>EB CONN NO</th>
                                      <th style='font-size:12px;padding:4px 4px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:4px 4px;'>EB DUE AMT</th>
                                      <th style='font-size:12px;padding:4px 4px;'>NET(%)/SURP</th>
                                      <th style='font-size:12px;padding:4px 4px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:4px 4px;'>TRN.ID</th>  
                                      <th style='font-size:12px;padding:4px 4px;'>API.ID</th>                                 
                                      <th style='font-size:12px;padding:4px 4px;'>OPR.ID</th>
                                      <th style='font-size:12px;padding:4px 4px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:4px 4px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:4px 4px;'>STATUS</th>
                                      <th style='font-size:12px;padding:4px 4px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:4px 4px;text-align:right;'>C.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>BILL</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                       $str = "";
                                       foreach($recharge as $d)
                                       {
                                           
                                                                                       
                                          $net_name = "";
                                          foreach($network as $r)
                                          {
                                              if($d->net_code == $r->net_code)
                                                  $net_name = $r->net_name;
                                          }

                                          $api_name = "";
                                          foreach($api as $r)
                                          {
                                              if($d->api_code == $r->api_code)
                                                  $api_name = $r->api_name;
                                          }

                                          $rech_status = "";
                                          $status = "";
                                          $o_bal = 0;
                                          $u_bal = 0;
                                          $r_tot = 0;

                                          $rech_option = $d->con_option;
                                          $rech_status = $d->con_status;
                                          $r_tot = $d->con_total;
                                          $u_bal = $d->user_balance;

                                          if($rech_option == 1 && ($rech_status == "PENDING" || $rech_status == "SUCCESS"))
                                          {
                                              $str = $str."<tr>";
                                             
                                          }
                                          else if($rech_option == 2 && $rech_status == "PENDING")
                                          {
                                              $str = $str."<tr>";
                                             
                                          }
                                          else if($rech_option == 2 && $rech_status == "FAILURE")
                                          {  
                                              $str = $str."<tr style='background-color:#E8DAEF;'>";
                                              
                                          }

                                          if($rech_status == "PENDING" && $rech_option == 1)
                                          {
                                              $status = "<span class='badge blue white-text text-darken-1' style='font-size:12px;border-radius:5px;'>PENDING</span>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if($rech_status == "PENDING" && $rech_option == 2)
                                          {
                                              $status = "<span class='badge red white-text text-darken-1' style='font-size:12px;border-radius:5px;'>FAILURE</span>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if($rech_status == "FAILURE"  && $rech_option == 2)
                                          {      
                                              $status = "<span class='badge red white-text text-darken-1' style='font-size:12px;border-radius:5px;'>FAILURE</span>";
                                              //$o_bal = floatval($u_bal) - floatval($r_tot);
                                              $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                              //$u_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if ($rech_status == "SUCCESS")
                                          {
                                              $status = "<span class='badge green white-text text-darken-1' style='font-size:12px;border-radius:5px;'>SUCCESS</span>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          
                          
                                          $str = $str."<td style='font-size:11px;padding:4px 4px;'>".$j."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_acc_no."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$net_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_amount."</td>";
                                          if($rech_option == 2 && $rech_status == "FAILURE")
                                          {
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>&nbsp;</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".$d->con_total."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->api_trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_opr_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'></td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'></td>";
                                          }
                                          else  
                                          {
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_net_per;
                                              $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->con_net_per_amt."";
                                              $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->con_net_surp."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".$d->con_total."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->api_trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_opr_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->created_at."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_date."</td>";
                                          }
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$status."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";

                                          $str = $str. "<td  style='font-size:11px;padding:7px 8px;text-align:center;'>";
                                          $str = $str. "<button class='btn-floating btn-sm' id='bill_".$d->trans_id."'>
                                          <i class='material-icons right'>show_chart</i></button></td>";
                                          $str = $str."</tr>";
                                                                                 
                                          $j++;
                                       }
                                       
                                       echo $str;
                                       

                                      
                                    ?>

                            </tbody>
                        </table>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
                <h4>Info</h4>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    @include('user.bottom1')

   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#tbl_body').delegate('button', 'click', function(e) {
                      e.preventDefault();
                      var gid=this.id;
                      var nid=gid.split("bill_");
                      if(nid.length>1)
                      {
                          swal({
                              title: 'Are you sure?',
                              text: "Confirmation Alert",
                              type: 'warning',
                              showCancelButton: true,
                              confirmButtonColor: '#3085d6',
                              cancelButtonColor: '#d33',
                              confirmButtonText: 'Yes, Get Bill!'
                              }).then((result) => {
                              if (result.value) {
                                  var code = nid[1];
                                  window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                              }
                              else
                              {
                                  swal("Cancelled", "Sorry...", "error");
                              }
                          });
                        
                      }

                });
            
           
      });
    </script>
    </body>
</html>
