<!-- Topbar -->
<nav class="navbar fixed-top navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarCollapse" class="btn btn-link  rounded-circle mr-3">
        <i class="fa fa-bars"></i> <font style = "font-size: 12px">VaibavOnline</font>
    </button>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item text-gray-800 font-weight-bold" style="margin: 10px 25px;">
                <a href="#" > <i class="fas fa-rupee-sign"></i> {{$user->ubal}} </a>
        </li>

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow ">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small">Hi {{$user->user}}!</span>
            <img class="img-profile rounded-circle" src="{{ asset('uploadphoto/user.jpg') }}">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow " aria-labelledby="userDropdown" style="border:0px solid silver;">
            <a class="dropdown-item" href="{{url('changepassword')}}">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-300 shadow"></i>
                <font style = "font-size: 12px;">Change Password</font>
            </a>
            
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{url('logout')}}" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-300 shadow"></i>
                <font style = "font-size: 12px;">Logout</font>
            </a>
        </div>
        </li>

    </ul>

</nav>
<!-- End of Topbar -->