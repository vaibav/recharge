<div class="sidebar" data-toggle="sidebar-hide">
        <nav class="sidebar-nav">
          <ul class="nav">
           
            
            <?php
                if($user->mode == "SUPER DISTRIBUTOR")
                {
            ?>

            <li class="nav-item">
                <a class="nav-link" href="{{url('dashboard_user')}}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
           
           
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_user')}}">
                            <i class="nav-icon icon-anchor"></i> User Creation Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_view_user')}}">
                            <i class="nav-icon icon-anchor"></i> User Details</a>
                        </li>
                       
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('remotepayment_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Remote Payment </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentrequest_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Request </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentaccept_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Accept </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Stock Details </a>
                        </li>
                        
                    </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_rechargedetails_sdistributor')}}">
                            <i class="nav-icon icon-anchor"></i> Recharge Details</a>
                        </li>
                        
                    </ul>
            </li> 

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Complaint</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_distributor')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_view_distributor')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Details</a>
                        </li>
                       
                    </ul>
            </li>
           
            <?php
                }
                if($user->mode == "DISTRIBUTOR")
                {
            ?>

             <li class="nav-item">
                <a class="nav-link" href="{{url('dashboard_distributor')}}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_user')}}">
                            <i class="nav-icon icon-anchor"></i> User Creation Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_view_user')}}">
                            <i class="nav-icon icon-anchor"></i> User Details</a>
                        </li>
                       
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('remotepayment_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Remote Payment </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentrequest_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Request </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentaccept_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Accept </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails_distributor')}}">
                            <i class="nav-icon icon-action-undo"></i> Stock Details </a>
                        </li>
                        
                    </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_rechargedetails_distributor')}}">
                            <i class="nav-icon icon-anchor"></i> Recharge Details</a>
                        </li>
                       
                    </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Complaint</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_distributor')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_view_distributor')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Details</a>
                        </li>
                       
                    </ul>
            </li>

            <?php
                }
                if($user->mode == "RETAILER" )
                {
            ?>

             <li class="nav-item">
                <a class="nav-link" href="{{url('dashboard_retailer')}}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>
           
           
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_one')}}">
                            <i class="nav-icon icon-anchor"></i> User Details</a>
                        </li>
                       <li class="nav-item">
                        <a class="nav-link" href="{{url('surplus_retailer')}}">
                            <i class="nav-icon icon-anchor"></i> Network Surplus Details</a>
                        </li>
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentrequest_retailer')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Request </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails_retailer')}}">
                            <i class="nav-icon icon-action-undo"></i> Stock Details </a>
                        </li>
                        
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Money Transfer</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('bank_remitter_view')}}">
                            <i class="nav-icon icon-action-undo"></i> Remitter </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('ben_view')}}">
                            <i class="nav-icon icon-action-undo"></i> Beneficiary </a>
                        </li>
                        
                        
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargedetails_retailer')}}">
                            <i class="nav-icon icon-anchor"></i> Recharge Details</a>
                        </li>
                       <li class="nav-item">
                        <a class="nav-link" href="{{url('recharge_bill_retailer')}}">
                            <i class="nav-icon icon-anchor"></i> EBBill Details</a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Complaint</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_retailer')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_view_retailer')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Details</a>
                        </li>
                       
                    </ul>
            </li>
            <?php
                }
                if($user->mode == "API PARTNER" )
                {
            ?>
            
            <li class="nav-item">
                <a class="nav-link" href="{{url('dashboard_apipartner')}}">
                        <i class="nav-icon icon-speedometer"></i> Dashboard
                </a>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_one_api')}}">
                            <i class="nav-icon icon-anchor"></i> User Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('surplus_apipartner')}}">
                            <i class="nav-icon icon-anchor"></i> Network Surplus Details</a>
                        </li>
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>IP Address</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('apipartner_ipentry')}}">
                            <i class="nav-icon icon-anchor"></i> Entry</a>
                        </li>
                       
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentrequest_apipartner')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Request </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails_apipartner')}}">
                            <i class="nav-icon icon-action-undo"></i> Stock Details </a>
                        </li>
                        
                    </ul>
            </li>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargedetails_apipartner')}}">
                            <i class="nav-icon icon-anchor"></i> Recharge Details</a>
                        </li>
                       <li class="nav-item">
                        <a class="nav-link" href="{{url('recharge_bill_apipartner')}}">
                            <i class="nav-icon icon-anchor"></i> EBBill Details</a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Complaint</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_apipartner')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_view_apipartner')}}">
                            <i class="nav-icon icon-anchor"></i> Complaint Details</a>
                        </li>
                       
                    </ul>
            </li>
            <?php
                }
               
            ?>
          </ul>
        </nav>
        
      </div>