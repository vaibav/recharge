<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        <?php 
            if($user->mode == "RETAILER") {
            ?>
                @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_retailer')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            else if($user->mode == "SUPER DISTRIBUTOR") {
            
            ?>
                @include('user.sidebar_sdistributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_user')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            else if($user->mode == "DISTRIBUTOR" ) {
                ?>
                @include('user.sidebar_distributor', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_distributor')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
            else if($user->mode == "API PARTNER") {
                ?>
                @include('user.sidebar_apipartner', array('bal' => $user->ubal, 'uname' => $user->user));
            <?php
                
                echo "<div class='fixed-action-btn'><a class='btn-floating btn-large red' href = '".url('dashboard_apipartner')."'>
                        <i class='large material-icons'>home</i></a>          
                        </div> ";
            }
        ?>
       

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Complaint Details</span>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Rech Amt</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Complaint</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Admin Reply</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Reply Date</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                        
                                        $j = 1;
                                        foreach($comp1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_complaint."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->admin_reply."</td>";
                                          if($f->reply_status == 1)
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                          else if($f->reply_status == 2)
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_date."</td>";
                                          echo "</tr>";
                                          $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Complaint is Updated Successfully...', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

             $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("Save_");
                if(nid.length>1)
                {
                    var code = nid[1];
                    var amt = $("#Amt_" + code).val();
                    if(amt != "")
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Update!'
                            }).then((result) => {
                            if (result.value) {
                                
                                //alert(code + "---" + amt);
                                window.location.href = "<?php echo url('/'); ?>/complaint_update/" + code + "/" + amt;
                            }
                            else
                            {
                                swal("Cancelled", "No Update...", "error");
                                
                            }
                        });
                    }
                    else
                    {
                        swal("Cancelled", "Reply is Empty...", "error");
                    }
                    
                }
                
            });

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
      });
    </script>
    </body>
</html>
