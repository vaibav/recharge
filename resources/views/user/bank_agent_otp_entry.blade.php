<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Bank Agent OTP Entry</span>
                    <a class='btn-floating halfway-fab waves-effect waves-light red' id='btn_back'><i class='material-icons'>arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <?php
                    if($bank->count() > 0)
                    {
                        $agent_msisdn = $bank[0]->agent_msisdn;
                        $trans_id = $bank[0]->agent_trans_id;
                    }
                    else
                    {
                        $agent_msisdn = "9999999999";
                        $trans_id = "122324";
                    }
                ?>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                            <form  id = "rech_form" class="form-horizontal" action="{{url('bank_agent_otp_entry_store')}}" method="post" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="agent_msisdn" id="agent_msisdn" value="{{ $agent_msisdn }}">
                                <input type="hidden" name="trans_id" id="trans_id" value="{{ $trans_id }}">

                               
                                <div class="row" style="margin-bottom: 2px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <input id="otp" name = "otp" type="text" class="validate">
                                        <span class="text-danger">{{ $errors->first('otp') }}</span>
                                        <label for="otp">OTP</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin-top:4px;margin-bottom: 4px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin-top:4px;margin-bottom: 4px;">
                                        <button class="btn waves-effect waves-light green darken-2" type="submit" name="action" id="btn_otp">Resend OTP
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>

                            
                                
                            
                                

                                <div class="row" style="margin-bottom: 8px;">
                                    <div class="col s12 m12 l12 xl12 left-align" style="margin-top:4px;margin-bottom: 4px;margin-left:60px;">
                                        <button class="btn waves-effect waves-light " type="submit" name="action" id="btn_submit">Submit
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </div>
                        </form>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                    window.location.href = '".url('/'). "/bank_agent_view';
                });
                </script>";
            }
            else if($op == 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success');
                });
                </script>";
            }
            else if($op > 10)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error');
                    window.location.href = '".url('/'). "//bank_agent_view';
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#btn_back').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('bank_agent_view')}}", "_self");
            });
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Add!'
                        }).then((result) => {
                        if (result.value) {
                            //$('#loader').show();
                            $('#rech_form').attr('action', "{{url('bank_agent_otp_entry_store')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                   
            }); 

            $("#otp").keydown(function (e) 
            {
                numbersExactly(e);
            });

            $('#btn_otp').on('click',function(e)
            {
                e.preventDefault();
                $('#btn_submit').prop('disabled', true);
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Resend!'
                        }).then((result) => {
                        if (result.value) {
                            //$('#loader').show();
                            $('#rech_form').attr('action', "{{url('bank_agent_otp_resend')}}");
                            $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                   
            }); 

            function numbersExactly(e)
            {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                    // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) 
                {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            }
            
           
      });
    </script>
    </body>
</html>
