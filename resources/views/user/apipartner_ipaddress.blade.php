<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style = "background-color:#6D4C41;">
      @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'IP Address Entry', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
          
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> IP Address Entry</div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                         
                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <form class="form-horizontal" action="{{url('apipartner_ipentry_store')}}" method="POST" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  
                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Enter IP Address </label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="id_api_ip" type="text" name="api_ip" placeholder="000.000.000.000">
                                      <span class="text-danger">{{ $errors->first('api_ip') }}</span>
                                    </div>
                                  </div>

                                                                     
                                  <div class="form-group row" style="padding:6px 15px;">
                                      <button class="btn btn-sm btn-primary" type="submit">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>
                                  </div>

                                  <div class="form-group row" style="padding:6px 15px;">
                                      <label style="color:orange;font-weight:bold;font-size:16px;" >
                                      <?php
                                          if(session()->has('result'))
                                          {
                                            $op = session('result');
                                            echo $op['output'];
                                          }
                                        ?>
                                      </label>
                                         
                                  </div>

                                </form>
                          </div>
                      </div>
                      
                    </div>

                     

                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	 {
          
               
      });
    </script>
    </body>
</html>
