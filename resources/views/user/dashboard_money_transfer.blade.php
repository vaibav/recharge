<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
    </head>
     
    <body style = "background-color: #34495e;">

       <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" id = "btn_home" href = "#">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        

        
       
        
        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row" >
                <div class="col s12 m12 l12 x12 center-align" >
                    <h4 class="white-text">Vaibav Online</h4>
                    <h5 class="white-text">Hello {{$rem_name}}! Welcome To Money Transfer!</h5>
                </div>
        </div>
        <br><br><br><br><br>
        <div class="row" >
            <div class="col s12 m12 l2 x2 " ></div>
            <div class="col s12 m12 l4 xl4 " style="margin-bottom: 50px;">
              
                    <div class="card">
                        <div class="card-image">
                            
                            <span class="card-title">Beneficiary</span>
                            <a class="btn-floating halfway-fab waves-effect waves-light red" id='btn_ben_add'><i class="material-icons">add</i></a>
                        </div>
                        <div class="card-content">
                            <button class="btn waves-effect waves-light green darken-2" id="btn_ben_view">Details
                                                    
                            </button>
                        </div>
                    </div>
                
           
            </div>

            <div class="col s12 m12 l4 xl4 ">
               
               
                    <div class="card">
                            <div class="card-image">
                                
                                <span class="card-title">Transfer</span>
                                <a class="btn-floating halfway-fab waves-effect waves-light red" id='btn_money_add'><i class="material-icons">add</i></a>
                            </div>
                            <div class="card-content">
                                <button class="btn waves-effect waves-light green darken-2" id="btn_money_view">Details
                                                    
                                </button>
                               

                            </div>
                        </div>
                
           
            </div>
            <div class="col s12 m12 l2 x2 " ></div>
        </div>
        
        <!-- End Page Layout  -->

       

    <!--JavaScript at end of body for optimized loading-->
    @include('user.bottom1')

     

    <!-- script -->
    <script>
    $(document).ready(function() 
    {
        $(".dropdown-trigger").dropdown();
        $('.sidenav').sidenav();
        $('select').formSelect();

        $('#loader').hide();
    
        
        

        $('#btn_ben_add').on('click',function(e)
        {
            e.preventDefault();
            window.open("{{url('ben_entry')}}", "_self");
        });

        $('#btn_ben_view').on('click',function(e)
        {
            e.preventDefault();
            window.open("{{url('ben_view')}}", "_self");
        });

        $('#btn_home').on('click',function(e)
        {
            e.preventDefault();
            swal({
                title: 'Are you sure to logout this customer?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Logout Customer!'
                }).then((result) => {
                if (result.value) {
                    window.open("{{url('dashboard_retailer_new')}}", "_self");
                    
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        });

        
    });
    </script>

    </body>
  </html>