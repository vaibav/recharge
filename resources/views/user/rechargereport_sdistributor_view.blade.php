<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style ='background-color: #0E6655;'>
      @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Recharge Details', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                        <div class = "row">
                            <div class ="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <i class="fa fa-align-justify"></i> Recharge Details ---From-Date : <?php echo $from_date; ?> To-Date : <?php echo $to_date; ?> 
                            </div>
                            <div class ="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">
                                <input type = "text" class = "form-control" id= "icd" placeholder="search">
                                
                            </div>
                            <div class ="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right">
                                
                                <a href="{{ url('user_rechargedetails_sdistributor')}}">  <button class="btn btn-sm btn-primary" >
                                          <i class="fa fa-arrow-left"></i>Back</button></a>
                            </div>
                        </div>

                    
                    
                    </div>
    
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                         
                          <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <table class="table table-responsive-sm table-bordered" id="tbl">
                              <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>PARENT NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET.PER(%) / SURPLUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR. TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>MODE</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body">
                                  <?php 
                                        $str = "";
                                        $j = 1;
                                        $v = 0;
                                        //print_r(json_encode($recharge, true));
  
                                        //$data = $recharge1->data;
                                        foreach($recharge as $d1)
                                        {
                                           
                                            $d = $d1[0];
                                            $v = 0;
                                            if($d->trans_type == "WEB_RECHARGE" )
                                            {
                                                $net_name = "";

                                                


                                                foreach($network as $r)
                                                {
                                                    if($d->websuperparentrecharge2->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }
                                
                                
                                                $rech_status = "";
                                                $status = "";
                                                $o_bal = 0;
                                                $u_bal = 0;
                                                $r_tot = 0;
                                
                                                
                                                $rech_status1 = "NONE";
  
                                                
                                                if($d->trans_option == 1)
                                                {
                                                      $rech_status = $d->websuperparentrecharge1[0]->rech_status;

                                                     

                                                      $u_bal = $d->websuperparentrecharge1[0]->user_balance;
                                                      $r_tot = $d->websuperparentrecharge1[0]->rech_total;
                                                      $str = $str."<tr>";
  
                                                      if($rech_status == "PENDING" && sizeof($d->websuperparentrecharge1) == 1)
                                                      {
                                                          $status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                                                          
                                                         // $v = 1;
                                                      }
                                                      else if($rech_status == "PENDING" && sizeof($d->websuperparentrecharge1) > 1)
                                                      {
                                                          $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                          
                                                      }
                                                      else if ($rech_status == "SUCCESS")
                                                      {
                                                          $status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                                            
                                                          //$v = 1;
                                                      }


                                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($d->trans_option == 2)
                                                {
                                                   
                                                      $rech_status = $d->websuperparentrecharge1[0]->rech_status;
                                                      
                                                      $u_bal = $d->websuperparentrecharge1[0]->user_balance;
                                                      $r_tot = $d->websuperparentrecharge1[0]->rech_total;
                                                      $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                      $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                      $o_bal = floatval($u_bal) - floatval($r_tot);
                                                }
                                                
                                                $mode = "WEB";
                                                if (strpos($d->trans_id, 'VBG') !== false) {
                                                    $mode = "GPRS";
                                                }
                                                
                                                if($v == 0)
                                                {
                                                      $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge1[0]->parent_name."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->user_name."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->rech_mobile."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->rech_amount."</td>";
                                                      if($d->trans_option == 1)
                                                      {
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge1[0]->rech_net_per;
                                                          $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->websuperparentrecharge1[0]->rech_net_per_amt."";
                                                          $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->websuperparentrecharge1[0]->rech_net_surp."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->websuperparentrecharge1[0]->rech_total."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->reply_opr_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->rech_date."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentrecharge2->reply_date."</td>";
                                                      }
                                                      else if($d->trans_option == 2)
                                                      {
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->websuperparentrecharge1[0]->rech_total."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                      }
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$mode."</td>";
                                                      $j++;
                                                }
                                
                                                
                                            }


                                            $v = 0;
                                            if($d->trans_type == "API_RECHARGE" )
                                            {
                                                // checking user name
                                                

                                                $net_name = "";
                                                foreach($network as $r)
                                                {
                                                    if($d->websuperparentapirecharge2->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }
                                
                                
                                                $rech_status = "";
                                                $status = "";
                                                $o_bal = 0;
                                                $u_bal = 0;
                                                $r_tot = 0;
                                
                                                
                                                $rech_status1 = "NONE";
  
                                                
                                                if($d->trans_option == 1)
                                                {
                                                      $rech_status = $d->websuperparentapirecharge1[0]->rech_status;
                                                      
                                                      $u_bal = $d->websuperparentapirecharge1[0]->user_balance;
                                                      $r_tot = $d->websuperparentapirecharge1[0]->rech_total;
                                                      $str = $str."<tr>";
  
                                                      if($rech_status == "PENDING" && sizeof($d->websuperparentapirecharge1) == 1)
                                                      {
                                                          $status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                                                          //$v = 1;
                                                      }
                                                      else if($rech_status == "PENDING" && sizeof($d->websuperparentapirecharge1) > 1)
                                                      {
                                                          $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                      }
                                                      else if ($rech_status == "SUCCESS")
                                                      {
                                                          $status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                                                          //$v = 1;
                                                      }
                                                      $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($d->trans_option == 2)
                                                {
                                                      $rech_status = $d->websuperparentapirecharge1[1]->rech_status;
                                                      
                                                      $u_bal = $d->websuperparentapirecharge1[1]->user_balance;
                                                      $r_tot = $d->websuperparentapirecharge1[1]->rech_total;
                                                      $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                      $status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                      $o_bal = floatval($u_bal) - floatval($r_tot);
                                                }
                                                
                                               
                                                
                                                if($v == 0)
                                                {
                                                      $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->user_name."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->rech_mobile."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->rech_amount."</td>";
                                                      if($d->trans_option == 1)
                                                      {
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge1[0]->rech_net_per;
                                                          $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->websuperparentapirecharge1[0]->rech_net_per_amt."";
                                                          $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->websuperparentapirecharge1[0]->rech_net_surp."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->websuperparentapirecharge1[0]->rech_total."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->reply_opr_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->rech_date."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->websuperparentapirecharge2->reply_date."</td>";
                                                      }
                                                      else if($d->trans_option == 2)
                                                      {
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->websuperparentapirecharge1[0]->rech_total."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                      }
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                      $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>API</td>";
                                                      $j++;
                                                }
                                
                                                
                                            }
                                                                                 
                                           $str = $str."</tr>"; 
                                
                                            
                                            
                                           
                                                                                    
                                            
                                        }
                                       
                                        echo $str;
                                      
                                    ?>
                                    
                                  </tbody>
                                </table>
                                {{ $recharge->links('pagination::bootstrap-4') }}
                          </div>
                      </div>
                     
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
			{
                $('#icd').keyup(function(e) 
                {
                    var val=$(this).val().toLowerCase();    
                    $('#tbl tbody tr').hide();
                    var trs=$('#tbl tbody tr').filter(function(d)
                    {
                        return $(this).text().toLowerCase().indexOf(val)!=-1;
                    });
                    trs.show();
                });
      });
    </script>
    </body>
</html>
