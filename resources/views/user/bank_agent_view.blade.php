<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('user.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_retailer')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Bank Agent</span>
                    <?php 
                            if($status == "SUCCESS")
                            {
                                echo "<a class='btn-floating halfway-fab waves-effect waves-light red disabled' id='btn_add'><i class='material-icons'>add</i></a>";
                            }
                            else
                            {
                                echo "<a class='btn-floating halfway-fab waves-effect waves-light red' id='btn_add'><i class='material-icons'>add</i></a>";
                                
                            }
                        ?>  
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Agent Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Company</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Address</th>
                                      <th style='font-size:12px;padding:7px 8px;'>city</th>
                                      <th style='font-size:12px;padding:7px 8px;'>State</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Pincode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Trans ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Reply ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RESEND OTP</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        
                                        $j = 1;
                                        foreach($data as $f)
                                        {
                                            $state = "OTHER STATE";
                                            if($f->agent_state_code == 6)
                                                $state = "TAMIL NADU";

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_msisdn."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_name."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_cname."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_address."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_city."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$state."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_pincode."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_status."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_trans_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->agent_reply_id."</td>";
                                            if($f->agent_status == "OTP SUCCESS")
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;text-align:center'>";
                                                echo "<button class='btn-floating btn-sm blue' id='resend_".$f->agent_msisdn."'>
                                                <i class='small material-icons'>arrow_forward</i></td>";
                                            }
                                            else
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                            }

                                            echo "</tr>";
                                          
                                            $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                        
                       


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('user.bottom1')

   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#btn_add').on('click',function(e)
            {
                e.preventDefault();
                window.open("{{url('bank_agent_entry')}}", "_self");
            });
            
            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("resend_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure to View?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Resend OTP!'
                        }).then((result) => {
                        if (result.value) {
                          var code = nid[1];
                          window.location.href = "<?php echo url('/'); ?>/bank_agent_otp_entry";
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });

                   
                }
                
            });
      });
    </script>
    </body>
</html>
