<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google" content="notranslate">
    <title>VaibavOnline</title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto&display=swap" rel="stylesheet">

    <style type="text/css">
        .shadow { box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); }
        .card1 { word-wrap: break-word; background-color: #fff; background-clip: border-box; border: 1px solid #e3e6f0; border-radius: .35rem; }

        .input-field input[type=text] {
            background-color: transparent; border: 1px solid #E3B2F7; border-radius: 25px; outline: none; height: 2.5rem; 
            width: 100%; font-size: 15px; margin: 0 0 8px 0; padding: 0; padding-left: 15px; line-height: 1.05;
            -webkit-box-shadow: none; box-shadow: none; -webkit-box-sizing: inherit; box-sizing: inherit;
            -webkit-transition: border .3s, -webkit-box-shadow .3s; transition: none;
        }
         
        .input-field input:focus + label { color: #5D6D7E; }
        .row .input-field input:focus { border: 0.5px solid #5D6D7E; box-shadow: 1px 1px 1px #5D6D7E; }
        .input-field input[type=text].valid { border: 1px solid #e3e6f0; box-shadow: 0 1px 0 0 #e3e6f0; }
        .input-field>label:not(.label-icon).active {
            -webkit-transform: translateY(-24px) scale(0.8);
            transform: translateY(-24px) scale(1);
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
        }
        .input-field.col label { left: 1.6rem; }
        .title-con { font-family: 'Open Sans', sans-serif; font-size: 17px; font-weight: 600; margin: 1px 7px; color: #8E44AD; }
        .title-subcon { font-family: 'Open Sans', sans-serif; font-size: 14px; font-weight: 300; margin: 5px 7px; }
        .h-80 { height: 80px; }
        .m-36 { min-height: 360px; }
        .wrap-con {margin: 10px 10px;}
        .PT {font-family: 'Roboto', sans-serif;}
        .p-type {padding: 1px 2px; margin: 1px 2px;font-size: 14px;}
  
    </style>
   