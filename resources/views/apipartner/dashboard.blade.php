@extends('layouts.apipartner_new')
@section('styles')
<link href="{{asset('css/chat_admin.css')}}" rel="stylesheet">
<style>
    .table td, .table th {
    padding: .15rem !important;
   
  }
  .table td {
    font-size:12px !important;
  }
  .table th {
    font-size:14px !important;
  }
    .chart-container { height:200px !important; min-height:200px !important; }
    canvas{ height:200px !important; }
</style>
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white fw-bold" style = "margin-top:15px !important; margin-bottom:10px !important;">Dashboard </h2>
                <p><span class="text-white op-7 " style = "margin-top:10px !important; margin-bottom:10px !important;" id ="tr_1">ApiPartner Dashboard</span>
                <span id="tim" style ="font-size:12px !important;"> &nbsp;&nbsp;{{date('Y-m-d H:i:s')}}</span></p>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="#" class="btn btn-white btn-border btn-round mr-2" style = "padding:1px 5px;border:0 !important;">
                    <div class = "row">
                        <div class ="col-md-5 text-left">
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="re_1">Mobile Recharge: 0</p>
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="me_1">Money Transfer : 0 </p>
                        </div>
                        <div class ="col-md-4 text-left">
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="re_2">Success: 0 </p>
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="me_2"> Success: 0 </p>
                        </div>
                        <div class ="col-md-3">
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="re_3"> Failure: 0 </p>
                            <p style="margin:1px 2px;font-weight:430;font-size:14px;" id ="me_3"> Failure: 0  </p>
                        </div>
                    </div>
                    
                </a>
            </div>
        </div>
    </div>
</div>
<div class="page-inner mt--5">
    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card" style = "margin-bottom:15px;">
                <div class="card-body" style = "padding:5px 5px !important;">
                    <div class = "row">
                        <div class ="col-md-6">
                            <div class="chart-container" id ="chartLine"> 
                                <canvas id="multipleLineChart" ></canvas>
                            </div>
                        </div>
                        <div class ="col-md-6">
                            <div class="chart-container">
                                <canvas id="multipleBarChart"></canvas>
                            </div>
                        </div>
                    <div>
                    
                </div>
            </div>
        </div>
    </div>


    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <th>No</th>
                        <th>Mobile</th>
                        <th>Network</th>
                        <th>Amount</th>
                        <th>Net(%)</th>
                        <th>C.Amt</th>
                        <th>Trans.Id</th>
                        <th>Api Tr.Id</th>
                        <th>Reply</th>
                        <th>R.Date</th>
                        <th>Up.Date</th>
                        <th>Status</th>
                        <th>O.Bal</th>
                        <th>C.bal</th>
                        </thead>
                        <tbody id = "tbl_body">
                            <!-- Dynamic code here --> 
                            @php
                                echo $recharge;
                            @endphp
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--End page -->
</div>


<!-- Chatbox --> 
<div class="container" id = "chat1" style="display: none;">
    <div class="row no-gutters">
        <div class="col-md-4 border-right" >
        <div class="settings-tray">
            <div class="row">
                <div class="col-md-8">
                    <label style = "color:white !important;">VAIBAVONLINE</label>
                </div>
                <div class="col-md-4 text-right">
                    <i class="material-icons">menu</i>
                </div>
                </div>
            
        </div>
        
        <div style="max-height: 400px;overflow-y: scroll;">
            <div class="friend-drawer friend-drawer--onhover">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>Robo Cop</h6>
                <p class="text-muted">Hey, you're arrested!</p>
            </div>
            <span class="time text-muted small">13:21</span>
            </div>
            <hr>
            <div class="friend-drawer friend-drawer--onhover">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>Optimus</h6>
                <p class="text-muted">Wanna grab a beer?</p>
            </div>
            <span class="time text-muted small">00:32</span>
            </div>
            <hr>
            <div class="friend-drawer friend-drawer--onhover ">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>Skynet</h6>
                <p class="text-muted">Seen that canned piece of s?</p>
            </div>
            <span class="time text-muted small">13:21</span>
            </div>
            <hr>
            <div class="friend-drawer friend-drawer--onhover">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>Termy</h6>
                <p class="text-muted">Im studying spanish...</p>
            </div>
            <span class="time text-muted small">13:21</span>
            </div>
            <hr>
            <div class="friend-drawer friend-drawer--onhover">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>Richard</h6>
                <p class="text-muted">I'm not sure...</p>
            </div>
            <span class="time text-muted small">13:21</span>
            </div>
            <hr>
            <div class="friend-drawer friend-drawer--onhover">
            <img class="profile-image" src="{{asset('img/avatars/2.jpg')}}" alt="">
            <div class="text">
                <h6>XXXXX</h6>
                <p class="text-muted">Hi, wanna see something?</p>
            </div>
            <span class="time text-muted small">13:21</span>
            </div>
        </div>
        
        </div>
        <div class="col-md-8 " >
        <div class="settings-tray">
            <div class="row">
                <div class="col-md-8">
                    <label style = "color:white !important;">VAIBAVONLINE</label>
                </div>
                <div class="col-md-4 text-right">
                    <i class="material-icons">arrow_back</i>
                </div>
                </div>
        </div>
        <div class="chat-panel" >
            <div style="max-height: 330px;overflow-y: scroll;">
                <div class="row no-gutters">
                    <div class="col-md-3">
                    <div class="chat-bubble chat-bubble--left">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3 offset-md-9">
                    <div class="chat-bubble chat-bubble--right">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3 offset-md-9">
                    <div class="chat-bubble chat-bubble--right">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3">
                    <div class="chat-bubble chat-bubble--left">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3">
                    <div class="chat-bubble chat-bubble--left">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3">
                    <div class="chat-bubble chat-bubble--left">
                        Hello dude!
                    </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-3 offset-md-9">
                    <div class="chat-bubble chat-bubble--right">
                        Hello dude!
                    </div>
                    </div>
                </div>
            </div>
            
            <div class="row" style = "padding:15px 15px;background-color:silver;margin:0 !important;">
                <div class="col-8" >
                    <input type="text" placeholder="Type your message here..." >
                </div>
                <div class="col-4" >
                <i class="material-icons">send</i>
                </div> 
    
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<a id="prime" class="fab"><i class="prime zmdi zmdi-comment-outline"></i></a>
<!-- End chatbox --> 

@section('scripts')
<script src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js"></script>
<!-- Chart Circle -->
<script src="{{asset('js/circles.min.js')}}"></script>
<script>
$(document).ready(function() {
  //alert("hellow");
  var c = 1;
  //load_data();
  load_data1()
  load_data2();
  startTime();

    $( '#prime' ).on( 'click',  function() {
        $( '#chat1' ).toggle('slow');
    });

  window.setInterval(function(){
    /// call your function here
    //load_data();
    load_data1();
    load_data2()
    }, 17500);

   function load_data()
    {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        jQuery.ajax({
            url:'adminresult_data',
            type: 'GET',
            success: function( data ){

                $('#tbl_body').html(data);
                var d = new Date();
                $('#tim').html(d.toLocaleString());
                if(c == 1)
                {
                  $('#tim').css("color", "brown");
                  c = 2;
                }
                else if(c == 2)
                {
                  $('#tim').css("color", "purple");
                  c = 1;
                }
            },
            error: function (xhr, b, c) {
                console.log("xhr=" + xhr + " b=" + b + " c=" + c);
            }
        });
    }

    function load_data1()
    {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        jQuery.ajax({
            url:'apipartner_rech_data',
            type: 'GET',
            dataType: "json",
            success: function( data ){
                $('#re_1').text("Mobile Recharge : "+ data['rec_tot']);
                $('#re_2').text("Success : "+ data['res_tot']);
                $('#re_3').text("Failure : "+ data['ref_tot']);
                $('#me_1').text("Money Transfer : "+ data['mec_tot']);
                $('#me_2').text("Success : "+ data['mes_tot']);
                $('#me_3').text("Failure : "+ data['mef_tot']);
                $('#tr_1').text("Total Tr : "+data['trn_tot']+" ... Success: "+data['trs_tot']+" ... Failure:"+ data['trf_tot']);
              
            
            },
            error: function (xhr, b, c) {
                console.log("xhr=" + xhr + " b=" + b + " c=" + c);
            }
        });
    }

    function load_data2()
    {
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
        jQuery.ajax({
            url:'apipartner_chart_data',
            type: 'GET',
            dataType: "json",
            success: function( data ){
                var res = data['res'];
                var ref = data['ref']
                //chart_data(res, ref);
                new_chart_data(res,ref)
            },
            error: function (xhr, b, c) {
                console.log("xhr=" + xhr + " b=" + b + " c=" + c);
            }
        });
    }

    function new_chart_data(res,ref)
    {
        $('#multipleLineChart').remove();
        $('#chartLine').append('<canvas id="multipleLineChart"></canvas>');
        canvas = document.querySelector('#multipleLineChart');
        ctx = canvas.getContext('2d');
        ctx.canvas.width = $('#chartLine').width(); // resize to parent width
        ctx.canvas.height = $('#chartLine').height(); // resize to parent height
        var x = canvas.width/2;
        var y = canvas.height/2;
        ctx.font = '10pt Verdana';
        ctx.textAlign = 'center';
        ctx.fillText('This text is centered on the canvas', x, y);

        var myChart = new Chart(document.getElementById("multipleLineChart"), {
            type: 'line',
            data: {
                labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                datasets: [{
                    label: "Success",
                    borderColor: "#59d05d",
                    pointBorderColor: "#FFF",
                    pointBackgroundColor: "#59d05d",
                    pointBorderWidth: 2,
                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 1,
                    pointRadius: 4,
                    backgroundColor: 'transparent',
                    fill: true,
                    borderWidth: 2,
                    data: res
                }, {
                    label: "Failure",
                    borderColor: "#f3545d",
                    pointBorderColor: "#FFF",
                    pointBackgroundColor: "#f3545d",
                    pointBorderWidth: 2,
                    pointHoverRadius: 4,
                    pointHoverBorderWidth: 1,
                    pointRadius: 4,
                    backgroundColor: 'transparent',
                    fill: true,
                    borderWidth: 2,
                    data: ref
                }]
            },
            options : {
                responsive: true, 
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                },
                tooltips: {
                    bodySpacing: 2,
                    mode:"nearest",
                    intersect: 0,
                    position:"nearest",
                    xPadding:5,
                    yPadding:5,
                    caretPadding:10
                },
                layout:{
                    padding:{left:5,right:5,top:5,bottom:5}
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            maxTicksLimit: 7,
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        //Barchart
        var myMultipleBarChart = new Chart(document.getElementById("multipleBarChart"), {
            type: 'bar',
            data: {
                labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                datasets : [{
                    label: "Success",
                    backgroundColor: '#59d05d',
                    borderColor: '#59d05d',
                    data: res,
                }, {
                    label: "Failure",
                    backgroundColor: '#f3545d',
                    borderColor: '#f3545d',
                    data: ref,
                }],
            },
            options: {
                responsive: true, 
                maintainAspectRatio: false,
                legend: {
                    position : 'top'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
    }

    function chart_data(res, ref)
    {

        new Chart(document.getElementById("myChart"), {
            type: 'line',
            data: {
                labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                datasets: [{ 
                    data: res,
                    label: "Success",
                    borderColor: "#68FF33",
                    strokeColor: "#ffffff",
                    fill: false,
                    pointHoverRadius: 10
                }, { 
                    data: ref,
                    label: "Failure",
                    borderColor: "#E74C3C",
                    strokeColor: "#ffffff",
                    fill: false,
                    pointHoverRadius: 10,
                }
                ]
            },
            options: {
                
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontColor: '#ffffff'
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontColor: '#ffffff'
                        }
                    }]
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        fontColor: '#ffffff'
                    }
                }
            }
        });
    }

    function startTime() {
        var today = new Date();
        var day = today.getDate();
        var mn = today.getMonth();
        var yr = today.getFullYear();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        var tim  = day + "-" + mn+"-"+ yr+ " " +h + ":" + m + ":" + s;
        $('#tim').html(tim);
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
});
</script>
@stop
@stop