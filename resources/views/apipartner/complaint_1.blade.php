<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Complaint Entry</span>
                    
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l12 x12">
                            <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="#" method="get" accept-charset="UTF-8">
                            

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_f_date" type="text" name="f_date" class="datepicker" >
                                        <span class="text-danger">{{ $errors->first('f_date') }}</span>
                                        <label for="id_f_date">From Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                    <input id="id_t_date" type="text" name="t_date" class="datepicker" >
                                        <span class="text-danger">{{ $errors->first('t_date') }}</span>
                                        <label for="id_t_date">To Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_mobile" type="text" name="user_mobile" class="validate">
                                        <label for="id_user_mobile">Mobile No</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                

                                

                                

                               

                               
                                                    
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s12 m12 l2 xl2">
                                           
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                        </div>
                        
                    </div>
                
               
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Complaint is registered Successfully...', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                $('#btn_submit').prop('disabled', true);
                
                var fd = $('#id_f_date').val();
                var td = $('#id_t_date').val();
                var um = $('#id_user_mobile').val();
                
                if(fd != "" && td != "")
                {
                    $('#rech_form').attr('action', "{{url('complaint_apipartner_recharge')}}");
                    $('#rech_form').submit();
                }
                else if(um != "")
                {
                    $('#rech_form').attr('action', "{{url('complaint_apipartner_recharge')}}");
                    $('#rech_form').submit();
                }
                else 
                {
                    swal("Cancelled", "Please Enter From Date & to Date Or Mobile No...", "error");
                }


            }); 

           
  
      });
    </script>
    </body>
</html>
