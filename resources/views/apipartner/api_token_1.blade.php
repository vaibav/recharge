<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">API Token Generation</span>
                    
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <div class="row" style="margin-bottom: 10px;">
                        <div class="col s12 m12 l12 x12">
                            <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('api_token_2')}}" method="POST" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_api_type" name="api_type">
                                            <option value="-" >---Select--</option>
                                            <option value="MONEY_TRANSFER">MONEY TRANSFER</option>
                                            <option value="RECHARGE">RECHARGE</option>
                                          </select>
                                          <label>API Type</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_api_ip_address" type="text" name="api_ip_address" class="validate">
                                        <label for="id_api_ip_address">IP Address</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

    
                                                    
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s12 m12 l2 xl2">
                                           
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                        </div>
                        
                    </div>
                
               
                
                </div>

                <!-- Page Body --> 
                <div class = "row" style = "margin:10px 10px;"> 
                    <div class ="col s12 m12 l12 xl12" style = "padding:10px 10px;">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                <th style='font-size:12px;padding:7px 8px;'>API Type</th>
                                <th style='font-size:12px;padding:7px 8px;'>IP Address</th>
                                <th style='font-size:12px;padding:7px 8px;'>Token</th>
                                <th style='font-size:12px;padding:7px 8px;'>Date</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                        
                                        $j = 1;
                                        foreach($token as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->api_type."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->api_ip_address."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->auth_token."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td>";
                                          echo "</tr>";
                                          $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'API Token :".$res."', 'success'); 
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Token is not created...', 'error'); 
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                $('#btn_submit').prop('disabled', true);
                
                var at = $('#id_api_type').val();
                var ip = $('#id_api_ip_address').val();
               
                
                if(at != "-" && ip != "")
                {
                    $('#rech_form').attr('action', "{{url('api_token_2')}}");
                    $('#rech_form').submit();
                }
                else if(at == "-")
                {
                    swal("Cancelled", "Please Select API Type...", "error");
                }
                else if(ip == "")
                {
                    swal("Cancelled", "Please Enter IP Address...", "error");
                }


            }); 

           
  
      });
    </script>
    </body>
</html>
