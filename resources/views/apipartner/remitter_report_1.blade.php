<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Remitter (customer) Report</span>
                    
                </div>


                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                      <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       

                        <!-- Main Content-->
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                            <!-- Form Starts-->
                            <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Remitter Id</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Address</th>
                                        <th style='font-size:12px;padding:7px 8px;'>City</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Pincode</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                        
                                        <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Reply Id</th>
                                
                                        
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                        <?php 
                                            
                                                
                                            $j = 1;
                                      
                                            foreach($rem as $f)
                                            {
                                                                                      
    
                                                echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_id."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_rem_name."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_address."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_city."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_pincode."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->msisdn."</td>";
                                                
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_status."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_rep_opr_id."</td>";
                                               
                                                    
                                                
                                                echo "</tr>";
                                                                                        
                                                $j++;
                                            }
    
                                            

                                            
                                            ?>

                                    </tbody>
                                </table>
                            
                                {{ $rem->links('vendor.pagination.materializecss') }}
                                        
                                    

                                <!-- End Form-->
                            </div>
                        </div>

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                


                     
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

           
      });
    </script>
    </body>
</html>
