<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user));
       
        <div class='fixed-action-btn'><a class='btn-floating btn-large red' href = "{{url('dashboard_apipartner')}}">
                <i class='large material-icons'>home</i></a>          
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">User Network Details : <?php echo $user_name; ?></span>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network Code</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network Percentage</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network Surplus Charge</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                              <?php 
                                        $j = 1;
                                        foreach($user1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_code."</td>";
                                          foreach($net_1 as $r)
                                          {
                                              if($f->net_code == $r->net_code)
                                              {
                                                echo "<td  style='font-size:12px;padding:7px 8px;'>".$r->net_name."</td>";
                                              }
                                          }
                                          
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_net_per."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_net_surp."</td></tr>"; 
                                          $j++;
                                        }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('common.bottom')

    
   

    <script>
     $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

            

           
      });
    </script>
    </body>
</html>