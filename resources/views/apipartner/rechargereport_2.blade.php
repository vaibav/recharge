<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Report</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargedetails_apipartner')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>


                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                      <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <div class="row" >
                    
                            <div class="col s12 m12 l4 xl4 " >
                                <div class = "card-content white darken-1" >
                                    <div class="col s9 m9 l9 xl9 left-align">
                                        <label class="title-con" style="font-size:14px;">Recharge Success Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Success Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Failure Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Failure Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Credit Total</label><br>
                                    </div>
                                    <div class="col s2 m2 l2 xl2 right-align" >
                                        <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;{{ $total['wsua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;{{ $total['wsut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "re_3">&#x20B9;{{ $total['wfua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "tr_1">&#x20B9;{{ $total['wfut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "tr_2">&#x20B9;{{ $total['wrea_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "tr_3">&#x20B9;{{ $total['wret_tot'] }}</label><br>
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m12 l4 xl4 " >
                                <div class = "card-content white darken-1" >
                                    <div class="col s9 m9 l9 xl9 left-align">
                                        <label class="title-con" style="font-size:14px;">TNEB Success Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">TNEB Success Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">TNEB Failure Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">TNEB Failure Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">TNEB Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">TNEB Credit Total</label><br>
                                    </div>
                                    <div class="col s2 m2 l2 xl2 right-align" >
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['erea_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['eret_tot'] }}</label><br>
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m12 l4 xl4 " >
                                <div class = "card-content white darken-1" >
                                    <div class="col s9 m9 l9 xl9 left-align">
                                        <label class="title-con" style="font-size:14px;">MONEY Success Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">MONEY Success Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">MONEY Failure Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">MONEY Failure Credit Total</label><br>
                                        <label class="title-con" style="font-size:14px;">MONEY Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">MONEY Credit Total</label><br>
                                    </div>
                                    <div class="col s2 m2 l2 xl2 right-align" >
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['msua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;">&#x20B9;{{ $total['msut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfut_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mrea_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mret_tot'] }}</label><br>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Main Content-->
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                            <!-- Form Starts-->
                            <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                        <th style="font-size:12px;padding:7px 8px;">NO</th>
                                        <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                        <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                        <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                        <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                        <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">API TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                        <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                        <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                        <?php 
                                            
                                                
                                                $str = "";
                                                
                                                $j = 1;
                                                foreach($recharge as $d)
                                                {
                                                
                                                    if($d->trans_type == "RECHARGE" || $d->trans_type == "BANK_TRANSFER" || $d->trans_type == "BILL_PAYMENT")
                                                    {
                                                        $net_name = "";
                                                        foreach($d2 as $r)
                                                        {
                                                            if($d->net_code == $r->net_code){
                                                                $net_name = $r->net_name;
                                                                break;
                                                            }
                                                        }

                                                        $reply_id = $d->reply_id;
                                                        $reply_date = $d->reply_date;

                                                        $status = "";
                                                        $o_bal = 0;

                                                        $rech_status = $d->rech_status;
                                                        $rech_option = $d->rech_option;
                                                        $u_bal = $d->ret_bal;
                                                        $r_tot = $d->ret_total;

                                                        if($rech_status == "PENDING" && $rech_option == "0")
                                                        {
                                                            $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                            $o_bal = floatval($u_bal) + floatval($r_tot);
                                                            $str = $str."<tr>";
                                                        }
                                                        else if($rech_status == "PENDING" && $rech_option == "2")
                                                        {
                                                            $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                            $o_bal = floatval($u_bal) + floatval($r_tot);
                                                            $str = $str."<tr>";
                                                        }
                                                        else if($rech_status == "FAILURE" && $rech_option == "2")
                                                        {      
                                                            $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                            $o_bal = floatval($u_bal) - floatval($r_tot);
                                                            $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                        }
                                                        else if ($rech_status == "SUCCESS")
                                                        {
                                                            $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                            $o_bal = floatval($u_bal) + floatval($r_tot);
                                                            $str = $str."<tr>";
                                                        }

                                                        //percentage amount
                                                        $per = floatval($d->ret_net_per);
                                                        $peramt1 = 0;
                                                        $peramt2 = 0;
                                                        if(floatval($per) != 0)
                                                        {
                                                            $peramt1 = floatval($per) / 100;
                                                            $peramt1 = round($peramt1, 4);
                                                    
                                                            $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                                            $peramt2 = round($peramt2, 2);
                                                        }

                                                        $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                                        if($rech_status == "SUCCESS" || $rech_status == "PENDING")
                                                        {
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                                            $str = $str."--".$peramt2."--".$d->ret_net_surp."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->api_trans_id."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                                        }
                                                        else if($rech_status == "FAILURE")
                                                        {
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->reT_total."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                        }
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                        
                                                        $str = $str. "</tr>";

                                                    }                                     
                                                
                                                                       
                                                    $j++;
                                                }
                                                
                                                echo $str; 
                                            

                                            
                                            ?>

                                    </tbody>
                                </table>
                            
                                {{ $recharge->links('vendor.pagination.materializecss') }}
                                        
                                    

                                <!-- End Form-->
                            </div>
                        </div>

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                


                     
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#tbl_body').delegate('button', 'click', function(e) {
                    e.preventDefault();
                    var gid=this.id;
                    var nid=gid.split("bill1_");
                    if(nid.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid[1];
                                window.open("<?php echo url('/'); ?>/rechargedetails_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

                    var nid1=gid.split("bill2_");
                    if(nid1.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid1[1];
                                window.open ( "<?php echo url('/'); ?>/rechargebill_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

                    var nid2=gid.split("bill3_");
                    if(nid2.length>1)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Get Bill!'
                            }).then((result) => {
                            if (result.value) {
                                var code = nid2[1];
                                window.open ( "<?php echo url('/'); ?>/rechargemoney_invoice/" + code);
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                            }
                        });
                    
                    }

            });
  
      });
    </script>
    </body>
</html>
