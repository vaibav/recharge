<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('apipartner.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_apipartner')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>


        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Report</span>
                    
                </div>


                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                    <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <form id="rech_form" class="form-horizontal" action="{{url('rechargedetails_apipartner_view')}}" method="get" accept-charset="UTF-8">
                            <input type="hidden" id ="web_code" name="web_code" value="{{ $web }}">
                            <input type="hidden" id ="bill_code" name="bill_code" value="{{ $bill }}">
                            <input type="hidden" id ="money_code" name="money_code" value="{{ $money }}">

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_f_date" type="text" name="f_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <span class="text-danger">{{ $errors->first('f_date') }}</span>
                                        <label for="id_f_date">From Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                    <input id="id_t_date" type="text" name="t_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <span class="text-danger">{{ $errors->first('t_date') }}</span>
                                        <label for="id_t_date">To Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_rech_type"  name = "rech_type">
                                            <option value="ALL" selected>ALL</option>
                                            <option value="RECHARGE" >RECHARGE</option>
                                            <option value="BILL_PAYMENT" >BILL_PAYMENT</option>
                                            <option value="BANK_TRANSFER" >BANK_TRANSFER</option>
                                            </select>
                                            <label>Recharge Type</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                
                                <div class="row" style = "margin-bottom:8px;display:none;" id = "c1">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_rech_status" name="rech_status">
                                            <option value="-" >---Select--</option>
                                            <option value="SUCCESS">SUCCESS</option>
                                            <option value="FAILURE">FAILURE</option>
                                            <option value="PENDING">PENDING</option>
                                          </select>
                                          <label>Recharge Status</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;display:none;" id = "c2">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input type="text" id="id_rech_mobile" name="rech_mobile" class="validate">
                                        <span class="text-danger">{{ $errors->first('rech_mobile') }}</span>
                                        <label for="id_rech_mobile">Mobile No</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input type="text" id="id_rech_amount" name="rech_amount" class="validate">
                                        <span class="text-danger">{{ $errors->first('rech_amount') }}</span>
                                        <label for="id_rech_amount">Amount</label>
                                    </div>
                                </div>
                               

                                <div class="row" style = "margin-bottom:8px;display:none;" id = "c3">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_net_code" name="net_code">
                                            <option value="-" >---Select--</option>
                                            
                                          </select>
                                          <label>Network</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                           
                                    </div>
                                </div>

                               

                               
                                                    
                                <div class="row">
                                    <div class="col s4 m4 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s4 m4 l2 xl2">
                                            <button class="btn waves-effect waves-light blue " type="submit"  id="print_excel" name="print_excel">Print Excel
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>
                                   
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 


                     
                
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->


   

    @include('common.bottom')

   
   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });
            
            $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $("#id_rech_type").change(function(e)
            {
                var ty = $('option:selected', this).val();
               
                if(ty == "ALL")
                {
                    $('#c3').hide("slow");
                    $('#c2').hide("slow");
                    $('#c1').hide("slow");
                }
                else if(ty == "RECHARGE")
                {
                    var data = $("#web_code").val();
                    loadNetwork(data);
                    $('#c3').show("slow");
                    $('#c2').show("slow");
                    $('#c1').show("slow");
                
                }
                else if(ty == "BILL_PAYMENT")
                {
                    var data = $("#bill_code").val();
                    loadNetwork(data);
                    $('#c3').show("slow");
                    $('#c2').show("slow");
                    $('#c1').show("slow");
                
                }
                else if(ty == "BANK_TRANSFER")
                {
                    var data = $("#money_code").val();
                    loadNetwork(data);
                    $('#c3').show("slow");
                    $('#c2').show("slow");
                    $('#c1').show("slow");
                }
                    
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#web_code').val("-");
                $('#bill_code').val("-");
                $('#money_code').val("-");
                
                $('#rech_form').attr('action', "{{url('rechargedetails_apipartner_view')}}");
                $('#rech_form').submit();
                
                
            }); 

             $('#print_excel').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Download Excel!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargedetails_apipartner_excel')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

             function loadNetwork(data)
            {
                var net = JSON.parse(data);
                
                var net_code = $("#id_net_code");
                $(net_code).empty();
                var option1 = $("<option />");
                    option1.html("select Network");
                    option1.val("-");
                    net_code.append(option1);

                $(net).each(function () {
                    var option = $("<option />");
                    option.html(this.net_name);
                    option.val(this.net_code);
                    net_code.append(option);
                });

                $('select').formSelect();
            }

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
  
      });
    </script>
    </body>
</html>
