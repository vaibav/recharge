<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>VaibavOnline</title>

<!-- Styles Icons -->
<link href="{{ asset('css/coreui-icons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/simple-line-icons.css') }}" rel="stylesheet">

<!-- Main Styles  -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">