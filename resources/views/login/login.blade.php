<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
      <link rel="icon" type="image/png" href="{{ asset('img/brand/logo.png') }}">
      <title>
        LakshmiMobiles
      </title>
      <!--     Fonts and icons     -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css" rel="stylesheet">
      <script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"></script>

      <script>
            WebFont.load({
                google: {"families":["Lato:300,400,700,900"]},
                custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset("css/fonts.min.css") }}']},
                active: function() {
                    sessionStorage.fonts = true;
                }
            });
      </script>
      <!-- Nucleo Icons -->
      <link href="{{ asset('ret/css/nucleo-icons.css') }}" rel="stylesheet" />
      <link href="{{ asset('ret/css/nucleo-svg.css') }}" rel="stylesheet" />
      
      <!-- CSS Files -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="{{ asset('ret/css/argon.css') }}" rel="stylesheet" />
      <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
</head>
<body class="login-page">
 
  <section class="section section-shaped section-lg">
    <div class="shape shape-style-1 bg-gradient-default">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="container pt-lg-7">
      <div class="row justify-content-center">
        <div class="col-lg-5">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-white pb-3 text-center">
                <img src="https://ik.imagekit.io/i7626avdgax/laxmi/logo_web_heb8fE1RC.png?updatedAt=1637429687552" style = "width:230px; height:120px;">
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>sign in with credentials</small>
              </div>
              <form id = "id_login_form" action="{{url('login_check')}}" method="post" accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Username" type="text" name="username" id='username'>
                  </div>
                </div>
                <div class="form-group focused">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                    </div>
                    <input class="form-control" placeholder="Password" type="password" name='password' id='password'>
                  </div>
                </div>
                <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                  <label class="custom-control-label" for=" customCheckLogin"><span>Remember me</span></label>
                </div>
                <div class="text-center">
                  <button type="button" class="btn btn-primary my-4" name='btn_login' id = "btn_login">Sign in</button>
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-6">
              <a href="#" class="text-light"><small>Forgot password?</small></a>
            </div>
            <div class="col-6 text-right">
                 <?php
                    if(session()->has('data')) {
                        $op = session('data');
                        echo "<a href='#' class='text-light' id ='id_err'><small>".$op['output']."</small></a>";
                    }
                    else {
                        echo "<a href='#' class='text-light' id ='id_err'><small>&nbsp;</small></a>";
                    }
                ?>
              
            </div>
          </div>

           
        </div>
      </div>
    </div>
  </section>

  <footer class="footer">
    <div class="container">
      <div class="row row-grid align-items-center mb-5">
        <div class="col-lg-6">
          <h3 class="text-primary font-weight-light mb-2">Lakshmi Mobiles</h3>
          <h5 class="mb-0 font-weight-light">This is Lakshmi Mobiles Multi Recharge Portal Exclusively for Lakshmi Mobiles Customers. Customers can recharge their mobile numbers and get report in this portal.</h5>
        </div>
        <div class="col-lg-6 text-lg-center btn-wrapper">
          <!--<button target="_blank" href="{{ asset('apk/EazyPay.apk') }}" rel="nofollow" class="btn btn-icon-only btn-twitter rounded-circle" data-toggle="tooltip" data-original-title="Follow us">
            <span class="btn-inner--icon"><i class="fab fa-android"></i></span>
          </button>
          <button target="_blank" href="{{ asset('apk/VaibavSms.apk') }}" rel="nofollow" class="btn-icon-only rounded-circle btn btn-facebook" data-toggle="tooltip" data-original-title="Like us">
            <span class="btn-inner--icon"><i class="fab fa-android"></i></span>
          </button>-->
          
        </div>
      </div>
      <hr>
      <div class="row align-items-center justify-content-md-between">
        <div class="col-md-4">
          <div class="copyright">
            &copy; 2015-2020 <a href="" target="_blank">Lakshmi Mobiles</a>.
          </div>
        </div>
        <div class="col-md-8">
          <ul class="nav nav-footer justify-content-end">
            <li class="nav-item">
              <a href="" class="nav-link" target="_blank">Privacy Policy</a>
            </li>
            <li class="nav-item">
              <!--<a href="{{ asset('apk/EazyPay.apk') }}" class="nav-link">Android Apk</a>-->
            </li>
           
          </ul>
        </div>
      </div>
    </div>
  </footer>
  
  <!--   Core JS Files   -->
  <script
  src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
  <script src="{{ asset('ret/js/argon-design-system.min.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.js"></script>

  <script type="text/javascript">
        $(document).ready(function() 
        {
            $('#btn_login').on('click',function(e)
            {
                e.preventDefault();

                var a = $('#username').val();
                var b = $('#password').val();

                if(a != "" && b != "")
                {
                    $('#id_login_form').attr('action', "{{url('login_check')}}");
                    $('#id_login_form').submit();
                }
                else if(a == "")
                {
                    $('#id_err').html("User Name is Empty...");
                }
                else if(b == "")
                {
                    $('#id_err').html("Password is Empty...");
                }
            });
        });
    </script>
</body>





</html>