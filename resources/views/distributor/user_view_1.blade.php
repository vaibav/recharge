<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_distributor')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">User Details </span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
               <!-- Search Bar -->
                <div class = "row" style = "margin-bottom:5px;">
                    <div class ="col s12 m12 l12 xl12">
                        <nav>
                            <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                <input id="search" type="search" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">close</i>
                                </div>
                            </form>
                            </div>
                        </nav>
                    </div>
                </div>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table " id = "myTable">
                            <thead>
                            <tr>
                                  <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Code</th>
                                  <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                  <th style='font-size:12px;padding:7px 8px;'>KYC</th>
                                  <th style='font-size:12px;padding:7px 8px;'>User Type </th>
                                  <th style='font-size:12px;padding:7px 8px;'>Parent</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Parent Type</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Balance</th>
                                  <th style='font-size:12px;padding:7px 8px;'>View</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;

                                        foreach($user1 as $f1)
                                        {
                                          $f = $f1[0];
                                          $level = $f1[1];
                                         
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_code."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_name."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_per_name."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_kyc."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->account->user_type."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->account->parent_name."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->account->parent_type."</td>";
                                          
                                          $arr = (array)$f[0]->balance;
                                          if(empty($arr))
                                          {
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>0.00</td>";
                                          }
                                          else
                                          {
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f[0]->balance->user_balance."</td>";
                                          }

                                          echo "<td style='font-size:12px;padding:7px 8px;'>";
                                          echo "<button class='btn-floating btn-sm ' id='view_".$f[0]->user_code."'>
                                          <i class='small material-icons'>edit</i></td>";

                                          echo "<td style='font-size:12px;padding:7px 8px;'>";
                                          echo "<button class='btn-floating btn-sm amber' id='network_".$f[0]->user_code."'>
                                          <i class='small material-icons'>edit</i></td>";

                                          if($f[0]->account->user_status == 1)
                                          {
                                            echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>LIVE</td>";
                                          }
                                          else
                                          {
                                            echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>INACTIVE</td>";
                                          }
                                          
                                          echo "</tr>";
                                                                                  
                                          $j++;
                                        }

                                    ?>
                                       
                            </tbody>
                        </table>
                       
                       
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

   

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

           $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("view_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure to View?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                          var code = nid[1];
                          window.location.href = "<?php echo url('/'); ?>/user_view_distributor/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });

                   
                }
                var nid1=gid.split("network_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                  swal({
                        title: 'Are you sure to View?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View Network!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                          window.location.href = "<?php echo url('/'); ?>/user_view_network_distributor/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                    
                }
            });
            
            $("#search").on("keyup", function() {
                var value = $(this).val();
                // Hide all table tbody rows
                $('table tbody tr').hide();

                // Count total search result
                var len = $('table tbody tr:not(.notfound) td:nth-child(3):contains("'+value+'")').length;

                if(len > 0){
                // Searching text in columns and show match row
                $('table tbody tr:not(.notfound) td:contains("'+value+'")').each(function(){
                    $(this).closest('tr').show();
                });
                }else{
                $('.notfound').show();
                }
            });


            
      });
    </script>
    </body>
</html>
