<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
       
       
        @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user));
        
        <div class='fixed-action-btn'><a class='btn-floating btn-large red' href = "{{url('dashboard_distributor')}}">
                <i class='large material-icons'>home</i></a>          
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:56px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Stock Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('stockdetails_distributor')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>User</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Purchase (Debit)</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Sales (Credit)</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Balance</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                        
                                        $debit = 0;
                                        $credit = 0;
                                        $balance = 0;
                                        if($obalance != 0)
                                        {
                                            $balance = floatval($balance) + floatval($obalance);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>OPENING BALANCE</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                        }
                                        foreach($pay1 as $f)
                                        {
                                          if($f->user_name == $user->user && $f->rech_status == "SUCCESS")
                                          {
                                              // Debit +
                                              $debit = floatval($debit) + floatval($f->ret_total);
                                              $balance = floatval($balance) + floatval($f->ret_total);
                                              echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'>".$f->rech_date."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>".$f->parent_name."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->trans_type."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f->ret_total."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                              
                                          }
                                          else if($f->parent_name == $user->user && $f->rech_status == "SUCCESS")
                                          {
                                              // Credit -
                                              $credit = floatval($credit) + floatval($f->ret_total);
                                              $balance = floatval($balance) - floatval($f->ret_total);
                                              echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'>".$f->rech_date."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>".$f->user_name."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->trans_type."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f->ret_total."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                          }
                                          $j++;
                                        
                                        }

                                        if($ce_tot != '0')
                                        {
                                            $credit = floatval($credit) + floatval($ce_tot);
                                            $balance = floatval($balance) - floatval($ce_tot);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>RECHARGE</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($ce_tot,2,'.','')."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                            $j++;
                                        
                                        }
                                        
                                         if($old_re != '0')
                                        {
                                            $credit = floatval($credit) + floatval($old_re);
                                            $balance = floatval($balance) - floatval($old_re);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>OLD RECHARGE</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($old_re,2,'.','')."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                            $j++;
                                        
                                        }

                                        if($bill != '0')
                                        {
                                            $credit = floatval($credit) + floatval($bill);
                                            $balance = floatval($balance) - floatval($bill);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>EB BILL RECHARGE</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($bill,2,'.','')."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                            $j++;
                                        
                                        }

                                        if($money != '0')
                                        {
                                            $credit = floatval($credit) + floatval($money);
                                            $balance = floatval($balance) - floatval($money);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>MONEY TRANSFER</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($money,2,'.','')."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                            $j++;
                                        
                                        }

                                        if($mverify != '0')
                                        {
                                            $credit = floatval($credit) + floatval($mverify);
                                            $balance = floatval($balance) - floatval($mverify);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>BENEFICIARY VERIFY</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($mverify,2,'.','')."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                            $j++;
                                        
                                        }

                                        
                                      
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER</th>
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET(%)/SURP</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR.ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH TYPE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                      
                                       foreach($recharge as $f)
                                       {
                                          
                                                                                       
                                           $net_name = "";
                                           foreach($network as $r)
                                           {
                                               if($f->net_code == $r->net_code)
                                                   $net_name = $r->net_name;
                                           }

                                           $reply_id = "";
                                           $reply_date = "";
                                           $rech_status = "";
                                           $o_bal = 0;
                                           $z = 0;

                                            if($f->rech_type == "RECHARGE")
                                            {
                                                $reply_id = $f->newparentrecharge2->reply_opr_id;
                                                $reply_date =$f->newparentrecharge2->reply_date;
                                            }

                                            $rech_status = $f->rech_status;
                                            $rech_option = $f->rech_option;

                                           if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                            {      
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($f->user_balance) - floatval($f->rech_total);
                                                $z = 1;
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light'><i class='small material-icons '>check</i></button>";
                                                $o_bal = floatval($f->user_balance) + floatval($f->rech_total);
                                            }
               
                                           $o_bal = number_format($o_bal, 2, '.', '');
                                           $c_bal = floatval($f->user_balance);
                                           $c_bal = number_format($c_bal, 2, '.', '');

                                           
                                           if($z == 0)
                                           {
                                               echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                               
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_net_per."--".$f->rech_net_per_amt."";

                                               echo "--".$f->rech_net_surp."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";

                                               echo "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 130px;word-break: break-word;'>".$reply_id."</div></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_type."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                               
                                           }
                                           else
                                           {
                                               echo "<tr style='background-color:#E8DAEF;'><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                               
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";

                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$f->rech_total."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";

                                               
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;'></td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                               echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                           }

                                          
                                           
                                            echo "</tr>";
                                                                                   
                                           $j++;
                                       }
                                       

                                      
                                    ?>

                            </tbody>
                            </tbody>
                        </table>
                        {{ $recharge->links('vendor.pagination.materializecss') }}
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('common.bottom')

    

    <script>
     $(document).ready(function() 
	    {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
           
          
      });
    </script>
    </body>
</html>
