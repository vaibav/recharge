<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_distributor')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Agent Collection Verify Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('ds_collection_verify')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
               
               <!-- Page Body --> 
               <div class = "row">
                    <div class ="col s12 m12 l11 xl11">
                        <table class="bordered striped responsive-table ">
                            <thead>
                                <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Tr.Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Bill No</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Paid Amount</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Paid Date</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                    <th style='font-size:12px;padding:7px 8px;'>DD/Cheque No</th> 
                                    <th style='font-size:12px;padding:7px 8px;'>Image</th> 
                                    <th style='font-size:12px;padding:7px 8px;'>Status</th> 
                                    <th style='font-size:12px;padding:7px 8px;'>Action</th> 
                                </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                    $j = 1;
                                    $pay_tot = 0;
                                    foreach($collection as $f)
                                    {
                                        echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->bill_no."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_amount."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_date."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_mode."</td>";
                                        echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->pay_option."</td>";

                                        if($f->pay_image != "-") {
                                            echo "<td style='font-size:12px;padding:7px 8px;'>";
                                            echo "<img src ='".url("/")."/uploadbankreceipt/".$f->pay_image."' style='height:120px;width:120px;' alt='NO PHOTO'/>";
                                            echo "</td>";
                                        }
                                        else
                                        {
                                            echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                        }

                                        if($f->pay_status == "0") 
                                        {
                                            echo "<td style='font-size:12px;padding:7px 8px;'>PENDING</td>";
                                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'>";
                                            echo "<button class='btn-floating btn-sm waves-effect' id='success_".$f->trans_id."'>
                                            <i class='small material-icons'>check</i></button>&nbsp;&nbsp;&nbsp;";
                                            echo "<button class='btn-floating btn-sm waves-effect red' id='failure_".$f->trans_id."'>
                                            <i class='small material-icons'>close</i></button></td>";
                                        }
                                        else if($f->pay_status == "1") 
                                        {
                                            echo "<td style='font-size:12px;padding:7px 8px;'>SUCCESS</td>";
                                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'></td>";
                                           
                                        }
                                        else if($f->pay_status == "2") 
                                        {
                                            echo "<td style='font-size:12px;padding:7px 8px;'>FAILURE</td>";
                                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'></td>";
                                        }

                                       

                                        echo "</tr>";
                                                    
                                        $pay_tot = floatval($pay_tot) + floatval($f->pay_amount);
                                        $j++;
                                    }

                                    echo "<tr><td style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'>TOTAL</td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$pay_tot."</td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                    echo "</tr>";
                                ?>
                            </tbody>
                        </table>
                    
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

            
            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("success_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Success it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            
                            window.location.href = "<?php echo url('/'); ?>/ds_collection_verify_success/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                var nid1=gid.split("failure_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Failed it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.location.href = "<?php echo url('/'); ?>/ds_collection_verify_failure/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                
                
            });

      });
    </script>
    </body>
</html>
