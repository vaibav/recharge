<!-- Navbar goes here -->
<ul id="dropdown1" class="dropdown-content">
            <li><a href="#!">Bal : {{$bal}}</a></li>
            <li><a href="{{url('changepassword')}}">Change Password</a></li>
            <li class="divider"></li>
            <li><a href="{{url('logout')}}">logout</a></li>
        </ul>
        <div class="navbar-fixed">
            <nav class="#1e88e5 pink darken-1">
                <div class="row">
                    <div class="col s6 m6 l3 xl3">
                        <a href="#" data-target="slide-out" class="sidenav-trigger" style="display: block;margin: 0 10px;"><i class="material-icons">menu</i></a>
                        <img class="responsive-img" src="{{ asset('img/brand/logo.png') }}" style="height: 46px; width: 85px;padding-top: 15px;">
                    </div>
                    <div class="col s6 m6 l9 xl9 right-align">
                        <ul class="right ">
                            <li><a href="#" style="padding: 0 6px;">Hai {{$uname}}!&nbsp;&nbsp;&nbsp;&#x20B9;{{$bal}}</a></li>
                            <!-- Dropdown Trigger -->
                            <li><a class="dropdown-trigger" href="#!" data-target="dropdown1" style="padding: 0 6px;padding-top: 2px;"><i class="material-icons">more_vert</i></a></li>
                        </ul>
                    </div>
                </div>
                
            </nav>
        </div>
        
        <ul id="slide-out" class="sidenav">
            <li>
                <div class="user-view">
                <div class="background">
                    <img src="{{ asset('img/brand/back.jpg') }}">
                </div>
                <a href="#user"><img class="circle" src="{{ asset('uploadphoto/user.jpg') }}"></a>
                <a href="#name"><span class="white-text name">{{$uname}}</span></a>
                <a href="{{url('dashboard_distributor')}}"><span class="white-text name" style = "margin-top:4px;">{{$user->mode}}</span></a>
                </div>
            </li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown2"><i class="material-icons">cloud</i> User</a>
                <ul id="dropdown2" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('user_distributor')}}">User Creation Entry</a></li>
                    <li><a href="{{url('user_view_distributor')}}">User Details</a></li>
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown3"><i class="material-icons">cloud</i> Payment Details</a>
                <ul id="dropdown3" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('remotepayment_distributor')}}">Remote Payment</a></li>
                    <li><a href="{{url('paymentrequest_distributor')}}">Payment Request</a></li>
                    <li><a href="{{url('paymentaccept_distributor')}}">Payment Accept</a></li>
                    <li><a href="{{url('stockdetails_distributor')}}">Stock Details</a></li>
                
                </ul>
            </li>
            
             <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown7"><i class="material-icons">cloud</i> Collection</a>
                <ul id="dropdown7" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('ds_agent_allocation')}}">Area Allocation</a></li>
                    <li><a href="{{url('ds_collection_verify')}}">Collection Verify</a></li>
                    <li><a href="{{url('ds_collection')}}">Pending Report</a></li>
                    
                
                </ul>
            </li>
            
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown5"><i class="material-icons">cloud</i> Recharge</a>
                <ul id="dropdown5" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('user_rechargedetails_distributor')}}">Recharge Details</a></li>
                
                </ul>
            </li>
            <li><div class="divider"></div></li>
            <li><a href="#!" class="dropdown-trigger" data-target="dropdown6"><i class="material-icons">cloud</i>Complaint</a>
                <ul id="dropdown6" class="dropdown-content #fafafa grey lighten-5">
                    <li><a href="{{url('complaint_distributor')}}">Complaint Entry</a></li>
                    <li><a href="{{url('complaint_view_distributor')}}">Complaint Details</a></li>
                </ul>
            </li>
            
            
           
        </ul>