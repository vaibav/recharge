<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')

        <link rel="stylesheet" href="{{ asset('css/mstepper.min.css') }}">

        <style>
            .select-dropdown {
                top: 0 !important;
                overflow: scroll;
                
            }
        </style>
        
    </head>
    <body style = "background-color: #34495e;">
        @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

       

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">User Entry</span>
                </div>

                <div class="card-content" style = "border-radius:5px;padding:5px 5px;">
                    <form id="id_api_form" class="form-horizontal" action="{{url('user_store_distributor')}}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_code" id="id_user_code" value="0">

                    <ul class="stepper horizontal demos" id="horizontal" style = "margin:2px 2px;">
                        <li class="step">
                            <div data-step-label="Personal" class="step-title waves-effect waves-dark">Personal</div>
                            <div class="step-content">
                                
                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_per_name" type="text" name="user_per_name" >
                                        <span class="text-danger red-text">{{ $errors->first('user_per_name') }}</span>
                                        <label for="id_user_per_name">Full Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_state" type="text" name="user_state" >
                                        <span class="text-danger red-text">{{ $errors->first('user_state') }}</span>
                                        <label for="id_user_state">Area</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                       

                                         <input id="id_user_city" type="text" name="user_city" >
                                        <span class="text-danger red-text">{{ $errors->first('user_city') }}</span>
                                        <label for="id_user_city">City</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_mobile" type="text" name="user_mobile" >
                                        <span class="text-danger red-text">{{ $errors->first('user_mobile') }}</span>
                                        <label for="id_user_mobile">Mobile</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        
                                        <input id="id_user_phone" type="text" name="user_phone" >
                                        <span class="text-danger red-text">{{ $errors->first('user_phone') }}</span>
                                        <label for="id_user_phone">WhatsApp No</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_mail" type="text" name="user_mail" >
                                        <span class="text-danger red-text">{{ $errors->first('user_mail') }}</span>
                                        <label for="id_user_mail">Mail Id</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l2 xl2" style="margin: 3px 0px;">
                                        <select id="id_user_kyc" class="form-control" name="user_kyc" >
                                            <option value="-">--Select--</option>
                                            <option value="Driving_License">Driving License</option>
                                            <option value="Voter_ID">Voter ID</option>
                                            <option value="Ration card">Ration Card</option>
                                            <option value="Aadhar card">Aadhar Card</option>
                                            <option value="Pan card">Pan Card</option>
                                            <option value="Passport">Passport</option>
                                        </select>
                                        <label>Select KYC</label>
                                    </div>
                                    <div class="file-field input-field col s12 m12 l5 xl5">
                                        <div class="btn">
                                          <span>KYC Upload</span>
                                          <input type="file" id="id_user_kyc_proof" name="user_kyc_proof"> 
                                        </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text" placeholder = "Image(<200KB)">
                                        </div>
                                    </div>
                                    <div class="file-field input-field col s12 m12 l5 xl5">
                                        <div class="btn">
                                          <span>Photo Upload</span>
                                          <input type="file" id="id_user_photo" name="user_photo"> 
                                        </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text" placeholder = "Image(<20KB)">
                                        </div>
                                    </div>
                                </div>

                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue next-step">CONTINUE</button>
                                </div>
                            </div>
                        </li>
                        <!-- Step - 2 -->
                        <li class="step">
                            <div class="step-title waves-effect waves-dark">Account</div>
                            <div class="step-content">
                                
                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_name" type="text" name="user_name" >
                                        <span class="text-danger red-text">{{ $errors->first('user_name') }}</span>
                                        <label for="id_user_name">User Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_pwd" type="password" name="user_pwd" >
                                        <span class="text-danger red-text">{{ $errors->first('user_pwd') }}</span>
                                        <label for="id_user_pwd">Password</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_user_acc_type" name="user_acc_type">
                                            <option value="-">---Select---</option>
                                            <option value='RETAILER'>RETAILER</option>
                                            <option value="AGENT">AGENT</option>
                                        </select>
                                        <label>Account Type</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_user_setup_fee" type="text" name="user_setup_fee" >
                                        <span class="text-danger red-text">{{ $errors->first('user_setup_fee') }}</span>
                                        <label for="id_user_setup_fee">Setup Fee</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_pack_id" name="pack_id">
                                            <option value="-">---Select---</option>
                                           <?php
                                                foreach($package as $f) {
                                                    echo "<option value='".$f->pack_id."' >".$f->pack_name."</option>";
                                                }
                                            ?>
                                        </select>
                                        <label>Package</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:45px;">
                                    <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                        <label><input type="checkbox" id="id_user_rec_mode1" name="user_rec_mode[]" value="WEB"/><span>WEB &nbsp;</span></label>
                                    </div>
                                    <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                        <label><input type="checkbox" id="id_user_rec_mode2" name="user_rec_mode[]" value="GPRS" /><span>GPRS&nbsp;</span></label>
                                    </div>
                                    <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                        <label><input type="checkbox" id="id_user_rec_mode3" name="user_rec_mode[]" value="API" /><span>API&nbsp;</span></label>
                                    </div>
                                    <div class="input-field col s3 m3 l2 xl2" style="margin: 3px 0px;">
                                        <label><input type="checkbox" id="id_user_rec_mode4" name="user_rec_mode[]" value="SMS" /><span>SMS&nbsp;</span></label>
                                    </div>
                                </div>

                               

                                
                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue" type="submit" id = "btn_submit">SUBMIT</button>
                                    <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
                                </div>
                            </div>
                        </li>
                        <!-- Step - 3 -->
                        
                        
                    </ul>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

    <script src="{{ asset('js/mstepper.min.js') }}"></script>

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var stepper = document.querySelector('.stepper');
            var stepperInstace = new MStepper(stepper, {
                // options
                firstActive: 0 // this is the default
            });

            $("#id_user_phone, #id_user_mobile, #id_user_setup_fee").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $("#id_user_acc_type").change(function(e)
            {
                var ty = $('option:selected', this).val();

                if(ty != "-")
                {
                    if(ty == "API PARTNER")
                    {
                        $("#id_user_api_url_1").val("");
                        $("#id_user_api_url_2").val("");
                        $("#id_f1").show();
                    }
                    else
                    {
                        $("#id_user_api_url_1").val("*");
                        $("#id_user_api_url_2").val("*");
                        $("#id_f1").hide();
                    }
                }
            });
            
            $("#id_user_name").blur(function(e) 
            {
                e.preventDefault();
                var user_name = $("#id_user_name").val();
                $('#loader').show();

                if (user_name != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_user/" + user_name,
                        success: function (data) {
                            if(data == 1)
                            {
                                $('#loader').hide();
                                swal("Alert", "User Name Already Exists....", "error");
                                $("#id_user_name").val("");
                                $("#id_user_name").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_name").focus();
                }
                   
            });

            $("#id_user_mobile").blur(function(e) 
            {
                e.preventDefault();
                var user_mobile = $("#id_user_mobile").val();
                $('#loader').show();

                if (user_mobile != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_mobile/" + user_mobile,
                        success: function (data) {
                            if(data > 0)
                            {
                                $('#loader').hide();
                                swal("Alert", "Mobile No Already Exists....", "error");
                                $("#id_user_mobile").val("");
                                $("#id_user_mobile").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_mobile").focus();
                }
                   
            });
            

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var z = check_data();
                $('#btn_submit').prop('disabled', true);

                if(z[0] == 0)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Submit!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#id_api_form').attr('action', "{{url('user_store_distributor')}}");
                            $('#id_api_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                }
                else
                {
                    swal("Cancelled", z[1], "error");
                    $('#btn_submit').prop('disabled', false);
                }
                
               
                
                
            }); 

            function check_data()
            {
                var z = [];
                z[0] = 0;
                z[1] = "done";

                if($('#id_user_per_name').val() == "") {
                    z[0] = 1;
                    z[1] = "Person Name is Empty..";
                }
                else  if($('#id_user_city').val() == "") {
                    z[0] = 1;
                    z[1] = "City is Empty..";
                }
                else  if($('#id_user_state').val() == "") {
                    z[0] = 1;
                    z[1] = "Area is Empty..";
                }
                else  if($('#id_user_mobile').val() == "") {
                    z[0] = 1;
                    z[1] = "Mobile No is Empty..";
                }
                else  if($('#id_user_mail').val() == "") {
                    z[0] = 1;
                    z[1] = "mail is Empty..";
                }
                else  if($('#id_user_name').val() == "") {
                    z[0] = 1;
                    z[1] = "user name is Empty..";
                }
                else  if($('#id_user_pwd').val() == "") {
                    z[0] = 1;
                    z[1] = "Password is Empty..";
                }
                else  if($('#id_pack_id').val() == "-") {
                    z[0] = 1;
                    z[1] = "Package is Empty..";
                }
                
                return z;
            }

            function numbersOnly(e)
			{
				// Allow: backspace, delete, tab, escape, enter and .
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) 
				{
					 // let it happen, don't do anything
					 return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			}
      });
    </script>
    </body>
</html>
