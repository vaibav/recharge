<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
            @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user));

            <div class="fixed-action-btn">
                <a class="btn-floating btn-large red" href = "{{url('dashboard_distributor')}}">
                    <i class="large material-icons">home</i>
                </a>
                
            </div>
       
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Complaint Details</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Rech Amt</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Complaint</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Admin Reply</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Reply Date</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                        
                                        $j = 1;
                                        foreach($comp1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_complaint."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->admin_reply."</td>";
                                          if($f->reply_status == 1)
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                          else if($f->reply_status == 2)
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_date."</td>";
                                          echo "</tr>";
                                          $j++;
                                        }
                                        
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    

    @include('common.bottom')

   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

          
      });
    </script>
    </body>
</html>
