<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('common.top')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('distributor.sidebar', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard_distributor')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                <span class="card-title" style = "padding:12px;">Pending Collection Details</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <p>Current Area : <?php echo $area_name; ?></p>
                

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                            <table class="bordered striped responsive-table ">
                                <thead>
                                    <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Full Name</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Taken</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Paid</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Pending</th>
                                        <th style='font-size:12px;padding:7px 8px;display:none;'>Pay</th>
                                        
                                </tr>
                                </thead>
                                <tbody id="tbl_body">
                                <?php 
                                            $j = 1;

                                            foreach($c_details as $f)
                                            {
                                            
                                            
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_name']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['per_name']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_mobile']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['de_tot']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['ce_tot']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['pe_tot']."</td>";
                                            
                                            

                                            echo "<td style='font-size:12px;padding:7px 8px;display:none;'>";
                                            echo "<button class='btn-floating btn-sm ' id='view_".$f['user_name']."'>
                                            Pay</td>";

                                            

                                            
                                            
                                            echo "</tr>";
                                                                                    
                                            $j++;
                                            }
                                        ?>
                                </tbody>
                            </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('common.bottom')

    

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

           

      });
    </script>
    </body>
</html>
