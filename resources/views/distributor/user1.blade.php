<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
       
        
        <link href="{{ asset('css/jquery.steps1.css') }}" rel="stylesheet">
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style= " background-color: #0E6655;">
    @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'User Entry', 'uname' => $user->user, 'bal' => $user->ubal))

        <div id ="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url({{ asset('img/loader/3.gif') }}) 50% 50% no-repeat rgba(249,249,249, 0.5) ;display:none;" ></div>
        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:2px 5px;">
                <div class="card card-accent-primary" style="margin-bottom:2px;">
                  
                    <div class="card-body" style="padding:3px 10px;">
                            <!-- WIZARD STARTS... --> 
                            <form id="id_api_form" class="form-horizontal" action="{{url('user_store_user')}}" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_code" id="id_user_code" value="0">
                            <div id="wizard" >
                                <h2>Personal Details</h2>
                                <section >
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Name</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_per_name" type="text" name="user_per_name" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_per_name') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">City</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_city" type="text" name="user_city" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_city') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">State</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="id_user_state" name="user_state">
                                                        <option value="-">---Select---</option>
                                                        <option value="Tamil Nadu">Tamil Nadu</option>
                                                        <option value="Kerala">Kerala</option>
                                                        <option value="Karnataka">Karnataka</option>
                                                        <option value="Pondicherry">Pondicherry</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Landline</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_phone" type="text" name="user_phone" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Mobile</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_mobile" type="text" name="user_mobile" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_mobile') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Mail Id</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_mail" type="text" name="user_mail" placeholder="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">KYC</label>
                                                <div class="col-sm-8">
                                                    <select id="id_user_kyc" class="form-control" name="user_kyc" >
                                                        <option value="-">--Select--</option>
                                                        <option value="Driving_License">Driving License</option>
                                                        <option value="Voter_ID">Voter ID</option>
                                                        <option value="Ration card">Ration Card</option>
                                                        <option value="Aadhar card">Aadhar Card</option>
                                                        <option value="Pan card">Pan Card</option>
                                                        <option value="Passport">Passport</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Photo</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_photo" type="file" name="user_photo" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_photo') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">KYC Proof</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_kyc_proof" type="file" name="user_kyc_proof" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_kyc_proof') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <h2>Account Details</h2>
                                <section >
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">User Name</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_name" type="text" name="user_name" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_name') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Password</label>
                                                <div class="col-sm-8">
                                                <input class="form-control" id="id_user_pwd" type="password" name="user_pwd" placeholder="">
                                                <span class="text-danger">{{ $errors->first('user_pwd') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">User Account Type</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control" id="id_user_acc_type" name="user_acc_type">
                                                        <option value="-">---Select---</option>
                                                        <?php
                                                            if($user->mode == "SUPER DISTRIBUTOR")
                                                            {
                                                                echo "<option value='DISTRIBUTOR'>DISTRIBUTOR</option>";
                                                                echo "<option value='API PARTNER'>API PARTNER</option>";
                                                            }
                                                            else if($user->mode == "DISTRIBUTOR")
                                                            {
                                                                echo "<option value='RETAILER'>RETAILER</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Setup Fee</label>
                                                <div class="col-sm-8">
                                                    <input class="form-control" id="id_user_setup_fee" type="text" name="user_setup_fee" placeholder="">
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label" for="input-normal">Recharge Mode</label>
                                                <div class="col-sm-8">
                                                    <input type="checkbox" id="id_user_rec_mode" name="user_rec_mode[]" value="WEB">WEB &nbsp;
                                                    <input type="checkbox" id="id_user_rec_mode" name="user_rec_mode[]" value="GPRS">GPRS&nbsp;
                                                    <input type="checkbox" id="id_user_rec_mode" name="user_rec_mode[]" value="API">API&nbsp;
                                                    <input type="checkbox" id="id_user_rec_mode" name="user_rec_mode[]" value="SMS">SMS&nbsp;
                                                    <span class="text-danger">{{ $errors->first('user_rec_mode') }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            
                                        </div>
                                    </div>
                                    <div class = "row" id="id_f1" >
                                        <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label" for="input-normal">API Partner Success URL</label>
                                                <div class="col-sm-8">
                                                    <input class="form-control" id="id_user_api_url_1" type="text" name="user_api_url_1" placeholder="Http://">
                                                </div>
                                            </div>
                                        </div>
                                        <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            
                                        </div>
                                    </div>
                                    <div class = "row" id="id_f2" >
                                        <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group row">
                                                <label class="col-sm-2 col-form-label" for="input-normal">API Partner Failure URL</label>
                                                <div class="col-sm-8">
                                                    <input class="form-control" id="id_user_api_url_2" type="text" name="user_api_url_2" placeholder="Http://">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                
                                <h2>Network Details</h2>
                                <section >
                                    <div class = "row">
                                        <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div style ="height:250px;overflow-y:scroll;">
                                                <table class="table table-responsive-sm table-bordered" >
                                                    <thead>
                                                        <tr>
                                                        <th style='font-size:12px;padding:7px 8px;'>No</th>
                                                        <th style='font-size:12px;padding:7px 8px;display:none;'>Network ID</th>
                                                        <th style='font-size:12px;padding:7px 8px;'>Network Name</th>
                                                        <th style='font-size:12px;padding:7px 8px;'>Percentage</th>
                                                        <th style='font-size:12px;padding:7px 8px;'>Surplus Charge</th>   
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbl_body">
                                                        <?php 
                                                            $j = 1;
                                                            foreach($network as $f)
                                                            {
                                                                echo "<tr><td style='font-size:12px;padding:2px 8px;'>".$j."</td>";
                                                               
                                                                
                                                                echo "<td style='font-size:12px;padding:2px 8px;display:none'>";
                                                                echo "<input type='hidden' id='id_net_code_".$j."' name='net_code_".$j."' value='".$f->net_code."' /></td>";

                                                                echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                                echo "<input type='text' id='id_net_name_".$j."' name='net_name_".$j."' class='form-control' value='".$f->net_name."' /></td>";

                                                                echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                                echo "<input type='text' id='id_net_per_".$j."' name='net_per_".$j."' class='form-control' value='0'/></td>";

                                                                echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                                echo "<input type='text' id='id_net_surp_".$j."' name='net_surp_".$j."' class='form-control' value='0' /></td></tr>";

                                                                $j++;
                                                            }
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                    </div>
                                    
                                </section>
                                
                            </div>
                            </form>                   

                            <!-- WIZARD ENDS --> 
                      
                    </div>
                    
                </div>
                <p style="margin-bottom:2px;"><p>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script src="{{ asset('js/jquery.steps.js') }}"></script>
    <script>
     $(document).ready(function() 
		{
          //Initialize tooltips
            $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft"
            });
            
            $("#id_user_phone, #id_user_mobile, #id_user_setup_fee").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $("#id_user_acc_type").change(function(e)
            {
                var ty = $('option:selected', this).val();

                if(ty != "-")
                {
                    if(ty == "API PARTNER")
                    {
                        $("#id_user_api_url_1").val("");
                        $("#id_user_api_url_2").val("");
                        $("#id_f1").show();
                        $("#id_f2").show();
                    }
                    else
                    {
                        $("#id_user_api_url_1").val("*");
                        $("#id_user_api_url_2").val("*");
                        $("#id_f1").hide();
                        $("#id_f2").hide();
                    }
                }
            });


            $("#id_user_name").blur(function(e) 
            {
                e.preventDefault();
                var user_name = $("#id_user_name").val();
                $('#loader').show();

                if (user_name != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_user1/" + user_name,
                        success: function (data) {
                            if(data == 1)
                            {
                                $('#loader').hide();
                                swal("Alert", "User Name Already Exists....", "error");
                                $("#id_user_name").val("");
                                $("#id_user_name").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_name").focus();
                }
                   
            });

            $("#id_user_mobile").blur(function(e) 
            {
                e.preventDefault();
                var user_mobile = $("#id_user_mobile").val();
                $('#loader').show();

                if (user_mobile != "")
                {
                    $.ajax({
                        type: 'GET', //THIS NEEDS TO BE GET
                        url: "<?php echo url('/'); ?>/check_mobile1/" + user_mobile,
                        success: function (data) {
                            if(data > 0)
                            {
                                $('#loader').hide();
                                swal("Alert", "Mobile No Already Exists....", "error");
                                $("#id_user_mobile").val("");
                                $("#id_user_mobile").focus();
                            }
                            else
                            {
                                $('#loader').hide();
                            }
                        },
                        error: function() { 
                            $('#loader').hide();
                            console.log(data);
                        }
                    });
                }
                else
                {
                    $('#loader').hide();
                    $("#id_user_mobile").focus();
                }
                   
            });
            
            $("a[href$='#finish']").click(function(e) 
            {
                var cfm = confirm("Are you Sure to Save?");
                var c = $('#id_pr_url').val();
                if(cfm)
                {
                    $('#id_api_form')[0].submit();
                }
                else
                {
                    alert("Sorry !");
                }
                   
            });

            function numbersOnly(e)
			{
				// Allow: backspace, delete, tab, escape, enter and .
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) 
				{
					 // let it happen, don't do anything
					 return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			}
            
      });

    
    </script>
    </body>
</html>
