@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Payment </h2>
                <h5 class="text-white op-7 mb-3">Payment Accept</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
     <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                        <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                        <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                        <th style='font-size:12px;padding:7px 8px;'>Request Amount</th>
                        <th style='font-size:12px;padding:7px 8px;'>Request Date</th>
                        <th style='font-size:12px;padding:7px 8px;'>Payment Mode</th>
                        <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                        <th style='font-size:12px;padding:7px 8px;'>Grant Amount</th>
                        <th style='font-size:12px;padding:7px 8px;'>Action</th>
                          
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                    <?php 
                        $j = 1;
                        foreach($pay1 as $f)
                        {
                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_amount."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_date."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_type."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_id."</td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' id='Amt_".$f->trans_id."' /></td>";
                          echo "<td style='font-size:12px;padding:7px 8px;'>";
                          echo "<button class='btn btn-icon btn-round btn-success ' id='Save_".$f->trans_id."'><i class='fa fa-check' style='color:white'></i></button>&nbsp;&nbsp;";

                          echo "<button class='btn btn-icon btn-round btn-danger' id='Delete_".$f->trans_id."'><i class='fa fa-times' style='color:white'></i></button>";
                    
                          echo "</tr>";
                          $j++;
                        }
                    ?>
                    
                    </tbody>
                </table>
                </div>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    <br>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op = session('result');
        
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
        
        session()->forget('result');
    }
?>


<script>
$(document).ready(function() {
    //alert("hellow");

    $("input[type=text]").keydown(function (e) 
    {
        numbersOnly(e);
    });

     $('#tbl_body').delegate('input', 'keyup', function(e) 
    {
        e.preventDefault();
        var bal = $("#id_user_bal").val();
        var amt = $("#" + this.id).val();
        //alert(bal+"---"+this.id);
        if(parseFloat(amt) > parseFloat(bal))
        {
            swal("Cancelled", "Error! High value than Balance!", "error");
            $("#" +this.id).val("");
        }
    });
    

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("Save_");
        if(nid.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Transfer!'
                }).then((result) => {
                if (result.value) {
                    var code = nid[1];
                    var amt = $("#Amt_" + code).val();

                    if(amt != "") {
                        startloader();
                        window.location.href = "<?php echo url('/'); ?>/paymentaccept_store/" + code + "/" + amt;
                    }
                    else {
                        swal("Cancelled", "Amount is Empty...", "error");
                    }
                    //alert(code + "---" + amt);
                    
                }
                else
                {
                    swal("Cancelled", "No Transfer...", "error");
        
                }
            });
            
        }
        var nid1=gid.split("Delete_");
        var net=0;
        var ctx=0;
        if(nid1.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Cancel!'
                }).then((result) => {
                if (result.value) {
                    var code = nid1[1];
                    startloader();
                    window.location.href = "<?php echo url('/'); ?>/paymentaccept_delete/"+code;
                }
                else
                {
                    swal("Cancelled", "No Transfer...", "error");
                    
                }
            });
            
        }
    });

     function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }

    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop