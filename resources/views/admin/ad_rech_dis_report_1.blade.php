@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Distributor Recharge</h2>
                <h5 class="text-white op-7 mb-3">Distributor /Super Distributor Recharge Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('rechargedetails_distributor_view')}}" method="get" accept-charset="UTF-8">
        <input type="hidden" id ="web_code" name="web_code" value="{{ $web }}">
        <input type="hidden" id ="bill_code" name="bill_code" value="{{ $bill }}">
        <input type="hidden" id ="money_code" name="money_code" value="{{ $money }}">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_f_date">From Date</label>
                    <input type="text" class="form-control" id="id_f_date" name="f_date" value ="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_t_date">To Date</label>
                    <input type="text" class="form-control" id="id_t_date" name="t_date" value ="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_api_result_type">Recharge Type</label>
                    <select class="form-control" id="id_rech_type"  name = "rech_type">
                         <option value="ALL" selected>ALL</option>
                        <option value="RECHARGE" >RECHARGE</option>
                        <option value="BILL_PAYMENT" >BILL_PAYMENT</option>
                        <option value="BANK_TRANSFER" >BANK_TRANSFER</option>
                    </select>
                </div>
            </div>
        </div>

        
        <div class="row" id = "c1">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_api_result_type">User Name</label>
                    <select class="form-control" id="id_user_name"  name = "user_name">
                        <option value="" >---Select--</option>
                        <?php
                            $str = "";
                            foreach($user1 as $f)
                            {
                                $str = $str."<option value='".$f->user_name."'>".$f->user_name."-".$f->user_code."-".$f->personal->user_per_name."</option>";
                            }
                            echo $str;
                        ?>
                    </select>
                </div>
                
            </div>
            <div class="col-md-6" >
                <div class="form-group">
                    <label for="id_api_result_type">Recharge Status</label>
                    <select class="form-control" id="id_rech_status" name="rech_status">
                        <option value="-" >---Select--</option>
                        <option value="SUCCESS">SUCCESS</option>
                        <option value="FAILURE">FAILURE</option>
                        <option value="PENDING">PENDING</option>    
                    </select>
                </div>
                
            </div>
        </div>

        <div class="row" id = "c2">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_rech_mobile">Mobile No</label>
                    <input type="text" class="form-control" id="id_rech_mobile" name="rech_mobile">
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_rech_amount">Amount</label>
                    <input type="text" class="form-control" id="id_rech_amount" name="rech_amount">
                </div>

                
            </div>
        </div>

        <div class="row" id = "c3">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_net_code">Network</label>
                    <select class="form-control" id="id_net_code" name="net_code">
                       
                    </select>
                </div>
                
            </div>
            <div class="col-md-6">
                
            </div>
        </div>

       

       
       
     
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

   
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_f_date, #id_t_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
    {
        numbersOnly(e);
    });

     $("#id_rech_type").change(function(e)
    {
        var ty = $('option:selected', this).val();
       
        if(ty == "ALL")
        {
            $('#c3').hide("slow");
            $('#c2').hide("slow");
            $('#c1').hide("slow");
        }
        else if(ty == "RECHARGE")
        {
            var data = $("#web_code").val();
            loadNetwork(data);
            $('#c3').show("slow");
            $('#c2').show("slow");
            $('#c1').show("slow");
        
        }
        else if(ty == "BILL_PAYMENT")
        {
            var data = $("#bill_code").val();
            loadNetwork(data);
            $('#c3').show("slow");
            $('#c2').show("slow");
            $('#c1').show("slow");
        
        }
        else if(ty == "BANK_TRANSFER")
        {
            var data = $("#money_code").val();
            loadNetwork(data);
            $('#c3').show("slow");
            $('#c2').show("slow");
            $('#c1').show("slow");
        }
            
    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        $('#web_code').val("-");
        $('#bill_code').val("-");
        $('#money_code').val("-");
        $('#rech_form').attr('action', "{{url('rechargedetails_distributor_view')}}");
        $('#rech_form').submit();
        
        
    }); 

    function loadNetwork(data)
    {
        var net = JSON.parse(data);
        
        var net_code = $("#id_net_code");
        $(net_code).empty();
        var option1 = $("<option />");
            option1.html("select Network");
            option1.val("-");
            net_code.append(option1);

        $(net).each(function () {
            var option = $("<option />");
            option.html(this.net_name);
            option.val(this.net_code);
            net_code.append(option);
        });

        $('select').formSelect();
    }

    function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
   
});
</script>
@stop
@stop