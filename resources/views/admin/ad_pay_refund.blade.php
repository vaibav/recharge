@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Refund Payment</h2>
                <h5 class="text-white op-7 mb-3">Admin Refund Payment</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id= "rech_form" class="form-horizontal" action="{{url('refundpayment_store')}}" id="a_remote" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_f_date">User Name</label>
                     <select id="id_user_name"  name = "user_name" class="form-control">
                      <option value = '-'>----Select----</option>
                        <?php
                            $str = "";
                            foreach($user1 as $f)
                            {
                                $str = $str."<option value='".$f->user_name."'>".$f->user_name."--".$f->personal->user_per_name."</option>";
                            }
                            echo $str;
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class = "row" >
                    <div class ="col-md-6" >
                        <table class="bordered striped responsive-table ">
                            <tr>
                                <td style='font-size:12px;padding:7px 8px;'>USER NAME</td>
                                <th style='font-size:12px;padding:7px 8px;' id="u_n"></th>
                            </tr>
                            <tr>
                                <td style='font-size:12px;padding:7px 8px;'>BALANCE</td>
                                <th style='font-size:12px;padding:7px 8px;' id="u_b"></th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_t_date">Refund Amount</label>
                    <input type="text" class="form-control" id="id_user_amount" name="user_amount">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_t_date">Remarks</label>
                    <input type="text" class="form-control" id="id_user_remarks" name="user_remarks">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <br>
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
        </div>
       
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

    <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                              <th style='font-size:12px;padding:7px 8px;'>NO</th>
                              <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                              <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                              <th style='font-size:12px;padding:7px 8px;'>AMOUNT</th>
                              <th style='font-size:12px;padding:7px 8px;'>DATE</th>
                              <th style='font-size:12px;padding:7px 8px;'>MODE</th>
                              <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                              <th style='font-size:12px;padding:7px 8px;'>REMARKS</th>
                              
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                        <?php 
                                  
                            $j = 1;
                            foreach($pay1 as $f)
                            {
                              echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->parent_name."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_date."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_type."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_status."</td>";
                              echo "<td style='font-size:12px;padding:7px 8px;'>".$f->reply_id."</td>";
                              echo "</tr>";
                              $j++;
                            }
                            
                                
                        ?>

                        </tbody>
                    </table>
                    
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>        

@section('scripts')

<?php
    if(session()->has('result'))
    {
        $op = session('result');
        
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
        
        session()->forget('result');
    }
?>

<script>
$(document).ready(function() {

    $("#id_user_amount").keydown(function (e) 
    {
        numbersOnly(e);
    });

    $("#id_user_name").change(function(e)
    {
        var un = $('option:selected', this).val();
    
        if(un != "-")
        {
           getUserBalance(un);
        }
            
    });

    function getUserBalance(u_n)
    {
        
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        $('#loader').show(); 

        $.ajax({
            url:'get_ub/' + u_n,
            type: 'GET',
            success: function( response ){
                $('#u_b').text(response);
                $('#u_n').text(u_n);
                $('#id_user_name').formSelect();
                $('#loader').hide(); 
            },
            error: function (xhr, b, c) {
                console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                $('#loader').hide(); 
            }
        });
    }

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        $('#btn_submit').prop('disabled', true);
        var form = $(this).parents('form');
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Transfer!'
                }).then((result) => {
                if (result.value) {
                    startloader();
                    $('#rech_form').attr('action', "{{url('refundpayment_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "No Transfer...", "error");
                    $('#btn_submit').prop('disabled', false);
                }
            });
        
        
    }); 

     function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }

    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
   
});
</script>
@stop
@stop