<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Pending Report</span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tran.Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API.Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>User</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Request</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API Pr</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Opr ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                      
                                        foreach($new_recharge as $f)
                                        {
                                           
                                                                                        
                                            $net_name = "";
                                            foreach($network as $r)
                                            {
                                                if($f->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }

                                            $api_name = "";
                                            $req_status = "";

                                            foreach($api as $r)
                                            {
                                                if($f->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                                            if($f->request != null)
                                            {
                                                
                        
                                                $req_status = $f->request->req_id;
                                            }
                                           

                                            
                                            /*foreach($recharge4 as $r)
                                            {
                                                if($f->trans_id == $r->trans_id)
                                                    $req_status = $r->rech_req_status;
                                            }*/

                                            $rech_status = $f->rech_status;
                                            $rech_option = $f->rech_option;

                                           if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                
                                            }
                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                            {      
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                               
                                            }
                                            
                                            if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->api_trans_id."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
    
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                                
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                echo "<td  style='font-size:12px;padding:7px 8px;white-space:pre-wrap;'><div style='width: 180px;word-break: break-word;'>".$req_status."</div></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$api_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;' id='mode_".$f->trans_id."'>".$f->rech_mode."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'><input type='text' id='opr_".$f->trans_id."' class='form-control' /></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'>";
                                                echo "<button class='btn-floating btn-small waves-effect' id='success_".$f->trans_id."'>
                                                <i class='small material-icons'>edit</i></button>&nbsp;&nbsp;&nbsp;";
                                                echo "<button class='btn-floating btn-small waves-effect red' id='failure_".$f->trans_id."'>
                                                <i class='small material-icons'>close</i></button>&nbsp;&nbsp;&nbsp;";
                                                echo "<button class='btn-floating btn-small waves-effect amber' id='reprocess_".$f->trans_id."'>
                                                <i class='small material-icons '>directions_run</i></button></td>";
                                            }
                                            
                                            
                                            echo "</tr>";
                                                                                    
                                            $j++;
                                        }
                                    ?>

                            </tbody>
                        </table>
                      
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        $net_code = 0;
        $mob = 0;
        $net_type = 0;
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            
            
            
            
            
            session()->forget('result');
        }
    ?>

    <script>
    $(document).ready(function() 
    {
        
        
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("success_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Success it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            var opr = $('#opr_' + code).val();
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/pendingreport_success/" + code + "/" + opr;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                var nid1=gid.split("failure_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Failed it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            var opr = $('#opr_' + code).val();
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/pendingreport_failure/" + code + "/" + opr;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                var nid2=gid.split("reprocess_");
                var net=0;
                var ctx=0;
                if(nid2.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Reprocess it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid2[1];
                            var mode = $('#mode_' + code).text();
                            alert(code +"----"+ mode);
                            if(mode == "API")
                                window.location.href = "<?php echo url('/'); ?>/pendingreport_reprocessapi/" + code;
                            else
                                window.location.href = "<?php echo url('/'); ?>/pendingreport_reprocess/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                }

                
            });

        
        

        
    });
    </script>
    </body>
</html>
