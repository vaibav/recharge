@extends('layouts.admin_new')
@section('styles')
    <style>
        table td {font-size:11px !important;padding:4px 4px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Distributor Recharge</h2>
                <h5 class="text-white op-7 mb-3">Distributor / Super Distributor Recharge Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               <a href="{{url('rechargedetails_distributor')}}" class="btn btn-secondary btn-round">Back</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                <div class="table-responsive">
                    <table class="display table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th style="font-size:12px;padding:7px 8px;">NO</th>
                            <th style="font-size:12px;padding:7px 8px;">PARENT NAME</th>
                            <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                            <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                            <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                            <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                            <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                            <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                            <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                            <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                           
                              
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                            <?php 
                                
                                $str = "";
                                $j = 1;

                                foreach($recharge as $d)
                                {
                                    $net_name = "";
                                    foreach($d2 as $r)
                                    {
                                        if($d->net_code == $r->net_code)
                                            $net_name = $r->net_name;
                                    }

                                    $reply_id = $d->reply_id;
                                    $reply_date = $d->reply_date;

                                    $status = "";
                                    $d_o_bal = 0;
                                    $s_o_bal = 0;
                                    $e = 0;

                                    $rech_status = $d->rech_status;
                                    $rech_option = $d->rech_option;
                                    $d_u_bal = $d->dis_bal;
                                    $d_r_tot = $d->dis_total;
                                    $s_u_bal = $d->sup_bal;
                                    $s_r_tot = $d->sup_total;

                                    if($rech_status == "PENDING" && $rech_option == "0")
                                    {
                                        $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                        $d_o_bal = floatval($d_u_bal) + floatval($d_r_tot);
                                        $s_o_bal = floatval($s_u_bal) + floatval($s_r_tot);
                                    }
                                    else if($rech_status == "PENDING" && $rech_option == "2")
                                    {
                                        $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                        $d_o_bal = floatval($d_u_bal) + floatval($d_r_tot);
                                        $s_o_bal = floatval($s_u_bal) + floatval($s_r_tot);
                                    }
                                    else if($rech_status == "FAILURE" && $rech_option == "2")
                                    {      
                                        $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                        $d_o_bal = floatval($d_u_bal) - floatval($d_r_tot);
                                        $s_o_bal = floatval($s_u_bal) - floatval($s_r_tot);
                                        $e = 1;
                                    }
                                    else if ($rech_status == "SUCCESS")
                                    {
                                        $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                        $d_o_bal = floatval($d_u_bal) + floatval($d_r_tot);
                                        $s_o_bal = floatval($s_u_bal) + floatval($s_r_tot);
                                    }

                                    if($e == 1) {
                                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                                    }
                                    else {
                                        $str = $str."<tr>";
                                    }

                                    if($u_type == "-")
                                    {
                                        
                                        // For Distributor
                                        $str = $str."<td>".$j."</td>";
                                        $str = $str."<td>".$d->child_name."</td>";
                                        $str = $str."<td>".$d->user_name."</td>";
                                        $str = $str."<td>".$d->rech_mobile."</td>";
                                        $str = $str."<td>".$net_name."</td>";
                                        $str = $str."<td>".$d->rech_amount."</td>";

                                        //percentage amount
                                        $per = floatval($d->dis_net_per);
                                        $peramt1 = 0;
                                        $peramt2 = 0;
                                        if(floatval($per) != 0)
                                        {
                                            $peramt1 = floatval($per) / 100;
                                            $peramt1 = round($peramt1, 4);
                                    
                                            $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                            $peramt2 = round($peramt2, 2);
                                        }

                                        $str = $str."<td>".$d->dis_net_per."--".$peramt2."--".$d->dis_net_surp."</td>";
                                        $str = $str."<td>".$d->dis_total."</td>";
                                        $str = $str."<td>".$d->trans_id."</td>";
                                        $str = $str."<td>".$reply_id."</td>";
                                        $str = $str."<td>".$d->rech_date."</td>";
                                        $str = $str."<td>".$reply_date."</td>";
                                        $str = $str."<td>".$status."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($d_o_bal,2, ".", "")."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($d_u_bal,2, ".", "")."</td></tr>";

                                        // For Super Distributor
                                        if($e == 1) {
                                            $str = $str."<tr style='background-color:#E8DAEF;'>";
                                        }
                                        else {
                                            $str = $str."<tr>";
                                        }

                                        $str = $str."<td>".$j."</td>";
                                        $str = $str."<td>".$d->parent_name."</td>";
                                        $str = $str."<td>".$d->user_name."</td>";
                                        $str = $str."<td>".$d->rech_mobile."</td>";
                                        $str = $str."<td>".$net_name."</td>";
                                        $str = $str."<td>".$d->rech_amount."</td>";

                                        //percentage amount
                                        $per = floatval($d->sup_net_per);
                                        $peramt1 = 0;
                                        $peramt2 = 0;
                                        if(floatval($per) != 0)
                                        {
                                            $peramt1 = floatval($per) / 100;
                                            $peramt1 = round($peramt1, 4);
                                    
                                            $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                            $peramt2 = round($peramt2, 2);
                                        }

                                        $str = $str."<td>".$d->sup_net_per."--".$peramt2."--".$d->sup_net_surp."</td>";
                                        $str = $str."<td>".$d->sup_total."</td>";
                                        $str = $str."<td>".$d->trans_id."</td>";
                                        $str = $str."<td>".$reply_id."</td>";
                                        $str = $str."<td>".$d->rech_date."</td>";
                                        $str = $str."<td>".$reply_date."</td>";
                                        $str = $str."<td>".$status."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($s_o_bal,2, ".", "")."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($s_u_bal,2, ".", "")."</td></tr>";

                                    }
                                    else if($u_type == "DISTRIBUTOR")
                                    {
                                        // For Distributor
                                        if($u_name == $d->child_name)
                                        {
                                            $str = $str."<td>".$j."</td>";
                                            $str = $str."<td>".$d->child_name."</td>";
                                            $str = $str."<td>".$d->user_name."</td>";
                                            $str = $str."<td>".$d->rech_mobile."</td>";
                                            $str = $str."<td>".$net_name."</td>";
                                            $str = $str."<td>".$d->rech_amount."</td>";

                                            //percentage amount
                                            $per = floatval($d->dis_net_per);
                                            $peramt1 = 0;
                                            $peramt2 = 0;
                                            if(floatval($per) != 0)
                                            {
                                                $peramt1 = floatval($per) / 100;
                                                $peramt1 = round($peramt1, 4);
                                        
                                                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                                $peramt2 = round($peramt2, 2);
                                            }

                                            $str = $str."<td>".$d->dis_net_per."--".$peramt2."--".$d->dis_net_surp."</td>";
                                            $str = $str."<td>".$d->dis_total."</td>";
                                            $str = $str."<td>".$d->trans_id."</td>";
                                            $str = $str."<td>".$reply_id."</td>";
                                            $str = $str."<td>".$d->rech_date."</td>";
                                            $str = $str."<td>".$reply_date."</td>";
                                            $str = $str."<td>".$status."</td>";
                                            $str = $str."<td  style='text-align:right;'>".number_format($d_o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='text-align:right;'>".number_format($d_u_bal,2, ".", "")."</td></tr>";
                                        }
                                        

                                    }
                                    else if($u_type == "SUPER DISTRIBUTOR")
                                    {
                                        // For Distributor
                                        if($u_name == $d->parent_name)
                                        {
                                            // For Super Distributor
                                        if($e == 1) {
                                            $str = $str."<tr style='background-color:#E8DAEF;'>";
                                        }
                                        else {
                                            $str = $str."<tr>";
                                        }

                                        $str = $str."<td>".$j."</td>";
                                        $str = $str."<td>".$d->parent_name."</td>";
                                        $str = $str."<td>".$d->user_name."</td>";
                                        $str = $str."<td>".$d->rech_mobile."</td>";
                                        $str = $str."<td>".$net_name."</td>";
                                        $str = $str."<td>".$d->rech_amount."</td>";

                                        //percentage amount
                                        $per = floatval($d->sup_net_per);
                                        $peramt1 = 0;
                                        $peramt2 = 0;
                                        if(floatval($per) != 0)
                                        {
                                            $peramt1 = floatval($per) / 100;
                                            $peramt1 = round($peramt1, 4);
                                    
                                            $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                            $peramt2 = round($peramt2, 2);
                                        }

                                        $str = $str."<td>".$d->sup_net_per."--".$peramt2."--".$d->sup_net_surp."</td>";
                                        $str = $str."<td>".$d->sup_total."</td>";
                                        $str = $str."<td>".$d->trans_id."</td>";
                                        $str = $str."<td>".$reply_id."</td>";
                                        $str = $str."<td>".$d->rech_date."</td>";
                                        $str = $str."<td>".$reply_date."</td>";
                                        $str = $str."<td>".$status."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($s_o_bal,2, ".", "")."</td>";
                                        $str = $str."<td  style='text-align:right;'>".number_format($s_u_bal,2, ".", "")."</td></tr>";
                                        }
                                        

                                    }
                                                                            
                                    $j++;
                                }
                                
                                echo $str; 
                                
                            ?>

                        </tbody>
                    </table>
                    {{ $recharge->links('vendor.pagination.atlantis') }}
                </div>
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");

    
   
  
   
});
</script>
@stop
@stop