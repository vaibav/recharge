@extends('layouts.admin_default')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div style = "height:20px;margin-top:-25px;text-align:right;padding-right:15px;">
            <a href ="{{url('ad_entry')}}" class= "btn btn-primary btn-fab  btn-round"><i class='small material-icons' style='color:white'>add</i></a>
        </div>
        <div class="card-body table-responsive">

            <div class="row">
            <div class="col-md-12">
            <table class="table table-hover ">
                <thead class="text-primary">
                <tr>
                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                    <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                    <th style='font-size:12px;padding:7px 8px;'>Advertisement</th>
                    <th style='font-size:12px;padding:7px 8px;'>Order</th>
                    <th style='font-size:12px;padding:7px 8px;'>From Date</th>
                    <th style='font-size:12px;padding:7px 8px;'>To Date</th>
                    <th style='font-size:12px;padding:7px 8px;'>Image</th>
                    <th style='font-size:12px;padding:7px 8px;'>Status</th>
                    <th style='font-size:12px;padding:7px 8px;'>Action</th>
                </tr>
                </thead>
                <tbody id="tbl_body">
                <?php 
                            
                    $j = 1;
                    foreach($add as $f)
                    {
                        echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ad_data."</td>";
                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ad_order."</td>";
                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->from_date."</td>";
                        echo "<td style='font-size:12px;padding:7px 8px;'>".$f->too_date."</td>";
        
                        if($f->ad_photo != "")
                        {
                            echo "<td style='font-size:12px;padding:7px 8px;'>";
                            echo "<img src ='".url("/")."/img/adv/".$f->ad_photo."' style='height:50px;width:100px;' alt='NO PHOTO'/>";
                            echo "</td>";
                        }
                        else 
                        {
                            echo "<td></td>";
                        }
                        

                        if($f->ad_status == 1)
                            echo "<td style='font-size:12px;padding:7px 8px;'>LIVE</td>";
                        else 
                            echo "<td style='font-size:12px;padding:7px 8px;'>IN-ACTIVE</td>";
                        
                        echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'>";
                        echo "<button class='btn btn-warning btn-fab btn-fab-mini btn-round' id='inactive_".$f->trans_id."'>
                        <i class='small material-icons' style='color:white;'>edit</i></button>&nbsp;&nbsp;&nbsp;";
                        echo "<button class='btn btn-danger btn-fab btn-fab-mini btn-round' id='delete_".$f->trans_id."'>
                        <i class='small material-icons' style='color:white;'>close</i></button></td>";
                        
                        
                        echo "</tr>";
                            $j++;
                    }
                    
                ?>

                </tbody>
            </table>
            
            </div>
            
            </div>
            
            <br><br><br>
          
            
           
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    </div>
    
</div>
<!-- Code Ending-->
            
          

@section('scripts')

<?php
if(session()->has('msg'))
{
    $op = session('msg');
    if($op == 1)
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Alert!', 'Advertisement is deleted Successfully...', 'success'); 
        });
        </script>";
    }
    
    else
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Error!', 'Sorry...', 'error'); 
        });
        </script>";
    }
    session()->forget('msg');
}
?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("inactive_");
        if(nid.length>1)
        {
            var code = nid[1];
            window.location.href = "<?php echo url('/'); ?>/ad_edit/" + code;
        }

        var nid1=gid.split("delete_");
        if(nid1.length>1)
        {
            var code = nid1[1];
            window.location.href = "<?php echo url('/'); ?>/ad_delete/" + code;
            
        }
        
    });


  
   
});
</script>
@stop
@stop