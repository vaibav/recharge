<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')

        <link rel="stylesheet" href="{{ asset('css/mstepper.min.css') }}">

        <style>
            .select-dropdown {
                top: 0 !important;
                overflow: scroll;
                
            }
        </style>
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">API Provider Entry</span>
                </div>

                <div class="card-content" style = "border-radius:5px;padding:5px 5px;">
                    <form id="id_api_form" class="form-horizontal" action="{{url('apirequest_store')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" id = "id_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="pr_code" id="id_pr_code" value="0">
                    <ul class="stepper horizontal demos" id="horizontal" style = "margin:2px 2px;">
                        <li class="step">
                            <div data-step-label="Network Entry" class="step-title waves-effect waves-dark">API Details</div>
                            <div class="step-content">
                                
                                <div class="row #ede7f6 #ffcdd2 red lighten-4" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_pr_name" type="text" name="pr_name" >
                                        <span class="text-danger red-text">{{ $errors->first('pr_name') }}</span>
                                        <label for="id_pr_name">Provider Name</label>
                                    </div>
                                    <div class="input-field col s12 m12 l8 xl8" style="margin: 3px 0px;">
                                        <input id="id_pr_url" type="text" name="pr_url" >
                                        <span class="text-danger red-text">{{ $errors->first('pr_url') }}</span>
                                        <label for="id_pr_url">URL</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class ="col s12 m12 l12 xl12" style ="height:300px;overflow-y:scroll;">
                                        <table class="bordered striped responsive-table" >
                                            <thead>
                                                <tr>
                                                    <th style='font-size:12px;padding:7px 8px;'>Parameter Type</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Parameter Name</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Value</th> 
                                                </tr>
                                            </thead>
                                            <tbody id="tbl_body">
                                                <?php 
                                                    $j = 1;
                                                    for($j = 1; $j <= 18; $j++)
                                                    {
                                                        echo "<tr><td style='font-size:12px;padding:4px 8px;'>";
                                                        echo "<select id='id_para_type_".$j."' name='para_type_".$j."' class='form-control'>";
                                                        echo "<option value='-'>---SELECT---</option>";
                                                        echo "<option value='PRE_DEFINED'>PRE_DEFINED</option>";
                                                        echo "<option value='USER_DEFINED'>USER_DEFINED</option>";
                                                        echo "<option value='TRANSACTION_ID'>TRANSACTION_ID</option>";
                                                        echo "<option value='NETWORK'>NETWORK</option>";
                                                        echo "<option value='NETWORK_REPLACE'>NETWORK_REPLACE</option>";
                                                        echo "<option value='MOBILE_NO'>MOBILE_NO</option>";
                                                        echo "<option value='MESSAGE'>MESSAGE</option>";
                                                        echo "<option value='AMOUNT'>AMOUNT</option></select></td>";

                                                        echo "<td style='font-size:12px;padding:4px 8px;'>";
                                                        echo "<input type='text' id='id_para_name_".$j."' name='para_name_".$j."' class='form-control'></td>";

                                                        echo "<td style='font-size:12px;padding:4px 8px;'>";
                                                        echo "<input type='text' id='id_para_value_".$j."' name='para_value_".$j."' class='form-control'></td></tr>";
                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue next-step">CONTINUE</button>
                                </div>
                            </div>
                        </li>
                        <li class="step">
                            <div class="step-title waves-effect waves-dark">Network Entry</div>
                            <div class="step-content">
                                
                                <div class="row">
                                    <div class ="col s12 m12 l12 xl12" style ="height:300px;overflow-y:scroll;">
                                        <table class="bordered striped responsive-table">
                                            <thead>
                                                <tr>
                                                    <th style='font-size:12px;padding:7px 8px;'>Select</th>
                                                    <th style='font-size:12px;padding:7px 8px;display:none;'>Network ID</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Network Name</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Network Code</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Percentage</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbl_body">
                                                <?php 
                                                    $j = 1;
                                                    foreach($network as $f)
                                                    {
                                                        echo "<tr><td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<label class='switch switch-label switch-pill switch-primary'>
                                                        <input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select_".$j."' style='display:none;'>
                                                        <span class='switch-slider' data-checked='On' data-unchecked='Off'></span></label></td>";
                                                        
                                                        echo "<td style='font-size:12px;padding:2px 8px;display:none'>";
                                                        echo "<input type='hidden' id='id_net_code_".$j."' name='net_code_".$j."' value='".$f->net_code."' /></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_name_".$j."' name='net_name_".$j."' class='form-control' value='".$f->net_name."' /></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_short_".$j."' name='net_short_".$j."' class='form-control'/></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_per_".$j."' name='net_per_".$j."' class='form-control' /></td></tr>";

                                                        $j++;
                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue" type="submit" id = "btn_submit">SUBMIT</button>
                                    <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
                                </div>
                            </div>
                        </li>
                        
                    </ul>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->
        
        
        <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Important Note !</h4>

            <ul>
                    <li  class = "pink-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                         Type API Provider name as Capital Letters.</li>
                    <li  class = "blue-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                            Type API Provider name without Spaces</li>
                    <li class = "pink-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                                If Get Method - Put "?" at end of the API Url <br>
                                &nbsp;&nbsp;&nbsp;&nbsp;Example - http://www.domainname.com/api_program.php?
                            </li>
                    <li class = "blue-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                        If POST Method - Dont Put "?" at end of the API Url <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;Example - http://www.domainname.com/api_program.php
                    </li>

                    <li class = "pink-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                        If Recharge API - Add Mobile_No, Amount, Transaction_id, Network Parameters
                        <br> &nbsp;&nbsp;&nbsp;&nbsp;Compulsory 
                    </li>

                    <li class = "blue-text" style="font-size:18px;"><i class="material-icons tiny red-text">done_all</i>
                        After Completing this Process - Add API Result Parameters Compulsory 
                    </li>
                    
                  </ul>
        
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn">Agree</a>
        </div>
    </div>

    @include('admin.bottom1')

    <script src="{{ asset('js/mstepper.min.js') }}"></script>

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('.modal').modal('open');

            var stepper = document.querySelector('.stepper');
            var stepperInstace = new MStepper(stepper, {
                // options
                firstActive: 0 // this is the default
            })
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var o = checkParameter();
                var c = $('#id_pr_url').val();
                if(o == 0)
                {
                    if(isUrlValid(c))
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Submit!'
                            }).then((result) => {
                            if (result.value) {
                                
                                $('#id_api_form').attr('action', "{{url('apirequest_store')}}");
                                $('#id_api_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                
                            }
                        });
                    }
                    else
                    {
                        swal("Cancelled", "Invalid URL...", "error");
                    }
                }
                else
                {
                    swal("Cancelled", "Duplicate Parameter Type...", "error");
                }
               
                
                
            }); 

            function checkParameter()
            {
                var i = 1;
                var o = 0;
                var p = 0;
                var q = 0;
                var r = 0;
                var s = 0;
                var a = "";
                
                for(i = 1; i <= 8; i++)
                {
                    a = $('#id_para_type_' + i).val();
                   
                    if(a == "TRANSACTION_ID")
                        p++;
                    if(a == "NETWORK")
                        q++;
                    if(a == "MOBILE_NO")
                        r++;
                    if(a == "AMOUNT")
                        s++;
                }

                if(p > 1 || q > 1 || r > 1 || s > 1)
                    o = 1;
                
                return o;
            }

            function isUrlValid(url) {
                    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
            }
      });
    </script>
    </body>
</html>
