@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">API Provider Result</h2>
                <h5 class="text-white op-7 mb-3">Result Entry</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{url('n_apirequest')}}" class="btn btn-secondary btn-round">API Provider</a>
                <a href="{{url('n_apirequest_network')}}" class="btn btn-secondary btn-round">API Network</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('n_apiresult_store')}}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_api_code">Provider Name</label>
                    <select class="form-control" id="id_api_code" name="api_code">
                    <option value="" >---Select--</option>
                    <?php
                        foreach($api as $f)
                        {
                            if($f->api_code == $api_code)
                                echo "<option value='".$f->api_code."' selected>".$f->api_name."</option>";
                            else
                                echo "<option value='".$f->api_code."' >".$f->api_name."</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_api_result_type">Result Type</label>
                    <select class="form-control" id="id_api_result_type" name="api_result_type">
                        <option value = "DIRECT">DIRECT</option>
                        <option value = "INDIRECT">INDIRECT</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_api_res_para_name">Parameter</label>
                    <select class="form-control" id="id_api_res_para_name" name="api_res_para_name">
                        <option value="-">--Select--</option>
                        <option value="STATUS 1">STATUS-SUCCESS</option>
                        <option value="STATUS 2">STATUS-FAILED</option>
                        <option value="MY_TRANSACTION_ID">MY TRANSACTION ID</option>
                        <option value="MOBILE NO">MOBILE NO</option>
                        <option value="AMOUNT">AMOUNT</option>
                        <option value="OPERATOR">OPERATOR</option>
                        <option value="OPERATOR_ID">OPERATOR ID</option>
                        <option value="BALANCE">BALANCE AMT</option>
                        <option value="SERVER_REPLY_STATUS">SERVER REPLY STATUS</option>
                        <option value="DATA">DATA</option>
                        <option value="ARRAY">ARRAY</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_api_res_para_field">Parameter Field(Type a1, a2... for position or enter parameter name)</label>
                    <input type="text" class="form-control" id="id_api_res_para_field" name="api_res_para_field">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_api_res_para_value">Parameter Value</label>
                    <input type="text" class="form-control" id="id_api_res_para_value" name="api_res_para_value">
                </div>
            </div>
            
        </div>
     
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Save</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

    <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                            <th style='font-size:12px;padding:7px 8px;'>Code</th>
                            <th style='font-size:12px;padding:7px 8px;'>Provider</th>
                            <th style='font-size:12px;padding:7px 8px;'>Param Name</th>
                            <th style='font-size:12px;padding:7px 8px;'>Param Field</th>
                            <th style='font-size:12px;padding:7px 8px;'>Param Value</th>
                            <th style='font-size:12px;padding:7px 8px;'>Action</th>
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                    <?php 
                        $j = 1;
                        foreach($api2 as $f)
                        {
                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                            echo "<td id='code_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_tr_id."</td>";
                            echo "<td id='name_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_code."</td>";
                            echo "<td id='short_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_name."</td>";
                            echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_field."</td>";
                            echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_value."</td>";

                            echo "<td style='font-size:12px;padding:7px 8px;'>";
                            echo "<button class='btn btn-small btn-danger' id='delete_".$f->api_res_tr_id."'>
                            <i class='small material-icons '>clear</i></td></tr>";

                            $j++;
                        }
                    ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_api_code').change(function(e) {

        var api_code = $('option:selected', this).val();
        if(api_code != "-")
        {
            window.location.href = "<?php echo url('/'); ?>/n_apiresult/"+api_code;
        }

    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('n_apiresult_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        
        
    }); 

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid1=gid.split("delete_");
        var net=0;
        var ctx=0;
        if(nid1.length>1)
        {
            swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete!'
                    }).then((result) => {
                    if (result.value) {
                        
                        var api_tr_id = nid1[1];
                        window.location.href = "<?php echo url('/'); ?>/n_apiresult_delete/"+api_tr_id;
                    }
                    else
                    {
                        swal("Cancelled", "Sorry....", "error");
                        
                    }
                });
            
        }
    });
  
   
});
</script>
@stop
@stop


