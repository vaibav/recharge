<!-- BEGIN: Header-->
<header class="page-topbar " id="header">
    <div class="navbar navbar-fixed"> 
    <nav class="navbar-main title-pink nav-collapsible sideNav-lock navbar-dark  ">
        <div class="nav-wrapper">
        
        <ul class="navbar-list right">
            <li><a class="dropdown-trigger profile-button" href="javascript:void(0);" 
                data-target="profile-dropdown"><i class="material-icons">more_vert</i></a></li>
        </ul>
        
        <!-- profile-dropdown-->
        <ul class="dropdown-content" id="profile-dropdown" style="width:350px;">
            <li><a class="grey-text text-darken-1" href="#">{{$uname}}</a></li>
            <li><a class="grey-text text-darken-1" href="#">{{$bal}}</a></li>
            <li class="divider"></li>
            <li><a class="grey-text text-darken-1" href="{{url('changepassword_retailer')}}"><i class="material-icons">lock_outline</i> Change Pwd</a></li>
            <li><a class="grey-text text-darken-1" href="{{url('logout')}}"><i class="material-icons">keyboard_tab</i> Logout</a></li>
        </ul>
        </div>
        
    </nav>
    </div>
</header>
<!-- END: Header-->

<!-- BEGIN: SideNav-->
 <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
    <div class="brand-sidebar">
    <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="#"><img src="{{ asset('img/brand/logo.png') }}" alt="materialize logo"/></a>
        <a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
    <li class="{{$act1[0]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
        <i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">Dashboard</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li class="active"><a class="collapsible-body {{$act[0]}}" href="{{url('dashboard')}}" data-i18n="">
            <i class="material-icons">radio_button_unchecked</i><span>Recharge</span></a>
            </li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[1]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">cloud</i><span class="menu-title" data-i18n="">Server</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('serveronoff')}}" class="waves-effect waves-cyan {{$act[1]}}"  data-i18n="">ON/OFF</a></li>
        </ul>
        </div>
    </li>

     <li class="{{$act1[2]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">account_balance</i><span class="menu-title" data-i18n="">Network</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('network_type')}}" class="waves-effect waves-cyan {{$act[2]}}"  data-i18n="">Network Type Entry</a></li>
            <li><a href="{{url('network')}}" class="waves-effect waves-cyan {{$act[3]}}"  data-i18n="">Network Entry</a></li>
            <li><a href="{{url('network_surplus')}}" class="waves-effect waves-cyan {{$act[4]}}"  data-i18n="">Network Surplus Entry</a></li>
            <li><a href="{{url('network_line')}}" class="waves-effect waves-cyan {{$act[5]}}"  data-i18n="">Network Line</a></li>
            <li><a href="{{url('network_line_apipartner')}}" class="waves-effect waves-cyan {{$act[6]}}"  data-i18n="">API Partner Line</a></li>
            <li><a href="{{url('offer_line')}}" class="waves-effect waves-cyan {{$act[7]}}"  data-i18n="">Other Line </a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[3]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">account_balance</i><span class="menu-title" data-i18n="">API</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('apirequest')}}" class="waves-effect waves-cyan {{$act[8]}}"  data-i18n="">API Request Entry</a></li>
            <li><a href="{{url('apirequest_edit')}}" class="waves-effect waves-cyan {{$act[9]}}"  data-i18n="">API Request Edit</a></li>
            <li><a href="{{url('apiresult')}}" class="waves-effect waves-cyan {{$act[10]}}"  data-i18n="">API Result Entry</a></li>
            <li><a href="{{url('apiprovider_method')}}" class="waves-effect waves-cyan {{$act[11]}}"  data-i18n="">API Method Entry</a></li>
            <li><a href="{{url('api_details_1')}}" class="waves-effect waves-cyan {{$act[12]}}"  data-i18n="">API URL Details</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[4]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">cloud</i><span class="menu-title" data-i18n="">User</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('user')}}" class="waves-effect waves-cyan {{$act[13]}}"  data-i18n="">User Creation</a></li>
            <li><a href="{{url('user_view')}}" class="waves-effect waves-cyan {{$act[14]}}"  data-i18n="">User Details</a></li>
            <li><a href="{{url('user_approval_view')}}" class="waves-effect waves-cyan {{$act[15]}}"  data-i18n="">Parent Approval</a></li>
            <li><a href="{{url('changeparent')}}" class="waves-effect waves-cyan {{$act[16]}}"  data-i18n="">Change Parent</a></li>
            <li><a href="{{url('apiagent')}}" class="waves-effect waves-cyan {{$act[17]}}"  data-i18n="">Apipartner Browser</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[5]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">account_balance</i><span class="menu-title" data-i18n="">Payment</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('opening_balance_view')}}" class="waves-effect waves-cyan {{$act[18]}}"  data-i18n="">Opening Balance</a></li>
            <li><a href="{{url('remotepayment')}}" class="waves-effect waves-cyan {{$act[19]}}"  data-i18n="">Remote Payment</a></li>
            <li><a href="{{url('paymentaccept')}}" class="waves-effect waves-cyan {{$act[20]}}"  data-i18n="">Payment Accept</a></li>
            <li><a href="{{url('refundpayment')}}" class="waves-effect waves-cyan {{$act[21]}}"  data-i18n="">Refund Payment</a></li>
            <li><a href="{{url('payment_report')}}" class="waves-effect waves-cyan {{$act[22]}}"  data-i18n="">Payment Details</a></li>
            <li><a href="{{url('stockdetails')}}" class="waves-effect waves-cyan {{$act[23]}}"  data-i18n="">Stock Details</a></li>
            <li><a href="{{url('admin_user_stock_1')}}" class="waves-effect waves-cyan {{$act[24]}}"  data-i18n="">User Stock Details</a></li>
            <li><a href="{{url('backup_distributor_all')}}" class="waves-effect waves-cyan {{$act[25]}}"  data-i18n="">Distributor Stock Details</a></li>
            <li><a href="{{url('backup_view_all')}}" class="waves-effect waves-cyan {{$act[26]}}"  data-i18n="">Retailer Stock Details</a></li>
            <li><a href="{{url('stockdetails_all')}}" class="waves-effect waves-cyan {{$act[27]}}"  data-i18n="">All Stock </a></li>
        </ul>
        </div>
    </li>

    <li class=" {{$act1[6]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">money</i><span class="menu-title" data-i18n="">Collection</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('agent_allocation')}}" class="waves-effect waves-cyan {{$act[28]}}"  data-i18n="">Agent Allocation</a></li>
            <li><a href="{{url('collection')}}" class="waves-effect waves-cyan {{$act[29]}}"  data-i18n="">Pending Report</a></li>
            <li><a href="{{url('collection_verify')}}" class="waves-effect waves-cyan {{$act[30]}}"  data-i18n="">Collection Verify</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[7]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">timeline</i><span class="menu-title" data-i18n="">Report</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('rechargedetails')}}" class="waves-effect waves-cyan {{$act[31]}}"  data-i18n="">Recharge Details</a></li>
            <li><a href="{{url('rechargedetails_distributor')}}" class="waves-effect waves-cyan {{$act[32]}}"  data-i18n="">Distributor Recharge</a></li>
            <li><a href="{{url('rechargerequest')}}" class="waves-effect waves-cyan {{$act[33]}}"  data-i18n="">Recharge Request</a></li>
            <li><a href="{{url('serverreply')}}" class="waves-effect waves-cyan {{$act[34]}}"  data-i18n="">Server Reply</a></li>
            <li><a href="{{url('pendingreport')}}" class="waves-effect waves-cyan {{$act[35]}}"  data-i18n="">Pending Recharge</a></li>
            <li><a href="{{url('pendingbill')}}" class="waves-effect waves-cyan {{$act[36]}}"  data-i18n="">Pending EBBill</a></li>
            <li><a href="{{url('pendingmoney')}}" class="waves-effect waves-cyan {{$act[37]}}"  data-i18n="">Pending Money Transfer</a></li>
            <li><a href="{{url('rechargeinfo')}}" class="waves-effect waves-cyan {{$act[38]}}"  data-i18n="">Recharge Info</a></li>
            <li><a href="{{url('admin_user_old_recharge_1')}}" class="waves-effect waves-cyan {{$act[39]}}"  data-i18n="">Old Recharge</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[8]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">add_alert</i><span class="menu-title" data-i18n="">Money Transfer</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('bank_agent_view_all')}}" class="waves-effect waves-cyan {{$act[40]}}"  data-i18n="">Agent</a></li>
            <li><a href="{{url('bank_remitter_view_all')}}" class="waves-effect waves-cyan {{$act[41]}}"  data-i18n="">Remitter</a></li>
            <li><a href="{{url('admin_ben_view_1')}}" class="waves-effect waves-cyan {{$act[42]}}"  data-i18n="">Beneficiary</a></li>
            <li><a href="{{url('moneyreport')}}" class="waves-effect waves-cyan {{$act[43]}}"  data-i18n="">Report</a></li>
        </ul>
        </div>
    </li>

    <li class="{{$act1[9]}} bold"><a class="collapsible-header waves-effect waves-cyan " href="#">
            <i class="material-icons">timeline</i><span class="menu-title" data-i18n="">Utilities</span></a>
        <div class="collapsible-body">
        <ul class="collapsible collapsible-sub" data-collapsible="accordion">
            <li><a href="{{url('smsdetails')}}" class="waves-effect waves-cyan {{$act[44]}}"  data-i18n="">Sms Details</a></li>
            <li><a href="{{url('smssend')}}" class="waves-effect waves-cyan {{$act[45]}}"  data-i18n="">Sms Send</a></li>
            <li><a href="{{url('complaint')}}" class="waves-effect waves-cyan {{$act[46]}}"  data-i18n="">Complaints</a></li>
            <li><a href="{{url('complaint_view')}}" class="waves-effect waves-cyan {{$act[47]}}"  data-i18n="">Complaint Details</a></li>
            <li><a href="{{url('offer_entry')}}" class="waves-effect waves-cyan {{$act[48]}}"  data-i18n="">Flash News</a></li>
            <li><a href="{{url('offer_view')}}" class="waves-effect waves-cyan {{$act[49]}}"  data-i18n="">Flash News details</a></li>
            <li><a href="{{url('mobile_user')}}" class="waves-effect waves-cyan {{$act[50]}}"  data-i18n="">Mobile User</a></li>
            <li><a href="{{url('bonrixresult')}}" class="waves-effect waves-cyan {{$act[51]}}"  data-i18n="">Bonrix Result</a></li>
        </ul>
        </div>
    </li>

    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->