@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Recharge Request</h2>
                <h5 class="text-white op-7 mb-3">Recharge Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               <a href="{{url('rechargerequest')}}" class="btn btn-secondary btn-round">Back</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       
        <!-- End -->
        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                            <th style="font-size:12px;padding:7px 8px;">NO</th>
                            <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                            <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                            <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                            <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                            <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                            <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">REQUEST</th>
                            <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                            <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                            <?php 
                                
                                    
                                    $str = "";
                                    
                                    $j = 1;
                                    foreach($recharge as $d)
                                    {
                                        $net_name = "";
                                        foreach($d2 as $r)
                                        {
                                            if($d->net_code == $r->net_code)
                                                $net_name = $r->net_name;
                                        }

                                        $api_name = "";
                                        $reply_id = "NA";
                                        $reply_date = "";
                                        $req_id = "";
                                        foreach($d3 as $r)
                                        {
                                            if($d->api_code == $r->api_code)
                                                $api_name = $r->api_name;
                                        }

                                        if($d->request != null) {
                                            $req_id = $d->request->req_id;
                                        }
                
                                        $reply_id = $d->reply_id;
                                        $reply_date = $d->reply_date;

                                        $rech_status = $d->rech_status;
                                        $rech_option = $d->rech_option;
                                        $r_tot = $d->ret_tot;

                                        $status = "";

                                        if($rech_status == "PENDING" && $rech_option == "0")
                                        {
                                            $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                            
                                            $str = $str."<tr>";
                                        }
                                        else if($rech_status == "PENDING" && $rech_option == "2")
                                        {
                                            $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                           
                                            $str = $str."<tr>";
                                        }
                                        else if($rech_status == "FAILURE" && $rech_option == "2")
                                        {      
                                            $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                            
                                            $str = $str."<tr style='background-color:#E8DAEF;'>";
                                        }
                                        else if ($rech_status == "SUCCESS")
                                        {
                                            $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                            
                                            $str = $str."<tr>";
                                        }

                                        //percentage amount
                                        $per = floatval($d->ret_net_per);
                                        $peramt1 = 0;
                                        $peramt2 = 0;
                                        if(floatval($per) != 0)
                                        {
                                            $peramt1 = floatval($per) / 100;
                                            $peramt1 = round($peramt1, 4);
                                    
                                            $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                            $peramt2 = round($peramt2, 2);
                                        }
                                    
                                        if($d->trans_type == "RECHARGE" || $d->trans_type == "BANK_TRANSFER")
                                        {
                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$peramt2."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->ret_net_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$req_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$reply_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                            }
                                            else if($d->rech_status == "FAILURE")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                 $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$req_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            
                                        
                                        }
                                        if($d->trans_type == "BILL_PAYMENT1")
                                        {
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->billpayment[0]->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }
                            
                                            $api_name = "";
                                            foreach($d3 as $r)
                                            {
                                                if($d->billpayment[0]->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                            
                                            $rech_status = "";
                                            $status = "";
                                            $o_bal = 0;
                                            $u_bal = 0;
                                            $r_tot = 0;
                            
                                        
                            
                                            if($d->trans_option == 1)
                                            {
                                                $str = $str."<tr>";
                                                $rech_status = $d->billpayment[0]->con_status;
                                                $rech_option = $d->billpayment[0]->con_option;          
                                                $r_tot = $d->billpayment[0]->con_total;
                                                $u_bal = $d->billpayment[0]->user_balance;
                                            
                                            }
                                            else if($d->trans_option == 2)
                                            {  
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                $rech_status = $d->billpayment[1]->con_status;
                                                $rech_option = $d->billpayment[1]->con_option;          
                                                $r_tot = $d->billpayment[1]->con_total;
                                                $u_bal = $d->billpayment[1]->user_balance;
                                                
                                            }
                                            
                                            if($rech_status == "PENDING" && $rech_option == 1)
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == 2)
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if($rech_status == "FAILURE"  && $rech_option == 2)
                                            {      
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                                //$u_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn-floating btn-small waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                            }
                                            
                                            
                                            
                            
                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                                            if($d->trans_option == 1)
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;white-space:pre-wrap;'><div style='width: 180px;word-break: break-word;'>".$d->billpayment[0]->reply_opr_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                                            }
                                            else if($d->trans_option == 2)
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                        } 
                                        
                                                                                
                                        $j++;
                                    }
                                    
                                    echo $str; 
                                

                                
                                ?>

                        </tbody>
                    </table>
                    {{ $recharge->links('vendor.pagination.atlantis') }}
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_f_date, #id_t_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

   
    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();

        var fd = $('#id_f_date').val();
        var td = $('#id_t_date').val();
        var ur = $('#id_user_name').val();
        
        if(fd != "" && td != "" && ur != "-")
        {
            startloader();
            $('#rech_form').attr('action', "{{url('n_check_account_2')}}");
            $('#rech_form').submit();
        }
        else{
            swal("Cancelled", "Sorry...Anyone Date is Empty", "error");
        }
        
    }); 

   



    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop