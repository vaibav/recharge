@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">User</h2>
                <h5 class="text-white op-7 mb-3">User Entry</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
       <form id="id_api_form" class="form-horizontal" action="{{url('user_store')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user_code" id="id_user_code" value="0">
        <input type="hidden" name="user_api_url_1" value="-">
        <input type="hidden" name="user_api_url_2" value="-">

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_nam">User Name</label>
                    <input type="text" class="form-control" id="id_user_name"  name="user_name">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_pwd">Password</label>
                    <input type="password" class="form-control" id="id_user_pwd"  name="user_pwd">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_setup_fee">Setup Fee</label>
                    <input type="text" class="form-control" id="id_user_setup_fee" name="user_setup_fee">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_acc_type">Account Type</label>
                    <select id="id_user_acc_type" name="user_acc_type" class="form-control">
                        <option value="-">---Select---</option>
                        <option value="SUPER DISTRIBUTOR">SUPER_DISTRIBUTOR</option>
                        <option value="AGENT">AGENT</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pack_id">Package</label>
                    <select id="id_pack_id" name="pack_id" class="form-control">
                        <option value="-">---Select---</option>
                       <?php
                            foreach($package as $f) {
                                echo "<option value='".$f->pack_id."' >".$f->pack_name."</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <label for="id_t_date">Category</label><br>
                <div class="form-group">
                    <label><input type="checkbox" id="id_user_rec_mode1" name="user_rec_mode[]" value="WEB"/><span>WEB &nbsp;</span></label>
                    <label><input type="checkbox" id="id_user_rec_mode2" name="user_rec_mode[]" value="GPRS" /><span>GPRS&nbsp;</span></label>
                    <label><input type="checkbox" id="id_user_rec_mode3" name="user_rec_mode[]" value="API" /><span>API&nbsp;</span></label>
                    <label><input type="checkbox" id="id_user_rec_mode4" name="user_rec_mode[]" value="SMS" /><span>SMS&nbsp;</span></label>
                </div>
            </div>
        </div>
        <hr>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_per_name">Full Name</label>
                    <input type="text" class="form-control" id="id_user_per_name"  name="user_per_name">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="d_user_state">Area</label>
                    <input type="text" class="form-control" id="id_user_state" name="user_state">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_city">City</label>
                    <input type="text" class="form-control" id="id_user_city"  name="user_city">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_mobile">Mobile</label>
                    <input type="text" class="form-control" id="id_user_mobile"  name="user_mobile">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_phone">WhatsApp No</label>
                    <input type="text" class="form-control" id="id_user_phone" name="user_phone">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_mail">Mail Id</label>
                    <input type="text" class="form-control" id="id_user_mail"  name="user_mail">
                </div>
            </div>
        </div>
        <hr>

         <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_kyc">Select KYC</label>
                    <select id="id_user_kyc" class="form-control" name="user_kyc" >
                        <option value="-">--Select--</option>
                        <option value="Driving_License">Driving License</option>
                        <option value="Voter_ID">Voter ID</option>
                        <option value="Ration card">Ration Card</option>
                        <option value="Aadhar card">Aadhar Card</option>
                        <option value="Pan card">Pan Card</option>
                        <option value="Passport">Passport</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_kyc_proof">KYC Upload</label>
                    <input type="file" class="form-control" id="id_user_kyc_proof" name="user_kyc_proof"> 
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_user_photo">Photo Upload</label>
                    <input type="file" class="form-control" id="id_user_photo" name="user_photo"> 
                </div>
            </div>
        </div>
        <hr>
     
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

   
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>


@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

 <?php
    if(session()->has('msg'))
    {
        $op = session('msg');
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op."', 'success'); 
            });
            </script>";
        session()->forget('msg');
    }
?>


<script>
$(document).ready(function() {

    $("#id_user_phone, #id_user_mobile, #id_user_setup_fee").keydown(function (e) 
    {
        numbersOnly(e);
    });

    $("#id_user_name").blur(function(e) 
    {
        e.preventDefault();
        var user_name = $("#id_user_name").val();
        $('#loader').show();

        if (user_name != "")
        {
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: "<?php echo url('/'); ?>/check_user/" + user_name,
                success: function (data) {
                    if(data == 1)
                    {
                        $('#loader').hide();
                        swal("Alert", "User Name Already Exists....", "error");
                        $("#id_user_name").val("");
                        $("#id_user_name").focus();
                    }
                    else
                    {
                        $('#loader').hide();
                    }
                },
                error: function() { 
                    $('#loader').hide();
                    console.log(data);
                }
            });
        }
        else
        {
            $('#loader').hide();
            $("#id_user_name").focus();
        }
           
    });

    $("#id_user_mobile").blur(function(e) 
    {
        e.preventDefault();
        var user_mobile = $("#id_user_mobile").val();
        $('#loader').show();

        if (user_mobile != "")
        {
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: "<?php echo url('/'); ?>/check_mobile/" + user_mobile,
                success: function (data) {
                    if(data > 0)
                    {
                        $('#loader').hide();
                        swal("Alert", "Mobile No Already Exists....", "error");
                        $("#id_user_mobile").val("");
                        $("#id_user_mobile").focus();
                    }
                    else
                    {
                        $('#loader').hide();
                    }
                },
                error: function() { 
                    $('#loader').hide();
                    console.log(data);
                }
            });
        }
        else
        {
            $('#loader').hide();
            $("#id_user_mobile").focus();
        }
           
    });
    

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        swal({
            title: 'Are you sure?',
            text: "Confirmation Alert",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Submit!'
            }).then((result) => {
            if (result.value) {
                
                $('#id_api_form').attr('action', "{{url('user_store')}}");
                $('#id_api_form').submit();
            }
            else
            {
                swal("Cancelled", "Sorry...", "error");
                
            }
        });
       
        
        
    }); 


    function numbersOnly(e)
    {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) 
      {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    }
   
});
</script>
@stop
@stop