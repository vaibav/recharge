<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('user.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style ='background-color: #0E6655;'>
        @include('user.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('user.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('user.title', array('title' => 'Remote Payment', 'uname' => $user->user, 'bal' => $user->ubal))

        <div id ="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: url({{ asset('img/loader/3.gif') }}) 50% 50% no-repeat rgba(249,249,249, 0.5) ;display:none;" ></div>

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i> Remote Payment</div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                          <div class ="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                              <form class="form-horizontal" action="{{url('remotepayment_store_user')}}" id = "u_remote" method="post" accept-charset="UTF-8">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="user_code" id="id_user_code" value="">
                                  
                                  <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal">User Name</label>
                                    <div class="col-sm-8">
                                      <input class="form-control" id="id_user_name" type="text" name="user_name" placeholder="">
                                      <span class="text-danger">{{ $errors->first('user_name') }}</span>
                                        <div>
                                                <select id="id_user_list" size="10" class="form-control" style=" display:none;" >
                                                <?php
                                                    $str = "";
                                                    foreach($user1 as $f)
                                                    {
                                                        $per_name = "";
                                                        foreach($user2 as $r)
                                                        {
                                                            if($f->user_code == $r->user_code)
                                                            {
                                                                $per_name = $r->user_per_name;
                                                                break;
                                                            }
                                                        }
                                                        $str = $str."<option value='".$f->user_code."'>".$f->user_name."-".$f->user_code."-".$per_name."</option>";
                                                    }
                                                    echo $str;
                                                ?>
                                                </select>
                                         </div>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal">Transfer Amount</label>
                                    <div class="col-sm-8">
                                      <input class="form-control" id="id_user_amount" type="text" name="user_amount" placeholder="">
                                      <span class="text-danger">{{ $errors->first('user_amount') }}</span>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="input-normal">Remarks</label>
                                    <div class="col-sm-8">
                                      <input class="form-control" id="id_user_remarks" type="text" name="user_remarks" placeholder="">
                                      <span class="text-danger">{{ $errors->first('user_remarks') }}</span>
                                    </div>
                                  </div>

                                   
                                 
                                  
                                  <div class="form-group row" style="padding:6px 15px;">
                                      <button class="btn btn-sm btn-primary" type="submit" id = "btn_submit">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>
                                  </div>

                                  <div class="form-group row" style="padding:6px 15px;">
                                        <?php
                                          if(session()->has('result'))
                                          {
                                            $op = session('result');
                                            echo $op['output'];
                                          }
                                        ?> 
                                         
                                  </div>

                                </form>
                          </div>
                          <div class ="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                              <table class="table table-responsive-sm table-bordered">
                                  <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Transferred Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Payment Mode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                                      
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body">
                                    <?php 
                                        
                                        $j = 1;
                                        foreach($pay1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->grant_user_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->grant_date."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->payment_mode."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_remarks."</td>";
                                          echo "</tr>";
                                          $j++;
                                        }
                                        
                                    ?>
                                    
                                  </tbody>
                                </table>
                          </div>
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('user.footer')
    @include('user.bottom')

    <script>
     $(document).ready(function() 
	   {
            $('#id_user_name').keyup(function(e) 
            {
                //$('#myModal').modal();
                filter();
            });
            $('#id_user_name').blur(function(e) 
            {
                //$('#myModal').modal();
                $('#id_user_list').hide("slow");
            });

            $("#id_user_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            function filter() {
                var keyword = document.getElementById("id_user_name").value;
                var select = document.getElementById("id_user_list");
                for (var i = 0; i < select.length; i++) {
                    var txt = select.options[i].text;
                    if (txt.substring(0, keyword.length).toLowerCase() !== keyword.toLowerCase() && keyword.trim() !== "") {
                        select.options[i].style.display = 'none';
                    } else {
                        select.options[i].style.display = 'list-item';
                    }
                }
                $('#id_user_list').show("slow");
            }
            $('#id_user_list').click(function(e) 
            {
                //$('#myModal').modal();
                
                var a =$('#id_user_list :selected').text();
                var b = a.split("-");
                //alert(b[0]);
                $('#id_user_name').val(b[0]);
                $('#id_user_code').val(b[1]);
                //$('#pur_a3').val(b[1]);
                $('#id_user_list').hide("slow");
            });
            
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#btn_submit').prop('disabled', true);
                var form = $(this).parents('form');
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Transfer!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#a_remote').attr('action', "{{url('remotepayment_store_user')}}");
							              $('#a_remote').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No Transfer...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                
                
            }); 

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
               
     });
    </script>
    </body>
</html>
