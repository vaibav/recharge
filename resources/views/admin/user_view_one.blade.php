@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">User Edit</h2>
                <h5 class="text-white op-7 mb-3">User Details Edit Option</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       
        <div class="card">
            <div class="card-body">
                 <form id="id_user_form"  action="{{url('user_update')}}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-4">
                       
                            <div class="table-responsive">
                                <table class="display table table-striped table-hover ">
                                    <thead></thead>
                                    <tbody>
                                        <?php 
                                        $user_code = "NONE";
                                        $user_kyc2 = "";
                                        $user_rmode = "";
                                        
                                        foreach($user1 as $f)
                                        {

                                            $user_code = $f->user_code;
                                            $user_kyc2 = $f->user_kyc;

                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>NAME</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control'  id='id_user_per_name' name='user_per_name' value ='".$f->user_per_name."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_per_name')."</span></td></tr>";


                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>AREA</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_state' name='user_state' value='".$f->user_state."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_state')."</span></td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>CITY</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_city'  name='user_city' value='".$f->user_city."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_city')."</span></td></tr>";
                                            
                                           
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>MOBILE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_mobile' name='user_mobile' value='".$f->user_mobile."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_mobile')."</span></td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>WHATSAPP</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_phone' name='user_phone' value='".$f->user_phone."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_phone')."</span></td></tr>";
                                            
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>MAIL</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_mail' name='user_mail' value='".$f->user_mail."' />";
                                                echo "<span class='text-danger'>". $errors->first('user_mail')."</span></td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>KYC</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_kyc' name='user_kyc' value='".$f->user_kyc."' /></td></tr>";
                                
                                        }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                    
                        </div>

                        <div class="col-md-4">
                             <div class="table-responsive">
                                <table class="display table table-striped table-hover ">
                                    <thead></thead>
                                    <tbody>
                                        <?php 
                                      
                                        
                                        foreach($user1 as $f)
                                        {
                                            $user_rmode = $f->account->user_rec_mode;

                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>USER NAME</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_name."</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>PASSWORD</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_pwd."</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>SET UP FEE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_setup_fee."</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>USER TYPE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->user_type."</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>PARENT NAME</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->parent_name."</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>PARENT TYPE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->account->parent_type."</td></tr>";

                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>PACKAGE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><select id='id_pack_id' name='pack_id' class='form-control'>";

                                                foreach($package as $r) {

                                                        if($r->pack_id == $f->account->pack_id)
                                                            echo "<option value='".$r->pack_id."' selected>".$r->pack_name."</option>";
                                                        else
                                                            echo "<option value='".$r->pack_id."' >".$r->pack_name."</option>";
                                                }
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>RECHARGE MODE</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_rech_mode' name='user_rech_mode' value='".$f->account->user_rec_mode."' /></td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>API SUCCESS URL</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_api_url_1'  name='user_api_url_1' value='".$f->account->user_api_url_1."' /></td></tr>";
                    
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>API FAILURE URL</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' class='form-control' id='id_user_api_url_2' name='user_api_url_2' value='".$f->account->user_api_url_2."' /></td></tr>";
  
                                
                                        }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="table-responsive">
                                <table class="display table table-striped table-hover ">
                                    <thead></thead>
                                    <tbody>
                                        <?php 
                                       
                                        
                                        foreach($user1 as $f)
                                        {
                                            
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>PHOTO</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadphoto/".$f->user_photo."' style='height:120px;width:120px;' alt='NO PHOTO'/>";
                                                echo "<input class='form-control' id='id_user_photo' type='file' name='user_photo' placeholder='JPEG only'>";
                                                
                                                echo "</td></tr>";
                                            
                                            echo "<tr><th style='font-size:12px;padding:7px 8px;'>KYC PROOF</th>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                echo "<img src ='".url("/")."/uploadkyc/".$f->user_kyc_proof."' style='height:120px;width:120px;' alt='NO PHOTO'/>";
                                                echo "<input class='form-control' id='id_user_kyc_proof' type='file' name='user_kyc_proof' placeholder='JPEG only'>";
                                                
                                                echo "</td></tr>";
                                            
                                           
                                
                                        }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                    
                        </div>
                    </div>

                    <input type = "hidden" id="id_user_code" name = "user_code" value="<?php echo $user_code; ?>">
                    <input type = "hidden" id="id_user_kyc2" name = "user_kyc2" value="<?php echo $user_kyc2; ?>">
                    <input type = "hidden" id="id_user_rmode" name = "user_rmode" value="<?php echo $user_rmode; ?>">

                    <!-- End Body --> 
                    <div class = "row">
                        <div class ="col-md-8 ">
                            <center><input type = "submit" class = "btn btn-primary" id="btn_submit" name="btn_update" value = "UPDATE" /></center>
                            
                        </div>
                        <div class ="col-md-4 ">
                            <center><input type = "submit" class = "btn btn-secondary" id="btn_submit1" name="btn_update" value = "UPDATE PHOTO" /></center>
                            
                        </div>
                                
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>

<!-- KYC Modal Structure -->
<div class="modal" id="kycModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">KYC Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row" style = "margin-bottom:8px;">
            <div class="col-sm-12 col-md-8" style="margin: 3px 0px;">
                <label>Select KYC</label>
                <select id="id_user_kyc1" name="user_kyc1" class="form-control">
                    <option value="-">--Select--</option>
                    <option value="Driving_License">Driving License</option>
                    <option value="Voter_ID">Voter ID</option>
                    <option value="Ration card">Ration Card</option>
                    <option value="Aadhar card">Aadhar Card</option>
                    <option value="Pan card">Pan Card</option>
                    <option value="Passport">Passport</option>
                </select>
                
            </div>
        
            <div class="col-sm-12 col-md-8" style="margin: 3px 0px;">
                <button type="button" class="btn btn-secondary" id="btn_search">Submit</button>
                
            </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
    

     <!-- KYC Modal Structure -->
     <div class="modal" id="rmodeModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Recharge Mode Details</h5>
                
              </div>
              <div class="modal-body">
                <div class="row" style = "margin-bottom:10px;">
                    <div class="col-md-2" style="margin: 3px 0px;">
                        <label><input type="checkbox" id="id_user_rec_mode1" name="user_rec_mode" value="WEB"/><span>WEB &nbsp;</span></label>
                    </div>
                    <div class="col-md-2" style="margin: 3px 0px;">
                        <label><input type="checkbox" id="id_user_rec_mode2" name="user_rec_mode" value="GPRS" /><span>GPRS&nbsp;</span></label>
                    </div>
                    <div class="col-md-2" style="margin: 3px 0px;">
                        <label><input type="checkbox" id="id_user_rec_mode3" name="user_rec_mode" value="API" /><span>API&nbsp;</span></label>
                    </div>
                    <div class="col-md-2" style="margin: 3px 0px;">
                        <label><input type="checkbox" id="id_user_rec_mode4" name="user_rec_mode" value="SMS" /><span>SMS&nbsp;</span></label>
                    </div>
                    <div class="col-md-2" style="margin: 3px 0px;">
                        <button type="button" class="btn btn-secondary" id="btn_search1">Submit</button>
                    </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
     <div id="rmodeModal" class="modal modal-fixed-footer">
        <div class="modal-content">
                <h4></h4>
               
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat"  id="cx1">Close</a>
        </div>
    </div>
            
          

@section('scripts')

<?php
    if(session()->has('msg'))
    {
        $op = session('msg');
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op."', 'success'); 
            });
            </script>";
        session()->forget('msg');
    }
?>


<script>
$(document).ready(function() {
    //alert("hellow");
   var kyc = "";
        var rmd = "";
        

        $("#id_user_phone, #id_user_mobile").keydown(function (e) 
        {
            numbersOnly(e);
        });

        $('#id_user_kyc').keyup(function(e) 
        {
            kyc = $('#id_user_kyc2').val();
            $('#kycModal').modal('show');
           
            
        });

        $('#btn_search').on('click',function(e)
        {
            e.preventDefault();
            $('#kycModal').modal('hide');
            selectKYC();
           
        });

        function selectKYC()
        {
            var kyc1 = $('#id_user_kyc1').val();
            if(kyc1 == "-")
            {
                $('#id_user_kyc').val(kyc);
            }
            else
            {
                $('#id_user_kyc').val(kyc1);
            }   
        }
        
       

        $('#id_user_rech_mode').keyup(function(e) 
        {
            rmd = $('#id_user_rmode').val();
            $('#rmodeModal').modal('show');
            
        });

        $('#btn_search1').on('click',function(e)
        {
            e.preventDefault();
            $('#rmodeModal').modal('hide');
            selectRechMode();
           
        });

        function selectRechMode()
        {
            var str = "";
            $('input[name="user_rec_mode"]:checked').each(function() {
                str = str + this.value + "@";
            }); 
            if(str == "")
            {
                $('#id_user_rech_mode').val(rmd);
            }
            else
            {
                $('#id_user_rech_mode').val(str);
            }
        }

        

        $('#btn_submit').on('click',function(e)
        {
            e.preventDefault();
            var form = $(this).parents('form');
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Update it!'
                }).then((result) => {
                if (result.value) {
                    form.submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
            
        }); 

        $('#btn_submit1').on('click',function(e)
        {
            e.preventDefault();
            var form = document.getElementById('id_user_form');
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Upload it!'
                }).then((result) => {
                if (result.value) {
                    form.setAttribute("method","POST");
                    form.setAttribute("enctype","multipart/form-data");
                    form.setAttribute("action", "{{ url('user_update_photo') }}");
                    form.submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
            
        }); 

        function numbersOnly(e)
        {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) 
            {
                    // let it happen, don't do anything
                    return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }           
    
   
  
   
});
</script>
@stop
@stop