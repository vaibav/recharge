<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                <span class="card-title" style = "padding:12px;">Pending Collection Details</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                <p>Current Area : <?php echo $area_name; ?></p>
                

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                            <table class="bordered striped responsive-table ">
                                <thead>
                                    <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Full Name</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Taken</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Paid</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Pending</th>
                                        <th style='font-size:12px;padding:7px 8px;display:none;'>Pay</th>
                                        
                                </tr>
                                </thead>
                                <tbody id="tbl_body">
                                <?php 
                                            $j = 1;

                                            foreach($c_details as $f)
                                            {
                                            
                                            
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_name']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['per_name']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['user_mobile']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['de_tot']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['ce_tot']."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f['pe_tot']."</td>";
                                            
                                            

                                            echo "<td style='font-size:12px;padding:7px 8px;display:none;'>";
                                            echo "<button class='btn-floating btn-sm ' id='view_".$f['user_name']."'>
                                            Pay</td>";

                                            

                                            
                                            
                                            echo "</tr>";
                                                                                    
                                            $j++;
                                            }
                                        ?>
                                </tbody>
                            </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            
            if($op == 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $("#id_user_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#btn_submit').prop('disabled', true);
                var form = $(this).parents('form');
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Allocate!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('agent_allocation_store')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No Allocate...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });
                
                
            }); 

             function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

      });
    </script>
    </body>
</html>
