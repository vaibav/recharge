<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Reqeust Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargerequest')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>REQUEST</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>PROVIDER</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>MODE</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                    
                                        
                                        $str = "";
                                        
                                        $j = 1;
                                        foreach($recharge as $d)
                                        {
                                        
                                            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
                                            {
                                                if(sizeof($d->newrecharge1) > 0)
                                                {
                                                    $net_name = "";
                                                    foreach($d2 as $r)
                                                    {
                                                        if($d->newrecharge1[0]->net_code == $r->net_code)
                                                            $net_name = $r->net_name;
                                                    }
                                
                                                    $api_name = "";

                                                    $reply_id = "NA";
                                                    $reply_date = "";
                                                    foreach($d3 as $r)
                                                    {
                                                        if($d->newrecharge2->api_code == $r->api_code)
                                                            $api_name = $r->api_name;
                                                    }
                            
                                                    $request_id = $d->newrecharge2->rech_req_status;
                                                    $reply_id = $d->newrecharge2->reply_opr_id;
                                                    $reply_date = $d->newrecharge2->reply_date;
                                                
                                
                                                    $rech_status = "";
                                                    $status = "";
                                                    
                                                    $r_tot = 0;
                                
                                                    if($d->trans_option == 1)
                                                    {
                                                        $rech_status = $d->newrecharge1[0]->rech_status;
                                                        $rech_option = $d->newrecharge1[0]->rech_option;
                                                       
                                                        $str = $str."<tr>";
                                                    }
                                                    else if($d->trans_option == 2)
                                                    {
                                                        $rech_status = $d->newrecharge1[1]->rech_status;
                                                        $rech_option = $d->newrecharge1[1]->rech_option;
                                                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                    }
                                                    if($rech_status == "PENDING" && $rech_option == "0")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                        
                                                    }
                                                    else if($rech_status == "PENDING" && $rech_option == "2")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                        
                                                    }
                                                    else if($rech_status == "FAILURE" && $rech_option == "2")
                                                    {      
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                        
                                                    }
                                                    else if ($rech_status == "SUCCESS")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                        
                                                    }
                                
                                                    $mode = "WEB";
                                                    if($d->trans_id != "")
                                                    {
                                                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                                
                                                        $r_l = $matches[0][0];
                                
                                                        $r_l = substr($r_l, -1);
                                
                                                        if($r_l == "R")
                                                            $mode = "WEB";
                                                        else if($r_l == "A")
                                                            $mode = "API";
                                                        else if($r_l == "G")
                                                            $mode = "GPRS";
                                                        else if($r_l == "S")
                                                            $mode = "SMS";
                                
                                                    }
                                
                                                    $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->user_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_mobile."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_amount."</td>";
                                                    if($d->trans_option == 1)
                                                    {
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->rech_total."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->api_trans_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 240px;word-break: break-word;'>".$request_id."</div></td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_date."</td>";
                                                    }
                                                    else if($d->trans_option == 2)
                                                    {
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[1]->rech_total."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                    }
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                                }
                                                
                                            
                                            }
                                            if($d->trans_type == "BILL_PAYMENT")
                                            {
                                                $net_name = "";
                                                foreach($d2 as $r)
                                                {
                                                    if($d->billpayment[0]->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }
                                
                                                $api_name = "";
                                                foreach($d3 as $r)
                                                {
                                                    if($d->billpayment[0]->api_code == $r->api_code)
                                                        $api_name = $r->api_name;
                                                }
                                
                                                $rech_status = "";
                                                $status = "";
                                                $o_bal = 0;
                                                $u_bal = 0;
                                                $r_tot = 0;
                                
                                            
                                
                                                if($d->trans_option == 1)
                                                {
                                                    $str = $str."<tr>";
                                                    $rech_status = $d->billpayment[0]->con_status;
                                                    $rech_option = $d->billpayment[0]->con_option;          
                                                    $r_tot = $d->billpayment[0]->con_total;
                                                    $u_bal = $d->billpayment[0]->user_balance;
                                                
                                                }
                                                else if($d->trans_option == 2)
                                                {  
                                                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                    $rech_status = $d->billpayment[1]->con_status;
                                                    $rech_option = $d->billpayment[1]->con_option;          
                                                    $r_tot = $d->billpayment[1]->con_total;
                                                    $u_bal = $d->billpayment[1]->user_balance;
                                                    
                                                }
                                                
                                                if($rech_status == "PENDING" && $rech_option == 1)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                }
                                                else if($rech_status == "PENDING" && $rech_option == 2)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                }
                                                else if($rech_status == "FAILURE"  && $rech_option == 2)
                                                {      
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                }
                                                else if ($rech_status == "SUCCESS")
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                                }
                                                
                                                $mode = "WEB";
                                                if($d->trans_id != "")
                                                {
                                                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                                
                                                    $r_l = $matches[0][0];
                                
                                                    $r_l = substr($r_l, -1);
                                
                                                    if($r_l == "R")
                                                        $mode = "WEB";
                                                    else if($r_l == "A")
                                                        $mode = "API";
                                                    else if($r_l == "G")
                                                        $mode = "GPRS";
                                                    else if($r_l == "S")
                                                        $mode = "SMS";
                                
                                                }
                                                
                                
                                                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->user_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                                                if($d->trans_option == 1)
                                                {
                                                   
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->api_trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 240px;word-break: break-word;'>".$d->billpayment[0]->con_req_status."</div></td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                                                }
                                                else if($d->trans_option == 2)
                                                {
                                                   
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                }
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                            } 
                                                                       
                                            $j++;
                                        }
                                        
                                        echo $str; 
                                    

                                    
                                    ?>

                            </tbody>
                        </table>
                        {{ $recharge->links('vendor.pagination.materializecss') }}
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd'
            });

           $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
            {
                numbersOnly(e);
            });
            
            $('#print_view').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargedetails_view')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

            $('#print_excel').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargedetails_excel')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
      });
    </script>
    </body>
</html>
