@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Recharge Log</h2>
                <h5 class="text-white op-7 mb-3">Log Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{url('n_apirequest_network')}}" class="btn btn-secondary btn-round">API Network</a>
                <a href="{{url('n_apiresult')}}" class="btn btn-secondary btn-round">API Result</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="id_user_date">Date</label>
                    <input type="text" class="form-control" id="id_user_date" name="user_date">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="id_user_method">User Method</label>
                    <select class="form-control" id="id_user_method" name="user_method">
                        <option value ="ALL">ALL</option>
                        <option value ="WEB">WEB</option>
                        <option value ="GPRS">GPRS</option>
                        <option value ="API">API</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="id_user_name">User Name</label>
                    <select class="form-control" id="id_user_name" name="user_name">
                        <option value = "ALL">ALL</option>
                       <?php 
                            foreach($user_details as $d)
                            {
                                if($d->user_name == $u_name) {
                                    echo "<option value ='".$d->user_name."' selected>".$d->user_name."</option>";
                                }
                                else {
                                    echo "<option value ='".$d->user_name."'>".$d->user_name."</option>";
                                }
                            }
                       ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <br>
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
            
        </div>
            <div class="clearfix"></div>
        
        </div>
    </div>
    <br>

    <div class="card">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style='font-size:12px;padding:7px 8px;'>No</th>
                        <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                        <th style='font-size:12px;padding:7px 8px;'>Date</th>
                        <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                        <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                        <th style='font-size:12px;padding:7px 8px;'>IP</th>
                        <th style='font-size:12px;padding:7px 8px;'>Agent</th>
                    </tr>
                </thead>
                <tbody id="tbl_body">
                    <?php 
                        echo $data;
                    ?>
                    
                </tbody>
            </table>
        </div>
    </div>
    </div>

    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_user_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        var u_dt = $('#id_user_date').val();
        var u_mt = $('option:selected', '#id_user_method').val();
        var u_na = $('option:selected', '#id_user_name').val();

        window.location.href = "<?php echo url('/'); ?>/n_log_view?u_date=" +u_dt +"&u_method=" +u_mt + "&u_name="+ u_na;
    
        
    }); 

});
</script>
@stop
@stop


