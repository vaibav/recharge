<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style = "background-color: #34495e;">
      @include('admin.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('admin.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('admin.title', array('title' => 'Super Distributor Recharge Details', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                        <div class = "row">
                            <div class ="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <i class="fa fa-align-justify"></i> Super Distributor Recharge Details ---From-Date : <?php echo $from_date; ?> To-Date : <?php echo $to_date; ?> 
                            </div>
                            <div class ="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-right">
                                <input type = "text" class = "form-control" id= "icd" placeholder="search">
                                
                            </div>
                            <div class ="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-right">
                                
                                <a href="{{ url('rechargedetails')}}">  <button class="btn btn-sm btn-primary" >
                                          <i class="fa fa-arrow-left"></i>Back</button></a>
                            </div>
                        </div>

                    
                    
                    </div>
    
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                         
                          <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <table class="table table-responsive-sm table-bordered" id="tbl">
                              <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>SUPER DISTRIBUTOR NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>DISTRIBUTOR NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NETWORK</th>
                                      <th style='font-size:12px;padding:7px 8px;'>RECH.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NET.PER(%) / SURPLUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.AMT</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>OPR. TRN ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>R.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>UP.DATE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>O.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>C.BAL</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>MODE</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body">
                                  <?php 
                                        $j = 1;

                                       //print_r($recharge);
                                        foreach($recharge as $f1)
                                        {
                                            $f = $f1[0];

                                            $reply_id = "";
                                            $reply_date = "";
                                            $rech_status = "";
                                            $rech_mode = "";
                                            $o_bal = 0;
                                            $z = 0;
                                            
                                            if(sizeof($f) > 1)
                                            {
                                                // Web Recharge
                                                $net_name = "";
                                                foreach($network as $r)
                                                {
                                                    if($f[1]->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }

                                                //print_r($f[1]);

                                                if($f[1]->super_parent_name != "NONE")
                                                {
                                                    if($f[1]->rech_status == "PENDING")
                                                    {
                                                        $reply_id = "";
                                                        $reply_date = "";
                                                        if($f[1]->userrechargeparent->rech_status == "PENDING")
                                                            $rech_status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                                                        else
                                                        {
                                                            $rech_status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                            $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                                                            $reply_date = $f[1]->userrechargeparent->reply_date;
                                                        }
                                                            
                                                        $o_bal = floatval($f[1]->user_balance) + floatval($f[1]->rech_total);

                                                    }
                                                    else if ($f[1]->rech_status == "SUCCESS")
                                                    {
                                                        $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                                                        $reply_date = $f[1]->userrechargeparent->reply_date;
                                                        $rech_status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                                                        $o_bal = floatval($f[1]->user_balance) + floatval($f[1]->rech_total);
                                                    }
                                                    else if ($f[1]->rech_status == "FAILURE")
                                                    {
                                                        $reply_id = $f[1]->userrechargeparent->reply_opr_id;
                                                        $reply_date = $f[1]->userrechargeparent->reply_date;
                                                        $rech_status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                        $o_bal = floatval($f[1]->user_balance) - floatval($f[1]->rech_total);
                                                        $z = 1;
                                                    }

                                                    $rech_mode = $f[1]->userrechargeparent->rech_mode;
                                                }
                                                
                                                $o_bal = number_format($o_bal, 2, '.', '');
                                                $c_bal = floatval($f[1]->user_balance);
                                                $c_bal = number_format($c_bal, 2, '.', '');

                                                $mode = "WEB";
                                                if($f[1]->trans_id != "")
                                                {
                                                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$f[1]->trans_id, $matches);

                                                    $r_l = $matches[0][0];

                                                    $r_l = substr($r_l, -1);

                                                    if($r_l == "R")
                                                        $mode = "WEB";
                                                    else if($r_l == "A")
                                                        $mode = "API";
                                                    else if($r_l == "G")
                                                        $mode = "GPRS";
                                                    else if($r_l == "S")
                                                        $mode = "SMS";

                                                }

                                                if($z == 0)
                                                {
                                                    echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->super_parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->user_name."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_mobile."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$net_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_amount."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_net_per."<i class='icon-control-play ' style='color:blue;'></i> ".$f[1]->rech_net_per_amt."";
                                                    
                                                    echo "<i class='icon-control-play ' style='color:blue;'></i>".$f[1]->rech_net_surp."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f[1]->rech_total."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->trans_id."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$reply_id."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_date."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$reply_date."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$rech_status."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$mode."</td>";
                                                    
                                                    echo "</tr>";
                                                }
                                                else
                                                {
                                                    echo "<tr style='background-color:#E8DAEF;'><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->super_parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->user_name."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_mobile."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$net_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->rech_amount."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f[1]->rech_total."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[1]->trans_id."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$mode."</td>";
                                                    
                                                    echo "</tr>";
                                                }
                                            }
                                            else
                                            {
                                                // API Recharge
                                                $z = 0;
                                                $net_name = "";
                                                foreach($network as $r)
                                                {
                                                    if($f[0]->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }

                                                //print_r($f[0]);

                                                if($f[0]->super_parent_name == "NONE")
                                                {
                                                    if($f[0]->rech_status == "PENDING")
                                                    {
                                                        $reply_id = "";
                                                        $reply_date = "";
                                                        if($f[0]->userrechargeparent->rech_status == "PENDING")
                                                            $rech_status = "<span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span>";
                                                        else
                                                        {
                                                            $rech_status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                            $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                                                            $reply_date = $f[0]->userrechargeparent->reply_date;
                                                        }
                                                            
                                                        $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);

                                                    }
                                                    else if ($f[0]->rech_status == "SUCCESS")
                                                    {
                                                        $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                                                        $reply_date = $f[0]->userrechargeparent->reply_date;
                                                        $rech_status = "<span class='badge badge-success' style='padding:4px 4px;font-size:11px;'>SUCCESS</span>";
                                                        $o_bal = floatval($f[0]->user_balance) + floatval($f[0]->rech_total);
                                                    }
                                                    else if ($f[0]->rech_status == "FAILURE")
                                                    {
                                                        $reply_id = $f[0]->userrechargeparent->reply_opr_id;
                                                        $reply_date = $f[0]->userrechargeparent->reply_date;
                                                        $rech_status = "<span class='badge badge-danger' style='padding:4px 4px;font-size:11px;'>FAILURE</span>";
                                                        $o_bal = floatval($f[0]->user_balance) - floatval($f[0]->rech_total);
                                                        $z = 1;
                                                    }

                                                    $rech_mode = $f[0]->userrechargeparent->rech_mode;
                                                }
                                                
                                                $o_bal = number_format($o_bal, 2, '.', '');
                                                $c_bal = floatval($f[0]->user_balance);
                                                $c_bal = number_format($c_bal, 2, '.', '');

                                                if($z == 0)
                                                {
                                                    echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_name."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_mobile."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$net_name."</td>";
                                                     echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_amount."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_net_per."<i class='icon-control-play ' style='color:blue;'></i> ".$f[0]->rech_net_per_amt."";
                                                    
                                                      echo "<i class='icon-control-play ' style='color:blue;'></i>".$f[0]->rech_net_surp."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f[0]->rech_total."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->trans_id."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$reply_id."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_date."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$reply_date."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$rech_status."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$rech_mode."</td>";
                                                    
                                                    echo "</tr>";
                                                }
                                                else
                                                {
                                                    echo "<tr style='background-color:#E8DAEF;'><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->parent_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->user_name."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_mobile."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$net_name."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->rech_amount."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f[0]->rech_total."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'>".$f[0]->trans_id."</td>";

                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$o_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$c_bal."</td>";
                                                    echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$rech_mode."</td>";
                                                    
                                                    echo "</tr>";
                                                }
                                            }
                                            
                                                                                    
                                            $j++;
                                        }

                                    
                                      
                                    ?>
                                    
                                  </tbody>
                                </table>
                                {{ $recharge->links('pagination::bootstrap-4') }}
                          </div>
                      </div>
                     
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('admin.footer')
    @include('admin.bottom')

    <script>
     $(document).ready(function() 
			{
                $('#icd').keyup(function(e) 
                {
                    var val=$(this).val().toLowerCase();    
                    $('#tbl tbody tr').hide();
                    var trs=$('#tbl tbody tr').filter(function(d)
                    {
                        return $(this).text().toLowerCase().indexOf(val)!=-1;
                    });
                    trs.show();
                });
      });
    </script>
    </body>
</html>
