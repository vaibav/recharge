@extends('layouts.admin_default')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="row">
    <div class="col-md-12">
    <div class="card">
       
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('network_type_store')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="net_code" id="id_net_code" value="0">
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_net_type" >Enter Network Type</label>
                    <input id="id_net_type" type="text" name="net_type" class="form-control " >
                    <span class="text-danger red-text">{{ $errors->first('net_type') }}</span>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="form-group">
                
               
                </div>
            </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_ad_order">Status</label>
                    <select class="form-control " data-style="btn btn-link" id="id_net_type_status" name="net_type_status">
                        <option value="1">LIVE</option>
                        <option value="2">INACTIVE</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="form-group">
                
                </div>
            </div>
            </div>

            <br>
          
            
            <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Add</button>
               
            </div>
            </div>
            
            <div class="clearfix"></div>

            <div class = "row table-responsive">
                <div class = "col-md-12">
                <table class="table table-hover">
                    <thead class="text-warning">
                        <th>NO</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Action</th>
                    </thead>
                    <tbody id = "tbl_body">
                        <!-- Dynamic code here --> 
                        @php
                            $j = 1;
                            foreach($data as $f)
                            {
                                echo "<tr><td>".$j."</td>";
                                echo "<td id='code_".$j."'>".$f->net_type_code."</td>";
                                echo "<td id='type_".$j."'>".$f->net_type_name."</td>";

                                if($f->net_type_status == 1)
                                {
                                echo "<td id='status_".$j."'>LIVE</td>";
                                }
                                else
                                {
                                echo "<td id='status_".$j."'>INACTIVE</td>";
                                }
                                
                                $link = url("/")."/network_type_delete/".$f->net_type_code;

                                echo "<td><button  class='btn btn-warning btn-fab btn-fab-mini btn-round' id='edit_".$j."'>
                                <i class='small material-icons' style='color:white;'>edit</i> </button>&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo "<a class='btn btn-danger btn-fab btn-fab-mini btn-round' href='".$link."'>
                                <i class='small material-icons ' style='color:white;'>clear</i></a></td></tr>";
                                $j++;
                            }
                        @endphp
                    </tbody>
                </table>
                </div>
            </div>
        </form>
        </div>
    </div>
    </div>
    
</div>
<!-- Code Ending-->
            
          

@section('scripts')
<?php
if(session()->has('result'))
{
    $op = session('result');
    echo "<script>
    $(document).ready(function() 
    {
        swal('Alert!', '".$op['output']."', 'success'); 
    });
    </script>";
    session()->forget('result');
}
?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, View!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('network_type_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "No View...", "error");
                    
                }
            });
        
        
    });  

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("edit_");
        if(nid.length>1)
        {
                swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, View!'
                    }).then((result) => {
                    if (result.value) {
                        
                        var cd = $('#code_'+nid[1]).text();
                        var ty = $('#type_'+nid[1]).text();
                        var st = $('#status_'+nid[1]).text();

                        $('#id_net_code').val(cd);
                        $('#id_net_type').val(ty);

                       
                    }
                    else
                    {
                        swal("Cancelled", "Sorry....", "error");
                        
                    }
                });
            
        }
        var nid1=gid.split("delete_");
        var net=0;
        var ctx=0;
        if(nid1.length>1)
        {
            swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Deletes!'
                    }).then((result) => {
                    if (result.value) {
                        
                        var code = nid1[1];
                        window.location.href = "<?php echo url('/'); ?>/network_delete/"+code;
                    }
                    else
                    {
                        swal("Cancelled", "Sorry....", "error");
                        
                    }
                });
            
        }
    });

    $('#id_net_photo').on('change', function() {
        var a = this.files[0].size;
        if (a > 307200)
        {
            $('#id_net_photo').val(''); 
            swal("Alert", "File size is higher than 600KB", "error");
        }
    });

});
</script>
@stop
@stop