<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">API Provider Direct Result Entry</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('apidirectresult_store')}}" method="post" accept-charset="UTF-8">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="api_tr_id" id="id_api_tr_id" value="0">

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_api_code" name="api_code">
                                            <option value="" >---Select--</option>
                                            <?php
                                                foreach($api1 as $f)
                                                {
                                                    if($f->api_code == $api_code)
                                                        echo "<option value='".$f->api_code."' selected>".$f->api_name."</option>";
                                                    else
                                                        echo "<option value='".$f->api_code."' >".$f->api_name."</option>";
                                                }
                                            ?>
                                          </select>
                                          <label>API Provider</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_api_res_para_name" name="api_res_para_name">
                                                <option value="-">--Select--</option>
                                                <option value="STATUS 1">STATUS-SUCCESS</option>
                                                <option value="STATUS 2">STATUS-FAILED</option>
                                                <option value="MY_TRANSACTION_ID">MY TRANSACTION ID</option>
                                                <option value="MOBILE NO">MOBILE NO</option>
                                                <option value="AMOUNT">AMOUNT</option>
                                                <option value="OPERATOR">OPERATOR</option>
                                                <option value="OPERATOR_ID">OPERATOR ID</option>
                                                <option value="BALANCE">BALANCE AMT</option>
                                                <option value="SERVER_REPLY_STATUS">SERVER REPLY STATUS</option>
                                                <option value="DATA">DATA</option>
                                                <option value="ARRAY">ARRAY</option>
                                          </select>
                                          <label>Parameter Name</label>
                                    </div>
                                </div>


                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input type="text" id="id_api_res_para_field" name="api_res_para_field" >
                                        <span class="text-danger red-text">{{ $errors->first('api_res_para_field') }}</span>
                                        <label for="id_api_res_para_field">Parameter Field(Type a1,a2... for position)</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <input type="text" id="id_api_res_para_value" name="api_res_para_value" >
                                        <span class="text-danger red-text">{{ $errors->first('api_res_para_value') }}</span>
                                        <label for="id_api_res_para_value">Parameter Value</label>
                                    </div>
                                </div>
                                
                             

                                         
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                           
                                    </div>
                                        
                                </div>
                            </form>

                       <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                          <table class="bordered striped responsive-table">
                              <thead>
                                <tr>
                                     <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Code</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Provider</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Param Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Param Field</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Param Value</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Action</th>
                                </tr>
                              </thead>
                              <tbody id="tbl_body">
                                <?php 
                                    $j = 1;
                                    foreach($api2 as $f)
                                    {
                                        echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                        echo "<td id='code_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_tr_id."</td>";
                                        echo "<td id='name_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_code."</td>";
                                        echo "<td id='short_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_name."</td>";
                                        echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_field."</td>";
                                        echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->api_res_para_value."</td>";

                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                        echo "<button class='btn-floating btn-sm waves-effect waves-light red' id='delete_".$f->api_res_tr_id."'>
                                        <i class='small material-icons '>clear</i></td></tr>";

                                        $j++;
                                    }
                                ?>
                                
                              </tbody>
                          </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#id_api_code').change(function(e) {

                var api_code = $('option:selected', this).val();
                if(api_code != "-")
                {
                    window.location.href = "<?php echo url('/'); ?>/apidirectresult/"+api_code;
                }

            });
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Submit!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('apidirectresult_store')}}");
							$('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            
                        }
                    });
                
                
            }); 

            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid1=gid.split("delete_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                  swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Deletes!'
                            }).then((result) => {
                            if (result.value) {
                                
                                var api_tr_id = nid1[1];
                                window.location.href = "<?php echo url('/'); ?>/apidirectresult_delete/"+api_tr_id;
                            }
                            else
                            {
                                swal("Cancelled", "Sorry....", "error");
                                
                            }
                        });
                    
                }
            });

           
      });
    </script>
    </body>
</html>
