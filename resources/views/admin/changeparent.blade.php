<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Change Parent</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <form id = 'rech_form' class="form-horizontal" action="{{url('changeparent_store')}}" method="POST" accept-charset="UTF-8">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_code" id="id_user_code" value="<?php echo $user_code; ?>">
                       
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_user_name"  name = "user_name">
                                            <option value="" >---Select--</option>
                                            <?php
                                                $str = "";
                                                foreach($user1 as $f)
                                                {
                                                    if($f->user_code == $user_code)
                                                    {
                                                        $str = $str."<option value='".$f->user_name."' selected>".$f->user_name."-".$f->user_code."-".$f->personal->user_per_name."</option>";
                                                    }
                                                    else
                                                    {
                                                        $str = $str."<option value='".$f->user_name."'>".$f->user_name."-".$f->user_code."-".$f->personal->user_per_name."</option>";
                                                    }
                                                    
                                                }
                                                echo $str;
                                            ?>
                                          </select>
                                          <label>User Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <button class="btn-floating btn-sm amber" id = "btn_get">
                                           Get</button>
                                    </div>
                                </div>
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_parent_name" name="parent_name">
                                            <option value="" >---Select--</option>
                                            <?php
                                                $str = "";
                                                foreach($user2 as $f)
                                                {
                                                    $str = $str."<option value='".$f->user_name."'>".$f->user_name."-".$f->user_code."</option>";
                                                }
                                                echo $str;
                                            ?>
                                          </select>
                                          <label>Parent Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                
                                
                               
                               


                               

                               
                                                    
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s12 m12 l2 xl2">
                                        <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            
                                    </div>
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Parent has been Changed Successfully....', 'success'); 
                });
                </script>";
            }
            else if($op == 2)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Error! Not Changed', 'error'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...Not Changed...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#btn_get').click(function(e) 
            {
                e.preventDefault();
                var user_name = $('#id_user_name').val();
                //var user_code = $('#id_user_code').val();

                var ax = $('option:selected', '#id_user_name').text();
                var bx = ax.split("-");
                var user_code = bx[1];

                alert(user_name+"--"+user_code);

                if(user_name != "")
                {
                    window.location.href = "<?php echo url('/'); ?>/changeparent/" +user_name + "/" + user_code;
                }

            });

           
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('changeparent_store')}}");
							$('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

           
      });
    </script>
    </body>
</html>
