@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Account Check </h2>
                <h5 class="text-white op-7 mb-3">User Account Balance Check</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <label style="color:white !important;">Time : {{$time}} Sec</label>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
     <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                            <th style='font-size:12px;padding:7px 8px;'>Date</th>
                            <th style='font-size:12px;padding:7px 8px;'>O.BAL</th>
                            <th style='font-size:12px;padding:7px 8px;'>C.BAL</th>
                            <th style='font-size:12px;padding:7px 8px;'>OP.BAL</th>
                            <th style='font-size:12px;padding:7px 8px;'>PURCHASE</th>
                            <th style='font-size:12px;padding:7px 8px;'>RECH.TOT</th>
                            <th style='font-size:12px;padding:7px 8px;'>Eb.TOT</th>
                            <th style='font-size:12px;padding:7px 8px;'>MON.TRNS</th>
                            <th style='font-size:12px;padding:7px 8px;'>Mon.VERIFY</th>
                            <th style='font-size:12px;padding:7px 8px;'>CP.BAL</th>
                            <th style='font-size:12px;padding:7px 8px;'>DEV</th>
                          
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                    <?php 
                        echo $data;
                    ?>
                    
                    </tbody>
                </table>
                </div>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    <br>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_f_date, #id_t_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

   
    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();

        var fd = $('#id_f_date').val();
        var td = $('#id_t_date').val();
        var ur = $('#id_user_name').val();
        
        if(fd != "" && td != "" && ur != "-")
        {
            startloader();
            $('#rech_form').attr('action', "{{url('n_check_account_2')}}");
            $('#rech_form').submit();
        }
        else{
            swal("Cancelled", "Sorry...Anyone Date is Empty", "error");
        }
        
    }); 

   



    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop