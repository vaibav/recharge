@extends('layouts.admin_new')
@section('styles')
<link href="{{asset('css/chat_admin.css')}}" rel="stylesheet">
<style>
    .table td, .table th {
    padding: .15rem !important;
   
  }
  .table td {
    font-size:12px !important;
  }
  .table th {
    font-size:14px !important;
  }
    .chart-container { height:200px !important; min-height:200px !important; }
    canvas{ height:200px !important; }
</style>
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white fw-bold" style = "margin-top:15px !important; margin-bottom:10px !important;">Dashboard </h2>
                <p><span class="text-white op-7 " style = "margin-top:10px !important; margin-bottom:10px !important;" id ="tr_1">Server On OFF Entry</span>
                <span id="tim" style ="font-size:12px !important;"> &nbsp;&nbsp;{{date('Y-m-d H:i:s')}}</span></p>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="#" class="btn btn-white btn-border btn-round mr-2" style = "padding:1px 5px;border:0 !important;">
                    
                    
                </a>
            </div>
        </div>
    </div>
</div>
<div class="page-inner mt--5">
    
 <form id="rech_form" class="form-horizontal" action="{{url('serveronoff_store')}}" method="post" accept-charset="UTF-8">
<input type="hidden" name="_token" value="{{ csrf_token() }}">


    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">

                   
                    <div class="form-group">
                    <label for="id_server_status">Change Server Status</label>
                    <select class="form-control selectpicker" data-style="btn btn-link" id="id_server_status" name="server_status">
                        <?php
                            if($s_status == 1)
                            {
                                echo "<option value='1' selected>ON</option>";
                            }
                            else 
                            {
                                echo "<option value='1'>ON</option>";
                            }

                            if($s_status == 2)
                            {
                                echo "<option value='2' selected>OFF</option>";
                            }
                            else 
                            {
                                echo "<option value='2'>OFF</option>";
                            }
                        ?>
                    </select>
                </div>
                   
                </div>
            </div>
        </div>
    </div>

    <div class="row mt--2">
        <div class="col-md-12">
            <div class="card full-height">
                <div class="card-body">

                   
                   <div class="form-group">
                        <?php 
                                $stx = "";
                                if($s_status == 1)
                                {
                                    $stx = "Server is in ON status......";
                                }
                                else if($s_status == 2)
                                {
                                    $stx = "Server is in OFF status......";
                                }
                        ?>
                        <button class = " btn btn-warning" style = "font-size:16px;"><?php echo $stx; ?> </button>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Update</button>
               
            </div>
            </div>
    </div
</form>
<!--End page -->
</div>




@section('scripts')
<?php
if(session()->has('msg'))
{
    $op = session('msg');
    if($op == 1)
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Alert!', 'Server Status has been changed Successfully...', 'success'); 
        });
        </script>";
    }
    else
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Error!', 'Sorry... Not Changed', 'error'); 
        });
        </script>";
    }
    session()->forget('msg');
}
?>
<script>
$(document).ready(function() {
  //alert("hellow");
   $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('serveronoff_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        
        
    }); 

});
</script>
@stop
@stop
