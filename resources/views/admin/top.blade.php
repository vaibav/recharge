<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Lakshmi Mobiles</title>

<!-- Styles Icons -->
<link href="{{ asset('css/coreui-icons.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/flag-icon.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
<link href="{{ asset('css/simple-line-icons.css') }}" rel="stylesheet">

<!-- Main Styles  -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/pace.min.css') }}" rel="stylesheet">

<!-- Date-Time Picker  -->
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">

<!-- Sweetalert Design  -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">