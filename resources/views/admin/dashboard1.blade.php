<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body class = "white">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                <div class="row" style="margin-bottom: 5px;">
                    <div class="col s12 m12 l5 xl5" >
                            <div class="col s4 m4 l2 xl2  light-blue-text text-darken-4" style="font-size: 16px; font-weight:600;padding: 3px; ">
                                News :
                            </div>
                            <div class="col s8 m8 l10 xl10 amber-text text-darken-4" style="font-size: 16px; font-weight:600; padding: 3px; ">
                                <?php 
                                    $stx = "";
                                    foreach($offer as $r)
                                    {
                                        $stx = $stx . $r->offer_details. "&nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left"><?php echo $stx; ?> </marquee>
                            </div>
                    </div>
                    <div class="col s12 m12 l5 xl5">
                            <div class="col s9 m9 l4 xl4  light-blue-text text-darken-4" style="font-size: 16px; font-weight:600; padding: 3px; ">
                                Inactive Networks :
                            </div>
                            <div class="col s3 m3 l8 xl8 amber-text text-darken-4" style="font-size: 16px; font-weight:600; padding: 3px; ">
                                <?php 
                                    $sty = "";
                                    //print_r($in_active);
                                    foreach($in_active as $r)
                                    {
                                        $sty = $sty . $r->net_name. " &nbsp;&nbsp;";
                                    }
                                ?>
                                    <marquee direction="left" ><?php echo $sty; ?> </marquee>
                            </div>
                    </div>
                    <div class="col s12 m12 l2 xl2 center-align">
                    <a class="btn-floating  btn-small waves-effect waves-light  white darken-1 center-align  light-blue-text text-darken-4" id="tim" style="font-size:14px;width: 160px;font-weight:600;box-shadow:none;height: 25px;border-radius: 2%;margin-right: 30px; ">5/26/2019, 10:39:39 PM </a>
                    </div>
                </div>

                <div class="row" style="margin-bottom: 5px;margin-left:8px;margin-right:8px;">
                    
                <div class="col s12 m12 l3 xl3 card shadow" style = "margin:0px 0px;height:180px;">
                    <div class = "card-content white darken-1" style = "border-radius:10px;padding:0px 0px;">
                        <div class="col s9 m9 l9 xl9 left-align" style="margin: 2px 2px;padding: 1px 2px;">
                            <label class="title-con" style="font-size:14px;">Recharge Total</label><br>
                            <label class="title-con" style="font-size:14px;">Success Total</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Total</label><br>
                            <label class="title-con" style="font-size:14px;">Total Transaction</label><br>
                            <label class="title-con" style="font-size:14px;">Success Transaction</label><br>
                            <label class="title-con" style="font-size:14px;">Failure Transaction</label><br>
                        </div>
                        <div class="col s2 m2 l2 xl2 right-align" style="margin: 0px 0px;padding: 1px 2px;">
                            <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;0.00</label><br>
                            <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;0.00</label><br>
                            <label class="title-con" style="font-size:14px;" id = "re_3">&#x20B9;0.00</label><br>
                            <label class="title-con" style="font-size:14px;" id = "tr_1">0</label><br>
                            <label class="title-con" style="font-size:14px;" id = "tr_2">0</label><br>
                            <label class="title-con" style="font-size:14px;" id = "tr_3">0</label><br>
                        </div>
                    </div>
                </div>

                <div class="col s12 m12 l9 xl9 card shadow" style = "margin:0px 0px;">
                    <div class = "card-content white darken-1" style = "border-radius:10px;padding:0px 0px;">
                        <canvas id="myChart" style = "height:180px"></canvas>
                    </div>
                </div>
    
               
    
               
    
    
    
    
               
            

                </div>

            <div class="card shadow" >
                

                <div class="card-content white darken-1" style = "border-radius:10px;padding:0px 0px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style="font-size:12px;padding:7px 8px;">NO</th>
                                <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                                <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

    <script>
     $(document).ready(function() 
	 {
            var c = 1;
            load_data();
            load_data1();
            load_data2();
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            window.setInterval(function(){
			    /// call your function here
			    load_data();
                load_data1();
                load_data2()
			}, 17500);
            
            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'adminresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d.toLocaleString());
						if(c == 1)
						{
							$('#tim').css("background-color", "#EC7063");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("background-color", "#27AE60");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function load_data1()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'adminrech_data',
                    type: 'GET',
                    dataType: "json",
                    success: function( data ){
                        $('#re_1').text(data['rec_tot']);
                        $('#re_2').text(data['res_tot']);
                        $('#re_3').text(data['ref_tot']);
                        $('#tr_1').text(data['trn_tot']);
                        $('#tr_2').text(data['trs_tot']);
                        $('#tr_3').text(data['trf_tot']);
                    
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function load_data2()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'adminchart_data',
                    type: 'GET',
                    dataType: "json",
                    success: function( data ){
                        var res = data['res'];
                        var ref = data['ref']
                        chart_data(res, ref);
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }

            function chart_data(res, ref)
            {
                new Chart(document.getElementById("myChart"), {
                    type: 'line',
                    data: {
                        labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                        datasets: [{ 
                            data: res,
                            label: "Success",
                            borderColor: "#52BE80",
                            fill: false
                        }, { 
                            data: ref,
                            label: "Failure",
                            borderColor: "#CD6155",
                            fill: false
                        }
                        ]
                    },
                    options: {
                       
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }
      });
    </script>
    </body>
</html>
