<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

      

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:60px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Money Transfer Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('moneyreport')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                <div class="card-content white darken-1" style = "border-radius:4px;">
              
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                            <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                            <th style='font-size:12px;padding:7px 8px;'>CUSTOMER MOBILE</th>
                            <th style='font-size:12px;padding:7px 8px;'>BANK</th>
                            <th style='font-size:12px;padding:7px 8px;'>ACCOUNT NO</th>
                            <th style='font-size:12px;padding:7px 8px;'>TRANSFER TYPE</th>
                            <th style='font-size:12px;padding:7px 8px;'>AMOUNT</th>
                            <th style='font-size:12px;padding:7px 8px;'>DATE</th>
                            <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                            <th style='font-size:12px;padding:7px 8px;'>BANK REP.ID</th>
                           
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                      
                                       foreach($money as $f)
                                       {
                                            $bank = "";
                                            if($f->bk_code == "UTIB") {
                                                $bank = "AXIS BANK";
                                            }
                                            else if($f->bk_code == "BARB") {
                                                $bank = "BANK OF BARODA";
                                            }
                                            else if($f->bk_code == "CNRB") {
                                                $bank = "CANARA BANK";
                                            }
                                            else if($f->bk_code == "CORP") {
                                                $bank = "CORPORATION BANK";
                                            }
                                            else if($f->bk_code == "CIUB") {
                                                $bank = "CITY UNION BANK";
                                            }
                                            else if($f->bk_code == "HDFC") {
                                                $bank = "HDFC BANK";
                                            }
                                            else if($f->bk_code == "IBKL") {
                                                $bank = "IDBI BANK LTD";
                                            }
                                            else if($f->bk_code == "IDIB") {
                                                $bank = "INDIAN BANK";
                                            }
                                            else if($f->bk_code == "IOBA") {
                                                $bank = "INDIAN OVERSEAS BANK";
                                            }
                                            else if($f->bk_code == "ICIC") {
                                                $bank = "ICICI BANK";
                                            }
                                            else if($f->bk_code == "KVBL") {
                                                $bank = "KARUR VYSYA BANK";
                                            }
                                            else if($f->bk_code == "SBIN") {
                                                $bank = "STATE BANK OF INDIA";
                                            }
                                            else if($f->bk_code == "SYNB") {
                                                $bank = "SYNDICATE BANK";
                                            }
                                            else if($f->bk_code == "TMBL") {
                                                $bank = "TAMILNADU MERCANTILE BANK";
                                            }
                                            else if($f->bk_code == "VIJB") {
                                                $bank = "VIJAYA BANK";
                                            }
                                            else {
                                                $bank = $f->bk_code;
                                            }
                                            
                                            if($f->mt_status == "SUCCESS" || $f->mt_status == "FAILURE")
                                            {
                                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rem_id."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$bank."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->bk_acc_no."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->bk_trans_type."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mt_amount."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mt_date."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mt_status."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mt_reply_id."</td>";                                     
                                                echo "</tr>";
                                            }

                                            
                                                     
                                           $j++;
                                       }
                                       

                                      
                                    ?>

                            </tbody>
                        </table>
                        
                        {{ $money->links('vendor.pagination.materializecss') }}


                        

                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
                <h4>Info</h4>
                <table class="table table-responsive-sm table-bordered" style = "border:1px solid #f86c6b;" id="rech_offer_body">
                </table>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>

    @include('admin.bottom1')

   
   

    <script>
     $(document).ready(function() 
	 {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            

            
           
      });
    </script>
    </body>
</html>
