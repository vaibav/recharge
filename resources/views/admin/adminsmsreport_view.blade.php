<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Mobile Sms Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('smsdetails')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                  <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                  <th style='font-size:12px;padding:7px 8px;'>TRANS ID</th>
                                  <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                  <th style='font-size:12px;padding:7px 8px;'>PROVIDER</th>
                                  <th style='font-size:12px;padding:7px 8px;'>DATE</th>
                                  <th style='font-size:12px;padding:7px 8px;'>MOBILE NO</th>
                                  <th style='font-size:12px;padding:7px 8px;'>MESSAGE</th>
                                  <th style='font-size:12px;padding:7px 8px;'>REPLY</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                        $str = "";
                                       foreach($sms as $d)
                                       {
                                           
                                          $api_name = "";
                                          foreach($api as $r)
                                          {
                                              if($d->api_code == $r->api_code)
                                                  $api_name = $r->api_name;
                                          }

                                        
                                          $str = $str."<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->created_at."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_mobile."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 280px;word-break: break-word;'>".$d->user_message."</div></td>";
                                          $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 350px;word-break: break-word;'>".$d->reply_message."</div></td>";
                                          $str = $str."</tr>";
                                        
                                                                                 
                                          $j++;
                                       }
                                       
                                       echo $str;
                                      
                                    ?>

                            </tbody>
                        </table>
                        
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	    {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            
            
      });
    </script>
    </body>
</html>
