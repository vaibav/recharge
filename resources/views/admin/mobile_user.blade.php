<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Android Login User Details</span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                    <th style='font-size:12px;padding:7px 8px;'>USER TYPE</th>
                                    <th style='font-size:12px;padding:7px 8px;'>MOBILE IMEI</th>
                                    <th style='font-size:12px;padding:7px 8px;'>MOBILE</th>
                                    <th style='font-size:12px;padding:7px 8px;'>AUTH TOKEN</th>
                                    <th style='font-size:12px;padding:7px 8px;'>USER OTP</th>
                                    <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                    <?php 
                                            $j = 1;
                                        
                                            foreach($data as $f)
                                            {
                                                                                    
                                                $stat = "PENDING";
                                                if($f->user_status == 1)
                                                    $stat = "SUCCESS";
                                                else if($f->user_status == 2)
                                                    $stat = "INVALID OTP";
                                                

                                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_type."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mobile_imei."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->mobile_name."</td>";

                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->auth_token."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_otp."</td>";
                                                
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$stat."</td>";
                                            
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>";
                                                echo "<button class='btn-floating btn-sm waves-effect' id='success_".$f->auth_token."'>
                                                <i class='small material-icons'>edit</i></button>&nbsp;&nbsp;&nbsp;";
                                                echo "<button class='btn-floating btn-sm waves-effect red' id='failure_".$f->auth_token."'>
                                                <i class='small material-icons'>close</i></button></td>";
                                            
                                                echo "</tr>";
                                                                                        
                                                $j++;
                                            }

                                                
                                    ?>

                            </tbody>
                        </table>
                      
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['output'];
            $res = $op1['msg'];
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            
            
            session()->forget('result');
        }
    ?>

    <script>
    $(document).ready(function() 
    {
        
        
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("success_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Update it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/mobile_user_update/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                var nid1=gid.split("failure_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Delete it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.location.href = "<?php echo url('/'); ?>/mobile_user_delete/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

               

                
            });

            
    });
    </script>
    </body>
</html>
