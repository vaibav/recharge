<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Remitter Details</span>
                    
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
              
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Remitter Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Address</th>
                                      <th style='font-size:12px;padding:7px 8px;'>City</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Pincode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API Provider</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Reply Id</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                      
                                        foreach($data as $f)
                                        {
                                                                                  

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_rem_name."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_address."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_city."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_pincode."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->msisdn."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->api_code."</td>";
                                            if($f->rem_status == "PENDING")
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'>PENDING</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'><input type='text' id='opr_".$f->rem_id."' class='form-control' /></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:center;'>";
                                                echo "<button class='btn-floating btn-sm ' id='success_".$f->rem_id."'>
                                                <i class='small material-icons'>check</i></button>&nbsp;&nbsp;&nbsp;";
                                                echo "<button class='btn-floating btn-sm red' id='failure_".$f->rem_id."'>
                                                <i class='small material-icons'>close</i></button></td>";
                                            }     
                                            else 
                                            {
                                                
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_status."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rem_rep_opr_id."</td>";
                                                echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                            }
                                           
                                                
                                            
                                            echo "</tr>";
                                                                                    
                                            $j++;
                                        }

                                       
                                    ?>
                            </tbody>
                        </table>
                       
                       
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op1 = session('result');
            $op = $op1['msg'];
            $res = $op1['output'];
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$res."', 'success'); 
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', '".$res."', 'error'); 
                });
                </script>";
            }
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

           $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("success_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Add it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            var opr = $('#opr_' + code).val();
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/bank_remitter_update_success/" + code + "/" + opr;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

                var nid1=gid.split("failure_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Cancel it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            var opr = $('#opr_' + code).val();
                            window.location.href = "<?php echo url('/'); ?>/bank_remitter_update_failure/" + code+ "/" + opr ;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                }

               

                
            });
      });
    </script>
    </body>
</html>
