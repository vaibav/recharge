<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style = "background-color: #34495e;">
        @include('admin.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('admin.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('admin.title', array('title' => 'Server Result', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card">
                  <div class="card-header">
                    <i class="fa fa-align-justify"></i>Recharge Request</div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                          <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <form id= "rech_form" class="form-horizontal" action="{{url('rechargerequest_view')}}" method="post" accept-charset="UTF-8">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  
                                  
                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">From Date</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="id_f_date" type="text" name="f_date" placeholder="YYYY-MM-DD">
                                      <span class="text-danger">{{ $errors->first('f_date') }}</span>
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">To Date</label>
                                    <div class="col-sm-6">
                                      <input class="form-control" id="id_t_date" type="text" name="t_date" placeholder="YYYY-MM-DD">
                                      <span class="text-danger">{{ $errors->first('t_date') }}</span>
                                    </div>
                                  </div>

                                  <hr>
                                   
                                  

                                  <div class="form-group row">
                                    <label class="col-sm-5 col-form-label" for="input-normal">Provider</label>
                                    <div class="col-sm-6">
                                      <select class="form-control" id="id_api_code" name="api_code" >
                                            <option value="-">---Select---</option>
                                            <?php
                                                foreach($api as $f)
                                                {
                                                    echo "<option value='".$f->api_code."'>".$f->api_name."-".$f->api_code."</option>";
                                                }
                                            ?>
                                            
                                      </select>
                                    </div>
                                  </div>

                                 
                                  
                                  <div class="form-group row" style="padding:6px 15px;">
                                      <button class="btn btn-sm btn-primary" type="submit" id="print_view">
                                          <i class="fa fa-dot-circle-o"></i> Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                      <button class="btn btn-sm btn-primary" type="submit" id="print_excel">
                                          <i class="fa fa-dot-circle-o"></i> Print Excel</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                        <button class="btn btn-sm btn-danger" type="reset">
                                          <i class="fa fa-ban"></i> Reset</button>
                                  </div>

                                  <div class="form-group row" style="padding:6px 15px;">
                                        
                                         
                                  </div>

                                </form>
                          </div>
                         
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('admin.footer')
    @include('admin.bottom')

    <script>
     $(document).ready(function() 
	   {
            
          $('#id_f_date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
            $('#id_t_date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
            
            $('#print_view').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargerequest_view')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 
           
            $('#print_excel').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargerequest_excel')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

            
  
     });
    </script>
    </body>
</html>
