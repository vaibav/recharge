<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge EBBill Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargebilldetails')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
              
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:4px 4px;'>USER NAME</th>
                                    <th style='font-size:12px;padding:4px 4px;'>EB CONN NO</th>
                                    <th style='font-size:12px;padding:4px 4px;'>NETWORK</th>
                                    <th style='font-size:12px;padding:4px 4px;'>EB DUE AMT</th>
                                    <th style='font-size:12px;padding:4px 4px;'>NET.PER(%) / SURPLUS</th>
                                    <th style='font-size:12px;padding:4px 4px;text-align:right;'>C.AMT</th>
                                    <th style='font-size:12px;padding:4px 4px;'>TRN ID</th>
                                    <th style='font-size:12px;padding:4px 4px;'>API TRN ID</th>
                                    <th style='font-size:12px;padding:4px 4px;'>REQUEST ID</th>
                                    <th style='font-size:12px;padding:4px 4px;'>OPR. TRN ID</th>
                                    <th style='font-size:12px;padding:4px 4px;'>R.DATE</th>
                                    <th style='font-size:12px;padding:4px 4px;'>UP.DATE</th>
                                    <th style='font-size:12px;padding:4px 4px;'>STATUS</th>
                                    <th style='font-size:12px;padding:4px 4px;text-align:right;'>O.BAL</th>
                                    <th style='font-size:12px;padding:4px 4px;text-align:right;'>C.BAL</th>
                                    <th style='font-size:12px;padding:4px 4px;text-align:right;'>PROVIDER</th>
                                    <th style='font-size:12px;padding:4px 4px;text-align:right;'>MODE</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                        $str = "";
                                       foreach($recharge as $d)
                                       {
                                           
                                                                                       
                                          $net_name = "";
                                          foreach($network as $r)
                                          {
                                              if($d->net_code == $r->net_code)
                                                  $net_name = $r->net_name;
                                          }

                                          $api_name = "";
                                          foreach($api as $r)
                                          {
                                              if($d->api_code == $r->api_code)
                                                  $api_name = $r->api_name;
                                          }

                                          $rech_status = "";
                                          $status = "";
                                          $o_bal = 0;
                                          $u_bal = 0;
                                          $r_tot = 0;

                                          $rech_option = $d->con_option;
                                          $rech_status = $d->con_status;
                                          $r_tot = $d->con_total;
                                          $u_bal = $d->user_balance;

                                          if($rech_option == 1 && ($rech_status == "PENDING" || $rech_status == "SUCCESS"))
                                          {
                                              $str = $str."<tr>";
                                             
                                          }
                                          else if($rech_option == 2 && $rech_status == "PENDING")
                                          {
                                              $str = $str."<tr>";
                                             
                                          }
                                          else if($rech_option == 2 && $rech_status == "FAILURE")
                                          {  
                                              $str = $str."<tr style='background-color:#E8DAEF;'>";
                                              
                                          }

                                          if($rech_status == "PENDING" && $rech_option == 1)
                                          {
                                            $status = "<button class='btn-floating btn-sm waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if($rech_status == "PENDING" && $rech_option == 2)
                                          {
                                              $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if($rech_status == "FAILURE"  && $rech_option == 2)
                                          {      
                                              $status = "<button class='btn-floating btn-sm waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                              //$o_bal = floatval($u_bal) - floatval($r_tot);
                                              $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                              //$u_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          else if ($rech_status == "SUCCESS")
                                          {
                                              $status = "<button class='btn-floating btn-sm waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                              $o_bal = floatval($u_bal) + floatval($r_tot);
                                          }
                                          
                          
                                          $str = $str."<td style='font-size:11px;padding:4px 4px;'>".$j."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->user_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_acc_no."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$net_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_amount."</td>";
                                          if($rech_option == 2 && $rech_status == "FAILURE")
                                          {
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>&nbsp;</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".$d->con_total."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->api_trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'><div style='width: 140px;word-break: break-word;'>".$d->con_req_status."</div></td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_opr_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'></td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'></td>";
                                          }
                                          else  
                                          {
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_net_per;
                                              $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->con_net_per_amt."";
                                              $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->con_net_surp."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".$d->con_total."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->api_trans_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'><div style='width: 140px;word-break: break-word;'>".$d->con_req_status."</div></td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_opr_id."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->created_at."</td>";
                                              $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->reply_date."</td>";
                                          }
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$status."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$api_name."</td>";
                                          $str = $str."<td  style='font-size:11px;padding:4px 4px;'>".$d->con_mode."</td>";
                                                                                 
                                          $j++;
                                       }
                                       
                                       echo $str;
                                      
                                    ?>

                            </tbody>
                        </table>
                       
                        
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            
            
            
      });
    </script>
    </body>
</html>
