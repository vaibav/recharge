@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">API Provider</h2>
                <h5 class="text-white op-7 mb-3">Network Entry</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{url('n_apirequest')}}" class="btn btn-secondary btn-round">API Provider</a>
                <a href="{{url('n_apiresult')}}" class="btn btn-secondary btn-round">API Result</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('n_apirequest_network_store')}}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pr_name">Provider Name</label>
                    <select class="form-control" id="id_api_code" name="api_code">
                        <?php 
                            foreach($api as $d) {
                                echo "<option value ='".$d->api_code."'>".$d->api_name."</option>";
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <br>
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Save</button>
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style='font-size:12px;padding:7px 8px;'>Select</th>
                        <th style='font-size:12px;padding:7px 8px;display:none;'>Network ID</th>
                        <th style='font-size:12px;padding:7px 8px;'>Network Name</th>
                        <th style='font-size:12px;padding:7px 8px;'>Network Code</th>
                    </tr>
                </thead>
                <tbody id="tbl_body">
                    <?php 
                        $j = 1;
                        foreach($network as $f)
                        {
                            echo "<tr><td style='font-size:12px;padding:2px 8px;'>";
                            echo "<input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select[]' value = '".$f->net_code."' /></td>";

                            echo "<td style='font-size:12px;padding:5px 8px;'>".$f->net_name."</td>";

                            echo "<td style='font-size:12px;padding:2px 8px;'>";
                            echo "<input type='text' id='id_net_short_".$j."'  name='net_".$f->net_code."' /></td></tr>";

                            $j++;
                        }
                    ?>
                    
                </tbody>
            </table>
            </div>
        </div>
            
        
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $("#id_api_code").change(function(e)
    {
        var ty = $('option:selected', this).val();
        var ax = $('option:selected', this).text();
        
        if(ty != "-")
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });

            $.ajax({
                url:'n_apirequest_network_get?api_code=' + ty,
                type: 'GET',
                success: function( response ){
                    $("#tbl_body").html(response);
                },
                error: function (xhr, b, c) {
                    console.log('xhr=' + xhr + ' b=' + b + ' c=' + c);
                }
            });
            
            

        }
            
    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('n_apirequest_network_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        
        
    }); 


  
   
});
</script>
@stop
@stop


