<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">User Stock Details</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <form id = "rech_form" class="form-horizontal" action="{{url('admin_user_stock_2')}}" method="get" accept-charset="UTF-8">
                                  
                                  
                       
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_f_date" type="text" name="f_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <span class="text-danger">{{ $errors->first('f_date') }}</span>
                                        <label for="id_f_date">From Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                    <input id="id_t_date" type="text" name="t_date" class="datepicker" value = "<?php echo date('Y-m-d'); ?>">
                                        <span class="text-danger">{{ $errors->first('t_date') }}</span>
                                        <label for="id_t_date">To Date</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_user_name"  name = "user_name">
                                            <option value = '-'>----Select User----</option>
                                            <?php
                                                $str = "";
                                                foreach($users as $f)
                                                {
                                                    $str = $str."<option value='".$f->user_name."'>".$f->user_name."--".$f->personal->user_per_name."</option>";
                                                }
                                                echo $str;
                                            ?>
                                        </select>
                                        <label>User Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
              
                                <div class="row">
                                    <div class="col s4 m4 l2 xl2">
                                        <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                <i class="material-icons right">send</i>
                                        </button>
                                    </div>

                                    <div class="col s4 m4 l2 xl2">
                                        <button class="btn waves-effect waves-light blue " type="submit"  id="print_excel" name="print_excel">Print Excel
                                                <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                    <div class="col s4 m4 l2 xl2">
                                        <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                <i class="material-icons right">cloud</i>
                                        </button>
                                    </div>
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd',
              autoClose:true
            });

           
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                var un = $('option:selected', "#id_user_name").val();
                var fd = $("#id_f_date").val();
                var td = $("#id_t_date").val();

                if(fd != "" && td != "" && un != "-")
                {
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('admin_user_stock_2')}}");
                    $('#rech_form').submit();
                }
                else if(fd == "")
                {
                    swal("Cancelled", "From Date is Empty...", "error");
                }
                else if(td == "")
                {
                    swal("Cancelled", "To Date is Empty...", "error");
                }
                else if(un == "-")
                {
                    swal("Cancelled", "Please Select User Name...", "error");
                }
 
                
            }); 

            
      });
    </script>
    </body>
</html>
