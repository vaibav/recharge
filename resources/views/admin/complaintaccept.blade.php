<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Complaints</span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Type</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Rech Tr Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Recharge Amt</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Complaint</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Admin Reply</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                        foreach($comp1 as $f)
                                        {
                                          echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_type."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;' id='rech_".$f->trans_id."'>".$f->rech_trans_id."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_complaint."</td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'><input type='text' id='Amt_".$f->trans_id."' class='form-control' /></td>";
                                          echo "<td style='font-size:12px;padding:7px 8px;'>";
                                          echo "<button class='btn btn-small waves-effect' id='refund_".$f->trans_id."'>
                                          Refund</button>&nbsp;<button class='btn btn-small waves-effect orange' id='not_".$f->trans_id."'>
                                          Not</button></td>";

                                         
                                          echo "</tr>";
                                          $j++;
                                        }
                                    ?>

                            </tbody>
                        </table>
                      
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Complaint is Updated Successfully...', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>
    <script>
    $(document).ready(function() 
    {
        
        
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

       $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("refund_");
                if(nid.length>1)
                {
                    var code = nid[1];
                    var amt = $("#Amt_" + code).val();
                    var trd = $("#rech_" + code).text();
                    if(code != "" && amt != "" && trd != "")
                    {
                        //alert(code + "---" +amt +"----" + trd);
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Refund!'
                            }).then((result) => {
                            if (result.value) {
                                
                                //alert(code + "---" + amt);
                                window.location.href = "<?php echo url('/'); ?>/complaint_refund/" + code + "/" + amt + "/" + trd;
                            }
                            else
                            {
                                swal("Cancelled", "No Refund...", "error");
                                
                            }
                        });
                    }
                    else
                    {
                        swal("Cancelled", "Reply is Empty...", "error");
                    }
                    
                }

                var nid1 = gid.split("not_");
                if(nid1.length > 1)
                {
                    var code = nid1[1];
                    var amt = $("#Amt_" + code).val();
                    if(code != "" && amt != "" )
                    {
                        //alert(code + "---" +amt +"----" + trd);
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Not Refund!'
                            }).then((result) => {
                            if (result.value) {
                                
                                //alert(code + "---" + amt);
                                window.location.href = "<?php echo url('/'); ?>/complaint_update/" + code + "/" + amt;
                            }
                            else
                            {
                                swal("Cancelled", "No Refund...", "error");
                                
                            }
                        });
                    }
                    else
                    {
                        swal("Cancelled", "Reply is Empty...", "error");
                    }
                    
                }
                
            });

            

        
        

        
    });
    </script>
    </body>
</html>
