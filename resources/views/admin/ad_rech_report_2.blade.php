@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Recharge Report</h2>
                <h5 class="text-white op-7 mb-3">Recharge Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               <a href="{{url('rechargedetails')}}" class="btn btn-secondary btn-round">Back</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Todays Recharge</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">{{ $total['wrea_tot'] }}</h3>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-info w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['wsua_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['wsut_tot'] }} &nbsp;Credit</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center ">
                                    <span class="stamp stamp-md bg-danger mr-3">
                                        <i class="fas fa-times"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['wfua_tot'] }} &nbsp;<small>Failure</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['wfut_tot'] }} &nbsp; Credit</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

               
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Todays TNEB</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">$170</h3>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-success w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['esua_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['esut_tot'] }} Credit</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center ">
                                    <span class="stamp stamp-md bg-danger mr-3">
                                        <i class="fas fa-times"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['efua_tot'] }} &nbsp;<small>Failure</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['efut_tot'] }} Credit</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
               
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Todays Money TR</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">{{ $total['mrea_tot'] }}</h3>
                        </div>
                        <div class="progress progress-sm">
                        <div class="progress-bar bg-danger w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['msua_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['msut_tot'] }} Credit</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex align-items-center ">
                                    <span class="stamp stamp-md bg-danger mr-3">
                                        <i class="fas fa-times"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['mfua_tot'] }} &nbsp;<small>Failure</small></a></b></h5>
                                        <small class="text-muted">&#x20B9;{{ $total['mfut_tot'] }} Credit</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End -->
        <br>

        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                <div class="table-responsive">
                    <table class="display table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th style="font-size:12px;padding:7px 8px;">NO</th>
                            <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                            <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                            <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                            <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                            <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                            <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                            <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                            <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                            <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                            <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                              
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                            <?php 
                                
                                    
                                    $str = "";
                                    
                                    $j = 1;
                                    foreach($recharge as $d)
                                    {
                                    
                                        if($d->trans_type == "RECHARGE" || $d->trans_type == "BILL_PAYMENT" || $d->trans_type == "BANK_TRANSFER")
                                        {
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }

                                            $api_name = "";
                                            $reply_id = "NA";
                                            $reply_date = "";
                                            foreach($d3 as $r)
                                            {
                                                if($d->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                    
                                            $reply_id = $d->reply_id;
                                            $reply_date = $d->reply_date;

                                            $rech_status = $d->rech_status;
                                            $rech_option = $d->rech_option;
                                            $u_bal = $d->ret_bal;
                                            $r_tot = $d->ret_total;

                                            $status = "";
                                            $o_bal = 0;
                                           

                                            if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                            {      
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) - floatval($r_tot);
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }

                                            $mode = "WEB";
                                            if($d->trans_id != "")
                                            {
                                                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                        
                                                $r_l = $matches[0][0];
                        
                                                $r_l = substr($r_l, -1);
                        
                                                if($r_l == "R")
                                                    $mode = "WEB";
                                                else if($r_l == "A")
                                                    $mode = "API";
                                                else if($r_l == "G")
                                                    $mode = "GPRS";
                                                else if($r_l == "S")
                                                    $mode = "SMS";
                        
                                            }

                                            //percentage amount
                                            $per = floatval($d->ret_net_per);
                                            $peramt1 = 0;
                                            $peramt2 = 0;
                                            if(floatval($per) != 0)
                                            {
                                                $peramt1 = floatval($per) / 100;
                                                $peramt1 = round($peramt1, 4);
                                        
                                                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                                $peramt2 = round($peramt2, 2);
                                            }

                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$peramt2."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->ret_net_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                            }
                                            else if($d->rech_status == "FAILURE")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                            
                                        
                                        }
                                        if($d->trans_type == "BILL_PAYMENT1")
                                        {
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }

                                            $api_name = "";
                                            $reply_id = "NA";
                                            $reply_date = "";
                                            foreach($d3 as $r)
                                            {
                                                if($d->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                    
                                            $reply_id = $d->reply_id;
                                            $reply_date = $d->reply_date;

                                            $rech_status = $d->rech_status;
                                            $rech_option = $d->rech_option;
                                            $u_bal = $d->ret_bal;
                                            $r_tot = $d->ret_total;

                                            $status = "";
                                            $o_bal = 0;
                                           
                                            $o_bal = floatval($u_bal) + floatval($r_tot);

                                            if($rech_status == "PENDING" && $rech_option == "0")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                                
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == "2")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                                
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "FAILURE" && $rech_option == "2")
                                            {      
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) - floatval($r_tot);
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                                $str = $str."<tr>";
                                            }

                                            $mode = "WEB";
                                            if($d->trans_id != "")
                                            {
                                                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                        
                                                $r_l = $matches[0][0];
                        
                                                $r_l = substr($r_l, -1);
                        
                                                if($r_l == "R")
                                                    $mode = "WEB";
                                                else if($r_l == "A")
                                                    $mode = "API";
                                                else if($r_l == "G")
                                                    $mode = "GPRS";
                                                else if($r_l == "S")
                                                    $mode = "SMS";
                        
                                            }

                                            //percentage amount
                                            $per = floatval($d->ret_net_per);
                                            $peramt1 = 0;
                                            $peramt2 = 0;
                                            if(floatval($per) != 0)
                                            {
                                                $peramt1 = floatval($per) / 100;
                                                $peramt1 = round($peramt1, 4);
                                        
                                                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                                $peramt2 = round($peramt2, 2);
                                            }

                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$peramt2."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->ret_net_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                            }
                                            else if($d->rech_status == "FAILURE")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                            
                                        } 
                                        if($d->trans_type == "BANK_TRANSFER1")
                                        {
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }
                            
                                            $api_name = "";
                                            foreach($d3 as $r)
                                            {
                                                if($d->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                            
                                            
                                            $status = "";
                                            $o_bal = 0;
                                           

                                            $rech_status = $d->rech_status;
                                            $rech_option = $d->rech_option; 
                                            $r_tot = $d->ret_total;
                                            $u_bal = $d->ret_bal;
                                        
                            
                                            
                                            if($rech_status == "PENDING" && $rech_option == 0)
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == 2)
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "FAILURE"  && $rech_option == 2)
                                            {      
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                                //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) + floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            
                                            $mode = "WEB";
                                            if($d->trans_id != "")
                                            {
                                                preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                            
                                                $r_l = $matches[0][0];
                            
                                                $r_l = substr($r_l, -1);
                            
                                                if($r_l == "R")
                                                    $mode = "WEB";
                                                else if($r_l == "A")
                                                    $mode = "API";
                                                else if($r_l == "G")
                                                    $mode = "GPRS";
                                                else if($r_l == "S")
                                                    $mode = "SMS";
                            
                                            }

                                            //percentage amount
                                            $per = floatval($d->ret_net_per);
                                            $peramt1 = 0;
                                            $peramt2 = 0;
                                            if(floatval($per) != 0)
                                            {
                                                $peramt1 = floatval($per) / 100;
                                                $peramt1 = round($peramt1, 4);
                                        
                                                $peramt2 = floatval($d->rech_amount) * floatval($peramt1);
                                                $peramt2 = round($peramt2, 2);
                                            }
                                            
                            
                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->ret_net_per;
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$peramt2."";
                                                $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->ret_net_surp."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$d->reply_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_date."</td>";
                                            }
                                            else if($d->rech_status == "FAILURE")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                        } 
                                        if($d->trans_type == "REMOTE_PAYMENT" || $d->trans_type == "SELF_PAYMENT" || $d->trans_type == "FUND_TRANSFER" || $d->trans_type == "REFUND_PAYMENT")
                                        {
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }
                            
                                            $api_name = "";
                                            $status = "";
                                            $o_bal = 0;
                                           

                                            $rech_status = $d->rech_status;
                                            $rech_option = $d->rech_option; 
                                            $r_tot = $d->ret_total;
                                            $u_bal = $d->ret_bal;
                                            
                                            if($rech_status == "PENDING" && $rech_option == 0)
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                                
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "PENDING" && $rech_option == 2)
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-times' style='color:white'></i></button>";
                                                
                                                $str = $str."<tr>";
                                            }
                                            else if($rech_status == "FAILURE"  && $rech_option == 2)
                                            {      
                                                $status = "<button class='btn btn-icon btn-round btn-danger' ><i class='fa fa-reply' style='color:white'></i></button>";
                                                //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                
                                                $str = $str."<tr style='background-color:#E8DAEF;'>";
                                            }
                                            else if ($rech_status == "SUCCESS")
                                            {
                                                $status = "<button class='btn btn-icon btn-round btn-success ' ><i class='fa fa-check' style='color:white'></i></button>";
                                                $o_bal = floatval($u_bal) - floatval($r_tot);
                                                $str = $str."<tr>";
                                            }
                                            
                                            $mode = "WEB";
                                            

                                            //percentage amount
                                            $per = floatval($d->ret_net_per);
                                            $peramt2 = 0;
                                           
                                            
                            
                                            $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->parent_name."--->".$d->user_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>-</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            if($d->rech_status == "SUCCESS" || $d->rech_status == "PENDING")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>0--0--0</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$d->reply_id."</div></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_date."</td>";
                                            }
                                            else if($d->rech_status == "FAILURE")
                                            {
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->ret_total."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                            }
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                        }                                         
                                    
                                    
                                    
                                    //$str = $str."</tr>"; 
                            
                                        
                                        
                                    
                                                                                
                                        $j++;
                                    }
                                    
                                    echo $str; 
                                

                                
                                ?>

                        </tbody>
                    </table>
                    {{ $recharge->links('vendor.pagination.atlantis') }}
                </div>
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");

    
   
  
   
});
</script>
@stop
@stop