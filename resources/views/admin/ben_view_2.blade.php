<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
       
       
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user));
        
        <div class='fixed-action-btn'><a class='btn-floating btn-large red' href = "{{url('dashboard')}}">
                <i class='large material-icons'>home</i></a>          
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:56px">
                
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Beneficiary Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('admin_ben_view_1')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                
                
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                <th style='font-size:12px;padding:7px 8px;'>Remitter</th>
                                <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                <th style='font-size:12px;padding:7px 8px;'>Account No</th>
                                <th style='font-size:12px;padding:7px 8px;'>Account Holder Name</th>
                                <th style='font-size:12px;padding:7px 8px;'>Bank Name</th>
                                <th style='font-size:12px;padding:7px 8px;'>IFSC Code</th>
                                <th style='font-size:12px;padding:7px 8px;'>Mobile No</th>
                                <th style='font-size:12px;padding:7px 8px;'>Branch</th>
                                <th style='font-size:12px;padding:7px 8px;'>API Provider</th>
                                <th style='font-size:12px;padding:7px 8px;'>Request Status</th>
                                <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                <th style='font-size:12px;padding:7px 8px;'>Reply Id</th>
                                
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                       $j = 1;
                                      
                                       foreach($bene as $f)
                                       {
                                                                                 
                                           $bank = "";
                                           if($f->bank_code == "UTIB") {
                                               $bank = "AXIS BANK";
                                           }
                                           else if($f->bank_code == "BARB") {
                                               $bank = "BANK OF BARODA";
                                           }
                                           else if($f->bank_code == "CNRB") {
                                               $bank = "CANARA BANK";
                                           }
                                           else if($f->bank_code == "CORP") {
                                               $bank = "CORPORATION BANK";
                                           }
                                           else if($f->bank_code == "CIUB") {
                                               $bank = "CITY UNION BANK";
                                           }
                                           else if($f->bank_code == "HDFC") {
                                               $bank = "HDFC BANK";
                                           }
                                           else if($f->bank_code == "IBKL") {
                                               $bank = "IDBI BANK LTD";
                                           }
                                           else if($f->bank_code == "IDIB") {
                                               $bank = "INDIAN BANK";
                                           }
                                           else if($f->bank_code == "IOBA") {
                                               $bank = "INDIAN OVERSEAS BANK";
                                           }
                                           else if($f->bank_code == "ICIC") {
                                               $bank = "ICICI BANK";
                                           }
                                           else if($f->bank_code == "KVBL") {
                                               $bank = "KARUR VYSYA BANK";
                                           }
                                           else if($f->bank_code == "SBIN") {
                                               $bank = "STATE BANK OF INDIA";
                                           }
                                           else if($f->bank_code == "SYNB") {
                                               $bank = "SYNDICATE BANK";
                                           }
                                           else if($f->bank_code == "TMBL") {
                                               $bank = "TAMILNADU MERCANTILE BANK";
                                           }
                                           else if($f->bank_code == "VIJB") {
                                               $bank = "VIJAYA BANK";
                                           }
                                           else {
                                               $bank = $f->bank_code;
                                           }


                                           $rem_name = "";
                                           $rem_mobile = "";
                                           foreach($remitter as $r)
                                           {
                                               if($f->msisdn == $r->msisdn)
                                               {
                                                   $rem_name = $r->user_rem_name;
                                                   $rem_mobile = $r->msisdn;
                                               }
                                           }

                                           echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_id."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$rem_name."-".$rem_mobile."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_acc_no."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_acc_name."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$bank."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->bank_ifsc."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->msisdn."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->bk_acc_branch."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->api_code."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_req_status."</td>";
                                           echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_status."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->ben_rep_opr_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'></td>";
                                          
                                               
                                           
                                           echo "</tr>";
                                                                                   
                                           $j++;
                                       }
                                        
                                      
                                    ?>
                            </tbody>
                        </table>
                        {{ $bene->links('vendor.pagination.materializecss') }}
                    </div>
                </div>

                
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->


    @include('admin.bottom1')

    <script>
    $(document).ready(function() 
	{
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();
            
           
    });
    </script>
    </body>
</html>
