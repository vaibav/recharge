<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('admin_user_old_recharge_1')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <div class="row" >
                    
                            <div class="col s12 m12 l4 xl4 " >
                                <div class = "card-content white darken-1" >
                                    <div class="col s9 m9 l9 xl9 left-align">
                                        <label class="title-con" style="font-size:14px;">Recharge Success Amount Total</label><br>
                                        <label class="title-con" style="font-size:14px;">Recharge Success Credit Total</label><br>
                                        
                                    </div>
                                    <div class="col s2 m2 l2 xl2 right-align" >
                                        <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;{{ $total['wsua_tot'] }}</label><br>
                                        <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;{{ $total['wsut_tot'] }}</label><br>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="col s12 m12 l4 xl4 " >
                               
                            </div>

                            <div class="col s12 m12 l4 xl4 " >
                                
                            </div>

                        </div>

                        <!-- Main Content-->
                        <div class = "row">
                            <div class ="col s12 m12 l12 xl12">
                            <!-- Form Starts-->
                            <table class="bordered striped responsive-table ">
                                    <thead>
                                    <tr>
                                        <th style="font-size:12px;padding:7px 8px;">NO</th>
                                        <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                        <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                        <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                        <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                        <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                        <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                        <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbl_body">
                                    <?php 
                                        
                                            
                                        $str = "";
                                        
                                        $j = 1;
                                        foreach($recharge as $d)
                                        {
                                        
                                            $net_name = "";
                                            foreach($d2 as $r)
                                            {
                                                if($d->net_code == $r->net_code)
                                                    $net_name = $r->net_name;
                                            }
                        
                                            $api_name = "";
                                            $reply_id = "NA";
                                            $reply_date = "";
                                            foreach($d3 as $r)
                                            {
                                                if($d->api_code == $r->api_code)
                                                    $api_name = $r->api_name;
                                            }
                    
                                            
                                            $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                            
                        
                                            $str = $str."<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_mobile."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_amount."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->rech_total."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->reply_id."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->rech_date."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($d->user_balance,2, ".", "")."</td>";
                                            $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td></tr>";                              
                                            $j++;
                                        }
                                        
                                        echo $str; 
                                    
                                    ?>

                                    </tbody>
                                </table>
                            
                                {{ $recharge->links('vendor.pagination.materializecss') }}
                                        
                                    

                                <!-- End Form-->
                            </div>
                        </div>

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

      });
    </script>
    </body>
</html>
