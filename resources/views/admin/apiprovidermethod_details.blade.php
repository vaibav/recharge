<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Api Provider Method Details </span>
                    
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                                <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>CODE</th>
                                      <th style='font-size:12px;padding:7px 8px;'>NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>METHOD</th>
                                      <th style='font-size:12px;padding:7px 8px;'>STATUS</th>
                                      <th style='font-size:12px;padding:7px 8px;'>ACTION</th>
                                </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                       

                                        foreach($api as $f)
                                        {
                                            

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->api_code."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->api_name."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->api_method."</td>";

                                            if($f->api_status == 1)
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'>LIVE&nbsp;&nbsp;</td>";
                                            }
                                            else
                                            {
                                                echo "<td style='font-size:12px;padding:7px 8px;'>INACTIVE&nbsp;&nbsp;</td>";
                                            }

                                            echo "<td style='font-size:12px;padding:7px 8px;'>";
                                            echo "<button class='btn-floating btn-sm ' id='status_".$f->api_code."'>
                                            <i class='small material-icons'>edit</i></td>";
                                          
                                            echo "</tr>";

                                          
                                            $j++;
                                        }

                                    ?>
                                       
                            </tbody>
                        </table>
                       
                       
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

    <script>
     $(document).ready(function() 
	  {
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("status_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Change Method!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            window.location.href = "<?php echo url('/'); ?>/apiprovider_method_change/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                }
            
                
            });


            
      });
    </script>
    </body>
</html>
