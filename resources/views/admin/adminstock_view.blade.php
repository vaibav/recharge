<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Stock Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('stockdetails')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                      <th style='font-size:12px;padding:7px 8px;'>User</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Remarks</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Purchase (Debit)</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Sales (Credit)</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>Balance</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                        
                                        $debit = 0;
                                        $credit = 0;
                                        $balance = 0;
                                        if($obalance != 0)
                                        {
                                            $balance = floatval($balance) + floatval($obalance);
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>OPENING BALANCE</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                        }
                                        foreach($pay1 as $f)
                                        {
                                          if($f->user_name == $user->user && $f->trans_status == 1)
                                          {
                                              // Debit +
                                              $debit = floatval($debit) + floatval($f->grant_user_amount);
                                              $balance = floatval($balance) + floatval($f->grant_user_amount);
                                              echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'>".$f->grant_date."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>".$f->grant_user_name."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->payment_mode."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_remarks."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f->grant_user_amount."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                              
                                          }
                                          else if($f->grant_user_name == $user->user && $f->trans_status == 1)
                                          {
                                              // Credit -
                                              $credit = floatval($credit) + floatval($f->grant_user_amount);
                                              $balance = floatval($balance) - floatval($f->grant_user_amount);
                                              echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:15%;'>".$f->grant_date."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;width:35%;'>".$f->user_name."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->payment_mode."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_remarks."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;'></td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".$f->grant_user_amount."</td>";
                                              echo "<td  style='font-size:12px;padding:7px 8px;text-align:right;'>".number_format($balance,2,'.','')."</td>";
                                          }
                                          $j++;
                                         
                                          
                                          

                                          
                                        }
                                      
                                    ?>
                                    
                                  </tbody>

                            </tbody>
                        </table>
                       
                        
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd'
            });

           

            

            
      });
    </script>
    </body>
</html>
