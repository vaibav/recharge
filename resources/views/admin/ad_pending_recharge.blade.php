@extends('layouts.admin_new')
@section('styles')
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Pending Recharge  </h2>
                <h5 class="text-white op-7 mb-3">Recharge Pending Report</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
     <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                          <th style='font-size:12px;padding:7px 8px;'>Tran.Id</th>
                          <th style='font-size:12px;padding:7px 8px;'>API.Id</th>
                          <th style='font-size:12px;padding:7px 8px;'>User</th>
                          <th style='font-size:12px;padding:7px 8px;'>Network</th>
                          <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                          <th style='font-size:12px;padding:7px 8px;'>Amount</th>
                          <th style='font-size:12px;padding:7px 8px;'>Date</th>
                          <th style='font-size:12px;padding:7px 8px;'>Status</th>
                          <th style='font-size:12px;padding:7px 8px;'>Request</th>
                          <th style='font-size:12px;padding:7px 8px;'>API Pr</th>
                          <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                          <th style='font-size:12px;padding:7px 8px;'>Opr ID</th>
                          <th style='font-size:12px;padding:7px 8px;'>Action</th>
                          
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                    <?php 
                        $j = 1;
                      
                        foreach($recharge as $f)
                        {
                                                              
                            $net_name = "";
                            foreach($network as $r)
                            {
                                if($f->net_code == $r->net_code)
                                    $net_name = $r->net_name;
                            }

                            $api_name = "";
                            $req_status = "";

                            foreach($api as $r)
                            {
                                if($f->api_code == $r->api_code)
                                    $api_name = $r->api_name;
                            }
                            if($f->request != null)
                            {
                                $req_status = $f->request->req_id;
                            }
                           

                            $rech_status = $f->rech_status;
                            $rech_option = $f->rech_option;

                           if($rech_status == "PENDING" && $rech_option == "0")
                            {
                                $status = "<button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button>";
                                
                            }
                            else if($rech_status == "PENDING" && $rech_option == "2")
                            {
                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                
                            }
                            else if($rech_status == "FAILURE" && $rech_option == "2")
                            {      
                                $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                
                            }
                            else if ($rech_status == "SUCCESS")
                            {
                                $status = "<button class='btn-floating btn-small waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                               
                            }
                            
                            if($rech_status == "PENDING" && $rech_option == "0")
                            {
                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->api_trans_id."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->user_name."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";

                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_amount."</td>";
                                
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                echo "<td  style='font-size:12px;padding:7px 8px;white-space:pre-wrap;'><div style='width: 180px;word-break: break-word;'>".
                                    strip_tags($req_status)."</div></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$api_name."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;' id='mode_".$f->trans_id."'>".$f->rech_mode."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'><input type='text' id='opr_".$f->trans_id."' class='form-control' /></td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'>";
                                echo "<button class='btn btn-icon btn-round btn-success '  id='success_".$f->trans_id."'><i class='fa fa-check' style='color:white'></i></button>&nbsp;&nbsp;&nbsp;";
                                echo "<button class='btn btn-icon btn-round btn-danger' id='failure_".$f->trans_id."'><i class='fa fa-times' style='color:white'></i></button>&nbsp;&nbsp;&nbsp;";
                                echo "<button class='btn btn-icon btn-round btn-primary' id='reprocess_".$f->trans_id."'>
                                <i class='fa fa-reply' style='color:white'></i></button></td></tr>";
                            }
                            
                            
                            echo "</tr>";
                                                                    
                            $j++;
                        }
                    ?>
                    
                    </tbody>
                </table>
                </div>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    <br>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
    $net_code = 0;
    $mob = 0;
    $net_type = 0;
    if(session()->has('result'))
    {
        $op1 = session('result');
        $op = $op1['msg'];
        $res = $op1['output'];
        if($op == 0)
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$res."', 'success'); 
            });
            </script>";
        }
        else if($op == 1 || $op == 2 || $op == 3 || $op == 4 || $op == 5 || $op == 6 || $op == 7 || $op == 8) 
        {
            echo "<script>
            $(document).ready(function() 
            {
                swal('Error!', '".$res."', 'error'); 
            });
            </script>";
        }
        
        session()->forget('result');
    }
?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("success_");
        if(nid.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Success it!'
                }).then((result) => {
                if (result.value) {
                    var code = nid[1];
                    var opr = $('#opr_' + code).val();
                    //alert(code +"----"+ opr);
                    window.location.href = "<?php echo url('/'); ?>/pendingreport_success/" + code + "/" + opr;
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
           
        }

        var nid1=gid.split("failure_");
        var net=0;
        var ctx=0;
        if(nid1.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Failed it!'
                }).then((result) => {
                if (result.value) {
                    var code = nid1[1];
                    var opr = $('#opr_' + code).val();
                    //alert(code +"----"+ opr);
                    window.location.href = "<?php echo url('/'); ?>/pendingreport_failure/" + code + "/" + opr;
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
           
        }

        var nid2=gid.split("reprocess_");
        var net=0;
        var ctx=0;
        if(nid2.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Reprocess it!'
                }).then((result) => {
                if (result.value) {
                    var code = nid2[1];
                    var mode = $('#mode_' + code).text();
                    alert(code +"----"+ mode);
                    if(mode == "API")
                        window.location.href = "<?php echo url('/'); ?>/pendingreport_reprocessapi/" + code;
                    else
                        window.location.href = "<?php echo url('/'); ?>/pendingreport_reprocess/" + code;
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
        }

        
    });

   



    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop