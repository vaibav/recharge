<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Info Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargeinfo')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Trans Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>type</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Date</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                        $j = 1;
                                        
                                       
                                        
                                        foreach($info as $f)
                                        {

                                            echo "<tr><td style='font-size:12px;padding:7px 8px;width:5%;'>".$j."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->rech_type."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                                            echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td></tr>";
                                           
                                            $j++;
                                        }
                                       

                                      
                                    ?>

                            </tbody>
                        </table>
                        
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            
            
      });
    </script>
    </body>
</html>
