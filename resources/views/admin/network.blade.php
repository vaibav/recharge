<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Network Entry</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('network_store')}}" method="post" enctype="multipart/form-data">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="net_code" id="id_net_code" value="0">

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_net_name" type="text" name="net_name" >
                                        <span class="text-danger">{{ $errors->first('net_name') }}</span>
                                        <label for="id_net_name">Network Name</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_net_short_code" type="text" name="net_short_code" >
                                        <span class="text-danger">{{ $errors->first('net_short_code') }}</span>
                                        <label for="id_net_short_code">Short Code</label>
                                    </div>
                                </div>


                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                          <select id="id_net_type_code" name="net_type_code">
                                            <option value="" >---Select--</option>
                                            <?php
                                                foreach($networktype as $f)
                                                {
                                                    echo "<option value='".$f->net_type_code."'>".$f->net_type_name."</option>";
                                                }
                                            ?>
                                          </select>
                                          <label>Network Type</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                          <select id="id_net_status" name="net_status">
                                            <option value="1">LIVE</option>
                                            <option value="2">INACTIVE</option>
                                          </select>
                                          <label>Status</label>
                                    </div>
                                </div>
                                
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="file-field input-field col s12 m12 l8 xl8">
                                        <div class="btn">
                                          <span>File</span>
                                          <input type="file" id="id_net_photo" name="net_photo"> 
                                        </div>
                                        <div class="file-path-wrapper">
                                          <input class="file-path validate" type="text" placeholder = "Image(<300KB)">
                                        </div>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    
                                </div>

                                         
                                <div class="row">
                                    <div class="col s6 m6 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s6 m6 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    
                                        
                                </div>
                            </form>

                       <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                          <table class="bordered striped responsive-table">
                              <thead>
                                <tr>
                                  <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Code</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Name</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Short Code</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Type Code</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Type </th>
                                  <th style='font-size:12px;padding:7px 8px;'>Image </th>
                                  <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                  <th style='font-size:12px;padding:7px 8px;'>Action</th>
                                </tr>
                              </thead>
                              <tbody id="tbl_body">
                                <?php 
                                    $j = 1;
                                    foreach($network as $f)
                                    {
                                      echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                      echo "<td id='code_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_code."</td>";
                                      echo "<td id='name_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_name."</td>";
                                      echo "<td id='short_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_short_code."</td>";
                                      echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_type_code."</td>";

                                      foreach($networktype as $r)
                                      {
                                          if($r->net_type_code == $f->net_type_code)
                                              echo "<td style='font-size:12px;padding:7px 8px;'>".$r->net_type_name."</td>";
                                      }

                                      $path = asset('networkimage/'.$f->net_code.'.jpg');

                                      if (@getimagesize($path)) 
                                      {
                                          echo "<td><img src='".$path."' alt='No image' style = 'width:80px; height:28px;' /></td>";
                                      }
                                      else
                                      {
                                        echo "<td>No Imgae</td>";
                                      }

                                      if($f->net_status == 1)
                                      {
                                        echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>LIVE</td>";
                                      }
                                      else
                                      {
                                        echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>INACTIVE</td>";
                                      }
                                      
                                      $link = url("/")."/network_delete/".$f->net_code;

                                      echo "<td style='font-size:12px;padding:7px 8px;'><button class='btn-floating btn-sm waves-effect waves-light' id='edit_".$j."'>
                                      <i class='small material-icons'>edit</i> </button>&nbsp;&nbsp;&nbsp;&nbsp;";
                                      echo "<button class='btn-floating btn-sm waves-effect waves-light red' id='delete_".$f->net_code."'>
                                      <i class='small material-icons '>clear</i></td></tr>";
                                      $j++;
                                    }
                                ?>
                                
                              </tbody>
                          </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

          
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Submit!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('network_store')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            
                        }
                    });
                
                
            }); 

            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("edit_");
                if(nid.length>1)
                {
                      swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, View!'
                            }).then((result) => {
                            if (result.value) {
                                
                              var cd = $('#code_'+nid[1]).text();
                              var na = $('#name_'+nid[1]).text();
                              var sh = $('#short_'+nid[1]).text();
                              var ty = $('#type_'+nid[1]).text();
                              var st = $('#status_'+nid[1]).text();

                              var sst = "1";
                              if(st == "INACTIVE")
                                  sst = "2";

                              $('#id_net_code').val(cd);
                              $('#id_net_name').val(na);
                              $('#id_net_short_code').val(sh);
                              $('#id_net_type_code').val(ty);
                              $('#id_net_status').val(sst);

                              M.updateTextFields();
                              $('select').formSelect();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry....", "error");
                                
                            }
                        });
                    
                }
                var nid1=gid.split("delete_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                  swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Deletes!'
                            }).then((result) => {
                            if (result.value) {
                                
                              var code = nid1[1];
                              window.location.href = "<?php echo url('/'); ?>/network_delete/"+code;
                            }
                            else
                            {
                                swal("Cancelled", "Sorry....", "error");
                                
                            }
                        });
                    
                }
            });

            $('#id_net_photo').on('change', function() {
                var a = this.files[0].size;
                if (a > 307200)
                {
                    $('#id_net_photo').val(''); 
                    swal("Alert", "File size is higher than 600KB", "error");
                }
            });
      });
    </script>
    </body>
</html>
