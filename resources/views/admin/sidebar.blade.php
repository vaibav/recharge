<div class="sidebar" data-toggle="sidebar-hide">
        <nav class="sidebar-nav">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="{{url('dashboard')}}">
                <i class="nav-icon icon-speedometer"></i> Admin
                <span class="badge badge-primary">NEW</span>
              </a>
            </li>
            
            <?php
                if($user->mode == "ADMIN")
                {
            ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i> Server</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('serveronoff')}}">
                            <i class="nav-icon icon-plus"></i> OnOff Entry</a>
                        </li>
                        
                       
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i> Network</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('network_type')}}">
                            <i class="nav-icon icon-plus"></i> Network Type Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('network')}}">
                            <i class="nav-icon icon-plus"></i> Network Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('network_surplus')}}">
                            <i class="nav-icon icon-plus"></i> Network Surplus Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('network_line')}}">
                            <i class="nav-icon icon-plus"></i> Network Line Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('network_line_apipartner')}}">
                            <i class="nav-icon icon-plus"></i> API Partner Line </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('offer_line')}}">
                            <i class="nav-icon icon-plus"></i> 121 Offer Line </a>
                        </li>
                        <!--
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_details.php">
                            <i class="nav-icon icon-puzzle"></i> Network List</a>
                        </li>
                        
                        
                        -->
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i> API Master</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        
                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('apirequest')}}">
                            <i class="nav-icon icon-puzzle"></i> API Request Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('apirequest_edit')}}">
                            <i class="nav-icon icon-puzzle"></i> API Request Edit</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('apiresult')}}">
                            <i class="nav-icon icon-puzzle"></i> API Result Entry</a>
                        </li>
                        <!--
                        <li class="nav-item">
                          <a class="nav-link" href="admin_api_request_details.php">
                              <i class="nav-icon icon-puzzle"></i> API Request Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_network_bonrix_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Bondrix Network</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_bondrixurl_entry.php">
                              <i class="nav-icon icon-puzzle"></i> Bondrix URL</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_apilink_code_details.php">
                            <i class="nav-icon icon-puzzle"></i> API Link Code Details</a>
                        </li>
                        -->
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>User</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user')}}">
                            <i class="nav-icon icon-puzzle"></i> User Creation Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_view')}}">
                            <i class="nav-icon icon-puzzle"></i> User Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('user_approval_view')}}">
                            <i class="nav-icon icon-puzzle"></i> Admin Approval</a>
                        </li>
                        
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('changeparent')}}">
                            <i class="nav-icon icon-puzzle"></i> Parent Change</a>
                        </li>
                        
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('apiagent')}}">
                            <i class="nav-icon icon-puzzle"></i> Apipartner Browser</a>
                        </li>
                        <!--
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_photoupdate.php">
                            <i class="nav-icon icon-puzzle"></i> Edit Photo</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_details.php">
                            <i class="nav-icon icon-puzzle"></i> User Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_accounts.php">
                            <i class="nav-icon icon-puzzle"></i> User Accounts</a>
                        </li>
                        -->
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Payment Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('opening_balance_view')}}">
                            <i class="nav-icon icon-action-undo"></i> Opening Balance </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('remotepayment')}}">
                            <i class="nav-icon icon-action-undo"></i> Remote Payment </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('paymentaccept')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Accept </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('refundpayment')}}">
                            <i class="nav-icon icon-action-undo"></i> Refund Payment </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('payment_report')}}">
                            <i class="nav-icon icon-action-undo"></i> Payment Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails')}}">
                            <i class="nav-icon icon-action-undo"></i> Stock Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('backup_view_all')}}">
                            <i class="nav-icon icon-action-undo"></i> Retailer Stock Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('stockdetails_all')}}">
                            <i class="nav-icon icon-action-undo"></i> All Stock </a>
                        </li>
                        <!--
                        <li class="nav-item">
                        <a class="nav-link" href="admin_fund_details.php">
                            <i class="nav-icon icon-puzzle"></i> Payment Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_remote_fund.php">
                            <i class="nav-icon icon-puzzle"></i> User Remote Payment</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_amount_deduct.php">
                            <i class="nav-icon icon-puzzle"></i> User Refund Payment</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_user_payment.php">
                            <i class="nav-icon icon-puzzle"></i> User Payment</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_bill_payment.php">
                              <i class="nav-icon icon-puzzle"></i> Bill Payment</a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link" href="admin_money_payment.php">
                              <i class="nav-icon icon-puzzle"></i> Money Transfer</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="admin_user_payment_details.php">
                                <i class="nav-icon icon-puzzle"></i> User Payment Details</a>
                            </li>
                            -->
                    </ul>
            </li>
            
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Recharge Details</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargedetails')}}">
                            <i class="nav-icon icon-plus"></i> Recharge Details</a>
                        </li>
                        
                         <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargebilldetails')}}">
                            <i class="nav-icon icon-plus"></i> EBBILL Details</a>
                        </li>

                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargedetails_distributor')}}">
                            <i class="nav-icon icon-plus"></i> Distributor Recharge</a>
                        </li>

                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargedetails_super')}}">
                            <i class="nav-icon icon-plus"></i> Super.Dist Recharge</a>
                        </li>
                        
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargerequest')}}">
                            <i class="nav-icon icon-plus"></i> Recharge Request</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('serverreply')}}">
                            <i class="nav-icon icon-plus"></i> Server Reply</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('pendingreport')}}">
                            <i class="nav-icon icon-plus"></i> Pending Recharge</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('pendingbill')}}">
                            <i class="nav-icon icon-plus"></i> Pending EBBill </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('rechargeinfo')}}">
                            <i class="nav-icon icon-plus"></i> Recharge Info </a>
                        </li>
                        <!--
                        <li class="nav-item">
                        <a class="nav-link" href="admin_pending_details.php">
                            <i class="nav-icon icon-puzzle"></i> Pending Report</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_recharge_details_one.php">
                            <i class="nav-icon icon-puzzle"></i> Date Recharge Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_dist_recharge_details.php">
                            <i class="nav-icon icon-puzzle"></i> Distributor Recharge</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="admin_dist_recharge_details_date.php">
                              <i class="nav-icon icon-puzzle"></i> Date Distributor Details</a>
                          </li>
                          <li class="nav-item">
                          <a class="nav-link" href="admin_provider_recharge_details.php">
                              <i class="nav-icon icon-puzzle"></i> Provider Details</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="admin_server_reply.php">
                                <i class="nav-icon icon-puzzle"></i> Server Reply</a>
                            </li>
                        -->
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Money Transfer</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('bank_remitter_view_all')}}">
                            <i class="nav-icon icon-action-undo"></i> Remitter </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('ben_view_all')}}">
                            <i class="nav-icon icon-action-undo"></i> Beneficiary </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('pendingmoney')}}">
                            <i class="nav-icon icon-action-undo"></i> Pending Report </a>
                        </li>
                        
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Sms</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('smsdetails')}}">
                            <i class="nav-icon icon-action-undo"></i> Sms Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('smssend')}}">
                            <i class="nav-icon icon-action-undo"></i> Sms Send </a>
                        </li>
                        
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Others</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint')}}">
                            <i class="nav-icon icon-action-undo"></i> Complaints </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('complaint_view')}}">
                            <i class="nav-icon icon-action-undo"></i> Complaint Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('offer_entry')}}">
                            <i class="nav-icon icon-action-undo"></i> Offers </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('offer_view')}}">
                            <i class="nav-icon icon-action-undo"></i> Offer Details </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('mobile_user')}}">
                            <i class="nav-icon icon-action-undo"></i> Mobile User </a>
                        </li>
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Bonrix</a>
                    <ul class="nav-dropdown-items" style = "background-color:#78281F;">
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('bonrixresult')}}">
                            <i class="nav-icon icon-plus"></i> Bonrix Result</a>
                        </li>
                        
                       
                    </ul>
            </li>
            <!--
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Others</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_offer_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Offer Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_offer_details.php">
                            <i class="nav-icon icon-puzzle"></i> Offer Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_complaint_details.php">
                            <i class="nav-icon icon-puzzle"></i> Complaint Details</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bank_entry.php">
                            <i class="nav-icon icon-puzzle"></i> Bank Entry</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bank_details.php">
                            <i class="nav-icon icon-puzzle"></i> Bank View</a>
                        </li>
                        
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Bulk SMS</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_bulksms.php">
                            <i class="nav-icon icon-puzzle"></i> Entry</a>
                        </li>   
                    </ul>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-puzzle"></i>Downloads </a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                        <a class="nav-link" href="admin_download.php">
                            <i class="nav-icon icon-puzzle"></i>Downloads </a>
                        </li>   
                    </ul>
            </li>
            -->
            <?php
                }
            ?>
          </ul>
        </nav>
        
      </div>