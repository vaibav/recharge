<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Network Line Entry</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                            <form id="rech_form" class="form-horizontal" action="{{url('network_line_store')}}" method="post" accept-charset="UTF-8">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="net_code" id="id_net_tr_code" value="0">

                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_net_code" name="net_code">
                                            <option value="" >---Select--</option>
                                            <?php
                                                foreach($network as $f)
                                                {
                                                    if($f->net_code == $net_code)
                                                        echo "<option value='".$f->net_code."' selected>".$f->net_name."</option>";
                                                    else
                                                        echo "<option value='".$f->net_code."'>".$f->net_name."</option>";
                                                }
                                            ?>
                                          </select>
                                          <label>Network Name</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select   id="id_api_code" name="api_code" >
                                            <option value ="-">---Select---</option>
                                            <?php
                                                foreach($api1 as $f)
                                                {
                                                    echo "<option value='".$f->api_code."'>".$f->api_name."</option>";
                                                }
                                            ?>
                                        </select>
                                        <label>Api Provider</label>
                                    </div>
                                </div>


                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_net_line_type" name="net_line_type" >
                                            <option value="-">---Select---</option>
                                            <option value="ALL">ALL</option>
                                            <option value="RANGE_VALUES">RANGE_VALUES</option>
                                            <option value="SPECIFIC_AMOUNT">SPECIFIC_AMOUNT</option>
                                        </select>
                                        <label >Line Type</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                   
                                </div>
                                
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input type="text" id="id_net_line_min" name="net_line_min" id = "id_f1">
                                        <span class="text-danger">{{ $errors->first('net_line_min') }}</span>
                                        <label for="id_net_line_min">Min Amount</label>
                                    </div>
                                    <div class="input-field col s12 m12 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;" id = "id_f2">
                                        <input type="text" id="id_net_line_max" name="net_line_max" >
                                        <span class="text-danger">{{ $errors->first('net_line_max') }}</span>
                                        <label for="id_net_line_max">Max Amount</label>
                                    </div>
                                    
                                </div>

                                

                                         
                                <div class="row">
                                    <div class="col s6 m6 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s6 m6 l2 xl2">
                                            <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                           
                                    </div>
                                        
                                </div>
                            </form>

                       <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                          <table class="bordered striped responsive-table">
                              <thead>
                                <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                      <th style='font-size:12px;padding:7px 8px;display:none;'>Network Code</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                      <th style='font-size:12px;padding:7px 8px;display:none;'>API Code</th>
                                      <th style='font-size:12px;padding:7px 8px;'>API Provider </th>
                                      <th style='font-size:12px;padding:7px 8px;'>Line Type</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Min Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Max Amount</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Action</th>
                                </tr>
                              </thead>
                              <tbody id="tbl_body">
                                <?php 
                                     $j = 1;
                                     foreach($net1 as $f)
                                     {
                                       echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                       echo "<td id='code_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_line_tr_id."</td>";
                                       echo "<td id='name_".$j."' style='font-size:12px;padding:7px 8px;display:none;'>".$f->net_code."</td>";

                                       foreach($network as $r)
                                       {
                                           if($r->net_code == $f->net_code)
                                               echo "<td style='font-size:12px;padding:7px 8px;'>".$r->net_name."</td>";
                                       }

                                       echo "<td id='short_".$j."' style='font-size:12px;padding:7px 8px;display:none;'>".$f->api_code."</td>";

                                       foreach($api1 as $r)
                                       {
                                           if($r->api_code == $f->api_code)
                                               echo "<td style='font-size:12px;padding:7px 8px;'>".$r->api_name."</td>";
                                       }

                                       
                                       echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_line_type."</td>";
                                       echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_line_value_1."</td>";
                                       echo "<td id='type_".$j."' style='font-size:12px;padding:7px 8px;'>".$f->net_line_value_2."</td>";

                                      

                                       if($f->net_line_status == 1)
                                       {
                                         echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>LIVE</td>";
                                       }
                                       else
                                       {
                                         echo "<td id='status_".$j."' style='font-size:12px;padding:7px 8px;'>INACTIVE</td>";
                                       }
                                       
                                      
                                       echo "<td style='font-size:12px;padding:7px 8px;'>";
                                       echo "<button class='btn-floating btn-sm waves-effect waves-light red' id='delete_".$f->net_line_tr_id."'>
                                       <i class='small material-icons '>clear</i></td></tr>";
                                       $j++;
                                    }
                                ?>
                                
                              </tbody>
                          </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op['output']."', 'success'); 
            });
            </script>";
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $("#id_net_line_min, #id_net_line_max").keydown(function (e) 
            {
                numbersOnly(e);
            });
            
            $('#id_net_code').change(function(e) 
            {
                var net_code = $('option:selected', this).val();
                if(net_code != "-")
                {
                    window.location.href = "<?php echo url('/'); ?>/network_line/"+net_code;
                }

            });

            $("#id_net_line_type").change(function(e)
            {
                var ty = $('option:selected', this).val();
                
                if(ty != "-")
                {
                    if(ty == "ALL")
                    {
                        $("#id_net_line_min").val("*");
                        $("#id_net_line_max").val("*");
                        $("#id_net_line_min").hide();
                        $("#id_net_line_max").hide();
                        $("#id_lab_min").text("Minimum Amount");
                        $("#id_f1").hide();
                        $("#id_f2").hide();
                        M.updateTextFields();
                    }
                    else if(ty == "RANGE_VALUES")
                    {
                        $("#id_net_line_min").val("");
                        $("#id_net_line_max").val("");
                        $("#id_net_line_min").show();
                        $("#id_net_line_max").show();
                        $("#id_lab_min").text("Minimum Amount");
                        $("#id_f1").show();
                        $("#id_f2").show();
                        M.updateTextFields();
                    }
                    else if(ty == "SPECIFIC_AMOUNT")
                    {
                        $("#id_net_line_min").val("");
                        $("#id_net_line_max").val("*");
                        $("#id_net_line_min").show();
                        $("#id_net_line_max").hide();
                        $("#id_lab_min").text("Specific Amount");
                        $("#id_f1").show();
                        $("#id_f2").hide();
                        M.updateTextFields();
                    }
                }
                else
                {
                    $("#id_net_line_min").val("");
                    $("#id_net_line_max").val("");
                    $("#id_net_line_min").show();
                    $("#id_net_line_max").show();
                    $("#id_lab_min").text("Minimum Amount");
                    $("#id_f1").show();
                    $("#id_f2").show();
                    M.updateTextFields();
                }
            });
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Submit!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('network_line_store')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                            
                        }
                    });
                
                
            }); 

            $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid1=gid.split("delete_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                  swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Delete!'
                            }).then((result) => {
                            if (result.value) {
                                
                                var code = nid1[1];
                                window.location.href = "<?php echo url('/'); ?>/network_line_delete/"+code;
                            }
                            else
                            {
                                swal("Cancelled", "Sorry....", "error");
                                
                            }
                        });
                    
                }
            });

            function numbersOnly(e)
			{
				// Allow: backspace, delete, tab, escape, enter and .
				if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) 
				{
					 // let it happen, don't do anything
					 return;
				}
				// Ensure that it is a number and stop the keypress
				if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
					e.preventDefault();
				}
			}
      });
    </script>
    </body>
</html>
