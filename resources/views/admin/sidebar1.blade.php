<!-- Navbar goes here -->
<ul id="dropdown1" class="dropdown-content">
    <li><a href="#!">Bal : {{$bal}}</a></li>
    <li><a href="{{url('changepassword')}}">Change Password</a></li>
    <li class="divider"></li>
    <li><a href="{{url('logout')}}">logout</a></li>
</ul>
<div class="navbar-fixed " style = "box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15); -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15);">
    <nav class="#0d47a1 white darken-4" style = "box-shadow:none;">
        <div class="row">
            <div class="col s6 m6 l3 xl3">
                <a href="#" data-target="slide-out" class="sidenav-trigger light-blue-text text-darken-4" style="display: block;margin: 0 10px;"><i class="material-icons">menu</i></a>
                <img class="responsive-img" src="{{ asset('img/brand/logo.png') }}" style="height: 46px; width: 85px;padding-top: 15px;">
            </div>
            <div class="col s6 m6 l9 xl9 right-align">
                <ul class="right ">
                    <li><a href="#" style="padding: 0 6px;font-weight:600;" class = "light-blue-text text-darken-4">&#x20B9;{{$bal}}</a></li>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-trigger light-blue-text text-darken-4" href="#!" data-target="dropdown1" style="padding: 0 6px;padding-top: 2px;"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
        
    </nav>
</div>
        
<ul id="slide-out" class="sidenav #1a237e indigo darken-4">
    <li>
        <div class="user-view">
        <div class="background">
            <img src="{{ asset('img/brand/back.jpg') }}">
        </div>
        <a href="#user"><img class="circle" src="{{ asset('img/brand/logo.png') }}"></a>
        <a href="#name"><span class="white-text name">{{$uname}}</span></a>
        <a href="{{url('dashboard')}}"><span class="white-text name" style = "margin-top:4px;">{{$user->mode}}</span></a>
        </div>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Server <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                <li><a href="{{url('serveronoff')}}" class = "white-text">OnOff Entry</a></li>
                <li><a href="{{url('ad_view')}}" class = "white-text">Advertisement</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text"> Network <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('network_type')}}" class = "white-text">Network Type Entry</a></li>
                    <li><a href="{{url('network')}}" class = "white-text">Network Entry</a></li>
                    <li><a href="{{url('network_surplus')}}" class = "white-text">Network Surplus Entry</a></li>
                    <li><a href="{{url('network_line')}}" class = "white-text">Network Line Entry</a></li>
                    <li><a href="{{url('network_line_apipartner')}}" class = "white-text">API Partner Line </a></li>
                    <li><a href="{{url('offer_line')}}" class = "white-text">Other Line </a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text"> API Master <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('n_apirequest')}}" class = "white-text">API Request Entry</a></li>
                    <li><a href="{{url('n_apirequest_netwokr')}}" class = "white-text">API Network Entry</a></li>
                    <li><a href="{{url('apirequest_edit')}}" class = "white-text">API Request Edit</a></li>
                    <li><a href="{{url('n_apiresult')}}" class = "white-text">API Result Entry</a></li>
                    <li><a href="{{url('apiprovider_method')}}" class = "white-text">API Method Entry</a></li>
                    <li><a href="{{url('api_details_1')}}" class = "white-text">API URL Details</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text"> User <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('user')}}" class = "white-text">User Creation Entry</a></li>
                    <li><a href="{{url('user_view')}}" class = "white-text">User Details</a></li>
                    <li><a href="{{url('user_approval_view')}}" class = "white-text">Admin Approval</a></li>
                    <li><a href="{{url('changeparent')}}" class = "white-text">Parent Change</a></li>
                    <li><a href="{{url('apiagent')}}" class = "white-text">Apipartner Browser</a></li>
                    
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text"> Payment Details <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('opening_balance_view')}}" class = "white-text">Opening Balance</a></li>
                    <li><a href="{{url('remotepayment')}}" class = "white-text">Remote Payment</a></li>
                    <li><a href="{{url('paymentaccept')}}" class = "white-text">Payment Accept</a></li>
                    <li><a href="{{url('refundpayment')}}" class = "white-text">Refund Payment</a></li>
                    <li><a href="{{url('payment_report')}}" class = "white-text">Payment Details</a></li>
                    <li><a href="{{url('stockdetails')}}" class = "white-text">Stock Details </a></li>
                    <li><a href="{{url('admin_user_stock_1')}}" class = "white-text">User Stock Details </a></li>
                    <li><a href="{{url('backup_distributor_all')}}" class = "white-text">Distributor Stock Details </a></li>
                    <li><a href="{{url('backup_view_all')}}" class = "white-text">Retailer Stock Details </a></li>
                    <li><a href="{{url('stockdetails_all')}}" class = "white-text">All Stock </a></li>
                   
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text"> Collection <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('agent_allocation')}}" class = "white-text">Agent Allocation</a></li>
                    <li><a href="{{url('collection')}}" class = "white-text">Pending Report</a></li>
                    <li><a href="{{url('collection_verify')}}" class = "white-text">Collection Verify</a></li>
                  
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Recharge <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('rechargedetails')}}" class = "white-text">Recharge Details</a></li>
                    <li><a href="{{url('rechargedetails_distributor')}}" class = "white-text">Distributor Recharge</a></li>
                    <li><a href="{{url('rechargerequest')}}" class = "white-text">Recharge Request </a></li>
                    <li><a href="{{url('serverreply')}}" class = "white-text">Server Reply </a></li>
                    <li><a href="{{url('pendingreport')}}" class = "white-text">Pending Recharge </a></li>
                    <li><a href="{{url('pendingbill')}}" class = "white-text">Pending EBBill </a></li>
                    <li><a href="{{url('pendingmoney')}}" class = "white-text">Pending Money Transfer </a></li>
                    <li><a href="{{url('rechargeinfo')}}" class = "white-text">Recharge Info </a></li>
                    <li><a href="{{url('admin_user_old_recharge_1')}}" class = "white-text">Old Recharge </a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Money Transfer <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('bank_agent_view_all')}}" class = "white-text">Agent</a></li>
                    <li><a href="{{url('bank_remitter_view_all')}}" class = "white-text">Remitter</a></li>
                    <li><a href="{{url('admin_ben_view_1')}}" class = "white-text">Beneficiary</a></li>
                    <li><a href="{{url('moneyreport')}}" class = "white-text">Report</a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

    <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li>
            <a class="collapsible-header  white-text">Utilities <i class="material-icons white-text">arrow_drop_down</i></a>
            <div class="collapsible-body #1e88e5 blue darken-1 ">
              <ul >
                    <li><a href="{{url('smsdetails')}}" class = "white-text">Sms Details</a></li>
                    <li><a href="{{url('smssend')}}" class = "white-text">Sms Send</a></li>
                    <li><a href="{{url('complaint')}}" class = "white-text">Complaints</a></li>
                    <li><a href="{{url('complaint_view')}}" class = "white-text">Complaint Details</a></li>
                    <li><a href="{{url('offer_entry')}}" class = "white-text">Flash News </a></li>
                    <li><a href="{{url('offer_view')}}" class = "white-text">Flash News details </a></li>
                    <li><a href="{{url('mobile_user')}}" class = "white-text">Mobile User </a></li>
                    <li><a href="{{url('bonrixresult')}}" class = "white-text">Bonrix Result </a></li>
              </ul>
            </div>
          </li>
        </ul>
    </li>

</ul>