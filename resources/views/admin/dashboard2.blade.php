<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('common.top_admin')
    
</head>
<body style = "background-color: white;">
<?php $ax = ['active', '', '', '', '','','','','','','','','','','','',
'', '', '', '','','','','','','','','','','','','', '', '', '','','','','','','','','','','','','','','','','','','',''];  
$bx = ['active', '', '', '', '','','', '', '', '','']; ?>
@include('admin.menu_dashboard', array('bal' => $user->ubal, 'uname' => $user->user, 'act' => $ax, 'act1' => $bx))
       
<div id = "main">
<div class="row">
<div class="content-wrapper-before title-pink" style ="height:250px;"></div>
<div class="col s12">
<div class="container-fluid" >

        
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               
               

                <div class="row" style="margin-bottom: 5px;margin-left:8px;margin-right:8px;margin-top:10px;">
                    
                    <div class="col s12 m12 l3 xl3 " style = "margin:0px 0px;height:180px;">
                        <div class = "card-content " style = "border-radius:10px;padding:0px 0px;">
                            <div class="col s9 m9 l9 xl9 left-align white-text text-darken-4" style="margin: 2px 2px;padding: 1px 2px;">
                                <label class="title-con" style="font-size:14px;">Recharge Total</label><br>
                                <label class="title-con" style="font-size:14px;">Success Total</label><br>
                                <label class="title-con" style="font-size:14px;">Failure Total</label><br>
                                <label class="title-con" style="font-size:14px;">Total Transaction</label><br>
                                <label class="title-con" style="font-size:14px;">Success Transaction</label><br>
                                <label class="title-con" style="font-size:14px;">Failure Transaction</label><br>
                            </div>
                            <div class="col s2 m2 l2 xl2 right-align white-text text-darken-4" style="margin: 0px 0px;padding: 1px 2px;">
                                <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;0.00</label><br>
                                <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;0.00</label><br>
                                <label class="title-con" style="font-size:14px;" id = "re_3">&#x20B9;0.00</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_1">0</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_2">0</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_3">0</label><br>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m12 l9 xl9 " style = "margin:0px 0px;margin-top:10px;">
                        <div class = "card-content " style = "border-radius:10px;padding:0px 0px;">
                            <canvas id="myChart" style = "height:180px"></canvas>
                        </div>
                    </div>

                </div>
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12 card shadow">
                        <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                
                                <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                                <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                
                                <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                                
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                    
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Body --> 
                
            
            </div>
        </div>
        <!-- End Page Layout  -->

</div>
</div>
</div>
</div>    
   

    @include('common.bottom_admin')
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0-rc.1/Chart.js"></script>
  
  
   

    <script>
     $(document).ready(function() 
	 {
        var c = 1;
        load_data();
        load_data1();
        load_data2();
        startTime();
        $(".dropdown-trigger").dropdown();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        window.setInterval(function(){
            /// call your function here
            load_data();
            load_data1();
            load_data2();
        }, 17500);
        
        function load_data()
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            jQuery.ajax({
                url:'adminresult_data',
                type: 'GET',
                success: function( data ){

                    $('#tbl_body').html(data);
                    if(c == 1)
                    {
                        $('#tim').css("color", "#08E4FA");
                        $('#tim').css("font-weight", "bold");
                        c = 2;
                    }
                    else if(c == 2)
                    {
                        $('#tim').css("color", "#68FF33");
                        $('#tim').css("font-weight", "bold");
                        c = 1;
                    }
                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function load_data1()
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            jQuery.ajax({
                url:'adminrech_data',
                type: 'GET',
                dataType: "json",
                success: function( data ){
                    $('#re_1').text(data['rec_tot']);
                    $('#re_2').text(data['res_tot']);
                    $('#re_3').text(data['ref_tot']);
                    $('#tr_1').text(data['trn_tot']);
                    $('#tr_2').text(data['trs_tot']);
                    $('#tr_3').text(data['trf_tot']);
                
                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function load_data2()
        {
            $.ajaxSetup({
                headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
            });
            jQuery.ajax({
                url:'adminchart_data',
                type: 'GET',
                dataType: "json",
                success: function( data ){
                    var res = data['res'];
                    var ref = data['ref']
                    chart_data(res, ref);
                    //chart_data1();
                },
                error: function (xhr, b, c) {
                    console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                }
            });
        }

        function chart_data(res, ref)
        {
           /* Chart.types.Line.extend({
            name: "LineAlt",
            initialize: function () {
                Chart.types.Line.prototype.initialize.apply(this, arguments);

                var ctx = this.chart.ctx;
                var originalStroke = ctx.stroke;
                ctx.stroke = function () {
                ctx.save();
                ctx.shadowColor = '#000';
                ctx.shadowBlur = 10;
                ctx.shadowOffsetX = 8;
                ctx.shadowOffsetY = 8;
                originalStroke.apply(this, arguments)
                ctx.restore();
                }
            }
            });

            var data = {
                labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                datasets: [{ 
                        data: res,
                        label: "Success",
                        borderColor: "#68FF33",
                        strokeColor: "#ffffff",
                        fill: false
                    }, { 
                        data: ref,
                        label: "Failure",
                        borderColor: "#E74C3C",
                        strokeColor: "#ffffff",
                        fill: false
                    }
                    ],
                options: {
                    
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: '#ffffff'
                        }
                    }
                }
            };

            var ctx = document.getElementById("myChart").getContext("2d");
                var canvas = new Chart(ctx).LineAlt(data, {
                datasetFill: false
            });*/

           new Chart(document.getElementById("myChart"), {
                type: 'line',
                data: {
                    labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],
                    datasets: [{ 
                        data: res,
                        label: "Success",
                        borderColor: "#68FF33",
                        strokeColor: "#ffffff",
                        fill: false
                    }, { 
                        data: ref,
                        label: "Failure",
                        borderColor: "#E74C3C",
                        strokeColor: "#ffffff",
                        fill: false
                    }
                    ]
                },
                options: {
                    
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: '#ffffff'
                        }
                    }
                }
            });
        }

        function startTime() {
            var today = new Date();
            var day = today.getDate();
            var mn = today.getMonth();
            var yr = today.getFullYear();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            var tim  = day + "-" + mn+"-"+ yr+ " " +h + ":" + m + ":" + s;
            $('#tim').html(tim);
            var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
                    
            
      });
    </script>
    </body>
</html>
