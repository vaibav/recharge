@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Network Package</h2>
                <h5 class="text-white op-7 mb-3">Entry</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{url('n_apirequest')}}" class="btn btn-secondary btn-round">View</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('n_netpack_store')}}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class = "row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pack_id_new">New Package (Don't Select Existing Package)</label>
                    <input type="text" class="form-control" id="id_pack_id_new" name="pack_id_new">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pack_id_old">Existin package</label>
                    <select class="form-control" id="id_pack_id_old" name="pack_id_old">
                    <option value="-" >---Don't Select if New Package--</option>
                    <?php
                        foreach($package as $f)
                        {
                            if($f->pack_id == $pack_id)
                                echo "<option value='".$f->pack_id."' selected>".$f->pack_name."</option>";
                            else
                                echo "<option value='".$f->pack_id."' >".$f->pack_name."</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_net_code">Network Name</label>
                    <select class="form-control" id="id_net_code" name="net_code">
                    <option value="" >---Select--</option>
                    <?php
                        foreach($network as $f) {
                            if($f->net_code == $net_code){
                                echo "<option value='".$f->net_code."' selected>".$f->net_name."</option>";
                            }
                            else {
                                echo "<option value='".$f->net_code."' >".$f->net_name."</option>";
                            }
                        }
                    ?>
                    </select>
                </div>
            </div>
        </div>

        <div class = "row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_from_amt">From Amount</label>
                    <input type="text" class="form-control" id="id_from_amt" name="from_amt">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_to_amt">To Amount</label>
                    <input type="text" class="form-control" id="id_to_amt" name="to_amt">
                </div>
            </div>
        </div>

        <div class = "row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_net_per">Network Percentage(%)</label>
                    <input type="text" class="form-control" id="id_net_per" name="net_per" placeholder = "Enter 0 if Empty">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_net_surp">SurPlus Amount</label>
                    <input type="text" class="form-control" id="id_net_surp" name="net_surp" placeholder = "Enter 0 if Empty">
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Save</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

    <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                            <th style='font-size:12px;padding:7px 8px;'>NO</th>
                            <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                            <th style='font-size:12px;padding:7px 8px;'>Package</th>
                            <th style='font-size:12px;padding:7px 8px;'>Network</th>
                            <th style='font-size:12px;padding:7px 8px;'>From Amount</th>
                            <th style='font-size:12px;padding:7px 8px;'>To Amount</th>
                            <th style='font-size:12px;padding:7px 8px;'>Percentage</th>
                            <th style='font-size:12px;padding:7px 8px;'>Surplus Amount</th>
                            <th style='font-size:12px;padding:7px 8px;'>Status</th>
                            <th style='font-size:12px;padding:7px 8px;'>Action</th>
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                        <?php echo $pack; ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $("#id_from_amt, #id_to_amt, #id_net_per, #id_net_surp").keydown(function (e) 
    {
        numbersOnly(e);
    });

    $('#id_pack_id_old').change(function(e) {

        var pack_id = $('option:selected', this).val();
        if(pack_id != "-")
        {
            window.location.href = "<?php echo url('/'); ?>/n_netpack/"+pack_id;
        }

    });

    $('#id_net_code').change(function(e) {

        var pack_id = $('option:selected', '#id_pack_id_old').val();
        var net_code = $('option:selected', this).val();
        if(pack_id != "-" && net_code != "")
        {
            window.location.href = "<?php echo url('/'); ?>/n_netpack/"+pack_id + "/" +net_code;
        }

    });

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();

        var z = [];
        z= checkData();

        if(z[0] == 0)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('n_netpack_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        }
        else {
            swal("Cancelled", z[1], "error");
        }
        
    }); 

    function checkData()
    {
        var z = [];
        z[0] = 0;
        z[1] = "Success";

        var pack_old = $('option:selected', "#id_pack_id_old").val();
        var pack_new = $("#id_pack_id_new").val();

        if(pack_old == "-"){
            if(pack_new == "")
            {
                z[0] = 1;
                z[1] = "New Package Name is not Entered";
            }
        }

        if(pack_new == ""){
            if(pack_old == "-")
            {
                z[0] = 1;
                z[1] = "Old Package is not Selected";
            }
        }

        if($("#id_net_code").val() == "" ) {
            z[0] = 1;
            z[1] = "Network is not Selected";
        }

        if($("#id_from_amt").val() == "" ) {
            z[0] = 1;
            z[1] = "From Amount is not Entered";
        }

        if($("#id_to_amt").val() == "" ) {
            z[0] = 1;
            z[1] = "To Amount is not Entered";
        }

        if($("#id_net_per").val() == "" ) {
            z[0] = 1;
            z[1] = "Percentage is not Entered";
        }

        if($("#id_net_surp").val() == "" ) {
            z[0] = 1;
            z[1] = "Surplus Amount is not Entered";
        }

        return z;
    }

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;

        var nid=gid.split("id_status_");
        if(nid.length>1)
        {
            swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Change Status!'
                    }).then((result) => {
                    if (result.value) {
                        var tr_id = nid[1];
                        window.location.href = "<?php echo url('/'); ?>/n_netpack_status/"+tr_id;
                    }
                    else {
                        swal("Cancelled", "Sorry....", "error");
                    }
                });
            
        }

        var nid1=gid.split("id_delete_");
        if(nid1.length>1)
        {
            swal({
                    title: 'Are you sure?',
                    text: "Confirmation Alert",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete!'
                    }).then((result) => {
                    if (result.value) {
                        var tr_id = nid1[1];
                        window.location.href = "<?php echo url('/'); ?>/n_netpack_delete/"+tr_id;
                    }
                    else{
                        swal("Cancelled", "Sorry....", "error");
                    }
                });
            
        }
    });

   
    function numbersOnly(e)
    {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188, 189, 109]) !== -1 ||
            // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) 
        {
                // let it happen, don't do anything
                return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
  
   
});
</script>
@stop
@stop


