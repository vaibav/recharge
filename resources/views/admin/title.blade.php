<ol class="breadcrumb">
    <li class="breadcrumb-item">Home</li>
    <li class="breadcrumb-item">
    <?php 
        echo "<a href='".url('dashboard')."'>".$user->mode."</a>";
        
    ?>
    
    </li>
    <li class="breadcrumb-item active">{{$title}}</li>
    <!-- Breadcrumb Menu-->
    <li class="breadcrumb-menu d-md-down-none">
    <div class="btn-group" role="group" aria-label="Button group">
        
        <a class="btn" href="./">
        <i class="icon-graph"></i>  Welcome {{$uname}}</a>
        <a class="btn" href="#">
        <i class="fa fa-bell-o"></i> Balance
        <span class="badge badge-info">{{$bal}}</span>
        </a>
    </div>
    </li>
</ol>