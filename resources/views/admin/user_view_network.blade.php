@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">User Network Package</h2>
                <h5 class="text-white op-7 mb-3">User package details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       

        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                <div class="table-responsive">
                    <table class="display table table-striped table-hover ">
                        <thead>
                        <tr>
                          <th style='font-size:12px;padding:7px 8px;'>NO</th>
                          <th style='font-size:12px;padding:7px 8px;'>Code</th>
                          <th style='font-size:12px;padding:7px 8px;'>Network</th>
                          <th style='font-size:12px;padding:7px 8px;'>From Amount</th>
                          <th style='font-size:12px;padding:7px 8px;'>To Amount</th>
                          <th style='font-size:12px;padding:7px 8px;'>Percentage </th>
                          <th style='font-size:12px;padding:7px 8px;'>Surplus Charge </th>
                          
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                            <?php 
                                
                                $j = 1;
                                $total_amt = 0;

                                foreach($packDetails as $f)
                                {
                                    $netName = "";
                                    $user_kyc = "";
                                    foreach($networkDetails as $r)
                                    {
                                        if($f->net_code == $r->net_code)
                                        {
                                            $netName = $r->net_name;
                                            break;
                                        }
                                    }

                                   

                                  echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_code."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$netName."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->from_amt."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->to_amt."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_per."</td>";
                                  echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->net_surp."</td>";
                                 

                                
                                
                                  
                                  echo "</tr>";

                                  
                                  $j++;
                                }


                                
                            ?>

                        </tbody>
                    </table>
                    
                </div>
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");
    $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("view_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            window.location.href = "<?php echo url('/'); ?>/user_view/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                }
                var nid1=gid.split("network_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            window.location.href = "<?php echo url('/'); ?>/user_view_network/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                   
                   
                    
                }

                var nid2=gid.split("status_");
                if(nid2.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid2[1];
                            window.location.href = "<?php echo url('/'); ?>/user_update_status/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });

                }

                var nid3=gid.split("delete_");
                if(nid3.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Delete it!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid3[1];
                            window.location.href = "<?php echo url('/'); ?>/user_delete/"+code;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });

                }
            });

            $("#search").on("keyup", function() {
                var value = $(this).val();
                // Hide all table tbody rows
                $('table tbody tr').hide();

                // Count total search result
                var len = $('table tbody tr:not(.notfound) td:nth-child(3):contains("'+value+'")').length;

                if(len > 0){
                // Searching text in columns and show match row
                $('table tbody tr:not(.notfound) td:contains("'+value+'")').each(function(){
                    $(this).closest('tr').show();
                });
                }else{
                $('.notfound').show();
                }
            });
    
   
  
   
});
</script>
@stop
@stop