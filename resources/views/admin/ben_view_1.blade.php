<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Beneficiary Details</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <form id = "rech_form" class="form-horizontal" action="{{url('admin_ben_view_2')}}" method="get" accept-charset="UTF-8">
                                  
                                
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="msisdn" type="text" name="msisdn"  >
                                        <span class="text-danger">{{ $errors->first('msisdn') }}</span>
                                        <label for="msisdn">Customer Mobile No</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>

                               
                                <div class="row">
                                    <div class="col s4 m4 l2 xl2">
                                        <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                <i class="material-icons right">send</i>
                                        </button>
                                    </div>

                                   
                                    
                                        
                                </div>
                            </form>

                            <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

          
           
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();

                var fd = $("#msisdn").val();

                if(fd != "")
                {
                    $('#loader').show();
                    $('#rech_form').attr('action', "{{url('admin_ben_view_2')}}");
                    $('#rech_form').submit();
                }
                else if(fd == "")
                {
                    swal("Cancelled", "Customer Mobile No is Empty...", "error");
                }
                
 
                
            }); 

            
      });
    </script>
    </body>
</html>
