@extends('layouts.admin_new')
@section('styles')
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Pending Money  </h2>
                <h5 class="text-white op-7 mb-3">Money Transfer Pending Report</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
     <div class="card">
        <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-striped ">
                    <thead>
                    <tr>
                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                        <th style='font-size:12px;padding:7px 8px;'>Trans Id</th>
                        <th style='font-size:12px;padding:7px 8px;'>API Trans Id</th>
                        <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                        <th style='font-size:12px;padding:7px 8px;'>Benificiary Account</th>
                        <th style='font-size:12px;padding:7px 8px;'>Bank</th>
                        <th style='font-size:12px;padding:7px 8px;'>Amount</th>
                        <th style='font-size:12px;padding:7px 8px;'>Date</th>
                        <th style='font-size:12px;padding:7px 8px;'>Req. Status</th>
                        <th style='font-size:12px;padding:7px 8px;'>Status</th>
                        <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                        <th style='font-size:12px;padding:7px 8px;'>Operator ID</th>
                        <th style='font-size:12px;padding:7px 8px;'>Action</th>
                          
                    </tr>
                    </thead>
                    <tbody id="tbl_body">
                    <?php 
                        $j = 1;
                                    
                        foreach($bank as $f)
                        {
                            
                            $net_name = "";
                            $py = $f->rech_bank;
                            $px = explode("--", $py);
                            $bank = "";
                            $mode = "-";

                            if(sizeof($px) > 1)
                            {
                                
                                if($px[0] == "UTIB") {
                                    $bank = "AXIS BANK";
                                }
                                else if($px[0] == "BARB") {
                                    $bank = "BANK OF BARODA";
                                }
                                else if($px[0] == "CNRB") {
                                    $bank = "CANARA BANK";
                                }
                                else if($px[0] == "CORP") {
                                    $bank = "CORPORATION BANK";
                                }
                                else if($px[0] == "CIUB") {
                                    $bank = "CITY UNION BANK";
                                }
                                else if($px[0] == "HDFC") {
                                    $bank = "HDFC BANK";
                                }
                                else if($px[0] == "IBKL") {
                                    $bank = "IDBI BANK LTD";
                                }
                                else if($px[0] == "IDIB") {
                                    $bank = "INDIAN BANK";
                                }
                                else if($px[0] == "IOBA") {
                                    $bank = "INDIAN OVERSEAS BANK";
                                }
                                else if($px[0] == "ICIC") {
                                    $bank = "ICICI BANK";
                                }
                                else if($px[0] == "KVBL") {
                                    $bank = "KARUR VYSYA BANK";
                                }
                                else if($px[0] == "SBIN") {
                                    $bank = "STATE BANK OF INDIA";
                                }
                                else if($px[0] == "SYNB") {
                                    $bank = "SYNDICATE BANK";
                                }
                                else if($px[0] == "TMBL") {
                                    $bank = "TAMILNADU MERCANTILE BANK";
                                }
                                else if($px[0] == "VIJB") {
                                    $bank = "VIJAYA BANK";
                                }
                                else {
                                    $bank = $f->bk_code;
                                }

                                $mode = $px[1];
                            }

                            $req_id = '-';
                            if($f->request != null)
                            {
                                $req_id = $f->request->req_id;
                            }
                         
                            echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->api_trans_id."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;' id='user_".$f->trans_id."'>".$f->user_name."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_mobile."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$bank."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;' id='amt_".$f->trans_id."'>".$f->rech_amount."</td>"; 
                            echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->rech_date."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 180px;word-break: break-word;'>".$req_id."</div></td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;'><button class='btn btn-icon btn-round btn-primary' ><i class='fa fa-bicycle' style='color:white'></i></button></td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;' id='mode_".$f->trans_id."'>".$mode."</td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'><input type='text' id='opr_".$f->trans_id."' class='form-control' /></td>";
                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>";
                            echo "<button class='btn btn-icon btn-round btn-success '  id='success_".$f->trans_id."'><i class='fa fa-check' style='color:white'></i></button>&nbsp;&nbsp;&nbsp;";
                            echo "<button class='btn btn-icon btn-round btn-danger' id='failure_".$f->trans_id."'><i class='fa fa-times' style='color:white'></i></button></td>";
                            echo "</tr>";
                                                                
                            $j++;
                            
                        }

                    ?>
                    
                    </tbody>
                </table>
                </div>
            </div>
        </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- End -->
    <br>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
    if(session()->has('msg'))
    {
        $op = session('msg');
        echo "<script>
            $(document).ready(function() 
            {
                swal('Alert!', '".$op."', 'success'); 
            });
            </script>";
        session()->forget('msg');
    }
?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#tbl_body').delegate('button', 'click', function(e) {
        e.preventDefault();
        var gid=this.id;
        var nid=gid.split("success_");
        if(nid.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Success!'
                }).then((result) => {
                if (result.value) {
                    var code = nid[1];
                    var opr = $('#opr_' + code).val();
                    var amt = $('#amt_' + code).text();
                    //alert(code +"----"+ opr + "---" + amt);
                    window.location.href = "<?php echo url('/'); ?>/pendingmoney_success/" + code + "/" + opr + "/" + amt;
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
            
        }

        var nid1=gid.split("failure_");
        var net=0;
        var ctx=0;
        if(nid1.length>1)
        {
            swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Failed!'
                }).then((result) => {
                if (result.value) {
                    var code = nid1[1];
                    var opr = $('#opr_' + code).val();
                    var amt = $('#amt_' + code).text();
                    //alert(code +"----"+ opr);
                    window.location.href = "<?php echo url('/'); ?>/pendingmoney_failure/" + code + "/" + opr + "/" + amt;
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                }
            });
            
        }
    });

   



    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop