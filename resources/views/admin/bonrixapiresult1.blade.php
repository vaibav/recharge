<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
            <div class="card " style = "margin-top:60px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Bonrix Result Details</span>
                </div>
                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <table class="bordered striped responsive-table">
                                <thead>
                                    <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                        <th style='font-size:12px;padding:7px 8px;'>ID</th>
                                        <th style='font-size:12px;padding:7px 8px;'>URL</th>
                                        <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                    </tr>
                                </thead>
                                <tbody id="tbl_body" id="tbl_body">
                                <?php 
                                    
                                    
                                ?>
                                
                                </tbody>
                        </table>
                        
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
            var c = 1;
            load_data();
            $(".dropdown-trigger").dropdown();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            window.setInterval(function(){
                /// call your function here
                load_data();
			}, 17000);

            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'bonrixresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d);
						if(c == 1)
						{
							$('#tim').css("color", "red");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("color", "green");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }
      });
    </script>
    </body>
</html>
