@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Account Check </h2>
                <h5 class="text-white op-7 mb-3">User Account Balance Check</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="#" class="btn btn-secondary btn-round">view</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('n_check_account_2')}}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group" style="position:relative">
                    <label for="id_f_date">From Date</label>
                    <input type="text" class="form-control" id="id_f_date"  name="f_date">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group" style="position:relative">
                    <label for="id_t_date">To Date</label>
                    <input type="text" class="form-control" id="id_t_date"  name="t_date">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="id_user_name">User Name</label>
                    <select class="form-control" id="id_user_name" name="user_name">
                    <option value="-" >---Select--</option>
                    <?php
                        foreach($users as $f)
                        {
                            echo "<option value='".$f->user_name."'>".$f->user_name."</option>";
                        }
                    ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <br/>
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
            
            
        </div>

       

             
       
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_f_date, #id_t_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

   
    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();

        var fd = $('#id_f_date').val();
        var td = $('#id_t_date').val();
        var ur = $('#id_user_name').val();
        
        if(fd != "" && td != "" && ur != "-")
        {
            startloader();
            $('#rech_form').attr('action', "{{url('n_check_account_2')}}");
            $('#rech_form').submit();
        }
        else{
            swal("Cancelled", "Sorry...Anyone Date is Empty", "error");
        }
        
    }); 

   



    function startloader()
    {
        swal({
            title: "Processing...",
            text: "Please wait",
            showConfirmButton: false,
            allowOutsideClick: false
        });

        swal.showLoading();
        //swal.close();
    }

    function stoploader()
    {
        swal.close();
    }
    
  
   
});
</script>
@stop
@stop