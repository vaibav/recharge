@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Server Reply</h2>
                <h5 class="text-white op-7 mb-3">Recharge Reply Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               <a href="{{url('serverreply')}}" class="btn btn-secondary btn-round">Back</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
        

        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped ">
                        <thead>
                        <tr>
                              <th style='font-size:12px;padding:7px 8px;'>NO</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 1</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 2</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 3</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 4</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 5</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 6</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 7</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 8</th>
                              <th style='font-size:12px;padding:7px 8px;'>Field 9</th>
                              <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                              <th style='font-size:12px;padding:7px 8px;'>Api</th>
                              <th style='font-size:12px;padding:7px 8px;'>Date</th>
                              
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                        <?php 
                                  
                            $j = 1;
                              
                            foreach($reply as $f)
                            {
                                $api_name = "-";
                                foreach($api as $r)
                                {
                                    if($f->a11 == $r->api_code ) {
                                        $api_name = $r->api_name;
                                    }
                                }
                                
                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a1."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a2."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a3."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a4."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a5."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a6."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a7."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a8."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a9."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->a10."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->created_at."</td>";
                                echo "</tr>";
                                                                        
                                $j++;
                            }
                                
                        ?>

                        </tbody>
                    </table>
                    {{ $reply->links('vendor.pagination.atlantis') }}
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");
   
});
</script>
@stop
@stop