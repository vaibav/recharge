@extends('layouts.admin_new')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" rel="stylesheet">
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Payment Report</h2>
                <h5 class="text-white op-7 mb-3">Payment Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('payment_view')}}" method="get" accept-charset="UTF-8">
        

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_f_date">From Date</label>
                    <input type="text" class="form-control" id="id_f_date" name="f_date" value ="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_t_date">To Date</label>
                    <input type="text" class="form-control" id="id_t_date" name="t_date" value ="<?php echo date('Y-m-d'); ?>">
                </div>
            </div>
            <div class="col-md-4">
                
            </div>
        </div>

        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_api_result_type">Payment Type</label>
                    <select class="form-control" id="id_trans_type"  name = "trans_type">
                         <option value="ALL" selected>ALL</option>
                        <option value="FUND_TRANSFER" >FUND TRANSFER</option>
                        <option value="REMOTE_PAYMENT" >REMOTE PAYMENT</option>
                        <option value="REFUND_PAYMENT" >REFUND PAYMENT</option>
                        <option value="SELF_PAYMENT" >SELF PAYMENT</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6" >
                
            </div>
        </div>

       

        

       
       
     
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Submit</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

   
    <!-- End -->
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_f_date, #id_t_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    
    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        $('#rech_form').attr('action', "{{url('payment_view')}}");
        $('#rech_form').submit();
        
        
    }); 
   
});
</script>
@stop
@stop