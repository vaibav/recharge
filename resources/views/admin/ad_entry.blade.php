@extends('layouts.admin_default')
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/css/bootstrap-material-datetimepicker.min.css" rel="stylesheet" />
<style>
.dtp > .dtp-content > .dtp-date-view > header.dtp-header { background: rgb(72, 173, 219) !important;  }
.dtp div.dtp-date, .dtp div.dtp-time { background: rgb(72, 173, 219) !important; }
</style>
@stop
@section('content')

<!-- Your code here-->
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div style = "height:20px;margin-top:-25px;text-align:right;padding-right:15px;">
            <a href ="{{url('ad_view')}}" class= "btn btn-primary btn-fab  btn-round"><i class='small material-icons' style='color:white'>arrow_back</i></a>
        </div>
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('ad_store')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_ad_data" >Enter Advertisement</label>
                    <textarea id="id_ad_data" name="ad_data" class="form-control" rows="3"> </textarea>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="form-group">
                
               
                </div>
            </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_ad_order">Select Order</label>
                    <select class="form-control " data-style="btn btn-link" id="id_ad_order" name="ad_order">
                        <option value="-">---Select Order---</option>
                        <?php 
                            for($i = 1;$i <= 50; $i++)
                            {
                                echo "<option value ='".$i."'>".$i."</option>";
                            }

                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="form-group">
                
                </div>
            </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_from_date" >From Date</label>
                    <input id="id_from_date" type="text" name="from_date" class="form-control " >
                    <span class="text-danger red-text">{{ $errors->first('from_date') }}</span>
                    
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="form-group">
                    <label for="id_to_date" >To Date</label>
                    <input id="id_to_date" type="text" name="to_date" class="form-control " >
                    <span class="text-danger red-text">{{ $errors->first('to_date') }}</span>
                    
                </div>
            </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group form-file-upload form-file-simple">
                    <input type="text" class="form-control inputFileVisible" placeholder="Select Image (<30kb)">
                    <input type="file" class="inputFileHidden" id="id_ad_photo" name="ad_photo">
                </div>
            </div>
            <div class="col-md-4 text-center">
                
            </div>
            </div>
            
            <br><br><br>
          
            
            <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Update</button>
               
            </div>
            </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    </div>
    
</div>
<!-- Code Ending-->
            
          

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-datetimepicker/2.7.1/js/bootstrap-material-datetimepicker.min.js"></script>
<?php
if(session()->has('msg'))
{
    $op = session('msg');
    if($op == 1)
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Alert!', 'Advertisement is added Successfully...', 'success'); 
        });
        </script>";
    }
    else if($op == 3)
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Error!', 'Sorry This Order is already Added...', 'error'); 
        });
        </script>";
    }
    else
    {
        echo "<script>
        $(document).ready(function() 
        {
            swal('Error!', 'Sorry...', 'error'); 
        });
        </script>";
    }
    session()->forget('msg');
}
?>


<script>
$(document).ready(function() {
    //alert("hellow");

    $('#id_from_date, #id_to_date').bootstrapMaterialDatePicker({
       format:'YYYY-MM-DD',
       switchOnClick:true,
       time:false
     });

    // New date is selected
    $('#id_from_date, #id_to_date').bootstrapMaterialDatePicker().on('dateSelected',function(e, date){
        $('.dtp-btn-ok').click();
    });

    $('.form-file-simple .inputFileVisible').click(function() {
      $(this).siblings('.inputFileHidden').trigger('click');
    });

    $('.form-file-simple .inputFileHidden').change(function() {
      var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
      $(this).siblings('.inputFileVisible').val(filename);
    });

  

    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, View!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('ad_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "No View...", "error");
                    
                }
            });
        
        
    });  


  
   
});
</script>
@stop
@stop