<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Distributor Stock All Details </span>
                    
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER_TYPE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>CURRENT BALANCE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>FUND TRANSFER</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>SUCCESS RECHARGE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>PENDING RECHARGE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>OLD RECHARGE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>TOTAL BALANCE</th>
                                      <th style='font-size:12px;padding:7px 8px;text-align:right;'>DEVIATION</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                      
                                      echo $data;
                                    
                                  ?>
                                       
                            </tbody>
                        </table>
                       
                       
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	  {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            
      });
    </script>
    </body>
</html>
