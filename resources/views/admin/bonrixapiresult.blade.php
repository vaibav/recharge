<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top')
        
    </head>
    <body class="app header-fixed sidebar-fixed sidebar-lg-hide" style = "background-color: #34495e;">
      @include('admin.header', array('bal' => $user->ubal))


    <div class="app-body">
        @include('admin.sidebar')
      <main class="main">
        <!-- Breadcrumb-->
        @include('admin.title', array('title' => 'Bonrix Result Details', 'uname' => $user->user, 'bal' => $user->ubal))

        <div class="container-fluid" style="padding-left:15px;padding-right:15px;">
          <div class="animated fadeIn">
            
            <!-- USER CODING HERE -->
            <div class="row">
              <div class="col-lg-12" style="padding:6px 5px;">
                <div class="card card-accent-primary">
                  <div class="card-header">
                        <div class = "row">
                            <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <i class="fa fa-align-justify"></i> Bonrix Api Result Details
                            </div>
                            <div class ="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right">
                                <p class="pull-right"  id="tim"> </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" style="padding:6px 10px;">
                      <div class = "row">
                         
                          <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <table class="table table-responsive-sm table-bordered">
                                  <thead>
                                    <tr>
                                      <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>URL</th>
                                      <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                      
                                    </tr>
                                  </thead>
                                  <tbody id="tbl_body" id="tbl_body">
                                    <?php 
                                       
                                      
                                    ?>
                                    
                                  </tbody>
                                </table>
                          </div>
                      </div>
                      
                    </div>
                    <div class="card-footer">
                     
                    </div>
                </div>
              </div>
            </div>
            <!-- USER CODING ENDS-->
           
          </div>
        </div>
      </main>
     
    </div>
    
    @include('admin.footer')
    @include('admin.bottom')

    <script>
     $(document).ready(function() 
	 {
        var c = 1;
           load_data();
           window.setInterval(function(){
			/// call your function here
			load_data();
			}, 17000);

            function load_data()
            {
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
                });
                jQuery.ajax({
                    url:'bonrixresult_data',
                    type: 'GET',
                    success: function( data ){

                        $('#tbl_body').html(data);
                        var d = new Date();
						$('#tim').html(d);
						if(c == 1)
						{
							$('#tim').css("color", "red");
							c = 2;
						}
						else if(c == 2)
						{
							$('#tim').css("color", "green");
							c = 1;
						}
                    },
                    error: function (xhr, b, c) {
                        console.log("xhr=" + xhr + " b=" + b + " c=" + c);
                    }
                });
            }
      });
    </script>
    </body>
</html>
