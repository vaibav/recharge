@extends('layouts.admin_new')
@section('styles')

@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">API Provider</h2>
                <h5 class="text-white op-7 mb-3">URL Entry</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
                <a href="{{url('n_apirequest_network')}}" class="btn btn-secondary btn-round">API Network</a>
                <a href="{{url('n_apiresult')}}" class="btn btn-secondary btn-round">API Result</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
        <form id="rech_form" class="form-horizontal" action="{{url('n_apirequest_store')}}" method="post" accept-charset="UTF-8">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pr_name">Provider Name</label>
                    <input type="text" class="form-control" id="id_pr_name"  name="pr_name">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="id_pr_method">Provider Method</label>
                    <select class="form-control" id="id_pr_method" name="pr_method">
                        <option value = "GET">GET</option>
                        <option value = "POST">POST</option>
                    </select>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="id_pr_url">Provider URL</label>
                    <input type="text" class="form-control" id="id_pr_url" name="pr_url">
                </div>
            </div>
        </div>
            
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary pull-left" id = "btn_submit">Update</button>
                </div>
            </div>
        </div>
            
            <div class="clearfix"></div>
        </form>
        </div>
    </div>
    <br>

    <div class="card">
    <div class="row">

<div class="col-md-12">
    <!-- Basic Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Important Note</h6>
        </div>
        <div class="card-body" >
            When your enter Mobile No field in Url, Replace Original mobile no to &lt;mobileno&gt;, Original Amount field to &lt;amt&gt; and Original Operator field to &lt;opr&gt;, and our transaction id field to &lt;trans_id&gt; 
            <br>
            For example: Original Url
                <br>http://www.domain_name.com/rechage?user_name=bbbb&password=1234&mobileno=9898989898&amt=15&operator=Airtel&urid=VBA100001
                <br>In this Replace <b>9898989898</b> to &lt;mobileno&gt; , Replace <b>15</b> to &lt;amt&gt; , Replace <b>Airtel</b> to &lt;opr&gt; and Replace <b>VBA100001</b> to &lt;trans_id&gt;
            Then Url is :  <br>http://www.domain_name.com/rechage?user_name=bbbb&password=1234&mobileno= &lt;mobileno&gt;&amt=&lt;amt&gt;&operator=&lt;opr&gt;&urid=&lt;trans_id&gt;
        </div>
    </div>
</div>
</div>
    </div>
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')

<?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

<script>
$(document).ready(function() {
    //alert("hellow");
    $('#btn_submit').on('click',function(e)
    {
        e.preventDefault();
        
        swal({
                title: 'Are you sure?',
                text: "Confirmation Alert",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Submit!'
                }).then((result) => {
                if (result.value) {
                    
                    $('#rech_form').attr('action', "{{url('n_apirequest_store')}}");
                    $('#rech_form').submit();
                }
                else
                {
                    swal("Cancelled", "Sorry...", "error");
                    
                }
            });
        
        
    }); 


  
   
});
</script>
@stop
@stop


