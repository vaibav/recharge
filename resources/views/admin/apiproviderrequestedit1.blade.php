<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')

        <link rel="stylesheet" href="{{ asset('css/mstepper.min.css') }}">

        <style>
            .select-dropdown {
                top: 0 !important;
                overflow: scroll;
                
            }
        </style>
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">API Provider - Edit</span>
                </div>

                <div class="card-content" style = "border-radius:5px;padding:5px 5px;">
                    <form id="id_api_form" class="form-horizontal" action="{{url('apirequest_update')}}" method="post" accept-charset="UTF-8">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="pr_code" id="id_pr_code" value="<?php echo $api_code ?>">


                    <ul class="stepper horizontal demos" id="horizontal" style = "margin:2px 2px;">
                        <li class="step">
                            <div data-step-label="Network Entry" class="step-title waves-effect waves-dark">API Details</div>
                            <div class="step-content">
                                
                                <div class="row #ede7f6 #ffcdd2 red lighten-4" style = "margin-bottom:10px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <input id="id_pr_name" type="text" name="pr_name" value= "<?php echo $api1[0]->api_name; ?>">
                                        <span class="text-danger red-text">{{ $errors->first('pr_name') }}</span>
                                        <label for="id_pr_name">Provider Name</label>
                                    </div>
                                    <div class="input-field col s12 m12 l8 xl8" style="margin: 3px 0px;">
                                        <input id="id_pr_url" type="text" name="pr_url" value= "<?php echo $api1[0]->api_url; ?>">
                                        <span class="text-danger red-text">{{ $errors->first('pr_url') }}</span>
                                        <label for="id_pr_url">URL</label>
                                    </div>
                                </div>

                                <div class="row" style = "margin-bottom:10px;">
                                    <div class ="col s12 m12 l12 xl12" style ="height:200px;overflow-y:scroll;">
                                        <table class="bordered striped responsive-table" >
                                            <thead>
                                                <tr>
                                                    <th style='font-size:12px;padding:7px 8px;'>Parameter Type</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Parameter Name</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Value</th> 
                                                </tr>
                                            </thead>
                                            <tbody id="tbl_body">
                                                <?php 
                                                    $j = 1;
                                                    foreach($api2 as $f)
                                                    {
                                                        echo "<tr><td style='font-size:12px;padding:7px 8px;display:none;'>";
                                                        echo "<input type='hidden' id='id_para_tr_id_".$j."' name='para_tr_id_".$j."' value='".$f->api_para_tr_id."'></td>";

                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<select id='id_para_type_".$j."' name='para_type_".$j."' class='form-control'>";
                                                        echo "<option value='-'>---SELECT---</option>";
                                                        if($f->api_para_type == "PRE_DEFINED")
                                                                echo "<option value='PRE_DEFINED' selected>PRE_DEFINED</option>";
                                                            else
                                                                echo "<option value='PRE_DEFINED' >PRE_DEFINED</option>";
                                                        if($f->api_para_type == "USER_DEFINED")
                                                            echo "<option value='USER_DEFINED' selected>USER_DEFINED</option>";
                                                        else
                                                            echo "<option value='USER_DEFINED' >USER_DEFINED</option>";

                                                        if($f->api_para_type == "TRANSACTION_ID")
                                                            echo "<option value='TRANSACTION_ID' selected>TRANSACTION_ID</option>";
                                                        else
                                                            echo "<option value='TRANSACTION_ID'>TRANSACTION_ID</option>";
                                                        if($f->api_para_type == "NETWORK")
                                                            echo "<option value='NETWORK' selected>NETWORK</option>";
                                                        else
                                                            echo "<option value='NETWORK'>NETWORK</option>";
                                                        if($f->api_para_type == "MOBILE_NO")
                                                            echo "<option value='MOBILE_NO' selected>MOBILE_NO</option>";
                                                        else
                                                            echo "<option value='MOBILE_NO'>MOBILE_NO</option>";
                                                            
                                                        if($f->api_para_type == "MESSAGE")
                                                            echo "<option value='MESSAGE' selected>MESSAGE</option>";
                                                        else
                                                            echo "<option value='MESSAGE'>MESSAGE</option>";
                                                            
                                                        if($f->api_para_type == "NETWORK_REPLACE")
                                                            echo "<option value='NETWORK_REPLACE' selected>NETWORK_REPLACE</option>";
                                                        else
                                                            echo "<option value='NETWORK_REPLACE'>NETWORK_REPLACE</option>";
                                                            
                                                        if($f->api_para_type == "AMOUNT")
                                                            echo "<option value='AMOUNT' selected>AMOUNT</option></select></td>";
                                                        else
                                                            echo "<option value='AMOUNT'>AMOUNT</option></select></td>";

                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<input type='text' id='id_para_name_".$j."' name='para_name_".$j."' value='".$f->api_para_name."' class='form-control'></td>";

                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<input type='text' id='id_para_value_".$j."' name='para_value_".$j."' value='".$f->api_para_value."' class='form-control'></td></tr>";
                                                        $j++;
                                                    }

                                                    for(;$j <= 18; $j++)
                                                    {
                                                        echo "<tr><td style='font-size:12px;padding:7px 8px;display:none;'>";
                                                        echo "<input type='hidden' id='id_para_tr_id_".$j."' name='para_tr_id_".$j."' value='0'></td>";
                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<select id='id_para_type_".$j."' name='para_type_".$j."' class='form-control'>";
                                                        echo "<option value='-'>---SELECT---</option>";
                                                        echo "<option value='PRE_DEFINED'>PRE_DEFINED</option>";
                                                        echo "<option value='USER_DEFINED'>USER_DEFINED</option>";
                                                        echo "<option value='TRANSACTION_ID'>TRANSACTION_ID</option>";
                                                        echo "<option value='NETWORK'>NETWORK</option>";
                                                        echo "<option value='NETWORK_REPLACE'>NETWORK_REPLACE</option>";
                                                        echo "<option value='MOBILE_NO'>MOBILE_NO</option>";
                                                        echo "<option value='MESSAGE'>MESSAGE</option>";
                                                        echo "<option value='AMOUNT'>AMOUNT</option></select></td>";

                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<input type='text' id='id_para_name_".$j."' name='para_name_".$j."' class='form-control'></td>";

                                                        echo "<td style='font-size:12px;padding:7px 8px;'>";
                                                        echo "<input type='text' id='id_para_value_".$j."' name='para_value_".$j."' class='form-control'></td></tr>";

                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue next-step">CONTINUE</button>
                                </div>
                            </div>
                        </li>
                        <li class="step">
                            <div class="step-title waves-effect waves-dark">Network Entry</div>
                            <div class="step-content">
                                
                                <div class="row">
                                    <div class ="col s12 m12 l12 xl12" style ="height:250px;overflow-y:scroll;">
                                        <table class="bordered striped responsive-table">
                                            <thead>
                                                <tr>
                                                    <th style='font-size:12px;padding:7px 8px;'>Select</th>
                                                    <th style='font-size:12px;padding:7px 8px;display:none;'>Network ID</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Network Name</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Network Code</th>
                                                    <th style='font-size:12px;padding:7px 8px;'>Percentage</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbl_body">
                                                <?php 
                                                    $j = 1;
                                                    foreach($network as $f)
                                                    {
                                                        $sel = "off";
                                                        $shr = "";
                                                        $per = "";
                                                        foreach($api3 as $s)
                                                        {
                                                            if($f->net_code == $s->net_code)
                                                            {
                                                                $sel = "on";
                                                                $shr = $s->api_net_short_code;
                                                                $per = $s->api_net_per;
                                                            }

                                                        }

                                                        echo "<tr><td style='font-size:12px;padding:2px 8px;'>";

                                                        if($sel == "on")
                                                        {
                                                            echo "<label class='switch switch-label switch-pill switch-primary'>
                                                            <input class='switch-input' type='checkbox' checked id='id_net_select_".$j."' name='net_select_".$j."' style='display:none;'>
                                                            <span class='switch-slider' data-checked='On' data-unchecked='Off'></span></label></td>";
                                                        }
                                                        else
                                                        {
                                                            echo "<label class='switch switch-label switch-pill switch-primary'>
                                                            <input class='switch-input' type='checkbox'  id='id_net_select_".$j."' name='net_select_".$j."' style='display:none;'>
                                                            <span class='switch-slider' data-checked='On' data-unchecked='Off'></span></label></td>";
                                                        }
                                                        
                                                        
                                                        echo "<td style='font-size:12px;padding:2px 8px;display:none'>";
                                                        echo "<input type='hidden' id='id_net_code_".$j."' name='net_code_".$j."' value='".$f->net_code."' /></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_name_".$j."' name='net_name_".$j."' class='form-control' value='".$f->net_name."' /></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_short_".$j."' name='net_short_".$j."' value='".$shr."' class='form-control'/></td>";

                                                        echo "<td style='font-size:12px;padding:2px 8px;'>";
                                                        echo "<input type='text' id='id_net_per_".$j."' name='net_per_".$j."' value='".$per."' class='form-control' /></td></tr>";

                                                        $j++;
                                                    }
                                                ?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                                <div class="step-actions">
                                    <button class="waves-effect waves-dark btn blue" type="submit" id = "btn_submit">SUBMIT</button>
                                    <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
                                </div>
                            </div>
                        </li>
                        
                    </ul>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script src="{{ asset('js/mstepper.min.js') }}"></script>

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            session()->forget('msg');
        }
    ?>

    <script>
     $(document).ready(function() 
	   {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            var stepper = document.querySelector('.stepper');
            var stepperInstace = new MStepper(stepper, {
                // options
                firstActive: 0 // this is the default
            })
            
            
            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                var o = checkParameter();
                var c = $('#id_pr_url').val();
                if(o == 0)
                {
                    if(o == 0)
                    {
                        swal({
                            title: 'Are you sure?',
                            text: "Confirmation Alert",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Submit!'
                            }).then((result) => {
                            if (result.value) {
                                
                                $('#id_api_form').attr('action', "{{url('apirequest_update')}}");
                                $('#id_api_form').submit();
                            }
                            else
                            {
                                swal("Cancelled", "Sorry...", "error");
                                
                            }
                        });
                    }
                    else
                    {
                        swal("Cancelled", "Invalid URL...", "error");
                    }
                }
                else
                {
                    swal("Cancelled", "Duplicate Parameter Type...", "error");
                }
               
                
                
            }); 

            function checkParameter()
            {
                var i = 1;
                var o = 0;
                var p = 0;
                var q = 0;
                var r = 0;
                var s = 0;
                var a = "";
                
                for(i = 1; i <= 18; i++)
                {
                    a = $('#id_para_type_' + i).val();
                   
                    if(a == "TRANSACTION_ID")
                        p++;
                    if(a == "NETWORK")
                        q++;
                    if(a == "MOBILE_NO")
                        r++;
                    if(a == "AMOUNT")
                        s++;
                }

                if(p > 1 || q > 1 || r > 1 || s > 1)
                    o = 1;
                
                return o;
            }

            function isUrlValid(url) {
                    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
            }
      });
    </script>
    </body>
</html>
