<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>

        <!-- loader-->
        <div id="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999;background: 50% 50% no-repeat rgba(249, 249, 249, 0.753); display: none;">
            <div class="preloader-wrapper big active" style="position: absolute; top: 50%; left: 50%;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
               

            <div class="card " style = "margin-top:57px">
                <div class="card-image">
                <span class="card-title" style = "padding:12px;">Apipartner User Agent Entry</span>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                        <form id="rech_form" class="form-horizontal" action="{{url('apiagent_store')}}" method="POST" accept-charset="UTF-8">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  
                                <div class="row" style = "margin-bottom:20px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                          <select id="id_user_name"  name = "user_name" >
                                              <option value = '-'>----Select----</option>
                                            <?php
                                                $str = "";
                                                foreach($apipartner as $f)
                                                {
                                                    $str = $str."<option value='".$f->user_name."'>".$f->user_name."-".$f->user_code."-".$f->user_per_name."</option>";
                                                }
                                                echo $str;
                                            ?>
                                            </select>
                                          <label>User Name</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                <div class="row" style = "margin-bottom:8px;">
                                    <div class="input-field col s12 m12 l4 xl4" style="margin: 3px 0px;">
                                        <select id="id_agent_type" name="agent_type" >
                                                <option value="-">---Select---</option>
                                                <option value="APPLICATION">APPLICATION</option>
                                                <option value="BROWSER">BROWSER</option>
                                            
                                        </select>
                                        <label >User Agent</label>
                                    </div>
                                    <div class="input-field col s2 m2 l1 xl1" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                    <div class="input-field col s8 m8 l4 xl4" style="margin: 3px 0px;">
                                        <p>&nbsp;</p>
                                    </div>
                                </div>
                                
                                
                                

                               
                                                    
                                <div class="row">
                                    <div class="col s12 m12 l2 xl2">
                                            <button class="btn waves-effect waves-light  " type="submit"  id="btn_submit" name="btn_submit">Submit
                                                    <i class="material-icons right">send</i>
                                            </button>
                                    </div>

                                    <div class="col s12 m12 l2 xl2">
                                        <button class="btn waves-effect waves-light #ff6f00 amber darken-4 " type="reset"  >Reset
                                                    <i class="material-icons right">cloud</i>
                                            </button>
                                    </div>
                                    <div class="col s12 m12 l2 xl2">
                                            
                                    </div>
                                        
                                </div>
                            </form>
                    </div>
                </div>
                <!-- End Body --> 

                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                          <table class="bordered striped responsive-table">
                              <thead>
                                <tr>
                                        <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                      <th style='font-size:12px;padding:7px 8px;'>TR ID</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER NAME</th>
                                      <th style='font-size:12px;padding:7px 8px;'>USER AGENT</th>
                                </tr>
                              </thead>
                              <tbody id="tbl_body">
                              <?php 
                                        
                                        $j = 1;
                                        foreach($data as $f)
                                        {

                                            if($f->agent_type == "BROWSER")
                                            {
                                                echo "<tr style = 'background-color:#EAEDED;'>";
                                            }
                                            else
                                            {
                                                echo "<tr>";
                                            }
                                          echo "<td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->user_name."</td>";
                                          echo "<td  style='font-size:12px;padding:7px 8px;'>".$f->agent_type."</td>";
                                          echo "</tr>";

                                          
                                          $j++;
                                        }
                                        
                                    ?>
                                
                              </tbody>
                          </table>
                    </div>
                </div>
                <!-- End Report  -->
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('result'))
        {
            $op = session('result');
            
            echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', '".$op."', 'success'); 
                });
                </script>";
            
            session()->forget('result');
        }
    ?>

    <script>
     $(document).ready(function() 
	 {
           
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $("#id_user_amount").keydown(function (e) 
            {
                numbersOnly(e);
            });

            $('#btn_submit').on('click',function(e)
            {
                e.preventDefault();
                
                $('#btn_submit').prop('disabled', true);
                var tx = $('option:selected', '#id_user_name').val();
                var ty = $('option:selected', '#id_agent_type').val();
                
                if(tx != "-" && ty != "-")
                {

                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Store!'
                        }).then((result) => {
                        if (result.value) {
                            $('#loader').show();
                            $('#rech_form').attr('action', "{{url('apiagent_store')}}");
							$('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            $('#btn_submit').prop('disabled', false);
                        }
                    });

                }
                else
                {
                    swal("Cancelled", "Please select user name or Agent type", "error");
                    $('#btn_submit').prop('disabled', false);
                }
                
                
            }); 

             function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

      });
    </script>
    </body>
</html>
