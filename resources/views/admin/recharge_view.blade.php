<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Recharge Details</span>
                    <a class="btn-floating halfway-fab waves-effect waves-light  purple center-align" href="{{ url('rechargedetails')}}" ><i class="Small material-icons">arrow_back</i></a>
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <div class="row" >
                    
                    <div class="col s12 m12 l4 xl4 " >
                        <div class = "card-content white darken-1" >
                            <div class="col s9 m9 l9 xl9 left-align">
                                <label class="title-con" style="font-size:14px;">Recharge Success Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Recharge Success Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">Recharge Failure Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Recharge Failure Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">Recharge Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">Recharge Credit Total</label><br>
                            </div>
                            <div class="col s2 m2 l2 xl2 right-align" >
                                <label class="title-con" style="font-size:14px;" id = "re_1">&#x20B9;{{ $total['wsua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" id = "re_2">&#x20B9;{{ $total['wsut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" id = "re_3">&#x20B9;{{ $total['wfua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_1">&#x20B9;{{ $total['wfut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_2">&#x20B9;{{ $total['wrea_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" id = "tr_3">&#x20B9;{{ $total['wret_tot'] }}</label><br>
                            </div>
                        </div>
                    </div>

                     <div class="col s12 m12 l4 xl4 " >
                        <div class = "card-content white darken-1" >
                            <div class="col s9 m9 l9 xl9 left-align">
                                <label class="title-con" style="font-size:14px;">TNEB Success Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">TNEB Success Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">TNEB Failure Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">TNEB Failure Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">TNEB Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">TNEB Credit Total</label><br>
                            </div>
                            <div class="col s2 m2 l2 xl2 right-align" >
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['esut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['efut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['erea_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['eret_tot'] }}</label><br>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m12 l4 xl4 " >
                        <div class = "card-content white darken-1" >
                            <div class="col s9 m9 l9 xl9 left-align">
                                <label class="title-con" style="font-size:14px;">MONEY Success Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">MONEY Success Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">MONEY Failure Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">MONEY Failure Credit Total</label><br>
                                <label class="title-con" style="font-size:14px;">MONEY Amount Total</label><br>
                                <label class="title-con" style="font-size:14px;">MONEY Credit Total</label><br>
                            </div>
                            <div class="col s2 m2 l2 xl2 right-align" >
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['msua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;">&#x20B9;{{ $total['msut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfua_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mfut_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mrea_tot'] }}</label><br>
                                <label class="title-con" style="font-size:14px;" >&#x20B9;{{ $total['mret_tot'] }}</label><br>
                            </div>
                        </div>
                    </div>

                </div>
                
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                <th style="font-size:12px;padding:7px 8px;">NO</th>
                                <th style="font-size:12px;padding:7px 8px;">USER NAME</th>
                                <th style="font-size:12px;padding:7px 8px;">MOBILE</th>
                                <th style="font-size:12px;padding:7px 8px;">NETWORK</th>
                                <th style="font-size:12px;padding:7px 8px;">AMOUNT</th>
                                <th style="font-size:12px;padding:7px 8px;">NET(%)/SURP</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.AMT</th>
                                <th style="font-size:12px;padding:7px 8px;">TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">OPR. TRN ID</th>
                                <th style="font-size:12px;padding:7px 8px;">R.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">UP.DATE</th>
                                <th style="font-size:12px;padding:7px 8px;">STATUS</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">O.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">C.BAL</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">PROVIDER</th>
                                <th style="font-size:12px;padding:7px 8px;text-align:right;">MODE</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                                <?php 
                                       
                                        
                                        $str = "";
                                        
                                        $j = 1;
                                        foreach($recharge as $d)
                                        {
                                           
                                            if($d->trans_type == "WEB_RECHARGE" || $d->trans_type == "API_RECHARGE")
                                            {
                                                if(sizeof($d->newrecharge1) > 0)
                                                {
                                                    $net_name = "";
                                                    foreach($d2 as $r)
                                                    {
                                                        if($d->newrecharge1[0]->net_code == $r->net_code)
                                                            $net_name = $r->net_name;
                                                    }
                                
                                                    $api_name = "";
                                                    $reply_id = "NA";
                                                    $reply_date = "";
                                                    foreach($d3 as $r)
                                                    {
                                                        if($d->newrecharge2->api_code == $r->api_code)
                                                            $api_name = $r->api_name;
                                                    }
                            
                                                    $reply_id = $d->newrecharge2->reply_opr_id;
                                                    $reply_date = $d->newrecharge2->reply_date;
                                                   
                                
                                                    $rech_status = "";
                                                    $status = "";
                                                    $o_bal = 0;
                                                    $u_bal = 0;
                                                    $r_tot = 0;
                                
                                                    if($d->trans_option == 1)
                                                    {
                                                        $rech_status = $d->newrecharge1[0]->rech_status;
                                                        $rech_option = $d->newrecharge1[0]->rech_option;
                                                        $u_bal = $d->newrecharge1[0]->user_balance;
                                                        $r_tot = $d->newrecharge1[0]->rech_total;
                                                        $str = $str."<tr>";
                                                    }
                                                    else if($d->trans_option == 2)
                                                    {
                                                        $rech_status = $d->newrecharge1[1]->rech_status;
                                                        $rech_option = $d->newrecharge1[1]->rech_option;
                                                        $u_bal = $d->newrecharge1[1]->user_balance;
                                                        $r_tot = $d->newrecharge1[1]->rech_total;
                                                        $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                    }
                                                    if($rech_status == "PENDING" && $rech_option == "0")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                        $o_bal = floatval($u_bal) + floatval($r_tot);
                                                    }
                                                    else if($rech_status == "PENDING" && $rech_option == "2")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                        $o_bal = floatval($u_bal) + floatval($r_tot);
                                                    }
                                                    else if($rech_status == "FAILURE" && $rech_option == "2")
                                                    {      
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                        $o_bal = floatval($u_bal) - floatval($r_tot);
                                                    }
                                                    else if ($rech_status == "SUCCESS")
                                                    {
                                                        $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                        $o_bal = floatval($u_bal) + floatval($r_tot);
                                                    }
                                
                                                    $mode = "WEB";
                                                    if($d->trans_id != "")
                                                    {
                                                        preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                                
                                                        $r_l = $matches[0][0];
                                
                                                        $r_l = substr($r_l, -1);
                                
                                                        if($r_l == "R")
                                                            $mode = "WEB";
                                                        else if($r_l == "A")
                                                            $mode = "API";
                                                        else if($r_l == "G")
                                                            $mode = "GPRS";
                                                        else if($r_l == "S")
                                                            $mode = "SMS";
                                
                                                    }
                                
                                                    $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->user_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_mobile."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_amount."</td>";
                                                    if($d->trans_option == 1)
                                                    {
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_net_per;
                                                        $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->newrecharge1[0]->rech_net_per_amt."";
                                                        $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->newrecharge1[0]->rech_net_surp."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[0]->rech_total."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->newrecharge1[0]->rech_date."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$reply_date."</td>";
                                                    }
                                                    else if($d->trans_option == 2)
                                                    {
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->newrecharge1[1]->rech_total."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                        $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                    }
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                                }
                                                
                                               
                                            }
                                            if($d->trans_type == "BILL_PAYMENT")
                                            {
                                                $net_name = "";
                                                foreach($d2 as $r)
                                                {
                                                    if($d->billpayment[0]->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }
                                
                                                $api_name = "";
                                                foreach($d3 as $r)
                                                {
                                                    if($d->billpayment[0]->api_code == $r->api_code)
                                                        $api_name = $r->api_name;
                                                }
                                
                                                $rech_status = "";
                                                $status = "";
                                                $o_bal = 0;
                                                $u_bal = 0;
                                                $r_tot = 0;
                                
                                               
                                
                                                if($d->trans_option == 1)
                                                {
                                                    $str = $str."<tr>";
                                                    $rech_status = $d->billpayment[0]->con_status;
                                                    $rech_option = $d->billpayment[0]->con_option;          
                                                    $r_tot = $d->billpayment[0]->con_total;
                                                    $u_bal = $d->billpayment[0]->user_balance;
                                                   
                                                }
                                                else if($d->trans_option == 2)
                                                {  
                                                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                    $rech_status = $d->billpayment[1]->con_status;
                                                    $rech_option = $d->billpayment[1]->con_option;          
                                                    $r_tot = $d->billpayment[1]->con_total;
                                                    $u_bal = $d->billpayment[1]->user_balance;
                                                    
                                                }
                                                
                                                if($rech_status == "PENDING" && $rech_option == 1)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($rech_status == "PENDING" && $rech_option == 2)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($rech_status == "FAILURE"  && $rech_option == 2)
                                                {      
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light red'><i class='small material-icons '>clear</i></button>";
                                                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if ($rech_status == "SUCCESS")
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light green'><i class='small material-icons '>check</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                
                                                $mode = "WEB";
                                                if($d->trans_id != "")
                                                {
                                                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                                
                                                    $r_l = $matches[0][0];
                                
                                                    $r_l = substr($r_l, -1);
                                
                                                    if($r_l == "R")
                                                        $mode = "WEB";
                                                    else if($r_l == "A")
                                                        $mode = "API";
                                                    else if($r_l == "G")
                                                        $mode = "GPRS";
                                                    else if($r_l == "S")
                                                        $mode = "SMS";
                                
                                                }
                                                
                                
                                                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->user_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_acc_no."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_amount."</td>";
                                                if($d->trans_option == 1)
                                                {
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->con_net_per;
                                                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->billpayment[0]->con_net_per_amt."";
                                                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->billpayment[0]->con_net_surp."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_opr_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->created_at."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[0]->reply_date."</td>";
                                                }
                                                else if($d->trans_option == 2)
                                                {
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->billpayment[0]->con_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->billpayment[1]->reply_opr_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                }
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                            } 
                                            if($d->trans_type == "BANK_TRANSFER")
                                            {
                                                $net_name = "";
                                                foreach($d2 as $r)
                                                {
                                                    if($d->moneytransfer[0]->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }
                                
                                                $api_name = "";
                                                foreach($d3 as $r)
                                                {
                                                    if($d->moneytransfer[0]->api_code == $r->api_code)
                                                        $api_name = $r->api_name;
                                                }
                                
                                                $rech_status = "";
                                                $status = "";
                                                $o_bal = 0;
                                                $u_bal = 0;
                                                $r_tot = 0;
                                
                                               
                                
                                                if($d->trans_option == 1)
                                                {
                                                    $str = $str."<tr>";
                                                    $rech_status = $d->moneytransfer[0]->mt_status;
                                                    $rech_option = $d->moneytransfer[0]->mt_option;          
                                                    $r_tot = $d->moneytransfer[0]->mt_total;
                                                    $u_bal = $d->moneytransfer[0]->user_balance;
                                                   
                                                }
                                                else if($d->trans_option == 2)
                                                {  
                                                    $str = $str."<tr style='background-color:#E8DAEF;'>";
                                                    $rech_status = $d->moneytransfer[1]->mt_status;
                                                    $rech_option = $d->moneytransfer[1]->mt_option;          
                                                    $r_tot = $d->moneytransfer[1]->mt_total;
                                                    $u_bal = $d->moneytransfer[1]->user_balance;
                                                    
                                                }
                                                if($rech_status == "PENDING" && $rech_option == 1)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light blue'><i class='small material-icons '>directions_run</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($rech_status == "PENDING" && $rech_option == 2)
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if($rech_status == "FAILURE"  && $rech_option == 2)
                                                {      
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light #ff6e40 deep-orange accent-2'><i class='small material-icons '>clear</i></button>";
                                                    //$o_bal = floatval($u_bal) - floatval($r_tot);
                                                    $o_bal = floatval($u_bal) - floatval($r_tot) ;
                                                    //$u_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                else if ($rech_status == "SUCCESS")
                                                {
                                                    $status = "<button class='btn-floating btn-small waves-effect waves-light '><i class='small material-icons '>check</i></button>";
                                                    $o_bal = floatval($u_bal) + floatval($r_tot);
                                                }
                                                
                                                $mode = "WEB";
                                                if($d->trans_id != "")
                                                {
                                                    preg_match_all('/([0-9]+|[a-zA-Z]+)/',$d->trans_id, $matches);
                                
                                                    $r_l = $matches[0][0];
                                
                                                    $r_l = substr($r_l, -1);
                                
                                                    if($r_l == "R")
                                                        $mode = "WEB";
                                                    else if($r_l == "A")
                                                        $mode = "API";
                                                    else if($r_l == "G")
                                                        $mode = "GPRS";
                                                    else if($r_l == "S")
                                                        $mode = "SMS";
                                
                                                }
                                                
                                
                                                $str = $str."<td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->user_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->bk_acc_no."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_amount."</td>";
                                                if($d->trans_option == 1)
                                                {
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_per;
                                                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i> ".$d->moneytransfer[0]->mt_per_amt."";
                                                    $str = $str."<i class='icon-control-play ' style='color:blue;'></i>".$d->moneytransfer[0]->mt_surp."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_reply_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->mt_date."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[0]->reply_date."</td>";
                                                }
                                                else if($d->trans_option == 2)
                                                {
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>&nbsp;</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".$d->moneytransfer[0]->mt_total."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->trans_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$d->moneytransfer[1]->mt_reply_id."</td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                    $str = $str."<td  style='font-size:11px;padding:7px 8px;'></td>";
                                                }
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$status."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($o_bal,2, ".", "")."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;text-align:right;'>".number_format($u_bal,2, ".", "")."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$api_name."</td>";
                                                $str = $str."<td  style='font-size:11px;padding:7px 8px;'>".$mode."</td></tr>";
                                            }                                        
                                           
                                           
                                           
                                           //$str = $str."</tr>"; 
                                
                                            
                                            
                                           
                                                                                    
                                            $j++;
                                        }
                                        
                                        echo $str; 
                                       

                                      
                                    ?>

                            </tbody>
                        </table>
                       
                        {{ $recharge->links('vendor.pagination.materializecss') }}
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <script>
     $(document).ready(function() 
	 {
           
            
            $(".dropdown-trigger").dropdown();
            $('select').formSelect();
            $('.sidenav').sidenav();
            $('.fixed-action-btn').floatingActionButton();

            $('.datepicker').datepicker({
              selectMonths: true,
              selectYears: 200, 
              format: 'yyyy-mm-dd'
            });

           $("#id_rech_amount, #id_rech_mobile").keydown(function (e) 
            {
                numbersOnly(e);
            });
            
            $('#print_view').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargedetails_view')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

            $('#print_excel').on('click',function(e)
            {
                e.preventDefault();
                
                swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, View!'
                        }).then((result) => {
                        if (result.value) {
                            
                            $('#rech_form').attr('action', "{{url('rechargedetails_excel')}}");
							              $('#rech_form').submit();
                        }
                        else
                        {
                            swal("Cancelled", "No View...", "error");
                            
                        }
                    });
                
                
            }); 

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32, 188]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }
      });
    </script>
    </body>
</html>
