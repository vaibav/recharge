@extends('layouts.admin_new')
@section('styles')
    <style type="text/css">
        table th td {font-size:12px !important;padding:7px 8px !important;}
    </style>
@stop
@section('content')

<!-- Your code here-->
<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-4">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Payment Report</h2>
                <h5 class="text-white op-7 mb-3">Payment Details</h5>
            </div>
            <div class="ml-md-auto py-2 py-md-0">
               <a href="{{url('payment_report')}}" class="btn btn-secondary btn-round">Back</a>
            </div>
        </div>
    </div>
</div>

<div class="page-inner mt--5">
<div class="row mt--2">
    <div class="col-md-12">
       <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Remote Payment</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">{{ $total['rem_cnt'] }}</h3>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-info w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['rem_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                        
                    </div>
                </div>

               
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Fund Transfer</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">{{ $total['fun_cnt'] }}</h3>
                        </div>
                        <div class="progress progress-sm">
                            <div class="progress-bar bg-success w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['fun_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
               
            </div>

            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h5><b>Refund Payment</b></h5>
                            </div>
                            <h3 class="text-info fw-bold">{{ $total['ref_cnt'] }}</h3>
                        </div>
                        <div class="progress progress-sm">
                        <div class="progress-bar bg-danger w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="d-flex align-items-center">
                                    <span class="stamp stamp-md bg-success mr-3">
                                        <i class="fas fa-check"></i>
                                    </span>
                                    <div>
                                        <h5 class="mb-1"><b><a href="#">&#x20B9;{{ $total['ref_tot'] }} &nbsp;<small>Success</small></a></b></h5>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        <!-- End -->
        <br>

        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                <div class="table-responsive">
                    <table class="display table table-striped table-hover ">
                        <thead>
                        <tr>
                            <th >NO</th>
                            <th >TR.ID</th>
                            <th >FROM USER</th>
                            <th >TO USER</th>
                            <th >REQ.AMT</th>
                            <th >REQ.DATE</th>
                            <th >GRANT AMT</th>
                            <th >GRANT DATE</th>
                            <th >MODE</th>
                            <th >REMARKS</th>
                            <th >STATUS</th>
                              
                        </tr>
                        </thead>
                        <tbody id="tbl_body">
                            <?php 
                                
                                    
                                    $str = "";
                                    
                                    $j = 1;
                                    foreach($payment as $d)
                                    {
                                        $str = $str . "<tr>";
                                        $str = $str . "<td>".$j."</td>";
                                        $str = $str . "<td>".$d->trans_id."</td>";
                                        $str = $str . "<td>".$d->parent_name."</td>";
                                        $str = $str . "<td>".$d->user_name."</td>";
                                        $str = $str . "<td>".$d->rech_amount."</td>";
                                        $str = $str . "<td>".$d->rech_date."</td>";
                                        $str = $str . "<td>".$d->ret_total."</td>";
                                        $str = $str . "<td>".$d->reply_date."</td>";
                                        $str = $str . "<td>".$d->trans_type."</td>";
                                        $str = $str . "<td>".$d->reply_id."</td>";
                                        $str = $str . "<td>".$d->rech_status."</td>";
                                        $str = $str . "</tr>";

                                        $j++;

                                       
                                    }
                                    
                                    echo $str; 
                                

                                
                                ?>

                        </tbody>
                    </table>
                    {{ $payment->links('vendor.pagination.atlantis') }}
                </div>
                </div>
            </div>
                <div class="clearfix"></div>
            </div>
        </div>

    
    </div>
    
</div>
<!-- Code Ending-->
</div>


            
          

@section('scripts')


<script>
$(document).ready(function() {
    //alert("hellow");

    
   
  
   
});
</script>
@stop
@stop