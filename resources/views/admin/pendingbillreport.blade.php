<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">Pending EBBill Report</span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Trans Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>API Trans Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Network</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Consumer No</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Consumer Name</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Mobile</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Amount</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Req. Status</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Status</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Mode</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Operator ID</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        $j = 1;
                                      
                                        foreach($recharge_1 as $f)
                                        {
                                            
                                            $v = 0;
                                            foreach($recharge_2 as $r)
                                            {
                                                if($f->trans_id == $r->trans_id)
                                                {
                                                    $v = 1;
                                                    break;
                                                }
                                                    
                                            }

                                            if ($v == 0)
                                            {
                                                $net_name = "";
                                                foreach($network as $r)
                                                {
                                                    if($f->net_code == $r->net_code)
                                                        $net_name = $r->net_name;
                                                }

                                                

                                                echo "<tr><td style='font-size:11px;padding:7px 8px;'>".$j."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->trans_id."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->api_trans_id."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;' id='user_".$f->trans_id."'>".$f->user_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$net_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->con_acc_no."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->con_name."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->con_mobile."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->con_amount."</td>"; 
                                                echo "<td  style='font-size:11px;padding:7px 8px;'>".$f->created_at."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'><div style='width: 300px;word-break: break-word;'>".$f->con_req_status."</div></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;'><span class='badge badge-primary' style='padding:4px 4px;font-size:11px;'>PENDING</span></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;' id='mode_".$f->trans_id."'>".$f->con_mode."</td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'><input type='text' id='opr_".$f->trans_id."' class='form-control' /></td>";
                                                echo "<td  style='font-size:11px;padding:7px 8px;text-align:right;'>";
                                                echo "<button class='btn-floating btn-sm waves-effect' id='success_".$f->trans_id."'>
                                                <i class='small material-icons'>edit</i></button></button>&nbsp;&nbsp;&nbsp;";
                                                echo "<button class='btn-floating btn-sm waves-effect red' id='failure_".$f->trans_id."'>
                                                <i class='small material-icons'>close</i></button></td>";
                                                echo "</tr>";
                                                                                    
                                                $j++;
                                            }
                                            
                                        }

                                        
                                    ?>

                            </tbody>
                        </table>
                      
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

     <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op > 0)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Recharge Bill Process has been completed...', 'success'); 
                });
                </script>";
            }
            else 
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Not Completed...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>

    <script>
    $(document).ready(function() 
    {
        
        
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("success_");
                if(nid.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Success!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid[1];
                            var opr = $('#opr_' + code).val();
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/pendingbill_success/" + code + "/" + opr;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                    
                }

                var nid1=gid.split("failure_");
                var net=0;
                var ctx=0;
                if(nid1.length>1)
                {
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Failed!'
                        }).then((result) => {
                        if (result.value) {
                            var code = nid1[1];
                            var user = $('#user_' + code).text();
                            var opr = $('#opr_' + code).val();
                            //alert(code +"----"+ opr);
                            window.location.href = "<?php echo url('/'); ?>/pendingbill_failure/" + code + "/" + opr ;
                        }
                        else
                        {
                            swal("Cancelled", "Sorry...", "error");
                        }
                    });
                    
                }
            });

        
        

        
    });
    </script>
    </body>
</html>
