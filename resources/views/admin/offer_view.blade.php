<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('admin.top1')
        
    </head>
    <body style = "background-color: #34495e;">
        @include('admin.sidebar1', array('bal' => $user->ubal, 'uname' => $user->user))

        <div class="fixed-action-btn">
            <a class="btn-floating btn-large red" href = "{{url('dashboard')}}">
                <i class="large material-icons">home</i>
            </a>
            
        </div>
        <!-- Page Layout here -->
        <div class="row">
            <div class="col s12 m12 l12 xl12">
                

            <div class="card " style = "margin-top:58px;">
                <div class="card-image">
                    <span class="card-title" style = "padding:12px;">News Details</span>
                   
                </div>

                <div class="card-content white darken-1" style = "border-radius:4px;">
                
               
                <!-- Page Body --> 
                <div class = "row">
                    <div class ="col s12 m12 l12 xl12">
                       <!-- Form Starts-->
                       <table class="bordered striped responsive-table ">
                            <thead>
                            <tr>
                                    <th style='font-size:12px;padding:7px 8px;'>NO</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Tr Id</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Offer Details</th>
                                    <th style='font-size:12px;padding:7px 8px;'>User Type</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Date</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Offer Status</th>
                                    <th style='font-size:12px;padding:7px 8px;'>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbl_body">
                            <?php 
                                        
                                        $j = 1;
                                        foreach($off1 as $f)
                                        {
                                            echo "<tr><td style='font-size:12px;padding:7px 8px;'>".$j."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->trans_id."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->offer_details."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->user_type."</td>";
                                            echo "<td style='font-size:12px;padding:7px 8px;'>".$f->created_at."</td>";
                                            if($f->offer_status == 1)
                                                echo "<td style='font-size:12px;padding:7px 8px;'>LIVE</td>";
                                            else 
                                                echo "<td style='font-size:12px;padding:7px 8px;'>IN-ACTIVE</td>";
                                            
                                            echo "<td  style='font-size:11px;padding:7px 8px;text-align:left;'>";
                                            echo "<button class='btn-floating btn-sm waves-effect amber' id='inactive_".$f->trans_id."'>
                                            <i class='small material-icons'>edit</i></button>&nbsp;&nbsp;&nbsp;";
                                            echo "<button class='btn-floating btn-sm waves-effect red' id='delete_".$f->trans_id."'>
                                            <i class='small material-icons'>close</i></button></td>";
                                           
                                         
                                            echo "</tr>";
                                             $j++;
                                        }
                                        
                                    ?>

                            </tbody>
                        </table>
                      
                                
                              

                        <!-- End Form-->
                    </div>
                </div>
                <!-- End Body --> 
                
            </div>
            </div>
        </div>
        <!-- End Page Layout  -->

    @include('admin.bottom1')

    <?php
        if(session()->has('msg'))
        {
            $op = session('msg');
            if($op == 1)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Offer Status is Updated Successfully...', 'success'); 
                });
                </script>";
            }
            else if($op == 2)
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Alert!', 'Offer is deleted Successfully...', 'success'); 
                });
                </script>";
            }
            else
            {
                echo "<script>
                $(document).ready(function() 
                {
                    swal('Error!', 'Sorry...', 'error'); 
                });
                </script>";
            }
            session()->forget('msg');
        }
    ?>

    <script>
    $(document).ready(function() 
    {
        
        
        $(".dropdown-trigger").dropdown();
        $('select').formSelect();
        $('.sidenav').sidenav();
        $('.fixed-action-btn').floatingActionButton();

        $('#tbl_body').delegate('button', 'click', function(e) {
                e.preventDefault();
                var gid=this.id;
                var nid=gid.split("inactive_");
                if(nid.length>1)
                {
                    var code = nid[1];
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Active/Inactive It!'
                        }).then((result) => {
                        if (result.value) {
                            
                            //alert(code + "---" + amt);
                            window.location.href = "<?php echo url('/'); ?>/offer_inactive/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "No Inactive...", "error");
                            
                        }
                    });
                }

                var nid1=gid.split("delete_");
                if(nid1.length>1)
                {
                    var code = nid1[1];
                    swal({
                        title: 'Are you sure?',
                        text: "Confirmation Alert",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Delete It!'
                        }).then((result) => {
                        if (result.value) {
                            
                            //alert(code + "---" + amt);
                            window.location.href = "<?php echo url('/'); ?>/offer_delete/" + code;
                        }
                        else
                        {
                            swal("Cancelled", "No Inactive...", "error");
                            
                        }
                    });
                    
                }
                
            });

            function numbersOnly(e)
            {
              // Allow: backspace, delete, tab, escape, enter and .
              if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
              // Allow: Ctrl+A, Command+A
              (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
              // Allow: home, end, left, right, down, up
              (e.keyCode >= 35 && e.keyCode <= 40)) 
              {
                // let it happen, don't do anything
                return;
              }
              // Ensure that it is a number and stop the keypress
              if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
              }
            }

            
    });
    </script>
    </body>
</html>
