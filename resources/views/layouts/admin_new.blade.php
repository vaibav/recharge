<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	 @include('includes.common_meta')

	<!-- Fonts and icons -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset("css/fonts.min.css") }}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.0/css/all.min.css" rel="stylesheet">
	<link href="{{ asset('css/atlantis1.css') }}" rel="stylesheet" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
	@yield('styles')
</head>
<body>
	<div class="wrapper sidebar_minimize">
		<div class="main-header" >
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">
				
				<a href="index.html" class="logo">
					<img src="{{ asset('img/brand/logo.png') }}" alt="navbar brand" class="navbar-brand" style = "height:40px;width:130px;">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<?php 
				$stx = "";
				foreach($user->offer as $r)
				{
					$stx = $stx . $r->offer_details. " ";
				}

				$sty = "";
				foreach($user->inactive as $r)
				{
					$sty = $sty . $r->net_name. " ";
				}
            
        	?>
			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2" style = "max-height:60px !important;">
				
				<div class="container-fluid">
					<div class="collapse" id="search-nav" style = "max-width:600px !important;">
						<form class="navbar-left navbar-form nav-search mr-md-3">
							<div class = "row">
								<div class ="col-md-6 text-left">
									<p style = "margin-bottom:0px;color:white;">
										<marquee>{{$stx}}</marquee>
									</p>
								</div>
								<div class ="col-md-6 text-left">
									<p style = "margin-bottom:0px;color:white;">Inactive Networks: 
										<marquee>{{$sty}}</marquee>
									</p>
								</div>
							</div>
							
						</form>
					</div>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-envelope"></i>
							</a>
							<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
								<li>
									<div class="dropdown-title d-flex justify-content-between align-items-center">
										Messages 									
										<a href="#" class="small">Mark all as read</a>
									</div>
								</li>
								<li>
									<div class="message-notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-img"> 
													<img src="{{ asset('img/new/jm_denis.jpg') }}" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jimmy Denis</span>
													<span class="block">
														How are you ?
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="{{ asset('img/new/chadengle.jpg') }}" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Chad</span>
													<span class="block">
														Ok, Thanks !
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="{{ asset('img/new/mlane.jpg') }}" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jhon Doe</span>
													<span class="block">
														Ready for the meeting today...
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="{{ asset('img/new/talha.jpg') }}" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Talha</span>
													<span class="block">
														Hi, Apa Kabar ?
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all messages<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
												<div class="notif-content">
													<span class="block">
														New user registered
													</span>
													<span class="time">5 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
												<div class="notif-content">
													<span class="block">
														Rahmad commented on Admin
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-img"> 
													<img src="../assets/img/profile2.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="block">
														Reza send messages to you
													</span>
													<span class="time">12 minutes ago</span> 
												</div>
											</a>
											<a href="#">
												<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
												<div class="notif-content">
													<span class="block">
														Farrah liked Admin
													</span>
													<span class="time">17 minutes ago</span> 
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
								<i class="fas fa-layer-group"></i>
							</a>
							<div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
								<div class="quick-actions-header">
									<span class="title mb-1">Quick Actions</span>
									<span class="subtitle op-8">Shortcuts</span>
								</div>
								<div class="quick-actions-scroll scrollbar-outer">
									<div class="quick-actions-items">
										<div class="row m-0">
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-file-1"></i>
													<span class="text">Generated Report</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-database"></i>
													<span class="text">Create New Database</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-pen"></i>
													<span class="text">Create New Post</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-interface-1"></i>
													<span class="text">Create New Task</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-list"></i>
													<span class="text">Completed Tasks</span>
												</div>
											</a>
											<a class="col-6 col-md-4 p-0" href="#">
												<div class="quick-actions-item">
													<i class="flaticon-file"></i>
													<span class="text">Create New Invoice</span>
												</div>
											</a>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{ asset('img/brand/logo.png') }}" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="{{ asset('img/brand/logo.png') }}" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4>Admin</h4>
												<p class="text-muted">Bal:{{$user->ubal}}</p><a href="#" class="btn btn-xs btn-secondary btn-sm">View Profile</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">My Profile</a>
										<a class="dropdown-item" href="#">My Balance</a>
										<a class="dropdown-item" href="#">Inbox</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Account Setting</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="{{url('logout')}}">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{ asset('img/brand/logo.png') }}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a >
								<span>
									Lakshmi Mobiles
									<span class="user-level">Bal:{{$user->ubal}}</span>
								</span>
							</a>
							<div class="clearfix"></div>

							
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-item active">
							<a href="{{url('dashboard')}}">
								<i class="fas fa-home"></i>
								<p>Dashboard</p>
								<span class="caret"></span>
							</a>
						</li>
                        <li class="nav-item">
							<a data-toggle="collapse" href="#base1">
								<i class="fas fa-server"></i>
								<p>Server</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="base1">
								<ul class="nav nav-collapse">
                                    <li><a href="{{url('serveronoff')}}" style = "padding:3px 5px;"><span class="sub-item">OnOff Entry</span></a></li>
                                    <li><a href="{{url('ad_view')}}" style = "padding:3px 5px;"><span class="sub-item">Advertisement</span></a></li>
									<li><a href="{{url('n_log')}}" style = "padding:3px 5px;"><span class="sub-item">Log</span></a></li>
									<li><a href="{{url('n_backup_retailer')}}" style = "padding:3px 5px;"><span class="sub-item">Retailer Backup</span></a></li> 
									<li><a href="{{url('n_backup_update_retailer')}}" style = "padding:3px 5px;"><span class="sub-item">Retailer Backup Bal</span></a></li>
									<li><a href="{{url('n_backup_distributor')}}" style = "padding:3px 5px;"><span class="sub-item">Distributor Backup</span></a></li> 
									<li><a href="{{url('n_backup_update_distributor')}}" style = "padding:3px 5px;"><span class="sub-item">Distributor Backup Bal</span></a></li>
									<li><a href="{{url('n_check_account_1')}}" style = "padding:3px 5px;"><span class="sub-item">Check Retailer</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#base">
								<i class="fas fa-layer-group"></i>
								<p>API Master</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="base">
								<ul class="nav nav-collapse">
                                    <li><a href="{{url('network_type')}}" style = "padding:3px 5px;"><span class="sub-item">Network Type </span></a></li>
                                    <li><a href="{{url('network')}}" style = "padding:3px 5px;"><span class="sub-item">Network </span></a></li>
                                    <li><a href="{{url('network_surplus')}}" style = "padding:3px 5px;"><span class="sub-item">Network Surplus </span></a></li>
                                    <li><a href="{{url('network_line')}}" style = "padding:3px 5px;"><span class="sub-item">Network Line </span></a></li>
                                    <li><a href="{{url('network_line_apipartner')}}" style = "padding:3px 5px;"><span class="sub-item">API Partner Line</span></a></li>
                                    <li><a href="{{url('offer_line')}}" style = "padding:3px 5px;"><span class="sub-item">Other Line</span></a></li>
                                    <li><a href="{{url('n_netpack')}}" style = "padding:3px 5px;"><span class="sub-item">Package</span></a></li>
                                    <hr>
                                    <li><a href="{{url('n_apirequest')}}" style = "padding:3px 5px;"><span class="sub-item">New API Request</span></a></li>
                                    <li><a href="{{url('apirequest_edit')}}" style = "padding:3px 5px;"><span class="sub-item">New API Edit</span></a></li>
									<li><a href="{{url('n_apirequest_network')}}"style = "padding:3px 5px;" ><span class="sub-item">New API Network</span></a></li>
									<li><a href="{{url('n_apiresult')}}"style = "padding:3px 5px;" ><span class="sub-item">New API Result</span></a></li>
                                    <li><a href="{{url('api_details_1')}}" style = "padding:3px 5px;" ><span class="sub-item">API URL Details</span></a></li>
								</ul>
							</div>
						</li>
						
						<li class="nav-item">
							<a data-toggle="collapse" href="#forms">
								<i class="fas fa-pen-square"></i>
								<p>User</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="forms">
								<ul class="nav nav-collapse">
								    <li><a href="{{url('user')}}"><span class="sub-item">User Creation Entry</span></a></li>
                                    <li><a href="{{url('user_view')}}"><span class="sub-item">User Details</span></a></li>
                                    <li><a href="{{url('user_approval_view')}}"><span class="sub-item">Admin Approval</span></a></li>
                                    <li><a href="{{url('changeparent')}}"><span class="sub-item">Parent Change</span></a></li>
                                    <li><a href="{{url('apiagent')}}"><span class="sub-item">Apipartner Browser</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#tables">
								<i class="fas fa-table"></i>
								<p>Payment Details</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="tables">
								<ul class="nav nav-collapse">
                                    <li><a href="{{url('opening_balance_view')}}"><span class="sub-item">Opening Balance</span></a></li>
                                    <li><a href="{{url('remotepayment')}}"><span class="sub-item">Remote Payment</span></a></li>
                                    <li> <a href="{{url('paymentaccept')}}"><span class="sub-item">Payment Accept</span></a></li>
                                    <li><a href="{{url('refundpayment')}}"><span class="sub-item">Refund Payment</span></a></li>
                                    <li><a href="{{url('payment_report')}}"><span class="sub-item">Payment Details</span></a></li>
                                    <li><a href="{{url('stockdetails')}}"><span class="sub-item">Stock Details</span></a></li>
                                    <li><a href="{{url('admin_user_stock_1')}}"><span class="sub-item">User Stock</span></a></li>
                                    <li><a href="{{url('backup_distributor_all')}}"><span class="sub-item">Distributor Stock</span></a></li>
                                    <li><a href="{{url('backup_view_all')}}"><span class="sub-item">Retailer Stock</span></a></li>
                                    <li><a href="{{url('stockdetails_all')}}"><span class="sub-item">All Stock</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#maps">
								<i class="fas fa-map-marker-alt"></i>
								<p>Collection</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="maps">
								<ul class="nav nav-collapse">
									<li><a  href="{{url('agent_allocation')}}"><span class="sub-item">Agent Allocation</span></a></li>
									<li><a  href="{{url('collection')}}"><span class="sub-item">Pending Report</span></a></li>
									<li><a  href="{{url('collection_verify')}}"><span class="sub-item">Collection Verify</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#charts">
								<i class="far fa-chart-bar"></i>
								<p>Recharge Report</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="charts">
								<ul class="nav nav-collapse">
									<li><a  href="{{url('rechargedetails')}}"><span class="sub-item">Recharge Details</span></a></li>
									<li><a  href="{{url('rechargedetails_distributor')}}"><span class="sub-item">Distributor Recharge</span></a></li>
									<li><a  href="{{url('rechargerequest')}}"><span class="sub-item">Recharge Request</span></a></li>
									<li><a  href="{{url('serverreply')}}"><span class="sub-item">Server Reply</span></a></li>
									<li><a  href="{{url('pendingreport')}}"><span class="sub-item">Pending Recharge</span></a></li>
									<li><a  href="{{url('pendingbill')}}"><span class="sub-item">Pending EBBill</span></a></li>
									<li><a  href="{{url('pendingmoney')}}"><span class="sub-item">Pending Money Transfer</span></a></li>
									<li><a  href="{{url('rechargeinfo')}}"><span class="sub-item">Recharge Info</span></a></li>
									<li><a  href="{{url('admin_user_old_recharge_1')}}"><span class="sub-item">Old Recharge</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#charts1">
								<i class="fas fa-rupee-sign"></i>
								<p>Money Transfer</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="charts1">
								<ul class="nav nav-collapse">
									<li><a  href="{{url('agent_allocation')}}"><span class="sub-item">Agent</span></a></li>
									<li><a  href="{{url('collection')}}"><span class="sub-item">Remitter</span></a></li>
									<li><a  href="{{url('collection_verify')}}"><span class="sub-item">Beneficiary</span></a></li>
									<li><a  href="{{url('collection_verify')}}"><span class="sub-item">Report</span></a></li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#charts2">
								<i class="fas fa-brush"></i>
								<p>Utilities</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="charts2">
								<ul class="nav nav-collapse">
									<li><a  href="{{url('smsdetails')}}"><span class="sub-item">Sms Details</span></a></li>
									<li><a  href="{{url('smssend')}}"><span class="sub-item">Sms Send</span></a></li>
									<li><a  href="{{url('complaint')}}"><span class="sub-item">Complaints</span></a></li>
									<li><a  href="{{url('complaint_view')}}"><span class="sub-item">Complaints Details</span></a></li>
									<li><a  href="{{url('offer_entry')}}"><span class="sub-item">Flash News</span></a></li>
									<li><a  href="{{url('offer_view')}}"><span class="sub-item">Flash News Details</span></a></li>
									<li><a  href="{{url('mobile_user')}}"><span class="sub-item">Mobile User</span></a></li>
									<li><a  href="{{url('bonrixresult')}}"><span class="sub-item">Bonrix Result</span></a></li>
								</ul>
							</div>
						</li>
						
						
						
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="content">
                @yield('content')
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="#">
									Android
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Api Document
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Privacy Policy
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright ml-auto">
						&copy; 2016-2020,  <i class="fa fa-heart heart text-danger"></i>  <a href="#">Lakshmi Mobiles</a>
					</div>				
				</div>
			</footer>
		</div>

		
	
	</div>
	<!--   Core JS Files   -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> 
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

	<!-- Atlantis JS -->
	<script src="{{ asset('js/atlantis.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.js"></script>
    @yield('scripts')
	
</body>
</html>