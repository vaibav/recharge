<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('img/brand/11.jpg') }}">
<div class="logo"><img src = "{{ asset('img/brand/logo.png') }}" style = "height:40px;width:160px;" /></div>
<div class="sidebar-wrapper">
<ul class="nav">
    <li class="nav-item active">
        <a class="nav-link " href="{{url('dashboard')}}" id="axz" >
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="javascript:;" id="ax1" >
            <i class="material-icons">cloud</i>
            <p>Server</p>
        </a>
        <div id="ax1_" style="display: none;">
            <a class="dropdown-item" href="{{url('serveronoff')}}">OnOff Entry</a>
            <a class="dropdown-item" href="{{url('ad_view')}}">Advertisement</a>
            <a class="dropdown-item" href="{{url('n_backup_retailer')}}">Retailer Backup</a>
            <a class="dropdown-item" href="{{url('n_backup_update_retailer')}}">Retailer Backup Bal</a>
            <a class="dropdown-item" href="{{url('n_check_account_1')}}">Check Retailer</a>
        </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax2" >
        <i class="material-icons">network_check</i>
        <p>Network</p>
    </a>
    <div id="ax2_" style="display: none;">
        <a class="dropdown-item" href="{{url('network_type')}}">Network Type Entry</a>
        <a class="dropdown-item" href="{{url('network')}}">Network Entry</a>
        <a class="dropdown-item" href="{{url('network_surplus')}}">Network Surplus Entry</a>
        <a class="dropdown-item" href="{{url('network_line')}}">Network Line Entry</a>
        <a class="dropdown-item" href="{{url('network_line_apipartner')}}">API Partner Line</a>
        <a class="dropdown-item" href="{{url('offer_line')}}">Other Line</a>
        <a class="dropdown-item" href="{{url('n_netpack')}}">Package</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax3" >
        <i class="material-icons">settings_ethernet</i>
        <p>API Master</p>
    </a>
    <div id="ax3_" style="display: none;">
        <a class="dropdown-item" href="{{url('n_apirequest')}}">API Request Entry</a>
        <a class="dropdown-item" href="{{url('n_apirequest_network')}}">API Network Entry</a>
        <a class="dropdown-item" href="{{url('apirequest_edit')}}">API Request Edit</a>
        <a class="dropdown-item" href="{{url('n_apiresult')}}">API Result Entry</a>
        <a class="dropdown-item" href="{{url('apiprovider_method')}}">API Method Entry</a>
        <a class="dropdown-item" href="{{url('api_details_1')}}">API URL Details</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax4" >
        <i class="material-icons">person</i>
        <p>User</p>
    </a>
    <div id="ax4_" style="display: none;">
        <a class="dropdown-item" href="{{url('user')}}">User Creation Entry</a>
        <a class="dropdown-item" href="{{url('user_view')}}">User Details</a>
        <a class="dropdown-item" href="{{url('user_approval_view')}}">Admin Approval</a>
        <a class="dropdown-item" href="{{url('changeparent')}}">Parent Change</a>
        <a class="dropdown-item" href="{{url('apiagent')}}">Apipartner Browser</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax5" >
        <i class="material-icons">account_balance</i>
        <p>Payment Details</p>
    </a>
    <div id="ax5_" style="display: none;">
        <a class="dropdown-item" href="{{url('opening_balance_view')}}">Opening Balance</a>
        <a class="dropdown-item" href="{{url('remotepayment')}}">Remote Payment</a>
        <a class="dropdown-item" href="{{url('paymentaccept')}}">Payment Accept</a>
        <a class="dropdown-item" href="{{url('refundpayment')}}">Refund Payment</a>
        <a class="dropdown-item" href="{{url('payment_report')}}">Payment Details</a>
        <a class="dropdown-item" href="{{url('stockdetails')}}">Stock Details</a>
        <a class="dropdown-item" href="{{url('admin_user_stock_1')}}">User Stock</a>
        <a class="dropdown-item" href="{{url('backup_distributor_all')}}">Distributor Stock</a>
        <a class="dropdown-item" href="{{url('backup_view_all')}}">Retailer Stock</a>
        <a class="dropdown-item" href="{{url('stockdetails_all')}}">All Stock</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax6" >
        <i class="material-icons">money</i>
        <p>Collection</p>
    </a>
    <div id="ax6_" style="display: none;">
        <a class="dropdown-item" href="{{url('agent_allocation')}}">Agent Allocation</a>
        <a class="dropdown-item" href="{{url('collection')}}">Pending Report</a>
        <a class="dropdown-item" href="{{url('collection_verify')}}">Collection Verify</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax7" >
        <i class="material-icons">timeline</i>
        <p>Recharge Report</p>
    </a>
    <div id="ax7_" style="display: none;">
        <a class="dropdown-item" href="{{url('rechargedetails')}}">Recharge Details</a>
        <a class="dropdown-item" href="{{url('rechargedetails_distributor')}}">Distributor Recharge</a>
        <a class="dropdown-item" href="{{url('rechargerequest')}}">Recharge Request</a>
        <a class="dropdown-item" href="{{url('serverreply')}}">Server Reply</a>
        <a class="dropdown-item" href="{{url('pendingreport')}}">Pending Recharge</a>
        <a class="dropdown-item" href="{{url('pendingbill')}}">Pending EBBill</a>
        <a class="dropdown-item" href="{{url('pendingmoney')}}">Pending Money Transfer</a>
        <a class="dropdown-item" href="{{url('rechargeinfo')}}">Recharge Info</a>
        <a class="dropdown-item" href="{{url('admin_user_old_recharge_1')}}">Old Recharge</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax8" >
        <i class="material-icons">add_alert</i>
        <p>Money Transfer</p>
    </a>
    <div id="ax8_" style="display: none;">
        <a class="dropdown-item" href="{{url('agent_allocation')}}">Agent</a>
        <a class="dropdown-item" href="{{url('collection')}}">Remitter</a>
        <a class="dropdown-item" href="{{url('collection_verify')}}">Beneficiary</a>
        <a class="dropdown-item" href="{{url('collection_verify')}}">Report</a>
    </div>
    </li>
    <li class="nav-item ">
    <a class="nav-link " href="javascript:;" id="ax9" >
        <i class="material-icons">wb_incandescent</i>
        <p>Utilities</p>
    </a>
    <div id="ax9_" style="display: none;">
        <a class="dropdown-item" href="{{url('smsdetails')}}">Sms Details</a>
        <a class="dropdown-item" href="{{url('smssend')}}">Sms Send</a>
        <a class="dropdown-item" href="{{url('complaint')}}">Complaints</a>
        <a class="dropdown-item" href="{{url('complaint_view')}}">Complaint Details</a>
        <a class="dropdown-item" href="{{url('offer_entry')}}">Flash News</a>
        <a class="dropdown-item" href="{{url('offer_view')}}">Flash News details</a>
        <a class="dropdown-item" href="{{url('mobile_user')}}">Mobile User</a>
        <a class="dropdown-item" href="{{url('bonrixresult')}}">Bonrix Result</a>
    </div>
    </li>
    
    
</ul>
</div>
</div>