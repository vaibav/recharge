<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('includes.common_meta')
    @include('includes.admin_head')
    @yield('styles')
</head>

<body class="">
  <div class="wrapper ">
    @include('layouts.admin_sidebar')
    <div class="main-panel">
    @include('includes.admin_header')
      <div class="content" style="margin-top:35px;">
        <div class="container-fluid">
            @yield('content')
        </div>
      </div>

    </div>
  </div>
  @include('includes.admin_footer')
  @yield('scripts')
   <script>
  $(document).ready(function() { $('.sidebar-wrapper ul li a').on('click',function(e) { $('#' + this.id + '_').toggle('slow'); }); });
  </script>
</body>
</html>