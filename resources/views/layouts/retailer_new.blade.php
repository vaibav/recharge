<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  @include('includes.common_meta')
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js"></script>
  
 
  <script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['{{ asset("css/fonts.min.css") }}']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
  </script>
  <!-- Nucleo Icons -->
  <link href="{{ asset('ret/css/nucleo-icons.css') }}" rel="stylesheet" />
  <link href="{{ asset('ret/css/nucleo-svg.css') }}" rel="stylesheet" />
  
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="{{ asset('ret/css/argon.css') }}" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.css" rel="stylesheet">
  @yield('styles')
</head>
<body class="landing-page">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="#">
        <img src="{{ asset('img/brand/logo.png') }}">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="#">
                <img src="{{ asset('img/brand/logo.png') }}">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <ul class="navbar-nav navbar-nav-hover align-items-lg-center">
         
          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">User</span>
            </a>
            <div class="dropdown-menu">
              <a href="{{url('user_one')}}" class="dropdown-item">User Details</a>
              <a href="{{url('surplus_retailer')}}" class="dropdown-item">Network Surplus</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">Payment</span>
            </a>
            <div class="dropdown-menu">
              <a href="{{url('paymentrequest_retailer')}}" class="dropdown-item">Request</a>
              <a href="{{url('stockdetails_retailer')}}" class="dropdown-item">Stock</a>
              <a href="{{url('report_money_verify')}}" class="dropdown-item">Beneficiary Verify</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">Report</span>
            </a>
            <div class="dropdown-menu">
              <a href="{{url('rechargedetails_retailer')}}" class="dropdown-item">Recharge</a>
              <a href="{{url('recharge_bill_retailer')}}" class="dropdown-item">EB Bill</a>
              <a href="{{url('recharge_money_retailer')}}" class="dropdown-item">Money Transfer</a>
              <a href="{{url('money_user_check')}}" class="dropdown-item">Money Go</a>
              <a href="{{url('ben_view')}}" class="dropdown-item">Beneficiary</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a href="#" class="nav-link" data-toggle="dropdown" href="#" role="button">
              <i class="ni ni-collection d-lg-none"></i>
              <span class="nav-link-inner--text">Utilities</span>
            </a>
            <div class="dropdown-menu">
              <a href="{{url('complaint_retailer')}}" class="dropdown-item">Complaint Entry</a>
              <a href="{{url('complaint_view_retailer')}}" class="dropdown-item">Complaint Report</a>
              <a href="{{url('collection_retailer')}}" class="dropdown-item">Collection Report</a>
              
            </div>
          </li>

        </ul>
        <ul class="navbar-nav align-items-lg-center ml-lg-auto">
          <li class="nav-item">
            <a class="nav-link " href="#" target="_blank" data-toggle="tooltip" title="Like us on Facebook">
              
              <span class="nav-link-inner--text ">Hi {{$user->user}}</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="#" target="_blank" data-toggle="tooltip" title="Follow us on Instagram">
              
              <span class="nav-link-inner--text ">Bal:{{$user->ubal}}</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{url('dashboard_retailer')}}" >
              <i class="fas fa-home"></i>
              <span class="nav-link-inner--text d-lg-none">Home</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-icon" href="{{url('logout')}}" >
              <i class="fas fa-sign-out-alt"></i>
              <span class="nav-link-inner--text d-lg-none">Logout</span>
            </a>
          </li>
         
          
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->

  <div class="wrapper">
    
  	@yield('content')
    <br /><br />
    <footer class="footer">
      <div class="container">
       
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; 2021 <a href="" target="_blank">Lakshmi Mobiles</a>.
            </div>
          </div>
          <div class="col-md-6">
            <ul class="nav nav-footer justify-content-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Api Document</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Privacy Policy</a>
              </li>
              
            </ul>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!--   Core JS Files   -->
  <script
  src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
  <script src="{{ asset('ret/js/argon-design-system.min.js') }}" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.0/sweetalert2.js"></script>
  <script type="text/javascript">
    $(function() {
      $("#close").click(function() {
        $('.overlay-ribbon').hide();
      });

    });
  </script>

  @yield('scripts')
</body>

</html>